---
id: "2834963876774059"
title: Fräuleins Mini-Weihnachtsmarkt
start: 2020-12-12 11:00
end: 2020-12-12 16:00
locationName: Fräulein Berger
address: Herrenkellergasse 14, 89073 Ulm
link: https://www.facebook.com/events/2834963876774059/
image: 130280722_3503159209797349_4987930596349269944_n.jpg
teaser: DAS FRÄULEIN ZU VERSCHENKEN!   Diesen Samstag 11:00 bis 16:00 unser
  klitzekleiner Mini-Weihnachtsmarkt, mit vielen tollen Leckereien zum
  verschenke
isCrawled: true
---
DAS FRÄULEIN ZU VERSCHENKEN!


Diesen Samstag 11:00 bis 16:00 unser klitzekleiner Mini-Weihnachtsmarkt, mit vielen tollen Leckereien zum verschenken oder selber genießen

vegane Plätzchen • Bratapfelmarmelade Lebkuchengranola • Ingwer Zitronen Sirup 
Energy Balls • Glühweinzaubergewürz 

Alles mit Liebe für euch selbstgemacht 🧡

Am TO GO Fenster wird es auch weihnachtlich:

Tannenspitzentee • Glühwein • Bratäpfemuffins  Chai-Bananenbrot • Glühwein-Brownies 

 Kommt vorbei! Wir freuen uns auf euch!