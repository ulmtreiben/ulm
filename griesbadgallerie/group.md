---
name: Griesbadgallerie
website: http://www.griesbadgalerie.de/
email: mail@griesbadgalerie.de
scrape:
  source: facebook
  options:
    page_id: Griesbadgalerie
---
Die Griesbadgalerie befindet sich im historischen Stadtviertel „Auf dem Kreuz“ am Seelengraben. Als eines der ältesten erhaltenen Gebäude im Ulmer Westen ist es seit über 30 Jahren ein geschätzter Ausstellungsraum im historischen Siechenhaus Ulms, einem Spätrenaissancehaus aus dem 16. Jahrhundert. Die historischen Räume prägen die besondere Atmosphäre der Ausstellungen. Drei Räume mit zentralen Säulen können für künstlerische Ausstellungen und Vorführungen genutzt werden; zudem gibt es Kellergewölbe, die künftig in künstlerische Konzepte einbezogen werden wird.
