---
id: "329133754926612"
title: Dead Plants and Living Objects / opening
start: 2020-08-21 19:00
end: 2020-08-21 22:00
locationName: Griesbadgalerie
address: Seelengraben 30, 89073 Ulm
link: https://www.facebook.com/events/329133754926612/
teaser: '"this sounds good (and looks good)" presents:  Rie Nakajima (UK) + Pierre
  Bereth (Belgien) // Dead Plants and Living Objects Ausstellung in der Griesb'
isCrawled: true
---
"this sounds good (and looks good)" presents:

Rie Nakajima (UK) + Pierre Bereth (Belgien) // Dead Plants
and Living Objects
Ausstellung in der Griesbadgalerie
21.08. – 27.09.20

21.08., 19h Vernissage und Sound-Performance

Rie Nakajima und Pierre Berthet interessieren sich für den Klang von Dingen, die man sich normalerweise nicht anhört. In ihrem fortlaufenden gemeinsamen Projekt „Dead Plants & Living Objects“ bringen sie ihre eigenwilligen Instrumente zum Duett zusammen. So harmoniert der tiefe Sound eines in flatternde Rohre pustenden
Staubsaugers mit dem filigranen Klirren von Geschirr, das von selbstständig durch den Raum wandernden Motoren angeschlagen wird. Mit ihren Installationen passen sich die Beiden immer wieder an die vorgefundene Raumsituation an. In Ulm werden sie die Griesbadgalerie innen, wie außen bespielen.

Rie Nakajima and Pierre Berthet are interested in the sound of things you normally don't listen to. In their ongoing joint project "Dead Plants & Living Objects" they bring their idiosyncratic instruments together in a duet. Thus, the deep sound of a vacuum cleaner blowing into fluttering pipes harmonizes with the filigree clinking of crockery struck by motors moving independently through the room. With their installations, the two of them repeatedly adapt to the room situation they find. In Ulm, they will use the Griesbadgalerie both inside and outside.

http://www.rienakajima.com
http://pierre.berthet.be/

This sounds good (and looks good) ist eine intermediale, interdisziplinäre Programmreihe, die neue Musik, experimentelle, akustische Performance und visuelle Kunst verbindet. Wichtig ist uns das erlebbar-machen extremer Ansätze von zeitgenössischen Kompositionen, in Kombination mit medialer Kunst und der Live-Performance der Künstler.

