---
id: "1185933551909377"
title: I never promised you a Rose garden
start: 2021-08-06 18:00
end: 2021-08-08 18:00
locationName: Rosengarten Ulm Donau
address: 89073 Ulm
link: https://www.facebook.com/events/1185933551909377/
image: 226098608_4343196669061450_5153026649779966881_n.jpg
isCrawled: true
---
FR 06.08.2021, 18 – 24 Uhr/ 
Vernissage: 19 Uhr
SA 07.08.2021, 14 – 24 Uhr
SO 08.08.2021, 11 – 18 Uhr
Rosengarten / In der Adlerbastei

Biancoshock/ Christian Greifendorf/ Edgar Braig/ Emil Kräs/
Guido Weggenmann/ Helmut Smits/ Matthias Garff/ Patrick Nicolas/ Peter Pan Soundwerkstatt/ Richard Geczi/ Sany und Sil Krol

Der Rosengarten in Ulm wird für ein Wochenende in einen Park für bildende, zeitgenössische Kunst verwandelt. Es gilt die pittoreske Anmutung und Verschlafenheit des Gartens in einen lebendigen Kontext zur aktuellen Kunst zu setzten. Das gelingt am besten durch ortsspezifische Installationen und künstlerische Performance. Auch skulpturale Arbeiten werden einen wichtigen Raum einnehmen und die Spannung erhöhen.
12 Künstler*innen werden mit ihren Arbeiten, Ideen und Interventionen den Rosengarten neu definieren. Aus ganz unterschiedlichen Positionen heraus,
machen sie sich den Platz zu Eigen. Und wir werden staunen, grübeln, verblüfft sein und es in vollen Zügen genießen!

For one weekend, the Rose Garden in Ulm will be transformed into a park for visual, contemporary art. The aim is to put the picturesque charm and sleepiness of the garden in an exciting context with current modern art.
This is best achieved through site-specific installations and artistic performance. Sculptural works will also occupy an important space and increase the tension.
12 artists will redefine the Rose Garden with their works, ideas and interventions. From very different positions, they make the space their own and invite us to wonder, reflect and surprise!
And we will marvel, ponder, be amazed and enjoy it!

In Zusammenarbeit und Unterstützung durch das Gleis 44/ Stadt Ulm/ Neustart Kultur/ Förderung der Bundesregierung für Kultur und Medien
In cooperation with and supported by Gleis 44/ City of Ulm/ Neustart Kultur/ Funding by the Federal Government for Culture and Media



