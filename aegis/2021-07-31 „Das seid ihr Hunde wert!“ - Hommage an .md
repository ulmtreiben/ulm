---
id: "2846658352262917"
title: „Das seid ihr Hunde wert!“ - Hommage an Erich Mühsam
start: 2021-07-31 19:00
locationName: Reithalle Ulm (offizielle Seite)
address: Schillerstraße 1, 89077 Ulm
link: https://www.facebook.com/events/2846658352262917/
image: 214811839_4385879748110506_6579131297295819937_n.jpg
isCrawled: true
---
Eröffnung der Literaturwoche Donau

Eigen, Mutig, Gut: Der singende Tresen, Manja Präkels & Markus Liske 

„Das seid ihr Hunde wert!“
 Eine Hommage an Erich Mühsam 

Erich Mühsams Tagebücher im Gepäck durchqueren Markus Liske, Manja Präkels & Der Singende Tresen das Land und widmen ihre Kunst dem Dichter und Revolutionär Erich Mühsam, der zwischen zwei Weltkriegen, zwischen Hoffnung und Verzweiflung stets mit dem Humor eines Überlebenskünstlers ausgerüstet war. Mühsam schrieb Kampf- und Spottlieder, leidenschaftliche Plädoyers für politische Gefangene und verfolgte ein anarchistisches Revolutionskonzept. Es wird Zeit, den Publizisten und Antimilitaristen Mühsam wieder zu entdecken als das, was er (auch) war: Ein wortmächtiger Humanist. Unveröffentlichte Schätze aus dem Nach-lass dieses umtriebigen Protagonisten und vertonte Texte führen mit Spielfreude und Witz zu einem furiosen Abend: „Man höre, wie unvergleichlich Manja Präkels singt! (... ) so, wie sie singt, könnte sie das Telefonbuch heruntersingen und damit gegen alle guten Sitten verstoßen!“ – so der Wiener Kritiker Harald Justin, dem man nur jubelnd zustimmen kann. 

Manja Präkels – Texte, Stimme, Ukulele / Thorsten Müller – Klarinetten, Akkordeon, Orgel / Benjamin Hiesinger – Kontrabaß / Florian Segelke – Gitarren / Johannes Metzger – Schlagwerk / Lesend und brummend: Markus Liske

https://buchmarkt.de/wp-content/uploads/2021/07/LIWO-FLYER-2021.pdf

Der Kultursommer ist eine Initiative der Kulturabteilung der Stadt Ulm in Zusammenarbeit mit der Ulmer Kulturszene. Die einzelnen Programmpunkten werden von Gleis 44, Roxy Ulm oder HfG Archiv veranstaltet. Alle Beteiligten bedanken sich bei den Stadtwerke Ulm / Neu-Ulm und bei der Kulturstiftung des Bundes für die freundliche Unterstützung. Die Veranstaltungsreihe wird im Programm Kultursommer 2021 durch die Beauftragte der Bundesregierung für Kultur und Medien (BKM) mit Mitteln aus NEUSTART KULTUR gefördert.