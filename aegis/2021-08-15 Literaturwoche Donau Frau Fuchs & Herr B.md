---
id: "1155392294941267"
title: "Literaturwoche Donau: Frau Fuchs & Herr Bokowski"
start: 2021-08-15 19:30
end: 2021-08-15 21:30
address: Wilhelmsburg, 89075 Ulm
link: https://www.facebook.com/events/1155392294941267/
image: 211822467_3041536632785805_4090917246980641836_n.jpg
isCrawled: true
---
Frau Fuchs & Herr Bokowski: Lachen Sie mal, ganz im Ernst. Leseshow
Lesebühne in der Wilhelmsburg

Pointiert, überspitzt und satirisch: Bei Fuchs und Bokowski reihen sich messerscharfe Sätze an skurrile Dialoge. Als Teil der Lesebühne „Fuchs und Söhne“ beehren uns heute Abend Kirsten Fuchs und Paul Bokowski. Ein glänzendes Doppel auf der Lesebühne: Mal komisch, mal ernst, schräg und schnoddrig - selten leuchtete unsere Gegenwart so originell wie in den Texten von Fuchs und Bokowski. 

Kirsten Fuchs, geboren 1977 in Karl-Marx-Stadt, ist Schriftstellerin, Lesebühnenautorin und Kolumnistin und lebt in Berlin. Sie schreibt regelmäßig für „Das Magazin“ und die „Süddeutsche Zeitung“ und hat diverse Romane, Kurzgeschichtenbände sowie Theaterstücke veröffentlicht. Kirsten Fuchs war bei verschiedenen Lesebühnen aktiv, u. a. „Erfolgsschriftsteller im Schacht“, „O-Ton Ute“ und „Chaussee der Enthusiasten“. 
Seit 2014 liest sie monatlich bei der Lesebühne „Fuchs & Söhne“. 

Paul Bokowski wurde 1982 in Mainz am Rhein geboren. Der langjährige Wahlberliner ist Gründungsmitglied der stadtbekannten Lesebühne „Fuchs & Söhne“. Zuletzt erschien das Hörbuch „Feine Auslese. Bokowski gibt sein 
Bestes“ (Voland & Quist).

Bei schlechtem Wetter in der Museums-
gesellschaft Ulm, Neue Straße 85, nur begrenzte Anzahl Plätze möglich