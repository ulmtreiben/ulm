---
id: "1676706879386297"
title: '"POEDU" - Kinderkonzert mit Poesie von Kindern für Kinder'
start: 2021-08-08 11:00
locationName: Teutonia Biergarten & Restaurant
address: Friedrichsau 6, 89073 Ulm
link: https://www.facebook.com/events/1676706879386297/
image: 227256503_561302438555795_827163343188943265_n.jpg
isCrawled: true
---
-Die Veranstaltung muss leider in der Teutonia stattfinden!!-

Für dieses besondere Kinderkonzert machen Indauna und das Ponte Festival gemeinsame Sache mit den Aegis Buchhandlungen. Franziska Pentz und Rasmus Schöll entdeckten das wundervolle Buch „POEDU – Poesie von Kindern für Kinder“ des Elif Verlages, durch welches Kinder in dieser besonderen Pandemiezeit zu kleinen PoetInnen werden durften - ein rundum gelungenes Projekt, in dem eine Menge Kreativität und Herzblut steckt. Daraus wird die Schauspielerin Sonja Halter Ausschnitte lesen, erzählen und zum Mitmachen anregen. Mit ausgewählten Klavierstücken von Schumann, Debussy und Satie - gespielt von Janis Pfeifer - wird der Fantasie hier freien Lauf gelassen.

Der Eintritt ist frei, um Spenden wird gebeten. 
Bei Regen findet die Veranstaltung in der Teutonia statt.

Bild: Hohensteinschule Stuttgart