---
id: "346714043250069"
title: Iris Wolff & Corinna Kroker - "Die Unschärfe der Welt." Über das Schreiben,
  über Geschichte(n)
start: 2021-03-07 17:00
link: https://www.facebook.com/events/346714043250069/
image: 156806257_3780314788680944_8798642897642093171_o.jpg
teaser: Mit ihrem neuesten Roman „Die Unschärfe der Welt“ legt Iris Wolff erneut einen
  berührenden Roman über eine Familie aus dem Banat vor - deren Bande so
isCrawled: true
---
Mit ihrem neuesten Roman „Die Unschärfe der Welt“ legt Iris Wolff erneut einen berührenden Roman über eine Familie aus dem Banat vor - deren Bande so eng geknüpft sind, dass sie selbst über Grenzen hinweg nicht zerreißen. Ein Roman über Menschen aus vier Generationen, der auf berückend poetische Weise Verlust und Neuanfang miteinander in Beziehung setzt. Nominiert für den Deutschen Buchpreis 2020, den Bayerischen Buchpreis in der Kategorie Belletristik 2020 sowie den Wilhelm-Raabe-Literaturpreis 2020. Iris Wolff und ihre Lektorin Corinna Kroker (Klett Cotta Verlag) sprechen über das komplexe Handwerk des Schreibens und Erinnerns.
Moderation Florian L. Arnold

Die Premiere findet am kommenden Sonntag um 18 Uhr statt: https://youtu.be/my41kfu1Sxo

Mit Dank an den Kulturspeicher Ulm im Kornhaus, Jan Ilg und die Aegis Buchhandlung.

-- 