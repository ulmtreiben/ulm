---
id: "139524161653158"
title: "Literaturwoche Donau: Vier Frauen, vier Generationen,  vier Lebenswege:
  Yvonne Hergane liest"
start: 2021-08-04 19:30
end: 2021-08-13 21:15
locationName: Cabaret Eden
address: Karlstraße 71, 89073 Ulm
link: https://www.facebook.com/events/139524161653158/
image: 223498484_3055393254733476_3808582686084273776_n.jpg
isCrawled: true
---
4. 8. 2021, 19:30 Uhr
Vier Frauen, vier Generationen, 
vier Lebenswege: Yvonne Hergane liest aus „Die Chamäleondamen“
Moderation: Sarah Käsmayr, 
Verlegerin des MaroVerlags
+ Konzert Bernhard Eder
Ort: Cabaret Eden, Karlstr. 71

Eine der spannendsten Neuerscheinungen 2020 und sogleich auf der Liste „Bayerns Beste Independent Bücher 2020“: Yvonne Herganes Romandebüt spannt über mehr als 120 Jahre einen Bogen von der ersten bis zur letzten Mutterfigur einer Familie. Raffiniert ist nicht nur die Sprache, sondern auch die Konstruktion, denn die Lebensläufe werden nicht chronologisch erzählt, sondern parallel. Das bedeutet: „Die Chamäleondamen“ ist ein großes Panorama von feinsinnig poetisch und mitunter bitterkomisch erzählten Schicksalen, das einem nicht mehr aus dem Kopf gehen wird. 
Yvonne Hergane, 1968 in Reschitza, Rumänien geboren, arbeitet als Autorin sowie literarische Übersetzerin aus dem Englischen, vor allem von Kinder- und Jugendliteratur. 