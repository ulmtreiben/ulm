---
id: "1430838353778837"
title: "Literatur unter Bäumen: Wunder. Mirabilis Verlag, Meissen"
start: 2020-08-29 19:00
end: 2020-08-29 21:00
locationName: Stadtbücherei Neu-Ulm Bibliothek
address: Heiner-Metzger-Platz 1, 89231 Neu-Ulm
link: https://www.facebook.com/events/1430838353778837/
teaser: Literatur unter Bäumen widmet sich in seiner vierten Auflage den
  Verlegerinnen. War die Buchbranche früher eine männliche Domäne, ist die
  unabhängige
isCrawled: true
---
Literatur unter Bäumen widmet sich in seiner vierten Auflage den Verlegerinnen. War die Buchbranche früher eine männliche Domäne, ist die unabhängige Verlagsszene der Gegenwart geprägt durch zahlreiche Verlegerinnen, die für neue, unbekannte, übersehene oder unterschätzte Bereiche der Literatur eintreten. Das wichtigste Kapital der eingeladenen Verlegerinnen ihre Begeisterung, ihre Überzeugung von der absoluten Notwendigkeit ihrer Arbeit und ihre Liebe zum Wort. Im diesjährigen Programm wollen wir diese Verlegerinnen kennenlernen.  "Ohne Autoren kein Verlag" - welche Texte sind Vergelerinnen wichtig, wie arbeiten sie, worüber sprechen ihre Bücher? 

„Mirabilia“ bedeutet auf deutsch „Wunder“. Der Mirabilis Verlag, 2011 von Barbara Miklaw ins Leben gerufen, ist ein Schatzkästchen für besondere Literatur vom Gedicht bis zum hochwertigen Kunstband. Anliegen des Verlags ist es, Bücher zu veröffentlichen, die auch lange nach dem Lesen noch in Erinnerung bleiben – der besonderen Sprache, der tiefgehenden und berührenden Erzählweise wegen. "Ohne Autoren kein Verlag" ist das Credo der Verlegerin, die eine kontinuierliche und aufmerksame Zusammenarbeit mit den Autoren ins Zentrum stellt - von der Entdeckung bis zur öffentlichen Lesung. Besonderheit bei "Mirabilis": Doppeltalente. Viele AutorenInnen sind zugleich auch als Bildende KünstlerInnen hervorgetreten. Bei Mirabilis werden die Bücher durchweg von Künstlern gestaltet. Als Doppeltalente in Kunst und Literatur hat die Verlegerin ihre Autoren Martina Altschäfer und Florian L. Arnold dabei, aber auch die Mirabilis-Titel zu Peter Handke und die Lyrik-Reihe tas:ir werden eine Rolle spielen.
mirabilis-verlag.de

Durch den Abend führt Bibliothekar, Buchblogger und Literaturspezialist Marius Müller.

Eintritt: 8 € inkl. eines nichtalkoholischen Getränks
Tickets an der Abendkasse, Einlass 18:30 Uhr

Wegen schlechter Witterung sind wir in der Stadtbücherei 
Neu-Ulm, Heiner-Metzger-Platz 1. Dort sind aufgrund der Corona-Regeln nur 25 Plätze möglich, wir bitten darum, zeitig da zu sein. Plätze begrenzt!

www.literaturbaeume.neu-ulm.de
www.literatursalon.net
facebook.com/literaturbaeume/ 
instagram: stadt.neuulm
Die Reihe wird ermöglicht dank der Unterstützung durch Prof. Gerhard Mayer. Eine Veranstaltungsreihe der Stadt Neu-Ulm in Kooperation mit dem Literatursalon Donau e. V.