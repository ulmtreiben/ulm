---
id: "3116833585099561"
title: "Stürmt die Burg: Die Buchhandlung bringt Gäste mit - Aus Glut zum Trost"
start: 2020-08-13 20:00
end: 2020-08-13 21:30
locationName: Festung Wilhelmsburg Xii
address: Prittwitz Straße, Ulm
link: https://www.facebook.com/events/3116833585099561/
teaser: "Die Buchhandlung bringt Gäste mit: Aus Glut zum Trost Buchhandlung Aegis mit
  Dincer Gücyeter und Wolfgang Schiffer Durch den Abend geleiten Rasmus Sch"
isCrawled: true
---
Die Buchhandlung bringt Gäste mit:
Aus Glut zum Trost
Buchhandlung Aegis mit Dincer Gücyeter und Wolfgang Schiffer
Durch den Abend geleiten Rasmus Schöll und Florian L. Arnold.

20 Uhr

2017 erschienen im kleinen, aber feinen Nettetaler Elif Verlag zwei Gedichtbände. Aus Glut geschnitzt von Dincer Gücyeter und Denen zum Trost, die sich in ihrer Gegenwart nicht finden können des Isländers Ragnar Helgi Olafsson, aus dem Isländischen übersetzt von Wolfgang Schiffer. Beide Werke wurden von den Leserinnen und Lesern und auch von der Literaturkritik sehr gelobt: "Aus Glut geschnitzt" ist ein sprachlich großartig verfasstes Buch eines leidenschaftlichen Dichters (Matthias Ehlers/WDR). "Denen zum Trost…" ist ein nicht nur ein bewegender Gedichtband, sondern auch ein bibliophiles Kleinod, das sich wohltuend von der Masse der Bücher abhebt. (Gerrit Wustmann/SIGNATUREN)

Wolfgang Schiffer, geboren 1946 in Nettetal-Lobberich, studierte Germanistik, Philosophie und Theaterwissenschaften; arbeitete seit 1976 als Hörspieldramaturg beim WDR, wo er von 1991 bis 2011 in leitender Position für Hörspiel, Radio-Feature und Literatur zuständig war. Dem Gedichtband von Ragnar Helgi Ólafsson, der inzwischen in der 3. Auflage vorliegt, folgten im ELIF Verlag bis heute in Zusammenarbeit mit Jón Thor Gíslason drei weitere Übersetzungen aus dem Isländischen. Für seine Publikationen als Autor, Übersetzer und Herausgeber erhielt Schiffer zahlreiche Auszeichnungen, u.a. das Ritterkreuz des Isländischen Falkenordens.

Dinçer Güçyeter, geb.1979, Nettetal, Vater von zwei Kindern, gründete 2012 den ELIF VERLAG (www.elifverlag.de) mit dem Programmschwerpunkt Lyrik. Im Verlag erschienen Einzelbände und Anthologien mit bundesweit zahlreichen Lesungen. Ende 2017 hat Dincer Gücyeter seinen neuen Gedichtband »Aus Glut geschnitzt« veröffentlicht, der bereits in der zweiten Auflage vorliegt.                                                       -------- 
Stürmt die Burg 2020
Der Pop-up-Space auf der Wilhelmsburg Kaserne in diesem Sommer in die dritte Runde, anders als gewohnt, noch wichtiger als sonst! Mit ihrem großzügigen Innenhof bietet die Bundesfestung ausreichend Platz, um auch in diesem Sommer die Burg zu stürmen – mit Abstand versteht sich und mit Fokus auf lokale Kulturakteure aus Ulm und der Region. Jeweils von Donnerstag bis Sonntag erwartet euch ein buntes Kulturangebot. Unter dem Motto #kulturerhalten bespielen in diesem Jahr lokale Künstlerinnen und Künstler den Burginnenhof und bringen Kunst und Kultur zurück auf die Bühne. Von Musik über Tanz bis hin zu Comedy, Literatur und Schauspiel – das Programm ist so vielfältig wie die Ulmer Kulturszene selbst. An den Donnerstagen und Samstagen gestalten Mitglieder des Arbeitskreis Kultur das Bühnenprogramm, die Freitage und Sonntage wurden im Rahmen einer Open-Stage-Ausschreibung an Ulmer Kulturakteure vergeben. Sonntags gibt es Kulturprogramm für die ganze Familie - immer um 14 Uhr einen Programmpunkt speziell für Kinder. 
--------
Burgbar No. XII
Neuer Name, neues Konzept, gewohnte Location und gewohnt tolle Atmosphäre - das ist die Burgbar No. XII. Ab dem 24. Juli dienen zwei umgebaute Container als Corona-konforme Pop up Bar auf der
Wilhelmsburg. Streetfood-Stände sorgen von Donnerstag bis Sonntag für kulinarische Vielfalt. Sonntags erwartet die Gäste ein Frühschoppen mit Weißwurst, am Nachmittag wird das Angebot durch Kuchen und Kaffee vom Coffee Bike erweitert. Öffnungszeiten: Do.-Sa.: 18-24 Uhr;
So.: 10.30-18 Uhr. Weitere Infos unter www.burgbar12.de
--------
Free Shuttle - Der Wilhelmsbus
Von Donnerstag bis Samstag gibt es einen kostenlosen Shuttlebus [halbstündlich 18-24 Uhr], der vom Hans-und-Sophie-Scholl-Platz (Neue Mitte) und einem weiteren Halt an der Bushaltestelle Frauenstraße zur Wilhelmsburg fährt und die Besucherinnen und Besucher anschließend wieder in die Stadt bringt. Wir wollen euch und uns vor einer Infektion schützen – bitte tragt deshalb eine Mund-Nasen-Bedeckung im Bus. An den Sonntagen ist kein Shuttlebus-Verkehr eingerichtet. Aktuelle Informationen gibt es hier: www.die-wilhelmsburg.de
--------
Corona-Hinweis
Zur Sicherheit aller haltet euch bitte an die aktuell gültigen Hygiene- und Abstandsregelungen und bringt euren Mund- und Nasenschutz mit. Wir minimieren die Infektionsgefahr durch Maßnahmen wie Besucherbegrenzung und bitten euch, rücksichtsvoll mit den anderen Gästen umzugehen. Bei Krankheitssymptomen bleibt bitte zuhause. Lasst uns zusammen #kulturerhalten in diesem
besonderen Sommer - wir freuen uns auf euch! Ein Corona-FAQ findet ihr unter www.burgbar12.de/faq
--------
Der Pop up Space wird von der Kulturabteilung der Stadt Ulm koordiniert und von der Brauerei Gold Ochsen unterstützt. Alle Infos unter: www.die-wilhelmsburg.de