---
id: "545394643305304"
title: "Literaturwoche Donau: Gion Mathias Caveltys Buchlabyrinthe + Konzert Herr
  Polaris"
start: 2021-08-10 19:30
end: 2021-08-10 22:15
locationName: Cabaret Eden
address: Karlstraße 71, 89073 Ulm
link: https://www.facebook.com/events/545394643305304/
image: 214418815_3044668029139332_8670684690607939356_n.jpg
isCrawled: true
---
Was passiert, wenn Menschen nicht in Büchern lesen, sondern Bücher in Menschen? Beim Schweizer Autor Gion Mathias Cavelty ist nie etwas so, wie man es kennt – oder erwartet. Das neueste Werk ist eine absurde Höllenfahrt voll schrägem Personal und noch schrägerem Humor: Der Inquisitor Innozenz wird vom Papst ins kleine Dorf Schwamendingen geschickt. Seine Mission: den Schädel des ersten Menschen aufzuspüren, der sich dort in den Händen einer diabolischen Sekte befinden soll. Begleitet wird Innozenz vom absolut reinen Buch, das durch keinen einzigen Buchstaben besudelt wird. 

Gion Mathias Cavelty (*1974 in Chur) ist Schriftsteller und Satiriker. Sein Debütroman „Quifezit“ machte ihn in den 1990er-Jahren zum literarischen Wunderkind. 
Seine Bücher stellen ironisch-subversiv alle geltenden Regeln des Literaturbetriebes auf den Kopf und Daniel Kehlmann wünscht sich: „Würde Cavelty doch nur mehr schreiben!“
Moderation: Florian L. Arnold, Autor

21:30 Uhr: 
Konzert Der Herr Polaris
Der Herr Polaris ist etwas für Liebhaber abwegig schöner Songs, der besonderen Sprache, die immer schon mehr wagte, weiter ging, präziser war, als vieles andere da draußen. Für eine Welt, die Kopf steht, aber gut damit leben kann.

Nur begrenzte Anzahl Plätze möglich.

#Corona_Hinweis
Zu Aufführungen in Innenräumen bitte immer einen tagesaktuellen Coronatest oder Genesenenbestätigung etc. vorzeigen, anderenfalls ist kein Einlass möglich. 

Zur Sicherheit aller haltet euch bitte an die aktuell gültigen Hygiene- und Abstandsregelungen – weitere Informationen veröffentlichen wir auf der Website. Bei Krankheitssymptomen bleibt bitte zuhause. Stay safe & stay tuned!