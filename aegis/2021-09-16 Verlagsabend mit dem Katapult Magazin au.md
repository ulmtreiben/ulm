---
id: "909278236687421"
title: Katapult! So blickt man auf die Welt * Ulmer Leseorte
start: 2021-09-16 19:30
end: 2021-09-16 22:00
locationName: Cabaret Eden
address: Karlstraße 71, 89073 Ulm
link: https://www.facebook.com/events/909278236687421/
image: 241759892_1504800816525131_4234316371859365612_n.jpg
isCrawled: true
---
Katapult! So blickt man auf die Welt.

Das KATAPULT-Magazin ist das wachstumsstärkste Magazin Deutschlands und mischt von der Ostseestadt Greifswald aus seit 2015 die Medienbranche auf. Es hat mittlerweile über 80.000 Abonnenten und 700.000 Follower in den sozialen Medien, es baut eine alte Schule aus, pflanzt einen Wald, legt sich mit großen Verlagen an, hat einen Buchverlag und eine Lokalzeitung gegründet, gründet ein Musiklabel, entwickelt Online-Spiele wie die Kartenolympiade, initiiert eine Buchmesse in Greifswald und demnächst folgt eine eigene Journalistenschule ...
Wie haben die das gemacht? Gründer und Chefredakteur Benjamin Fredrich und Buchverlagsleiter Sebastian Wolter stellen KATAPULT vor und erzählen, warum man für solche Ideen ins Schwimmbad gehen muss.
Moderation: Florian L. Arnold

Achtung: Zuschauende müssen einen 2G-Nachweis vorzeigen! 

Beginn: 19:30 Uhr
Ort: Cabaret Eden

Eintritt: 10€


Der Ulmer Kultursommer ist eine Initiative der Kulturabteilung der Stadt Ulm in Zusammenarbeit mit der Ulmer Kulturszene. Die einzelnen Programmpunkte werden von Gleis 44, Roxy Ulm oder HfG Archiv veranstaltet. Alle Beteiligten bedanken sich bei den Stadtwerke Ulm / Neu-Ulm und bei der Kulturstiftung des Bundes für die freundliche Unterstützung.
www.katapult-verlag.de/
Veranstalter: ROXY gemeinnützige GmbH in Kooperation mit dem Literatursalon Donau, der Aegis Buchhandlung und dem Cabaret Eden