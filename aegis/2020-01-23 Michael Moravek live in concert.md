---
id: "593287601420124"
title: Michael Moravek live in concert
start: 2020-01-23 20:00
end: 2020-01-23 23:00
locationName: Aegis Literatur Buchhandlung
address: Breite Gasse 2, 89073 Ulm
link: https://www.facebook.com/events/593287601420124/
image: 81583778_1034358813569336_5182596418853928960_o.jpg
teaser: Michael Moravek gehört sicherlich zu den außergewöhnlichsten
  Singer-Songwritern Deutschlands. Seine Texte sind voller Poesie und Schönheit,
  seine Gita
isCrawled: true
---
Michael Moravek gehört sicherlich zu den außergewöhnlichsten Singer-Songwritern Deutschlands.
Seine Texte sind voller Poesie und Schönheit, seine Gitarre  umscheichelt das Ohr des Zuhörer, um sich in dessen Herz einzugraben und es nicht mehr loszulassen.
Songs, die die Zeit immer etwas langsamer vergehen lassen.
