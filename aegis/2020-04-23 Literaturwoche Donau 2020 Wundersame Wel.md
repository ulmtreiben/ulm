---
id: "571217563741004"
title: "Literaturwoche Donau 2020: Wundersame Welten + Kurzkonzert"
start: 2020-04-23 19:30
end: 2020-04-23 23:00
locationName: Casino
address: Weinhof 7, 89073 Ulm
link: https://www.facebook.com/events/571217563741004/
image: 88094150_2798417646870668_3828279276352831488_o.jpg
teaser: "WUNDERSAME WELTEN. Bernd Schuchter und Hubert Flattinger Moderation: Florian
  L. Arnold  Bernd Schuchter erzählt in „Rikolas letzter Auftritt“ vom dile"
isCrawled: true
---
WUNDERSAME WELTEN. Bernd Schuchter und Hubert Flattinger
Moderation: Florian L. Arnold

Bernd Schuchter erzählt in „Rikolas letzter Auftritt“ vom dilettierenden Schriftsteller Richard Kola, der ein Verlagshaus gründet – nicht irgendeins, sondern das größte Österreichs. Thomas Manns Felix Krull steht Pate bei dieser Erzählung von Aufstieg und Fall eines Hasardeurs: Es geht um die enthusiastische Liebe zur Literatur, um Skrupellosigkeit und den Gewinn auf Kosten anderer. So macht sich Kola schließlich an die Veröffentlichung von Adolf Hitlers zweitem Buch. 
Hubert Flattinger war Pflasterstein-Maler, Tierpfleger, Grafiker und Werbeleiter, aber geschrieben und gemalt hat er immer, von besonderen Menschen und Talenten. In seinem Erzählband „Mrs. O'Hara sagt Gute Nacht“ begegnen wir einem Piratenmädchen aus Strandhill, das dem Touristen aus „Oistaraik“ erst einmal Manieren beibringt. Da ist Ned Stoney, halb Junge, halb Greis, der wie kein anderer von Amerika – dem Land der unerschöpflichen Murmeln und Kartoffeln – zu schwärmen weiß. Und nicht zuletzt findet sich Mrs. O’Hara, die mit Toten spricht und besonders gut mit Geistern kann … 

Etwa 21:30: Kurzkonzert Lenka Zupkova, Violine & Electronics.
Lenka Zupkova improvisiert frei mit live-elektronischen Mitteln. Das Eregbnis: Eine Musik aus höchst überraschenden und komplexen Klangspuren. 

Ab 22 Uhr: Late Night Literatur: Preziosen. 
Die Wunderkammern des Limbus Verlag. Mit Bernd Schuchter

Der von Bernd Schuchter ins Leben gerufene Limbus Verlag verlegt seit nunmehr 15 Jahren gut Geschriebenes und gut Gedachtes. Eine Wunderkammer für Menschen, die sich nach sinnstiftender Literatur sehnen sind die „Limbus Preziosen“. Da findet man Étienne de La Boéties „Abhandlung über die freiwillige Knechtschaft“, Heinrich Heines „Bummel durch Tirol“ und einen brandneuen Band mit verschollenen Erzählungen von Marlen Haushofer. Öffnen wir diese Schatzkiste!