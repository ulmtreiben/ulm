---
id: "1974139982761218"
title: "Literatur unter Bäumen: Expeditionen. Ink Press aus Zürich"
start: 2021-09-04 19:30
address: Auf dem Schwal, Insel, Neu-Ulm
link: https://www.facebook.com/events/1974139982761218/
image: 233261963_4435579846507318_4896945394588024971_n.jpg
isCrawled: true
---
INK PRESS ist ein unabhängiger Schweizer Verlag für Literatur und Kunst in Zürich: 2015 wurde der Verlag von Verlegerin und Buchhändlerin Susanne Schenzle gegründet. Schenzle war vorher beim Ammann Verlag tätig gewesen und hatte mit Christian Ruzicska den Secession Verlag für Literatur gegründet. Inkpress bringt Bücher aus dem Bulgarischen, in der Reihe TADOMA lernt man internationale Literatur aus allen anderen Ländern kennen. 

"Ink Press ist ein Raum für Künstlerinnen und Künstler und ihre Publikationen, eine Möglichkeit, mit Haut und Haaren an die Öffentlichkeit zu treten, pur und klar, ohne Schnörkel und Bandagen", so Susanne Schenzle. Sie kommt mit ihrer Überetzerin Viktoria Dimitrova Popova, deren Übersetzung von Kalin Terzijskis Roman "Alkohol" die Verlagsgründung und die erfolgreiche bulgarische Reihe quasi in Gang setzte. 
Durch den Abend führt Florian L. Arnold
Lesung: Anna-Elisabeth Brüderl

Musik: Elisabeth Haselberger, Virtuosin für jegliche Form von Flöte, wird uns in Klangwelten führen zwischen Harmonie und Fremdheit, Elektronik und reinem Flötenklang.

Sollte das Wetter nicht mitspielen, sind wir in den Innenräumen der NUWOG in der Schützenstraße 32, 89231 Neu-Ulm. In den Innenräumen gelten die aktuellen AHA-Regeln, Abstandsregeln und die tagesaktuellen Bestimmungen zum Coronaschutz. 