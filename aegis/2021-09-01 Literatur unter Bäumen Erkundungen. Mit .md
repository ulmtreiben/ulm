---
id: "1572584523084304"
title: "Literatur unter Bäumen: Erkundungen. Mit dem Dörlemann Verlag"
start: 2021-09-01 19:30
address: Auf dem Schwal, Insel, Neu-Ulm
link: https://www.facebook.com/events/1572584523084304/
image: 233013762_4435543543177615_7964697400757213632_n.jpg
isCrawled: true
---
Der Dörlemann Verlag in Zürich ist eine eingeführte Marke in Sachen Wiederentdeckungen. Neben Klassikern in schöner Neuauflage und -übersetzung findet man auch die Klassiker von Morgen, etwa Felicitas Hoppe und Jürg Halter im Programm. Sabine Dörlemann entdeckt jedes Jahr zwei, drei tolle Autoren, wie zum Beispiel Patrick Leigh Fermor. "Fabelhaft, was die alles gefunden hat, hoch interessante Sachen. Und plötzlich merkt man, ha: In dieser großen Flutwelle, die jedes Jahr über uns kommt, sind die wirklich interessanten Sachen verschwunden, und dann kommt sie – und andere auch – und holt sie aus diesem Malstrom heraus." (Michael Krüger)

Sabine Dörlemann besucht uns, stellt herausragende Neuigkeiten aus den aktuellen programmen vor und weiss überaus kurzweilig, viel über ihre Autoren*innen und Übersetzer*innen zu erzählen.

Musik zum Abend kommt vom jungen Jazzpianisten Dominik Wiedenmann, über dessen meisterliches Improvisationstalent man nicht genug staunen kann.

Moderation: Florian L. Arnold

Sollte das Wetter nicht mitspielen, sind wir in den Innenräumen der NUWOG in der Schützenstraße 32, 89231 Neu-Ulm. In den Innenräumen gelten die aktuellen AHA-Regeln, Abstandsregeln und die tagesaktuellen Bestimmungen zum Coronaschutz. 