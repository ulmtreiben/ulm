---
id: "392264845661852"
title: "Literatur unter Bäumen: Erhellungen.  Mit dem Wallstein Verlag"
start: 2021-09-02 19:30
address: Auf dem Schwal, Insel, Neu-Ulm
link: https://www.facebook.com/events/392264845661852/
image: 233556164_4435556249843011_3753554004019269190_n.jpg
isCrawled: true
---
Was dieser Verlag nicht alles macht! Exquisite Wiederauflagen, typographische Kleinorde, eine Literaturzeitschrift von höchster Qualität und Sachbücher, die man lesen muss. 

Verleger Thedel von Wallmoden und Claudia Hillebrand stellen fabelhafte Bücher des Verlags vor, unter anderem von Georges-Arthur Goldschmidt, Anna Baar und Theresa Präauer. Und sie berichten uns, warum Bücher "machen" der schönste Beruf der Welt ist.
Moderation: Florian L. Arnold

Musik zum Abend kommt vom jungen Jazzpianisten Dominik Wiedenmann, über dessen meisterliches Improvisationstalent man nicht genug staunen kann.
https://www.youtube.com/user/KING0FKEYS

Sollte das Wetter nicht mitspielen, sind wir in den Innenräumen der NUWOG in der Schützenstraße 32, 89231 Neu-Ulm. In den Innenräumen gelten die aktuellen AHA-Regeln, Abstandsregeln und die tagesaktuellen Bestimmungen zum Coronaschutz. 