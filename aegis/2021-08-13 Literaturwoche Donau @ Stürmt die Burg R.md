---
id: "835241464090387"
title: "Literaturwoche Donau @ Stürmt die Burg: Raphaela Edelbauer liest + Konzert
  mit Thom and the Wolves"
start: 2021-08-13 19:30
address: Wilhelmsburg, 89075 Ulm
link: https://www.facebook.com/events/835241464090387/
image: 215409171_3044665019139633_7626670078370734640_n.jpg
isCrawled: true
---
Lesung und Konzert:
"DAVE": Raphaela Edelbauer liest
Moderation: Rasmus Schöll. 
+ Konzert mit Thom and the Wolves
Literaturwoche Donau

Veranstaltungsort: Bühne im Innenhof der Wilhelmsburg

Hinweis: Bei schlechtem Wetter finden die Veranstaltungen der Literaturwoche Donau im Cabaret Eden (Karlstraße 71) statt. Aktuelle Infos werden unter www.literatursalon.net und auf Social Media bekanntgegeben.  

Syz‘ Leben dreht sich allein um die perfekte künstliche Intelligenz namens DAVE. Perfekt? Dave allein ist in der Lage, die Menschheit aus dem Elend zu befreien, in das sie sich selbst manövriert hat. Syz‘ Dasein aber gerät aus den Fugen: Er verliebt sich in eine junge Ärztin. Und: DAVE droht ein Totalausfall. 
“Etwas ganz Neues, etwas Eigenes“ lobte der NDR Raphaela Edelbauers zweiten Roman: „Erregend irreale Settings“. 
Raphaela Edelbauer lebt und arbeitet in Wien. 

Die „Literaturwoche Donau“ wird seit 2013 ehrenamtlich organisiert und durchgeführt. Das Literaturfestival der unabhängigen Literatur präsentiert einen Mix aus Bekanntem und Neuem, Debütant*innen und „alten Hasen“, Wieder- und Neuentdeckungen, Schönes und Schräges, Heiteres und Ernstes, kurzum: Gute Literatur mit hochkarätigen Gästen aus der unabhängigen Literaturszene. Literatur als Ereignis für die Sinne - vor allem für den wichtigsten menschlichen Sinn, den Verstand. Kommen Sie, bringen Sie Freunde*innen mit, feiern Sie mit uns das gute Buch aus den unabhängigen Verlagen. Das wünschen: Florian L. Arnold, Rasmus Schöll
www.literatursalon.net > Literaturwoche Donau 2021.


#StürmtdieBurg2021 
Vom 30. Juli bis 29. August 2021 darf die Wilhelmsburg jeweils von Mittwoch bis Sonntag gestürmt werden! Wie im vergangenen Sommer steht „Stürmt die Burg" im Zeichen von #kulturerhalten und bietet Kulturschaffenden eine langersehnte Bühne und Räumlichkeiten zur Darbietung von Musik, Literatur, Theater, Tanz, Kunst und Kinderprogramm.
Nicht nur auf der Bühne, sondern auch neben der Bühne ist in diesem Jahr viel geboten: Audiovisuelle Performances, Audiowalks, ein Theaterprojekt der adk, FabienneYoga über den Dächern Ulms und sechs Installationen in den Innenräumen der Bundesfestung bieten ein abwechslungsreiches Programm. Immer sonntags gibt es ein abwechslungsreiches Programm für die ganze Familie. Über 80 verschiedene Akteure beteiligen sich am Pop up Space auf der Wilhelmsburg, mit dabei sind das Ulmer Zelt, die popbastion.ulm, die vh ulm, die Literaturwoche Donau, das Internationale Donaufest, der Arbeitskreis Kultur, der Förderkreis Bundesfestung Ulm e.V., Bands gegen Rechts und Broken Stage.

#Gastronomie
Auch in diesem Sommer bietet die Burgbar No. XII erfrischende Drinks und leckeres Essen an allen Veranstaltungstagen. Für weitere Informationen zur Gastronomie, besucht die Burgbar-Webseite: www.burgbar12.de

#Shuttlebus
Wie gehabt bringt Euch ein Shuttlebus kostenlos vom Hans-und-Sophie-Scholl-Platz in der Stadtmitte oder von der Haltestelle Frauenstraße (Höhe Gold Ochsen Brauerei) auf die Wilhelmsburg und bis 24 Uhr wieder zurück in die Stadt.

#Corona_Hinweis
Zur Sicherheit aller haltet euch bitte an die aktuell gültigen Hygiene- und Abstandsregelungen – weitere Informationen veröffentlichen wir auf der Website. Bei Krankheitssymptomen bleibt bitte zuhause. Stay safe & stay tuned!

#WeitereInfos
www.die-wilhelmsburg.de

#Unterstützung 
Der Pop up Space wird von der Kulturabteilung der Stadt Ulm koordiniert und von der Brauerei Gold Ochsen unterstützt. Alle Infos unter: www.die-wilhelmsburg.de