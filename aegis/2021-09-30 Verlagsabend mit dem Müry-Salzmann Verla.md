---
id: "375045140966883"
title: Verlagsabend mit dem Müry-Salzmann Verlag aus Salzburg
start: 2021-09-30 19:30
locationName: Aegis Buchhandlung
address: Breite Gasse 2, 89073 Ulm
link: https://www.facebook.com/events/375045140966883/
image: 241575799_1500481373623742_5010294810275825103_n.jpg
isCrawled: true
---
2009 gegründet - ein Verlag, der Leidenschaft für Inhalte mit höchster Buchausstattung feiert.


Beginn: 19:30 Uhr
Ort: Aegis Buchhandlung