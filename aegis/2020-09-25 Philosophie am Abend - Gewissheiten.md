---
id: "914037099002717"
title: Philosophie am Abend - Gewissheiten
start: 2020-09-25 20:00
end: 2020-09-25 22:00
locationName: Ulmer Volkshochschule
address: Kornhausplatz 5, 89073 Ulm
link: https://www.facebook.com/events/914037099002717/
teaser: Ein Abend mit Dr. Martin Böhnisch Auf Gewissheiten können wir bauen. Sie
  dienen uns als Orientierung in unsicheren, aber auch in sicheren Zeiten. Wo s
isCrawled: true
---
Ein Abend mit Dr. Martin Böhnisch
Auf Gewissheiten können wir bauen. Sie dienen uns als Orientierung in unsicheren, aber auch in sicheren Zeiten. Wo soll es lang gehen? Was ist zu tun? Wie sollen wir uns verhalten? Für den, der sich einer Sache gewiss ist, ist dies kein Problem: Dort muss man drehen, hier geht es weiter, das darf man nicht übersehen, bei diesen Dingen sollte man aufpassen. Gewissheit bringt uns zum Handeln, zum Urteilen. Sie lässt uns souverän und mächtig erscheinen.

Auf der anderen Seite kennen wir den Zustand, wenn Gewissheiten ins Wanken kommen. Wir werden mit einer neuen Situation konfrontiert, mit einer entgegengesetzten Haltung, mit Tatsachen, die unseren Annahmen widersprechen oder geraten in eine persönliche Krise, weil das, von dem wir überzeugt waren, sich als Irrtum herausstellte.

 

Warum eigentlich? Von welcher Art des Wissens sind unsere Gewissheiten und auf welchem Wissen bauen sie auf? Wo beginnt Gewissheit und wo hat der Zweifel ein Ende? Diese philosophischen Fragen haben nicht nur eine existenzielle, sondern auch politische Dimension. Denn Politiker wollen und müssen Bürgern Sicherheit bieten. Im Vortrag wollen wir dem Phänomen der Gewissheit auf den Grund gehen. Eingeladen sind alle philosophisch Interessierten und die, die es noch werden wollen.

Mit Dr. Martin Böhnisch