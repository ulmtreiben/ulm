---
id: "1848772748763022"
title: "Literaturwoche Donau 2020: Sneak Preview"
start: 2020-04-07 20:00
end: 2020-04-07 21:30
locationName: Aegis Literatur Buchhandlung
address: Breite Gasse 2, 89073 Ulm
link: https://www.facebook.com/events/1848772748763022/
image: 84833384_2558055861133887_1376956709687263232_o.jpg
teaser: Die Literaturwoche in ihrer achten Auflage wird einige Neuigkeiten bringen -
  viele Überraschungen für Literatur-, Sprach- und Musik{!)-Fans verraten w
isCrawled: true
---
Die Literaturwoche in ihrer achten Auflage wird einige Neuigkeiten bringen - viele Überraschungen für Literatur-, Sprach- und Musik{!)-Fans verraten wir an diesem Abend mit Leseproben und Empfehlungen zum Festivalbesuch.