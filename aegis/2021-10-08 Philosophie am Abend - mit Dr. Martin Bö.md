---
id: "272590361119451"
title: Philosophie am Abend - mit Dr. Martin Böhnisch
start: 2021-10-08 19:00
locationName: Aegis Buchhandlung
address: Breite Gasse 2, 89073 Ulm
link: https://www.facebook.com/events/272590361119451/
image: 241216586_1500484843623395_5817367990643074506_n.jpg
isCrawled: true
---
"Autonomie des Sterbens" - Eingeladen sind alle an Philosophie Interessierte und auch die, die es noch werden wollen.

Beginn: 19:00 Uhr
Ort: Aegis Buchhandlu