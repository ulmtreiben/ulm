---
id: "576995077020595"
title: "Literaturwoche Donau: Peng Peng Parker! Nora Gomringer & Band"
start: 2021-08-17 19:30
end: 2021-08-17 21:00
address: Roxy Soundgarten, Schillerstraße 1/12, Ulm
link: https://www.facebook.com/events/576995077020595/
image: 235949102_3071965306409604_9094517602285136998_n.jpg
isCrawled: true
---
Gomringer & Scholz, dahinter stecken die Lyrikerin und Rezitatorin Nora Gomringer und der Jazz-Musiker Philipp Scholz sowie Pianist Philip Frischkorn. 
Mit „Peng Peng Parker“ widmet sich das Trio der 1893 geborenen und 1967 in New York gestorbenen Dichterin, Dramatikerin und Werbetexterin Dorothy Parker. 

Parker schrieb über die Liebe und das Leben, seufzend und lachend, immer trinkfest. Ihr lyrisches Werk ist witzig und lakonisch, mal zart, mal hart. Niemand hat so verlachend über das Rangeln zwischen den Geschlechtern geschrieben. Ein New York der 1920er Jahre – ohne sie undenkbar. 

Aus der Fülle von Parkers Spott-, Humor- und Liebesgedichten haben Nora Gomringer und Philipp Scholz, begleitet vom Pianisten Jonas Timm, erstaunliche Songs geschaffen. Melodiös und rhythmisch, witzig und eigen weicht das Duo jenes Schwarzweiß alter Fotos und klingender Pianotasten auf zu Melodien, die schon lange zwischen den Zeilen stehen. 
Kritikerinnen und Kritiker sind sich einig: Hier wird die bewährte Kombination von Jazz und Rezitation auf eine neue Ebene gehoben: „Hier wird die Rezitation zu einem bunten szenischen Spiel“ so „Literaturkritik.de“

Bei gutem Wetter sind wir im schönen Roxy-Biergarten. Der Sound Garten schließt direkt an das Roxy an. Einfach rechts an den Hallen vorbei, lauft ihr direkt auf unseren Biergarten zu.
Bitte beachte, dass es im Sound Garten keine Reservierungen gibt. 

Aktuelle Informationen - zum Beispiel zur Verlegung bei schlechtem Wetter - findet ihr auf unserer Homepage und den Social Media Kanälen.

Bei schlechtem Wetter weichen wir aus ins Innere des Roxy. Dort gibt es nur eine begrenzte Anzahl von Sitzplätzen!

https://www.roxy.ulm.de/programm/programm.php?m=8&j=2021&vid=3804