---
id: "1379497245577905"
title: Kulturnacht - Thom And The Wolves
start: 2020-09-19 19:00
end: 2020-09-20 01:00
locationName: Aegis Buchhandlung
address: Breite Gasse 2, 89073 Ulm
link: https://www.facebook.com/events/1379497245577905/
teaser: Thom And The Wolves (Berlin) live and solo in concert  Schon aus einigen
  hundert Metern Entfernung ist sie zu hören - eine Stimme, welche in ihrer Int
isCrawled: true
---
Thom And The Wolves (Berlin) live and solo in concert

Schon aus einigen hundert Metern Entfernung ist sie zu hören - eine Stimme, welche in ihrer Intimität zuweilen an Jeff Buckley oder Damien Rice erinnert und doch irgendwie völlig einzigartig daherkommt, begleitet von einer mit warmen Klängen gespielten Gitarre.
Ob in Berlin, Hamburg oder Dresden.
 

Besonders seine Zeit in Los Angeles inspirierte Thom zum Schreiben seiner ersten eigenen EP, von welcher der Song “Come On Over” u.a. in Tom Robinson’s BBC Introducing Radio Show gefeatured worden war.
 
Zurück in Deutschland zog es Thom nach Berlin. Dort lernte den Musiker und Produzenten Almost Charlie alias Dirk Homuth kennen, mit dessen Unterstützung schließlich sein erstes Solo Album, „The Gold In Everything“ entstand. Der Song „The Art Of Being Alone“.

Für das am 29. Mai erschienende Album, „Thom & The Wolves“ gelang es dem 27-jährigen Musiker und seiner Band, Matteo Pavesi 
(Produzent von Alice Phoebe Lou) als Arrangeur zu gewinnen. 
Über zwei Jahre entstand in Studios in Berlin und Hamburg eine Indie-Rock Platte, welche in ihrer stilistischen Ausrichtung an Acts wie Radiohead, Jeff Buckley oder Big Thief erinnert.



