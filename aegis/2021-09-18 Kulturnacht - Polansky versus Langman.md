---
id: "214623200701626"
title: Kulturnacht - Polansky versus Langman
start: 2021-09-18 19:00
end: 2021-09-18 22:00
locationName: Aegis Buchhandlung
address: Breite Gasse 2, 89073 Ulm
link: https://www.facebook.com/events/214623200701626/
image: 241420002_1500466860291860_8784743640942874499_n.jpg
isCrawled: true
---
Live und in Farbe aus Prag - Andrej Polansky an der Bratsche und Emül Langman am Schlagzeug. Das Duo spielt einen zärtlich zerstörten Sound, der einen durch die Nacht treiben lässt. Stündlich, je 35 Minuten.

Andrej Polansky on viola and Emül Langman on drums have a true rock band sound. The duo plays tenderly destroyed and decomposed covers as well as their own intense and mood filled music. Every full hour. 