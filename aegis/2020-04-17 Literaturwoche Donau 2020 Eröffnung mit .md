---
id: "148450386123924"
title: "Literaturwoche Donau 2020: Eröffnung mit Raoul Schrott"
start: 2020-04-17 19:30
end: 2020-04-17 22:30
address: Museumsgesellschaft Ulm
link: https://www.facebook.com/events/148450386123924/
image: 88185922_2798402633538836_7644658881955627008_o.jpg
teaser: Raoul Schrott kommt - wir freuen uns auf einen fabelhaften Autor und Erzähler
  und auf ein Buch, das die vielleicht un-wahrscheinlichste und zugleich w
isCrawled: true
---
Raoul Schrott kommt - wir freuen uns auf einen fabelhaften Autor und Erzähler und auf ein Buch, das die vielleicht un-wahrscheinlichste und zugleich wahre Geschichte eines Seefahrerlebens nacherzählt: "Eine Geschichte des Windes oder Von dem deutschen Kanonier der erstmals die Welt umrundete und dann ein zweites und ein drittes Mal".
Was für ein Abenteuer! Der Hannes aus Aachen kam als erster einmal ganz um die Welt. Vor 500 Jahren brach er mit Magellans Flotte zu den Gewürzinseln auf. Und damit ins völlig Ungewisse. Schwelgerisch und voll geradezu fühlbarer Details schenkt Raoul Schrott uns Lesern  ein ganzes Leben auf hoher See.