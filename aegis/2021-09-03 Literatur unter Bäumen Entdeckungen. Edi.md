---
id: "1268689913548504"
title: "Literatur unter Bäumen: Entdeckungen. Edition Fünf aus Gräfelfing"
start: 2021-09-03 19:30
address: Auf dem Schwal, Insel, Neu-Ulm
link: https://www.facebook.com/events/1268689913548504/
image: 233124954_4435570413174928_6047209224211356710_n.jpg
isCrawled: true
---
Eine bibliopphile Bibliothek des weiblichen Erzählens: Seit 2010 erscheinen in der Münchner "edition fünf" literarische Meilensteine von Autorinnen aus aller Welt: Romane, Erzählungen und (auto-)biografische Texte, vergessene Schätze und lohnende Entdeckungen, Deutschsprachiges und Übersetztes, Klassikerinnen und Debütantinnen. Silke Wenigers Verlag verleiht den Texten neuen Glanz, auch durch die Gestaltung der Bücher. Hier gibt es großartige Literatur zu entdecken und Verlegerin Silke Weniger verführt uns an diesem Abend, unbekannte Schätze zu heben!

Moderation: Florian L. Arnold
Lesung: Anna-Elisabeth Brüderl

Sollte das Wetter nicht mitspielen, sind wir in den Innenräumen der NUWOG in der Schützenstraße 32, 89231 Neu-Ulm. In den Innenräumen gelten die aktuellen AHA-Regeln, Abstandsregeln und die tagesaktuellen Bestimmungen zum Coronaschutz. 