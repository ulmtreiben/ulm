---
id: "374498837593996"
title: Double-Feature MÜRY-SALZMANN & Christina Maria Landerl "ALLES VON MIR"
start: 2021-09-30 19:30
locationName: Aegis Buchhandlung
address: Breite Gasse 2, 89073 Ulm
link: https://www.facebook.com/events/374498837593996/
image: 243007415_4392139014165182_1611412723656643792_n.jpg
isCrawled: true
---
Müry-Salzmann ist ein Salzburger Verlag, der sich alles leistet, was man für gute Bücher benötigt: Spannende Autor*innen, gutes Lektorat, aufregendes Design und die Lust an der Entdeckung neuer Literatur, die man gelesen haben sollte. Gegründet wurde der Verlag 2009 von der Verlegerin Mona Müry und dem Investor Christian Dreyer auf der Basis einer 20-jährigen Erfahrung in einem der ältesten Verlage Österreichs, dem auch eine große Druckerei angehörte. So verbindet sich von Anfang an neben der Leidenschaft für Inhalte auch jene für die Buchherstellung.

In den Reigen der Autor*innen reihen sich bereits etablierte wie Walter Kappacher, Jürgen Flimm, Olga Neuwirth, Diane Middlebrook, Wilhelm Holzbauer, Friedrich Kurrent, Adolph Stiller, Anton Thuswaldner oder Michael Frayn, ausserdem jede Menge neue Talente, zum Beispiel die Texte von Elke Laznia und Lydia Haider.

Das neueste Talent im Reigen der Müry-Salzmann-Autor*innen ist Christina Maria Landerl. Sie wird uns ihren Roman "Alles von mir" vorstellen. "Alles von mir" begleitet eine Frau auf einer Reise durch den Süden der USA. Auf dem Papier nimmt die Erzählung nur 128 Seiten ein, doch ist ihr Text ein Gesamtkunstwerk aus Literatur, Film und Musik, in dessen Tiefen Freiheitsstreben, Rassismus, Frauen- und Bürgerrechte mitschwingen.

Mit „Alles von mir“ hat Christina Maria Landerl einen Roman wie einen alten Bluessong aus den 30er Jahren geschrieben: voller Musik und voller Melancholie. Repeat please.  
Ludwig Lohmann (Buchhandlung ocelot, Berlin)