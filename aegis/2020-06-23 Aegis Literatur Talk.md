---
id: "563382567900792"
title: Aegis Literatur Talk
start: 2020-06-23 19:00
locationName: Liederkranz Kulturbiergarten
address: Friedrichsau 9, 89073 Ulm
link: https://www.facebook.com/events/563382567900792/
image: 104492962_1164386107233272_1983591021746238010_o.jpg
teaser: Das Wohnzimmer der Literaten ist eine Mischung aus Lesung, Live-Musik und
  Talk. Zu Gast haben wir den Lyriker "Rok" der aus seinem neuesten Werk "fünf
isCrawled: true
---
Das Wohnzimmer der Literaten ist eine Mischung aus Lesung, Live-Musik und Talk. Zu Gast haben wir den Lyriker "Rok" der aus seinem neuesten Werk "fünf sterne für eine durchzechte nacht" liest. Das ist ein intimes Abtasten der eigenen Einsamkeit, ein angenehm leiser Klagegesang der Welt, voller Poesie und Schönheit. Und den Musiker Michael Moravek. Er gehört sicherlich zu den außergewöhnlichsten Singer-Songwritern Deutschlands.
Seine Gitarre umscheichelt das Ohr des Zuhörer, um sich in dessen Herz einzugraben und es nicht mehr loszulassen.
Songs, die die Zeit immer etwas langsamer vergehen lassen.
Moderation: Rasmus Schöll und Florian L. Arnold (Aegis Buchhandlung)