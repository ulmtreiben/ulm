---
id: "772278016585687"
title: Die Theaterei Herrlingen zu Gast
start: 2020-02-13 19:30
end: 2020-02-13 22:00
locationName: Aegis Literatur Buchhandlung
address: Breite Gasse 2, 89073 Ulm
link: https://www.facebook.com/events/772278016585687/
image: 82008150_1038303719841512_7113168984156930048_n.jpg
teaser: "Ursula Berlinghof und Frank Ehrhardt von der Theaterei Herrlingen
  präsentieren Ausschnitte aus Theateraufführungen nach Romanvorlagen: DER HALS
  DER GI"
isCrawled: true
---
Ursula Berlinghof und Frank Ehrhardt von der Theaterei Herrlingen präsentieren Ausschnitte aus Theateraufführungen nach Romanvorlagen: DER HALS DER GIRAFFE von Judith Schalansky, IN ZEITEN DES ABNEHMENDEN LICHTS von Eugen Ruge und ALTES LAND von Dörte Hansen. Theaterautorin und Theaterei-Leiterin Edith Ehrhardt erzählt, wie Theaterfassungen entstehen.