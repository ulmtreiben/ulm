---
id: "863210590771923"
title: "Literaturwoche Donau 2020: „Die Geschichte des Körpers“"
start: 2020-04-21 19:30
end: 2020-04-21 22:00
locationName: Casino
address: Weinhof 7, 89073 Ulm
link: https://www.facebook.com/events/863210590771923/
image: 87451704_1081861188819098_5099604890347372544_o.jpg
teaser: „Kein Buch von Thomas Stangl, in dem man nicht mit Glück und Staunen erfährt,
  wie unverwechselbar literarische Sprache unserer Zeit ist.“ (Wortmeldung
isCrawled: true
---
„Kein Buch von Thomas Stangl, in dem man nicht mit Glück und Staunen erfährt, wie unverwechselbar literarische Sprache unserer Zeit ist.“ (Wortmeldungen Literaturpreis 2019).
In seinem ersten Erzählband verhilft Thomas Stangl der ‚kleinen Form‘ der Erzählung zu großer Dimension: Mit wohldosierter, feinsinniger Ironie entwickelt der Wiener Autor Studien des Unstimmigen und Irritierenden. Mit Leichtigkeit und Präzision im Ton bildet Stangl die Komplexität der Welt ab. An diesem Abend wird er begleitet von Jürgen Grözinger, der den polyphonen Texten seine eigene, im Moment entstehende Klangperspektive schenkt. 

Di., 21. 4. 2020, 19:30 Uhr, „Die Geschichte des Körpers“.
Thomas Stangl liest aus seinem neuen Buch. Mit Live-Percussion-Interludes von Juergen Groezinger. Ab 22 Uhr: DJ-Set von Jürgen Grözinger a.ka. Simon Sarow 
Eine Kooperation von KlangHaus und Literaturwoche Donau.