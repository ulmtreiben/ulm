---
id: "1420191851712861"
title: "Literaturwoche Donau: Raphaela Edelbauer: Dave"
start: 2021-08-13 19:30
end: 2021-08-13 22:30
address: Wilhelmsburg, 89075 Ulm
link: https://www.facebook.com/events/1420191851712861/
image: 213031413_3041160372823431_8965646688656374407_n.jpg
isCrawled: true
---
Diese Autorin muss man lesen - und auch live erleben! Mit ihrem neuen Roman kommt sie nach Ulm und kredenzt uns eine exquisite Dystopie: Syz‘ Leben dreht sich allein um die perfekte künstliche Intelligenz namens DAVE. Perfekt? Nun ja. Dave allein ist in der Lage, die Menschheit aus dem Elend zu befreien, in das sie sich selbst manövriert hat. Aber will es das überhaupt?
Syz‘ Dasein aber gerät aus den Fugen: Er verliebt sich in eine junge Ärztin. Und: DAVE droht ein Totalausfall. 

“Etwas ganz Neues, etwas Eigenes“ lobte der NDR Raphaela Edelbauers zweiten Roman: „Erregend irreale Settings“. 
Raphaela Edelbauer lebt und arbeitet in Wien. 
Moderation: Rasmus Schöll, @[570143636657525:274:Aegis Buchhandlung]

ca 21:30 Uhr: Konzert @[100063709420012:2048:Thom And The Wolves]
Indiepop der besten Art: Eine markante Stimme, die zuweilen an Jeff Buckley oder Damien Rice erinnert , dazu intelligente Arrangements und Texte. Hier heißt es: Aufgehen in den persönlichen Geschichten, Tagebucheinträgen einer flüchtigen und unbeständigen Zeit. 