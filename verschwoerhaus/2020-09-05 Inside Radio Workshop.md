---
id: C4F486C0-A71D-462B-8F19-FC6C0F97E3F2
title: +++fällt aus +++ Radio Free FM Exkursion
start: 2020-09-05 13:00
end: 2020-09-05 17:00
address: Radio free FM Platzgasse 18, 89073 Ulm, Deutschland
teaser: Coronabedingt fällt diese Exkursion aus. Wird aber in 2021 nachgeholt. Weiter
  gehts im Lab ab dem 12
isCrawled: true
---
Coronabedingt fällt diese Exkursion aus. Wird aber in 2021 nachgeholt. Weiter gehts im Lab ab dem 12. September, um 15 Uhr. 