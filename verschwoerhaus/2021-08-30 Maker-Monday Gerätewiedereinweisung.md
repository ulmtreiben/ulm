---
id: 4btfpncsri0d6q1qvjqtmrc14b
title: Maker-Monday Gerätewiedereinweisung
start: 2021-08-30 18:00
end: 2021-08-30 21:00
address: Verschwörhaus, Weinhof 9, 89073 Ulm, Deutschland
teaser: Wir wollen an diesem Tag vornehmlich Wiedereinweisungen auf die Geräte geben.
  Sollte genügend Zeit s
isCrawled: true
---
Wir wollen an diesem Tag vornehmlich Wiedereinweisungen auf die Geräte geben. Sollte genügend Zeit sein, gibt es heute aber auch die ersten neuen Einweisungen vor allem für den Lasercutter. Bitte meldet euch vorher an (s.U.). Wer keinen Impf- oder Genesenennachweis vorweisen kann, bringt bitte einen tagesaktuellen Schnelltestnachweis mit.



Für die Anmeldung gerne eine Mail an makermonday@verschwoerhaus.de - oder für Leute, die bereits im Slack sind, in #makermonday bescheidgeben.



🖥 Von 3D-Druck bis Lasercut

📍 Werkstätten

🙂 Stefan Kaufmann