---
id: 1ug3eo7ikt1ooqcdn0pursp4ug
title: +++ fällt aus +++ Offenes Jugend hackt Lab
start: 2020-04-07 17:00
end: 2020-04-07 19:30
address: Verschwörhaus, Weinhof 9, 89073 Ulm, Deutschland
teaser: Das offene Lab am 7. April fällt leider aus. Wir informieren rechtzeitig, wann
  es wieder weiter geht
isCrawled: true
---
Das offene Lab am 7. April fällt leider aus. Wir informieren rechtzeitig, wann es wieder weiter geht. 





 🖥 Eigene Ideen verwirklichen, Inspiration suchen und mit den Mentor*innen experimentieren 

📍 Ganzes Haus 

🏢 Jugend Hackt Lab Ulm 

🏢 Jugend Hackt Mentor*innen 

🏢 Tom Novy