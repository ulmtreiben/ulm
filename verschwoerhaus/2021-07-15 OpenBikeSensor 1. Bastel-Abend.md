---
id: 04l7pn3e5h5chba812nd9m2ttf
title: "OpenBikeSensor: 1. Bastel-Abend"
start: 2021-07-15 18:00
end: 2021-07-15 21:30
address: Verschwörhaus, Weinhof 9, 89073 Ulm, Deutschland
teaser: "Das erste Treffen für die Konstruktion des OpenBikeSensors. Wir werden uns
  noch öfters treffen, ein "
isCrawled: true
---
Das erste Treffen für die Konstruktion des OpenBikeSensors. Wir werden uns noch öfters treffen, ein regelmäßiger Termin wird noch gefunden.<br><br>Wie oft passiert es uns, dass uns ein Auto beinahe vom Sattel holt? Was wir alle kennen, überprüfen wir nun wissenschaftlich. Dazu&nbsp; wurde ein Sensor entwickelt, der den Abstand misst und Überholmanöver Geodaten zuordnet. Wo ist es sicher? Wo nicht? Welche Uhrzeit? Welche Strecken?<br><br>Wir werden in der ersten Welle 10 Geräte im VSH Ulm bauen und gemeinsam mit dem ADFC einsetzen.<br><br>Ich bitte um eine Anmeldung/Ankündigung, bevor man vorbeikommt. Dazu eine kurze Email an&nbsp;<a href="mailto:norbertschulz@gmx.net">norbertschulz@gmx.net</a>&nbsp;<br><br>Mehr Informationen auf&nbsp;<a href="https://www.openbikesensor.org/">https://www.openbikesensor.org/</a><br><br>🖥Zusammenbau der ersten 10 OpenBikeSensor-Kits &amp; Austausch<i></i><br>📍Haus/Draußen<br>🏢ADFC<br>🙂Norbert