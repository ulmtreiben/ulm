---
id: ohtn003jet66qjunq5gglcb1fo_R20200727T180000
title: ++ eingeschränkt ++ Maker-Monday
start: 2020-07-27 20:00
end: 2020-07-27 22:00
address: Verschwörhaus, Weinhof 9, 89073 Ulm, Deutschland
teaser: Aufgrund der Covid-19-Auflagen ist der Betrieb nur eingeschraenkt moeglich und
  vornehmlich fuer Mens
isCrawled: true
---
Aufgrund der Covid-19-Auflagen ist der Betrieb nur eingeschraenkt moeglich und vornehmlich fuer Menschen, die bereits Geraeteeinweisungen haben. Neueinweisungen sind ggf. nicht moeglich.





🖥 Von 3D-Druck bis zur Holzwerkstatt

📍 Werkstätten

🙂 Sabine Wieluch

🙂 Henning Schütz