---
id: 0vdrts4ktvnfnbiammgvtqsmjo
title: Upcycling Nähcafé
start: 2020-01-27 19:00
end: 2020-01-27 22:00
address: Verschwörhaus, Weinhof 9, 89073 Ulm, Deutschland
teaser: Aus alt mach neu - Die Hochschulgruppe für Nachhaltigkeit will mit euch alte
  Stoffe „upcyceln“.Wir
isCrawled: true
---
Aus alt mach neu - Die Hochschulgruppe für Nachhaltigkeit will mit euch alte Stoffe „upcyceln“.



Wir stellen euch Maschinen, Material, Nähvorlagen und Unterstützung - nähen tut ihr selbst.

Auch eigene Projekte können mitgebracht und bearbeitet werden, wenn euch zu Hause dafür Zeit, Kenntnis und/oder Nähmaschine fehlt.



Falls ihr alte/ungetragene Klamotten rumliegen habt ist das natürlich die perfekte Gelegenheit ihnen neues Leben zu schenken.



Für die gemütliche Cafe-Atmosphäre sorgen Sofas und Sitzsäcke, Getränke und Kuchen.



Wir freuen uns auf einen kreativen Abend im Verschwörhaus!

Eure HSGN Ulm

 https://www.uni-ulm.de/misc/hg-nachhaltigkeit/home



🖥 Aus alt mach neu

🏢 Hochschulgruppe für Nachhaltigkeit