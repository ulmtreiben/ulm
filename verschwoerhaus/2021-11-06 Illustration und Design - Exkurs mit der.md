---
id: 06evcoorkhljg96f4oc3ssn713
title: Illustration und Design - Exkurs mit der Artistin Adela Knajzl
start: 2021-11-06 13:00
end: 2021-11-06 14:30
teaser: <p>In unserem Exkurs in die Welt der Illustration mit Adela Knajzl, einer
  Ulmer Illustratorin und De
isCrawled: true
---
<p>In unserem Exkurs in die Welt der Illustration mit Adela Knajzl, einer Ulmer Illustratorin und Designerin – taucht ihr ein in die verschiedenen Themenbereiche der Illustration, entdeckt zeitgenössische Illustratoren und diverse Techniken. Anhand eigener Arbeiten zeigt Adela wie Illustrationen für Auftraggeber entstehen. Ihr erfahrt welche Bildungswege es gibt um ein/e Illustrator*in zu werden.&nbsp;</p><p>Der Talk ist sowohl für Anfänger, Interessierte als auch Fortgeschrittene geeignet – es sind keinerlei Vorkenntnisse notwendig. Ihr habt auch die Möglichkeit eure Mappen, Skizzen und Zeichnungen mitzubringen, nach dem Vortrag gibt euch Adela gerne Tipps dazu.</p><p>Weitere Infos zu Adela Knajzl: https://graphein.org&nbsp;<br>6. November 2021 um 13 Uhr im Verschwörhaus - Hybrid: vor Ort (bitte aktuelle Corona-Regeln beachten) und als Stream<br><br>🖥 Wie entstehen Illustrationen? Adela zeigt Themenbereiche, zeitgenössische Künstler*innen und Techniken<br>📍 Salon &amp; Stream<br>🏢 Jugend hackt Lab<br>🙂 Tom Novy<br>🙂 Adela Knajzl<br></p>