---
id: 9hit4hqlkrc9mk1dj0qjlvsg36_R20211206T170000
title: Maker-Monday FabLab (bitte anmelden)
start: 2021-12-06 18:00
end: 2021-12-06 21:00
address: Verschwörhaus, Weinhof 9, 89073 Ulm, Deutschland
teaser: Heute ist nur das FabLab (im Erdgeschoss) geöffnet. Bitte
  beachten:<br>&nbsp;<br>* Geimpften- oder G
isCrawled: true
---
Heute ist nur das FabLab (im Erdgeschoss) geöffnet. Bitte beachten:<br>&nbsp;<br>* Geimpften- oder Genesenennachweis mitbringen<br>* Für das Fablab (Lasercutter, 3D-Druck) möglichst vorab per Mail an <a href="mailto:makermonday@verschwoerhaus.de">makermonday@verschwoerhaus.de</a> anmelden<br>* Die offene Holzwerkstatt (im UG) können wir leider gerade nicht anbieten<br><br>🖥 Von 3D-Druck bis Lasercut<br>📍 Werkstätten<br>🙂 David, Felix