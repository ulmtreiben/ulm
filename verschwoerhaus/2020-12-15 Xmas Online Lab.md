---
id: 3775h9j5lic015vbqku521cfkr
title: Xmas Online Lab
start: 2020-12-15 17:00
end: 2020-12-15 18:00
teaser: <p>Am Dienstag (15.12 um 17 Uhr) lassen wir das Jahr im Jugend hackt Lab
  ausklingen mit einer kleine
isCrawled: true
---
<p>Am Dienstag (15.12 um 17 Uhr) lassen wir das Jahr im Jugend hackt Lab ausklingen mit einer kleinen Online-Xmas-Party in Mozilla Hubs. Habt ihr Ideen oder Wünsche für das Lab in 2020? Was hat euch dieses Jahr am meisten gefallen? Daneben kann natürlich gecodet und getüftelt werden.</p><p>https://gather.town/app/VjvVYjXauBS3vEdn/JH-Ulm</p><p><br></p><p>🖥Xmas Lab<br>📍Online<br>🏢Jugend hackt Lab Ulm<br>🙂Alle<br></p>