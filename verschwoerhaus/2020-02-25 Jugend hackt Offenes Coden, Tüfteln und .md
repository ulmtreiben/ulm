---
id: 7s1bft6kjpkpkvpa6r1c17q4oh
title: "Jugend hackt: Offenes Coden, Tüfteln und Weltverbessern"
start: 2020-02-25 17:00
end: 2020-02-25 19:30
teaser: 🖥 Eigene Ideen verwirklichen, Inspiration suchen und mit den Mentor*innen
  experimentieren 📍 Ganze
isCrawled: true
---
🖥 Eigene Ideen verwirklichen, Inspiration suchen und mit den Mentor*innen experimentieren 

📍 Ganzes Haus 

🏢 Jugend Hackt Ulm 

🏢 Jugend Hackt Mentor*innen