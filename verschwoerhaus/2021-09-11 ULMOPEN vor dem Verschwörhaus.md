---
id: 2q2jlur0grrblmkr8u6ndf7tgv
title: ULMOPEN vor dem Verschwörhaus
start: 2021-09-11 14:00
end: 2021-09-11 16:30
address: Verschwörhaus, Weinhof 9, 89073 Ulm, Deutschland
teaser: <p>Das Verschwörhaus lädt am&nbsp;<strong>11. September 2021 ab 14
  Uhr</strong>&nbsp;im Rahmen von U
isCrawled: true
---
<p>Das Verschwörhaus lädt am&nbsp;<strong>11. September 2021 ab 14 Uhr</strong>&nbsp;im Rahmen von ULMOPEN zum Tag der Begegnung ein. Dazu gibt es Sitzmöglichkeiten vor dem Schaufenster (aka Aquarium) durch die Hochbeetbank und zusätzliche Stizgelegenheiten. ULMOPEN ermöglicht so mit anderen Menschen in Kontakt zu kommen und die eigene Stadt neu kennenzulernen. Wir wollen aber auch dadurch untereinander zur Diskussion anregen, nämlich wie wir uns die Gesellschaft vorstellen, in der wir in Zukunft leben wollen. Mit diesem Schwerpunkt beschäftigen wir uns bereits am&nbsp;<strong>10. September um 18 Uhr</strong>&nbsp;live vor Ort und online (Link wird bekanntgegeben) in einem&nbsp;<a href="https://verschwoerhaus.de/five-needs-wohnen-in-der-zukunft-lab-talk-im-september/">Talk</a>.</p><p>Wir freuen uns über zahlreiche Besucher und informieren natürlich auch gerne über das Verschwörhaus selbst :)</p><p><br></p><p>https://ulmopen.de/</p>