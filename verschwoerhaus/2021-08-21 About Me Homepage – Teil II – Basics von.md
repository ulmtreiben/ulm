---
id: 0u7t38v6iou05r6cgfoepsjn8k
title: About Me Homepage – Teil II – Basics von JavaScript
start: 2021-08-21 11:00
end: 2021-08-21 13:30
teaser: <p>Ihr wolltet schon immer eine eigene Homepage haben – und die Basics dazu
  kennen? Dann seid ihr in
isCrawled: true
---
<p>Ihr wolltet schon immer eine eigene Homepage haben – und die Basics dazu kennen? Dann seid ihr in unserem Workshop genau richtig! Nach dem Motto: von Lab-Teilnehmenden für Lab-Teilnehmende, zeigen euch&nbsp;<em>Christopher</em>&nbsp;und&nbsp;<em>LarsvomMars</em>, was es alles für die eigene Website braucht 🙂 Diesmal beschäftigen wir uns vor allem JavaScript. Außerdem habt ihr jede Menge Zeit zum Experimentieren an der eigenen Homepage.</p><p>Keine Vorkenntnisse und keine Anmeldung nötig.</p><p>&nbsp;</p><p><strong>Sa. 21. August, 11.00 – 13.30 Uhr online:</strong>&nbsp;Link geben wir hier bekannt</p>