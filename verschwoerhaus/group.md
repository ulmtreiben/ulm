---
name: Verschwörhaus
website: https://verschwoerhaus.de
email: kontakt18@verschwoerhaus.de
scrape:
  source: ical
  options:
    url: https://calendar.google.com/calendar/ical/slaun4l80uh2s0ototiol4qkgo%40group.calendar.google.com/public/basic.ics
---
Verschwörhaus – das „Experimentierfeld für die Welt von morgen“, mit 3D-Druckern, offenen Werkstätten, Vortragsräumen und vor allem jeder Menge Inhalte, um unsere Stadt Ulm gemeinsam in die Zukunft begleiten zu können!
