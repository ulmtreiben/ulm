---
id: 6gg7jfosfm8c8t299omik33la6
title: "Online-Workshop: Was ist Linux? Ein kurzer Überblick"
start: 2021-04-03 11:00
end: 2021-04-03 13:30
teaser: <p>Als Linux bezeichnet man ein freies und kostenloses Betriebssystem, das
  durch den Support und die
isCrawled: true
---
<p>Als Linux bezeichnet man ein freies und kostenloses Betriebssystem, das durch den Support und die Verbesserungen vieler Ehrenamtlicher lebt. Ubuntu ist neben vielen anderen die bekannteste Linux-Distribution. Wer Open Source gut findet und sein Betriebssystem frei, offen und balastfrei gestalten mag – wird Linux lieben. Wie aber funktioniert Linux und wie bekommt man es auf den eigenen Rechner?</p><p>Mit diesen und vielen anderen Fragen wollen wir uns in einem Online-Workshop beschäftigen und euch einen kurzen Linux-Überblick geben.</p><br><br>🖥 Workshop zu Linux für Einsteiger*innen und Pros<br>📍 Lab-Workshop Online<br>🙂 DermitdemZopf<br>🙂 Tom Novy&nbsp;&nbsp;