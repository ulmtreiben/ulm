---
id: 2l3kidk1h6hlmil9lj88lf1p43
title: "OpenBikeSensor: 8. Bastel-Abend [Anmeldung erforderlich]"
start: 2021-10-05 18:00
end: 2021-10-05 20:00
address: Verschwörhaus, Weinhof 9, 89073 Ulm, Deutschland
teaser: Dies ist das achte gemeinsame Treffen für die Konstruktion des
  OpenBikeSensors. Wir bauen die restli
isCrawled: true
---
Dies ist das achte gemeinsame Treffen für die Konstruktion des OpenBikeSensors. Wir bauen die restlichen paar Bausätze noch fertig und können hoffentlich die 3 defekten noch reparieren. Dann noch der Funktionstest und Fehleranalyse. Inzwischen ist auch das Datenportal fertig und wir können die OBS im Livebetrieb testen und die Daten auswerten<br><br>Ich bitte um eine Anmeldung/Ankündigung, bevor man vorbeikommt. Dazu eine kurze Email an&nbsp;<a href="mailto:norbertschulz@gmx.net">norbertschulz@gmx.net</a><br><br>Wie oft passiert es uns, dass uns ein Auto beinahe vom Sattel holt? Was wir alle kennen, überprüfen wir nun wissenschaftlich. Dazu&nbsp;wurde ein Sensor entwickelt, der den Abstand misst und Überholmanöver Geodaten zuordnet. Wo ist es sicher? Wo nicht? Welche Uhrzeit? Welche Strecken?<br><br>Mehr Informationen dazu auf&nbsp;<a href="https://www.openbikesensor.org/">https://www.openbikesensor.org</a><br><br>🖥Zusammenbau der ersten 10 OpenBikeSensor-Kits &amp; Austausch<i></i><br>📍Haus/Draußen<br>🏢ADFC<br>🙂Norbert