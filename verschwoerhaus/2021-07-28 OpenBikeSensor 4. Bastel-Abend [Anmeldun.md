---
id: 1fs6fvnbc5o8sh72mk2ott0a8j
title: "OpenBikeSensor: 4. Bastel-Abend [Anmeldung erforderlich]"
start: 2021-07-28 18:00
end: 2021-07-28 21:00
address: Verschwörhaus, Weinhof 9, 89073 Ulm, Deutschland
teaser: Dies ist das vierte gemeinsame Treffen für die Konstruktion des
  OpenBikeSensors. Wir bauen weiter an
isCrawled: true
---
Dies ist das vierte gemeinsame Treffen für die Konstruktion des OpenBikeSensors. Wir bauen weiter an den angefangenen Sensoren, einige sind schon fertig gebaut. Nun läuft der Funktionstest und Fehleranalyse. Auch haben wir immer noch 4 neue Bausätze, die angefangen werden müßten.<br><br>Ich bitte um eine Anmeldung/Ankündigung, bevor man vorbeikommt. Dazu eine kurze Email an&nbsp;<a href="mailto:norbertschulz@gmx.net">norbertschulz@gmx.net</a><br><br>Wie oft passiert es uns, dass uns ein Auto beinahe vom Sattel holt? Was wir alle kennen, überprüfen wir nun wissenschaftlich. Dazu&nbsp;wurde ein Sensor entwickelt, der den Abstand misst und Überholmanöver Geodaten zuordnet. Wo ist es sicher? Wo nicht? Welche Uhrzeit? Welche Strecken?<br><br>Mehr Informationen dazu auf&nbsp;<a href="https://www.openbikesensor.org/">https://www.openbikesensor.org</a><br><br>🖥Zusammenbau der ersten 10 OpenBikeSensor-Kits &amp; Austausch<i></i><br>📍Haus/Draußen<br>🏢ADFC<br>🙂Norbert