---
id: 9hit4hqlkrc9mk1dj0qjlvsg36
title: Maker-Monday FabLab & Holz (bitte anmelden)
start: 2021-11-08 18:00
end: 2021-11-08 21:00
address: Verschwörhaus, Weinhof 9, 89073 Ulm, Deutschland
teaser: "Heute ist sowohl die Holzwerkstatt als auch das FabLab geöffnet. Bitte
  beachten: * Geimpften- oder"
isCrawled: true
---
Heute ist sowohl die Holzwerkstatt als auch das FabLab geöffnet. Bitte beachten:

 

* Geimpften- oder Genesenennachweis mitbringen

* Für die Holzwerkstatt zwingend vorher per Mail an makermonday@verschwoerhaus.de anmelden (begrenzte Plätze) 

* Für das Fablab (Lasercutter, 3D-Druck) möglichst vorab per Mail an makermonday@verschwoerhaus.de anmelden



🖥 Von 3D-Druck bis Lasercut

📍 Werkstätten

🙂 Stefan Kaufmann