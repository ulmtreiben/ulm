---
id: 7es9g8vdrkfn9ijj79c49vpohu
title: Create your own fashion - Mode- und Nähworkshop
start: 2021-12-04 13:00
end: 2021-12-04 17:00
address: Verschwörhaus, Weinhof 9, 89073 Ulm, Deutschland
teaser: Genug fast fashion gekauft, werde zu deiner/m eigenen Modeschöpfer*in. Egal ob
  Rucksack, Laptoptasch
isCrawled: true
---
Genug fast fashion gekauft, werde zu deiner/m eigenen Modeschöpfer*in. Egal ob Rucksack, Laptoptasche, Jutebeutel oder aber ein cooles Stoffarmband - alles ist möglich. Und das sogar aus qualitativem und nachhaltigem Stoff und Nähfaden. Wie das geht wollen wir uns in einem Jugend hackt Lab Workshop anschauen, wir packen für euch die Nähmaschinen aus und für das Knowhow sorgt die Fashion-Designerin und Schneiderin Anna Mills. Es sind keine Vorkenntnisse nötig und alles an Stoff- und Nähmaterial haben wir vor Ort. Einfach vorbei kommen.<br><br>Habt ihr spezielle Nähwünsche, würden wir uns freuen, wenn ihr die vorab mit uns abklärt, wir sind für alle Experimente offen, brauchen dafür aber natürlich genügend Material vor Ort. Kontakt: tom[at]<a href="http://verschwoerhaus.de">verschwoerhaus.de</a><br><br>Wann?<br><br>Samstag: 4. Dezember 2021, 13 bis 17 Uhr im Verschwörhaus (Covidschutzregeln bitte beachten)<br><br>12-19 Jahre<br><br>🖥&nbsp;Wir packen die Nähmaschinen aus - lasst uns im Workshop gemeinsam eure Ideen verwirklichen<br>📍Ganzes EG<br>🏢Jugend hackt Lab<br>🙂Tom Novy<br>🙂Anna Mills<br><u></u>