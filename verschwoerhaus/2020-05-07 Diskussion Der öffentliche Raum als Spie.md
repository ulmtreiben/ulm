---
id: 06j8hicegcv3527padraelcml2
title: "Diskussion: Der öffentliche Raum als Spielfeld von Kunst und Kultur"
start: 2020-05-07 20:00
end: 2020-05-07 21:00
address: Ulmer Volkshochschule, Kornhauspl. 5, 89073 Ulm, Deutschland
teaser: <p>Die Ergebnisse des Ungehörsam-Workshop werden am<strong> 7. Mai 2020 im
  Club Orange der vh Ulm </
isCrawled: true
---
<p>Die Ergebnisse des Ungehörsam-Workshop werden am<strong> 7. Mai 2020 im Club Orange der vh Ulm </strong>zum folgenden Thema vorgestellt:</p><p><em><strong>Der öffentliche Raum als Spielfeld von Kunst und Kultur</strong></em></p><p>Wie kann man sich den öffentlichen Raum kreativ aneignen? In dem Podiumsgespräch stellen Akteure*innen aus der Street Art, Performance-Kunst und Digital-Kreative des Verschwörhauses vor, welche kreativen Aktionen, Projekte und Strategien den öffentlichen Raum (zurück-)erobern und was für ein Spannungsfeld dabei entsteht. Dabei soll sowohl auf Schwierigkeiten wie Genehmigungen oder abwehrende Haltung der Bevölkerung eingegangen werden, als auch auf die besondere Relevanz Kunst und Kultur in den öffentlichen Raum zu tragen. Mit auf dem Podium sind Simon Pfeffel, Performance-Künstler aus Stuttgart; die Ulmer Street Art Künstler Milo und Chris von »Partners in Paint«, sowie Mary Aufheimer und Tomas Novy&nbsp;<strong>vom Jugend hackt Lab</strong>.</p><p>Eintritt frei / offen für alle Besucher*innen</p><p>🖥Vorstellung des Lab-Ungehörsam-Workshops und Diskussion zum Thema "öffentlichen Raum zurückerobern"<br>📍Club Orange der Ulmer Volkshochschule&nbsp;<br>🙂Mary Aufheimer<br></p><p>🙂Tom Novy<br></p><p>🙂Tanja Leuthe<br></p>