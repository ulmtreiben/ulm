---
id: 5o5ld6fji26sd2t33bqp0nk9k4
title: "LoRaWAN und TTN: Bastel-Abend [Anmeldung erforderlich]"
start: 2021-09-08 18:30
end: 2021-09-08 21:30
address: Verschwörhaus, Weinhof 9, 89073 Ulm, Deutschland
teaser: Wir treffen uns jeden zweiten Mittwoch zum Thema LoRaWAN/The Things Network
  (TTN). Im weiteren Sinne
isCrawled: true
---
Wir treffen uns jeden zweiten Mittwoch zum Thema LoRaWAN/The Things Network (TTN). Im weiteren Sinne geht's also um dieses "Internet of Things" und  darüber wollen wir nicht abstrakt faseln, sondern selber machen, ausprobieren was möglich ist und dann gerne auf dieser Basis philosophieren.



Im vierwöchigen Wechsel  gibt's einmal einen "Offenen Abend" und einmal den "Bastelabend":

- Beim OFFENEN ABEND haben wir Zeit für alle deine/eure Fragen und Ideen. Alle Anwesenden und das Kernteam erklären gerne alles zum Thema von A bis Z. Manchmal gibt's zum Anfang vielleicht sogar einen Vortrag.

- Beim BASTELABEND soll Zeit sein an konkreten Projekten zu arbeiten. Hier ist natürlich auch jeder willkommen und vor allem könnt' ihr eure eigenen Bastelprojekte mitbringen um daran zu arbeiten - Werkzeug und Unterstützung ist vorhanden. Evtl. hat das Kernteam an diesem Abend nur nicht Zeit Neulingen alle Grundlagen ganz in Ruhe zu erklären, was aber keinen vom Reinschauen abhalten soll.



+++ Aktuell erfordern wir eine Anmeldung/Ankündigung, bevor man vorbeikommt. Dazu eine Mail an info@ttnulm.de +++



Es gibt auch Leihgeräte und wirklich günstige Bausätze um eigene Nodes aufzubauen. Mehr Informationen auf https://lora.ulm-digital.com





🖥 Alles um das Freie Funknetzwerk in Ulm

🙂 Matthias Schneider

🙂 Johannes Deger

🙂 Simon Lüke

🙂 Jakob Pietron

🙂 Kai-Uwe Piazzi