---
id: 345oq4bhptvid8a9d6va3frhin
title: "OpenBikeSensor: 3. Bastel-Abend"
start: 2021-07-20 18:00
end: 2021-07-20 21:00
address: Verschwörhaus, Weinhof 9, 89073 Ulm, Deutschland
teaser: Dies ist das dritte Treffen für die Konstruktion des OpenBikeSensors. Wir
  bauen weiter an den angefa
isCrawled: true
---
Dies ist das dritte Treffen für die Konstruktion des OpenBikeSensors. Wir bauen weiter an den angefangenen Sensoren, 5 von 10 sind fast fertig. Wir werden uns noch öfters treffen.<br><br>Ich bitte um eine Anmeldung/Ankündigung, bevor man vorbeikommt. Dazu eine kurze Email an&nbsp;<a href="mailto:norbertschulz@gmx.net">norbertschulz@gmx.net</a><br><br>Wie oft passiert es uns, dass uns ein Auto beinahe vom Sattel holt? Was wir alle kennen, überprüfen wir nun wissenschaftlich. Dazu&nbsp;wurde ein Sensor entwickelt, der den Abstand misst und Überholmanöver Geodaten zuordnet. Wo ist es sicher? Wo nicht? Welche Uhrzeit? Welche Strecken?<br><br>Mehr Informationen dazu auf&nbsp;<a href="https://www.openbikesensor.org/" id="ow566" __is_owner="true">https://www.openbikesensor.org</a><br><br>🖥Zusammenbau der ersten 10 OpenBikeSensor-Kits &amp; Austausch<i></i><br>📍Haus/Draußen<br>🏢ADFC<br>🙂Norbert