---
id: 263ttna8ovm2vtqbsj1mo9fqni_R20211020T163000
title: "LoRaWAN und TTN: Offener Abend [Anmeldung erforderlich]"
start: 2021-10-20 18:30
end: 2021-10-20 21:30
address: Verschwörhaus, Weinhof 9, 89073 Ulm, Deutschland
teaser: Das erste Treffen nach der zweiten und dritten Welle! <br>&nbsp;<br>Wir
  treffen uns jeden zweiten Mi
isCrawled: true
---
Das erste Treffen nach der zweiten und dritten Welle! <br>&nbsp;<br>Wir treffen uns jeden zweiten Mittwoch zum Thema LoRaWAN/The Things Network (TTN). Im weiteren Sinne geht's also um dieses "Internet of Things" und  darüber wollen wir nicht abstrakt faseln, sondern selber machen, ausprobieren was möglich ist und dann gerne auf dieser Basis philosophieren.<br><br>+++ Aktuell erfordern wir eine Anmeldung/Ankündigung, bevor man vorbeikommt. Dazu eine Mail an <a href="mailto:info@ttnulm.de">info@ttnulm.de</a>. Außerdem ist für die Teilnahme vor Ort ein 3G-Nachweis erforderlich. +++<br><br>Es gibt auch Leihgeräte und wirklich günstige Bausätze um eigene Nodes aufzubauen. Mehr Informationen auf <a href="https://lora.ulm-digital.com">https://lora.ulm-digital.com</a><br><br>🖥 Alles um das Freie Funknetzwerk in Ulm<br>🙂 Matthias Schneider<br>🙂 Simon Lüke<br>🙂 Jakob Pietron<br>🙂 Kai-Uwe Piazzi