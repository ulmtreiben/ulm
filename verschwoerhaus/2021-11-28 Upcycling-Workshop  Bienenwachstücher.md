---
id: 6lj68e9lcgq34b9j69ij2b9k65h38b9pc8rj0b9h69j3ichi61i68dhjck
title: ++abgesagt++ Upcycling-Workshop | Bienenwachstücher
start: 2021-11-28 11:00
end: 2021-11-28 13:00
teaser: Möchtest du dem ganzen Konsumwahnsinn am Black Friday entgegenwirken und statt
  neu kaufen, lieber s
isCrawled: true
---


Möchtest du dem ganzen Konsumwahnsinn am Black Friday entgegenwirken und statt neu kaufen, lieber selber etwas machen?



Dann komm zu unserem Upcycling-Workshop!

Gemeinsam mit dem Ulmer Upcyclinglabel Jacke wie Hose gestalten wir im Rahmen der "Make Something Week" am 28. November, um 11.00 Uhr, einen spannenden Vormittag, wo ihr ein „neues“, schönes und individuelles Bienenwachstuch herstellen könnt. 🐝



Der Workshop ist natürlich kostenlos. Ihr könnt gerne eure eigene Kleidungsstücke* mitbringen, wir haben aber auch vor Ort einige Stoffreste für euch vorbeireitet. 



Falls wir euer Interesse wecken konnten, dann meldet euch gerne unter unserer Mail info@greenpeace.de an und schon könnt ihr eure Lebensmittel daheim stylisch einpacken! 🌼





*die Kleidungsstücke sollten sauber sein und einen hohen Anteil an Baumwolle besitzen



**Beachtet bitte die Corona Bestimmungen des Landes Baden-Württemberg. In der aktuellen Alarmstufe ist die Teilnahme nur mit einem 2G Nachweis (geimpft, genesen) möglich. Aus Sicherheitsgründen bitten wir dennoch vor Ort einen Selbsttest durchzuführen. (Tests werden von uns gestellt.) Außerdem ist das Tragen einer Mund- & Nasenmaske Pflicht.



🖥Upcycling-Workshop 

📍Ganzes EG

🏢Greenpeace

🙂Ria

🙂Sabrina







