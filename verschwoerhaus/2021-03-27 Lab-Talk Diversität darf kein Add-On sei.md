---
id: 590pbe5uqk17695lf02d2d76kt
title: "Lab-Talk: Diversität darf kein Add-On sein"
start: 2021-03-27 14:00
end: 2021-03-27 15:30
teaser: <p>In unserem Lab-Talk am&nbsp;<em>Sa. 27.03. um 14:00 – 15:30</em>&nbsp;Uhr
  sprechen Victoria Kure-
isCrawled: true
---
<p>In unserem Lab-Talk am&nbsp;<em>Sa. 27.03. um 14:00 – 15:30</em>&nbsp;Uhr sprechen Victoria Kure-Wu und Ilona Stuetz darüber, was Diversität bedeutet und &nbsp;warum es bei der Entwicklung von Hard- und Software nicht nur ein “Bonus” sein darf. Und über das Projekt ichbinkeinvirus.org, das im Rahmen des #WirVsVirus Hackathons im März 2020 entwickelt wurde und bei allen Förderrunden abgelehnt wurde und trotzdem im Sommer 2020 live ging<em>.</em></p><p>Das Feedback zum Projekt war damals unter anderem, dass der Code nicht “wertvoll” genug sei, um gefördert zu werden und Rassismus etwas ist, über das man sich “später” kümmern müsse.</p><br><br>🖥 Lasst uns über Diversität sprechen<br>📍 Online-Talk<br>🙂 Ilona Stuetz<br>🙂 Victoria Kure-Wu