---
id: 3efk6dtmafgu852djc0991tda6
title: Maker-Monday Gerätewiedereinweisung
start: 2021-07-12 18:00
end: 2021-07-12 21:00
address: Verschwörhaus, Weinhof 9, 89073 Ulm, Deutschland
teaser: Wir wollen an diesem Tag vornehmlich Wiedereinweisungen auf die Geräte geben.
  Sollte genügend Zeit s
isCrawled: true
---
Wir wollen an diesem Tag vornehmlich Wiedereinweisungen auf die Geräte geben. Sollte genügend Zeit sein, gibt es heute aber auch die ersten neuen Einweisungen vor allem für den Lasercutter. Bitte meldet euch vorher an (s.U.). Ein tagesaktueller Schnelltest ist laut Verordnung nicht zwingend notwendig, aber schadet auch nicht.



Für die Anmeldung gerne eine Mail an makermonday@verschwoerhaus.de - oder für Leute, die bereits im Slack sind, in #makermonday bescheidgeben.



🖥 Von 3D-Druck bis Lasercut

📍 Werkstätten

🙂 Stefan Kaufmann