---
id: 0qk0e9oc0cmsfhoajlkjed8osn
title: "Five Needs: Wohnen in der Zukunft – Lab Talk im September"
start: 2021-09-10 18:00
end: 2021-09-10 19:30
teaser: "<p>Insbesondere in Großstädten ist Wohnraum teuer und rar. Mit
  fortschreitendem Klimawandel wird es "
isCrawled: true
---
<p>Insbesondere in Großstädten ist Wohnraum teuer und rar. Mit fortschreitendem Klimawandel wird es dabei auch um die Frage gehen, wie wir zukünftig ökologisch und praktisch Wohnraum für alle schaffen. Wagen wir daher einen Blick in die Zukunft des Wohnens!</p><p>Zu Gast im Jugend hackt Lab sind Vanessa und Johnny von der FH Würzburg mit ihrem Projekt&nbsp;<strong>five Needs: Wohnen in der Zukunft</strong>. Ihr Fokus liegt dabei auf der Frage, wie sich Wohnraum designtechnisch minimieren lässt aber dennoch nicht als Beengung wahrgenommen wird – sondern vielmehr als angenehm, schön und offen.</p><p>Bewohner*innen sollen zwischen individuellen Rückzug, als auch dem Bedürfnis nach Gemeinschaft und sozialer Nähe flexibel wählen können.</p><p>&nbsp;</p><p><strong>Freitag, 10. September 2021 um 18.oo Uhr: online oder Live-Stream aus dem Verschwörhaus mit Sitzmöglichkeit für Besucher*innen (Sofern es das Geschehen zulässt: wir informieren euch hier)&nbsp;</strong></p>