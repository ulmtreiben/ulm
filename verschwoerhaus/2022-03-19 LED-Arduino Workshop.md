---
id: 2b8biatokup1jt5sjcep83vk70
title: LED-Arduino Workshop
start: 2022-03-19 13:00
end: 2022-03-19 16:00
teaser: <p>Dich hat schon immer fasziniert wie Lichteffekte funktionieren? Du möchtest
  verstehen wie aus ele
isCrawled: true
---
<p>Dich hat schon immer fasziniert wie Lichteffekte funktionieren? Du möchtest verstehen wie aus elektrischem Strom Bilder werden? Und du möchtest mit Hilfe von LEDs und Arduino eigene Animationen programmieren? Dann schnell ins Jugend hackt Lab!</p><p>Programmierer David Jäckel ist für diesen Workshop bei uns. Zusammen werfen wir einen kurzen Blick auf die Geschichte der Animation und schauen wie das eigentlich alles funktioniert mit Strom, Farben, Licht und Bildschirmen. Danach erschaffen wir mit einem Arduino und RGB-LEDs unsere eigenen Animationen.</p><p>Kostenlos, ohne Vorkenntnisse und für alle Jugendlichen und junge Menschen offen. Einfach vorbei kommen – bitte Covid-Vorgaben vor Ort beachten.</p><p>Sa. 29.01.2022 | 13:00 – 16:30</p><br>🖥&nbsp;Zusammen werfen wir einen kurzen Blick auf die Geschichte der Animation und schauen wie das eigentlich alles funktioniert mit Strom, Farben, Licht und Bildschirmen.<br>📍Ganzes EG<br>🏢Jugend hackt Lab<br>🙂Tom Novy