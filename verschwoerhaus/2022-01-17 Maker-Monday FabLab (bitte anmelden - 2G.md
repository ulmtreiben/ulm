---
id: 2n92ugt82rppp0lrahshs4haai
title: Maker-Monday FabLab (bitte anmelden - 2G+)
start: 2022-01-17 18:00
end: 2022-01-17 21:00
address: Verschwörhaus, Weinhof 9, 89073 Ulm, Deutschland
teaser: "Heute ist nur das FabLab (im Erdgeschoss) geöffnet. Bitte beachten: *
  Geimpften- oder Genesenennac"
isCrawled: true
---
Heute ist nur das FabLab (im Erdgeschoss) geöffnet. Bitte beachten:

 

* Geimpften- oder Genesenennachweis mitbringen, außerdem ist ein tagesaktueller Schnelltest notwendig

* Für das Fablab (Lasercutter, 3D-Druck) möglichst vorab per Mail an makermonday@verschwoerhaus.de anmelden

* Die offene Holzwerkstatt (im UG) können wir leider gerade nicht anbieten



🖥 Von 3D-Druck bis Lasercut

📍 Werkstätten

🙂 David, Felix