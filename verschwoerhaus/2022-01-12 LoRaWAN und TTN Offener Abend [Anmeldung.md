---
id: 71d2q75ils62j33kpocdktc6tl
title: "LoRaWAN und TTN: Offener Abend [Anmeldung erforderlich]"
start: 2022-01-12 18:30
end: 2022-01-12 21:30
address: Verschwörhaus, Weinhof 9, 89073 Ulm, Deutschland
teaser: Wir treffen uns jeden zweiten Mittwoch zum Thema LoRaWAN/The Things Network
  (TTN). Im weiteren Sinne
isCrawled: true
---
Wir treffen uns jeden zweiten Mittwoch zum Thema LoRaWAN/The Things Network (TTN). Im weiteren Sinne geht's also um dieses "Internet of Things" und darüber wollen wir nicht abstrakt faseln, sondern selber machen, ausprobieren was möglich ist und dann gerne auf dieser Basis philosophieren.



+++ Aktuell erfordern wir eine Anmeldung/Ankündigung, bevor man vorbeikommt. Dazu eine Mail an info@ttnulm.de. Außerdem ist für die Teilnahme vor Ort ein 2G-Nachweis erforderlich. +++



Es gibt auch Leihgeräte und wirklich günstige Bausätze um eigene Nodes aufzubauen. Mehr Informationen auf https://lora.ulm-digital.com



🖥 Alles um das Freie Funknetzwerk in Ulm

🙂 Matthias Schneider

🙂 Simon Lüke

🙂 Jakob Pietron

🙂 Kai-Uwe Piazzi