---
id: 5md11pdnbrgd12dglko5p3fh57
title: "LoRaWAN und TTN: Offener Abend"
start: 2021-07-14 18:30
end: 2021-07-14 21:30
address: Verschwörhaus, Weinhof 9, 89073 Ulm, Deutschland
teaser: "Das erste Treffen nach der zweiten und dritten Welle!  Wir treffen uns jeden
  zweiten Mittwoch zum "
isCrawled: true
---
Das erste Treffen nach der zweiten und dritten Welle! 

 

Wir treffen uns jeden zweiten Mittwoch zum Thema LoRaWAN/The Things Network (TTN). Im weiteren Sinne geht's also um dieses "Internet of Things" und  darüber wollen wir nicht abstrakt faseln, sondern selber machen, ausprobieren was möglich ist und dann gerne auf dieser Basis philosophieren.

 

+++ Aktuell bitten wir um eine Anmeldung/Ankündigung, bevor man vorbeikommt. Dazu eine Mail an info@ttnulm.de +++



Es gibt auch Leihgeräte und wirklich günstige Bausätze um eigene Nodes aufzubauen. Mehr Informationen auf https://lora.ulm-digital.com



🖥 Alles um das Freie Funknetzwerk in Ulm

🙂 Matthias Schneider

🙂 Johannes Deger

🙂 Simon Lüke

🙂 Jakob Pietron

🙂 Kai-Uwe Piazzi