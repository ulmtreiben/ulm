---
id: 302thg86pt4n10hse4qr7du1sg_R20211025T160000
title: Maker-Monday FabLab & Holz (bitte anmelden)
start: 2021-10-25 18:00
end: 2021-10-25 21:00
address: Verschwörhaus, Weinhof 9, 89073 Ulm, Deutschland
teaser: "Heute ist sowohl die Holzwerkstatt als auch das FabLab geöffnet. Bitte
  beachten: * Geimpften- oder"
isCrawled: true
---
Heute ist sowohl die Holzwerkstatt als auch das FabLab geöffnet. Bitte beachten:

 

* Geimpften- oder Genesenennachweis mitbringen

* Für die Holzwerkstatt zwingend vorher per Mail an makermonday@verschwoerhaus.de anmelden (begrenzte Plätze) 

* Für das Fablab (Lasercutter, 3D-Druck) möglichst vorab per Mail an makermonday@verschwoerhaus.de anmelden



🖥 Von 3D-Druck bis Lasercut

📍 Werkstätten

🙂 Stefan Kaufmann