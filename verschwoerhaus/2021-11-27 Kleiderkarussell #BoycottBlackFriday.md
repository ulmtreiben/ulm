---
id: 6u8gdfp1fjiv9no996469a99ig
title: "++abgesagt++ Kleiderkarussell #BoycottBlackFriday"
start: 2021-11-27 10:00
end: 2021-11-27 15:00
teaser: "Du hast keine Lust auf die Konsumflut am Black Friday, aber möchtest trotzdem
  Abwechslung in deinem "
isCrawled: true
---
Du hast keine Lust auf die Konsumflut am Black Friday, aber möchtest trotzdem Abwechslung in deinem Kleiderschrank? Dann komm zu unserem Kleiderkarussell! Diesmal veranstaltet die Hochschulgruppe für Nachhaltigkeit gemeinsam mit Greenpeace Ulm und der IJM Hochschulgruppe das Kleiderkarussell im Verschwörhaus Ulm. Bring deine Klamotten vorbei und nimm andere mit - es muss nicht 1:1 getauscht werden. So wirst du deine ungetragenen Teile los und kommst mit supercoolen wieder nach Hause. Die Klamotten, die am Ende übrig bleiben, werden gespendet. 



Weitere Informationen:

Es gelten die aktuellen Corona Bestimmungen des Landes Baden-Württemberg. In der aktuellen Alarmstufe ist der Einlass mit einem 2G Nachweis (geimpft, genesen) möglich. Bitte informiere dich tagesaktuell über die Bestimmungen.



Bitte bring nur gewaschene und intakte Kleidungsstücke mit. Zum Reparieren von Kleidungsstücken kannst du gerne zu unserem Nähcafé kommen.





 

🖥Kleidertauschevent

📍Ganzes EG

🏢HSGN

🙂Jurek Lang

🙂Anna Lindner