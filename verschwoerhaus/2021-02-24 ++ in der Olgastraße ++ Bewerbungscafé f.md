---
id: offgf8veugk3tni6e1ugpvmqjn_R20210224T140000
title: ++ in der Olgastraße ++ Bewerbungscafé für Geflüchtete
start: 2021-02-24 15:00
end: 2021-02-24 17:00
address: Verschwörhaus, Weinhof 9, 89073 Ulm, Deutschland
teaser: "++ Achtung: Termine vorerst im Caritas-Gebäude in der Olgastraße 137.
  Telefonische Anmeldung vorab e"
isCrawled: true
---
++ Achtung: Termine vorerst im Caritas-Gebäude in der Olgastraße 137. Telefonische Anmeldung vorab erforderlich! ++<br><br>Das Bewerbungs-Café ist ein Kooperationsprojekt von Hauptamtlichen und Ehrenamtlichen. Es ist ein Begegnungsort, in dem das Thema "Bewerbung" im Fokus steht. Geflüchtete haben hier eine Anlaufstelle, um Unterlagen zu aktualisieren, zu erstellen, Fragen rund um Jobsuche und Bewerbung in Deutschland zu klären und Hilfe im Bewerbungsprozess zu erhalten.Das Café findet jeden Mittwoch von 15 bis 17 Uhr in den Räumen des zentral gelegenen Verschwörhaus Ulm im Weinhof 9 statt. <br><br>Wenn Sie Interesse haben, regelmäßig ehrenamtlich mitzuwirken, sprechen Sie Alexandra Specht an (Kontaktinfo <a href="https://www.caritas-ulm-alb-donau.de/hilfe-und-beratung/migration-flucht/migrationsberatung/migrationsberatung" id="ow394" __is_owner="true">https://www.caritas-ulm-alb-donau.de/hilfe-und-beratung/migration-flucht/migration-flucht</a> Tel. 0731-2063 32).<br><br>🖥 Bewerbungs-Cafe für Geflüchtete<br>📍 Olgastr 137<br>🏢 Caritas Ulm<br>🏢 Agentur für Arbeit<br>🏢 Stadt Ulm<br>🏢 Jobcenter Ulm