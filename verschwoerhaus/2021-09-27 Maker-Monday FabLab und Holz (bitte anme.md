---
id: 302thg86pt4n10hse4qr7du1sg
title: Maker-Monday FabLab und Holz (bitte anmelden)
start: 2021-09-27 18:00
end: 2021-09-27 21:00
address: Verschwörhaus, Weinhof 9, 89073 Ulm, Deutschland
teaser: "Heute ist sowohl die Holzwerkstatt als auch das FabLab geöffnet. Bitte
  beachten: * Geimpften- oder"
isCrawled: true
---
Heute ist sowohl die Holzwerkstatt als auch das FabLab geöffnet. Bitte beachten:

 

* Geimpften- oder Genesenennachweis mitbringen

* Für die Holzwerkstatt zwingend vorher per Mail an makermonday@verschwoerhaus.de anmelden (begrenzte Plätze) 

* Für das Fablab (Lasercutter, 3D-Druck) möglichst vorab per Mail an makermonday@verschwoerhaus.de anmelden



🖥 Von 3D-Druck bis Lasercut

📍 Werkstätten

🙂 Stefan Kaufmann