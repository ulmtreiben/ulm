---
id: 7eaj1atp4it065nh41poe8d6iq
title: "Kulturnacht: Codekunst und Vintage Tech(nic)"
start: 2021-09-18 16:00
end: 2021-09-18 23:00
address: Verschwörhaus, Weinhof 9, 89073 Ulm, Deutschland
teaser: Interaktive digitale Installationen, wieder zum Leben erweckte alte
  Informationstechnik und Einblick
isCrawled: true
---
Interaktive digitale Installationen, wieder zum Leben erweckte alte Informationstechnik und Einblicke, was mit Open-Source-Technologien möglich ist. Aktuell als Rundgang ums Haus mit spielerischen Möglichkeiten an den Fenstern geplant.





🖥Interaktive Ausstellungen und Getränke

📍Außenbereiche rund ums Haus

🏢VSH-Kulturnacht-Crew