---
id: 346ajekih1t8ha9fmlfpl18u0t
title: Robo und Coding Workshop im Jugend hack Lab
start: 2020-01-25 13:00
end: 2020-01-25 18:00
address: Verschwörhaus, Weinhof 9, 89073 Ulm, Deutschland
teaser: Du interessierst dich für Roboter? Und wolltest schon immer einmal wissen, was
  so ein Mikrocontroll
isCrawled: true
---


Du interessierst dich für Roboter? Und wolltest schon immer einmal wissen, was so ein Mikrocontroller alles kann? Dann bist du in unserem Robo und Coding Workshop genau richtig! Neben den Grundlagen des Programmierens und Codens, lernst du die bunte Programmiersprache NEPO kennen und außerdem, wie man einen M-Bot ansteuert. Eigens dafür kommt Andrea Herold ins Verschwörhaus, von der Roberta-Inititive des Frauenhofer-Instituts München.

🖥 Kostenloser Robo und Coding Workshop für Jugendliche ab 12 bis 18 Jahren. 13-18 Uhr

📍 Ganzes Haus

🏢 Jugend Hackt Lab Ulm

🙂Andrea Herold

🙂Open Roberta