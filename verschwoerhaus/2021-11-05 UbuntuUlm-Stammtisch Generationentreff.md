---
id: 3dm5570keitfcrssvblh6o4s65
title: UbuntuUlm-Stammtisch Generationentreff
start: 2021-11-05 14:00
end: 2021-11-05 16:00
address: Verschwörhaus, Weinhof 9, 89073 Ulm, Deutschland
teaser: Der Ubuntu-Stammtisch des Generationentreff Ulm – ein Treff zum Austausch über
  das Freie Betriebssys
isCrawled: true
---
Der Ubuntu-Stammtisch des Generationentreff Ulm – ein Treff zum Austausch über das Freie Betriebssystem (nicht nur) für die ältere Generation.



http://www.gt-ulm.de/computertraining/





🖥 Austauschtreffen zum freien Betriebssystem

📍 Workshopraum Bär

🏢 Generationentreff Ulm