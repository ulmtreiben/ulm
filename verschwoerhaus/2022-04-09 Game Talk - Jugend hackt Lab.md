---
id: 6skjc8madb93jvcqfclgfrksqt
title: "Videospiele-Talk: Was macht Death Stranding so besonders?"
start: 2022-04-09 15:00
end: 2022-04-09 16:30
teaser: <h1><br></h1><br>🖥Um mit euch herauszufinden, was Death Stranding ausmacht,
  welche Symbole und Ansp
isCrawled: true
---
<h1><br></h1><br>🖥Um mit euch herauszufinden, was Death Stranding ausmacht, welche Symbole und Anspielungen auch auf unsere gegenwärtige Gesellschaft sich darin finden, haben wir die Gamerin und Kommunikationswissenschaftlerin Marie Friske eingeladen. Stream auf verschwoerhaus.de/live<br>&nbsp; 📍Ganzes EG&nbsp;<br>🏢Jugend hackt Lab&nbsp;<br>🙂Tom Novy&nbsp;<br>🙂Marie Friske<span style="font-weight: normal;"><br></span>