---
id: 0pkdsn1h7s5llak5d9uagvfqi3
title: "Lab-Workshop: Game Adventure mit Twine"
start: 2021-07-06 17:00
end: 2021-07-06 19:00
teaser: <p>Für Gamer*innen, die gerne schreiben, und Leser*innen, die gerne am
  Computer spielen, sind Text-A
isCrawled: true
---
<p>Für Gamer*innen, die gerne schreiben, und Leser*innen, die gerne am Computer spielen, sind Text-Adventures genau das Richtige! Im Workshop können eigene interaktive Geschichten erfunden und am Notebook zum Leben erweckt werden. Ob Science Fiction, Superheld*innenabenteuer oder Krimi – alle Genres sind möglich. Dabei werden unterschiedliche Spiel- und Geschichtsverläufe entwickelt und die Text-Adventures können an den eigenen Mediengeräte oder im Netz online gespielt werden.</p><p>Ihr wollt wissen, wie Twine funktioniert und eure eigene Story erschaffen? Dann kommt zu unserem Workshop – ohne Anmeldung und kostenlos für alle zwischen 12 und 18 Jahren.</p><p><strong>Di. 06.07.2021 | 17:00 – 19:00</strong></p><p><a href="https://bbb.ulm.dev/b/twine">Link zur Veranstaltung</a></p>