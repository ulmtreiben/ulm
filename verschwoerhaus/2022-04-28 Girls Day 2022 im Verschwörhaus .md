---
id: 1tev20vq60k4b4nkjebv4m1p46
title: "Girls Day 2022 im Verschwörhaus "
start: 2022-04-28 17:00
end: 2022-04-28 19:30
teaser: 🖥&nbsp;Was ist das Verschwörhaus? Und was gibt es alles so in einem
  Makerspace? Das wollen wir euch
isCrawled: true
---
🖥&nbsp;Was ist das Verschwörhaus? Und was gibt es alles so in einem Makerspace? Das wollen wir euch beim diesjährigen&nbsp;<em><strong>Jugend hackt Lab Girls Day</strong></em>&nbsp;in einem Workshop zeigen: 3D-Druck, LEDs löten oder gleich ein Raumschiff lasercutten? Ihr könnt euch nicht entscheiden: macht nichts, dann machen wir doch am besten alles :) - und als Goodie könnt ihr das Ergebnis mitnehmen. - ohne Anmeldung<br>📍Ganzes EG<br>🏢Jugend hackt Lab, Frauen und Computerkram<br>🙂Jana Funke + Tom Novy