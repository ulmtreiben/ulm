---
id: 5ubg8l55npdeuk15ldh3c4u790
title: Jugend hackt Open Lab
start: 2021-09-21 17:00
end: 2021-09-21 19:30
teaser: <p>Das Jugend hackt-Lab im Verschwoerhaus ist wieder back und damit sind auch
  unsere offenen Termine
isCrawled: true
---
<p>Das Jugend hackt-Lab im Verschwoerhaus ist wieder back und damit sind auch unsere offenen Termine zurück. Es darf gemeinsam rumgenerded werden. Wir freuen uns auf Euch und Eure Projekte und haben ansonsten auch immer noch ein paar kleine Projektideen in der Hinterhand.</p><p>Ob altbekanntes Gesicht oder Neuzugang: Kommt einfach rum, lernt nette Leute kennen und habt Spaß im Lab!</p><p>Das Open Lab ist kostenlos für alle jungen Menschen zwischen 12 und 19 Jahren. Natürlich gilt auch im Open Lab 3G für Mentor*innen, Registrierung und eine Mund-Nasenbedeckung im ganzen Hausi für alle.</p>