---
id: 3snrct11qp8s09llqpv5fku5lf
title: "+++fällt aus +++ Von Zombies und anderen Pandemien: Das Politische in
  Videogames"
start: 2021-11-19 20:00
end: 2021-11-19 21:00
address: Verschwörhaus, Weinhof 9, 89073 Ulm, Deutschland
teaser: <p>+++fällt aus +++ Wird evtl. im Dezember nachgeholt +++</p><p>Computerspiele
  werden oft nur als Un
isCrawled: true
---
<p>+++fällt aus +++ Wird evtl. im Dezember nachgeholt +++</p><p>Computerspiele werden oft nur als Unterhaltung wahrgenommen - trotzdem sind sie auch politisch oder bringen verschiedene politische Kontexte durch Erzählungen, Gameplay und Grafik hervor. Das zeigt sich auch am zunehmenden Einsatz der Figur des Zombies, die sinnbildlich in Games als externe Bedrohung für eine überforderte Gemeinschaft interpretiert werden kann. Zombies stehen aber auch für Narrationen von Pandemien oder Krankheiten in digitalen Spielen. Bei genauer Betrachtung ist sogar die Zunahme der Zombie-Erzählungen in Videogames in den letzten Jahren feststellbar.</p><p><br>Im Rahmen der&nbsp;<em>Aktionstage Netzpolitik &amp; Demokratie 2021</em>&nbsp;spricht der Neu-Ulmer Kulturwissenschaftler&nbsp;<strong>Arno Görgen</strong>&nbsp;über das Politische in digitalen Spielen. Denn Computerspiele existieren nicht einfach unabhängig von der Gesellschaft in der wir leben, sondern spiegeln sie in bestimmten Aspekten auch wieder. Wie das mit Narrationen von Pandemien und Zombies einhergeht wollen wir nach dem Talk mit euch diskutieren.<br><br>Der Talk und die anschließende Diskussionsrunde wird als Hybridformat angeboten. Erwachsene Gäste bitten wir sich an die 3G-Regel (Getestet/Genesen/Geimpft) zu halten und alle Besucher*innen bitte Mund-Nasenschutz während der Veranstaltung&nbsp; tragen. Der Talk wird auch online übertragen (<a href="http://verschwoerhaus.de/live">verschwoerhaus.de/live</a>).</p><br>Eintritt: kostenlos<br><br>🖥Wie Zombie-Erzählungen in Videogames politische Kontexte hervorbringen und aktuelle Gesellschaftsthemen spiegeln<br>📍Salon<br>🏢Jugend hackt Lab<br>🙂Tom Novy<br>🙂Arno Görgen