---
id: 35175uvhbasu6bm4ofvi7m2u3l
title: "+++ fällt aus +++Animationen häcken im Jugend hackt Lab "
start: 2020-11-07 13:00
end: 2020-11-07 17:00
address: Verschwörhaus, Weinhof 9, 89073 Ulm, Deutschland
teaser: <p>Dich hat schon immer fasziniert wie Animationen funktionieren? Du möchtest
  verstehen wie aus elek
isCrawled: true
---
<p>Dich hat schon immer fasziniert wie Animationen funktionieren? Du möchtest verstehen wie aus elektrischem Strom Bilder werden? Und du möchtest mit Hilfe von LEDs und Arduino eigene Animationen programmieren? Dann schnell ins Jugend hackt Lab!</p><p>Programmierer David Jaeckel ist für diesen Workshop bei uns. Zusammen werfen wir einen kurzen Blick auf die Geschichte der Animation und schauen wie das eigentlich alles funktioniert mit Strom, Farben, Licht und Bildschirmen. Danach erschaffen wir mit einem Arduino und RGB-LEDs unsere eigenen Animationen.</p><p>Kostenlos, ohne Vorkenntnisse und für alle Jugendlichen und junge Menschen offen.Kostenlos, ohne Vorkenntnisse und für alle Jugendlichen und junge Menschen offen.<br></p><p>🖥Kostenloser LED-Programmier-Lern Workshop<br>📍Salon<br>🏢Jugend hackt Lab Ulm<br>🙂David Jäckel</p><p>🙂Tom Novy et. al.</p>