---
id: 5r1gk0mau29rd69fdd816rsb3d
title: Maker-Monday
start: 2021-06-21 18:00
end: 2021-06-21 21:00
address: Verschwörhaus, Weinhof 9, 89073 Ulm, Deutschland
teaser: Die erste Fablaböffnungszeit 2021! Wir wollen an diesem Tag vornehmlich
  Wiedereinweisungen auf die G
isCrawled: true
---
Die erste Fablaböffnungszeit 2021! Wir wollen an diesem Tag vornehmlich Wiedereinweisungen auf die Geräte geben. Sollte genügend Zeit sein, gibt es heute aber auch die ersten neuen Einweisungen vor allem für den Lasercutter. Bitte meldet euch vorher an (s.U.) und bringt einen tagesaktuellen Schnelltest mit.<br><br>Für die Anmeldung gerne eine Mail an <a href="mailto:makermonday@verschwoerhaus.de">makermonday@verschwoerhaus.de</a> - oder für Leute, die bereits im Slack sind, in #makermonday bescheidgeben.<br><br>🖥 Von 3D-Druck bis zur Holzwerkstatt<br>📍 Werkstätten<br>🙂 Sabine Wieluch<br>🙂 Henning Schütz