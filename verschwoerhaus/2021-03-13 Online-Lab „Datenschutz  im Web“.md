---
id: 1i0huik6ds804sgeaphn1pml8a
title: "Online-Lab: „Datenschutz  im Web“"
start: 2021-03-13 11:00
end: 2021-03-13 13:00
teaser: <p>Mit dem Thema&nbsp;<em>Datenschutz im Web</em>&nbsp;wollen wir anschließen
  an unseren Webdesign-W
isCrawled: true
---
<p>Mit dem Thema&nbsp;<em>Datenschutz im Web</em>&nbsp;wollen wir anschließen an unseren Webdesign-Workshop vom&nbsp;<a href="https://verschwoerhaus.de/about-me-homepage-web-design-workshop/">19. Februar</a>. Was hat sich mit der Datenschutz-Grundverordnung (DSGVO) seit 2018 geändert? Was für Rechte habt ihr, wenn ihr Websites besucht, die mit euren Daten arbeiten möchten? Und: Was müsst ihr bei eurer eigenen Homepage beachten – beispielsweise was den Cookie-Banner angeht.</p><p>All dem wollen wir uns in einem kostenlosen Online-Workshop widmen. Fragen zum Workshop gerne auch an unseren Workshop-Teamer:&nbsp;<a href="mailto:der@mit-dem-zopf.cf">der@mit-dem-zopf.cf&nbsp;</a>– „<em>Dфҿ</em>&nbsp;mit dem Zopf“ ist Hamburger Schüler, Junghacker*in und interessiert sich für Netzwerke, Flutter und Linux.</p><p>🖥 Alles zu Datenschutz im Web</p><p>&nbsp;📍 Online-Lab</p><p>🙂 DermitdemZopf</p><p><br></p>