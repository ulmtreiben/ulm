---
id: 2n92ugt82rppp0lrahshs4haai_R20220228T170000
title: Maker-Monday FabLab [Anmeldung erforderlich]
start: 2022-02-28 18:00
end: 2022-02-28 21:00
address: Verschwörhaus, Weinhof 9, 89073 Ulm, Deutschland
teaser: <html-blob>Heute ist nur das FabLab (im Erdgeschoss) geöffnet. Bitte
  beachten:<br>&nbsp;<br>* Geimpf
isCrawled: true
---
<html-blob>Heute ist nur das FabLab (im Erdgeschoss) geöffnet. Bitte beachten:<br>&nbsp;<br>* Geimpften- oder Genesenennachweis mitbringen, außerdem ist ein tagesaktueller Schnelltest notwendig<br>* Für das Fablab (Lasercutter, 3D-Druck) möglichst vorab per Mail an <a href="mailto:makermonday@verschwoerhaus.de">makermonday@verschwoerhaus.de</a> anmelden<br>* Die offene Holzwerkstatt (im UG) können wir leider gerade nicht anbieten<br><br>🖥 Von 3D-Druck bis Lasercut<br>📍 Werkstätten<br>🙂 Felix, Tobi</html-blob>