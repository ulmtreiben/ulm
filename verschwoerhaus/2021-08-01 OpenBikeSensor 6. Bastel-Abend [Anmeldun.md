---
id: 4i7bn4na1sj6f5jv6qb3ssof19
title: "OpenBikeSensor: 6. Bastel-Abend [Anmeldung erforderlich]"
start: 2021-08-01 19:00
end: 2021-08-01 21:00
address: Verschwörhaus, Weinhof 9, 89073 Ulm, Deutschland
teaser: Dies ist das sechste gemeinsame Treffen für die Konstruktion des
  OpenBikeSensors. Wir bauen weiter d
isCrawled: true
---
Dies ist das sechste gemeinsame Treffen für die Konstruktion des OpenBikeSensors. Wir bauen weiter den 10 Sensoren, ein paar Bausätze sind noch nicht ganz fertig und müssen noch in die Gehäuse eingebaut werden. Nun läuft der Funktionstest und Fehleranalyse. Andere sind schon fertig, funktionsgetestet und sollen nun richtig am Fahrrad mit allen Funktionen getestet werden.<br><br>Ich bitte um eine Anmeldung/Ankündigung, bevor man vorbeikommt. Dazu eine kurze Email an&nbsp;<a href="mailto:norbertschulz@gmx.net">norbertschulz@gmx.net</a><br><br>Wie oft passiert es uns, dass uns ein Auto beinahe vom Sattel holt? Was wir alle kennen, überprüfen wir nun wissenschaftlich. Dazu&nbsp;wurde ein Sensor entwickelt, der den Abstand misst und Überholmanöver Geodaten zuordnet. Wo ist es sicher? Wo nicht? Welche Uhrzeit? Welche Strecken?<br><br>Mehr Informationen dazu auf&nbsp;<a href="https://www.openbikesensor.org/">https://www.openbikesensor.org</a><br><br>🖥Zusammenbau der ersten 10 OpenBikeSensor-Kits &amp; Austausch<i></i><br>📍Haus/Draußen<br>🏢ADFC<br>🙂Norbert