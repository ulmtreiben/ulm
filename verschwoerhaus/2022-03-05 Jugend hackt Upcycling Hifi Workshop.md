---
id: 7olg5lorev7mq761rk5kcljpp6
title: "Jugend hackt: Upcycling Hifi Workshop"
start: 2022-03-05 13:00
end: 2022-03-05 18:00
teaser: 🖥 Lautsprecher modernisieren, HIFI upcyceln, alte Technik fit machen📍Ganzes
  EG🏢Jugend hackt Lab
isCrawled: true
---
🖥 Lautsprecher modernisieren, HIFI upcyceln, alte Technik fit machen

📍Ganzes EG

🏢Jugend hackt Lab

🙂Tom Novy

🙂Heinz-Wilhelm Schäbe