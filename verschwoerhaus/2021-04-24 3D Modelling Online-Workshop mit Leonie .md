---
id: 5kmd2oljor7saot728n62mdp9d
title: 3D Modelling Online-Workshop mit Leonie Wolf
start: 2021-04-24 12:00
end: 2021-04-24 18:00
teaser: "<h4>Lasst durch Games Kunst entstehen: Game Artistin Leonie Wolf ist wieder
  am Start und zeigt wie m"
isCrawled: true
---
<h4>Lasst durch Games Kunst entstehen: Game Artistin Leonie Wolf ist wieder am Start und zeigt wie man mit der Software Blender 3D-Modelle und 3D-Figuren erstellen kann. Vorkenntnisse nicht nötig! Und das ganze ist kostenlos. Weil aber die Plätze begrenzt sind, bitten wir euch um eine Anmeldung, kurze Mail reicht:&nbsp;<a href="mailto:tom@verschwoerhaus.de">tom@verschwoerhaus.de</a><br></h4><p>Der Workshop findet am&nbsp;<strong>24. April 2021 von 12:00-18:00 Uhr</strong>&nbsp;online statt: einen Zugangslink und weitere Infos gibt es nach der Anmeldung 🙂</p><p>Ihr benötigt: PC/Mac, Tastatur, Maus, Cam, Mikro und Lautsprecher.</p><p><strong>Alter: von 12-18&nbsp;</strong></p><p><strong><br></strong></p><p>🖥 Kostenloser Workshop zu 3D-Modelling f+r Einsteiger*innen und Pros</p><p>&nbsp;📍 Online-Lab</p><p>🙂 Leonie Wolf<br></p>