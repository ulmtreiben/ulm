---
id: 6s15vd71r54hmkiejr85kv5moj
title: "Hacking Bitsy: +1 Gamedesign Workshop"
start: 2020-02-15 13:00
end: 2020-02-15 18:00
address: Verschwörhaus, Weinhof 9, 89073 Ulm, Deutschland
teaser: <p>Mit dem kostenlosen und browserbasiertem Tool Bitsy lassen sich schnell
  eigene Spiele entwickeln,
isCrawled: true
---
<p>Mit dem kostenlosen und browserbasiertem Tool Bitsy lassen sich schnell eigene Spiele entwickeln, die durch große Welten führen, mit Rätseln aber vor allem Entscheidungen herausfordern. Doch dabei wollen wir es nicht belassen. In diesem Workshop lernst du nicht nur, wie man solche Spiele entwickelt, sondern auch wie man das Tool Bitsy hacken kann um es um nicht vorhergesehende Funktionen zu erweitern. – Vorkenntnisse jeglicher Art nicht notwendig!</p><p><strong>15. Februar 2020, von 13.00-18.00 Uhr mit dem Interaktions Designer Mat Loewe (Berlin).</strong></p><p>Der Workshop ist offen für Jugendliche und junge Erwachsene, kostenlos und ohne Voranmeldung.</p><p><br>🖥Kostenloser Hacking bitsy Workshop mit Mat Loewe<br>📍Salon<br>🏢Jugend hackt Lab Ulm<br>🙂Mat Loewe<br></p>