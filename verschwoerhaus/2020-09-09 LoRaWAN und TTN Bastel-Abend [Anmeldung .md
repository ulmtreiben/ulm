---
id: 083579pqhspbj4d4fmd5kih0rg_R20200909T163000
title: "LoRaWAN und TTN: Bastel-Abend [Anmeldung erforderlich]"
start: 2020-09-09 18:30
end: 2020-09-09 21:30
address: Verschwörhaus, Weinhof 9, 89073 Ulm, Deutschland
teaser: Wir treffen uns jeden zweiten Mittwoch zum Thema LoRaWAN/The Things Network
  (TTN). Im weiteren Sinne
isCrawled: true
---
Wir treffen uns jeden zweiten Mittwoch zum Thema LoRaWAN/The Things Network (TTN). Im weiteren Sinne geht's also um dieses "Internet of Things" und  darüber wollen wir nicht abstrakt faseln, sondern selber machen, ausprobieren was möglich ist und dann gerne auf dieser Basis philosophieren.<br><br>Im vierwöchigen Wechsel  gibt's einmal einen "Offenen Abend" und einmal den "Bastelabend":<br>- Beim OFFENEN ABEND haben wir Zeit für alle deine/eure Fragen und Ideen. Alle Anwesenden und das Kernteam erklären gerne alles zum Thema von A bis Z. Manchmal gibt's zum Anfang vielleicht sogar einen Vortrag.<br>- Beim BASTELABEND soll Zeit sein an konkreten Projekten zu arbeiten. Hier ist natürlich auch jeder willkommen und vor allem könnt' ihr eure eigenen Bastelprojekte mitbringen um daran zu arbeiten - Werkzeug und Unterstützung ist vorhanden. Evtl. hat das Kernteam an diesem Abend nur nicht Zeit Neulingen alle Grundlagen ganz in Ruhe zu erklären, was aber keinen vom Reinschauen abhalten soll.<br><br>+++ Aktuell erfordern wir eine Anmeldung/Ankündigung, bevor man vorbeikommt. Dazu eine Mail an <a href="mailto:info@ttnulm.de" id="ow667" __is_owner="true">info@ttnulm.de</a> +++<br><br>Es gibt auch Leihgeräte und wirklich günstige Bausätze um eigene Nodes aufzubauen. Mehr Informationen auf <a href="https://lora.ulm-digital.com">https://lora.ulm-digital.com</a><br><br><br>🖥 Alles um das Freie Funknetzwerk in Ulm<br>🙂 Matthias Schneider<br>🙂 Johannes Deger<br>🙂 Simon Lüke<br><br>🙂 Jakob Pietron<br>🙂 Kai-Uwe Piazzi