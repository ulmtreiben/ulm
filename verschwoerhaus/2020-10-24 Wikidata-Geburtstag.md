---
id: 0h7eudajqq6ueat18l06kaqstb
title: Wikidata-Geburtstag
start: 2020-10-24 14:00
end: 2020-10-24 19:00
address: Verschwörhaus, Weinhof 9, 89073 Ulm, Deutschland
teaser: Am&nbsp;<b>24.10.2020</b>&nbsp;wird Wikidata&nbsp;8. Wir wollen den Geburtstag
  gebührend feiern. Daz
isCrawled: true
---
Am&nbsp;<b>24.10.2020</b>&nbsp;wird Wikidata&nbsp;8. Wir wollen den Geburtstag gebührend feiern. Dazu laden wir herzlich ab&nbsp;<b>14:00 Uhr</b>&nbsp;ins Verschwörhaus ein. Unter Beachtung der Hygiene- und Abstandsregeln wollen wir Datensätze und Items mit Ulm-Bezug in Wikidata bearbeiten. Wer nicht vor Ort sein kann, kann auch&nbsp;virtuell&nbsp;dabei sein.<br><br>Wir bitten um Anmeldung unter https://de.wikipedia.org/wiki/Wikipedia:Ulm/Neu-Ulm#Wikidata-Geburtstag<br><br><br>📍 Salon + online<br>🏢 Wikipedia-Community Ulm + Neu-Ulm