---
id: 2knmerdadco09dk60f1571uit6
title: Open Jugend hackt Lab
start: 2021-10-26 17:00
end: 2021-10-26 19:30
teaser: Das Jugend hackt-Lab im Verschwörhaus ist wieder back und damit sind auch
  unsere offenen Termine zur
isCrawled: true
---
Das Jugend hackt-Lab im Verschwörhaus ist wieder back und damit sind auch unsere offenen Termine zurück. Es darf gemeinsam rumgenerded werden. Wir freuen uns auf Euch und Eure Projekte und haben ansonsten auch immer noch ein paar kleine Projektideen in der Hinterhand.



Ob altbekanntes Gesicht oder Neuzugang: Kommt einfach rum, lernt nette Leute kennen und habt Spaß im Lab!



Das Open Lab ist kostenlos für alle jungen Menschen zwischen 12 und 19 Jahren. Natürlich gilt auch im Open Lab 3G für Mentor*innen, Registrierung und eine Mund-Nasenbedeckung im ganzen Hausi für alle.





🖥 Eigene Ideen verwirklichen, Inspiration suchen und mit den Mentor*innen experimentieren

📍 Ganzes Haus

🏢 Jugend Hackt Lab Ulm

🏢 Jugend Hackt Mentor*innen

🏢 Tom Novy