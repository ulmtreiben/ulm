---
id: "769422010514907"
title: Mahnwache - Lieferkettengesetz
start: 2020-09-18 17:30
end: 2020-09-18 18:30
locationName: Münsterplatz Ulm
address: Münsterplatz 21, 89073 Ulm
link: https://www.facebook.com/events/769422010514907/
teaser: Es ist Zeit, den "Gewinnen ohne Gewissen" ein Ende zu
  setzen!  Lieferkettengesetz ! Wieso ist es bei uns legal, Produkte zum Kauf
  anzubieten, deren He
isCrawled: true
---
Es ist Zeit, den "Gewinnen ohne Gewissen" ein Ende zu setzen!
 Lieferkettengesetz !
Wieso ist es bei uns legal, Produkte zum Kauf anzubieten, deren Herstellung
Menschenrechte massiv verletzen und die Umwelt nachhaltig zerstören?
Es gibt jetzt die Chance, ein Gesetz zu erreichen,
 wenn wir gemeinsam Druck machen!
Wir fordern die Bundesregierung auf, menschenrechtliche Sorgfaltspflicht von
Unternehmen gesetzlich festzuschreiben und an die Missachtung klare
juristische Konsequenzen zu knüpfen.
Wir brauchen einen verbindlichen Rahmen zur Wahrung von Menschenrechten
und Umweltschutz in der gesamten Wertschöpfungskette.
