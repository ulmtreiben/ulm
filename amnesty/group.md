---
name: Amnesty Ulm
website: http://www.amnesty-ulm.de
email: kontakt@amnesty-ulm.de
scrape:
  source: facebook
  options:
    page_id: AmnestyUlm
---
Amnesty International ist eine offene Menschenrechtsorganisation, deren Stärke in der Mitarbeit möglichst vieler Menschen liegt. Einsteigen kann man überall, zu jeder Zeit und auf verschiedenen Wegen - auch bei uns im Bezirk Ulm.
