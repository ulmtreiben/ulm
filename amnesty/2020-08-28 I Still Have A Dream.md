---
id: "1361811834210412"
title: I Still Have A Dream
start: 2020-08-28 15:00
end: 2020-08-28 18:00
address: Friedrichsau 1, 89073 Ulm
link: https://www.facebook.com/events/1361811834210412/
teaser: Wir, haben einen Traum. Wir sind laut! Das mega Event in der Friedrichsau.
  Nähe Liederkranz am 28.08.2020 ab 15 Uhr. Ihr könnt uns nicht überhören!
isCrawled: true
---
Wir, haben einen Traum.
Wir sind laut! Das mega Event in der Friedrichsau. Nähe Liederkranz am 28.08.2020 ab 15 Uhr. Ihr könnt uns nicht überhören!
57 years later, I still have a dream. In Kooperation zwischen Mein ICH gegen Rassismus und Amnesty International Ulm.

"I have a dream", Martin Luther King, 28 August 1963, at the Lincoln Memorial, Washingtin D.C.

Martin Luther King gilt als einer der herausragendsten Vertreter im gewaltfreien Kampf gegen Unterdrückung und soziale Ungerechtigkeit. Er war zwischen Mitte der 1950er und Mitter der 1960er Jahre der bekannteste Sprecher des Civil Rights Movement, der US-amerikanischen Bürgerrechtsbewegung der Afroamerikaner. Martin Luther Kings Rede trug dazu bei, dass in den USA 1964 ein Gesetz eingeführt wurde, nach dem alle Menschen gleich sind. Am 4. April 1968 wurde er ermordet. Der oder die Täter konnten nie mit letzter Sicherheit festgestellt werden. Die Trauernden haben für die Verwirklichung seines Traumes weiter gekämpft.

Wir feiern das 57. Jubiläum Martin Luther Kings Rede in Washington, ein Tag der in die Geschichte eingeht. Lasst uns gemeinsam diesen Tag feiern und an ihn erinnern, an all das was sich verändert hat und sich noch ändern muss. Gutes Essen, Trinken, gute Musik, Entertainment und ihr. Einfach genießen und da sein.


We, have a dream. And:
We are loud! The mega event in the Friedrichsau. Near Liederkranz on 28.08.2020 from 15 oclock. You can´t miss us!
57 years later, I still have a dream. In cooperation between Mein ICH gegen Rassismus and Amnesty International Ulm.

„ I have a dream“, Martin Luther King, 28.08.1963, at th Lincoln Memorial, Washington D.C.

Martin Luther King ist considered one of the most outstanding representatives in the non-violent struggle against oppression and social injustice. Between the mid-50s and mid-60s, he was the best-known spokesman for the Civil Rights Movement, the US civil rights movement of African Americans.

Martin Luther King´s speech contributed to the fact that in 1964 a law was intoduced in the USA according to which all people are equal.
He was murdered on April 4. 1968. The perpetrator or perpetrators could never be determined with absolute certainty. The mourners continued to fight for the realization of his dream.

We celebrate the 57th anniversary of Martin Luther King´s speech in Washington, a day that will go down in history. Let us celebrate this day together and remember it, all that has changed and still has to change.

Beiträge mit "Pipi in den Augen" Garantie
@amnestyulm und unseren SpezialGästen wie Marco Kerler.

BURNER LIVE MUSIC
@serotonia_music
@deangelis_e_demoni
und Überraschungsbands

Entertainment, Kunst
@lisaearaoneli
@ruthless_girl

HEFTIGE TANZ PERFORMANCES
@real_bellososqosherif
@fkv_dance

und EXCLUSIVE
wird, vor dem offiziellen Start, die Mein ICH gegen Rassismus Pizza mit verstärktem Personal in die Friedrichsau geliefert.
Von @oezitastykitchen

Text -> @fischerinskleid @x.blondii.x

Flyer -> @lisaesraoneli

#meinichgegenrassismus #ulm #ulmneuulm #ulmgermany #ulmliebe #ulmcity #ulmgegenrechts #ulmzeigtgesicht #ulmheute #ihaveadream #rassismus #gegenrassismus #noracism #gibrassismuskeinechance #rassismusneindanke #blackandwhiteunite #blacklivesmatter