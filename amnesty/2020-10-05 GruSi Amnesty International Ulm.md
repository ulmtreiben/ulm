---
id: "916228458861836"
title: GruSi Amnesty International Ulm
start: 2020-10-05 20:00
end: 2020-10-05 22:00
locationName: Amnesty Ulm
address: Ensingerstraße 21, 89073 Ulm
link: https://www.facebook.com/events/916228458861836/
image: 110191504_3834950819854115_6876744368841499085_o.jpg
teaser: Jeden Montag treffen wir uns, um zu diskutieren, * was aktuell hier und
  anderswo geschieht  * was wir gegen Missstände unternehmen * was wir unterstüt
isCrawled: true
---
Jeden Montag treffen wir uns, um zu diskutieren,
* was aktuell hier und anderswo geschieht 
* was wir gegen Missstände unternehmen
* was wir unterstützen, mit wem wir zusammen etwas zu einem bestimmten Thema auf die Beine stellen
* was nach eingehender Betrachtung unseres Materiallagers an neuen Designs für T-Shirts, Pulllis, Fahnen... angebracht und hübsch wäre
... bei Kaffee immer, bei Kuchen gelegentlich

Jeder ist herzlich eingeladen, einfach mal vorbei zu kommen, sich umzuschauen, Fragen zu stellen, was wir eigentlich machen - wir unterhalten uns gerne :-)

Zur Zeit bieten wir auch eine virtuelle Teilnahme an, für diejenigen, die keine Zeit zu kommen haben oder aus gesundheitlichen Gründen nicht in situ teilnehmen möchten. Schreibt hierfür eine kurze Mail an kontakt@amnesty-ulm.de , dann bekommt ihr einen Link zugeschickt, über den ihr teilnehmen könnt.
Wir stellen Desinfektionsmittel bereit und das Büro ist groß, Abstand kann leicht gehalten werden.