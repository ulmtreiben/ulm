---
name: VEgi & BUnt, Ulm
website: https://www.facebook.com/vegibunt/
scrape:
  source: facebook
  options:
    page_id: vegibunt
---
Anlaufstelle für alle Veganer\*innen, Vegetarier\*innen sowie an einer tierleidfreien Lebensweise interessierten Personen aus Ulm und Umgebung.
