---
id: "565916600694614"
title: Svenja Gräfen liest »Freiraum«
start: 2020-05-28 20:00
end: 2020-05-28 22:00
locationName: Ulmer Volkshochschule
address: Kornhausplatz 5, 89073 Ulm
link: https://www.facebook.com/events/565916600694614/
image: 86842069_2921640521246452_4560565367755243520_o.jpg
teaser: Svenja Gräfen liest »Freiraum« Verschoben auf 28. Mai Eintritt EUR 8,00/6,00
  EinsteinHaus, Club Orange Nr. 20F 0118031  Bietet das Leben auf dem Land
isCrawled: true
---
Svenja Gräfen liest »Freiraum«
Verschoben auf 28. Mai
Eintritt EUR 8,00/6,00
EinsteinHaus, Club Orange
Nr. 20F 0118031

Bietet das Leben auf dem Land nicht viel mehr Freiräume für junge Großstädter? Eigentlich führen Vela und Maren eine glückliche, lesbische Beziehung. Aber ihre Träume zerbröseln zunehmend an den Anforderungen der Großstadt. Maren will ausbrechen und ein alternatives Leben führen; am Rande der Stadt, in einem Wohnprojekt mit vielen anderen, ohne Mieterhöhungen und permanente Konkurrenz. Hier kreist alles um Theo. So wie er versuchen Vela und Maren ihren Platz zwischen Hoffnung und Zukunftsangst zu finden – aber Vela spürt, dass in dieser Gemeinschaft etwas nicht stimmt. Nach »Das Rauschen in unseren Köpfen« beeindruckt die 29-jährige Autorin, (Netz-)Feministin und Poetry-Slammerin Svenja Gräfen erneut mit ihrer außergewöhnlich präzisen Sprache und genauen Beobachtungsgabe. Am nächsten Tag hält Gräfen einen Workshop, siehe Kurs-Nr. 20F0910033.

Foto: Constantin Timm