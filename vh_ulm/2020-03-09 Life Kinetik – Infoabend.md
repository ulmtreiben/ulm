---
id: "201661180892695"
title: Life Kinetik – Infoabend
start: 2020-03-09 18:00
end: 2020-03-09 19:00
address: Erbach, KaffCafé, Auf der Wühre 13
link: https://www.facebook.com/events/201661180892695/
image: 87019690_2921633601247144_3653257817147572224_o.jpg
teaser: Life Kinetik – Infoabend Karlheinz Kösling, Life Kinetik Trainer Montag, 9.
  März, 18 bis 19 Uhr Eintritt frei Erbach, KaffCafé, Auf der Wühre 13 Nr. 2
isCrawled: true
---
Life Kinetik – Infoabend
Karlheinz Kösling, Life Kinetik Trainer
Montag, 9. März, 18 bis 19 Uhr
Eintritt frei
Erbach, KaffCafé, Auf der Wühre 13
Nr. 20F 1505030

Jeder von uns nutzt seine 100 Milliarden Gehirnzellen, die wir seit Geburt haben, anders, aber keiner schöpft die riesigen Möglichkeiten auch nur annähernd aus. Durch spaßige visuelle und koordinative Aufgaben für Ihren Körper, wird Ihr Gehirn gezwungen, neue Verbindungen zwischen den Gehirnzellen zu schaffen. Je mehr dieser Verbindungen bestehen, desto höher ist die Leistungsfähigkeit des Gehirns. Kinder werden kreativer, Schüler/innen konzentrierter, Sportler/innen leistungsfähiger, Berufstätige stressresistenter und Senior/innen aufnahmefähiger und geschickter im Umgang mit Gefahrensituationen.

An diesem Infoabend erhalten Sie einen Überblick über die Inhalte des Life Kinetik Kurses 20F1505308