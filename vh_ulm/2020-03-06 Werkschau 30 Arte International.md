---
id: "182726709731657"
title: "Werkschau 30: Arte International"
start: 2020-03-06 18:00
end: 2020-03-06 21:00
locationName: Ulmer Volkshochschule
address: Kornhausplatz 5, 89073 Ulm
link: https://www.facebook.com/events/182726709731657/
teaser: Werkschau 30 Arte International – »Woher - Wohin« Ausstellungseröffnung –
  Kunst aus den Kursen  Leitung Paul Ganzenmiller Freitag, 6. März, 18 Uhr Ein
isCrawled: true
---
Werkschau 30
Arte International – »Woher - Wohin«
Ausstellungseröffnung – Kunst aus den Kursen 
Leitung Paul Ganzenmiller
Freitag, 6. März, 18 Uhr
Eintritt frei
EinsteinHaus, Atelier
Nr. 20F 0102011

Diese Werkschau präsentiert Arbeiten von Teilnehmer/innen aus dem Kurs »Arte International« unter der Leitung von Paul Ganzenmiller. »Woher - Wohin« ist das Thema dem sich der Kurs unter verschiedenen Aspekten nähern. Von der künstlerischen Technik standen drei Druckformen im Fokus. Zum einen Arbeiten mit Druckschablonen, inspiriert von »banksy«, kritisch, politisch oder persönlich, aber immer im heute. Zum zweiten, kleinere Linoldrucke als Grüße aus der Heimat. Und zum dritten collagierte Monotypien (also ein-Mal-Drucke), die den Weg der einzelnen Teilnehmer/innen beschreiben, das »Woher« und »Wohin«. Woher komme ich, wohin gehe ich? Künstlerisch, farblich, politisch oder auch geographisch. Immer geht es um innere oder äußere Reisen und Migrationen.