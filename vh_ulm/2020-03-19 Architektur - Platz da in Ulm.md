---
id: "543323976389731"
title: Architektur - Platz da in Ulm
start: 2020-03-19 20:00
end: 2020-03-19 23:00
locationName: Ulmer Volkshochschule
address: Kornhausplatz 5, 89073 Ulm
link: https://www.facebook.com/events/543323976389731/
image: 87043216_2921650774578760_7771150827078549504_o.jpg
teaser: vh Architektur - Platz da in Ulm! Öffentlicher Raum aus Sicht des
  Baubürgermeisters Tim von Winning Donnerstag, 19. März, 20 Uhr Eintritt EUR
  8,00/6,0
isCrawled: true
---
vh Architektur - Platz da in Ulm!
Öffentlicher Raum aus Sicht des Baubürgermeisters
Tim von Winning
Donnerstag, 19. März, 20 Uhr
Eintritt EUR 8,00/6,00
Zertifizierte Fortbildung AKBW
EinsteinHaus, Club Orange
Nr. 20F 0101301

Wie steht es um den Öffentlichen Raum? Wieviel Freiraum bleibt zwischen Kommerz und Event?
Der Baubürgermeister Tim von Winning wird in seinem Vortrag einen genaueren Blick auf dieses alte aber immer wieder aktuelle Thema werfen und mit dem Publikum diskutieren. Dabei wird er sowohl auf Herausforderungen und Interessenskonflikte in der Nutzung und Aneignung eingehen, als auch über Projekte und Planungen mit besonderem Fokus auf öffentlichem Raum sprechen.