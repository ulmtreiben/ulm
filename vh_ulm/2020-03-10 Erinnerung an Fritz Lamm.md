---
id: "230721314756368"
title: Erinnerung an Fritz Lamm
start: 2020-03-10 19:00
end: 2020-03-10 22:00
locationName: Ulmer Volkshochschule
address: Kornhausplatz 5, 89073 Ulm
link: https://www.facebook.com/events/230721314756368/
image: 87047958_2921636311246873_743771856154132480_n.jpg
teaser: Erinnerung an Fritz Lamm Heinz Kopp In Zusammenarbeit mit den Freidenkerinnen
  & Freidenkern Ulm/Neu-Ulm und den Naturfreunden Ulm Dienstag, 10. März,
isCrawled: true
---
Erinnerung an Fritz Lamm
Heinz Kopp
In Zusammenarbeit mit den Freidenkerinnen & Freidenkern Ulm/Neu-Ulm und den Naturfreunden Ulm
Dienstag, 10. März, 19 Uhr
Eintritt EUR 6,00
EinsteinHaus, Club Orange
Nr. 20F 0108110

Wie sonst nur wenige verkörperte Fritz Lamm Kontinuität und Erfahrungen der Arbeiterbewegung und zugleich Aufgeschlossenheit für neue gesellschaftliche Fragen, auf die Menschen neue Antworten finden mussten.
Fritz Lamm (1911–1977) hat nach 1948 unermüdlich für den Wiederaufbau der Arbeiterbewegung gewirkt: als Gewerkschafter, Betriebsrat, Funktionär der Naturfreunde, Mitglied der SPD (1931 und 1961 aus der Partei ausgeschlossen).
Der Referent wird einen sehr persönlichen Erinnerungsvortrag halten.