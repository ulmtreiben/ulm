---
id: "1300466730312721"
title: Der Schneesee und seine Fee
start: 2021-02-06 10:00
end: 2021-02-07 14:00
address: kontiki Kinder- & Jugendkunstschule
link: https://www.facebook.com/events/1300466730312721/
image: 123476551_3608596032550894_2293428851127094742_n.jpg
teaser: "Der Schneesee und seine Fee Dozentin: Sarah Percoco  Es war einmal ein See,
  der war immer voll Schnee, darum nannten ihn alle Leute nur Schneesee. Dor"
isCrawled: true
---
Der Schneesee und seine Fee
Dozentin: Sarah Percoco

Es war einmal ein See, der war immer voll Schnee, darum nannten ihn alle Leute nur Schneesee. Dort wuchs Klee, der Schneeseeklee, und darin äßte ein Reh, welches von einer anmutigen Fee geliebt wurde. Na, könnt ihr euch schon denken, wie diese heißen mag?

Die Kinder bekommen die Geschichte der Schneeseefee in gemütlicher Atmosphäre erzählt. Die vorkommenden Reime und Alliterationen regen zu Sprachspielen an, die wir im Kinderkreis gemeinsam zu lustigen Geschichten weiterstricken. Wir experimentieren mit Farbe, malen mit Zuckerkreide, modellieren mit Ton und bringen somit unsere eigene Vorstellungen die Geschichte der Fee zum Ausdruck. Außerdem werden wir versuchen, die Erzählung in Bewegung und theatralisch umzusetzen und zum Abschluss ein kleines Schneeseefeen- Theater spielen.

Einladung für die Familien zur Vorführung unseres Geschaffenen am Sonntag um 13:00 Uhr bis Ende.

Datum | Uhrzeit
1 Wochenende (10,67 Unterrichtsstunden)
Samstag, 6. Februar, 10:00 - 14:00 Uhr
Sonntag, 7. Februar, 10:00 - 14:00 Uhr
Termine
Ort
Ulm, kontiki, Malatelier

Kurs-Nummer
20H0380211

Zielgruppe
5 bis 8 Jahre

Beratung zum Kursinhalt
Mirtan Teichmüller 
Tel. 0731 1530-29 
teichmueller@kontiki.vh-ulm.de

Plätze
6 - 8 Teilnehmer/innen 
 noch freie Plätze

Preis
62,50 € (einschl. Material)