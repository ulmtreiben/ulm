---
id: "1027182431089666"
title: "Online-Veranstaltung: Reichsbürger. Die unterschätzte Gefahr"
start: 2020-11-26 18:00
link: https://www.facebook.com/events/1027182431089666/
image: 125924479_3648784911865339_1612318745047775247_n.jpg
teaser: "Online-Veranstaltung: Reichsbürger. Die unterschätzte Gefahr Dozent: Andreas
  Speit In Zusammenarbeit mit dem Bündnis gegen Rechts Ulm/Neu-Ulm  In Deut"
isCrawled: true
---
Online-Veranstaltung: Reichsbürger. Die unterschätzte Gefahr
Dozent: Andreas Speit
In Zusammenarbeit mit dem Bündnis gegen Rechts Ulm/Neu-Ulm

In Deutschland werden mehr als 12000 Menschen zu den sogenannten Reichsbürgern gerechnet. Für sie ist die Bundesrepublik kein souveränes Staatsgebilde, sondern bestehen die Deutschen Reiche aus der Zeit vor 1945 fort. Gegenwärtig würden fremde Mächte im Hintergrund die Fäden ziehen. Manche von ihnen gründen auch eigene Reiche, wie etwa der König von Deutschland in Wittenberg, stellen eigene Pässe und Führerscheine aus.

Viele erkennen die deutschen Behörden nicht an, verweigern Bußgeldzahlungen und Steuern. Lange Zeit hielt der deutsche Staat die Angehörigen dieser Szene für Spinner und tat sie als ungefährlich ab bis im Oktober 2016 ein Polizist in Franken von einem Reichsbürger erschossen wurde.

Der Vortrag als online-Veranstaltung auf der Plattform Zoom durchgeführt. Nach erfolgter Anmeldung erhalten Sie vor Kursbeginn die Einwahldaten per E-Mail übermittelt.

Datum | Uhrzeit
Termin: Donnerstag, 26.11.2020 | 18:00 Uhr
Ort
Online-Veranstaltung

Kurs-Nummer
20H0109306

Informationen zur Veranstaltung
Dr. Markus Stadtrecher 
Tel. 0731 1530-24 
stadtrecher@vh-ulm.de

Plätze
5 - 99 Teilnehmer/innen 
 noch freie Plätze

Preis
Eintritt frei