---
id: "3045629592165718"
title: Heimat ist ein Raum aus Zeit
start: 2020-03-11 18:00
end: 2020-03-11 21:00
locationName: Mephisto Ulm
address: Rosengasse 15, 89073 Ulm
link: https://www.facebook.com/events/3045629592165718/
image: 87018366_2921638491246655_8248575307188011008_o.jpg
teaser: Heimat ist ein Raum aus Zeit Dokumentarfilm von Thomas Heise Deutschland 2019,
  218 Minuten In Zusammenarbeit mit dem Mephisto-Kino Ulm Mittwoch, 11. M
isCrawled: true
---
Heimat ist ein Raum aus Zeit
Dokumentarfilm von Thomas Heise
Deutschland 2019, 218 Minuten
In Zusammenarbeit mit dem Mephisto-Kino Ulm
Mittwoch, 11. März, 18 Uhr
Eintritt EUR 8,50/5,00
Ulm, Mephisto-Kino, Rosengasse 15
Nr. 20F 0200001

In diesem monumentalen Dokumentarfilm folgt sein Macher Thomas Heise den Spuren seiner eigenen, zerrissenen Familie über das ausgehende 19. Jahrhundert hinweg bis hinein ins 20. Jahrhundert. Es geht um Menschen, die einst zufällig zueinander fanden, nur um einander wieder zu verlieren. Es geht um Sprechen und Schweigen. Erste Liebe und verschwundenes Glück. Väter, Mütter, Söhne, Brüder, Affären, Verletzung und Glück in wechselnden Landschaften, die verschiedene, einander durchwuchernde Spuren von Zeiten in sich tragen.
Der Film wurde vielfach ausgezeichnet – unter anderem mit dem Deutschen Dokumentarfilmpreis 2019.