---
id: "570752563619110"
title: "„Wir ziehen in den Frieden“: Grundlagen, Ansätze und Herausforderungen der
  Friedenspädagogik"
start: 2020-09-09 19:00
locationName: Einsteinhaus Ulm
address: Kornhausplatz 5, D-89073  Germany, 89073 Ulm
link: https://www.facebook.com/events/570752563619110/
teaser: „Wir ziehen in den Frieden“ ist der Titel eines Songs der Rock-Ikone Udo
  Lindenberg. Das Motto ist nicht nur eine Aufforderung und Aufmunterung, sonde
isCrawled: true
---
„Wir ziehen in den Frieden“ ist der Titel eines Songs der Rock-Ikone Udo Lindenberg. Das Motto ist nicht nur eine Aufforderung und Aufmunterung, sondern in der praktischen Umsetzung eine äußerst verantwortungsvolle Aufgabe. „In den Frieden ziehen“ erfordert Fähigkeiten, Kompetenzen und einen langen Atem. Hier ist auch die Friedenspädagogik gefordert, gerade in Zeiten von Hatespeech, Polarisierung und Coronakrise. Im Vortrag werden Grundlagen, Ansätze und Herausforderungen einer aktuellen Friedenspädagogik skizziert und zur Diskussion gestellt.
Referent: Uli Jäger (Berghof Foundation, Bereich Friedenspädagogik & Globales
Lernen; Honorarprofessor an der Universität Tübingen)
EinsteinHaus, Kornhausplatz 5, 89073 Ulm
Eintritt frei
Veranstalter: Verein für Friedensarbeit, Ulmer Volkshochschule und Gewerkschaft Erziehung und Wissenschaft, Kreisverband Alb-Donau/Ulm