---
id: "332641745297034"
title: Geschichten in den Brühlwiesen
start: 2021-09-30 18:00
end: 2021-09-30 20:00
address: Erbach, Spielplatz Brühlwiese
link: https://www.facebook.com/events/332641745297034/
image: 243357587_4538928132851008_256576746827777374_n.jpg
isCrawled: true
---

Geschichten in den Brühlwiesen
Tine Mehls, Geschichtenerzählerin, Naturpädagogin

An diesem Spätsommerabend nimmt Tine Mehls Sie mit in ferne Märchenwelten oder auch auf die Wiese von nebenan. Erleben Sie bewusst das Summen und Zirpen, staunen Sie über die Vielfalt der Pflänzchen am Wegesrand und lauschen entspannt den Märchen und Geschichten aus aller Welt, die Tine Mehls für diesen Abend zusammengestellt hat.

Donnerstag, 30. September, 18 Uhr
Eintritt 15,00 €
Anmeldung erforderlich
Treffpunkt: Erbach, Spielplatz Brühlwiese
Nr. 21H1505026

Anmeldung unter: 
https://www.vh-ulm.de/vh-programm/kurs-finder/detail/kurs/geschichten-in-den-bruehlwiesen/21H1505026