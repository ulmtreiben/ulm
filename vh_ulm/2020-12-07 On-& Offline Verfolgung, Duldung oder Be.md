---
id: "403170257763302"
title: "On-& Offline: Verfolgung, Duldung oder Benachteiligung? Situation der
  Christen in China und anderen Ländern"
start: 2020-12-07 20:00
link: https://www.facebook.com/events/403170257763302/
image: 129958274_3690648727678957_2340618332616090031_n.jpg
teaser: Online (mit Zoom)- und Präsenz-Veranstaltung Verfolgung, Duldung oder
  Benachteiligung? Situation der Christen in China und anderen Ländern In
  Zusammen
isCrawled: true
---
Online (mit Zoom)- und Präsenz-Veranstaltung
Verfolgung, Duldung oder Benachteiligung? Situation der Christen in China und anderen Ländern In Zusammenarbeit mit der Evangelischen und Katholischen Kirchengemeinde

Dozenten: Klaus Rieth, N. N.
Christen können nicht in allen Ländern der Erde frei leben. Es gibt Situationen, in denen sie verfolgt werden. In anderen Ländern gar mit dem Tod bedroht werden. Wieder andere Länder benachteiligen Christen im Schulwesen, bei der Ausbildung oder im Berufsleben und bei der Karriere. Wo sind die Grenzen?
Wie können wir mit solchen Verfolgungssituationen unserer christlichen Schwestern und Brüder umgehen?
Klaus Rieth berichtet aus verschiedenen Ländern, in denen es Christen nicht leicht haben. Und er informiert auch darüber, wie Kirchen helfen können, um die Situation der Menschen vor Ort zu verbessern.

Wenn Sie an der Veranstaltung online teilnehmen möchten, bitten wir um Ihre Anmeldung mit Ihrer E-Mail-Adresse telefonisch unter 0731 1530-20 oder alle@vh-ulm.de. Sie werden dann eine Stunde vorher mit einem Link zur Veranstaltung mit der Vidoeplattform Zoom eingeladen.

Bitte beachten Sie auch den Datenschutz:
https://www.vh-ulm.de/datenschutz
https://www.vh-ulm.de/datenschutz/datenschutzhinweise-fuer-online-kurse

Datum | Uhrzeit
Termin: Montag, 07.12.2020 | 20:00 Uhr
Ort
EinsteinHaus, Club Orange

Kurs-Nummer
20H0101525

Informationen zur Veranstaltung
Dr. Christoph Hantel 
Tel. 0731 1530-17 
hantel@vh-ulm.de

Plätze
1 - 50 Teilnehmer*innen 
 Eine Anmeldung ist nicht nötig.

Preis
Eintritt 6,00 €