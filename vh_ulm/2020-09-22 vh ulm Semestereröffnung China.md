---
id: "322836459129285"
title: "vh ulm Semestereröffnung: China"
start: 2020-09-22 18:30
end: 2020-09-22 22:00
locationName: Ulmer Volkshochschule
address: Kornhausplatz 5, 89073 Ulm
link: https://www.facebook.com/events/322836459129285/
teaser: Semestereröffnung Dienstag, 22. September, 18:30 Uhr Eintritt frei
  EinsteinHaus, Club Orange Nr. 20H0101000  ab 18:30 Uhr, 2.OG Foyer
  Sektempfang  Beg
isCrawled: true
---
Semestereröffnung
Dienstag, 22. September, 18:30 Uhr
Eintritt frei
EinsteinHaus, Club Orange
Nr. 20H0101000

ab 18:30 Uhr, 2.OG Foyer
Sektempfang

Begrüßung Dr. Christoph Hantel

Ausstellungseröffnung
Auf den Spuren den Uiguren, einer muslimischen Minderheit in China
Einführung Laurence Grangien, Fotografin

Laurence Grangien reiste mit dem Zug von Peking in die chinesische autonome Region Xinjiang, die Heimat der turksprachigen, muslimischen Uiguren. Die erste Etappe führte in die Stadt Pingyao, die 1997 von der UNESCO in die Liste des Weltkulturerbes aufgenommen wurde, da sie als besonders gut erhaltene Altstadt der Ying- und Ming-Dynastie gilt. Weiter ging es durch Xi’an, die einst Ausgangspunkt der historischen Seidenstraße nach Lanzhou war und in die Stadt Linxia (die Stadt der 100 Moscheen) sowie in die alte Oasenstadt Dunhuang mit ihren »Singenden Dünen« am Rand der Wüste Gobi.  In der Provinz Xinjiang, das Gebiet der Uiguren, erlebte Laurence Grangien dann eine Kultur, die vielen Menschen im Westen bis heute fremd ist.

19:45 Uhr, 1. OG Foyer
Leckeres chinesisches Fingerfood

ab 20 Uhr, Club Orange
Chinesische Musik