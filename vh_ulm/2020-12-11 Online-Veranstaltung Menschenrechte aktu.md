---
id: "379158569855158"
title: "Online-Veranstaltung: Menschenrechte aktuell"
start: 2020-12-11 18:00
link: https://www.facebook.com/events/379158569855158/
image: 130307255_3704911566252673_6005689808686424828_n.jpg
teaser: "Online-Veranstaltung: Menschenrechte aktuell Dozent: Urs M. Fiechtner In
  Zusammenarbeit mit dem Dokumentationszentrum Oberer Kuhberg  Am 10. Dezember"
isCrawled: true
---
Online-Veranstaltung: Menschenrechte aktuell
Dozent: Urs M. Fiechtner
In Zusammenarbeit mit dem Dokumentationszentrum Oberer Kuhberg

Am 10. Dezember ist der Internationale Tag der Menschenrechte – zu diesem Anlass bieten wir am folgenden Tag einen Überblick über die weltweite Lage der Menschenrechte heute. Was hat sich in jüngster Zeit in diesem Bereich getan, vor allem wenn eine weltweite Pandemie alles andere in den Hintergrund zu drängen scheint?

Der Vortrag als online-Veranstaltung auf der Plattform Zoom durchgeführt. Nach erfolgter Anmeldung erhalten Sie vor Kursbeginn die Einwahldaten per E-Mail übermittelt.

Datum | Uhrzeit
Termin: Freitag, 11.12.2020 | 18:00 Uhr
Ort
Online-Veranstaltung

Kurs-Nummer
20H0109309

Informationen zur Veranstaltung
Dr. Markus Stadtrecher 
Tel. 0731 1530-24 
stadtrecher@vh-ulm.de

Plätze
5 - 99 Teilnehmer*innen 
 noch freie Plätze

Preis
Eintritt frei