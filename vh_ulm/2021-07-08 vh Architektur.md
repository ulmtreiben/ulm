---
id: "211640757494675"
title: vh Architektur
start: 2021-07-08 20:00
address: Ulmer Volkshochschule
link: https://www.facebook.com/events/211640757494675/
image: 208692119_4283417848402039_6284880365977176506_n.jpg
isCrawled: true
---
Ulf Glänzer

21F0101305
Eintritt 8,00 /6,00 € bar an der Abendkasse
AIPs und Architekturstudenten*innen haben freien Eintritt

Welche Besonderheiten, Chancen aber auch Hindernisse gibt es, wenn ein deutsches Architekturbüro in China große Bauprojekte umsetzt? Ulf Glänzer vom renommierten Architekturbüro LATZ+PARTNER hat bereits viele chinesische Projekte geleitet und wird an diesem Abend spannende Einblicke geben. Der Schwerpunkt der vorgestellten Projekte liegt auf der Umgestaltung postindustrieller Landschaften für eine neue Nutzung - wie beispielsweise eine ehemalige Glutamat-Fabrik oder eine Schwerindustrieanlage.

vh Architektur - Vortragsreihe
In Zusammenarbeit mit der Architektenkammergruppe Ulm/Alb Donau
Die Vortragsreihe »vh Architektur« beleuchtet das Thema Architektur in all seiner Vielfalt. Renommierte Architekten*innen oder Bauhistoriker*innen geben fachliche Einblicke in Bauprojekte, Architekturgeschichte oder gegenwärtige Diskurse. Der Semesterschwerpunkt spielt bei der Auswahl der eingeladenen Referenten*innen genauso eine Rolle, wie aktuelle Themen der Baukultur, wie beispielsweise Nachhaltigkeit, Stadtentwicklung oder sozialer Wohnungsbau. 

Hier geht´s zur Anmeldung: https://bit.ly/36dGhW8