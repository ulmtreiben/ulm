---
id: "2676743159220256"
title: Artenvielfalt in Gärten und Städten
start: 2020-03-18 19:30
end: 2020-03-18 22:30
address: Unterkirchberg, Rathaus
link: https://www.facebook.com/events/2676743159220256/
image: 87032251_2921649224578915_287119547285110784_o.jpg
teaser: Artenvielfalt in Gärten und Städten Vortrag von Wolfgang Decrusch,
  Diplom-Biologe Mittwoch, 18. März, 19:30 Uhr Eintritt EUR 7,00 Unterkirchberg,
  Rath
isCrawled: true
---
Artenvielfalt in Gärten und Städten
Vortrag von Wolfgang Decrusch, Diplom-Biologe
Mittwoch, 18. März, 19:30 Uhr
Eintritt EUR 7,00
Unterkirchberg, Rathaus, Hauptstr. 49, Kleiner Saal
Nr. 20F 1507004

Insektensterben und Artenschwund sind inzwischen allgegenwärtige Schlagworte. Die Verantwortung wird auf Politik und Landwirtschaft verschoben. Was können aber Städte,  Gemeinden und jede/r einzelne Gartenbesitzer/in dieser rasanten Entwicklung entgegensetzen. Gärten lassen sich mit wenig Aufwand zu vielfältigen Lebensräumen gestalten. Durch Pflanzung von ausgewählten Gehölzen und Kräutern bietet man Insekten, Kleinsäugern und Vögeln verbesserte Lebensbedingungen. Im Vortrag wird an Beispielen gezeigt, welche Pflanzen für unterschiedliche Tiergruppen von Nutzen sein können, welche Strukturen für ein vielfältiges Insektenvorkommen nötig sind. Dabei ist weniger oft mehr und »Unordnung« im Garten führt zu »Ordnung« im Naturkreislauf. Anhand einzelner Tier- und Pflanzengruppen wird die mögliche Artenvielfalt im naturnahen kommunalen und privaten Garten demonstriert.