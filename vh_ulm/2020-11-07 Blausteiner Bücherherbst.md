---
id: "705726226714602"
title: Blausteiner Bücherherbst
start: 2020-11-07 16:00
address: Stadtbücherei Blaustein
link: https://www.facebook.com/events/705726226714602/
image: 122149777_3571613352915829_3528706367281189590_o.jpg
teaser: »Blausteiner Bücherherbst« Christel Freitag, Redakteurin SWR Kultur, Dr.
  Wolfgang Niess, Historiker, Autor, Moderator  Samstag, 7. November, 16 Uhr Ei
isCrawled: true
---
»Blausteiner Bücherherbst«
Christel Freitag, Redakteurin SWR Kultur, Dr. Wolfgang Niess, Historiker, Autor, Moderator

Samstag, 7. November, 16 Uhr
Eintritt 8,00 €
10-25 Teilnehmer/innen
Blaustein, Stadtbücherei, Marktplatz 2
Nr. 20H1504018

In Zusammenarbeit mit der Stadtbücherei Blaustein

Christel Freitag und Dr. Wolfgang Niess  präsentieren auch dieses Semester eine Auswahl empfehlenswerter Neuerscheinungen. Lassen Sie sich bei einem Glas Wein zur Lust auf das Lesen anstecken.

Christel Freitag ist studierte Musik- und Literaturwissenschaftlerin und Redakteurin beim  SWR Kultur in Tübingen. Dr. Wolfgang Niess ist Historiker, Sachbuchautor und war mehr als 35 Jahre als Moderator und Redakteur in den Kulturprogrammen des SDR bzw. SWR tätig.