---
id: "275193120632230"
title: Vortrag zur Ausstellung „Barriere:Zonen – Leben und Überleben mit Behinderung
  weltweit“
start: 2021-09-10 20:00
locationName: Einsteinhaus Ulm
address: Kornhausplatz 5, D-89073  Germany, 89073 Ulm
link: https://www.facebook.com/events/275193120632230/
image: 238260641_6634581366567236_4424937589039277504_n.jpg
isCrawled: true
---
Der Autor der Ausstellung „Barriere:Zonen“, Fotojournalist Till Mayer, berichtet seit vielen Jahren über Menschen mit Behinderung in Konflikten und Kriegen und arbeitet regelmäßig mit der gemeinnützigen Hilfsorganisation Handicap International zusammen. An diesem Abend gibt er uns einen Einblick in seine Arbeit.