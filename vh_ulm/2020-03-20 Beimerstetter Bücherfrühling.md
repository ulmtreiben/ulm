---
id: "149502392752549"
title: Beimerstetter Bücherfrühling
start: 2020-03-20 20:00
end: 2020-03-20 23:00
address: Beimerstetten, Rathaus
link: https://www.facebook.com/events/149502392752549/
image: 87053966_2921654894578348_7784920784287301632_n.jpg
teaser: Beimerstetter Bücherfrühling Thomas Mahr stellt die neuesten
  Buchveröffentlichungen vor Freitag, 20. März, 20 Uhr Eintritt frei
  Beimerstetten, Rathaus
isCrawled: true
---
Beimerstetter Bücherfrühling
Thomas Mahr stellt die neuesten Buchveröffentlichungen vor
Freitag, 20. März, 20 Uhr
Eintritt frei
Beimerstetten, Rathaus, Bürgersaal
Nr. 20F 1502020

Jedes Jahr kommen im Frühjahr massenhaft neue Bücher zur Leipziger Buchmesse auf den Markt. Doch wie kann man hier den Überblick behalten und herausfinden, welche Bücher man lesen sollte? Buchhändler Thomas Mahr hat wirklich lesenswerte Bücher ausgewählt und stellt Ihnen diese auf unterhaltsame Weise vor.