---
id: "2688317318128019"
title: "Agenda Rüstung: Deutschlands Stratgie zur Stärkung der Waffenindustrie"
start: 2021-09-27 19:30
end: 2021-09-27 21:00
address: Einsteinhaus Ulm, Club Orange
link: https://www.facebook.com/events/2688317318128019/
image: 242828013_4523327311077757_3903101564047321112_n.jpg
isCrawled: true
---
Ulmer Friedenswochen

Agenda Rüstung: Deutschlands Stratgie zur Stärkung der Waffenindustrie

Im Rahmen der Ulmer Friedenswochen und in Zusammenarbeit mit dem Verein für Friedensarbeit e. V., der Ulmer Ärzteinitiative / IPPNW und dem Verein Ulmer Weltladen e. V.Seit Jahren erfolgt eine gezielte Stärkung und Subventionierung von Rüstungsunternehmen, indem systematisch immer mehr Gelder über Schattenhaushalte in den Bereich gepumpt, Exporthindernisse abgebaut und Rüstungsprojekte subventioniert werden. Das alles ist Teil einer machtpolitischen Strategie, die sich hinter dem Begriff »Agenda Rüstung« verbirgt und die auch in Ulm ihren Niederschlag findet.


Jürgen Wagner
Montag, 27. September, 19:30 Uhr
Eintritt frei
EinsteinHaus, Club Orange
Nr. 21H0109507

Im Rahmen der Ulmer Friedenswochen und in Zusammenarbeit mit dem Verein für Friedensarbeit e. V., der Ulmer Ärzteinitiative / IPPNW und dem Verein Ulmer Weltladen e. V.