---
id: "199297368072384"
title: Natur im Ulmer Siedlungsraum
start: 2020-04-28 19:30
end: 2020-04-28 22:30
locationName: Ulmer Volkshochschule
address: Kornhausplatz 5, 89073 Ulm
link: https://www.facebook.com/events/199297368072384/
image: 89854856_2974502932626877_636310679191552000_o.jpg
teaser: Natur im Ulmer Siedlungsraum Roland Maier, Naturfreunde Ulm In Zusammenarbeit
  mit den Naturfreunden Ulm e. V. Dienstag, 28. April, 19:30 Uhr Eintritt
isCrawled: true
---
Natur im Ulmer Siedlungsraum
Roland Maier, Naturfreunde Ulm
In Zusammenarbeit mit den Naturfreunden Ulm e. V.
Dienstag, 28. April, 19:30 Uhr
Eintritt frei
EinsteinHaus, Club Orange
Nr. 20F 0101212

Es ist ein Phänomen unserer Zeit, dass Tiere und Pflanzen aus unserer Kulturlandschaft und gar aus fernen Regionen in unseren Siedlungsräumen zunehmend heimisch werden. Dies ist nicht nur dem Klimawandel zuzuschreiben. Neuerdings sind auch der sich verengende Landschaftsraum verbunden mit Nahrungsmangel sowie der weltweite Austausch im Spiel.
Bei Stadtspaziergängen können aufmerksame Bürger zu ihrer Freude eine Menge dessen beobachten, was sich an Artenvielfalt in unserer Stadtgesellschaft etabliert hat. Schwerpunkte solcher Veränderungen stellen beispielsweise unsere Gewässer und alte Gemäuer dar.
Eine ganze Reihe dieser Zuwanderer in unserer Flora und Fauna werden in Bildern vorgestellt.