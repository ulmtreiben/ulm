---
id: "218718700101586"
title: Showbabies
start: 2021-07-10 20:00
address: Ulmer Volkshochschule
link: https://www.facebook.com/events/218718700101586/
image: 213086519_4283440925066398_8056569966462817228_n.jpg
isCrawled: true
---
Wibke Richter

21F0118114
Eintritt 8,00 /6,00 € Abendkasse

Die Nachwuchsgruppe der Showbuddies stellt ihr Können unter Beweis und wird eine Show der noch nie dagewesenen Art zaubern. Improvisationstheater – immer wieder neu, immer wieder anders!

Hier geht´s zur Anmeldung: https://bit.ly/3jO2IJm