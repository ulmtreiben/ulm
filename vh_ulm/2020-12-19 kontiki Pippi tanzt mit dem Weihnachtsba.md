---
id: "376128983699492"
title: "kontiki: Pippi tanzt mit dem Weihnachtsbaum"
start: 2020-12-19 10:00
end: 2020-12-19 14:00
address: kontiki Kinder- & Jugendkunstschule
link: https://www.facebook.com/events/376128983699492/
image: 123499508_3608590929218071_3014616242399394790_n.jpg
teaser: "Pippi tanzt mit dem Weihnachtsbaum Dozentin: Sarah Percoco  Wer kennt sie
  nicht, das rothaarige, frech- fröhliche, starke Mädchen aus der Villa Kunter"
isCrawled: true
---
Pippi tanzt mit dem Weihnachtsbaum
Dozentin: Sarah Percoco

Wer kennt sie nicht, das rothaarige, frech- fröhliche, starke Mädchen aus der Villa Kunterbunt...?

Überall leuchten Weihnachtslichter in den Fenstern. Aber nicht alle Kinder können sich freuen, denn ein paar Kinder sind ganz allein... wir sind gespannt, was Pippi dieses mal wieder mutiges schafft...!

Wir hören die Geschichte von Astrid Lindgren »Pippi feiert Weihnachten«, tonen nebenbei unsere eigene bedeutenste Impulse der Geschichte und treffen vielleicht auf ein paar eigene Weihnachtserlebnisse?

Die Kinder haben die Gelegenheit Assoziationen zu Weihnachten zu thematisieren und zu erzählen was sie bewegt. Über das modellieren mit Ton können wir über unsere Hände gerade Gefühlen freien lauf lassen und dessen Gestalt geben. Ebenso werden wir mit bunten Farben und Zuckerkreide tätig werden und die Geschichte von Pippi und/ oder unsere Eigene zum Ausdruck bringen. Zum Ausklang gibt es noch eine leckere weihnachtliche Teerunde.

Datum | Uhrzeit
1 Halbtagsseminar (5,33 Unterrichtsstunden)
Samstag, 19. Dezember, 10:00 - 14:00 Uhr
Ort
Ulm, kontiki, Ästhetische Frühbildung

Kurs-Nummer
20H0380212

Zielgruppe
5 bis 8 Jahre

Beratung zum Kursinhalt
Mirtan Teichmüller 
Tel. 0731 1530-29 
teichmueller@kontiki.vh-ulm.de

Plätze
6 - 9 Teilnehmer/innen 
 noch freie Plätze

Preis
36,00 € (einschl. Material)