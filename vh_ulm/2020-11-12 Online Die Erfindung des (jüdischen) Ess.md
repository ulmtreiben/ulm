---
id: "437667657220655"
title: "Online: Die Erfindung des (jüdischen) Essens"
start: 2020-11-12 19:30
link: https://www.facebook.com/events/437667657220655/
image: 124467299_3628167727260391_1089539093031676614_o.jpg
teaser: "Online-Veranstaltung: Die Erfindung des (jüdischen) Essens Popkultur,
  Ernährung, Identität  Eine Veranstaltung aus der Reihe vhs.wissen live  Warum
  wu"
isCrawled: true
---
Online-Veranstaltung: Die Erfindung des (jüdischen) Essens
Popkultur, Ernährung, Identität

Eine Veranstaltung aus der Reihe vhs.wissen live

Warum wurden in der Corona-Krise so viele Konserven, Mehl und Zucker gekauft – obwohl die Versorgungssicherheit nie gefährdet war? Offenbar weisen bestimmte Nahrungsmittel über ihren Nährwert hinaus auch kulturelle Werte auf: Essen kann solide wirken und Sicherheit suggerieren – oder zum »Soul Food« werden. Der französische Theoretiker Roland Barthes nannte die Ernährung ein »System der Kommunikation, ein Korpus von Bildern, eine Gebrauchsanordnung, ein System der Situationen und Verhaltensweisen«. Für Barthes ist das Essen ein Zeichen, das sich lesen lässt. Der Vortrag erläutert in einem ersten Schritt solche und andere theoretischen Annäherungen an das allgegenwärtige »Kulturthema Essen« (Alois Wierlacher). In einem zweiten Schritt werden wir danach fragen, warum jemand eine bestimmte Speise als »jüdisch« wahrnimmt: Was ist an Pastrami, Bagels und gefillte Fisch jüdisch? Dabei werden wir auf popkulturelle Kontexte, aber auch auf Gegenwartsliteratur eingehen.

PD Dr. Caspar Battegay ist Lehrbeauftragter am Fachbereich Deutsche Sprach- und Literaturwissenschaft an der Universität Basel. Seit 2015 ist er Mitglied der Jungen Akademie

Nach erfolgter Anmeldung erhalten Sie im Vorfeld der Veranstaltung eine E-Mail mit den Zugangsdaten.

Datum | Uhrzeit
Termin: Donnerstag, 12.11.2020 | 19:30 Uhr
Ort
Online-Veranstaltung

Kurs-Nummer
20H0109318

Informationen zur Veranstaltung
Dr. Markus Stadtrecher 
Tel. 0731 1530-24 
stadtrecher@vh-ulm.de

Plätze
0 - 50 Teilnehmer/innen 
 noch freie Plätze

Preis
Eintritt frei