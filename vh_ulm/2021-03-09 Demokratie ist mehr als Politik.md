---
id: "154761996479725"
title: Demokratie ist mehr als Politik
start: 2021-03-09 20:00
link: https://www.facebook.com/events/154761996479725/
image: 157027905_3927641207313040_1197254561285589804_o.jpg
teaser: Dr. Martin Mäntele  21F0101202 Eintritt 6,00€  Bereits die Gründung der
  Hochschule für Gestaltung Ulm war ein politischer Akt. Ihr antifaschistischer
isCrawled: true
---
Dr. Martin Mäntele

21F0101202
Eintritt 6,00€

Bereits die Gründung der Hochschule für Gestaltung Ulm war ein politischer Akt. Ihr antifaschistischer Impuls richtete sich gegen reaktionäre Tendenzen, die wenige Jahre nach Kriegsende unübersehbar zunahmen. In dem Vortrag soll es einmal nicht um die politische Haltung oder den Einfluss der Politik auf die Geschicke der Hochschule gehen. Vielmehr stellt der Vortrag Beispiele vor, die zeigen, wie eine politisch motivierte Verleumdungskampagne oder ausgewählte Gesetzgebungen direkten oder indirekten Einfluss auf die Gestaltung nehmen konnten. Die Auswahl reicht vom Hochschulgebäude (Entwurf Max Bill) bis hin zu Verkaufsautomaten. Zugleich stellt sich die Frage wieweit Gestaltung durch demokratische Verfahren bestimmt sein kann.
Hier geht´s zur Anmeldung: https://bit.ly/2MNIjGs