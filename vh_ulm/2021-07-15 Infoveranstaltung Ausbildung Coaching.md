---
id: "564271778074000"
title: "Infoveranstaltung: Ausbildung Coaching"
start: 2021-07-15 18:00
address: Ulmer Volkshochschule
link: https://www.facebook.com/events/564271778074000/
image: 212757982_4286680494742441_7432212136910745127_n.jpg
isCrawled: true
---
In den vergangenen Jahren hat Coaching in Unternehmen mehr und mehr an Bedeutung gewonnen und ist mittlerweile eine anerkannte Disziplin bei Veränderung und Entwicklungsprozessen und wird als wichtige Zusatzqualifikation von Führungskräften im Umgang mit Mitarbeitern angesehen. Unternehmen wie Führungskräfte schätzen besonders die Effektivität, die Zielorientierung, den kurzen Zeiteinsatz, aber auch den persönlichen und individuellen Rahmen von Coaching. Unternehmen, die Coaching einsetzen, haben nachweislich bessere Ergebnisse und Beziehungen.

Zielgruppen
Menschen, die selbst als Coach arbeiten wollen – Führungskräfte, die Coaching als Zusatzqualifikation einsetzen wollen – Personalentwickler – Trainer und Berater – Menschen, die anderen zum Erfolg verhelfen wollen – Menschen, die in sozialen Bereichen arbeiten.

Inhalt
– Grundprinzipien im Coaching
– Beziehungsebenen
– Techniken, Tools und Methoden
– Ziele und individuelle Werte
– Motivation und Erfolgsfaktoren
– Glaubenssysteme und mentale Modelle
– Emotionale Zustände
– Konstruktiver Umgang mit Konflikten, Widerständen und Misserfolg
– Persönliche Fähigkeiten, Talente und Ressourcen
– Individueller Stil und Erfolgsstrategie

Es werden alle notwendigen Kenntnisse vermittelt, Techniken demonstriert und anschließend trainiert, um Coaching erfolgreich einsetzen zu können. Ziel des Trainings ist, den eigenen individuellen Stil als Coach oder Führungskraft zu erkennen und zu entwickeln.

Zertifizierung
Die Zertifizierung erfolgt nach dem internationalen Standard für Coaching-Ausbildungen.
Voraussetzungen sind:
– Teilnahme an allen Trainingstagen
– Coaching-Projekt während des Trainings mit einem Partner/einer Partnerin
– Schriftliche Abschlussprüfung
– Coaching-Demonstration am Ende des Trainings
– Akzeptanz der ethischen Grundprinzipien der IICD

Leitung
Manfred Förderer, international zertifizierter Coach, Trainer:
Danish Coaching Institute/Kopenhagen, 
Kefann Coaching Institut/Warschau, 
Biz Point, Coaching e Formação/Lissabon 
in Deutschland, Dänemark, Polen, Spanien und Portugal

Gebühr
1.450,00 € (einschl. Begleitmaterial, Getränke, Snacks)
zahlbar in 2 monatlichen Raten à 725,00 € möglich

21H1350004
Hier geht´s zur Anmeldung: https://bit.ly/36llyQ8
