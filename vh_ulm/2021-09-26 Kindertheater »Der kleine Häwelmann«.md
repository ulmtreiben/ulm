---
id: "5049218505093772"
title: Kindertheater »Der kleine Häwelmann«
start: 2021-09-26 15:00
end: 2021-09-26 17:00
locationName: Pfleghofhalle
address: Kirchgasse 9, 89129 Langenau
link: https://www.facebook.com/events/5049218505093772/
image: 242618835_4523269654416856_4885923769385793247_n.jpg
isCrawled: true
---
ab 4 Jahre
Kindertheater »Der kleine Häwelmann«

Was Theodor mag, ist Papierflieger basteln und Fußball spielen. Was er gar nicht mag, ist Schwimmen und ... 
ins Bett gehen. Doch in dieser Nacht geschieht etwas Besonderes. Zum Schlüsselloch hinaus segelt Theodor mit seinem Bett durch die Stadt, den Wald und in den Himmel. Mehr, mehr, mehr! Bis zum Sonnenaufgang ... 
Ein winzig großes Figurenspiel für aufgeweckte Zuschauer/innen ab 4 Jahren und ein Stück über Neugier, Forschergeist und die eigenen Grenzen.


In Zusammenarbeit mit der StadtBücherei Langenau


Sonntag, 26. September, 15 Uhr
Eintritt 6,00 €
Langenau, Pfleghofsaal
Nr. 21H1508020
Karten nur im Vorverkauf bei der StadtBücherei Langenau