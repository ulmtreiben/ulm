---
id: "324466278899501"
title: Die Neuerfindung der Diktatur
start: 2021-02-02 20:00
link: https://www.facebook.com/events/324466278899501/
image: 143828754_3831650070245488_6968928569811736055_o.jpg
teaser: Kai Strittmatter Eintritt 6,00 € 20H0101216  China ist Boomland, längst einer
  der Motoren der Weltwirtschaft. Innenpolitisch blieb das Land dabei imme
isCrawled: true
---
Kai Strittmatter
Eintritt 6,00 €
20H0101216

China ist Boomland, längst einer der Motoren der Weltwirtschaft. Innenpolitisch blieb das Land dabei immer autoritär, außenpolitisch zurückhaltend. Doch unter Xi Jinping, dem mächtigsten Staats- und Parteichef seit Mao, erfindet sich der autoritäre Staat neu, in offener Konkurrenz zum Westen. China marschiert nun selbstbewusst in die Welt, gleichzeitig gewährt sich sein System ein Update mit den Instrumenten des 21. Jahrhunderts: Peking setzt auf Big Data und künstliche Intelligenz wie keine zweite Regierung. Die Partei glaubt, den perfektesten Überwachungsstaat schaffen zu können, den die Erde je gesehen hat. Das Ziel ist die Kontrolle der KP über alle und alles. Kai Strittmatter beschreibt die Mechanismen der Diktatur, er zeigt, wie Xi Jinping China umbaut und was diese Entwicklung für uns bedeutet.
Hier gehts zur Anmeldung:
https://www.vh-ulm.de/vh-programm/kurs-finder?kfs_stichwort_schlagwort=20H0101216&clearallkatfilter=true