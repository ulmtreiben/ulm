---
id: "236867121351072"
title: "Yoga-Workshop: Mit Yoga zurück zur Langsamkeit"
start: 2021-01-29 15:00
link: https://www.facebook.com/events/236867121351072/
image: 142749441_3825921240818371_2894235006062665752_o.jpg
teaser: »Wenn du es eilig hast, gehe langsam« -- dieses Zitat stammt von dem
  Zeitmanagement-Experten L. J. Seiwert. Immer mehr Aufgaben müssen in kürzerer
  Zei
isCrawled: true
---
»Wenn du es eilig hast, gehe langsam« -- dieses Zitat stammt von dem Zeitmanagement-Experten L. J. Seiwert. Immer mehr Aufgaben müssen in kürzerer Zeit absolviert werden. Viele von uns hetzen nur noch durch den Tag. Am Abend fühlen wir uns erschöpft und energielos. Wer und was ist uns im Leben wirklich wichtig und nachhaltig von Bedeutung? Yoga hilft uns innezuhalten, zu entschleunigen, in der Ruhe wieder Kraft zu finden und uns zu begegnen. Dafür üben wir passende Asanas, Atemübungen und Entspannung.
Dieser Kurs wird über die Online-Plattform »Zoom« angeboten. Zur Teilnahme via »Zoom« benötigen Sie eine E-Mail-Adresse (bitte bei der Anmeldung angeben) und ein Endgerät mit Kamera und Mikrofon (Rechner, Laptop, Tablet, Smartphone ...). 

Kursnummer: 20H1023460
Hier gehts zur Anmeldung: https://www.vh-ulm.de/vh-programm/kurs-finder?kfs_stichwort_schlagwort=20H1023460&clearallkatfilter=true