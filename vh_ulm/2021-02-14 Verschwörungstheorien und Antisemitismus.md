---
id: "211001597376139"
title: Verschwörungstheorien und Antisemitismus
start: 2021-02-14 19:00
link: https://www.facebook.com/events/211001597376139/
image: 147901368_2785968368386725_1704136324097325080_o.jpg
teaser: "Online-Veranstaltung mit Dr. Karamba Diaby, MdB In Kooperation mit der
  Stiftung Erinnerung Ulm  Sein Bürgerbüro ist nur wenige hundert Meter
  entfernt:"
isCrawled: true
---
Online-Veranstaltung mit Dr. Karamba Diaby, MdB
In Kooperation mit der Stiftung Erinnerung Ulm

Sein Bürgerbüro ist nur wenige hundert Meter entfernt: Karamba Diaby hat als Bundestagsabgeordneter für Halle den Terroranschlag auf die dortige Synagoge sehr nah miterlebt. Der Referent analysiert die Zusammenhänge zwischen rechtsextremer Gewalt, der Verbreitung von Verschwörungstheorien und dem erstarkenden Antisemitismus auch in der Mitte der Gesellschaft. 

Diaby, seit 2010 Mitglied im Bündnis "Halle gegen Rechts – Bündnis für Zivilcourage" und Mitglied des Kuratoriums der Bundeszentrale für politische Bildung, richtet den Blick nicht zuletzt auf Gegenstrategien aus der Zivilgesellschaft.

Der Vortrag wird als Online-Veranstaltung auf der Plattform Zoom durchgeführt. Nach erfolgter Anmeldung über die Webseite der vh Ulm werden die Einwahldaten per E-Mail mitgeteilt.

Hier gehts zur Anmeldung:
https://www.vh-ulm.de/vh-programm/kurs-finder/detail/kurs/online-veranstaltung-verschwoerungstheorien-und-antisemitismus/21F0101203