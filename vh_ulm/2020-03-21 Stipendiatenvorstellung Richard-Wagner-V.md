---
id: "491684888442934"
title: Stipendiatenvorstellung Richard-Wagner-Verband
start: 2020-03-21 14:00
end: 2020-03-21 17:00
locationName: Ulmer Volkshochschule
address: Kornhausplatz 5, 89073 Ulm
link: https://www.facebook.com/events/491684888442934/
image: 86864368_2921656874578150_6037814326922838016_o.jpg
teaser: Stipendiatenvorstellung und Mitgliederversammlung In Zusammenarbeit mit
  dem  Richard-Wagner-Verband Samstag, 21. März, 14 Uhr Eintritt frei EinsteinHa
isCrawled: true
---
Stipendiatenvorstellung und Mitgliederversammlung
In Zusammenarbeit mit dem 
Richard-Wagner-Verband
Samstag, 21. März, 14 Uhr
Eintritt frei
EinsteinHaus, Club Orange
Nr. 20F 0120190

An diesem Tag stellen sich de  Bayreuth-Stipendiaten 2020 vor: Valérie Leoff (Sopran), Luke Sinclair (Tenor) und Christian Stolz (Dramaturg). Das Publikum kann sie sowohl im persönlichen Gespräch als auch durch Darbietungen ihrer Kunst kennenlernen. Anschließend findet die Mitgliederversammlung statt.
