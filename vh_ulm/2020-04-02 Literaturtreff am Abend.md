---
id: "1250029028521689"
title: Literaturtreff am Abend
start: 2020-04-02 18:00
end: 2020-04-02 21:00
address: Blaustein, Stadtbücherei
link: https://www.facebook.com/events/1250029028521689/
image: 89815116_2974424195968084_7742541190926434304_o.jpg
teaser: Literaturtreff am Abend Petra Harakaidis-Rohwer, Bibliothekarin In
  Zusammenarbeit mit der Stadtbücherei Blaustein Donnerstag, 2. April, 18 Uhr
  Eintrit
isCrawled: true
---
Literaturtreff am Abend
Petra Harakaidis-Rohwer, Bibliothekarin
In Zusammenarbeit mit der Stadtbücherei Blaustein
Donnerstag, 2. April, 18 Uhr
Eintritt frei
Blaustein, Stadtbücherei, Marktplatz 2
Nr. 20F 1504050

Haben Sie schon einmal den Wunsch verspürt, sich über Ihre/n Lieblingsautor/in zu unterhalten? Möchten Sie wissen, welche Neuerscheinungen in Ihrer Lieblingsbuchsparte zu erwarten sind? Suchen Sie Literaturtipps für Buchgeschenke? Oder haben Sie spezielle Bücherwünsche? In entspannter Atmosphäre können Sie sich mit der Bibliothekarin Frau Harakaidis-Rohwer über Literatur austauschen.