---
id: "1362304480646046"
title: Online-Kurs Kochen mit Jule
start: 2020-06-12 18:00
end: 2020-06-12 20:00
link: https://www.facebook.com/events/1362304480646046/
image: 97088880_3118999088177260_103123865584009216_o.jpg
teaser: "Online-Kurs Kochen mit Jule Dozentin: Julia Hörsch  In besonderen Zeiten,
  muss man besondere Wege gehen. Gerade jetzt in der Zeit von Corona, können w"
isCrawled: true
---
Online-Kurs Kochen mit Jule
Dozentin: Julia Hörsch

In besonderen Zeiten, muss man besondere Wege gehen. Gerade jetzt in der Zeit von Corona, können wir uns die viele Zeit zuhause etwas verschönern! Das geht bestens mit Kochen! Wir wollen gemeinsam ein leckeres Ossobucco mit Gnocchi, Selleriepüree, Schmorgemüse, Zwiebelmarmelade und Gremolata kochen. Einen Hauptgang, den man gleich danach mit seinen Liebsten genießen kann. Das ganze von zuhause aus, über Zoom, gemeinsam und mit vielen Tipps und Tricks!

Bitte für Webinar bereitstellen: Küche, Rezepte (die Sie bereits vorab erhalten), die eingekauften Lebensmittel und am besten schon einen gedeckten Tisch.

Dieser Kurs wird über die Online-Plattform »Zoom« angeboten. Zur Teilnahme benötigen Sie eine E-Mail-Adresse, ein Endgerät mit Kamera und Mikrofon (Rechner, Laptop, Tablet, Smartphone ...) und eine Internetverbindung.
Sie erhalten für den Kurstermin von uns eine Einladung per E-Mail für den Unterricht per »Zoom«.

Rechtliche Bestimmungen und Datenschutz Zoom:
https://zoom.us/docs/de-de/privacy-and-legal.html

Datum | Uhrzeit
1 Abend (2,67 Unterrichtsstunden) | Freitag
Beginn: 12.06.2020 | 18:00 bis 20:00 Uhr
Ort
Webinar

Kurs-Nummer
20F1508694

Beratung zum Kursinhalt
Monya Jabri 
Tel. 0731 1530-12 
jabri@vh-ulm.de

Plätze
4 - 8 Teilnehmer/innen 
 noch freie Plätze

Preis
15,00 €