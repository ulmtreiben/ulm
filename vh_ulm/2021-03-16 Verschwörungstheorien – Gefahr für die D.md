---
id: "870261547097187"
title: Verschwörungstheorien – Gefahr für die Demokratie?
start: 2021-03-16 20:00
link: https://www.facebook.com/events/870261547097187/
image: 159307066_3940228209387673_2272503647346246654_o.jpg
teaser: Prof. Dr. Michael Butter  21F0101204 Eintritt frei  Das Coronavirus ist völlig
  harmlos, aber dunkle Eliten schüren Panik, um uns unsere Grundrechte zu
isCrawled: true
---
Prof. Dr. Michael Butter

21F0101204
Eintritt frei

Das Coronavirus ist völlig harmlos, aber dunkle Eliten schüren Panik, um uns unsere Grundrechte zu stehlen. Hinter den Terroranschlägen von 9/11 steckte nicht Osama Bin Laden – sondern die USA selbst. Die Bundesrepublik ist kein Land, sondern eine Firma und die Bevölkerung Europas wird im Zuge eines »Großen Austauschs« gezielt islamisiert. Viele Menschen versuchen, Ereignisse oder Entwicklungen auf Verschwörungen zurückzuführen. Doch was genau ist eigentlich eine Verschwörungstheorie – und was nicht? Weshalb glauben Menschen an solche Behauptungen und gibt es heute mehr davon als früher? Haben das Internet und jetzt die Corona-Pandemie zu einer sprunghaften Zunahme geführt? Warum sind Verschwörungstheorien gerade in den populistischen Bewegungen der Gegenwart so populär? Und wann stellen sie eine Gefahr für die Demokratie dar?
Hier geht´s zur Anmeldung: https://bit.ly/38omx3t