---
id: "539720840542876"
title: Philosophie am Abend – Autonome Kunst
start: 2021-07-16 20:00
address: Ulmer Volkshochschule
link: https://www.facebook.com/events/539720840542876/
image: 215870491_4291762297567594_6516343745417560648_n.jpg
isCrawled: true
---
Dr. Martin Böhnisch

21F0710006
Eintritt frei

Lange Zeit galt in westlichen Demokratien die Kunst als edelste Verwirklichung des Freiheitsgedankens. Dieser Hoffnung, von Kant einst wieder erweckt, von den Romantikern in Szene gesetzt und den Dekonstruktivisten wieder in alle Einzelteile zerlegt, geht es mittlerweile an den Kragen: Nach kometenhaftem Aufstieg scheint ihr Autonomieverständnis am Boden der Realität angekommen zu sein. Das, was sie nur selten wollte - realistisch und daher frei von Moral sein - nun ist sie es geworden. Ästhetik und Moral gehen für eine Rezeptionsgemeinschaft wieder problemlos Hand in Hand, die - ganz im gesellschaftlichen Trend liegend - kaum noch zwischen Hergestelltem und Hersteller zu unterscheiden weiß. Wie konnte es dazu kommen? Wer trägt hier die Verantwortung? Diesen und anderen Fragen soll im Vortrag nachgegangen werden. Eingeladen sind alle an der Philosophie Interessierte und auch die, die es noch werden wollen.

Hier geht´s zur Anmeldung: https://bit.ly/36oM6QH