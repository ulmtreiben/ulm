---
id: "196865025008324"
title: Vorstellung und Diskussion der Studie Mobiles Baden-Württemberg
start: 2020-03-31 20:00
end: 2020-03-31 23:00
locationName: Ulmer Volkshochschule
address: Kornhausplatz 5, 89073 Ulm
link: https://www.facebook.com/events/196865025008324/
image: 87037682_2921671767909994_1858915219267387392_o.jpg
teaser: Nachhaltige Mobilität macht Platz für die Neugestaltung unserer Städte –
  Vorstellung und Diskussion der Studie »Mobiles Baden-Württemberg« Klaus Amler
isCrawled: true
---
Nachhaltige Mobilität macht Platz für die Neugestaltung unserer Städte – Vorstellung und Diskussion der Studie »Mobiles Baden-Württemberg«
Klaus Amler
Eine Veranstaltung im Rahmen des Projekts »Zukunftsdialog: Nachhaltige Mobilität« der Baden-Württemberg Stiftung und in Zusammenarbeit mit der Eine-Welt-Regionalpromotorin
Dienstag, 31. März, 20 Uhr
Eintritt frei
EinsteinHaus, Club Orange
Nr. 20F 0101210

Sie ist in aller Munde: die »Mobilitätswende«. Ökologische Faktoren wie Klimawandel, Luftverschmutzung (Feinstaub) und ökonomische oder technische Faktoren wie Digitalisierung, und autonomes Fahren sind die Treiber der teils schon sichtbaren Veränderungen von gesellschaftlichen Debatten, realer Mobilität und Märkten.
Die Baden-Württemberg Stiftung hat dazu die Studie »Mobiles Baden-Württemberg« erstellen lassen, deren Grundlage die Einhaltung des Pariser Weltklimaabkommens war. Auf ihrer Grundlage stellen wir uns die Fragen, wie nachhaltige Mobilität aussehen kann – an Land, auf See und in der Luft, für Alle und Viele, für Menschen und Güter. Und was bedeutet dies für die Potentiale zur Neugestaltung städtischer Räume?