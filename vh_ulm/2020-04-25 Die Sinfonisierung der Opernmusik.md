---
id: "192050902231573"
title: Die Sinfonisierung der Opernmusik
start: 2020-04-25 16:00
end: 2020-04-25 19:00
locationName: Ulmer Volkshochschule
address: Kornhausplatz 5, 89073 Ulm
link: https://www.facebook.com/events/192050902231573/
image: 89763846_2974498062627364_8218326178939797504_o.jpg
teaser: Die Sinfonisierung der Opernmusik Vortrag von Roland Bauer In Zusammenarbeit
  mit dem  Richard-Wagner-Verband Samstag, 25. April, 16 Uhr Eintritt EUR 1
isCrawled: true
---
Die Sinfonisierung der Opernmusik
Vortrag von Roland Bauer
In Zusammenarbeit mit dem 
Richard-Wagner-Verband
Samstag, 25. April, 16 Uhr
Eintritt EUR 10,00
EinsteinHaus, Club Orange
Nr. 20F 0120191

Von Beethovens sinfonischem Werk zu Richard Wagners Tristan und Isolde - Die Sinfonisierung der Opernmusik. Die Sinfonie wurde von Beethoven zur Vollendung geführt. Daher belegte Wagner sie mit dem Urteil vom »Ende«  und sah die Weiterentwicklung nur über eine Sinfonisierung der Opernmusik im Musikdrama als möglich. Mit »Tristan und Isolde« hat Wagner diesen Entwicklungsprozess selbst zu einem absoluten Höhepunkt - vielleicht sogar zu einem erneuten »Ende« - geführt. Von Beethoven ausgehend soll die Sinfonisierung der Opernmusik in »Tristan und Isolde« dargestellt werden, wobei der Halbtonschritt dafür exemplarisch steht. Roland Bauer referiert seit vielen Jahren in unterschiedlichsten Einrichtungen, bei Richard Wagner Verbänden und während der Festspiele im Kunstmusem Bayreuth. Der Schwerpunkt seiner Vortragstätigkeit bildet das gesamte Werk Wagners und dabei insbesondere die musiktheoretische Analyse.