---
id: "1064327344024548"
title: "Online: Zwangswelten. Zu neuen Forschungsansätzen der NS-Zwangsarbeit"
start: 2020-11-11 19:00
link: https://www.facebook.com/events/1064327344024548/
image: 122438219_3571631126247385_7025917953193840753_o.jpg
teaser: "Zwangswelten. Zu neuen Forschungsansätzen der NS-Zwangsarbeit  Dozent: Dr.
  Katarzyna Woniak In Zusammenarbeit mit dem Dokumentationszentrum Oberer Kuh"
isCrawled: true
---
Zwangswelten. Zu neuen Forschungsansätzen der NS-Zwangsarbeit

Dozent: Dr. Katarzyna Woniak
In Zusammenarbeit mit dem Dokumentationszentrum Oberer Kuhberg

Dr. Katarzyna Woniak stellt im Rahmen der DZOK-Veranstaltungsreihe »Neue Forschung vorgestellt« die Ergebnisse ihrer Habilitationsschrift zur NS-Zwangsarbeit vor. Obwohl bereits zahlreiche lokalhistorische und unternehmensgeschichtliche Studien zu diesem Thema vorliegen, präsentiert die Historikerin, die in Polen und Deutschland promovierte und am Institut für Geschichte, Theorie und Ethik der Medizin der Universität Ulm arbeitet, einen neuen Forschungsansatz: Sie skizziert die Methodik und Ergebnisse ihrer Emotions- und Alltagsgeschichte, bei der die Denkwelten und Gefühle, die Erlebnisse und Erfahrungen der Betroffenen im Mittelpunkt stehen. Im Anschluss gibt es Gelegenheit zur Diskussion.

Anmeldung unter info@dzok-ulm.de oder 0731 21312.

Datum | Uhrzeit
Termin: Mittwoch, 11.11.2020 | 19:00 Uhr

Kurs-Nummer
20H0108114

Informationen zur Veranstaltung
Dr. Markus Stadtrecher 
Tel. 0731 1530-24 
stadtrecher@vh-ulm.de

Plätze
 Anmeldung unter info@dzok-ulm.de oder 0731 21312

Preis
Eintritt frei