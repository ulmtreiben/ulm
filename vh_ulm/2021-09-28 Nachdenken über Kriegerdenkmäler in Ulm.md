---
id: "908317223101057"
title: Nachdenken über Kriegerdenkmäler in Ulm
start: 2021-09-28 18:00
end: 2021-09-28 21:00
address: Einsteinhaus Ulm, Club Orange
link: https://www.facebook.com/events/908317223101057/
image: 242728784_4525844144159407_8770732359565906172_n.jpg
isCrawled: true
---
Ulm Denk(t)mal
Gesprächsrunde

An vielen Orten findet man sie, die Kriegertafeln und -denkmäler – auch in Ulm. Sie symbolisieren stark den »Zeitgeist« ihrer Entstehung. Da wird das »ehrende Gedenken an die gefallenen Kameraden« beschworen oder auch die tradierte Formel vom »Opfertod für das Vaterland« benutzt.
Und heute? Ist es nicht überfällig, sie zu Friedensmahnmalen einer lebendigen Erinnerungskultur werden zu lassen? Wir wollen dies – in praktischer Absicht – für Ulm diskutieren.


Dienstag, 28. September, 18 Uhr
Eintritt frei
EinsteinHaus, Club Orange
Nr. 21H0109510

Im Rahmen der Ulmer Friedenswochen und in Kooperation mit der Gruppe Friedensbewegt Ulm

