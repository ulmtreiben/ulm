---
id: "298944844427550"
title: "Online: Gerd Müller oder »Wie das große Geld in den Fußball kam«"
start: 2020-05-18 19:30
end: 2020-05-18 22:30
link: https://www.facebook.com/events/298944844427550/
image: 96298396_3111259155617920_1534474935915773952_o.jpg
teaser: "Online-Veranstaltung: Gerd Müller oder »Wie das große Geld in den Fußball
  kam« Hans Woller Montag, 18. Mai, 19:30 Uhr Eintritt frei Webinar Nr. 20F010"
isCrawled: true
---
Online-Veranstaltung: Gerd Müller oder »Wie das große Geld in den Fußball kam«
Hans Woller
Montag, 18. Mai, 19:30 Uhr
Eintritt frei
Webinar
Nr. 20F0109311

Der Historiker Dr. Hans Woller schildert in diesem Vortrag die Etappen dieser ungewöhnlichen Karriere - aus kritischer Distanz und zugleich voller Empathie. Die Geschichte des FC Bayern München ist dabei stets präsent. Müllers Verein etablierte sich in den 1960er und 1970er Jahren an der Spitze des europäischen Fußballs, bewegte sich aber immer am Rande des finanziellen Ruins. Wie die Insolvenz abgewendet werden konnte, welche zwielichtige Rolle dabei die bayerische Staatsregierung und die CSU spielten und in welchem Maße Superstars wie Müller oder Beckenbauer von diesen Machenschaften profitierten, ist bisher noch nie so eindringlich dargestellt worden. Fußballgeschichte wird hier zur Zeitgeschichte, die damit eine neue wissenschaftliche Dimension gewinnt.

Der Referent war bis zu seiner Pensionierung als Chefredakteur am Institut für Zeitgeschichte in München. Er gilt als einer der besten Kenner der jüngeren italienischen Geschichte.

Nach erfolgter Anmeldung erhalten Sie im Vorfeld der Veranstaltung eine E-Mail mit den Zugangsdaten.
