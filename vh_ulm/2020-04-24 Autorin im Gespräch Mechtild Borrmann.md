---
id: "534366747279484"
title: "Autorin im Gespräch: Mechtild Borrmann"
start: 2020-04-24 20:30
end: 2020-04-24 23:30
locationName: Hugendubel Buchhandlung Ulm Hirschstraße
address: Hirschstraße 26 - 30, 89073 Ulm
link: https://www.facebook.com/events/534366747279484/
image: 89636501_2974495242627646_5762238294519709696_o.jpg
teaser: "Autorin im Gespräch: Mechtild Borrmann Moderation und Gesprächsleitung: Dr.
  Wolfgang Niess In Zusammenarbeit mit Hugendubel, unterstützt durch die Spa"
isCrawled: true
---
Autorin im Gespräch: Mechtild Borrmann
Moderation und Gesprächsleitung:
Dr. Wolfgang Niess
In Zusammenarbeit mit Hugendubel, unterstützt durch die Sparkasse Ulm
Freitag, 24. April, 20:30 Uhr
Eintritt EUR 10,00/8,00
Ulm, Hugendubel, Hirschstraße 26-30
Nr. 20F 0118033

Die Bestsellerautorin Mechtild Borrmann gehört mit ihrer eigenen soghaft-präzisen Sprache zu den beliebtesten deutschen Autoren/innen. Ihr Buch »Wer das Schweigen bricht« wurde mit dem deutschen Krimi Preis ausgezeichnet und stand monatelang auf der Bestseller-Liste. Ihr aktueller Roman »Grenzgänger« nimmt das Schicksal von Heimkindern in der deutschen Nachkriegszeit in den Blick. Das Gespräch mit Dr. Wolfgang Niess gibt Gelegenheit die Schriftstellerin persönlich zu erleben und Einblicke in ihre Biografie und ihren Schreibprozess zu bekommen. Dabei wird die preisgekrönte Autorin auch Einsicht in ihre Inspirationsquellen, Schreibflauten und literarische Vorbilder geben.

Foto: Thomas Gebauer