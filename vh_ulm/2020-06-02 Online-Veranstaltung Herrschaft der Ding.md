---
id: "566331830967723"
title: "Online-Veranstaltung: Herrschaft der Dinge."
start: 2020-06-02 19:30
end: 2020-06-02 22:30
link: https://www.facebook.com/events/566331830967723/
image: 97215718_3111294458947723_6232691080635088896_o.jpg
teaser: "Online-Veranstaltung: Herrschaft der Dinge. Eine neue Geschichte des Konsums
  und ein Blick auf morgen Frank Trentmann Dienstag, 2. Juni, 19:30 Uhr Ein"
isCrawled: true
---
Online-Veranstaltung: Herrschaft der Dinge. Eine neue Geschichte des Konsums und ein Blick auf morgen
Frank Trentmann
Dienstag, 2. Juni, 19:30 Uhr
Eintritt frei
Webinar
Nr. 20F0109313

Frank Trentmann, Historiker am Birkbeck College der Universität London, erzählt in Herrschaft der Dinge erstmals umfassend die faszinierende Geschichte des Konsums. Von der italienischen Renaissance bis hin zur globalisierten Wirtschaft der Gegenwart entwirft er eine weltumspannende Alltags- und Wirtschaftsgeschichte, die eine Fülle von Wissen bietet, den Blick aber ebenso auf die Herausforderungen der Zukunft lenkt angesichts von Überfluss, Klimawandel und Turbokapitalismus. Im Vortrag und der danach folgenden Diskussion werden konventionelle Sichtweisen von Shopping hinterfragt und der Blick auf die Rolle des Staates und die der Zivilgesellschaft für den Aufstieg der Konsumenten und unsere heutigen Lebensweisen gerichtet. Eine Geschichte des Konsums liefert einen unverzichtbaren Beitrag zu den wichtigsten politischen und wirtschaftlichen Debatten unserer Zeit.

Frank Trentmann ist Professor für Geschichte am Birkbeck College der Universität London. Er studierte und promovierte an der Harvard University und lehrte anschließend in Princeton. 2017 erhielt er von der Alexander von Humboldt-Stiftung den Humboldt-Forschungspreis.

Nach erfolgter Anmeldung erhalten Sie im Vorfeld der Veranstaltung eine E-Mail mit den Zugangsdaten.
