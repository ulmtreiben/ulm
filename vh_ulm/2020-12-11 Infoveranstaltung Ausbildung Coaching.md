---
id: "388902322538533"
title: "Infoveranstaltung: Ausbildung Coaching"
start: 2020-12-11 18:00
locationName: Ulmer Volkshochschule
address: Kornhausplatz 5, 89073 Ulm
link: https://www.facebook.com/events/388902322538533/
image: 128751133_3685602914850205_459059247651954452_n.jpg
teaser: "Infoveranstaltung: Ausbildung Coaching Dozent: Manfred Förderer In den
  vergangenen Jahren hat Coaching in Unternehmen mehr und mehr an Bedeutung
  gewon"
isCrawled: true
---
Infoveranstaltung: Ausbildung Coaching
Dozent: Manfred Förderer
In den vergangenen Jahren hat Coaching in Unternehmen mehr und mehr an Bedeutung gewonnen und ist mittlerweile eine anerkannte Disziplin bei Veränderung und Entwicklungsprozessen und wird als wichtige Zusatzqualifikation von Führungskräften im Umgang mit Mitarbeitern angesehen. Unternehmen wie Führungskräfte schätzen besonders die Effektivität, die Zielorientierung, den kurzen Zeiteinsatz, aber auch den persönlichen und individuellen Rahmen von Coaching. Unternehmen, die Coaching einsetzen, haben nachweislich bessere Ergebnisse und Beziehungen.

Zielgruppen
Menschen, die selbst als Coach arbeiten wollen – Führungskräfte, die Coaching als Zusatzqualifikation einsetzen wollen – Personalentwickler – Trainer und Berater – Menschen, die anderen zum Erfolg verhelfen wollen – Menschen, die in sozialen Bereichen arbeiten.

Inhalt
– Grundprinzipien im Coaching
– Beziehungsebenen
– Techniken, Tools und Methoden
– Ziele und individuelle Werte
– Motivation und Erfolgsfaktoren
– Glaubenssysteme und mentale Modelle
– Emotionale Zustände
– Konstruktiver Umgang mit Konflikten, Widerständen und Misserfolg
– Persönliche Fähigkeiten, Talente und Ressourcen
– Individueller Stil und Erfolgsstrategie

Es werden alle notwendigen Kenntnisse vermittelt, Techniken demonstriert und anschließend trainiert, um Coaching erfolgreich einsetzen zu können. Ziel des Trainings ist, den eigenen individuellen Stil als Coach oder Führungskraft zu erkennen und zu entwickeln.

Zertifizierung
Die Zertifizierung erfolgt nach dem internationalen Standard für Coaching-Ausbildungen.
Voraussetzungen sind:
– Teilnahme an allen Trainingstagen
– Coaching-Projekt während des Trainings mit einem Partner/einer Partnerin
– Schriftliche Abschlussprüfung
– Coaching-Demonstration am Ende des Trainings
– Akzeptanz der ethischen Grundprinzipien der IICD

Leitung
Manfred Förderer, international zertifizierter Coach, Trainer:
Danish Coaching Institute/Kopenhagen,
Kefann Coaching Institut/Warschau,
Biz Point, Coaching e Formação/Lissabon
in Deutschland, Dänemark, Polen, Spanien und Portugal

Gebühr
EUR 1.450,00 (einschl. Begleitmaterial, Getränke, Snacks)
zahlbar in 2 monatlichen Raten à EUR 725,00 möglich

Informationsabende
Donnerstag, 6. Februar, 18 bis circa 19:30 Uhr

Weitere Informationen
Weitere Informationen oder eine ausführliche Beratung erhalten Sie bei Norbert Herre, Telefon 0731 1530-16, herre@vh-ulm.de.

Datum | Uhrzeit
Termin: Freitag, 11.12.2020 | 18:00 Uhr
Ort
EinsteinHaus, Unterer Saal

Kurs-Nummer
20H1350002

Informationen zur Veranstaltung
Norbert Herre 
Tel. 0731 1530-16 
herre@vh-ulm.de

Plätze
0 - 10 Teilnehmer*innen 
 noch freie Plätze

Preis
Eintritt frei

Link zur Ausbildung:
https://www.vh-ulm.de/vh-programm/kurs-finder/detail/kurs/zertifizierte-coaching-ausbildung/21F1420001