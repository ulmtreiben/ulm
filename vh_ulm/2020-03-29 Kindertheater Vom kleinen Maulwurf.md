---
id: "225423838487461"
title: "Kindertheater: Vom kleinen Maulwurf"
start: 2020-03-29 15:00
end: 2020-03-29 17:00
locationName: Kulturstadel Hüttisheim
address: Hauptstraße 33, 89185 Hüttisheim
link: https://www.facebook.com/events/225423838487461/
image: 86973597_2921670451243459_3441714030526332928_o.jpg
teaser: ab 4 Jahre Kindertheater »Vom kleinen Maulwurf, der wissen wollte, wer ihm auf
  den Kopf gemacht hat« Maren Kaun Figurentheater Sonntag, 29. März, 15 U
isCrawled: true
---
ab 4 Jahre
Kindertheater »Vom kleinen Maulwurf, der wissen wollte, wer ihm auf den Kopf gemacht hat«
Maren Kaun Figurentheater
Sonntag, 29. März, 15 Uhr
Eintritt EUR 6,00
Hüttisheim, Kulturstadel, Hauptstr. 29
Nr. 20F 1506018

Oh nein! Es ist überhaupt nicht in Ordnung, sein Geschäft auf anderer, zumal kleinerer Leute Kopf zu machen. Auch ein Maulwurf ist schließlich jemand. Eine hundsgemeine Ungerechtigkeit! Da hilft nur eins: Auf! Und ihn finden. Die Sache muss geklärt werden. Eine anrüchige Geschichte zwischen Fell- und Federvieh mit feiner Nase in Szene gesetzt für Figuren, Musik und eine Spielerin.