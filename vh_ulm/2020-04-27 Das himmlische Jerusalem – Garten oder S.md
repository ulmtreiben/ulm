---
id: "645080789592002"
title: "Online: Das himmlische Jerusalem – Garten oder Stadt?"
start: 2020-04-27 20:00
end: 2020-04-27 21:30
locationName: Ulmer Volkshochschule
address: Kornhausplatz 5, 89073 Ulm
link: https://www.facebook.com/events/645080789592002/
image: 94222147_3071392352937934_110680834771517440_n.jpg
teaser: Das himmlische Jerusalem – Garten oder Stadt? Dekan Ulrich Kloos Montag, 27.
  April, 20 bis 21:30 Uhr  Die Veranstaltung wird als Videokonferenz mit Zo
isCrawled: true
---
Das himmlische Jerusalem – Garten oder Stadt?
Dekan Ulrich Kloos
Montag, 27. April, 20 bis 21:30 Uhr

Die Veranstaltung wird als Videokonferenz mit Zoom stattfinden. 

Auf der vh-Homepage finden Sie eine Anleitung, wie man Zoom installiert und Kamera und Ton am eigenen Bildschirm freischaltet. Wenn Sie darüberhinaus Fragen hierzu haben, mailen Sie uns bitte bis einen Tag vor der Veranstaltung. Ansonsten erhalten Sie am 27. April um 19.30 Uhr per Mail einen Link auf »Zoom«. 

Fragen und Anmeldung: Henrike Alle, alle@vh-ulm.de

Der katholische Dekan Ulrich Kloos aus Ulm hat in seinem Leben schon manches erlebt, wie den Gottesdienst mit Hunderten von weinenden Menschen vor zehn Jahren nach dem Amoklauf von Winnenden, der sich für ihn wie eine Vertreibung aus dem Paradies anfühlte. Das Paradies ist im alttestamentarischen Verständnis und im Mittelalter oft ein Garten gewesen. Aber es gibt auch die Vorstellung einer Stadt, die für den göttlichen Himmel steht. Wie passen diese unterschiedlichen Vorstellungen zusammen? Sind sie miteinander vereinbar oder lösen sie sich ab? Welches Bild haben wir heute in Zeiten von weltweiter Landflucht und urbanem Leben vom Himmel? Erscheint uns die Natur als göttlicher, friedlicher Ort, als Garten mit Pflanzen und Tieren - oder suchen wir das Heil im Getriebe der Großstadt? Dekan Kloos spannt den Bogen und freut sich auf das Gespräch.