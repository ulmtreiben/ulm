---
id: "169950821123805"
title: Artenvielfalt in Gärten und Städten
start: 2020-03-17 19:30
end: 2020-03-17 22:30
address: Erbach, Stadtbücherei
link: https://www.facebook.com/events/169950821123805/
image: 87280049_2921646171245887_7832466463096045568_o.jpg
teaser: Artenvielfalt in Gärten und Städten Vortrag von Wolfgang Decrusch,
  Diplom-Biologe Dienstag, 17. März, 19:30 Uhr Eintritt EUR 7,00 Erbach,
  Stadtbüchere
isCrawled: true
---
Artenvielfalt in Gärten und Städten
Vortrag von Wolfgang Decrusch, Diplom-Biologe
Dienstag, 17. März, 19:30 Uhr
Eintritt EUR 7,00
Erbach, Stadtbücherei, Erlenbachstr. 17
Nr. 20F 1505004

Insektensterben und Artenschwund sind inzwischen allgegenwärtige Schlagworte. Die Verantwortung wird auf Politik und Landwirtschaft verschoben. Was können aber Städte,  Gemeinden und jede/r einzelne Gartenbesitzer/in dieser rasanten Entwicklung entgegensetzen. Gärten lassen sich mit wenig Aufwand zu vielfältigen Lebensräumen gestalten. Durch Pflanzung von ausgewählten Gehölzen und Kräutern bietet man Insekten, Kleinsäugern und Vögeln verbesserte Lebensbedingungen. Im Vortrag wird an Beispielen gezeigt, welche Pflanzen für unterschiedliche Tiergruppen von Nutzen sein können, welche Strukturen für ein vielfältiges Insektenvorkommen nötig sind. Dabei ist weniger oft mehr und »Unordnung« im Garten führt zu »Ordnung« im Naturkreislauf. Anhand einzelner Tier- und Pflanzengruppen wird die mögliche Artenvielfalt im naturnahen kommunalen und privaten Garten demonstriert.