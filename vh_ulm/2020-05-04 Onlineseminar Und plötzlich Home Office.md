---
id: "2517748998475936"
title: "Onlineseminar: Und plötzlich Home Office"
start: 2020-05-04 18:30
end: 2020-05-04 20:00
locationName: Ulmer Volkshochschule
address: Kornhausplatz 5, 89073 Ulm
link: https://www.facebook.com/events/2517748998475936/
image: 95244999_3085094578234378_1738404527205253120_n.jpg
teaser: "Onlineseminar: Und plötzlich Home Office... Dozent: Bernd
  Müller-Dautenheimer  In den aktuellen Corona Zeiten wird das Thema Home Office
  (Arbeiten von"
isCrawled: true
---
Onlineseminar: Und plötzlich Home Office...
Dozent: Bernd Müller-Dautenheimer

In den aktuellen Corona Zeiten wird das Thema Home Office (Arbeiten von zuhause) immer aktueller. In diesem Kurs wird unser Dozent, mit über 15 Jahren Home Office Erfahrung, verschiedene Tips und Tricks verraten, damit sie im Home Office möglichst stressfrei (und strukturiert) arbeiten können.

Inhalt:
– Was braucht man an Technik um gut in seinem HomeOffice arbeiten zu können: Unterschiede zwischen Telearbeit / HomeOffice und Remote Worker
– Struktur schaffen: Wie sehen aktuell die Prioritäten und Verantwortlichkeiten aus. Welche Rollen wird von wem eingenommen.
– Teamregeln: Kernzeiten im Team, Verfügbarkeiten. Email Kommunikationsregeln und Dokumenten-Managment
– Meetings Regeln festlegen: Tägliche Check-Ins, Wöchentliche Jourfixe. Wie seiht es mit Arbeitsgruppen aus?
– Was mach ich mit meinen Kinder zuhaus?
– Arbeitsorganisation: Tagesroutine, Priorisierung von Arbeiten, Klare Trennung von Arbeit und Freizeit
– Virtuelle Leadership: Alles was ein Vor-Ort-Team braucht ist essenziell in einem virtuellen Team
– Teamleistung
– Konflikmanagement
– Kommunikation: Best Practice
– Tipps für Software zur Zusammenarbeit

Anmeldung erforderlich

Datum | Uhrzeit
1-mal (2 Unterrichtsstunden) | Montag
Beginn: 04.05.2020 | 18:30 bis 20:00 Uhr
Ort
Onlineseminar Beruf

Kurs-Nummer
20F1302161

Beratung zum Kursinhalt
Norbert Herre 
Tel. 0731 1530-16 
herre@vh-ulm.de

Plätze
5 - 10 Teilnehmer/innen 
 noch freie Plätze

Preis
15,00 €

https://www.vh-ulm.de/vh-programm/kurs-finder/detail/kurs/onlineseminar-und-ploetzlich-home-office/20F1302161