---
id: "264477598243672"
title: "Online-Veranstaltung: Philosophie am Abend"
start: 2020-05-28 19:00
link: https://www.facebook.com/events/264477598243672/
image: 100101065_3147835601960275_7898848275032178688_n.jpg
teaser: "Online-Veranstaltung: Philosophie am Abend Die Angst, die Freiheit und das
  Leben. Warum zu Demokratien auch das Irrationale gehört  Dozent: Martin Böh"
isCrawled: true
---
Online-Veranstaltung: Philosophie am Abend
Die Angst, die Freiheit und das Leben. Warum zu Demokratien auch das Irrationale gehört

Dozent: Martin Böhnisch
In Kooperation mit der Aegis Buchhandlung

Sich Ängstigen gehört zum Leben, ist sogar überlebensnotwendig. Manche Angst bereitet uns sogar Vergnügen und ist daher oft das Ergebnis einer freien Entscheidung. Wenn wir nachts durch den Wald gehen, einen Horrorfilm ansehen oder etwas Ekliges essen oder anfassen ... Wir ängstigen uns, wohlwissend, dass diese Angst ein Teil unseres Lebens und unserer Freiheit ist. Darüber hinaus gibt es auch die andere Seite der Angst: die unfreiwilige und daher nur schwer zu kontrollierende und lebensbedrohende Angst. Seit jeher hätten wir sie gerne aus unserem Leben verband und bekämpfen sie mit nur allen erdenkbaren Mitteln: Mit Technik, Gesetzen, Wissenschaft, Kunst und Therapien.

Am Donnerstag, den 28. Mai 2020 soll dem Phänomen der Angst unter einer philosophischen Perspektive nachgegangen, mögliche Zusammenhänge zum Leben und zur Freiheit aufgespürt und Bezüge zur Gegenwart hergestellt werden.

Dieser Kurs wird über die Online-Plattform »Zoom« angeboten. Zur Teilnahme benötigen Sie eine E-Mail-Adresse (bitte bei der Anmeldung angeben!) und ein entsprechendes Endgerät. Sie können auch ohne Kamera und Mikrofon teilnehmen. Hinweise zur Verwendung finden Sie unter https://www.vh-ulm.de/digitale-angebote/anleitungen-online-kurse, zum Datenschutz unter https://www.vh-ulm.de/cms/index.php?id=202.

Nach erfolgter Anmeldung erhalten Sie ca. eine Stunde vor Beginn die Zugangsdaten per E-Mail.

Datum | Uhrzeit
Termin: Donnerstag, 28.05.2020 | 19:00 Uhr
Ort
Webinar

Kurs-Nummer
20F0109308

Informationen zur Veranstaltung
Dr. Markus Stadtrecher 
Tel. 0731 1530-24 
stadtrecher@vh-ulm.de

Plätze
10 - 80 Teilnehmer/innen 
 noch freie Plätze

Preis
Eintritt frei