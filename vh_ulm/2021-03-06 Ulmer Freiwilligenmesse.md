---
id: "1290076921378856"
title: Ulmer Freiwilligenmesse
start: 2021-03-06 11:00
end: 2021-03-12 16:00
link: https://www.facebook.com/events/1290076921378856/
image: 156911661_3927612757315885_4899294406205602892_n.jpg
teaser: "Statt im EinsteinHaus findet die 8. Ulmer Freiwliigenmesse in diesem Jahr in
  digitaler Form statt. Das Programm finden Sie unter: https://www.engagier"
isCrawled: true
---
Statt im EinsteinHaus findet die 8. Ulmer Freiwliigenmesse in diesem Jahr in digitaler Form statt. Das Programm finden Sie unter: https://www.engagiert-in-ulm.de/sites/default/files/fm/DigitaleFreiwilligenmesse2021-Rahmenprogramm.pdf
