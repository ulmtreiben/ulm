---
id: "2455732604687246"
title: »Ach Virginia« – Was vom Leben übrig blieb
start: 2020-04-22 20:00
end: 2020-04-22 23:00
address: Langenau, Pfleghofsaal
link: https://www.facebook.com/events/2455732604687246/
image: 89850871_2974488675961636_1069048081875468288_n.jpg
teaser: »Ach Virginia« – Was vom Leben übrig blieb Ein großer Roman über Virginia
  Woolf Lesung mit Michael Kumpfmüller Mittwoch, 22. April, 20 Uhr Eintritt EU
isCrawled: true
---
»Ach Virginia« – Was vom Leben übrig blieb
Ein großer Roman über Virginia Woolf
Lesung mit Michael Kumpfmüller
Mittwoch, 22. April, 20 Uhr
Eintritt EUR 10,00/6,00
Vorverkauf Buchhandlung Mahr
Langenau, Pfleghofsaal
Nr. 20F 1508018

Wie kaum eine Frau ihrer Zeit steht Virginia Woolf für das Ringen um Eigenständigkeit, um Raum für sich, um eine unverkennbare Stimme. Ihr Leben war überreich an allem – auch an Düsternissen. Michael Kumpfmüller hat einen sprachmächtigen, kühnen Roman über die letzten zehn Tage ihres Lebens geschrieben.
Im März 1941 gerät die berühmte Schriftstellerin in ihre letzte große Krise: Sie hat soeben ein neues Buch beendet, über das kleine Cottage im Süden Englands, das sie mit ihrem Mann Leonard bewohnt, fliegen deutsche Bomber. Sie führt das Leben einer Gefangenen, die nicht weiß, wie und wohin sie ausbrechen soll –  und am Ende entscheidet sie sich für den Fluss. Diese letzten Tage Virginia Woolfs beschwört Michael Kumpfmüller in seinem neuen Roman eindrücklich herauf. Er zeichnet das Bild einer Person, die in Auflösung begriffen scheint und sich auf die Reise in den Innenraum macht, der eine Welt voller Schrecken und eben auch Wunder ist.