---
id: "860642184418999"
title: Teatro International e.V. – back to live
start: 2020-06-25 18:30
end: 2020-06-25 20:30
link: https://www.facebook.com/events/860642184418999/
image: 103954442_3189336087810226_3503878630085311111_n.jpg
teaser: Teatro International e.V. – back to live   Wir haben kleine und größere
  Scheitergeschichten von Menschen in und um Ulm gesammelt. An verschiedenen Sta
isCrawled: true
---
Teatro International e.V. – back to live 

Wir haben kleine und größere Scheitergeschichten von Menschen in und um Ulm gesammelt. An verschiedenen Stationen entlang der Donau - von der Adlerbastei bis zum Metzgerturm - laden wir dich ein, stehenzubleiben und den Geschichten und Gedanken zuzuhören. 

Donnerstag, 25. Juni, Freitag, 26. Juni, jeweils zwischen 18.30 und 20.30 Uhr. 
Kulturnacht, 19. September, 17.30 bis 20 Uhr

umsonst und draußen – kleine Spenden willkommen  

Erzähler/innen: 
Jorge Andreu, Gina Barollo, Biatta Bassirou, Tuba Gözukara, Lisam Diaz Moreira, Ioan Porumbel,  Alena Bihel, Laura Garcia Zabala, Jaume de la O., Henriette Lutter, Simon Kiehne, Maasuom Osspi, Raphaelle Polidor, Oksana Reivita, Lotte Stevens, Daniela Ventuiz, Maria Winter

Konzept/Regie: 
Claudia Schoeppl

Vielen Dank:
	•	Maria Winter für die Unterstützung als Geschichtenerzählerin
	•	Daniela Ventuiz für die Unterstützung bei der Konzipierung und Durchführung 
	•	allen, die uns helfen, dass der Parcours unter den geltenden Hygieneregeln stattfinden kann
	•	allen, die uns eine Geschichte oder ihre Gedanken geschenkt haben

www.teatrointernational.de 
www.facebook.com/teatrointernational/
www.instagram.com/teatrointernational

LOGOS: 
	•	Teatro International
	•	Stadt Ulm
	•	Vh Ulm 
	•	Landesamateurtheaterverband
