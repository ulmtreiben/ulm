---
id: "1076955673156384"
title: Wer wandert, braucht nur, was er tragen kann.
start: 2021-10-07 19:00
end: 2021-10-07 21:00
address: Stadtbücherei Erbach
link: https://www.facebook.com/events/1076955673156384/
image: 243434357_4539804706096684_2332393525921048240_n.jpg
isCrawled: true
---
Wer wandert, braucht nur, was er tragen kann.
Anne Donath

Mit Mitte 40 wagte Anne Donath den Sprung in ein neues Leben. Was für das Umfeld nach Verzicht, Entbehrung und Beschränkung aussah, war für sie die Eintrittskarte in ein weitgehend selbstbestimmtes Leben. Sie wurde als Frau, »die einfach nur lebt«  in DIE ZEIT, in Interwievs, im Radio, in Fernsehreportagen und in Talkshows bekannt. 2006 brachte der Piperverlag ihr Buch heraus, das 10 Jahre später eine erweiterte Neuauflage erlebte. Ihr Erfahrungsbericht lässt uns an der Entwicklung teilhaben, wie sie nun schon mehr als ein Vierteljahrhundert in ihrem Blockhaus auf 16 Quadratmetern lebt, ohne Strom, ohne Auto, in einem wilden Garten, mit großer Leidenschaft für's Reisen, für's Radln und auch, mit nun 73 Jahren, mit  Freude am wilden Zugvogeldasein.
An diesem Abend liest sie aus ihrem Buch, zeigt Bilder und freut sich auf ein lebendiges Gespräch.

In Zusammenarbeit mit der Stadtbücherei Erbach

Donnerstag, 7. Oktober, 19 Uhr
Eintritt 7,00 €
Erbach, Stadtbücherei, Erlenbachstr. 17
Nr. 21H1505006

Anmeldung erwünscht

https://www.vh-ulm.de/vh-programm/kurs-finder/detail/kurs/wer-wandert-braucht-nur-was-er-tragen-kann/21H1505006
