---
name: vh ulm
website: http://www.vh-ulm.de
email: info@vh-ulm.de
scrape:
  source: facebook
  options:
    page_id: UlmerVolkshochschule
---
1946 gegründet, sind wir mittlerweile zur Einrichtung mit dem breitest gefächerten Bildungsangebot und vor allem dem umfassendsten Netzwerk an Kooperationspartnern im kulturellen und gesellschaftlichen Bereich Ulms herangewachsen und schließlich auch zur Einrichtung, die mit ihren Angeboten am weitesten in die Region hinausreicht.
