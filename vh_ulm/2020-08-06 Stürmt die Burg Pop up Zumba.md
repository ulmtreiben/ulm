---
id: "745163689576315"
title: "Stürmt die Burg: Pop up Zumba"
start: 2020-08-06 18:30
end: 2020-08-06 19:30
locationName: Festung Wilhelmsburg Xii
address: Prittwitz Straße, Ulm
link: https://www.facebook.com/events/745163689576315/
teaser: 30.7. / 6.8. / 13.8. / 20.8. / 27.8.  jeweils um 18.30 Uhr  Tanzen, Spaß haben
  und nebenbei den ganzen Körper trainieren; das gibt’s dieses Jahr auch
isCrawled: true
---
30.7. / 6.8. / 13.8. / 20.8. / 27.8. 
jeweils um 18.30 Uhr

Tanzen, Spaß haben und nebenbei den ganzen Körper trainieren; das gibt’s dieses Jahr auch auf der Wilhelmsburg und zwar beim Zumba-Kurs der Ulmer Volkshochschule. Und anschließend?
…natürlich auf einen Drink in die Burgbar No. XII.

Ohne Anmeldung; max. 20 Personen. 
Freiwillige Spende für die Kursleitung erwünscht.
-------- 
Stürmt die Burg 2020
Der Pop up Space auf der Wilhelmsburg geht in diesem Sommer in die dritte Runde, anders als gewohnt, noch wichtiger als sonst! Mit ihrem großzügigen Innenhof bietet die Bundesfestung ausreichend Platz, um auch in diesem Sommer die Burg zu stürmen – mit Abstand versteht sich und mit Fokus auf lokale Kulturakteure aus Ulm und der Region. Jeweils von Donnerstag bis Sonntag erwartet euch ein buntes Kulturangebot. Unter dem Motto #kulturerhalten bespielen in diesem Jahr lokale Künstlerinnen und Künstler den Burginnenhof und bringen Kunst und Kultur zurück auf die Bühne. Von Musik über Tanz bis hin zu Comedy, Literatur und Schauspiel – das Programm ist so vielfältig wie die Ulmer Kulturszene selbst. An den Donnerstagen und Samstagen gestalten Mitglieder des Arbeitskreis Kultur das Bühnenprogramm, die Freitage und Sonntage wurden im Rahmen einer Open-Stage-Ausschreibung an Ulmer Kulturakteure vergeben. Sonntags gibt es Kulturprogramm für die ganze Familie - immer um 14 Uhr einen Programmpunkt speziell für Kinder. 
- - - - - - - -
Burgbar No. XII
Neuer Name, neues Konzept, gewohnte Location und gewohnt tolle Atmosphäre - das ist die Burgbar No. XII. Ab dem 24. Juli dienen zwei umgebaute Container als Corona-konforme Pop up Bar auf der
Wilhelmsburg. Streetfood-Stände sorgen von Donnerstag bis Sonntag für kulinarische Vielfalt. Sonntags erwartet die Gäste ein Frühschoppen mit Weißwurst, am Nachmittag wird das Angebot durch Kuchen und Kaffee vom Coffee Bike erweitert. Öffnungszeiten: Do.-Sa.: 18-24 Uhr;
So.: 10.30-18 Uhr. Weitere Infos unter www.burgbar12.de
- - - - - - - -
Free Shuttle - Der Wilhelmsbus
Von Donnerstag bis Samstag gibt es einen kostenlosen Shuttlebus [halbstündlich 18-24 Uhr], der vom Hans-und-Sophie-Scholl-Platz (Neue Mitte) und einem weiteren Halt an der Bushaltestelle Frauenstraße zur Wilhelmsburg fährt und die Besucherinnen und Besucher anschließend wieder in die Stadt bringt. Wir wollen euch und uns vor einer Infektion schützen – bitte tragt deshalb eine Mund-Nasen-Bedeckung im Bus. An den Sonntagen ist kein Shuttlebus-Verkehr eingerichtet. Aktuelle Informationen gibt es hier: www.die-wilhelmsburg.de
--------
Corona-Hinweis
Zur Sicherheit aller haltet euch bitte an die aktuell gültigen Hygiene- und Abstandsregelungen und bringt euren Mund- und Nasenschutz mit. Wir minimieren die Infektionsgefahr durch Maßnahmen wie Besucherbegrenzung und bitten euch, rücksichtsvoll mit den anderen Gästen umzugehen. Bei Krankheitssymptomen bleibt bitte zuhause. Lasst uns zusammen #kulturerhalten in diesem
besonderen Sommer - wir freuen uns auf euch! Ein Corona-FAQ findet ihr unter www.burgbar12.de/faq
--------
Der Pop up Space wird von der Kulturabteilung der Stadt Ulm koordiniert und von der Brauerei Gold Ochsen unterstützt. Alle Infos unter: www.die-wilhelmsburg.de