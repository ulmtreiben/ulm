---
id: "599781184000208"
title: "Online: Stressbewältigung im Alltag"
start: 2020-08-06 18:45
end: 2020-08-06 20:15
link: https://www.facebook.com/events/599781184000208/
image: 101708092_3168367889907046_7415737331237257216_n.jpg
teaser: "Online: Stressbewältigung im Alltag Dozentin: Gabriele Hagmann Wie fühlt sich
  Stress für mich an? Passt gerade mein Arbeits- und Lebensmodell für mich"
isCrawled: true
---
Online: Stressbewältigung im Alltag
Dozentin: Gabriele Hagmann
Wie fühlt sich Stress für mich an? Passt gerade mein Arbeits- und Lebensmodell für mich? Habe ich meine Berufung gefunden? Wie überwinde ich den Stress und schaffe ein Wohlbefinden für mich und meine Umwelt. Wie fühle ich Freude und Spaß, statt Frust und Stress? Mit höherer Selbstwirksamkeitserwartung gehe ich mit Rückschlägen anders um.

Datum | Uhrzeit
1 Abend (2 Unterrichtsstunden) | Donnerstag
Beginn: 06.08.2020 | 18:45 bis 20:15 Uhr
Ort
Onlineseminar Beruf

Kurs-Nummer
20F1404003

Beratung zum Kursinhalt
Norbert Herre 
Tel. 0731 1530-16 
herre@vh-ulm.de

Plätze
5 - 25 Teilnehmer/innen 
 noch freie Plätze

Preis
15,00 €