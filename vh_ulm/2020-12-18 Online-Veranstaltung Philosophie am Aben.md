---
id: "1800741896743265"
title: "Online-Veranstaltung: Philosophie am Abend: Macht"
start: 2020-12-18 20:00
link: https://www.facebook.com/events/1800741896743265/
image: 130290164_3704914969585666_5486412898480604204_n.jpg
teaser: "Online-Veranstaltung: Philosophie am Abend: Macht Dozent: Dr. Martin Böhnisch
  In Zusammenarbeit mit der Buchhandlung Aegis Ulm  Wer Macht ausübt, sche"
isCrawled: true
---
Online-Veranstaltung: Philosophie am Abend: Macht
Dozent: Dr. Martin Böhnisch
In Zusammenarbeit mit der Buchhandlung Aegis Ulm

Wer Macht ausübt, scheint hinlänglich klar. Diese Erfahrung haben wir bereits als Kinder machen können. Hier mein Wille, dort der Wille des Anderen. Hier ein Sieger, dort ein Verlierer. Was Macht jedoch ist, ist hingegen weniger leicht zu bestimmen. Ist es eine Art Tätigkeit einer Person, eine Gesinnung vieler, eine eigentümliche Kraft oder eine Art des Herrschens? Für den berühmten Soziologen Max Weber war und ist der Begriff der Macht schon immer »amorph« gewesen.

In der Veranstaltung »Philosophie am Abend« wollen wir der Macht etwas auf den Grund gehen. Dazu werden wir uns nicht nur auf der Makroebene bewegen. Auch die Meso- und Mikroebene soll in den Blick genommen werden. Eingeladen sind alle philosophisch Interessierten und die es noch werden wollen.

Der Vortrag als Online-Veranstaltung auf der Plattform Zoom durchgeführt. Nach erfolgter Anmeldung erhalten Sie vor Kursbeginn die Einwahldaten per E-Mail übermittelt.

Datum | Uhrzeit
Termin: Freitag, 18.12.2020 | 20:00 Uhr
Ort
Online-Veranstaltung

Kurs-Nummer
20H0710008

Informationen zur Veranstaltung
Dr. Markus Stadtrecher 
Tel. 0731 1530-24 
stadtrecher@vh-ulm.de

Plätze
5 - 99 Teilnehmer*innen 
 noch freie Plätze

Preis
Eintritt frei