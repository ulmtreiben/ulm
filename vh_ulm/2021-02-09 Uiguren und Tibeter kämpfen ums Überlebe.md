---
id: "542098420101895"
title: Uiguren und Tibeter kämpfen ums Überleben
start: 2021-02-09 20:00
link: https://www.facebook.com/events/542098420101895/
image: 145801700_3845998712143957_4083964330782323113_o.jpg
teaser: Ulrich Delius  Eintritt 6,00 € 20H0101218  Chinas Nationalitätenpolitik steht
  vor einem Scherbenhaufen. Wie soll Europa auf den Völkermord reagieren?
isCrawled: true
---
Ulrich Delius

Eintritt 6,00 €
20H0101218

Chinas Nationalitätenpolitik steht vor einem Scherbenhaufen. Wie soll Europa auf den Völkermord reagieren? Referent Ulrich Delius ist Afrika- und Asienexperte und seit 2017 Direktor der Gesellschaft für bedrohte Völker.
Hier geht's zur Anmeldung:
https://www.vh-ulm.de/vh-programm/kurs-finder?kfs_stichwort_schlagwort=20H0101218&clearallkatfilter=true