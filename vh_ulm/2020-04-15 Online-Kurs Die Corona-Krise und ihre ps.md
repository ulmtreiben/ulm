---
id: "218022076118247"
title: "Online-Kurs: Die Corona-Krise und ihre psychischen Folgen"
start: 2020-04-15 19:00
end: 2020-04-15 20:00
locationName: Ulmer Volkshochschule
address: Kornhausplatz 5, 89073 Ulm
link: https://www.facebook.com/events/218022076118247/
image: 92844902_3041974975879672_8420437504414449664_o.jpg
teaser: "Online-Kurs: Die Corona-Krise und ihre psychischen Folgen  In Zusammenarbeit
  mit der vhs Biberach und der TelefonSeelsorge Ulm/Neu-Ulm  Im Rahmen der"
isCrawled: true
---
Online-Kurs: Die Corona-Krise und ihre psychischen Folgen

In Zusammenarbeit mit der vhs Biberach und der TelefonSeelsorge Ulm/Neu-Ulm

Im Rahmen der Corona-Krise werden vor allem die ökonomischen und politischen Auswirkungen der Gegenmaßnahmen diskutiert. Wie aber geht man mit möglichen negativen Auswirkungen um, beispielsweise Ängsten oder Einsamkeit? Dazu wollen wir mit unsern Experten in Form dieses Webinars an drei Abenden ins Gespräch kommen. Prof. Dr. Harald Traue (Stress- und Emotionsforscher), Dr. Stefan Plöger (Leiter der TelefonSeelsorge Ulm/Neu-Ulm) und Dr. Ulrich Mack (Krankenhausseelsorger und Zenlehrer) leiten je ein Modul der Reihe.

Dieser Kurs wird über die Online-Plattform »Zoom« angeboten. Zur Teilnahme benötigen Sie eine E-Mail-Adresse (bitte bei der Anmeldung angeben!) und ein Endgerät mit Kamera und Mikrofon (Rechner, Laptop, Tablet, Smartphone ...). Während der Sitzungen kann man mit den Kursleitern ins Gespräch kommen, Sie können aber auch ohne Kamera und Mikrofon teilnehmen.

Link Rechtliche Bestimmungen und Datenschutz ZOOM:
https://zoom.us/docs/de-de/privacy-and-legal.html

3 Tage (4 Unterrichtsstunden) | Mittwoch, Donnerstag, Freitag
Beginn: 15.04.2020 | 19:00 bis 20:00 Uhr
Online-Kurs

Kurs-Nummer
20F0721030

Beratung zum Kursinhalt
Dr. Markus Stadtrecher 
Tel. 0731 1530-24 
stadtrecher@vh-ulm.de

Plätze
15 - 30 Teilnehmer/innen 
 noch freie Plätze

Preis
Gebührenfrei