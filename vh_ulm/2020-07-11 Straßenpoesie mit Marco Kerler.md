---
id: "274524013858011"
title: Straßenpoesie mit Marco Kerler
start: 2020-07-11 12:00
end: 2020-07-11 15:00
link: https://www.facebook.com/events/274524013858011/
image: 105542415_3223952537681914_5001110530500193684_o.jpg
teaser: Straßenpoesie mit Marco Kerler  Der öffentliche Raum eignet sich auch zum
  Dichten. Das beweist an diesem Nachmittag Marco Kerler, der mit Schreibmasch
isCrawled: true
---
Straßenpoesie mit Marco Kerler

Der öffentliche Raum eignet sich auch zum Dichten. Das beweist an diesem Nachmittag Marco Kerler, der mit Schreibmaschine ausgerüstet auf dem Hans-und-Sophie-Scholl-Platz sitzen wird. 

Wollten Sie schon immer mal eine Muse für einen Dichter sein? Dann kommen Sie vorbei und erzählen dem Ulmer Lyriker ein bisschen von sich und lassen sich überraschen wie Sie damit zu Poesie inspirieren können. Erhalten Sie ihr persönliches Gedicht! 

Eine kleine Spende für das Gedicht wäre schön, ist aber ist kein Muss. 

Die Aktion findet von 12-15 Uhr bei jedem Wetter statt.

Datum | Uhrzeit
Termin: Samstag, 11.07.2020 | 12:00 Uhr
Ort
Ulm, Hans-und-Sophie-Scholl-Platz

Kurs-Nummer
20F0101405

Informationen zur Veranstaltung
Natascha Bruns 
Tel. 0731 1530-34 
nataschabruns@vh-ulm.de

Plätze
 Keine Online-Anmeldung möglich/nötig

Preis
Gebührenfrei, Spenden erbeten

(Foto: Susanne Wasserlechner)