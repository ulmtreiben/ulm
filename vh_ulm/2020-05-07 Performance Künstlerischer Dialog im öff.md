---
id: "2509647235751662"
title: "Abgesagt wegen Corona-Pandemie: Künstlerischer Dialog"
start: 2020-05-07 18:00
end: 2020-05-07 19:00
address: Ulm Zentrum
link: https://www.facebook.com/events/2509647235751662/
image: 79372462_2778410518902787_3894190656226066432_o.jpg
teaser: Leider muss die Veranstaltung wegen der Corona-Pandemie abgesagt werden.  Der
  Stuttgarter Künstler Simon Pfeffel lädt Sie ein, Teil seiner Performance
isCrawled: true
---
Leider muss die Veranstaltung wegen der Corona-Pandemie abgesagt werden.

Der Stuttgarter Künstler Simon Pfeffel lädt Sie ein, Teil seiner Performance im öffentlichen Raum zu werden. Von ihm angeleitet und kuratiert werden alle, die Interesse an einem experimentellen Dialog mit dem öffentlichen Raum Ulms und deren Benutzer haben, an der Performance an einem zentralen Platz in Ulm teilnehmen. Bei der Aktion kann jeder mitmachen, es gibt keine besonderen Voraussetzungen, außer Neugierde und Lust, sich auf eine künstlerische Performance einzulassen. In der Aktion werden zufällige Passant/innen über spielerische Elemente zur Teilnahme eingeladen. Alle Beteiligten erhalten freien Eintritt zur anschließenden Podiumsdiskussion, bei der Sie aus dem Publikum heraus die Möglichkeit haben, von Ihren Erfahrungen bei der Aktion zu berichten. Nähere Infos laufen direkt über den Künstler. Anmeldung über fsjkultur@vh-ulm.de.
(Veranstalter: vh Ulm in Zusammenarbeit mit KunstWerk e.V.)



