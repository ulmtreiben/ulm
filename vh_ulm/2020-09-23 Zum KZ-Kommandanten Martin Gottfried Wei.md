---
id: "297777767994616"
title: "Zum KZ-Kommandanten Martin Gottfried Weiß: Ein Beispiel aus der neueren
  NS-Täterforschung"
start: 2020-09-23 19:00
end: 2020-09-23 21:00
locationName: Ulmer Volkshochschule
address: Kornhausplatz 5, 89073 Ulm
link: https://www.facebook.com/events/297777767994616/
teaser: "Zum KZ-Kommandanten Martin Gottfried Weiß: Ein Beispiel aus der neueren
  NS-Täterforschung Mittwoch, 23. September, 19 Uhr Eintritt frei EinsteinHaus,"
isCrawled: true
---
Zum KZ-Kommandanten Martin Gottfried Weiß: Ein Beispiel aus der neueren NS-Täterforschung
Mittwoch, 23. September, 19 Uhr
Eintritt frei
EinsteinHaus, Unterer Saal
Nr. 20H0108112

In Zusammenarbeit mit dem Dokumentationszentrum Oberer Kuhberg