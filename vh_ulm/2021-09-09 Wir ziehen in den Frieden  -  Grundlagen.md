---
id: "844084352896221"
title: '"Wir ziehen in den Frieden"  -  Grundlagen, Ansätze und Herausforderungen der
  Friedenspädagogik'
start: 2021-09-09 19:00
locationName: Einsteinhaus Ulm
address: Kornhausplatz 5, D-89073  Germany, 89073 Ulm
link: https://www.facebook.com/events/844084352896221/
image: 234892240_6592263447465695_6007208921083477749_n.jpg
isCrawled: true
---
„Wir ziehen in den Frieden“ erfordert Fähigkeiten, Kompetenzen und einen langen Atem. Hier ist auch die Friedenspädagogik gefordert, gerade in Zeiten von Hatespeech, Polarisierung und Coronakrise. Im Vortrag werden Grundlagen, Ansätze und Herausforderungen einer aktuellen Friedenspädagogik skizziert und zur Diskussion gestellt.

Referent: Uli Jäger, Berghof Foundation, Honorarprofessor an der Universität Tübingen

Veranstalter: 
Gewerkschaft Erziehung und Wissenschaft, Kreisverband Alb-Donau/Ulm
Verein für Friedensarbeit
vh Ulm

Eintritt frei

Mehr Informationen über die Ulmer Friedenswochen:
https://www.friedenswochen-ulm.de
