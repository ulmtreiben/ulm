---
id: "767864280628084"
title: Familiennachmittag vor dem kontiki
start: 2020-07-24 15:00
end: 2020-07-24 18:00
locationName: kontiki - Kunstschule für Kinder und Jugendliche
address: Magirus-Deutz-Straße 14, 89077 Ulm
link: https://www.facebook.com/events/767864280628084/
teaser: "Familiennachmittag vor dem kontiki Dozentin: Christine Wild  Farbenfroher
  Semesterabschluß für Familien vor dem kontiki.  Wir wollen umsonst und drauß"
isCrawled: true
---
Familiennachmittag vor dem kontiki
Dozentin: Christine Wild

Farbenfroher Semesterabschluß für Familien vor dem kontiki.

Wir wollen umsonst und draußen den Sommer begrüßen.

Es gibt Pflastermalen, eine Tonwerkstatt und Malen für bis zu 100 kleine und große Menschen!

Außerdem gibt es
– Einblicke für Eltern in das frühpädagogische Konzept von kontiki für Kinder von 2 bis 6 Jahren,
– eine Ausstellung mit Kinderkunst des Semesters,
– kleine Snacks und Getränk,
– die herrliche Blauufertreppe zum Herumsausen und das kontikischiff zum Klettern vor der Tür.

Bei Regen entfällt das Fest.
AHOI, kommt alle und seid kreativ!

Datum | Uhrzeit
Termin: Freitag, 24.07.2020 | 15:00 Uhr

Ort
Ulm, kontiki, Im Stadtregal, Magirus-Deutz-Str. 14

Kurs-Nummer
20F0370040

Zielgruppe
2 bis 14 Jahre

Informationen zur Veranstaltung
Mirtan Teichmüller 
Tel. 0731 1530-29 
teichmueller@kontiki.vh-ulm.de

Plätze
0 - 100 Teilnehmer/innen 
 keine Anmeldung erforderlich

Preis
Eintritt frei