---
id: "360166905182391"
title: Zwischen Faszinosum und Feindbild – Bilder von China in Deutschland
start: 2020-11-03 20:00
link: https://www.facebook.com/events/360166905182391/
image: 122140753_3571606479583183_2162259918831906727_o.jpg
teaser: "Online-Veranstaltung: Zwischen Faszinosum und Feindbild – Bilder von China in
  Deutschland  Dozentin: Prof. Dr. Mechthild Leutner  Der Vortrag beleucht"
isCrawled: true
---
Online-Veranstaltung: Zwischen Faszinosum und Feindbild – Bilder von China in Deutschland

Dozentin: Prof. Dr. Mechthild Leutner

Der Vortrag beleuchtet die wechselvollen Chinabilder in der Geschichte und sucht Kontinuitäten und Diskontinuitäten zu gegenwärtigen Chinabildern in den deutschen Medien aufzuzeigen. Begonnen wird mit kurzen Skizzen zu Marco Polos Chinasicht, zum positven Chinabild der Jesuiten im 17. und 18. Jahrhundert, zu den kolonialen China-Klischees im 19. und Anfang des 20. Jahrhunderts und zur Faszination Chinas geistiger Kultur im Unterschied zur materiellen Kultur des Westens in der Weimarer Republik.

Die Tradierung bestimmter Wahrnehmungslinien bis in die heutige Diskussion und die Ausformung gegenwärtiger Perspektiven auf China (China als neues Feindbild oder Bedrohung, China als Wirtschaftspartner, China als Technologieführer) werden im zweiten Teil des Vortrags erörtert.

Entgegen der ursprünglichen Planung wird der Vortrag als online-Veranstaltung durchgeführt. Nach erfolgter Anmeldung erhalten Sie vor Kursbeginn die Einwahldaten per E-Mail übermittelt.

Datum | Uhrzeit
Termin: Dienstag, 03.11.2020 | 20:00 Uhr
Online-Veranstaltung

Kurs-Nummer
20H0101210

Informationen zur Veranstaltung
Dr. Markus Stadtrecher 
Tel. 0731 1530-24 
stadtrecher@vh-ulm.de

Plätze
5 - 99 Teilnehmer/innen 
 noch freie Plätze

Preis
Eintritt 6,00 €