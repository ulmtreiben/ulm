---
id: "688012455332521"
title: "Online-Veranstaltung: Demokratien unter Druck"
start: 2020-05-19 19:30
link: https://www.facebook.com/events/688012455332521/
image: 97225883_3121674047909764_478991726560673792_n.jpg
teaser: "Online-Veranstaltung: Demokratien unter Druck Dozent: Robert Schwarz In
  Kooperation mit der Bertelsmann Stiftung  Eingeschränkte Presse- und
  Meinungsf"
isCrawled: true
---
Online-Veranstaltung: Demokratien unter Druck
Dozent: Robert Schwarz
In Kooperation mit der Bertelsmann Stiftung

Eingeschränkte Presse- und Meinungsfreiheit oder ausgehebelte Verfassungsgerichte – in der Regel sind dies Merkmale von Autokratien. Doch der aktuelle Transformationsindex der Bertelsmann Stiftung zeigt, dass auch in immer mehr Demokratien eine schleichende Aushöhlung von Rechtsstaatlichkeit und politischen Freiheiten stattfindet. Wesentliche Ursachen sind Machtsicherung und Vetternwirtschaft, die wirtschaftliche Ungleichheit verstärken und zur Spaltung der Gesellschaft beitragen. Die Auswirkungen der Corona-Pandemie drohen diese Entwicklungen zu verstärken.

Dieser Kurs wird über die Online-Plattform »Zoom« angeboten. Zur Teilnahme benötigen Sie eine E-Mail-Adresse (bitte bei der Anmeldung angeben!) und ein entsprechendes Endgerät. Sie können auch ohne Kamera und Mikrofon teilnehmen. Hinweise zur Verwendung finden Sie unter https://www.vh-ulm.de/digitale-angebote/anleitungen-online-kurse, zum Datenschutz unter https://www.vh-ulm.de/cms/index.php?id=202.

Nach erfolgter Anmeldung erhalten Sie ca. eine Stunde vor Beginn die Zugangsdaten per E-Mail.

Datum | Uhrzeit
Termin: Dienstag, 19.05.2020 | 19:30 Uhr
Ort
Webinar

Kurs-Nummer
20F0109307

Informationen zur Veranstaltung
Dr. Markus Stadtrecher 
Tel. 0731 1530-24 
stadtrecher@vh-ulm.de

Plätze
10 - 80 Teilnehmer/innen 
 noch freie Plätze

Preis
Eintritt frei