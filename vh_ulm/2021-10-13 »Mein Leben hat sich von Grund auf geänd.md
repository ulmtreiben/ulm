---
id: "290700995892279"
title: »Mein Leben hat sich von Grund auf geändert« – Schloss Dellmensingen 1942
start: 2021-10-13 19:30
end: 2021-10-13 21:00
address: Stadtücherei Erbach
link: https://www.facebook.com/events/290700995892279/
image: 243117198_4539902959420192_7230311892142500153_n.jpg
isCrawled: true
---
»Mein Leben hat sich von Grund auf geändert« – Schloss Dellmensingen 1942
Dr. Michael Koch

»Du siehst, mein Leben hat sich von Grund auf geändert, aber ich will durchhalten, in der Hoffnung, doch eines Tages meinen Peter wiedersehen zu dürfen.« Mit diesen Worten fasste die jüdische Krankenpflegerin Hedwig Ury aus Ulm 1942 ihren Aufenthalt im jüdischen Zwangsaltenheim im Schloss Dellmensingen zusammen. Wie belastend diese Binnendeportation insbesondere für die älteren Menschen gewesen sein muss, zeigt sich nicht zuletzt daran, dass dort innerhalb von nur sechs Monaten bereits 17 Personen verstarben. Am 19. August 1942 wurde das jüdische Zwangsaltenheim mit der Deportation der noch verbliebenen 101 Bewohnerinnen und Bewohner nach Theresienstadt aufgelöst. Nur vier der 128 Zwangsinsassen im Dellmensinger Schloss erlebten noch die Befreiung von Theresienstadt. Lange Zeit wurde darüber geschwiegen, was sich im Dellmensinger Schloss 1942 ereignet hat. Nur den alteingesessenen Familien im Ort war noch bekannt, dass sich hier von Februar bis August 1942 ein jüdisches Zwangsaltenheim befand, in dem insgesamt 128 zumeist ältere und pflegebedürftige Bewohnerinnen und Bewohner aus ganz Württemberg gegen ihren Willen leben mussten, weil sie als Juden immer weiter ausgegrenzt und entrechtet worden sind. 

In seinem Vortrag zeichnet Dr. Michael Koch diese Ereignisse nach und erinnert dabei auch an Einzelschicksale.


In Zusammenarbeit mit der Stadtbücherei Erbach

Mittwoch, 13. Oktober, 19:30 Uhr
Eintritt 5,00 €
Erbach, Stadtbücherei, Erlenbachstr. 17
Nr. 21H1505004

Anmeldung erwünscht

https://www.vh-ulm.de/vh-programm/kurs-finder/detail/kurs/mein-leben-hat-sich-von-grund-auf-geaendert-schloss-dellmensingen-1942/21H1505004