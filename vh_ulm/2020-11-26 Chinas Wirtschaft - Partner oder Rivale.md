---
id: "832307653977028"
title: Chinas Wirtschaft - Partner oder Rivale?
start: 2020-11-26 19:00
address: Villa Lindenhof Herrlingen
link: https://www.facebook.com/events/832307653977028/
image: 122116061_3571644312912733_7735031685568854611_o.jpg
teaser: Chinas Wirtschaft - Partner oder Rivale? Vortrag von Wolfgang Hirn, Autor,
  Reporter  Donnerstag, 26. November, 19 Uhr Eintritt 5,00 € Blaustein, Herrl
isCrawled: true
---
Chinas Wirtschaft - Partner oder Rivale?
Vortrag von Wolfgang Hirn, Autor, Reporter

Donnerstag, 26. November, 19 Uhr
Eintritt 5,00 €
Blaustein, Herrlingen, Villa Lindenhof, Belle Etage, Lindenhof 2
Nr. 20H1504012

In Zusammenarbeit mit dem Verein Haus unterm Regenbogen

Die chinesischen Unternehmen werden immer innovativer und internationaler. Wer sind diese kaum bekannten Firmen? Wie stark sind sie wirklich? Und wie sollten wir im Westen auf ihren Aufstieg reagieren? Diese Fragen wird Wolfgang Hirn an diesem Abend bearbeiten.