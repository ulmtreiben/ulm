---
id: "669010840557964"
title: "Online: Zeitmanagement – Was Sie wirklich erfolgreich macht"
start: 2020-09-24 18:45
end: 2020-09-24 20:15
link: https://www.facebook.com/events/669010840557964/
image: 101545592_3168353433241825_6457675567977201664_n.jpg
teaser: "Online: Zeitmanagement – Was Sie wirklich erfolgreich macht Dozentin:
  Gabriele Hagmann Wie erkenne ich meine Prioritäten und setze sie erfolgreich
  um,"
isCrawled: true
---
Online: Zeitmanagement – Was Sie wirklich erfolgreich macht
Dozentin: Gabriele Hagmann
Wie erkenne ich meine Prioritäten und setze sie erfolgreich um, und behalten dabei immer die Übersicht. Wie plane ich meine Aufgaben effektiv und erledige sie zufriedenstellend und stressfrei, wobei ich häufige Störungen reduziere und Zeitdiebe eliminiere. Was sind meine persönlichen Leistungszeiten und wie setze ich meine Stärken fokussiert und zielorientiert ein. Wie lerne ich neue Fähigkeiten und neue Gewohnheiten?

Datum | Uhrzeit
1 Abend (2 Unterrichtsstunden) | Donnerstag
Beginn: 24.09.2020 | 18:45 bis 20:15 Uhr
Ort
Onlineseminar Beruf

Kurs-Nummer
20F1404002

Beratung zum Kursinhalt
Norbert Herre 
Tel. 0731 1530-16 
herre@vh-ulm.de

Plätze
5 - 25 Teilnehmer/innen 
 noch freie Plätze

Preis
15,00 €