---
id: "3389250077818872"
title: Auswirkungen der Coronagesetze auf Minderheiten und Menschenrechte weltweit
start: 2020-11-18 19:30
locationName: Haus der Begegnung Ulm
address: Grüner Hof 7, 89073 Ulm
link: https://www.facebook.com/events/3389250077818872/
image: 122168782_3571635822913582_3907113412113121314_o.jpg
teaser: "Gefährdete Demokratie: Auswirkungen der Coronagesetze auf Minderheiten und
  Menschenrechte weltweit  Enno Haaks Mittwoch, 18. November, 19:30 Uhr Eintr"
isCrawled: true
---
Gefährdete Demokratie: Auswirkungen der Coronagesetze auf Minderheiten und Menschenrechte weltweit

Enno Haaks
Mittwoch, 18. November, 19:30 Uhr
Eintritt frei
Ulm, Haus der Begegnung, Grüner Hof 7
Nr. 20H0109505

In Zusammenarbeit mit dem Haus der Begegnung

In den vergangenen Jahren haben wir einen Vormarsch des Populismus, die Einschränkung der Pressefreiheit und die Eingriffe in eine unabhängige Justiz in vielen Ländern beobachten können. All das gefährdet die Demokratie und hat Auswirkungen auf Minderheiten und ihre Rechte. Zudem haben wir erlebt, wie durch die Corona-Pandemie Bürgerrechte eingeschränkt wurden und werden mussten. Weitgehend von der Bevölkerung mitgetragen hat das aber auch zu Verunsicherungen geführt und zu merkwürdigen Allianzen hervorgerufen. Wie reagieren in diesem Kontext Kirchen in der weltweiten evangelischen Diaspora auf solche Phänomene?