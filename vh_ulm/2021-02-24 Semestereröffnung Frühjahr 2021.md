---
id: "930595161034036"
title: Semestereröffnung Frühjahr 2021
start: 2021-02-24 18:30
link: https://www.facebook.com/events/930595161034036/
image: 152435677_3894377580639403_8198017178797816844_n.jpg
teaser: "Mut zum Streit:  Wie Demokratie lebendig bleibt  18:00 Uhr Einlass in kleinen
  digitalen Räumen zum Plauschen und technischem  »Warm-up«  18:30 Uhr  Be"
isCrawled: true
---
Mut zum Streit: 
Wie Demokratie lebendig bleibt

18:00 Uhr
Einlass in kleinen digitalen Räumen zum Plauschen und technischem 
»Warm-up«

18:30 Uhr 
Begrüßung/Einführung 
Dr. Christoph Hantel

Demokratie und Streit
Eröffnungsvortrag 
Prof. Dr. Nicole Deitelhoff, geschäftsführendes Vorstandsmitglied Leibniz-Institut Hessische Stiftung Friedens-und Konfliktforschung 

Moderation 
Dagmar Wirtz, Kommunikationsexpertin

Musikalischer Rahmen 
Manu Stahl, Musiker und Performer

Ab ca. 20 Uhr
Musik und gute Gespräche in selbstgewählten digitalen Räumen

Anmeldung unter Nr. 21F0101000
auf www.vh-ulm.de
Bitte melden Sie sich an, Sie bekommen einen Zugang in Zoom (dann können Sie sich aktiv an der Veranstaltung beteiligen) oder YouTube (zum Zuschauen).


