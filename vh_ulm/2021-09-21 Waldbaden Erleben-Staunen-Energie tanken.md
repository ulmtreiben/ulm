---
id: "219566953552422"
title: "Waldbaden: Erleben-Staunen-Energie tanken"
start: 2021-09-21 18:00
end: 2021-09-21 20:00
locationName: Markbronn Blaustein
address: Ulm
link: https://www.facebook.com/events/219566953552422/
image: 241884173_4496418963768592_4119740200633645295_n.jpg
isCrawled: true
---
Unser Leben ist oft vollgestopft mit Terminen, wir sind meist im Online-Modus, im Hamsterrad und Stress ausgesetzt. Wie gut tun da Oasen zum Luft holen und Kraft tanken. Also raus aus dem Alltag, rein in den Wald und back to the roots! Gezielte Übungen wie Perspektivenwechsel, bewusste Fokussierung oder auch mit allen Sinnen den Wald und die Natur wahrnehmen und erleben bringen die Teilnehmer*innen dazu, bei sich selber anzukommen, durchzuatmen, zur Ruhe zu kommen, zu staunen!

Treffpunkt ist am Parkplatz an der Landstraße  L 1244 von Arnegg nach Ermingen, nach dem Abzweig Richtung Markbronn.

www.rederei-fischer.de

Gebühr 16,00 €
Kursnummer: Nr. 21H1504312

Information und Anmeldung
Ulmer Volkshochschule im Alb-Donau-Kreis
Kornhausplatz 5
89073 Ulm
www.vh-ulm.de
adk@vh-ulm.de
0731 1530 15/ -11/ -42