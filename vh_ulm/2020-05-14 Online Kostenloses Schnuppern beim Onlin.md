---
id: "1001279900286920"
title: "Online: Kostenloses Schnuppern beim Online-Meeting mit Zoom"
start: 2020-05-14 18:00
end: 2020-05-14 19:00
locationName: Ulmer Volkshochschule
address: Kornhausplatz 5, 89073 Ulm
link: https://www.facebook.com/events/1001279900286920/
image: 94989116_3085104948233341_1464742130365759488_o.jpg
teaser: "Onlineseminar: Kostenloses Schnuppern beim Online-Meeting mit Zoom Dozent:
  Bernd Müller-Dautenheimer  In der aktuellen Situation werden oft Online-Mee"
isCrawled: true
---
Onlineseminar: Kostenloses Schnuppern beim Online-Meeting mit Zoom
Dozent: Bernd Müller-Dautenheimer

In der aktuellen Situation werden oft Online-Meetings angeboten. Die vh Ulm bietet Kurse mit dem Online-Meeting-Tool Zoom an. Aber was passiert wenn man an einem solchen Kurs teilnimmt? Ist mein Computer oder Tablet dazu geeignet? Was muß ich tun bei einem solchen Kurs.

Die Veranstaltung ist gedacht für vh-Kursteilnehmer die einen Online Kurs gebucht haben und vorab das Tool Zoom ausprobieren wollen bzw. für Personen die einfach nur neugierig sind wie so ein Meeting funktioniert und »ob Sie das selbst auch könnten«.

Anmeldung erforderlich

Datum | Uhrzeit
1-mal (1,33 Unterrichtsstunden) | Donnerstag
Beginn: 07.05.2020 | 20:00 bis 21:00 Uhr
Ort
Online-Seminar

Kurs-Nummer
20F1302152

Beratung zum Kursinhalt
Norbert Herre 
Tel. 0731 1530-16 
herre@vh-ulm.de

Plätze
5 - 10 Teilnehmer/innen 
 nur noch wenige Plätze

Preis
Gebührenfrei

https://www.vh-ulm.de/vh-programm/kurs-finder/detail/kurs/onlineseminar-kostenloses-schnuppern-beim-online-meeting-mit-zoom/20F1302152