---
id: "1322382954795026"
title: Zur Erinnerung an die als »asozial« Verfolgten und Ermordeten im
  Nationalsozialismus
start: 2021-01-27 16:00
link: https://www.facebook.com/events/1322382954795026/
image: 142881702_3823142414429587_1671613743151618531_o.jpg
teaser: Am 27. Januar 1945, vor nunmehr 76 Jahren, wurde das Konzentrationslager
  Ausschwitz befreit – der Ort, der für Morde und Gräueltaten an hunderttausend
isCrawled: true
---
Am 27. Januar 1945, vor nunmehr 76 Jahren, wurde das Konzentrationslager Ausschwitz befreit – der Ort, der für Morde und Gräueltaten an hunderttausenden Menschen steht wie kein anderer.

Menschen, die sich nicht in die propagierte »Volksgemeinschaft« einfügten, wurden im Nationalsozialismus als »asozial« verfolgt. Für viele Menschen, die als Bettler, Wohnsitzslose, Suchtkranke, »Arbeitsscheue« oder »Zigeuner« aus der gesellschaftlichen Norm fielen, bedeutete dies einen Leidens- und Sterbensweg in Arbeits-, Konzentrations- und Vernichtungslagern.
Das KZ Oberer Kuhberg und die kommunalen Wohlfahrtsbehörden spielten im lokalen Verfolgungsprozess eine wichtige Rolle.
Der Historiker Oliver Gaida führt in das Thema ein und wird anhand lokaler Quellen auch die Situation in Ulm
beleuchten und Ulmer Biografien aufzeigen. Im anschließenden Gespräch diskutieren Oliver Gaida, die Ulmer Sozialbürgermeisterin Iris Mann und die Leiterin des DRK-Obdachlosenheims in Ulm, Karin Ambacher, über Auswege aus der sozialen Ausgrenzung heute. 
Begrüßung durch Oberbügermeister Gunter Czisch, Moderation des Gesprächs: Petra Bergmann

Sie erreichen den Livestream unter: https://www.youtube.com/channel/UCgsPY7s_l3XwvizzVHKTOAw

Kursnummer: 20H0108125