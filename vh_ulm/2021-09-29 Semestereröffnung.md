---
id: "605555230442241"
title: Semestereröffnung
start: 2021-09-29 18:30
end: 2021-09-29 23:00
locationName: Einsteinhaus Ulm
address: Kornhausplatz 5, D-89073  Germany, 89073 Ulm
link: https://www.facebook.com/events/605555230442241/
image: 243015436_4536448436432311_1000036592538637441_n.jpg
isCrawled: true
---
Herzliche Einladung zur Semestereröffnung am 29.09. um 18:30 Uhr im Einsteinhaus. Für Sekt, Musik, Essen und Kunst ist gesorgt. Der Eintritt ist frei.

Schwerpunktthema des Semesters ist die USA.
 
Um Anmeldung wird gebeten. 

https://www.vh-ulm.de/vh-programm/kurs-finder/detail/kurs/semestereroeffnung/21H0101000