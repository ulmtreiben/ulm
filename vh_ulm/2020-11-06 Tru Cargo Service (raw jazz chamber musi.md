---
id: "1498967866977773"
title: "ENTFÄLLT: Tru Cargo Service (raw jazz chamber music)"
start: 2020-11-06 21:00
locationName: Einsteinhaus Ulm
address: Kornhausplatz 5, D-89073  Germany, 89073 Ulm
link: https://www.facebook.com/events/1498967866977773/
image: 121703592_4919990774685632_3590655654373816552_o.jpg
teaser: "Wegen doch recht begrenzter Plätze empfehlen wir den Vorverkauf:
  https://www.ulmtickets.de/produkte/5891-tickets-tru-cargo-service-raw-jazz-ch\
  amber-mu"
isCrawled: true
---
Wegen doch recht begrenzter Plätze empfehlen wir den Vorverkauf:
https://www.ulmtickets.de/produkte/5891-tickets-tru-cargo-service-raw-jazz-chamber-music-ulmer-volkshochschule-ulm-am-06-11-2020

Der 33. US-Präsident und eine Prozession in Isfahan. Nicht näher definierte Zwischenräume und Füchse, die Alexander Mitscherlich lesen. Fahrradfahren und türkische Märchen… All das kann man in Torsten Papenheims Kompositionen für Tru Cargo Service hören. Oder auch etwas ganz anderes, aber ohne Worte. 
Der Klang von Tru Cargo Service ist verspielt, roh und dicht. Und trotzdem voller Mut zur musikalischen Lücke. Neuer Jazz zwischen Kammermusik und Krach, Komposition und kollektiver Improvisation. ( Debutalbum „Dear Passengers“, Tiger Moon Recors, 2020)

Torsten Papenheim: Gitarre
Alexander Beierbach: Tenorsaxophon
Berit Jung: Kontrabass
Christian Marien: Schlagzeug

Eintritt: 15 €, ermäßigt 10 € (mit Ausweis: Mitglieder/Schüler/Studenten/Bufdis/FSJ), Menschen bis 16 Jahre haben freien Eintritt

Wegen doch recht begrenzter Plätze empfehlen wir den Vorverkauf:
https://www.ulmtickets.de/produkte/5891-tickets-tru-cargo-service-raw-jazz-chamber-music-ulmer-volkshochschule-ulm-am-06-11-2020
