---
id: "446573666696942"
title: Asana-Flow
start: 2021-02-19 15:00
end: 2021-02-19 19:30
link: https://www.facebook.com/events/446573666696942/
image: 148516367_3864316723645489_7780096443838465781_o.jpg
teaser: "Desanca Vukic  20H1029556 Gesamtgebühr: 54,00 EUR  In den Genuss kommen des
  Karana-Übens. Das Prana wahrnehmen, während der Atem jede Bewegung führt."
isCrawled: true
---
Desanca Vukic

20H1029556
Gesamtgebühr: 54,00 EUR

In den Genuss kommen des Karana-Übens. Das Prana wahrnehmen, während der Atem jede Bewegung führt. Sich auf die körperliche Dehnung einlassen, welche in das Lösen der Blockaden und in das »Bei-sich-selbst-ankommen« hineinbegleitet.
Hier geht´s zur Anmeldung: https://bit.ly/3rG2Hba
