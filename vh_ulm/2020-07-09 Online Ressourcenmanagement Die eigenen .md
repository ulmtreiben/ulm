---
id: "1988782594588238"
title: "Online: Ressourcenmanagement: Die eigenen Stärken bewusst einsetzen"
start: 2020-07-09 18:45
end: 2020-07-09 20:15
link: https://www.facebook.com/events/1988782594588238/
image: 101685169_3168377753239393_3759270207151931392_n.jpg
teaser: "Online: Ressourcenmanagement: Die eigenen Stärken bewusst einsetzen Dozentin:
  Gabriele Hagmann Wie setze ich meine Ressourcen optimal ein. Ihre eigene"
isCrawled: true
---
Online: Ressourcenmanagement: Die eigenen Stärken bewusst einsetzen
Dozentin: Gabriele Hagmann
Wie setze ich meine Ressourcen optimal ein. Ihre eigene Karriere beginnt im Kopf, mit Ihrem Selbstwertgefühl, Ihrem Selbstbewusstsein und klaren Zielen. Welche Strategien nutzen Sie, um Ihren persönlichen Weg bewusst zu gehen?

Datum | Uhrzeit
1 Abend (2 Unterrichtsstunden) | Donnerstag
Beginn: 09.07.2020 | 18:45 bis 20:15 Uhr
Ort
Onlineseminar Beruf

Kurs-Nummer
20F1407005

Beratung zum Kursinhalt
Norbert Herre 
Tel. 0731 1530-16 
herre@vh-ulm.de

Plätze
5 - 25 Teilnehmer/innen 
 noch freie Plätze

Preis
15,00 €