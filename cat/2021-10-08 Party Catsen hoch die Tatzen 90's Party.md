---
id: "546422863096363"
title: Party Catsen hoch die Tatzen 90's Party
start: 2021-10-08 21:30
end: 2021-10-09 03:00
locationName: Cat Ulm
address: Prittwitzstraße 36, 89075 Ulm
link: https://www.facebook.com/events/546422863096363/
image: 244081818_4756922767686401_5920103688682967885_n.jpg
isCrawled: true
---
Kommt mit uns auf eine Zeitreise durch die größten und schrillsten Hits der 90er Jahre.

Von Stilikonen bis Bad Taste. Von Pop bis Rock. Von Britney Spears über MC Hammer bis hin zu Nirvana.

Es ist für jeden etwas dabei. Kommt, und feiert mit uns den Sound unserer Jugend.

dj's hammertime & el duderino

Einlass nur für geimpft, genesen oder PCR Test (max. 48h alt), kein Antigen bzw. Schnelltest
Allgemeine Maskenpflicht außer am Platz oder bei Verzehr
Kontaktnachverfolgung mit App oder Zettel
 
Liebe Gäste, nach den geltenden Corona Vorgaben, gilt bei dieser Veranstaltung die 3G-Regel (geimpft, genesen oder PCR Test, der maximal 48h alt ist). Einen Antigen bzw. Schnelltest dürfen wir nicht akzeptieren. Daher gibt es auch keine Möglichkeit sich vor Ort zu testen. Wir müssen die entsprechenden Nachweise mit einem Ausweis kontrollieren.
Um Kontaktnachverfolgung zu gewährleisten müsst Ihr Euch entweder in eine App (Luca etc.) einloggen, oder vor Ort einen Zettel ausfüllen.
Leider gibt es auch eine allgemeine Maskenpflicht, die nur beim Verzehr (Trinken) und beim Sitzen am Tisch mit 1.5m Abstand nicht gilt.
 
Wir hätten uns auch gewünscht, dass wir mit weniger Einschränkungen wieder öffnen können, aber wir sehen aufgrund der behördlichen Vorgaben, die uns keinen Spielraum lassen, zur Zeit keine andere Möglichkeit. Alternative wäre gewesen, nicht zu öffnen und auf ungewisse Besserung in der Zukunft zu hoffen.
Unsere Bitte an Euch ist, diese Regeln konsequent zu beachten und auch aufeinander aufzupassen. Wir müssen das momentan so umsetzen - im Interesse Eurer und unserer Gesundheit.
Wir hoffen, dass wir trotz allem, gemeinsam ein tolles erstes Reptile House seit Beginn der Pandemie feiern können.
 
Wie in unserem Hygienekonzept beschrieben unterstützen wir die Impfkampagne des Landes Baden-Württemberg „dranbleibenbw.de“