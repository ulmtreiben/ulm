---
id: "1030847710999879"
title: Kulturmix Ulm - Kulturnacht 2021
start: 2021-09-18 19:00
locationName: Cat Ulm
address: Prittwitzstraße 36, 89075 Ulm
link: https://www.facebook.com/events/1030847710999879/
image: 242199091_4712041468841198_2159348668324404877_n.jpg
isCrawled: true
---
Musik mit Kunstausstellung

VooDooZilla, Hard Rock, Ulm
Marian Kucharik, Piano Impro, Memmingen

Amelie Mack, Öl, Acryl und Experimentell, Ulm
Mark Klawikowski, Puppen und Gemälde, Ulm

 

im Rahmen der Kulturnacht Ulm

Einlass gewähren wir lediglich genesenen und Personen mit komplettem Impfschutz oder ein negativer Coronatest. 

Bringt bitte eine Maske mit.

Einlass: 18:00 Uhr
Eintritt: Kulturnachtbändchen 10€ (8€ ermäßigt)
