---
id: "608643026682071"
title: Gemeinsame Mahnwache zur Argrarpolitik der EU - für eine Agrarwende
start: 2020-09-11 17:30
end: 2020-09-11 18:30
locationName: Münsterplatz Ulm
address: Münsterplatz 21, 89073 Ulm
link: https://www.facebook.com/events/608643026682071/
teaser: Wir fordern eine radikale Erneuerung von Landwirtschaft und
  Lebensmittelproduktion!  Pestizidverseuchtes Obst und Gemüse, Billigfleisch
  und lange Tier
isCrawled: true
---
Wir fordern eine radikale Erneuerung von Landwirtschaft und Lebensmittelproduktion! 
Pestizidverseuchtes Obst und Gemüse, Billigfleisch und lange Tiertransporte, Überproduktion für den Export, verunreinigtes Grundwasser - das kann  so nicht mehr weiter gehen. 
Diesen Herbst werden die Gemeinsame Agrarpolitik der EU (GAP) und somit die -Subventionen neu aufgesetzt. Für die nächsten sieben Jahre! 
Es ist muss jetzt gehandelt werden für eine zukunftsfähige, nachhaltige Landwirtschaft, die Mensch und Tier achtet. 


AbL, BUND, Genfrei-Bündnis und GREENPEACE 