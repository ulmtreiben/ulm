---
id: "508746790223823"
title: Zukunftsdialog - komm ins Gespräch!
start: 2021-07-31 10:00
end: 2021-07-31 15:00
address: Ulm Donauwiese
link: https://www.facebook.com/events/508746790223823/
image: 218389374_4981640811862816_2046841228797157756_n.jpg
isCrawled: true
---
Kommt ins Gespräch! In welcher Zukunft möchtet ihr Leben? Was wünscht ihr euch? Eine Tour durch ganz Deutschland, im Jahr der Bundestagswahl.