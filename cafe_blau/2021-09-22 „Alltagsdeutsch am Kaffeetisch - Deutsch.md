---
id: "434567944471585"
title: „Alltagsdeutsch am Kaffeetisch - Deutsch sprechen ohne Angst“
start: 2021-09-22 14:30
end: 2021-09-22 16:30
locationName: Café Blau
address: Gartenstraße 11, 89077 Ulm
link: https://www.facebook.com/events/434567944471585/
image: 239942028_3735594196541502_2561560958513092683_n.jpg
isCrawled: true
---
„Alltagsdeutsch am Kaffeetisch - Deutsch sprechen ohne Angst“