---
id: "434567964471583"
title: „Alltagsdeutsch am Kaffeetisch - Deutsch sprechen ohne Angst“
start: 2021-09-29 14:30
end: 2021-09-29 16:30
locationName: Café Blau
address: Gartenstraße 11, 89077 Ulm
link: https://www.facebook.com/events/434567964471583/
image: 239942028_3735594196541502_2561560958513092683_n.jpg
isCrawled: true
---
„Alltagsdeutsch am Kaffeetisch - Deutsch sprechen ohne Angst“