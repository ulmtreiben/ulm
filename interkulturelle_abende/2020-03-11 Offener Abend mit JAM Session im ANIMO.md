---
id: "287429565569796"
title: Offener Abend mit JAM Session im ANIMO
start: 2020-03-11 19:00
end: 2020-03-11 22:00
locationName: Animo!
address: Syrlinstraße 17, 89073 Ulm
link: https://www.facebook.com/events/287429565569796/
image: 88073803_1340851266123696_1389290133479489536_o.jpg
teaser: JAM SESSION IM ANIMO!  Bringt eure Musikinstrumente mit!  Wir wollen gemeinsam
  singen, trommeln, jammen.  Hast Du Lust neue Menschen kennen zu lernen?
isCrawled: true
---
JAM SESSION IM ANIMO!

Bringt eure Musikinstrumente mit!

Wir wollen gemeinsam singen, trommeln, jammen.

Hast Du Lust neue Menschen kennen zu lernen?

Wir laden alle ein, die Lust haben einen gemeinsamen Abend zu verbringen. In einem offenen Rahmen kann man sich kennen lernen und wieder treffen.

Wir freuen uns auf Dich!