---
name: Interkulturelle Abende Ulm
website: https://www.facebook.com/InterkulturelleAbendeUlm
scrape:
  source: facebook
  options:
    page_id: InterkulturelleAbendeUlm
---
Wir laden Ulmer, Zugezogene, Geflüchtete und Besucher ein, gemeinsam Zeit zu verbringen um sich kennen zu lernen oder wieder zu treffen.
