---
id: "325871548724600"
title: "Plattentest: 5 Gänge Vinyl mit DJ Roter Freibeuter"
start: 2020-09-16 19:00
locationName: Cabaret Eden
address: Karlstraße 71, 89073 Ulm
link: https://www.facebook.com/events/325871548724600/
teaser: "ACHTUNG: Diese Woche am Mittwoch!  Beim Fünf-Gänge-Menü präsentieren euch
  DJs, die in den vergangenen Jahren das Cabaret Eden mit Musik bespielt haben"
isCrawled: true
---
ACHTUNG: Diese Woche am Mittwoch!

Beim Fünf-Gänge-Menü präsentieren euch DJs, die in den vergangenen Jahren das Cabaret Eden mit Musik bespielt haben oder maßgeblich die Idee des Clubs mit getragen haben, ihre fünf Lieblingsplatten. Ungemixt in voller Länge.
Der Rote Freibeuter begeisterte Ulm viele Jahre mit seiner bunten Mixtur aus Soul, Funk, 60s Beat, Swing, Rockabilly, Balkan Groove, Sound aus den 50-70ern und 'ner Prise Rock'n'Roll, die mit allerlei Skurrilitäten aus der Popkultur abgeschmeckt war. - Welche Perlen aus seiner Schatztruhe er wohl zum Plattentest mitbringen wird? - Lassen wir uns bei coolen Drinks von ihm mitreißen ...