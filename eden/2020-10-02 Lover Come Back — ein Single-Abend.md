---
id: "252820536050535"
title: Lover Come Back — ein Single-Abend
start: 2020-10-02 20:00
locationName: Cabaret Eden
address: Karlstraße 71, 89073 Ulm
link: https://www.facebook.com/events/252820536050535/
teaser: ... für Singles und alle, die gerne wieder Single wären oder die irgendwann
  mal Single waren.  Gespielt werden Singles aus der Eden Schatztruhe. Jeder
isCrawled: true
---
... für Singles und alle, die gerne wieder Single wären oder die irgendwann mal Single waren.

Gespielt werden Singles aus der Eden Schatztruhe. Jeder Gast kann sich eine aus dem Fundus aussuchen – oder selbst eine Single mitbringen – die dann gespielt wird. 

Jetzt sind wir wirklich neugierig, was Ihr für dieses (Anti-) Speed-Dating an leidenschaftlicher Mukke beisteuern werdet!