---
id: "352849712542706"
title: "Wolfgang Schmickler: Satiren aus dem UnGeWissen - Lesung , musikalisch
  begleitet von Ronald Makowitzki"
start: 2020-08-28 20:00
locationName: Cabaret Eden
address: Karlstraße 71, 89073 Ulm
link: https://www.facebook.com/events/352849712542706/
teaser: Der Ulmer Autor und emeritierte Professor für Theoretische Chemie Wolfgang
  Schmickler eröffnet im Eden die Lesereihe „L&M – Literatur und Musik“ mit s
isCrawled: true
---
Der Ulmer Autor und emeritierte Professor für Theoretische Chemie Wolfgang Schmickler eröffnet im Eden die Lesereihe „L&M – Literatur und Musik“ mit seinen Satiren aus dem UnGeWissen. Schmickler erschafft in seinen Texten klaustro-phobische Situationen und lässt die Zuhörer ihre ganz alltäglichen Dilemmata nacherleben. Mit einer gesalzenen Portionen Ironie und dem Blick des Naturwissenschaftlers stellt er uns in UnGewissen Zeiten vor GeWissensfragen.

Wolfgang Schmickler hat zahlreiche wissenschaftliche Arbeiten publiziert und wurde mehrfach international ausgezeichnet, bevor er sich der Prosa verschrieb. Sein Wissenschaftskrimi „Kalte Verbrennung“ erschien 2015 im G. Hess Verlag, daneben veröffentlichte er Satiren und Erzählungen.

Der Leseabend wird bereichert durch den Musiker Ronald Makowitzki an der Gitarre. Makowitzkis Kompositionen kreuzen viele Stile. Mit "Mothers Menuett" fühlt man sich an Haydn erinnert, beim "One Note Thunder" denkt man unvermittelt an Keith Jarret. 
Und wenn "Liebe kennt den Moment" erklingt" ist für viele Udo Jürgens präsent. 

Im Eden spielen sich die Tennis-Kollegen Schmickler am Buch und Makowitzki an der Gitarre mit Witz und Schmackes ihre Bälle zu, bevor es zum Matchpoint kommt.

Die Lesung wird freundlich unterstützt durch den Förderkreis deutscher Schriftsteller in Baden-Württemberg e. V. 
