---
id: "1440351822995214"
title: "Literaturwoche Donau: Aufprall: BudeMunkWieland Eine Lesung. Eine Zeitreise.
  Ein Gespräch."
start: 2021-08-05 19:30
end: 2021-08-05 22:15
locationName: Cabaret Eden
address: Karlstraße 71, 89073 Ulm
link: https://www.facebook.com/events/1440351822995214/
image: 213624092_3044685415804260_5021655441869318165_n.jpg
isCrawled: true
---
„No Future“: Unter dieser Parole besetzt eine Gruppe junger Leute Anfang der Achtzigerjahre ein Haus in Kreuzberg. Aufbruchsstimmung wechselt mit inneren Streitigkeiten unter der ständigen Bedrohung durch die Staatsgewalt. Bis bei einem Unfall eine Besetzerin ums Leben kommt. Dies hat das Autorinnen-Künstlerkollektiv BudeMunkWieland so oder ähnlich erlebt und wird uns davon manches berichten. 
"Aufprall" spielt in einer Welt von Punk, Straßenschlachten, AIDS, Drogen, rauer Kunst und wilden Theorien, bloßem Sex und tiefer Zuneigung, zu einer Zeit, die keine Kompromisse kannte. 

„Im Abgründigsten und Kühlsten glühend lebendig“, Frankfurter Hefte, 3/2021

Moderation: Cora Schönemann, Cabaret Eden

Die „Literaturwoche Donau“ wird seit 2013 ehrenamtlich organisiert und durchgeführt. Das Literaturfestival der unabhängigen Literatur präsentiert einen Mix aus Bekanntem und Neuem, Debütant*innen und „alten Hasen“, Wieder- und Neuentdeckungen, Schönes und Schräges, Heiteres und Ernstes, kurzum: Gute Literatur mit hochkarätigen Gästen aus der unabhängigen Literaturszene. Literatur als Ereignis für die Sinne - vor allem für den wichtigsten menschlichen Sinn, den Verstand. Kommen Sie, bringen Sie Freunde*innen mit, feiern Sie mit uns das gute Buch aus den unabhängigen Verlagen. Das wünschen: Florian L. Arnold, Rasmus Schöll
www.literatursalon.net > Literaturwoche Donau 2021.

Schlechtwetter-Regelung: Sollte das Wetter nicht mitspielen, werden wir im CABARET EDEN, Karlstraße 71, sein. Da dort nur eine begrenzte Platzanzahl im Innenraum zur Verfügung steht, bitten wir um rechtzeitiges Eintreffen - keine Platzgarantie, keine Reservierung möglich! 

#Corona_Hinweis
Zu Aufführungen in Innenräumen bitte immer einen tagesaktuellen Coronatest oder Genesenenbestätigung etc. vorzeigen, anderenfalls ist kein Einlass möglich. 

Zur Sicherheit aller haltet euch bitte an die aktuell gültigen Hygiene- und Abstandsregelungen – weitere Informationen veröffentlichen wir auf der Website. Bei Krankheitssymptomen bleibt bitte zuhause. Stay safe & stay tuned!