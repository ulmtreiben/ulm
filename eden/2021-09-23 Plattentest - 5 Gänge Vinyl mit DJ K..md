---
id: "1173355519819363"
title: Plattentest - 5 Gänge Vinyl mit DJ K.
start: 2021-09-23 21:00
locationName: Cabaret Eden
address: Karlstraße 71, 89073 Ulm
link: https://www.facebook.com/events/1173355519819363/
image: 242428184_4644049708973566_7507503332900375252_n.jpg
isCrawled: true
---
Lockdown und verregneter Sommer waren gestern, jetzt geht's endlich weiter mit dem Eden-Plattentest. Zum Auftakt hat Hausherr DJ K. folgende Scheiben in seinen Koffer gepackt: IGGY POP blah blah blah, GREEN KEEPERS present the ziggy franklen radio show, AMY WINEHOUSE frank, WHITE LIES ritual, PET SHOP BOYS actually. Danach open end an den Turntables.
Weitere Termine:
T.REX am 30.9., ROTER FREIBEUTER am  7.10. und MANOLO P am 14.10. 