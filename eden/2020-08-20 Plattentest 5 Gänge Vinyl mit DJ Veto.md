---
id: "579586746253609"
title: "Plattentest: 5 Gänge Vinyl mit DJ Veto"
start: 2020-08-20 19:00
locationName: Cabaret Eden
address: Karlstraße 71, 89073 Ulm
link: https://www.facebook.com/events/579586746253609/
teaser: Beim Fünf-Gänge-Menü präsentieren euch DJs, die in den vergangenen Jahren das
  Cabaret Eden mit Musik bespielt haben, ihre fünf Lieblingsplatten. Ungem
isCrawled: true
---
Beim Fünf-Gänge-Menü präsentieren euch DJs, die in den vergangenen Jahren das Cabaret Eden mit Musik bespielt haben, ihre fünf Lieblingsplatten. Ungemixt in voller Länge.

Seit mehreren Jahren macht DJ Veto (Dopestroke, Turn Loose/ Ulm, Hip-Hop-Hooray), auch als Boogie Twins gemeinsam mit DJ Sill (Schwäbische Küche, Rapalicious/ Karlsruhe), das süddeutsche Nachtleben unsicher. Seine tiefen Wurzeln in der HipHop-Kultur stellen für ihn immer die Basis, aber niemals ein Limit dar. Was zählt sind Innovation und Partytauglichkeit, so dass neben den dicksten Rap-Tracks auch gerne mal NuFunk, Breakbeat oder Drum'n'Bass auf den Tellern landen. 

Bei Bier und chilligen Drinks lassen wir uns überraschen, was er aus seiner Plattenkiste zaubert.