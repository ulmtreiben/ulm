---
id: "545096806936939"
title: "Literaturwoche Donau: Konzert Der Herr Polaris"
start: 2021-08-10 21:30
end: 2021-08-10 22:15
locationName: Cabaret Eden
address: Karlstraße 71, 89073 Ulm
link: https://www.facebook.com/events/545096806936939/
image: 222813200_3055399018066233_7998241149057028364_n.jpg
isCrawled: true
---
Der Herr Polaris ist etwas für Liebhaber abwegig schöner Songs, der besonderen Sprache, die immer schon mehr wagte, weiter ging, präziser war, als vieles andere da draußen. Für eine Welt, die Kopf steht, aber gut damit leben kann.