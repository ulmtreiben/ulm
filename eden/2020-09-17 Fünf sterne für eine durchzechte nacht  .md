---
id: "293518385282228"
title: "Fünf sterne für eine durchzechte nacht : eine Performance mit rok & michael
  moravek"
start: 2020-09-17 20:00
end: 2020-09-17 22:15
locationName: Cabaret Eden
address: Karlstraße 71, 89073 Ulm
link: https://www.facebook.com/events/293518385282228/
teaser: 'Der Foto- und Videokünstler ROK veröffentlicht mit "fünf sterne für eine
  durchzechte nacht" seinen zweiten Gedichtband. So viel vorneweg: Das dieses M'
isCrawled: true
---
Der Foto- und Videokünstler ROK veröffentlicht mit "fünf sterne für eine durchzechte nacht" seinen zweiten Gedichtband. So viel vorneweg: Das dieses Mal mit Hardcover ausgestattete Buch hält das Niveau mühelos und steht seinem Vorgänger "die zärtlichkeit des schneemanns" in nichts nach. Geblieben ist auch die Zusammenarbeit mit dem Berliner Künstler und Musiker Samuel F.Sieber, der Illustrationen für das Buch und den Einband beigetragen hat. Seine Figuren spiegeln ROKs Lyrik auf wundersame Weise wider und umgekehrt. Wobei natürlich die Gedichte im Vordergrund stehen. Und die haben es wieder in sich. Hervorzuheben ist ROKs feines Gespür für Rhythmus und Melodie, beides erzeugt in seinen Gedichten einen langen Fluss. Der Leser wird in jenen Flow von der ersten Zeile an hineingezogen und will das schmale Bändchen gar nicht mehr beiseite legen. Das Kopfkino wird mit sachte bewegten Bildern und lakonischen Metaphern angeregt. Zudem sind einige Gedichte mit einer Prägnanz geschrieben, die wir in guten Rock- oder Pop-Songs finden. Einige Male schimmert auch der frühe Wolf Wondratschek ("Chucks Zimmer") durch und selbst der freigeistige Allen Ginsberg schwebt hin und wieder durch ROKs Wort-Räume und Wort-Träume. Seine Zeilen heben im Kopf des Lesers ab, ein sachter Flug gen Himmel, ein Sturz auf den Asphalt. Die harte Realität, die Schönheit des Lebens und die Farben der Melancholie sind oft nur ein Wort voneinander entfernt.  Auf „fünf sterne für eine durchzechte nacht“ feiern Poesie und Beat eine Hochzeit. Fünf Sterne für einen Ulmer Poeten!
MICHAEL MORAVEK begleitet rok auf der gitarre und spielt songs von seinem aktuellen album NOVEMBER IN MY SOUL!

A beautiful record by a great writer. Here are songs of sadness, heartbreak, dreams and LOVE.

Michael Moravek has given the world something special, unique and beautiful."

Steve Wickham (The Waterboys)