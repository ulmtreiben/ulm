---
id: "345095723524872"
title: "Plattentest: 5 Gänge Vinyl mit Serdar Dogan"
start: 2020-08-27 19:00
locationName: Cabaret Eden
address: Karlstraße 71, 89073 Ulm
link: https://www.facebook.com/events/345095723524872/
teaser: Beim Fünf-Gänge-Menü präsentieren euch DJs, die in den vergangenen Jahren das
  Cabaret Eden mit Musik bespielt haben, ihre fünf Lieblingsplatten. Ungem
isCrawled: true
---
Beim Fünf-Gänge-Menü präsentieren euch DJs, die in den vergangenen Jahren das Cabaret Eden mit Musik bespielt haben, ihre fünf Lieblingsplatten. Ungemixt in voller Länge.

Seit 2004 bedient Serdar Dogan, auch gemeinsam mich Michael Langlois und Martin Schubert als BLEEP GEEKS, den Dancefloor mit Hingabe und Empathie. An den Plattentellern wie im Radio oder im Blog zeigt er sich offen für Vielfalt, für die unterschiedlichsten Styles und Referenzen. Musikalisch bedeutet dies ein informierter und reflektierter Eklektizismus, der im Bewusstsein einer sich ständig verändernden Gegenwart nie zum Stillstand kommt.
Bei Bier und chilligen Drinks lassen wir uns überraschen, was er aus seiner Plattenkiste zaubert und welche spannenden Gespräche sich aus dem Miteinander entwickeln.