---
id: "444291336910287"
title: bleepgeeks w/ Michael Langlois & Serdar Dogan
start: 2021-09-11 22:00
locationName: Cabaret Eden
address: Karlstraße 71, 89073 Ulm
link: https://www.facebook.com/events/444291336910287/
image: 241637290_10161688511223298_7770794041069060860_n.jpg
isCrawled: true
---
Nach der fetten Studioparty am letzten Samstag geht’s Schlag auf Schlag weiter. Wieder mit House und Disco. Dieses Mal mit bleepgeeks auf unserem Hauptfloor!