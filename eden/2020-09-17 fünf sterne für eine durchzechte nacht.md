---
id: "330637354725940"
title: fünf sterne für eine durchzechte nacht. Eine Performance mit ROK & michael
  moravek - 25 Jahre free FM
start: 2020-09-17 20:00
end: 2020-09-17 22:00
locationName: Cabaret Eden
address: Karlstraße 71, 89073 Ulm
link: https://www.facebook.com/events/330637354725940/
teaser: "Radio free FM und Cabaret Eden präsentieren  ROK   - Musikalische Lesung
  /Musik : michael moravek  Der Foto- und Videokünstler ROK veröffentlicht"
isCrawled: true
---
Radio free FM und Cabaret Eden präsentieren

ROK   - Musikalische Lesung /Musik : michael moravek

Der Foto- und Videokünstler ROK veröffentlicht mit "fünf sterne für eine durchzechte nacht" seinen zweiten Gedichtband. Das neue Werk hält das Niveau mühelos und steht seinem Vorgänger "die zärtlichkeit des schneemanns" in nichts nach. Seine Figuren spiegeln ROKs Lyrik auf wundersame Weise wider und umgekehrt. Wobei natürlich die Gedichte im Vordergrund stehen. Der Leser wird in jenen Flow von der ersten Zeile an hineingezogen und will das schmale Bändchen gar nicht mehr beiseite legen. Das Kopfkino wird mit sachte bewegten Bildern und lakonischen Metaphern angeregt. Zudem sind einige Gedichte mit einer Prägnanz geschrieben, die wir in guten Rock- oder Pop-Songs finden. Einige Male schimmert auch der frühe Wolf Wondratschek ("Chucks Zimmer") durch und selbst der freigeistige Allen Ginsberg schwebt hin und wieder durch ROKs Wort-Räume und Wort-Träume. 
Die Zeilen heben im Kopf des Lesers ab, ein sachter Flug gen Himmel, ein Sturz auf den Asphalt. Die harte Realität, die Schönheit des Lebens und die Farben der Melancholie sind oft nur ein Wort voneinander entfernt.  
Auf „fünf sterne für eine durchzechte nacht“ feiern Poesie und Beat eine Hochzeit. Fünf Sterne für einen Ulmer Poeten!
Michael moravek
A beautiful record by a great writer. Here are songs of sadness, heartbreak, dreams and LOVE.

Michael Moravek has given the world something special, unique and beautiful."

Steve Wickham (The Waterboys)

-------------------------
Radio free FM gem. GmbH
Platzgasse 18
89077 Ulm

Wir danken für die Unterstützung durch das Ministerium für Wissenschaft, Forschung und Kunst Baden-Württemberg.