---
id: "663396131639023"
title: Eden Rocks Indie w/ v.strange & nikk rhodes
start: 2021-10-02 20:00
locationName: Cabaret Eden
address: Karlstraße 71, 89073 Ulm
link: https://www.facebook.com/events/663396131639023/
image: 243877580_4678409455537591_4983947828086258460_n.jpg
isCrawled: true
---
Endlich! Die Neuauflage des Eden Klassikers mit Indie, Pop! und New Rave von Briten und solchen, die so klingen als ob. Hauptsache Gitarre.