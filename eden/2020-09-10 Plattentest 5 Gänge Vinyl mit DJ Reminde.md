---
id: "2411989919108901"
title: "Plattentest: 5 Gänge Vinyl mit DJ Reminder"
start: 2020-09-10 19:00
locationName: Cabaret Eden
address: Karlstraße 71, 89073 Ulm
link: https://www.facebook.com/events/2411989919108901/
teaser: Beim Fünf-Gänge-Menü präsentieren euch DJs, die in den vergangenen Jahren das
  Cabaret Eden mit Musik bespielt haben oder maßgeblich die Idee des Clubs
isCrawled: true
---
Beim Fünf-Gänge-Menü präsentieren euch DJs, die in den vergangenen Jahren das Cabaret Eden mit Musik bespielt haben oder maßgeblich die Idee des Clubs mit getragen haben, ihre fünf Lieblingsplatten. Ungemixt in voller Länge.
Früher war Ulm eine Drum & Bass Hochburg weil, ja klar, da kommen die NME's her. Auf diesem fruchtbaren Boden kam auch Reminder als Warm Up -DJ bei der Partyreihe "Soulfire Session" zu Ruhm und Ehre. Mit seinem Dubstep Event "subt0ne" oder seiner Idee, den El Mero Mero Mexikaner nach Ulm zu bringen, hat er die Szene bereichert. Mit dieser eindrucksvollen Bilanz klingt es doch logisch, dass wir Reminder in den Lustgarten einladen und er uns dort einige seiner Lieblingsplatten präsentiert. Davon abgesehen hört und spielt er auch weit mehr als nur Drum & Bass oder Dubstep. Wir dürfen gespannt sein ...