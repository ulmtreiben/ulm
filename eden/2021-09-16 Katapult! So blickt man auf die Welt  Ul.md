---
id: "188718026683010"
title: Katapult! So blickt man auf die Welt * Ulmer Leseorte
start: 2021-09-16 19:30
locationName: Cabaret Eden
address: Karlstraße 71, 89073 Ulm
link: https://www.facebook.com/events/188718026683010/
image: 241665471_10158774800307756_8260061430716648354_n.jpg
isCrawled: true
---
Ulmer Leseorte - Wo Geschichten lebendig werden
Im Cabaret Eden
(ACHTUNG: HIER GILT 2G)

Gründer und Chefredakteur Benjamin Fredrich und Buchverlagsleiter Sebastian Wolter stellen KATAPULT vor.

Das KATAPULT-Magazin ist das wachstumsstärkste Magazin Deutschlands und mischt von der Ostseestadt Greifswald aus seit 2015 die Medienbranche auf. Es hat mittlerweile über 80.000 Abonnenten und 700.000 Follower in den sozialen Medien, es baut eine alte Schule aus, pflanzt einen Wald, legt sich mit großen Verlagen an, hat einen Buchverlag und eine Lokalzeitung gegründet, gründet ein Musiklabel, entwickelt Online-Spiele wie die Kartenolympiade, initiiert eine Buchmesse in Greifswald und demnächst folgt eine eigene Journalistenschule ...�Wie haben die das gemacht? Gründer und Chefredakteur Benjamin Fredrich und Buchverlagsleiter Sebastian Wolter stellen KATAPULT vor und erzählen, warum man für solche Ideen ins Schwimmbad gehen muß.

Achtung: Gäste müssen einen 2-G Nachweis erbringen (geimpft, genesen - Nachweis Erstimpfung mit Schnelltest)

***

29.08.2021 | 20:00 H | Me and all Hotel
Bernd Gieseking

31.08.2021 | 20:00 H | Jugendfarm Ulm 
Ernst Paul Dörfler

15.09. | 20:00 H | Club Action
Fabian Sommavilla

16.09. | 20:00 H | Cabaret Eden
Katapult-Verlag

21.09.2021 | 20:00 H | Donaustadion Ulm
Bernd Beyer

24.09.2021 | 20:00 H | Kinderladen Ulm
Sarah Straub

26.09.2021 | 16:00 H | Heyoka Zaubergarten
Patrick Salmen

***

Die Veranstaltungsreihe "Ulmer Leseorte" wird im Programm Kultursommer 2021 durch die Beauftrage der Bundesregierung für Kultur und Medien (BKM) mit Mitteln aus NEUSTART KULTUR gefördert.

Der Ulmer Kultursommer ist eine Initiative der Kulturabteilung der Stadt Ulm in Zusammenarbeit mit der Ulmer Kulturszene. Die einzelnen Programmpunkte werden von Gleis 44, Roxy Ulm oder HfG Archiv veranstaltet. Alle Beteiligten bedanken sich bei den Stadtwerke Ulm / Neu-Ulm und bei der Kulturstiftung des Bundes für die freundliche Unterstützung.

www.katapult-verlag.de/

Veranstalter: ROXY gemeinnützige GmbH