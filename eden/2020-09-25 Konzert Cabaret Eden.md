---
id: "249040486528168"
title: Konzert "Cabaret Eden"
start: 2020-09-25 20:30
locationName: Cabaret Eden
address: Karlstraße 71, 89073 Ulm
link: https://www.facebook.com/events/249040486528168/
teaser: Das „Cabaret Eden“ bietet in seiner einmaligen Atmosphäre den Rahmen, ein
  zeitgenössisches Programm anders erlebbar zu machen.  Der Komponist Konstant
isCrawled: true
---
Das „Cabaret Eden“ bietet in seiner einmaligen Atmosphäre den Rahmen, ein zeitgenössisches Programm anders erlebbar zu machen.  Der Komponist Konstantin Heuer schrieb eigens ein Werk für Violine und Cello für diese Spielstätte und wird als DJ fließende Übergänge zwischen den Werken schaffen.

Programm:

Iannis Xenakis: Dhipli Zyvia

Donnacha Dennehy: Pushpulling

Béla Bartók: Hungarian Folk Melodies

Zoltán Kodály: Duo für Violine und
Cello: 3. Satz

Dimitri Schostakowitsch: Quartett Op 36a 

Konstantin Heuer: Auftragskomposition 
