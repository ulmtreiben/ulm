---
id: "2101093653363145"
title: Gleiss.54
start: 2021-09-04 22:00
locationName: Cabaret Eden
address: Karlstraße 71, 89073 Ulm
link: https://www.facebook.com/events/2101093653363145/
image: 241359748_4583829794995558_7523320885937588915_n.jpg
isCrawled: true
---
Dancefloor mit Disco, Wave, Rap, Alternative und keinem Bum
w/ k.eden & disco petra
 
Mal ehrlich, wir sind doch alle ein bisschen Gleiss, mit dieser Vorstellung von Berlin. In Ulm gibt's bereits ein Gleis. Und in NYC war da noch so eine Disco, das Studio 54. Mit diesen zwei coolen Clubs als Inspiration muss das eine mega Party werden. Also lasst euch blicken zur Nacht mit zwei s und zehn mal mehr Party