---
id: "477791163115779"
title: Hansis Rockstüble - Gitarrenabend mit DJ Hans Francisco
start: 2020-10-28 19:00
locationName: Cabaret Eden
address: Karlstraße 71, 89073 Ulm
link: https://www.facebook.com/events/477791163115779/
image: 122716324_3684362561608957_3966982881007612127_n.jpg
teaser: Jeder kennt doch den Hans, und der hat  in den sozialen Medien gelesen, dass
  laute Musik die Ansteckung mit Corona erschwert. So soll vor allem die Gi
isCrawled: true
---
Jeder kennt doch den Hans, und der hat  in den sozialen Medien gelesen, dass laute Musik die Ansteckung mit Corona erschwert. So soll vor allem die Gitarre die Rezeptoren dermaßen in Schwingungen versetzen, dass kein Virus mehr andocken kann, auch nicht Corona. Na ja, das kann nun glauben, wer will, jedenfalls will Hans mithelfen, das Virus zu besiegen und zieht mit wehenden Fahnen und mit Hansis Rockstüble im Sturmgepäck ins Schlachtfeld. Ob dass was bringt? Der Hans ganz alleine gegen so viel Corona. Schaden kann´s jedenfalls nicht ...