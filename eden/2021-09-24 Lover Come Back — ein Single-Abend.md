---
id: "292031859022365"
title: Lover Come Back — ein Single-Abend
start: 2021-09-24 22:00
locationName: Cabaret Eden
address: Karlstraße 71, 89073 Ulm
link: https://www.facebook.com/events/292031859022365/
image: 242755038_4653404471371423_642949408811820092_n.jpg
isCrawled: true
---
... für Singles und alle, die gerne wieder Single wären oder die irgendwann mal Single waren.
Im Anschluss an die Revue "na dann tschüss ...!" werden Singles aus der Eden Schatztruhe aufgelegt. Jeder Gast kann sich eine aus dem Fundus aussuchen – oder selbst eine Platte mitbringen – die dann gespielt wird.
Jetzt sind wir wirklich neugierig, was Ihr für dieses (Anti-) Speed-Dating an leidenschaftlicher Mukke beisteuern werdet!