---
id: "282744470272581"
title: "Literaturwoche Donau: Markus Ostermair. Der Sandler. Lesung & Gespräch"
start: 2021-08-07 19:30
end: 2021-08-13 21:15
address: Cabaret Eden
link: https://www.facebook.com/events/282744470272581/
image: 216072807_3044279515844850_1030079702505613689_n.jpg
isCrawled: true
---
Faust mit metallenem Kern: Dieses Buch wird sie beunruhigen und zugleich begeistern. Denn das Debüt des Münchners Markus Ostermair behandelt ein oft ausgeblendetes, in dieser Republik ungern thematisiertes Topic: Obdachlosigkeit und Ausgrenzung. 

Karl ist ein „Sandler“, ein Obdachloser in München. Wenn man nicht zu Hause bleiben kann, weil es keines gibt – wenn es immer nur das große und unsichere "Draussen" gibt. Davon erzählt Markus Ostermair in seinem Debüt. Sein „Karl“ erzählt von der Scham des sozialen Abstiegs und dem unverhofften Glück. Zugleich geht es um Karls Freund Lenz: Ein Zettelschreiber und Utopist, mit dem es zu Ende geht und der Karl seine Lebensbeichte vermachen möchte.

Moderation: Florian L. Arnold, Autor

Eintritt: Kultur ist uns etwas wert. Viel wert. Jeder soll unsere Abende besuchen können, unabhängig vom Einkommen. Also sagen wir: PAY WHAT YOU CAN! Wer es sich leisten kann, darf gern ein bißchen mehr geben.

Schlechtwetter-Regelung: Sollte das Wetter nicht mitspielen, werden wir im "Cabaret Eden", Karlstraße 71, sein. Da dort nur eine begrenzte Platzanzahl im Innenraum zur Verfügung steht, bitten wir um rechtzeitiges Eintreffen - keine Platzgarantie, keine Reservierung möglich. 
Zu Aufführungen in Innenräumen bitte immer einen tagesaktuellen Corona-
Test vorzeigen, sonst können wir keinen Einlass gewähren!

Was Sie auch noch wissen sollten: Die „Literaturwoche Donau“ wird seit 2013 ehrenamtlich organisiert und durchgeführt. Wir können das Festival nur ermöglichen dank der finanziellen Unterstützung durch die Stadt Ulm und die Museumsgesellschaft Ulm sowie die Beiträge unseres Fördervereins „Literatursalon Donau“. Wenn Sie auch zukünftig ein Festival der Literatur für Ulm und Umgebung genießen möchten - unterstützen Sie uns. Werden Sie Fördermitglied im Literatursalon. Oder spenden Sie einfach mal so etwas. So können wir weiterhin Ungewöhnliches, Übersehenes, Unentdecktes auf Ihren literarischen Speiseplan setzen!