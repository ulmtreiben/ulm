---
id: "377862720344455"
title: "Literaturwoche Donau: Konzert Bernhard Eder"
start: 2021-08-04 21:30
end: 2021-08-04 22:30
locationName: Cabaret Eden
address: Karlstraße 71, 89073 Ulm
link: https://www.facebook.com/events/377862720344455/
image: 221644920_3055395474733254_8969388708077898231_n.jpg
isCrawled: true
---
Konzert Bernhard Eder. 

Eine Musik, die intimer, zarter und direkter nicht sein könnte. Entstanden in Lockdown-Zeiten, verborgen und „unterirdisch“, trifft Eders Liebe zur Gitarre auf seine Liebe zur Elektronik. 
„Intime Kunstwerke eines großartigen Songwriters“. 
(Rolling Stone Magazin)