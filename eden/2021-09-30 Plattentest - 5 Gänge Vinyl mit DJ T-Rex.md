---
id: "257103412898279"
title: Plattentest - 5 Gänge Vinyl mit DJ T-Rex
start: 2021-09-30 21:00
locationName: Cabaret Eden
address: Karlstraße 71, 89073 Ulm
link: https://www.facebook.com/events/257103412898279/
image: 243223891_4670706499641220_6771704347878650948_n.jpg
isCrawled: true
---
Jeder kennt Ihn, jeder weiß Bescheid. T-Rex, die Ulmer Clublegende schlechthin, präsentiert fünf seiner Lieblingsplatten zum Vinyltest im Eden. Falls die Stimmung passt, will er im Anschluss die Teller weiterdrehen. - seid mit dabei! 
Weitere Termine:  ROTER FREIBEUTER am  7.10. und MANOLO P am 14.10. 