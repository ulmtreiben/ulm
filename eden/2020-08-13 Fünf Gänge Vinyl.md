---
id: "3006882402871825"
title: "Plattentest: Fünf Gänge Vinyl"
start: 2020-08-13 18:00
locationName: Cabaret Eden
address: Karlstraße 71, 89073 Ulm
link: https://www.facebook.com/events/3006882402871825/
teaser: Beim Fünf Gänge Vinyl präsentieren euch DJs, die in vergangenen Tagen schon
  öfters das Cabaret Eden mit Musik bespielt haben, ihre fünf Lieblingsplatt
isCrawled: true
---
Beim Fünf Gänge Vinyl präsentieren euch DJs, die in vergangenen Tagen schon öfters das Cabaret Eden mit Musik bespielt haben, ihre fünf Lieblingsplatten.

An dem Abend werden nur die Drinks gemischt. Die wohl selektierten Vinylscheiben laufen ungemixt und in voller Länge von Seite A über Seite B. Jeder Song wird gespielt und wir genießen gemeinsam fünf Werke von fünf Künstlern, welche uns von den DJs präsentiert und näher gebracht werden. Keine Spotify Playlist und nichts aus der Dose. Nur kalte Drinks und kleine Snacks.
 
Von Hip Hop über House, zu Maultaschen in Berlin, Techno und als ''Eins Tiefer'' wieder zurück!
 
Viele kennen Nico Bulla noch aus der legendären ''Panda Bar'' oder aus der Frau Berger ''Halbzeit'', wo er regelmäßig die feiernde Menge als ''Dj Niggo'' mit seinen Hip Hop Sets hat brodeln lassen. 

Doch nicht nur im Hip Hop Bereich streckt der gebürtige Ulmer seine Finger aus. Auch im elektronischen Genre stand der Dj und Produzent unter anderem in Berlin hinter den Decks namhafter Clubs wie dem Watergate oder dem Sisyphos, meist mit seinem Duo-Projekt ''Eins Tiefer''.

Auch kulinarisch stand er mit Qualität hinter ''St.Mauli'' und wird uns am Donnerstag fünf seiner Lieblings-Alben vorstellen, die ihn seither auf seiner musikalischen Reise inspiriert haben.