---
id: "554176145896344"
title: BORDEL NOUVEAU - WE ARE BACK!
start: 2021-09-25 23:00
locationName: Cabaret Eden
address: Karlstraße 71, 89073 Ulm
link: https://www.facebook.com/events/554176145896344/
image: 241636210_1038874863596190_3587918865898821714_n.jpg
isCrawled: true
---
THE BOYS ARE BACK!

Das musikalische Trio Infernale rund um Bastian Stimmig, Harry Ken und Hans Pech ist zurück im CABARET EDEN! 

Nach Monaten des Wartens, Hoffens, Bangens und Däumchendrehens sind wir endlich wieder zurück wo alles begann! 

Am 25.09.2021 werdet ihr von feinsten melodischen Technoklängen, Afrohousebeats und weiteren elektronischen Feinschmeckereien umgarnt und wir werden Euch zeigen, dass wir nix verlernt haben und dass wir bis in die Haarspitzen motiviert sind! 

WIR FREUEN UNS SO AUF EUCH!

EINTRITT: 5 EURO

Ab 23 Uhr heisst es Feuer frei und Hände hoch im CLUB - DAS EDEN WIE IHR ES KENNT UND LIEBT! Bühne frei für einen großen Abend ;)

Line Up:

Bastian Stimmig
Harry Ken
Hans Pech

So wie es sich gehört und kompromisslos dazu bereit, mit Euch einen wunderschönen Abend zu verbringen!



