---
id: "1931248050366074"
title: Bassportation goes Eden
start: 2021-08-27 19:00
end: 2021-08-28 05:00
locationName: Cabaret Eden
address: Karlstraße 71, 89073 Ulm
link: https://www.facebook.com/events/1931248050366074/
image: 240734365_4494152853963777_2676888442917655277_n.jpg
isCrawled: true
---
Wir spielen am Freitag im Eden. Kleineres gemütliches Ding, begrenzte Plätze. 

Einlass nach 3G. Schnelltest (Antigen) reicht. 

Wir bespielen schon ab 19 Uhr die Außenterrasse.

