---
id: "719272766136198"
title: Tee mit Tanten spielen " Na dann tschüß"
start: 2021-09-25 20:00
end: 2021-09-25 22:00
locationName: Cabaret Eden
address: Karlstraße 71, 89073 Ulm
link: https://www.facebook.com/events/719272766136198/
image: 241795007_675071843885202_7816499754637037996_n.jpg
isCrawled: true
---
„na dann…tschüss.“ erzählt vom Morden und Verlassenwerden, vom Sitzenlassen und vom Sitzenbleiben, von falschen Versprechungen, späten Einsichten und vom falschen Leben.                                                                        Ein szenischer Ratgeber, was man zu tun hat, wenn es AUS ist und was man auf keinen Fall unternehmen sollte, wenn das Ende naht – egal welcher Art und wie man am besten und unmissverständlich „tschüss“ sagt. 
Es spielen und singen: Celia Endlicher, Claudia Cufrodelli, Daniela May, Katrin Hötzel und Frank Betz                                                 Regie: Katrin Hötzel