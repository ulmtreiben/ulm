---
id: "581979223178383"
title: "New Match: Reminder & Un:Cut"
start: 2021-07-16 23:00
locationName: Cabaret Eden
address: Karlstraße 71, 89073 Ulm
link: https://www.facebook.com/events/581979223178383/
image: 215138071_4429008933810979_517989675752416956_n.jpg
isCrawled: true
---
Als Opener nach dem Lockdown geben die DJs  Reminder und Un:Cut (deleted user) ein weiteres Gastspiel mit chilligem Drum´n´Bass mit Platten von 2005 bis heute.Als kleines zusätzliches Bonbon hat sich MC Marvelous (NME-Click) angekündigt. Damit ist ein musikalisch spannender und spaßiger Abend vorprogrammiert.