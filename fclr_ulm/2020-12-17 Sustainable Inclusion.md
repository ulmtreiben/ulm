---
id: "725684205035905"
title: Sustainable Inclusion
start: 2020-12-17 18:30
link: https://www.facebook.com/events/725684205035905/
image: 130303045_2530790247220088_9112156764411348694_o.jpg
teaser: Sustainability for all people! This workshop aims to bring the inclusion
  matter under the spotlight and investigates the participation of the marginal
isCrawled: true
---
Sustainability for all people! This workshop aims to bring the inclusion matter under the spotlight and investigates the participation of the marginalized groups in the scope of
sustainability. We would like to provide the facts to answer these following questions;
- What is done and planned to include more people from different communities to sustainability goals?
- How through the inclusion people can be empowered?
- What does inclusion mean in terms of sustainability?

Referent: Aslihan Demir

Link for participation: https://uni-ulm.webex.com/uni-ulm-de/j.php?MTID=m70e277eed1dd3fb97308be106860b995

Meeting-ID: 174 586 5112