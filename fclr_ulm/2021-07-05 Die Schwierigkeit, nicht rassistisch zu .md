---
id: "330758188655949"
title: Die Schwierigkeit, nicht rassistisch zu sein. Eine Einführung in die
  Rassismuskritik
start: 2021-07-05 18:30
link: https://www.facebook.com/events/330758188655949/
image: 201029230_2660917187540726_3510124312909808004_n.jpg
isCrawled: true
---
 📅 05.07.21 ⌚ 18:30 📌 Online 💬 deutsch

 Die Mordanschläge von Hanau, Racial Profiling und Polizeigewalt, prekäre Arbeitsbedingungen in der Fleischindustrie, strukturelle Benachteiligung auf dem Wohnungsmarkt, ungleiche Auswirkungen durch die Coronakrise. Martin Luther Kings „Traum, dass meine vier kleinen Kinder eines Tages in einer Nation leben werden, in der man sie nicht nach ihrer Hautfarbe, sondern nach ihrem Charakter beurteilen wird“, ist auch nach über 50 Jahren für die meisten Menschen dieser Welt immer noch keine Realität. Was hat das mit Rassismus zu tun und warum ist es – wie Annita Kalpaka und Nora Räthzel schon 1986 schrieben –„schwierig, nicht rassistisch zu sein“?

Die Veranstaltung findet statt in Kooperation mit adis e.V. und der Gewerkschaft für Erziehung und Wissenschaft.

Link zur Veranstaltung:
uni-ulm.zoom.us/j/6831655221

Meeting-ID: 683 165 5221
Kenncode: FCLR202

#fclr2021ulm #gew
____________________________________________

 The difficulty of not being racist. An introduction to the critique of racism

📅 05.07.21 ⌚ 6.30 pm 📌 Online 💬 German

 The Hanau murder attacks, racial profiling and policeviolence, precarious working conditions in the meat industry,structural disadvantage in the housing market, disparateeffects from the corona crisis.Martin Luther King's “dream that my four little children will one day live in a nation wherethey will not be judged by the color of their skin, but by the content of their character.” is still not a reality for mostpeople in this world, even after more than 50 years.

What does this have to do with racism and why is it - as Annita Kalpaka and Nora Räthzel wrote back in 1986 - so “difficult not to be racist”

In cooperation with: adis e.V. & Gewerkschaft für Erziehung und Wissenschaft

Link:
uni-ulm.zoom.us/j/6831655221

Meeting-ID: 683 165 5221
Kenncode: FCLR2021