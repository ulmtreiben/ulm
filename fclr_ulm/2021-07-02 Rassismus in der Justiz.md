---
id: "4213487415435788"
title: Rassismus in der Justiz
start: 2021-07-02 18:00
link: https://www.facebook.com/events/4213487415435788/
image: 199561196_2660904610875317_7265023408773664457_n.jpg
isCrawled: true
---
 02.07.21 ⌚ 18:00 📌 Online 💬 deutsch

Vorurteile haben wir (leider) alle. Aber was, wenn Vorurteile dort auftreten, wo Neutralität und Unvoreingenommenheit unabdingbar sein sollten - vor Gericht oder bei der Staatsanwaltschaft?
Werden Personen mit ausländisch klingenden Namen schneller angeklagt, wird eine Straftat mit rassistischem Hintergrund als solche auch wirklich erkannt oder wird aus politischen Gründen alles verharmlost und ist die Justiz eine von weißen, alten Männern beherrschte Elite? Sind auch das nur Vorurteile?
Wie die Justiz in Deutschland mit dem Vorwurf des Rassismus in den eigenen Reihen umgeht und was getan werden kann und muss um Rassismus in der Justiz entgegenzutreten, darüber sprechen die Oberstaatsanwältin Ines Karl von der Zentralstelle Hasskriminalität und die Rechtsanwältin Dr. Kati Lang, die sich für Opfer rechter Gewalt einsetzt.

Wir freuen uns auf einen spannenden Abend mit Inputs und Diskussion.

Link zur Veranstaltung:

https://uni-ulm.zoom.us/j/6831655221?pwd=d2p0aktVbENpTitoSGlPeHFvcms4UT09

Meeting-ID: 683 165 5221
Kenncode: FCLR2021

#fclr2021ulm
____________________________________________

Racism in judical institutions

02.07.21 ⌚ 18:00 📌 Online 💬 German

We all have prejudices (unfortunately). But what if prejudices appear where neutrality and impartiality should be indispensable - in court or at the public prosecutor's office?
Are people with foreign-sounding names charged more quickly, is a crime with a racist background really recognised as such or is everything played down for political reasons and is the judiciary an elite dominated by white old men? Are these also just prejudices?
How the judiciary in Germany deals with accusations of racism in its own ranks and what can and must be done to counter racism in the judical institutions will be discussed by senior public prosecutor Ines Karl from the Central Office for Hate Crime and lawyer Dr Kati Lang, who campaigns for victims of right-wing violence.

We look forward to an interesting evening with input and discussion.

Link:

https://uni-ulm.zoom.us/j/6831655221?pwd=d2p0aktVbENpTitoSGlPeHFvcms4UT09

Meeting-ID: 683 165 5221
Kenncode: FCLR2021