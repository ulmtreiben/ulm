---
id: "635067124561258"
title: Dekolonialisierung des öffentlichen Raumes
start: 2021-07-08 20:00
link: https://www.facebook.com/events/635067124561258/
image: 201182633_2661400940825684_770273983673132710_n.jpg
isCrawled: true
---
In deutschen Städte wie Ulm und Neu-Ulm werden weiterhin Menschen und Begriffe durch die Benennung von Straßen und Plätzen gewürdigt und verehrt, die Kolonialisierung, Rassismus, Ausgrenzung, Krieg und Mord einleiteten und durchführten. Nicht nur für Betroffene und Hinterbliebene ist dies ein Schlag ins Gesicht, sondern es ist auch für die jeweiligen Kommunen eine Schande. Es wird höchste Zeit diese Verehrung zu hinterfragen und nach Alternativen zu suchen. Welche Erfahrungen und Handlungsmöglichkeiten gibt es diesbezüglich? Wo gibt es positive Beispiele und welche Hürden sind zu nehmen? Wo liegen die Zusammenhänge zwischen durch Kolonialisierung und die NS-Zeit belastete Plätze- und Straßennamen? Welche Begriffe sind besonders problematisch und wie fühlen sich betroffene Menschen, wenn sie mit solchen konfrontiert werden? Der Referent Hamado Dipama beschäftigt sich mit diesen Themen seit vielen Jahren und teilt gerne seine Erfahrungen aus München, Bayern und der gesamten Republik zu der mittlerweile breit geführten Diskussion. Mit dieser Veranstaltung soll in Ulm und Neu-Ulm eine Fortsetzung der Auseinandersetzung stattfinden, in dessen Folge es hoffentlich ebenfalls zu einer breiten, sachlichen und offenen Diskussion kommt, der ein konkretes Handeln folgt. 

Referent: Hamado Dipama, Referent bei AGABY (Arbeitsgemeinschaft der Ausländer-, Migranten und Integrationsbeiräte Bayerns) und MigraNet-IQ-Netzwerk Bayern

Link zur Veranstaltung: https://uni-ulm.zoom.us/j/6831655221?pwd=d2p0aktVbENpTitoSGlPeHFvcms4UT09
Meeting-ID: 683 165 5221
Kenncode: FCLR2021

#dekolonialisierung #fclrulm2021
____________________________________________

In German cities like Ulm and Neu-Ulm, people and concepts that initiated and carried out colonisation, racism, exclusion, war and murder continue to be honoured and revered by naming streets and squares. Not only is this a slap in the face for those affected and those left behind, but it is also a disgrace for the respective communities. It is time to question this veneration and to look for alternatives. What experiences and possibilities for action are there regarded to this? Which positive examples exist and what hurdles have to be overcome? What are the connections between the names of squares and streets that are burdened by colonisation and the Nazi era? Which terms are particularly problematic and how do affected people feel when confronted with them? The speaker Hamado Dipama has been dealing with these issues for many years and is happy to share his experiences from Munich, Bavaria and the entire republic on the now widely held discussion. This event is intended to continue the debate in Ulm and Neu-Ulm, hopefully leading to a broad, objective and open discussion followed by concrete action.

Speaker: Hamado Dipama, speaker at AGABY (Association of Foreigners, Migrants and Integration Councils of Bavaria) and MigraNet-IQ-Network Bavaria