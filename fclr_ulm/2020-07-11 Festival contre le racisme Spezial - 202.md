---
id: "312297413145125"
title: Festival contre le racisme Spezial - 2020
start: 2020-07-11 09:00
end: 2020-07-19 21:00
locationName: Festival Contre Le Racisme Ulm
address: Ulm
link: https://www.facebook.com/events/312297413145125/
image: 106582807_2398835930415521_4106030394831676860_o.jpg
teaser: "[DE:] Das festival contre le racisme Ulm/Neu-Ulm findet in diesem Jahr zum 5.
  Mal statt - diesmal unter besonderen Umständen. Auch wenn Veranstaltunge"
isCrawled: true
---
[DE:] Das festival contre le racisme Ulm/Neu-Ulm findet in diesem Jahr zum 5. Mal statt - diesmal unter besonderen Umständen. Auch wenn Veranstaltungen derzeit nur bedingt möglich sind, zeigt sich gerade während der Corona-Pandemie, wie wichtig es ist, über menschenverachtende Denk- und Handlungsweisen aufzuklären.

Unser Aufruf für eine buntere Gesellschaft: 

Derzeit werden Menschen aufgrund ihres Äußeren als vermeintliche Überträger*innen des Corona-Virus verstärkt diffamiert, bedroht und angegriffen. Gleichzeitig versammeln sich vielerorts regelmäßig Personen zu Demonstrationen gegen die Einschränkung von Grundrechten, vermeintlich aus Angst vor dem Verlust demokratischer Werte. Neben Personen, die sich ernsthaft um unsere Demokratie sorgen, verbreiten Rechtspopulist*innen auf diesen Demonstrationen ihre demagogischen Ansichten und menschenverachtenden Einstellungen. Extremistische und rassistische Ansichten sollen so verstärkt Eingang in den gesellschaftlichen Diskurs finden, um die Grenze des Sagbaren zu verschieben.

Kaum (mediale) Beachtung findet hingegen die Situation Geflüchteter an der türkisch-griechischen Grenze, in den Lagern auf den griechischen Inseln oder in Aufnahmezentren in Deutschland. Dabei leben gerade dort viele Menschen notgedrungen auf engstem Raum - ohne die Möglichkeit Abstand zu halten, geschweige denn mit den Möglichkeiten eines funktionierenden Gesundheitssystems.

Das Virus bedroht jedoch alle Personen auf der Welt gleichermaßen. Am Beispiel der Geflüchteten zeigt sich, wie gespalten unsere Gesellschaft in Wirklichkeit ist – nicht alle erfahren den gleichen Schutz oder haben die gleichen Rechte und Ressourcen zur Verfügung.

In Vergessenheit geraten im derzeitigen Ausnahmezustand zudem die rassistischen Übergriffe der letzten Monate: In Christchurch, El Paso und Hanau, der antisemitische Anschlag in Halle, die Ermordung Walter Lübkes sowie der rassistische Angriff in der Schaffnerstraße Ulm und der antiziganistische Tötungsversuch in Erbach. Diese Attentate verdeutlichen: Rassismus ist ein globales Problem, welches auch lokal in Ulm & Neu-Ulm zutage tritt – gerade auch in Zeiten von Corona.

Mit unseren Veranstaltungen und Aktionen möchten wir Rassismus entgegentreten und uns einsetzen für eine Gemeinschaft und ein Zusammenleben ohne Diskriminierung, Unterdrückung und Bedrohung.

Unter dem Motto „Solidarität jetzt“ wollen wir sensibilisieren, weiterbilden und dazu anregen, genau dafür einzustehen. Wir rufen dazu auf, sich solidarisch zu zeigen mit Betroffenen und gemeinsam vom 24. Juni – 19. Juli 2020 in verschiedenen digitalen und dezentralen Veranstaltungen wie Vorträgen, Lesungen und musikalischen Beiträgen ein Zeichen zu setzen. Für eine bunte Gesellschaft!

Alle Veranstaltungen und Informationen zur Teilnahme findet ihr hier auf Facebook, auf Instagram [fclr_ulm_2020] und auf unserer Homepage [www.fclr-ulm.de]. 

Wir freuen uns auf ein tolles, spannendes und lehrreiches Festival mit euch!

[EN]: The festival “contre le racisme Ulm/Neu-Ulm” will take place for the 5th time this year. Even if events are currently only possible to a limited extent, the importance of educating oneself about inhumane ways of thinking and acting is particularly evident during the corona pandemic.

Our appeal for a multicultural society

People are currently being increasingly defamed, threatened and attacked as alleged carriers of the corona virus, just because of their appearance. At the same time, people in many places regularly gather for demonstrations against the restriction of fundamental rights, supposedly out of fear of losing democratic rights. Apart from people who are seriously concerned about our democracy, right-wing populists spread their demagogic views and inhuman attitudes at these demonstrations. This way, extremist and racist views are intended to increasingly find their way into social discourse in order to shift the boundaries of what can be said.
Hardly any (media) attention is paid to the situation of refugees on the Turkish-Greek border, in camps on the Greek islands or in reception centres in Germany. However, it is precisely there that many people are forced to live in very confined spaces - without the possibility of keeping their distance, let alone with the means of a functioning health care system.
The virus threatens all people in the world equally

The example of the fugitives shows how divided our society really is - not everyone experiences the same protection or has the same rights and resources at their disposal.

In the current state of emergency, the racist attacks of recent months are also being forgotten: In Christchurch, El Paso and Hanau, the anti-Semitic attack in Halle, the murder of Walter Lübke as well as the racist attack in Schaffnerstraße Ulm and the antiziganist killing attempt in Erbach. These assassinations clearly demonstrate: Racism is a global problem, which also occurs locally in Ulm & Neu-Ulm – even so in times of Corona.
With our events and campaigns, we want to counter racism and work for a community and living together without discrimination, oppression and threat.
With the motto "Solidarity now" we want to sensitize, educate and encourage to stand up for exactly that. We call on you to show solidarity with those affected and to make a statement together from 11th  - 19th  July 2020 in various digital and decentralised events such as lectures, readings and musical features. For a colourful society!

All events and information about participation can be found here on Facebook, on Instagram [fclr_ulm_2020] and on our homepage [www.fclr-ulm.de]. 

We are looking forward to a great and exciting festival with you!
