---
id: "197734488910651"
title: Rassismuskritik (an) der Universität - Freundliche Sondierungen. Ein Vortrag
  von Prof. Paul Mecheril
start: 2021-08-18 18:00
link: https://www.facebook.com/events/197734488910651/
image: 236016660_2702279093404535_4159621640091171972_n.jpg
isCrawled: true
---
In seinem Vortrag beschäftigt sich Prof. Mecheril mit einigen Fragen, die mit einer rassismuskritischen Auseinandersetzung mit der Universität, verstanden als Institution der Schaffung wirksamen Wissens, verbunden sind. Drei Fragen werden in den Vordergrund gerückt: Welche Gründe sprechen für die Bedeutsamkeit einer Rassismuskritik der Universität? Was ist das allgemeine Anliegen der Rassismuskritik? Was kennzeichnet eine rassismuskritisch inspirierte Demokratisierung der Universität?

Referent: Prof. Paul Mecheril ist Professor für Erziehungswissenschaft mit dem Schwerpunkt Migration an der Fakultät für Erziehungswissenschaft der Universität Bielefeld. Schwerpunkte: methodologische und methodische Fragen interpretativer Forschung, Pädagogische Professionalität, migrationsgesellschaftliche Zugehörigkeitsordnungen, Macht und Bildung. 

Die Veranstaltung findet in Kooperation mit dem International Office der Uni Ulm statt.

Link zur Veranstaltung: uni-ulm.zoom.us/j/6831655221

Meeting-ID: 683 165 5221
Kenncode: FCLR2021

#fclr2021ulm
____________________________________________

In his lecture, Prof. Mecheril deals with some questions that are connected to a racism-critical examination of the university, understood as an institution for the creation of effective knowledge. Three questions will be brought to the fore: What are the reasons for the importance of a critique of racism in the university? What is the general concern of a critique of racism? What characterises a democratisation of the university inspired by a critique of racism?

Speaker: Prof. Paul Mecheril is Professor of Educational Science with a focus on migration at the Faculty of Educational Science at Bielefeld University. Main areas of research: methodological and methodological questions of interpretative research, pedagogical professionalism, migration-societal orders of belonging, power and education.

The event takes place in cooperation with the International Office of the University of Ulm.

link to the event: uni-ulm.zoom.us/j/6831655221

Meeting-ID: 683 165 5221
Kenncode: FCLR2021