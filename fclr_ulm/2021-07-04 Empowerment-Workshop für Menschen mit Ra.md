---
id: "345016740521381"
title: Empowerment-Workshop für Menschen mit Rassismuserfahrungen
start: 2021-07-04 10:00
address: online
link: https://www.facebook.com/events/345016740521381/
image: 197754769_2654201144878997_5091044221957813167_n.jpg
isCrawled: true
---
Gehören rassistische Äußerungen anderer und exkludierende Erfahrungen zu Ihrem Alltag? Stehen Ihre Kompetenzen aufgrund Ihres Namens oder Ihrer Religionszugehörigkeit nicht immer im Vordergrund?  Dann gehören Sie zu denen, die von rassistischen Macht- und Herrschaftsverhältnisse betroffen sind. Denn Rassismus ist ein gesamtgesellschaftliches Problem, welche die Haltung, das Denken sowie die Sprache von allen Menschen beeinflusst.  Dieser Workshop ermöglicht Ihnen in einem „geschützten Raum – Safer Space“ sich mit diesen gesellschaftlichen Machtverhältnissen auseinanderzusetzen. Dabei verbinden sich Übungen aus Achtsamkeit, Erfahrungsaustausch und Erarbeitung von Handlungsstrategien für den Umgang mit Rassismuserfahrungen zu einem ganzheitlichen Konzept. Dadurch lernen Sie Rassismus wahrzunehmen und zu benennen, Respekt einzufordern und rassistische Diskriminierung jeglicher Art eindeutig abzuwehren. So können Sie anhand von Körper- und Reflexionsübungen die Macht von Empowerment in der Gruppe erleben. 

Die Veranstaltung richtet sich ausschließlich an Menschen mit Rassismuserfahrungen. 

Referentin: Hatice Avci, Soziale Arbeit M.A., Systemische Beraterin, Trainerin für Empowerment & rassismuskritische Haltung.

Die Veranstaltung findet statt in Kooperation mit Hatice Avci.

Wir freuen uns über Anmeldungen bis zum 21.06.2021 per Mail an fclr.anmeldung@uni-ulm.de  