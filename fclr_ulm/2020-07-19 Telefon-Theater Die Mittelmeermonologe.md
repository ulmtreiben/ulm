---
id: "1240136202994695"
title: "Telefon-Theater: Die Mittelmeermonologe (mit Anmeldung)"
start: 2020-07-19 18:00
end: 2020-07-19 21:30
locationName: Festival Contre Le Racisme Ulm
address: Ulm
link: https://www.facebook.com/events/1240136202994695/
image: 107421367_164519391790082_420778415770395989_o.jpg
teaser: "Telefon-Theater: Die Mittelmeer- Monologe (mit Anmeldung!)   English version
  below!  Das dokumentarische Theaterstück „Die Mittelmeer-Monologe“ vom Au"
isCrawled: true
---
Telefon-Theater: Die Mittelmeer- Monologe (mit Anmeldung!) 

English version below!

Das dokumentarische Theaterstück „Die Mittelmeer-Monologe“ vom Autoren und Regisseuren Michael Ruf ist wortgetreues Theater zum Thema Flucht und Seenotrettung. Die Texte stammen aus Interviews, welche weder inhaltlich noch in ihrer sprachlichen Ausdrucksweise verändert wurden. Es werden die Geschichten erzählt von den politisch Widerständigen Naomie aus Kamerun und Yassin aus Libyen, sowie von den Aktivist*innen Selma und Joe, die dem Sterben auf dem Mittelmeer etwas entgegensetzen. 
Einen dieser Monologe kannst du am 19.07.2020 zwischen 18 – 21.15 Uhr live an deinem Telefon erleben. Du wirst erst von einem*r der Schauspieler*innen angerufen und hörst dein exklusives Telefon-Theater mit der Möglichkeit danach in Austausch zu kommen.

Anschließend kannst du in einem zweiten Telefonat im 1:1-Gespräch über deine Eindrücke, Fragen und Gedanken mit (lokalen) Aktivist*innen oder auch dem Regisseuren Michael Ruf sprechen. 

Möchtest du mehr zum Stück, zu den gesprochenen oder den sprechenden Menschen an deinem Telefon hören? Wie ist die aktuelle Situation an Europas Außengrenzen? Welche Möglichkeiten gibt es aktiv zu werden?

Schreibe bis zum 18.07. eine Mail an theater@wort-und-herzschlag.de mit deinem Namen, deiner Telefon-Nummer, aus welchem Ort du anrufst (optional), ob wir dich mit einer Person, die als Muttersprache Englisch/Französisch spricht einteilen könnten, und erwähne wenn du zwischen 18.00 und 21.15 Uhr zeitliche Einschränkungen hast und wir antworten dir, in welcher Stunde und von wem du mit zwei Anrufen rechnen kannst!

Die Veranstaltung "Mittelmeer-Monologe" wird gefördert von: 
UNO-Flüchtlingshilfe, Engagement Global - Service für Entwicklungsinitiativen im Auftrag Bundesministerium für wirtschaftliche Zusammenarbeit und Entwicklung (BMZ) und Brot für die Welt (mit Mitteln des Kirchlichen Entwicklungsdienstes).

[EN]: The documentary play "The Mediterranean Monologues" by author and director Michael Ruf is literal theatre about escape and sea rescue. The texts are taken from interviews, which have not been changed in terms of content or language. The play tells the story of the politically resistant Naomie from Cameroon and Yassin from Libya, as well as of the activists Selma and Joe, who oppose dying on the Mediterranean Sea. 

You can experience an excerpt of this live on your phone on 19/07/2020 within one hour between 18 - 21.15 h. You will first be called by one of the actresses, who will tell you the stories of Naomie, Selma, Yassin and Joe.
Afterwards you can talk about your impressions, questions and thoughts with (local) activists or the director Michael Ruf.
Would you like to hear more about the play, the spoken roles, or the people on your phone? What is the current situation at Europe's external borders? What possibilities are there to become active?
Write an email to theater@wort-und-herzschlag.de by 18/07. with your name, your phone number, from which city you are calling (optional), if you have knowledge in German or French and mention if you have time restrictions between 18.00 and 21.15 and we will answer you when and from whom you can expect two calls!

The event "Mediterranean Monologues" is supported by: 
UNO-Flüchtlingshilfe, Engagement Global - Service für Entwicklungsinitiativen im Auftrag des Bundesministerium für Wirtschaftliche Zusammenarbeit und Entwicklung und Brot für die Welt (mit Mitteln des Kirchlichen Entwicklungsdienstes).