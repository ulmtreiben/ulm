---
id: "551905162649804"
title: "Ausstellung: WE ARE PART OF CULTURE"
start: 2021-07-17 10:00
end: 2021-07-17 20:00
locationName: Glacis-Galerie
address: Bahnhofstraße 1/3, 89231 Neu-Ulm
link: https://www.facebook.com/events/551905162649804/
image: 204077865_2666166640349114_2954116865492344804_n.jpg
isCrawled: true
---
Die Kunstausstellung WE ARE PART OF CULTURE (WAPOC) zeigt queere Persönlichkeiten von der Antike bis heute, welche die europäische Gesellschaft, unsere Kultur nachhaltig geprägt haben. Die Portraits der Persönlichkeiten wurden von national und international bekannten Künstler*innen speziell für die WAPOC geschaffen. Die Ausstellung fördert Vielfalt, Respekt und Akzeptanz. Sie schafft Vorbilder und Identifikationsfiguren, und zeigt: lesbische, schwule, bisexuelle, trans*, inter* und queere Personen haben schon immer unsere Gesellschaft entscheidend mitgeprägt.

WE ARE PART OF CULTURE wurde 2017 – 2019 vom Bundesministerium für Familie, Senioren, Frauen und Jugend im Rahmen des Bundesprogramms „Demokratie leben!“ sowie der Bundeszentrale für politische Bildung gefördert. 

Seit 2017 erfolgen Ausstellungen in Bahnhöfen, Museen, Firmen, Rathäusern und weiteren öffentlichen Räumlichkeiten. Nun ist die Ausstellung in der Glacis Galerie Neu-Ulm im Rahmen des fclr zu sehen.

Die Veranstaltung wird durchgeführt in Kooperation mit 100%Mensch.

#wapoc #fclr2021ulm
____________________________________________

Persecution, murder, exclusion, bullying and discrimi-nation characterise the world-wide situation of homosexual people and people with gender issues. Again and again, LGBTIQ* people become victims of physical and psychological violence. As we struggle for equality, acceptance and respect, we must make sure that those crimes are being prosecuted, that the unequal treatment is made visible, that we put a face to the victims and make their stories heard. However, this focus is lopsided for it assigns LGBTIQ* people one role, and one role only: the victim.  Our large art exhibition WE ARE PART OF CULTURE, created by the non-profit organisation Project 100% HUMAN (Projekt 100% MENSCH), aims to broaden this picture: we are more than victims! We have played an active and positive part in the creation of society, culture, politics, and the sciences. We made history!