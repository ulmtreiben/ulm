---
id: "527617475096746"
title: "Überlebende erzählen: Flucht nach Europa * Festival Contre Le Racisme * ROXY
  Sound Garten"
start: 2021-06-30 16:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/527617475096746/
image: 204527417_10158609532537756_7624852409157777355_n.jpg
isCrawled: true
---
Eintritt frei! 💛

Der Sound Garten bleibt wegen des Wetters heute leider geschlossen :(
Aber Achtung: Der Vortrag "Überlebende erzählen: Flucht nach Europa" im Rahmen des Festival Contre Le Racisme Ulm  findet trotzdem statt - nur jetzt halt eben in unserer Cafébar. 
Der Einlass erfolgt über das Foyer (da, wo ihr zu regulären Veranstaltungen reinkommt), der Eintritt ist kostenlos und ihr braucht keinen Test-, Impf- oder Genesenennachweis. Im Haus herrscht Maskenpflicht. Wir registrieren eure Kontaktdaten per luca-App oder Kontaktdatenformular. In der Gastro gibt es keinen Service, nur Selbstbedienung.


Seit 2015 sind Begriffe wie „Seenotrettung“ und „Mittelmeerroute“ allgemein bekannte Begriffe, doch häufig beschäftigen wir uns nur oberflächlich und fragmentarisch mit den Schicksalen, die sich dahinter verbergen. Wir wollen ein Licht darauf werfen, was Geflüchtete erleben, nachdem sie in Europa angekommen sind. Wie geht es hier weiter, welchen Schwierigkeiten begegnen sie auf ihrem Weg zu Fuß nach Deutschland und wie erleben sie ihren Alltag hier? Wir wollen deshalb zu einem Abend des Austauschs einladen und uns die Erlebnisse dreier Geflüchteter und einer ehrenamtlichen Helferin im Geflüchtetenlager auf Lesbos anhören.

Anschließend wird es die Möglichkeit geben, sich mit Aktivist*innen von Hilfsorganisationen oder lokalen Initiativen auszutauschen. 


www.fclr-ulm.de/blog