---
id: "988408401902243"
title: "Überlebende erzählen: Flucht nach Europa"
start: 2021-06-30 18:00
end: 2021-06-30 20:30
address: Roxy Ulm
link: https://www.facebook.com/events/988408401902243/
image: 199449412_2660899877542457_3974656630832910298_n.jpg
isCrawled: true
---
 📅 30.06.21 ⌚ 18:00 - 20:30 📌 Roxy Ulm 💬 deutsch

Seit 2015 sind Begriffe wie „Seenotrettung“ und „Mittelmeerroute“ allgemein bekannte Begriffe, doch häufig beschäftigen wir uns nur oberflächlich und fragmentarisch mit den Schicksalen, die sich dahinter verbergen. Wir wollen ein Licht darauf werfen, was Geflüchtete erleben, nachdem sie in Europa angekommen sind. Wie geht es hier weiter, welchen Schwierigkeiten begegnen sie auf ihrem Weg zu Fuß nach Deutschland und wie erleben sie ihren Alltag hier? Wir wollen deshalb zu einem Abend des Austauschs einladen und uns die Erlebnisse dreier Geflüchteter und einer ehrenamtlichen Helferin im Geflüchtetenlager auf Lesbos anhören.

Anschließend wird es die Möglichkeit geben, sich mit Aktivist*innen von Hilfsorganisationen oder lokalen Initiativen auszutauschen. 

In Kooperation mit dem Roxy Ulm.

Es gelten die aktuellen Hygienevorschriften des Roxys.

#fclr2021 #roxy
____________________________________________

 Survivers´Report: Flight to Europe

📅 30.06.21 ⌚ 6-8.30pm 📌 Roxy Ulm 💬 German

 Since 2015, terms like "sea rescue" and "Mediterranean route" have become common knowledge, but we often only deal superficially and fragmentarily with the fates behind them. We want to shed light on what refugees experience after they arrive in Europe. What happens next, what difficulties do they encounter on their way to Germany on foot and how do they experience their everyday life here?

Therefore, we want to invite you to an evening of exchange and listen to the experiences of three refugees and a volunteer helper in the refugee camp on Lesbos. Afterwards, there will be the opportunity to exchange ideas and ask questions with activists from aid organisations or local initiatives.

In Cooperation with: Roxy Ulm

The current hygiene regulations of the Roxy apply.