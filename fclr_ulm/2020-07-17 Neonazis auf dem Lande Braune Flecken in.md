---
id: "596006181317948"
title: "Neonazis auf dem Lande: Braune Flecken in Allgäuer Ökoszene"
start: 2020-07-17 19:30
end: 2020-07-17 20:30
link: https://www.facebook.com/events/596006181317948/
image: 106454218_2398860957079685_1288418713878990317_o.jpg
teaser: "[DE]: Bio, braun und barfuß. Unter diesem Titel deckte eine investigative
  Reportage des ARD im Mai 2019 den rechtsradikalen Hintergrund der Anastasiab"
isCrawled: true
---
[DE]: Bio, braun und barfuß. Unter diesem Titel deckte eine investigative Reportage des ARD im Mai 2019 den rechtsradikalen Hintergrund der Anastasiabewegung auf. Dass sich Neonazis und andere rechtsradikale Kräfte mit Hof- und Siedlungsprojekten in ländlichen Gebieten Stützpunkte ihrer völkischen Ideologie errichten ist nicht neu. Auch der quasi-religiöse Überbau den die Anastasiabewegung für eben diese Ideologie bietet ist in anderer Form bereits seit der Nachkriegszeit innerhalb der neonazistischen Artgemeinschaft zu finden. Doch dass sich solche Bewegungen inzwischen auch in der hiesigen Ökoszene ausbreiten und der ein oder andere Allgäuer Neonazi den netten Bauern von nebenan mimt, dürfte einigen neu sein. Radikale Rechte versuchen, sich  innerhalb der ökologischen und esoterische Szene zu verankern und dort Menschen für ihre völkische und rassistische Ideologie zu gewinnen. Hierbei verzeichnet die rechtsradikale Bewegung auch im Allgäu teils ernstzunehmende Erfolge. Der Vortrag soll Orientierungspunkte für eine dringend nötige Debatte innerhalb der ökologischen Szene im Allgäu über den Umgang mit völkischen und neonazistischen Gruppen bieten. 

Anmeldung bitte unter webinar-fclr@allgaeu-rechtsaussen.de. Ihr bekommt dann weitere Informationen zur Teilnahme per Mail. 

[EN]: Organic, brown and barefoot. This was the title of an investigative report on ARD television in May 2019, which revealed the right-wing extremist background of the Anastasia movement. That neo-Nazis and other right-wing extremist forces are establishing bases for their national ideology with farm and settlement projects in rural areas is nothing new. Also the quasi-religious superstructure that the Anastasia movement offers for just this ideology can be found in another form within the neo-Nazi community of species since the post-war period. 
But the fact that such movements are now spreading in the local eco-scene and that one or the other neo-Nazi from the Allgäu is playing the nice farmer from next door, might be new to some. Radical right-wingers are trying to anchor themselves within the ecological and esoteric scene and to win people over to their folk and racist ideology. The radical right-wing movement has had some serious successes in the Allgäu region. The lecture is intended to provide points of reference for an urgently needed debate within the ecological scene in the Allgäu on how to deal with neo-Nazi groups.

If you want to participate please register at webinar-fclr@allgaeu-rechtsaussen.de. You will then receive further information about participation by mail. 