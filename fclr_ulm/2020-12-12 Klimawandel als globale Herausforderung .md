---
id: "1398259523839061"
title: Klimawandel als globale Herausforderung - eine kritische venezolanische
  Perspektive
start: 2020-12-15 18:00
link: https://www.facebook.com/events/1398259523839061/
image: 130341499_2530781740554272_5285128090418395134_o.jpg
teaser: Der Vortrag wirft einen kritischen Blick auf die Auswirkungen des Klimawandels
  in Venezuela, die öffentliche Meinung zur Klimafrage, das Handeln und N
isCrawled: true
---
Der Vortrag wirft einen kritischen Blick auf die Auswirkungen des
Klimawandels in Venezuela, die öffentliche Meinung zur Klimafrage,
das Handeln und Nichthandeln der Regierung und die
Grassroot-Bewegungen gegen den Klimawandel, die sich aus der
turbulenten politischen, wirtschaftlichen und sozialen Lage ergeben.

Referent: Alejandro Ceballos

Link zur Veranstaltung: https://uni-ulm.webex.com/uni-ulm-de/j.php?MTID=m959176a0e5205e1359da28393ce5f802

Meetingpasswort: 174 086 8760