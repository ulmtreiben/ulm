---
id: "290107006130023"
title: Film – Masel Tov Cocktail
start: 2021-07-01 20:30
end: 2021-07-01 21:15
locationName: Bürgerhaus Mitte
address: Schaffner Straße, 89073 Ulm
link: https://www.facebook.com/events/290107006130023/
image: 199687272_2661382937494151_815439910168091256_n.jpg
isCrawled: true
---
Dimitrij Liebermann (19) ist Jude und hat Tobi geschlagen. Dafür soll er sich entschuldigen. Nur Leid tut es ihm nicht unbedingt.

Zutaten: 1 Jude, 12 Deutsche, 5cl Erinnerungskultur, 3cl Stereotype, 2 TL Patriotismus, 1 TL Israel, 1 Falafel, 5 Stolpersteine, einen Spritzer Antisemitismus
Zubereitung: Alle Zutaten in einen Film geben, aufkochen lassen und kräftig schütteln. Im Anschluss mit Klezmer-Musik garnieren.
Verzehr: Vor dem Verzehr anzünden und im Kino genießen. 100% Koscher.

Die Veranstaltung findet statt in Kooperation mit der Filmakademie Baden-Württemberg.

Platzzahl begrenzt. Um rechtzeitiges Erscheinen wird gebeten.

Adresse: Bürgerhaus Mitte, Schaffnerstraße 17, 89073 Ulm

Nachweis der 3G's notwendig: getestet, geimpft, genesen.

Freier Eintritt.

#maseltovcocktail #fclr2021ulm
____________________________________________

Dimitrij Liebermann (19) is Jewish and beat Tobi. He should apologize for that. But he is not necessarily sorry.

Ingredients: 1 Jew, 12 Germans, 5cl culture of remembrance, 3cl stereotypes, 2 tsp patriotism, 1 tsp Israel, 1 falafel, 5 stumbling blocks, a dash of anti-Semitism
Preparation: put all ingredients in a film, bring to a boil and shake vigorously. Garnish with Klezmer music afterwards.
Consumption: Light before consumption and enjoy in the cinema. 100% Kosher.

The event takes place in cooperation with the Filmakademie Baden-Württemberg.

Limited number of seats. We kindly ask you to come in time.

Adress: Bürgerhaus Mitte, Schaffnerstraße 17, 89073 Ulm

Entry only when tested, vaccinated or recovered.

Free entry.