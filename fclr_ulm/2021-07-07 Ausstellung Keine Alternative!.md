---
id: "606104246956006"
title: "Ausstellung: Keine Alternative!"
start: 2021-07-07 08:00
end: 2021-07-07 16:00
address: Universität Ulm Mensa Süd
link: https://www.facebook.com/events/606104246956006/
image: 203204780_2666188093680302_1072394010517143982_n.jpg
isCrawled: true
---
Die Ausstellung „Keine Alternative!“ der VVN BdA (Vereinigung der Verfolgten des Naziregimes - Bund der Antifaschistinnen und Antifaschisten) analysiert die Ideologie der „Alternative für Deutschland“ und die Beziehungen dieser Partei zu einer breiten völkischen Bewegung in Deutschland.

Die Veranstaltung wird durchgeführt in Kooperation mit dem VVN-BdA im Rahmen des Festival contre le racisme Ulm/Neu-Ulm.

Es gelten die vor Ort gültigen Hygienemaßnahmanen.