---
id: "200754474876074"
title: „You can be happy at home”
start: 2020-12-09 19:30
link: https://www.facebook.com/events/200754474876074/
image: 129294162_2527062724259507_2568869669343955090_o.jpg
teaser: Zum Auftakt unserer Veranstaltungsreihe "17 Ziele für Ulm und die Welt" hält
  Marlene Gärtner (Kulturwissenschaftlerin an der Uni Konstanz) einen Vortr
isCrawled: true
---
Zum Auftakt unserer Veranstaltungsreihe "17 Ziele für Ulm und die Welt" hält Marlene Gärtner (Kulturwissenschaftlerin an der Uni Konstanz) einen Vortrag unter dem Titel:
„You can be happy at home” -
Deutungskämpfe über Glück, Migration und das ‚gute Leben‘ in EU-finanzierten ‚Informationskampagnen‘ in Afrika

Zugangslink: https://uni-ulm.webex.com/webappng/sites/uni-ulm/meeting/download/3963abd6e5674929a3eb9ade0ccde45d?MTID=m5896495acb768836ce5873ff0d7d5309&siteurl=uni-ulm
Meeting-ID: 174 580 0806 
Passwort: KiSDPJD92Z2

Inhalt
Neben verstärkter Grenzsicherung setzen die Europäische Union und ihre Mitgliedsstaaten in Afrika zunehmend auf ‚externes Migrationsmanagement‘. Eine Komponente davon sind sogenannte ‚Informations- und Aufklärungskampagnen‘, die über die Risiken ‚irregulärer‘ Migration durch die Wüste und über das Mittelmeer aufklären sollen. Die Grundannahme ist, dass junge Afrikaner*innen sich auf der Basis von gezielten Informationen zu Schwierigkeiten und Herausforderungen von Migration gegen Auswanderung entscheiden werden. Der Vortrag zeigt, warum diese Auffassung falsch ist und erläutert, warum diese schematisierten „Informationskampagnen“ dennoch seit fast 30 Jahren in verschiedenen geographischen Kontexten eingesetzt werden.  
Marlene Gärtner wirft am Beispiel von zwei EU-finanzierten Kampagnen aus Kamerun und Interviewausschnitten aus ihrer Forschung einen genauen Blick auf dominante politische Wahrnehmungsmuster im Globalen Norden, so z.B. die eindimensionale Vorstellung von afrikanischer Migration nach Europa als Krise und als Bedrohung. Die Selbsterzählungen vieler Afrikaner*innen zeichnen ein anderes Bild: Für sie ist Migration nach Europa ein erfolgsversprechender Weg für den sozialen Aufstieg der eigenen Familie, der anderweitig kaum möglich ist. Hier kollidieren also unterschiedliche Vorstellungen davon, was es bedeutet ein ‚gutes Leben‘ zu führen und wer das Recht dazu hat, diese Träume außerhalb des eigenen Geburtslandes zu verwirklichen. Die Kampagnen sind eine Versuch, gegen die machtvolle Erzählung vom „Paradies Europa“ anzuerzählen. Doch die Slogans bleiben den sozialen Realitäten Kameruns gegenüber ignorant und unterschätzen die Orientierungs- und Wirkmacht von Migrationshoffnungen. Im afrikanischen Kontext erzählen sie keine ‚gute Geschichte‘ und bleiben daher weitgehend wirkungslos.    
Marlene Gärtner ist Kulturwissenschaftlerin (Universität Konstanz) und forscht für ihre Doktorarbeit zu alltäglichen Erzählungen über Migration in Kamerun und der kamerunischen Diaspora. 