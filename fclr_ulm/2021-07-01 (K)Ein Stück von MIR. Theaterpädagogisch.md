---
id: "532288037922400"
title: ABGESAGT - (K)Ein Stück von MIR. Theaterpädagogischer Workshop mit
  Schüler:innen
start: 2021-07-01 14:00
end: 2021-07-01 17:00
locationName: Münsterplatz Ulm
address: Münsterplatz 21, 89073 Ulm
link: https://www.facebook.com/events/532288037922400/
image: 202159722_2661425010823277_2614080468683083751_n.jpg
isCrawled: true
---
Kennst du noch die letzten 5 Schlagzeilen rassistischer Gewalt, die du gelesen hast? Kannst du dich noch an die jeweiligen Gesichter erinnern? Die Identitäten der Opfer verschwinden in der Masse und verblassen. Doch was geschieht, wenn du ihnen auf einmal im Spiegel begegnest?

Die Veranstaltung findet statt in Kooperation mit 'Mein Ich gegen Rassismus'.

Der Workshop besteht aus einem geschlossenen Teilnehmer:innenkreis.

#meinichgegenrassimus #fclr2021ulm
____________________________________________

Do you remember the last 5 headlines of racist violence you read? Can you still remember their faces? The identities of the victims disappear into the masses and fade away. But what happens when you suddenly meet them in the mirror?

The event takes place in cooperation with 'Mein Ich gegen Rassismus'.

The workshop consists of a closed circle of participants.