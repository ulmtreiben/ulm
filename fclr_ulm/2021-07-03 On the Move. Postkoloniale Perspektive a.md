---
id: "3073461556276723"
title: On the Move. Postkoloniale Perspektive auf Migration
start: 2021-07-03 14:00
end: 2021-07-03 17:00
link: https://www.facebook.com/events/3073461556276723/
image: 206364163_2670676656564779_1678320499928556729_n.jpg
isCrawled: true
---
📅 03.07.21 ⌚ 14:00 - 17:00 📌 Online 💬 deutsch

Teil 2 der Workshopreihe zu Spuren der (deutschen) Kolonialgeschichte 

Welche Machtverhältnisse aus der Kolonialzeit wirken noch heute nach? Ist es Zufall, dass zahlreiche Herkunftsländer von Migrierenden ehemalige Kolonien sind? Anhand eines interaktiven Zeitstrahls decken wir (weitere) blinde Flecken der deutschen Kolonialgeschichte auf. Dabei werden wirtschaftliche, politische und landrechtliche Fragen aufgeworfen, Kontinuitäten erkannt und deren Auswirkungen auf gegenwärtige Flucht- und Migrationsbewegungen untersucht. Im Fokus stehen dabei die drei ehemaligen deutschen Kolonialgebiete Tansania (ehemals Deutsch-Ostafrika), Namibia (ehemals Deutsch Südwestafrika) und Kamerun. Abschließend werden wir die Debatte um koloniale Relikte im Stadtbild, wie Straßenschilder oder Denkmäler, aufgreifen.

Eine Teilnahme am ersten Teil der Workshopreihe, der Veranstaltung "Vergangen und doch gegenwärtig" am 02. Juli ist eine gute Grundlage, allerdings keine Voraussetzung.

Über eine Anmeldung freuen wir uns per Mail an fclr.anmeldung@uni-ulm.de

Referentinnen: Charlotte Müller, Referentin bei fernsicht im iz3w und Rebekka Schön, Referentin des Programms Bildung trifft Entwicklung

#fclr2021ulm #iz3w  
____________________________________________

On the Move. Postcolonial perspective on migration. Part 2 of the workshop series on traces of (German) colonial history

📅 03.07.21 ⌚ 2-5 pm 📌 Online 💬 German

What power relations from the colonial era still have an impact today? Is it a coincidence that many countries of origin of are former colonies? Based on an interactive timeline, we will uncover (further) blind spots of German colonial history. Thereby, economic, political and land-law questions will be raised, continuities identified and their effects on current refugee and migration movements will be examined. We focus on the three former German colonial territories of Tanzania (former ‚Deutsch-Ostafrika‘), Namibia (former ‚Südwestafrika‘) and Cameroon. Finally, we will take up the debate on colonial relics in the cityscape, such as street signs or monuments.

Participation in the first part of the workshop series, the ‚iz3w‘ event on 02 July, is a good basis, but not a prerequisite.

We look forward to receiving your registration via Email to fclr.anmeldung@uni-ulm.de

Speaker: Charlotte Müller, speaker at fernsicht in iz3w and Rebekka Schön, speaker of the programme Bildung trifft Entwicklung.