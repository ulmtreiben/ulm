---
id: "384591376108577"
title: Wohnen fürs Leben, nicht für 			die Rendite! - Von Widerstand und Utopie.
start: 2020-12-11 18:30
link: https://www.facebook.com/events/384591376108577/
image: 130451201_2528446080787838_3874262832081672202_o.jpg
teaser: Wohnen fürs Leben, nicht für die Rendite! - Von Widerstand und
  Utopie. 			========================== (ein Abend mit dem Wurzel
  Kollektiv) 			 Mieten i
isCrawled: true
---
Wohnen fürs Leben, nicht für die Rendite! - Von Widerstand und Utopie.
			========================== (ein Abend mit dem Wurzel Kollektiv)
			
Mieten ist oft ein K(r)ampf und wir ackern für die Rendite. Das muss nicht so sein.
In diesem Workshop wollen wir mit Euch die Lage sichten und reale Utopien spinnen und kennenlernen.
			
Woher kommt die Wohnraumknappheit? Wieso steigen die Mieten immer weiter? Und wem gehören eigentlich die Wohnungen? 
Entlang dieser	Fragen werden wir uns orientieren und Alternativen zum aktuellen	Vorgehen darstellen.

Link zur Veranstaltung:
https://uni-ulm.webex.com/uni-ulm-de/j.php?MTID=m53eb930dc068e31bea11b1f3c2889a56 

Meeting-Kennnummer: 174 547 0776
Passwort: gmKu9pQcN52