---
id: "2839942652986121"
title: Rassismus geht unter die Haut
start: 2021-06-29 19:00
locationName: Ulmer Volkshochschule
address: Kornhausplatz 5, 89073 Ulm
link: https://www.facebook.com/events/2839942652986121/
image: 200758349_2661377257494719_384667241409544186_n.jpg
isCrawled: true
---
Stereotype, Vorurteile und Rassismus sind Konzepte, die das Denken von Menschen über andere Personen und soziale Gruppen beeinflussen. Die Wirkung von Stereotypen, Vorurteilen und Rassismus ist jedoch nicht auf kognitive Prozesse beschränkt.

In diesem Vortrag wird ein Einblick in aktuelle sozialpsychologische Studien gegeben, die sich mit der Wirkung von Stereotypen, Vorurteilen und Rassismus auf physiologische Prozesse beschäftigt haben und eindrucksvoll belegen, dass Rassismus in der Tat unter die Haut geht und den körperlichen Zustand sowie das Überleben der betroffenen Personen beeinflussen bzw. beeinträchtigen kann.

Referent: Prof. Dr. Johannes Keller (Universität Ulm, Institut für Psychologie & Pädagogik, Sozialpsychologie)

Die Veranstaltung findet in Kooperation mit der vh ulm statt.

Adresse: Volkshochschule Ulm, Club Orange, Kornhausplatz 5, 89073 Ulm

Es gelten die vor Ort gültigen Hygienmaßnahmen.

#fclrulm2021
____________________________________________

Stereotypes, prejudice and racism are concepts that influence how people think about other people and social groups. However, the effect of stereotypes, prejudice and racism is not limited to cognitive processes.

This lecture provides an insight into recent social psychological studies that have looked at the effect of stereotypes, prejudice and racism on physiological processes and provide impressive evidence that racism does indeed get under the skin and can affect or impair the physical condition and survival of the individuals concerned.

Speaker: Prof. Dr. Johannes Keller (University of Ulm, Institute for Psychology & Education, Social Psychology).

The event takes place in cooperation with vh ulm.

Adress: Volkshochschule Ulm, Club Orange, Kornhausplatz 5, 89073 Ulm

The hygiene measures valid on site apply.