---
id: "677934419579684"
title: 17 Nachhaltigkeitsziele und die Klimakrise
start: 2020-12-12 17:00
link: https://www.facebook.com/events/677934419579684/
image: 129865266_2528458214119958_4687047967484428147_o.jpg
teaser: Am Samstag ab 17 Uhr hält FFF Ulm/Neu-Ulm im Rahmen unserer
  Veranstaltungsreihe "17 Ziele für Ulm und die Welt" einen Vortrag, in dem sie
  euch erkläre
isCrawled: true
---
Am Samstag ab 17 Uhr hält FFF Ulm/Neu-Ulm im Rahmen unserer Veranstaltungsreihe "17 Ziele für Ulm und die Welt" einen Vortrag, in dem sie
euch erklären, wie sich jeder Punkt der 17 UN-Nachhaltigkeitsziele auf die Klimakrise auswirkt bzw. die Klimakrise auf sie. Außerdem erklären sie euch, welche eine Chance eine max. 1,5 Grad wärmere Welt für uns bietet.⁠
⁠
Neugierig geworden? Hier der Link zum Vortrag (Microsoft Teams): https://fffutu.re/b7Rpx8