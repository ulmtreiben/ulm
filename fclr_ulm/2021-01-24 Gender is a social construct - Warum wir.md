---
id: "236294081287556"
title: Gender is a social construct - Warum wir Geschlecht als binäre Kategorie bei
  der Formulierung von Nachhaltigkeitszielen überdenken sollten
start: 2021-01-24 19:00
link: https://www.facebook.com/events/236294081287556/
image: 141097759_2560025817629864_4178971629245182779_o.jpg
teaser: Der   Vortrag   gibt   eine   Einführung   in   das   Thema"Geschlechtliche
  Vielfalt" und möchte Impulse geben, warumexplizit   das   Nachhaltigkeitsz
isCrawled: true
---
Der   Vortrag   gibt   eine   Einführung   in   das   Thema"Geschlechtliche Vielfalt" und möchte Impulse geben, warumexplizit   das   Nachhaltigkeitsziel   5   "Gleichheit   derGeschlechter" über eine Gleichstellung von Mann und Frauhinausgehen sollte.