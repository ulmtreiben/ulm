---
id: "132982868839882"
title: Rassismus an den Außengrenzen Europas - Vortrag von Erik Marquardt
start: 2021-07-07 19:00
link: https://www.facebook.com/events/132982868839882/
image: 201139390_2661414230824355_5168760205447073650_n.jpg
isCrawled: true
---
Menschen ertrinken im Mittelmeer, müssen in menschenunwürdigen Lagern leben oder werden mit Gewalt daran gehindert, Asylanträge zu stellen. Das ist inzwischen Alltag an den Europäischen Außengrenzen. Doch das ist natürlich kein Zufall, denn statt auf Menschenrechte und Solidarität setzen die EU-Staaten in der Migrationspolitik auf Abschreckung und Abschottung. Erik Marquardt ist grüner Europaabgeordneter und beschäftigt sich seit Jahren mit der Situation an den Außengrenzen. Er wird von der Lage vor Ort berichten und sie politisch einordnen.

Referent: Erik Marquardt (Abgeordneter des Europaparlaments)

Link zur Veranstaltung: uni-ulm.zoom.us/j/6831655221
Meeting-ID: 683 165 5221
Kenncode: FCLR2021

#erikmarquardt #fclrulm2021