---
id: "547438169610661"
title: "Ausstellung: Was ihr nicht seht!"
start: 2021-06-21 00:00
end: 2021-06-30 03:00
address: Hafenbad 18 & 18/1, 89073 Ulm
link: https://www.facebook.com/events/547438169610661/
image: 204119999_2666186207013824_3129083328941334442_n.jpg
isCrawled: true
---
„Was wir erlebt haben, wissen viele sicher nicht, weil man es nicht sieht. Daher der Name „Was ihr nicht seht!“. Ich will mit dem Projekt Menschen zum Nach- und Umdenken bringen. Ich hoffe, dassweiße Menschenverstehen – und hoffentlich gemeinsam mit uns für eine anti-rassistische Zukunft kämpfen.“  - Dominik Lucha

Die Ausstellung zeigt die Arbeit „Was ihr nicht seht!“ von Dominik Lucha. Gemeinsam mit hunderten Schwarzen Menschen in Deutschland macht Dominik sichtbar, was oft ungesehen bleibt: „Was ihr nicht seht!“ thematisiert auf eindrückliche und zugängliche Weise den Alltagsrassismus, den Schwarze Menschen und People of Color in Deutschland erleben.

Im Juni 2020, nach der Ermordung George Floyds und den BlackLivesMatter-Protesten, startete Dominik Lucha das Projekt auf Instagram und es hat mittlerweile über 130 Tsd. Follower:innen. Auf dem Insta-Kanal können Schwarze Menschen anonym über ihre Rassismus-Erfahrungen in Deutschland berichten — und weiße Menschen können lernen, antirassistisch zu werden.

Mit „Was ihr nicht seht!“ wurde eine Plattform geschaffen, die Rassismus in Deutschland bezeugt und unübersehbar verdeutlicht, dass diese Erfahrungen eben so viel mehr sind, als individuelle Einzelschicksale. Mit Ausstellungen im Garten des StadtPalais – Museum für Stuttgart sowie an drei Orten in Biberach und einer Medieninstallation in den Ravensburger Schaufenstern, wurden die die tausend Zitate auch offline und im städtischen Raum sichtbar. Nun ist die Ausstellung von 21.06. – 18.07.2021 im (Achtung: Änderung!) Hafenbad 18 & 18/1 und Schuhhausgasse 8 in Ulm zu sehen.

Dominik Lucha kommt aus Ravensburg, lebt in Berlin und arbeitet hauptberuflich als Produzent in der Medienbranche. Mit dem Account kamen zahlreiche Anfragen und weiterführende Projekte, die „Was ihr nicht seht!“ langfristig weiterentwickeln.

Zusätzlich zu Instagram wurde nun dieses Ausstellungsformat entwickelt, das als fertige Box weiter gegeben werden kann, damit die Erfahrungen an möglichst vielen verschiedenen Orten Deutschlands zu lesen sind. Wir freuen uns über Unterstützer:innen, die @wasihrnichtseht in ihre Stadt, Büroräume, Praxen, Schaufenster oder Schulen bringen: ausstellung@wasihrnichtseht.org


Medienrapport

VOGUE>>Noch immer gibt es Menschen in Deutschland, die behaupten, dass Rassismus kein Problem sei. Diskriminierung ist aber Alltag für viele Menschen – diese aufzudecken, hat sich der Account @wasihrnichtseht zur Aufgabe gemacht. Teile deine Geschichte – mach dich sichtbar!“ <<[https://www.vogue.de/lifestyle/artikel/anti-rassismus-aktivistinnen]

BADISCHES TAGBLATT>>Dominik Lucha will rassistische Vorfälle, die für viele Menschen in Deutschland Alltag sind, sichtbarer machen und Brücken für den Dialog bauen.<< 
[https://www.badisches-tagblatt.de/Nachrichten/Vier-Fragen-an-Dominik-Lucha--47774.html]

ZETT>>Unter den Posts finden sich Sätze wie „Woher kommst du wirklich?“ und „Sie können aber gut Deutsch“, aber auch die Geschichten von aggressiven, zutiefst rassistischen Angriffen. Teil dieser Geschichten sind oft auch diejenigen, die Zeug*innen des rassistischen Ereignisses werden – aber keine Stellung beziehen.<<[https://ze.tt/der-instagram-account-was-ihr-nicht-seht-macht-rassismuserfahrungen-sichtbar/]

JETZT>>Der 29-Jährige hofft, dass die aktuelle Debatte nachhaltig wirkt. „Was ihr nicht seht“ will er auf jeden Fall langfristig betreiben.<<[https://www.jetzt.de/politik/was-ihr-nicht-seht-projekt-sammelt-rassismuserfahrungen]


#wasihrnichtseht #fclr2021ulm

____________________________________________

"What we have experienced, many certainly do not know, because you do not see it. Hence the name "What you don't see!". With the project, I want to make people think and rethink. I hope white people understand - and hopefully join us in fighting for an anti-racist future."  – Dominik Lucha

The exhibition shows the work "What you don't see!" by Dominik Lucha. Together with hundreds of Black people in Germany, Dominik makes visible what often remains unseen: "What you don't see!" addresses in an impressive and accessible way the everyday racism that Black people and People of Color experience in Germany.

In June 2020, after the murder of George Floyd and the BlackLivesMatter protests, Dominik Lucha started the project on Instagram and it now has over 130 thousand followers. On the Insta channel, black people can anonymously report on their experiences of racism in Germany - and white people can learn to become anti-racist.

With "What you don't see!" a platform was created that bears witness to racism in Germany and makes it unmistakably clear that these experiences are so much more than scattered individual fates.  With an exhibition between 21.06. - 18.07.2021 in the (caution: change!) Hafenbad 18 & 18/1 in Ulm, the thousand quotes will also be visible offline and in the urban space.

Dominik Lucha, comes from Ravensburg, lives in Berlin and works full-time as a producer in the media industry. With the account came numerous inquiries and continuing projects that further develop "What you don't see!" in the long term. 

In addition to Instagram, this exhibition format has now been developed that can be passed on as a finished box so that the experiences can be read in as many different places in Germany as possible. We are happy about supporters who bring @wasihrnichtseht into their city, offices, practices, shop windows or schools: ausstellung@wasihrnichtseht.org