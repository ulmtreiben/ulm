---
id: "440410483811461"
title: Konsum – das ist so ein Thema
start: 2021-01-16 18:30
link: https://www.facebook.com/events/440410483811461/
image: 138138198_2555288784770234_5877926311141594681_o.jpg
teaser: Es geht um Alternativen zum Konsumverhalten wie es uns eingebläut wird. Wo
  hier in Ulm/NeuUlm und Umgebung gibt es Möglichkeiten einzukaufen und gleic
isCrawled: true
---
Es geht um Alternativen zum Konsumverhalten wie es uns eingebläut wird. Wo hier in Ulm/NeuUlm und Umgebung gibt es Möglichkeiten einzukaufen und gleichzeitig das Klima zu schonen? Wie kann auf das Kaufen neuer Sachen verzichtet werden zugunsten der Weiterverwendung schon vorhandenerDinge? Wie können wir das Wegwerfen in unserem Leben zu einer Ausnahmehandlung machen?Und was genau machen wir, Seraphine und Annemarie, um das voranzutreiben? Freut euch auf eine Vorstellung unserer Ideen und Informationen rund um dieses Thema.