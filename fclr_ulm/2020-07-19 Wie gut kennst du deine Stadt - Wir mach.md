---
id: "277981643427122"
title: '"Wie gut kennst du deine Stadt?" - Wir machen Geschichte digital sichtbar'
start: 2020-07-19 14:00
end: 2020-07-19 17:30
link: https://www.facebook.com/events/277981643427122/
teaser: Online-Lab-Workshop im Rahmen des Festivals contre le racisme  Die
  Black-Lives-Matter-Proteste haben auch unsere Wahrnehmung von Rassismus im
  Alltag g
isCrawled: true
---
Online-Lab-Workshop im Rahmen des Festivals contre le racisme

Die Black-Lives-Matter-Proteste haben auch unsere Wahrnehmung von Rassismus im Alltag geschärft. Immer wieder fallen uns jetzt rassistische Bezeichnungen oder koloniale Überreste auf. Wir wollen uns in unserem Online-Workshop damit beschäftigen, wo sich Kolonialgeschichte, Rassismus und Militarismus in unserer Ulmer Umgebung, etwa in Straßennamen oder Denkmälern findet. Außerdem geht es darum, diese auch für andere sichtbar zu machen und Hintergründe zu erklären.

Finde rassistische und militaristische Straßennamen in Ulm mit open source.

Dazu haben wir großartige Werkzeuge: Software, Daten und freies Wissen im Internet! Wir schauen uns online an, wo wir ein komplettes Straßenverzeichnis finden, wie man das verarbeiten und durch weitere Informationen ergänzen kann. Am Ende des Workshops soll ein Stadtplan auf Basis von OpenStreetMap-Daten stehen, auf dem die Geschichte von Ulmer Straßennamen sichtbar wird: Welche sind problematisch, welche sind wirkliche Vorbilder? Dazu verwenden wir vor allem freie Online-Tools.

Der Workshop findet in Kooperation mit dem Jugend hackt Lap Ulm und dem Verschwörhaus statt. Er ist kostenlos und für alle Interessierten offen. 

Voraussetzungen:

Um mitzumachen braucht ihr einen Computer und Internet. Außerdem wäre ein Headset super. Tablet oder Smartphone reichen leider nicht aus, weil wir wirklich mit Daten arbeiten und auch ein bisschen programmieren wollen. Keine Sorge: Programmier-Vorkenntnisse braucht ihr keine, nur mit eurem Computer solltet ihr umgehen können.

Anmeldungen:

Du bist dabei? Cool! Schreib einfach eine Mail [lab-ulm@jugendhackt.org] ans Jugend hackt Lab mit deinem Namen und deinem Alter, danach bekommst du von uns einen Einladungslink in den virtuellen Seminarraum in BigBlueButton.

*****

Online lab workshop as part of the festival contre le racisme

The Black Lives Matter protests have also sharpened our perception of racism in our everyday life. Again and again we now notice racist labels or colonial remnants. In our online workshop we want to deal with where colonial history, racism and militarism can be found in our Ulm environment, for example in street names or monuments. We also want to make them visible to others and explain the background.

Find racist and militaristic street names in Ulm with open source.

We have great tools for this: software, data and free knowledge on the internet! We look online where we find a complete street directory, how to process it and how to add more information. At the end of the workshop we will have a city map based on OpenStreetMap data, which will show the history of Ulm street names: Which ones are problematic, which are real role models? For this purpose, we mainly use free online tools.

The workshop is held in cooperation with Jugend hackt Lap Ulm and the Verschwörhaus. It is free of charge and open to all people interested.

Requirements:

To participate you need a computer and internet. Also a headset would be great. Tablet or Smartphone are not enough, because we really want to work with data and also want to do some programming. Don't worry: You don't need any previous programming knowledge, you should only be able to use your computer.

Registration:

You're in? Cool! Just send a mail [lab-ulm@jugendhackt.org] to Jugend hackt Lab with your name and age, after that you'll get an invitation link to the virtual seminar room in BigBlueButton.