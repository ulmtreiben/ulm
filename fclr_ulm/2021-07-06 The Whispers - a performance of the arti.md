---
id: "942681722968059"
title: '"The Whispers" - a performance of the artist Mbene Mwambene'
start: 2021-07-06 19:30
locationName: Stadthaus Ulm
address: Ulm
link: https://www.facebook.com/events/942681722968059/
image: 200864320_2660875684211543_2664493686846839920_n.jpg
isCrawled: true
---
„The Whispers“ brings a personal journal of colonialism to life. It´s a response to the colonial perspective of beeing black. The artist traces his familiy history. What does it mean to decolonize yourself when living in Europe? What does it mean to have a name?

The performance will be followed by an audience discussion with the artist.

Further information about the actor and the performance: https://mbene.ch/

Admission free!

Proof of "Tested, Vaccinated or Recovered" is required!
Seat reservation desired: fclr.anmeldung@uni-ulm.de

(If registering more than one person, please indicate whether they are from the same household. Places will be allocated according to receipt of registration).

The current hygiene regulations of the Stadthaus Ulm apply.

Funded by: Partnerschaft für Demokratie in Ulm mit Fokus Wiblingen im Rahmen des Bundesprogramms „Demokratie leben!“ vom Bundesministerium für Familie, Senioren, Frauen und Jugend.

#fclr2021ulm #mbene
___________________________________________

Welche Rolle haben wir in der westlichen Welt für einen jungen, modernen Mann aus Afrika vorgesehen? Und was bedeutet es, sich zu dekolonisieren, wenn man in Europa lebt? Diese und andere Fragen rund um Identität und Stereotypen verhandelt der Theaterschaffende Mbene Mwambene in seinem Solo-Stück «The Whispers».

Im Anschluss an die Performance findet ein Publikumsgespräch mit dem Künstler statt.

Weitere Infos zum Künstler und zur Performance: https://mbene.ch/

Eintritt frei!

Ein Nachweis „Getestet, Geimpft oder Genesen“ ist erforderlich!
Platzreservierung erwünscht: fclr.anmeldung@uni-ulm.de

(Bei der Anmeldung von mehreren Personen bitte angeben, ob die  Personen aus einem Haushalt kommen. Die Plätze werden nach Eingang der Anmeldung vergeben.)

Es gelten die aktuellen Hygienevorschriften des Stadthauses Ulm.

Das Projekt wird gefördert durch die Partnerschaft für Demokratie in Ulm mit Fokus Wiblingen im Rahmen des Bundesprogramms „Demokratie leben!“ vom Bundesministerium für Familie, Senioren, Frauen und Jugend.