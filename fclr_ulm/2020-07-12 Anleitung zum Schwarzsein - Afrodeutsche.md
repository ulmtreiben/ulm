---
id: "253417515944058"
title: Anleitung zum Schwarzsein - Afrodeutsche Lebensperspektiven
start: 2020-07-12 19:30
end: 2020-07-12 21:00
link: https://www.facebook.com/events/253417515944058/
image: 106398722_2398852353747212_632444793635731779_o.jpg
teaser: Autorin Anne Chebu gibt einen Einblick in afrodeutsche Lebensperspektiven. Sie
  spricht über Schwarze deutsche Geschichte, Alltagsrassismus und hilft u
isCrawled: true
---
Autorin Anne Chebu gibt einen Einblick in afrodeutsche Lebensperspektiven. Sie spricht über Schwarze deutsche Geschichte, Alltagsrassismus und hilft unbewussten Rassismus besser zu verstehen. 

Ihr 2014 erschienenes Buch „Anleitung zum Schwarzsein“ hat sie vor allem Schwarzen Jugendlichen gewidmet, sie schreibt gerade an der Fortsetzung „Anleitung zum Schwarz bleiben“ und wird uns auch hier Einblicke geben. 

Möchtet ihr gerne an der digitalen Lesung teilnehmen?
Bitte schickt uns eine Mail an fclr.anmeldung@uni-ulm.de
Ihr erhaltet dann den Link zur online-Plattform.


Author Anne Chebu will provide us with an insight into Afro-German perspectives on life. She will talk about Black German history, everyday racism and will help us to better understand unconscious  racism. 
In 2014 she published the book "Anleitung zum Schwarzsein", which is particularly dedicated to Black teenagers.

Are you interested? Please write an e-mail to fclr.anmeldung@uni-ulm.de
Afterwards you will receive the link to the virtual room where the reading takes place. 
The reading will be in German language.
