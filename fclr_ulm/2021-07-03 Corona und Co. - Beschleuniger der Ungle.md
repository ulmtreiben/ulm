---
id: "355853815888300"
title: Corona und Co. - Beschleuniger der Ungleichheit
start: 2021-07-03 18:30
address: online
link: https://www.facebook.com/events/355853815888300/
image: 201134278_2660911237541321_625006299974827698_n.jpg
isCrawled: true
---
 📅 03.07.21 ⌚ 18:30 📌 Online 💬 deutsch

Schwarze und Hispanische US-Amerikaner*innen haben ein dreifach höheres Corona-Infektionsrisiko als weiße US-Amerikaner*innen [1]. Dies steht sinnbildlich dafür, inwiefern sozioökonomische Faktoren und der Gesundheitszustand von Individuen korrelieren. Hierbei wollen wir insbesondere die Rolle der Migration sowie die Auswirkungen von Wohlstand auf die Gesundheit beleuchten. So weisen Menschen mit Migrationsgeschichte beispielsweise eine schlechtere seelische Gesundheit auf [2]. Doch worin begründen sich diese Ungleichheiten? Dieser Frage wollen wir in unserem interaktiven Workshop auf den Grund gehen. Gemeinsam wollen wir uns auf die Suche nach Antworten und potentiellen Lösungsmöglichkeiten machen.  

[1]: https://www.tagesschau.de/ausland/coronarisiko-usa-101.html, zuletzt abgerufen am 19.04.2021
[2]: https://edoc.rki.de/bitstream/handle/176904/2186/23HLlnBjkkIb6.pdf?sequence=1&isAllowed=y, zuletzt abgerufen am 19.04.2021 

Die Veranstaltung wird durchgeführt in Kooperation mit der Grünen Jugend Ulm.

Link zur Veranstaltung:

https://uni-ulm.zoom.us/j/6831655221?pwd=d2p0aktVbENpTitoSGlPeHFvcms4UT09

Meeting-ID: 683 165 5221
Kenncode: FCLR2021

#fclr2021ulm
____________________________________________

 Corona & Co. - accelerator of inequality?
📅 03.07.21 ⌚ 6.30 pm 📌 Online 💬 German

The event will highlight the socio-economic aspects of corona and other diseases and how these promote inequalities and racisms. It will also address the correlation between wealth and health.    

Black and Hispanic Americans have a threefold higher risk of corona infection than white Americans [1]. This is emblematic of the extent to which socioeconomic factors and the health status of individuals correlate. In particular, we want to shed light on the role of migration and the impact of wealth on health. For example, people with a migration history show poorer mental health [2]. But what are the reasons for these inequalities? We want to get to the bottom of this question in our interactive workshop. Together we will search for answers and potential solutions.  

[1]: www.tagesschau.de/ausland/coronarisiko-usa-101.html, last accessed 19.04.2021

[2]: edoc.rki.de/bitstream/handle/176904/2186/23HLlnBjkkIb6.pdf, last accessed 19.04.2021

In cooperation with: Grüne Jugend Ulm.

Link:
https://uni-ulm.zoom.us/j/6831655221?pwd=d2p0aktVbENpTitoSGlPeHFvcms4UT09

Meeting-ID: 683 165 5221
Kenncode: FCLR2021