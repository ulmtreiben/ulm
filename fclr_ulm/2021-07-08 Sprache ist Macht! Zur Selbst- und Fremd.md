---
id: "576610323716711"
title: Sprache ist Macht! Zur Selbst- und Fremdbezeichnung von Sinti und Roma
start: 2021-07-08 18:30
link: https://www.facebook.com/events/576610323716711/
image: 199056290_2661395407492904_8543323707842630893_n.jpg
isCrawled: true
---
Die Fremdbezeichnung, die oft für Sinti und Roma genutzt wird, hat eine lange Geschichte und findet sich noch heute im Sprachgebrauch vieler Menschen. Woher kommt die Fremdbezeichnung? Was für Bilder transportiert sie? Und wer ist überhaupt damit gemeint? Welche Auswirkungen hat die Verwendung der Fremdbezeichnung auf Sinti und Roma? Und warum wird die Selbstbezeichnung so oft nicht verwendet? Was hat das Ganze mit Macht zu tun?
Der Vortrag beschäftigt sich mit diesen Fragen und geht auf das Spannungs- und Machtverhältnis von Selbst- und Fremdbezeichnungen von Sinti und Roma ein. Er beleuchtet verschiedene Positionen und Haltungen aus der Minderheit und der Dominanzgesellschaft.

Durchgeführt wird der Vortrag von Verena Lehmann. Sie ist Mitbegründerin des Sinti-Roma-Pride, einer unabhängigen Organisation für die junge Generation der Sinti und Roma, die sich im Grundsatz für gleichberechtigte Teilhabe, für Menschenrechte und für den kulturellen Erhalt von Sinti und Roma einsetzt.

Die Veranstaltung wird durchgeführt in Kooperation mit Sinti-Roma-Pride.

Link zur Veranstaltung:
https://uni-ulm.zoom.us/j/6831655221?pwd=d2p0aktVbENpTitoSGlPeHFvcms4UT09
Meeting-ID: 683 165 5221
Kenncode: FCLR2021

#SintiRomaPride #fclr2021ulm
____________________________________________

The external appellations used for Sinti and Roma have a long history and are still in use in the current language today! Where does the external appellation come from? Which ideas and pattern does it implicate? And who is meant by those terms? What impact does it have on Sinti and Roma? A lot of people don’t use the self-chosen term Sinti and Roma, but why? And what has all of this to do with power?
This event will deal with these questions and show the tension and power relation between external appellation and self-designation of Sinti and Roma. It will show different positions and attitudes of the minority as well as the majority.

The presentation will be given by Verena Lehmann. She is co-founder of „Sinti-Roma-Pride“, an independent organisation for the young generation of Sinti and Roma. They stand for equal participation, human rights and the cultural preservation of Sinti and Roma.

The event is organized in cooperation with Sinti-Roma-Pride.