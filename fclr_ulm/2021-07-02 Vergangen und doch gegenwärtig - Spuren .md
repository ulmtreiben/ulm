---
id: "1100348903821297"
title: '"Vergangen und doch gegenwärtig" - Spuren der deutschen Kolonialgeschichte'
start: 2021-07-02 10:00
end: 2021-07-02 13:00
address: online
link: https://www.facebook.com/events/1100348903821297/
image: 208325469_2670255206606924_4478558345308613385_n.jpg
isCrawled: true
---
 📅 02.07.21 ⌚ 10:00 - 13:00 📌 Online 💬 deutsch

 

Das deutsche Kolonialreich ist inzwischen über 100 Jahre vergangen - doch sind die Folgen des Kolonialismus überall zu finden. Im Workshop begeben wir uns auf Spurensuche und überprüfen Alltagssprache, Bilder und Ideen auf koloniale Kontinuitäten. Welche rassistischen Strukturen wurden durch Kolonialismus erschaffen, die unser Denken und Handeln bis heute prägen? Und wie haben sich die Kolonisierten zur Wehr gesetzt?

Im zweiten Teil betrachten wir aktuelle, globale Wirtschafts- und Handelsbeziehungen und diskutieren gemeinsam, ob und inwiefern diese als post-koloniale Politiken betrachtet werden können. Ist die Entwicklungszusammenarbeit eine Fortsetzung post-kolonialer Machtausübung? 

Die Veranstaltung findet in Kooperation mit iz3w statt.

Ein Anschlussworkshop "On the move" findet statt am 3. Juli, 14.-17 Uhr.

Wir freuen uns über Anmeldungen bis zum 27.06.2021 per Mail anfclr.anmeldung@uni-ulm.de 

#fclr2021ulm #iz3w
____________________________________________

 "Past and yet present" - traces of German colonial history

📅 02.07.21 ⌚ 10 am - 1pm 📌 Online 💬 German

The German colonial empire is now over 100 years gone - but the consequences of colonialism can be found everywhere. In the workshop, we go in search of traces and examine everyday language, images and ideas for colonial continuities. What racist structures were created by colonialism that still shape our thoughts and actions today? And how did the colonised defend themselves? In the second part, we look at current, global economic and trade relations and discuss together whether and to what extent these can be considered post-colonial policies. Is development cooperation a continuation of post-colonial exercise of power?

In cooperation with: iz3w

Follow-up workshop "On the move": 03.07.2021, 2-5pm

We look forward to receiving registrations by 27.06.2021 by mail to fclr.anmeldung@uni-ulm.de
