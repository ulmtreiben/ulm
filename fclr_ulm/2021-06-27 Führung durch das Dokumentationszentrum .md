---
id: "229423732076519"
title: Führung durch das Dokumentationszentrum Oberer Kuhberg
start: 2021-06-27 12:30
end: 2021-06-27 14:00
address: KZ-Gedenkstätte Oberer Kuhberg Ulm
link: https://www.facebook.com/events/229423732076519/
image: 201464819_2662108244088287_3130213689153734853_n.jpg
isCrawled: true
---
 📅 27.06.21 ⌚ 12:30 - 14:00 📌 DZOK Ulm 💬 englisch


Von November 1933 bis Juli 1935 befand sich im Fort Oberer
Kuhberg am Ulmer Stadtrand ein Konzentrationslager des
Landes Württemberg mit insgesamt mehr als 600 politischen und
weltanschaulichen Gegnern des NS-Regimes.

Die Führung durch die gut erhaltene Gedenkstätte findet auf englisch statt und ist kostenfrei.

Wir freuen uns über eine Anmeldung für die Führung bis 25.06.2021  bei info@dzok-ulm.de oder 0731-21312.

Adresse:
Dokumentationszentrum Oberer Kuhberg,
Am Hochsträss 1, Ulm

(Bildquelle: DZOK-Archiv)

#fclr2021ulm #dzok
____________________________________________
 Guided tour through the former concentration camp Oberer Kuhberg

📅 27.06.21 ⌚ 12.30 am - 2 pm 📌 DZOK Ulm 💬 English

English guided tour through the well-preserved memorial site at Oberen Kuhberg, where more than 600 political and ideological opponents of the Nazi regime were imprisoned from November 1933 to July 1935.

The tour is in English and free of charge.

We look forward to receiving your registration for the tour untill 25.06.2021 via mail at info@dzok-ulm.de or 0731-21312.

Address:
Dokumentationszentrum Oberer Kuhberg,
Am Hochsträss 1, Ulm