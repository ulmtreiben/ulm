---
id: "873239133168299"
title: Klangdemo "Courage, Engagement, Solidarität"
start: 2020-07-11 18:30
end: 2020-07-11 20:00
locationName: Festival Contre Le Racisme Ulm
address: Ulm
link: https://www.facebook.com/events/873239133168299/
image: 106462334_2398796757086105_5523042855575651892_o.jpg
teaser: '[DE]: "Unter dem Motto „Courage, Engagement, Solidarität“ rufen wir zur
  Teilnahme an der Klangdemo am 11. Juli 2020, 18.30- ca. 20.00 Uhr, auf dem
  Mün'
isCrawled: true
---
[DE]: "Unter dem Motto „Courage, Engagement, Solidarität“ rufen wir zur Teilnahme an der Klangdemo am 11. Juli 2020, 18.30- ca. 20.00 Uhr, auf dem Münsterplatz auf.  Die Klangdemo ist eine der ersten Veranstaltungen des Festivals contre le racisme – spezial.

Gemeinsam möchten wir ein Zeichnen für ein solidarisches Handeln setzen. Wir treten ein für ein gesellschaftliches Zusammenleben frei von Rassismus und menschenverachtende Einstellungen. Wir rufen dazu auf, Courage zu zeigen und einzustehen für eine offene, friedliche Zivilgesellschaft in Ulm und um Ulm herum, lokal und global.

Engagiert euch! Werdet gemeinsam mit anderen aktiv! Bezieht Stellung gegen rassistische Übergriffe und Diffamierungen im Alltag! In Solidarität miteinander - für ein friedliches und chancengleiches Zusammenleben in Vielfalt. 

Plakate & Alltagsmasken nicht vergessen! Bringt gerne auch ein Sitzkissen mit, dann könnt ihr es euch während den musikalischen Beiträgen gemütlich machen. 

U.a.
mit musikalischen Beiträgen der Bands „IZLEM“ und „Manu &
friends“, Poetry Slam und solidarischen Redebeiträgen
verschiedener Initiativen."

[EN]: Under the motto "Courage, commitment, solidarity" we call for Participation in the Demonstration on 11 July 2020, 18.30- approx. 20.00 hrs, on the Münsterplatz. The demo is one of the first Events of the festival contre le racisme - special.
Together we would like to create a sign for solidary action. We stand up for a social living together free of racism and inhuman attitudes. We want you to show courage and stand up for an open, peaceful Civil society in and around Ulm, locally and globally.
Get involved! Get active together with others! Take a stand against racist attacks and defamation in everyday life! In Solidarity with one another - for a peaceful and equal opportunity. Living together in diversity. 

Do not forget posters & everyday masks! Alos feel free to bring a seat pillow with you, so that you can make yourselve comfortable during the music concerts.

With musical contributions of the bands "GIZEM" and "Manu &
friends", Poetry Slam and speeches from various initiatives.