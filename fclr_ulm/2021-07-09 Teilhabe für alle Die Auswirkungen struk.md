---
id: "1500290063655398"
title: Teilhabe für alle? Die Auswirkungen struktureller Diskriminierung auf Sinti
  und Roma
start: 2021-07-09 17:00
link: https://www.facebook.com/events/1500290063655398/
image: 200918003_2661406597491785_6093381489757855255_n.jpg
isCrawled: true
---
Strukturelle Diskriminierung hat Auswirkungen auf alle Lebensbereiche von Sinti und Roma. Welche (institutionellen) Strukturen und Mechanismen erschweren Zugänge und die nicht gleichberechtigte Teilhabe am gesellschaftlichen Leben? Welche besonderen Herausforderungen stellen sich dabei insbesondere für Sinti und Roma? Und was braucht es, um strukturelle Diskriminierung zu erkennen und dieser entgegenzuwirken?

Ein Ansatz ist das Pilotprojekt ReFIT – Regionale Förderung von Inklusion und Teilhabe. Dieses wird aktuell mit finanzieller Unterstützung des Ministeriums für Soziales und Integration Baden-Württemberg in Ulm durchgeführt. Dabei werden vor allem Regelstrukturen und lokale Angebote in den Blick genommen, um diese für die Minderheit nutzbarer und diskriminierungsärmer zu machen. In der Projektlaufzeit wird u.a. ein Werkzeugkoffer für die Kommune erarbeitet, der an den Stellen zum Einsatz kommt, an denen die gesellschaftliche Teilhabe von Sinti und Roma erschwert ist. Dabei wird Antidiskriminierungsarbeit und Aufklärung über Antiziganismus als Querschnittsthema konsequent mitgedacht. ReFIT ist ein Projekt, das dauerhafte Strukturen schafft, die zur gleichberechtigten Teilhabe von Sinti und Roma beitragen.

Der Vortrag geht allgemein auf die Auswirkungen von struktureller Diskriminierung von Sinti und Roma ein und stellt zusätzlich das Pilotprojekt ReFIT vor. 

Die Veranstaltung wird durchgeführt in Kooperation mit dem Verband Deutscher Sinti und Roma, Landesverband Baden-Württemberg.

Link zur Veranstaltung: https://uni-ulm.zoom.us/j/6831655221?pwd=d2p0aktVbENpTitoSGlPeHFvcms4UT09
Meeting-ID: 683 165 5221
Kenncode: FCLR2021

#fclr2021ulm
____________________________________________

Structural discrimination has impacts on the live of Sinti and Roma. Which (institutional) structures and mechanisms make it difficult to access and prevent equal participation in social life? What are the specific challenges for Sinti and Roma? And what does it take to recognize and counteract structural discrimination?

One approach is the project ReFIT – regional promotion of inclusion and participation. The project currently takes place in Ulm and is funded by the Ministry of Social Affairs and Integration. ReFIT analyses control structures and local services to verify how usefull they are and what can be changed to make them less discriminating. During the project we will develop a toolbox for the municipality, that can be used when equal participation for Sinti and Roma is challenging. Cross-cutting issues are anti-discrimination work and antiziganism. ReFIT wants to create sustainable structures which contribute to equal participation for Sinti and Roma.

The lecture will deal in general with the effects of structural discrimination of Sinti and Roma and will additionally present the pilot project ReFIT. 

The event is organized in cooperation with the Association of German Sinti and Roma, Landesverband Baden-Württemberg.