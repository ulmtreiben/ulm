---
name: Frauenkreis / Women Circle
website: http://www.ulmtreiben.de/
email: womencircle@posteo.de
---
Women have been gathering together for centuries, from biblical ‘Red Tents’ where the women in a tribe would live together during the days of menstruation, right up to today. The purpose of our circles is to create a place for women to empower and build stronger relationships with each other.
