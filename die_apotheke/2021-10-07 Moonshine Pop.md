---
id: "405602571050382"
title: Moonshine Pop
start: 2021-10-07 20:00
locationName: Die Apotheke
address: Olgastraße 143, 89073 Ulm
link: https://www.facebook.com/events/405602571050382/
image: 242459785_4555172404525857_4884544389417169869_n.jpg
isCrawled: true
---
Ihr habt Bock auf einen gemütlichen DJ-Abend mit feiner Mucke? Dann seid ihr heute bei uns genau richtig!

Matze Edel von free.fm bespielt die Apotheke mit gepflegter Konversationsmusik. Auf den virtuellen Plattentellern drehen sich Indietronica von der Insel, DDR-Soul, Twee aus den Highlands oder japanischer Folk.

Eintritt frei