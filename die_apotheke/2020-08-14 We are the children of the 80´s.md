---
id: "744530922754187"
title: We are the children of the 80´s /
start: 2020-08-14 20:00
end: 2020-08-15 02:00
locationName: Die Apotheke
address: Olgastraße 143, 89073 Ulm
link: https://www.facebook.com/events/744530922754187/
teaser: Bist Du ein Kind der 80er und hast in Deiner Jugend zu Nirvana, Slipknot und
  Rage Against the Machine getanzt? Hast aber auch die Plattenkisten Deiner
isCrawled: true
---
Bist Du ein Kind der 80er und hast in Deiner Jugend zu Nirvana, Slipknot und Rage Against the Machine getanzt? Hast aber auch die Plattenkisten Deiner Eltern durchwühlt und bist dabei auf Schätze wie the doors, Jimi Hendrix und die Beatles gestoßen? Dann bist Du an diesem Abend bei uns genau richtig. Im Sitzen natürlich.

Musik: DJ BenJammin' (ROXY Sound Garten)