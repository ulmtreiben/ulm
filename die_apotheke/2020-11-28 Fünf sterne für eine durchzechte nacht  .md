---
id: "4058763680806022"
title: "Fünf sterne für eine durchzechte nacht : eine Performance mit rok & michael
  moravek"
start: 2020-12-18 20:00
end: 2020-12-18 23:00
locationName: Die Apotheke
address: Olgastraße 143, 89073 Ulm
link: https://www.facebook.com/events/4058763680806022/
image: 129117246_1030350817376990_5227500756340070462_o.jpg
teaser: 'ROK veröffentlicht mit "fünf sterne für eine durchzechte nacht" seinen
  zweiten Gedichtband. So viel vorneweg: Das dieses Mal mit Hardcover
  ausgestatt'
isCrawled: true
---
 ROK veröffentlicht mit "fünf sterne für eine durchzechte nacht" seinen zweiten Gedichtband. So viel vorneweg: Das dieses Mal mit Hardcover ausgestattete Buch hält das Niveau mühelos und steht seinem Vorgänger "die zärtlichkeit des schneemanns" in nichts nach. Geblieben ist auch die Zusammenarbeit mit dem Berliner Künstler und Musiker Samuel F.Sieber, der Illustrationen für das Buch und den Einband beigetragen hat. Seine Figuren spiegeln ROKs Lyrik auf wundersame Weise wider und umgekehrt. Wobei natürlich die Gedichte im Vordergrund stehen. Und die haben es wieder in sich. Hervorzuheben ist ROKs feines Gespür für Rhythmus und Melodie, beides erzeugt in seinen Gedichten einen langen Fluss. Der Leser wird in jenen Flow von der ersten Zeile an hineingezogen und will das schmale Bändchen gar nicht mehr beiseite legen. Das Kopfkino wird mit sachte bewegten Bildern und lakonischen Metaphern angeregt. Zudem sind einige Gedichte mit einer Prägnanz geschrieben, die wir in guten Rock- oder Pop-Songs finden. Einige Male schimmert auch der frühe Wolf Wondratschek ("Chucks Zimmer") durch und selbst der freigeistige Allen Ginsberg schwebt hin und wieder durch ROKs Wort-Räume und Wort-Träume. Seine Zeilen heben im Kopf des Lesers ab, ein sachter Flug gen Himmel, ein Sturz auf den Asphalt. Die harte Realität, die Schönheit des Lebens und die Farben der Melancholie sind oft nur ein Wort voneinander entfernt.  Auf „fünf sterne für eine durchzechte nacht“ feiern Poesie und Beat eine Hochzeit. Fünf Sterne für einen Ulmer Poeten!

MICHAEL MORAVEK
Was Michael Moravek mit November in Text und Musik gelungen ist, darf man als kleinen Glücksgriff in der deutschen Musiklandschaft bezeichnen.
FRÄNKISCHE ZEITUNG (CD der Woche) 

"... als ob er auf Glas wandeln würde, intoniert Moravek auf „Man Falling From The Sky“. Zu solchen Songs passt seine zerbrechliche Stimme voller kindlich-naivem Charme. Wright und Wickham geben ihren gemeinsamen Einstand auf „Falling Apart“, ein bezauberndes kleines Songjuwel."
ECLIPSED
danach: funky beats mit SOULGÜL

die veranstaltung ist auf 47 personen begrenzt!