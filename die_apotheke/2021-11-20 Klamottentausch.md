---
id: "1057655198341703"
title: Klamottentausch
start: 2021-11-20 10:00
locationName: Die Apotheke
address: Olgastraße 143, 89073 Ulm
link: https://www.facebook.com/events/1057655198341703/
image: 242582305_4555340244509073_7951275466799434612_n.jpg
isCrawled: true
---
Hi Mädels! Habt Ihr auch zu viel Klamotten im Schrank? Es ist Zeit mal wieder Auszumisten? Ballast loszuwerden? Und zum Wegwerfen sind die Klamotten doch eh viel zu Schade... na, dann nutzt die Möglichkeit bei uns und tauscht was das Zeug hält...

Habt Ihr Lust in gemütlicher Atmosphäre über den Tag verteilt nach Klamottenschätzen zu stöbern? Dann bringt ab 4.11.Eure Klamotten (bis 20 Teile) vorbei und nehmt Euch an dem Tag des Klamottentauschs maximal so viel neue schicke Fummel wieder mit!

Verbindet den Tausch doch mit einem leckeren Frühstück, einem Glas Sekt oder gönnt Euch Kaffee und Kuchen... wie auch immer, habt Spaß, schöne Stunden und fühlt euch mit euren Funden reich beschenkt!

(Nicht getauschte Klamotten können gerne am 21.11. wieder abgeholt werden - bitte dann mit Namen versehen - oder ihr spendet die Kleidung für einen guten Zweck und lasst sie da!)

Wir freuen uns auf Euch und den Tag!