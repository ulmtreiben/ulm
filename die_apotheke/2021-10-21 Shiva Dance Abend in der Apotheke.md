---
id: "383072300204137"
title: Shiva Dance Abend in der Apotheke
start: 2021-10-21 20:00
end: 2021-10-21 23:00
locationName: Die Apotheke
address: Olgastraße 143, 89073 Ulm
link: https://www.facebook.com/events/383072300204137/
image: 243281640_1482530185414141_6632788366104658293_n.jpg
isCrawled: true
---
Shiva Dance zu Gast in der Apotheke. Ein entspannter Abend zu Downtempo, Ethnogrooves, Worldmusic und musikalische Ausflüge in angrenzende Gebiete..., nicht nur in homöpathischen Dosen, zum stillsitzen ungeeignet :-)