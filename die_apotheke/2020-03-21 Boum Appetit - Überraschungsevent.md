---
id: "556895288198727"
title: Boum Appetit - Überraschungsevent
start: 2020-03-21 20:30
end: 2020-03-22 00:30
locationName: Die Apotheke
address: Olgastraße 143, 89073 Ulm
link: https://www.facebook.com/events/556895288198727/
image: 82365602_2826121750764273_3898674395924660224_n.jpg
teaser: Überraschunsevent - 5 DJ-Köche laden an diesem Abend zum gustatorischen und
  akkutischen Genuss ein.  Der frühe Vogel fängt den Wurm ;) Kommt und siche
isCrawled: true
---
Überraschunsevent - 5 DJ-Köche laden an diesem Abend zum gustatorischen und akkutischen Genuss ein.

Der frühe Vogel fängt den Wurm ;) Kommt und sichert euch einen Platz und lasst euch geschmacklich und musikalisch verwöhnen!

Eintritt frei!