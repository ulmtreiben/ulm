---
id: "747177669084101"
title: Stillgruppe Milchzahn
start: 2020-03-13 10:00
end: 2020-03-13 11:30
address: Café "Die Apotheke", 89073 Ulm
link: https://www.facebook.com/events/747177669084101/
image: 81076651_143554547087451_439245334699311104_o.jpg
teaser: Dein Kind ist älter als ein Jahr und du stillst noch immer? Dein Stillkind ist
  jünger, aber es ist noch kein Ende in Sicht? Oder du bist schwanger und
isCrawled: true
---
Dein Kind ist älter als ein Jahr und du stillst noch immer? Dein Stillkind ist jünger, aber es ist noch kein Ende in Sicht? Oder du bist schwanger und kannst es kaum erwarten, dein Kind zu stillen?

Dann bist du bei uns genau richtig!
Themen könnten sein: Stillen und Berufstätigkeit, Stillen in der Öffentlichkeit, Stillen in der Schwangerschaft und Alles, was uns rund ums Stillen bewegt. Ich selbst habe mein erstes Kind bis zum Alter von 2,5 Jahren gestillt und stille aktuell mein zweites Kind.

Wir treffen uns freitags von 10:00 bis 11:30 Uhr in Ulm im kinderfreundlichen Café "Die Apotheke" zum gemütlichen Austausch. Geschwisterkinder sind natürlich auch herzlich eingeladen. Weitere Infos und Anmeldung unter stillgruppe-Milchzahn@posteo.de