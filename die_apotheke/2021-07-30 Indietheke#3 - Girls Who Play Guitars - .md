---
id: "373558807529456"
title: Indietheke#3 - Girls Who Play Guitars - Special
start: 2021-07-30 21:00
locationName: Die Apotheke
address: Olgastraße 143, 89073 Ulm
link: https://www.facebook.com/events/373558807529456/
image: 219644372_4373267636049669_8803899725108536916_n.jpg
isCrawled: true
---
Indietheke #3- Girls Who Play Guitars - Special

Die Indietheke öffnet wieder ihre Pforten und startet direkt mit einem Special! 

Girls Who Play Guitars - Indie & Co von & mit Frauen

Freuen könnt ihr euch auf #YeahYeahYeahs #Metric #SteroTotal #girlinred #Folrence+TheMachine #TheSubways #TiredLion #ChickOnSpeed #Mine #Boy #KateNash...
...und natürlich Judith, welche euch mit köstlichen Drinks versorgt :)

CU!

/w Phil the Gap