---
id: "2985156484875405"
title: Funk & Soul
start: 2020-03-14 21:00
end: 2020-03-15 01:00
locationName: Die Apotheke
address: Olgastraße 143, 89073 Ulm
link: https://www.facebook.com/events/2985156484875405/
image: 87170890_2917299798313134_3583521962170777600_n.jpg
teaser: Heute lässt Songül die Puppen tanzen... Funk & Soul steht auf dem
  Programm!  Raus aus dem Alltag und rein ins Vergnügen!  Eintritt frei!
isCrawled: true
---
Heute lässt Songül die Puppen tanzen... Funk & Soul steht auf dem Programm!

Raus aus dem Alltag und rein ins Vergnügen!

Eintritt frei!