---
id: "124752515532377"
title: Improtheater in der Apotheke
start: 2020-03-18 20:00
end: 2020-03-18 22:00
locationName: Die Apotheke
address: Olgastraße 143, 89073 Ulm
link: https://www.facebook.com/events/124752515532377/
image: 86725584_1057555501292991_5556712933901205504_n.jpg
teaser: "Wir freuen uns Euch zu unserer neuen Improshow in der Apotheke einzuladen.
  Gebt uns Eure Vorgaben und wir heilen was ihr wollt: Liebesblindheit, Fleis"
isCrawled: true
---
Wir freuen uns Euch zu unserer neuen Improshow in der Apotheke einzuladen. Gebt uns Eure Vorgaben und wir heilen was ihr wollt: Liebesblindheit, Fleischeslust, Restalkohol, subtilen Humor, Geizhalsweh, Krokodilstränen und Nutellasucht. Schaut vorbei in der Apotheke, bringt Ideen und gute Laune mit oder lasst Euch davon anstecken. Einlass ist um 19:30 Uhr! Wir freuen uns auf einen schönen gemeinsamen Abend.
 
Wir sind die Improtheater Gruppe des Musischen Zentrum (MUZ) der Universität Ulm. Die MUZ hat viele spannende und kreative Gruppen in welchen auch Ihr Euch ausleben könnt. Hier noch ein Link zu anderen MUZ Veranstaltungen:
https://www.uni-ulm.de/einrichtungen/muz/musisches-zentrum/termine-konzerte/ 