---
id: "693709861214735"
title: "Indietheke #2 /w SAN_FRANCESCO & Phil the Gap"
start: 2020-09-12 21:00
locationName: Die Apotheke
address: Olgastraße 143, 89073 Ulm
link: https://www.facebook.com/events/693709861214735/
teaser: Kommt rum in die wunderschöne Apotheke und genießt leckere Drinks und tolle
  Musik! Serviert werden Klassiker von The Cure oder The Smiths, Dauerbrenn
isCrawled: true
---

Kommt rum in die wunderschöne Apotheke und genießt leckere Drinks und tolle Musik!
Serviert werden Klassiker von The Cure oder The Smiths, Dauerbrenner von Franz Ferdinand, Kraftklub oder The Killers, aber auch viel Genreverwandtes rund um Bilderbuch, Daft Punk oder Jan Delay! 

Get on your dancing shoes & let´s dance to Joy Division! 