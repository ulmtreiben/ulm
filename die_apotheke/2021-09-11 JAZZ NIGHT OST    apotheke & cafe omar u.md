---
id: "354742409653949"
title: JAZZ NIGHT OST    apotheke & cafe omar ulm
start: 2021-09-11 20:30
locationName: Omar
address: König-Wilhelm-Straße 5, 89073 Ulm
link: https://www.facebook.com/events/354742409653949/
image: 241512747_1207892522956151_944379476531991179_n.jpg
isCrawled: true
---
VENUES : APOTHEKE UND CAFE OMAR 

cafe omar : Jazzmess 
Magnus Schneider - Klavier  und Akkordeon
Uli Kuhn - Bass
Thomas Kleinhans - Schlagzeug

Das Trio schlägt den musikalischen Bogen von Latin, Swing, Cool Jazz über Tango 
bis hin zu Blues, Musette, den klassischen Jazzstandards und vieles mehr  
hoch konzentriert, aber trotzdem leicht und frisch - Jazz, der swingt und gehen auf kammermusikalische Entdeckungsreise. 

apotheke : huber hesse trio

 die Sängerin Villy Huber, Pianist Klaus Huber und Gitarrist Georg Hesse präsentieren Raritäten aus Soul, Latin, Pop, Blues und Jazz. Das Repertoire ist chillig, jazz-lastig und lässig-beschwingt.

Aftershow Apotheke : Stars & Beats w/ songül

dank geht an andreas schnell und thomas kleinhans und peter gruber!


