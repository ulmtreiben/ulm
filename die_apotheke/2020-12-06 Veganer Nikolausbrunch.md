---
id: "680356789315955"
title: Veganer Nikolausbrunch
start: 2020-12-06 10:00
locationName: Die Apotheke
address: Olgastraße 143, 89073 Ulm
link: https://www.facebook.com/events/680356789315955/
image: 125796924_3664126270297146_7257054926002103951_n.jpg
teaser: Morgen Kinder wird's was geben🌲🎅🥜🌰🥐🥨 Veganer Nikolausbrunch von 10.00 -
  14.00 Uhr.  - Verschiedene hausgemachte Aufstriche - Marmelade und Nouga
isCrawled: true
---
Morgen Kinder wird's was geben🌲🎅🥜🌰🥐🥨
Veganer Nikolausbrunch von 10.00 - 14.00 Uhr.

- Verschiedene hausgemachte Aufstriche
- Marmelade und Nougatcreme
- Biolandbackwaren
- Bananenbrot
- Birchermüsli
- Sojajoghurt mit Früchten
- Suppe
- Kaffee
- O- Saft
- Wasser
- Ein Glas Sekt

Erwachsene 15,90
Kinder ab 6 Jahren 7,90

Nur mit Voranmeldung:
kulturapotheke@gmx.de