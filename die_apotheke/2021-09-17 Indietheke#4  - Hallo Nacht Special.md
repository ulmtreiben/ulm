---
id: "402372088071464"
title: Indietheke#4  - Hallo Nacht Special
start: 2021-09-17 21:00
locationName: Die Apotheke
address: Olgastraße 143, 89073 Ulm
link: https://www.facebook.com/events/402372088071464/
image: 241777127_4524472660929165_6275256504970125649_n.jpg
isCrawled: true
---
Hereinspaziert in die Indietheke mit einem Special.
Diesmal nur mit deutschsprachigen Indie und viel amore! 
Freuen könnt ihr euch auf #Tomte #Kraftklub #Wanda #VonWegenLisbeth #Frittenbude #Granada #OddCouple...
...und natürlich Judith, welche euch mit köstlichen Drinks versorgt 🙂

CU!
/w Phil the Gap
/w SAN_FRANCESCO