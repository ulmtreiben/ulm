---
id: "201085997776235"
title: Ein Abend mit Rodneys Underground Press
start: 2020-05-16 19:30
end: 2020-05-16 22:30
locationName: Die Apotheke
address: Olgastraße 143, 89073 Ulm
link: https://www.facebook.com/events/201085997776235/
image: 88036139_2817505808342752_5772052260431331328_n.jpg
teaser: Roland Adelmann gründete 1992 Rodneys Underground Press, um der brodelnden
  Untergrundliteratur in Deutschland eine Stimme zu geben. Er vertrieb und ve
isCrawled: true
---
Roland Adelmann gründete 1992 Rodneys Underground Press, um der brodelnden Untergrundliteratur in Deutschland eine Stimme zu geben. Er vertrieb und vertreibt bis heute immer noch die wichtigsten Protagonisten und Protagonistinnen der Szene.

2013 sollte aus Rodneys Underground Press ein Verlag werden, der nicht nur die alten Veteranen, sondern auch neue Stimmen zu Wort kommen lässt. Ganz nach dem Motto „Poetry stirbt nie“ läuft der Laden, auch in Zeiten von Amazon und Co einfach weiter. 
Sein Sie gespannt, was Roland Adelmann über die ersten wilden Tage bis heute zu berichten hat. Wir sind es sehr!

Moderation: Marco Kerler

Eintritt: 6 Euro