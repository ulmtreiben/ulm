---
id: "379330426871704"
title: Shiva Dance Abend in der Apotheke
start: 2021-08-05 20:00
end: 2021-08-05 23:00
locationName: Die Apotheke
address: Olgastraße 143, 89073 Ulm
link: https://www.facebook.com/events/379330426871704/
image: 225609879_1437751589892001_4634812060323248926_n.jpg
isCrawled: true
---
Shiva Dance zu Gast in der Apotheke. Ein entspannter Abend zu Downbeat Grooves, Worldmusic und musikalische Ausflüge in angrenzende Gebiete..., nicht nur in homöpathischen Dosen. :-)