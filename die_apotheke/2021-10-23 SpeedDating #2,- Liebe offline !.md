---
id: "3085367335085611"
title: "SpeedDating #2,- Liebe offline !"
start: 2021-10-23 21:00
end: 2021-10-23 23:00
locationName: Die Apotheke
address: Olgastraße 143, 89073 Ulm
link: https://www.facebook.com/events/3085367335085611/
image: 242608509_4555664994476598_314411611620473631_n.jpg
isCrawled: true
---
Nachdem wir alle letztes Jahr beim Speed Dating so viel Spaß hatten, verlangt dieser Abend unbedingt nach einer Neuauflage.
Du bist zwischen 25 - 35 Jahre alt und hast Mr/Mrs. Right auch noch nicht gefunden ? Aber auch keine Lust auf tinder & Co.?
Dann probier´s doch mal mit SpeedDating.
8 Minuten quatschen und dann ohne Ausreden weiter zum nächsten Kandidaten...
Falls Du Lust hast Dating a la 90er auszuprobieren, dann melde Dich bis zum 15.10.21 bei uns.
Gerne auch per mail: kulturapotheke@gmx.de
Teilnehmerzahl begrenzt auf 20 Personen
Wir freuen uns auf einen lustigen Abend mit Euch !!!
P.S. Die nächste Runde ist dann für die 35 bis 45jährigen😊