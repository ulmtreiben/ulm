---
id: "618764985953835"
title: Veganer Sonntagsbrunch
start: 2021-10-10 10:00
locationName: Die Apotheke
address: Olgastraße 143, 89073 Ulm
link: https://www.facebook.com/events/618764985953835/
image: 242540347_4551808621528902_5636347198908600587_n.jpg
isCrawled: true
---
Genießt den Sonntagvormittag in entspannter Atmosphäre bei einem veganen Brunch in der Apotheke.
Euch erwarten u.a frische Brötchen & Brot,
hausgemachte Aufstriche, Bananenbrot,
Gemüsesuppe, Müsli...
O-Saft, Wasser & Tee sind im Preis enthalten.
Erwachsene: 17,90 Euro
Kind (ab 6 Jahren): 8,90 Euro

Nur mit Voranmeldung: 
entweder direkt in der Apotheke oder per mail: kulturapotheke@gmx.de