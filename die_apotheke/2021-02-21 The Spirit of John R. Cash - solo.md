---
id: "675929009796239"
title: The Spirit of John R. Cash - solo
start: 2021-10-17 16:00
locationName: Die Apotheke
address: Olgastraße 143, 89073 Ulm
link: https://www.facebook.com/events/675929009796239/
image: 118953460_330540031720023_8596378323571812530_n.jpg
isCrawled: true
---
Neuer Termin aufgrund des Lockdowns!
Ein Mann und seine Gitarre...
MIKE HÜHN als JOHNNY CASH entführt euch in die gute alte Zeit in Nashville, Tennessee. Er bringt die unverwechselbaren Songs des COUNTRYSTARS sehr originalgetreu auf die Bühne und ihr könnt bei Kaffee und Kuchen im schönen Cafehaus "die Apotheke" in Nostalgie schwelgen und mitwippen. <3
"Get rhythm when you got the blues.....", Johnny Cash.

Bitte im Cafe reservieren, Eintritt frei, jedoch sind kleine Scheinchen im Hut des Musikers ausdrücklich erbeten - die Zeiten sind hart <3 :-)


Bild: Lothar Felkel