---
id: "1327723351020057"
title: Soul and more mit Roter Freibeuter & monsieur A
start: 2021-10-15 21:00
locationName: Die Apotheke
address: Olgastraße 143, 89073 Ulm
link: https://www.facebook.com/events/1327723351020057/
image: 242614105_4551726988203732_2283399233253695581_n.jpg
isCrawled: true
---
Roter Freibeuter bekannt durch Schüttel dein Speck, zeigt seine kreative vielseitige Seite und lässt sich darauf ein, mit monsieur A  der auf 35 Jahre Plattenauflegen zurückblicken kann, nur mit Singels (7 inch) einen interessanten schönen soullastigen Abend zu gestalten.