---
id: "600737730930855"
title: Smile Day mit vanKost & Blumenpanzer
start: 2021-10-01 22:00
locationName: Die Apotheke
address: Olgastraße 143, 89073 Ulm
link: https://www.facebook.com/events/600737730930855/
image: 242926005_4569934446382986_4920064761769836714_n.jpg
isCrawled: true
---
Heute am Smile Day zaubern Euch
vanKost und Blumenpanzer ganz sicher ein Lächeln ins Gesicht. Denn, getreu der Tradition liefern sie einen Abend lang back-2-back ein wunderbares Spektrum an Elektronischer Tanzmusik. Von Deep-House über Ethno bis hin zu anderen musikalische Feinheiten wird an diesem  Abend alles geboten.