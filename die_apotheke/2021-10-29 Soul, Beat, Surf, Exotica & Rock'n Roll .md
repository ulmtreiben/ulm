---
id: "234850041944477"
title: Soul, Beat, Surf, Exotica & Rock'n Roll mit Dr.Essers & monsieur A
start: 2021-10-29 21:00
locationName: Die Apotheke
address: Olgastraße 143, 89073 Ulm
link: https://www.facebook.com/events/234850041944477/
image: 242481509_4551745261535238_1937045793367673567_n.jpg
isCrawled: true
---
Dr. Essers und monsieur A sind schon ein bekanntes DJ Team in der Apotheke. Sie überzeugen mit schwungvoller Musik aus dem Genres:

Soul, Beat, Surf, Exotika und Rock n Roll

 