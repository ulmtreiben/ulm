---
id: "367650101193683"
title: Silvester wie zu Hause
start: 2020-12-31 20:00
locationName: Die Apotheke
address: Olgastraße 143, 89073 Ulm
link: https://www.facebook.com/events/367650101193683/
image: 126045655_3664197310290042_8303976106058141022_o.jpg
teaser: Feiert mit uns ganz entspannt und ohne dresscode🥳🎊🎉🎆 Es gibt Chilli sin
  Carne, Schokofondue, Knabbereien und um Punkt 12 selbstverständlich ein Gl
isCrawled: true
---
Feiert mit uns ganz entspannt und ohne dresscode🥳🎊🎉🎆
Es gibt Chilli sin Carne, Schokofondue, Knabbereien und um Punkt 12 selbstverständlich ein Gläschen Sekt🥂🍾
Wer Lust hat kann beim Bleigießen die Zukunft deuten, eine Runde Tischkicker spielen, oder eine Wunderkerze nach der anderen abfackeln🎇🎇🎇
Und auf "Dinner for One" muss auch niemand verzichten😉
Wenn sich das für Euch nach einem gelungenen Jahreswechsel anhört, dann meldet Euch an:
kulturapotheke@gmx.de
Eintritt: 15 Euro