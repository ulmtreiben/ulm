---
id: "480147879526701"
title: Christopher Street Day 2020 | 10 Jahre Pride
start: 2020-06-06 16:30
end: 2020-06-06 23:30
locationName: CSD Ulm/Neu-Ulm e.V.
address: 89231 Neu-Ulm
link: https://www.facebook.com/events/480147879526701/
image: 79224700_3294153820626490_397110532999479296_o.jpg
teaser: CHRISTOPHER STREET DAY ULM  10 JAHRE PRIDE Das Kultur Festival 2020 Samstag,
  06.06.2020 auf dem Ulmer Münsterplatz  stay tuned <3
isCrawled: true
---
CHRISTOPHER STREET DAY ULM

10 JAHRE PRIDE
Das Kultur Festival 2020
Samstag, 06.06.2020 auf dem Ulmer Münsterplatz

stay tuned <3