---
id: "439865600512027"
title: Keine Gesundheitsversorgung 			für alle? - Menschen ohne Papiere in Deutschland
start: 2020-12-16 20:00
link: https://www.facebook.com/events/439865600512027/
image: 130304619_2530786770553769_7502575078568575652_o.jpg
teaser: '"In Deutschland können sich die Menschen auf eine qualitativ hochwertige
  medizinische Versorgung verlassen." Das schrieb zumindest die Bundesregierung'
isCrawled: true
---
"In Deutschland können sich die Menschen auf eine qualitativ
hochwertige medizinische Versorgung verlassen." Das schrieb
zumindest die Bundesregierung im 6. UN-Staatenberichtsverfahren. Aus
dem Alltag unserer Beratungsstelle wissen wir: Das gilt zwar für die
meisten, jedoch bei weitem nicht für alle Menschen in Deutschland.
Rechtliche Fallstricke und praktische Hürden diskriminieren Menschen
ohne Papiere in der Praxis darin, ihr Grundrecht auf gesundheitliche
Versorgung wahrzunehmen. Von Nachhaltigkeit sind wir in diesem
Bereich aktuell noch weit entfernt.

Robin Schöttke berichtet von der medizinischen Betreuung in der
Beratungsstelle des Medinetz Ulm e.V. und schildert die
zugrundeliegenden Probleme sowie mögliche Lösungsansätze.

Link zur Veranstaltung: https://uni-ulm.webex.com/uni-ulm-de/j.php?MTID=m19f9ebbc3cb861c52ba0da0acd8c9095

Meetingpasswort: 174 281 0228