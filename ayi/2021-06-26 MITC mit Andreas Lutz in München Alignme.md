---
id: "625315571424491"
title: "MITC mit Andreas Lutz in München: Alignment"
start: 2021-06-26 08:00
end: 2021-06-27 17:30
locationName: Ashtanga Yoga Institut München
address: Thorwaldsenstr. 29, 80335 Munich
link: https://www.facebook.com/events/625315571424491/
image: 107239326_959716221142439_7288920602607920425_o.jpg
teaser: Mtc mit Andreas Lutz am 23./24.01.2021 in München  Über Alignment wird ein
  anatomisch korrektes Üben erreicht. Die Verbindung von Alignment und Bandha
isCrawled: true
---
Mtc mit Andreas Lutz am 23./24.01.2021 in München

Über Alignment wird ein anatomisch korrektes Üben erreicht. Die Verbindung von Alignment und Bandhas in dem Prinzip Bandhalign® wird in der Theorie erläutert und in vielen praktischen Übungen umgesetzt. Anhand anatomischer Modelle werden interessante Besonderheiten der Anatomie in Bezug auf die körperliche Praxis demonstriert. Es bleibt Raum individuelle Probleme zu ergründen und in praktisch anzugehen. 
Ein Thema, das nicht nur für Yogalehrer neue Aspekte eröffnen kann.

Für wen ist ein Modular Therapy Course (MTC) interessant?
Der Modular Therapy Course (MTC) richtet sich gezielt an Yogalehrer, auch anderer Tradition sowie interessierte Yogis. Du tauchst ein in die anatomischen und physiologischen Hintergründe Deiner Yogapraxis. Wenn Du bereits eine eigene Yogapraxis etabliert hast und Dich Alignment, Prävention und Yogatherapie interessieren, dann bist Du hier genau richtig.
Du erfährst in dieser Weiterbildung die anatomischen Hintergründe und erlangst ein Verständnis über häufige Verletzungen und Erkrankungen. Für Deine Praxis erlernst Du therapeutische Sequenzen und Bandhlign® Techniken für eine gesunde Ausrichtung. Es liegt uns am Herzen, Dir viel theoretisches Wissen und praktische Inspirationen für Deine eigene Praxis und Deinen Unterricht mitzugeben. Zusätzlich erhältst Du ein Handbuch, in dem Du die wesentlichen Inhalte nachlesen kannst.  

Vertiefung im eLearning
Nach dem MTC hast Du zwei Monate die Möglichkeit im AYI® eLearning die erlernten Techniken zu vertiefen, zu reflektieren und für die persönliche Praxis und den Unterricht nutzbar zu machen. Hierfür findest Du in Deinem eLearning Hintergrund-Texte, Übungsvideos und Vertiefungsaufgaben. So kann das MTC ein Modul Deiner AYInnovation® Yogalehrer Ausbildung oder Weiterbildung werden. Schließt Du Deine Vertiefungen in dieser Zeit erfolgreich ab, erhältst Du Dein AYI® Zertifikat!

Über Andreas
Seit über 30 Jahren ist Andreas als Masseur, (Sport-)-Physiotherapeut, Manualtherapeut, Heilpraktiker, Rolfer und Shiatsu-Praktiker in eigener Praxis tätig. Er setzt sein medizinisch fundiertes Wissen und seine Erfahrung ein, um über das Alignment (die physiologische Ausrichtung) ein achtsames und sorgfältiges Üben zu ermöglichen. Solltest du Einschränkungen oder Schwierigkeiten haben ist er immer in der Lage therapeutisch lösende Techniken einzubringen, die es dir ermöglichen, modifiziert weiter zu üben. Sein persönlicher Favorit wurde über die Jahre auch der Schwerpunkt in seinem Unterricht: der Atem. Atmung in Bewegung (Vinyasa, Bandha) und besonders die Pranayamas sind ihm als die Essenz der Übung in Fleisch und Blut übergegangen und erzeugen eine ungeahnte Tiefe in seiner Lehrtätigkeit. Deine persönliche Transformation beginnt immer mit einer Einatmung: "ekam inhale" ...
www.andreas-lutz.com
Anmeldung und Preise:
Die Anmeldung läuft ausschließlich über die Yogaschule von Dr. Ronald Steiner in Ulm. 
Preis 295 € 
20 UE (Sa und So 08 Uhr bis 17:30 Uhr)