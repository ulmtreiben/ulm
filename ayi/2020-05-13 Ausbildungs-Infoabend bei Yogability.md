---
id: "192439878512826"
title: Ausbildungs-Infoabend bei Yogability
start: 2020-05-13 18:00
end: 2020-05-13 21:00
locationName: Yogability
address: Mühlenstraße 9, 58313 Herdecke
link: https://www.facebook.com/events/192439878512826/
image: 85260192_2781734058553338_3376229076481081344_o.jpg
teaser: AUSBILDUNGS-INFOABEND BEI YOGABILITY   Überlegst Du, ob eine
  Yogalehrer-Ausbildung das Richtige für Dich ist? Möchtest Du tiefer in die
  Materie eintau
isCrawled: true
---
AUSBILDUNGS-INFOABEND BEI YOGABILITY
 
Überlegst Du, ob eine Yogalehrer-Ausbildung das Richtige für Dich ist? Möchtest Du tiefer in die Materie eintauchen, hast aber keine Ahnung, was alles dahintersteckt und womit eine Yogalehrer-Ausbildung einhergeht? Oder welches von den vielen Angeboten für Dich das Richtige ist? Dann empfehlen wir Dir unseren Infoabend! In einer Gratis-Kurseinheit und einer ausführlichen Vorstellung der Ausbildungen mit anschliessendem gegenseitigen Austausch hast Du hier die Möglichkeit, uns und die Tradition des Ashtanga Yoga kennenzulernen. Und wer weiß, vielleicht startet auch Deine Ausbildung bald – auf dem Weg zum/zur Yogalehrer/in!

Z E I T P L A N 

18:00 - 19:00 Uhr
Geführte AYI® Ashtanga Basic Stunde (Level 1)
danach kurze Pause 

19:15 - 21:00 Uhr
Vorstellung der Ausbildungen, alle Infos,
Lehrer, Zeitpläne, Fragen und Antworten

 
SVENJA WILKE
Svenja ist eine der wenigen zertifizierten AYI® Expert Lehrerinnen in Deutschland und hat ihre 4-jährige Ausbildung (2010-2014) bei Anna Trökes und  Dr. Ronald Steiner absolviert. Svenja kam durch ihre Mutter (selbst Yogalehrerin) schon früh mit Yoga in Kontakt. Den dynamischen Stil des Ashtanga Yoga lernte sie 2007 in Sydney kennen. Die Dynamik, die Verbindung von Atem und Bewegung sowie die kraftvolle und gleichzeitig elegante Ausdrucksweise haben Svenja sofort in ihren Bann gezogen. Seitdem hat sie Ashtanga Yoga nicht mehr losgelassen. Svenja leitet Aus- und Weiterbildungen sowie Retreats in Deutschland und dem europäischen Ausland und betreibt gemeinsam mit ihrem Ehemann die Yogaschule Yogability – mitterweile eine der führenden Adressen für Ashtanga Yoga in NRW und darüber hinaus.

AYI® Begründer Dr. Ronald Steiner über Svenja Wilke:
"Svenja vereint eindrucksvoll die Eigenschaften, für die die AYI Methode steht. In ihrer persönlichen Praxis wechselt sie mühelos zwischen beeindruckender Akrobatik und angepasster Sanftheit. Genau diese Brücke schafft sie auch in Ihrem Unterricht. Wer eine ganz persönliche zu sich passende Praxis entwickeln möchte, ist bei Svenja genau richtig. Wer die AYI Methode in ihrer Vielfältigkeit in aller Tiefe erfahren möchte, findet mit Svenja die beste Begleitung in einer Yogalehrer Aus- und Weiterbildung.“

Anna Trökes über Svenja Wilke
„Ich habe Svenja mehr als 4 Jahre durch ihre Ausbildung begleitet. Ich erlebte sie als offen, in allem, was sie tat, als sehr kompetent und vor allem als eine äußerst einfühlsame und zugewandte Lehrerin. Ich kann sie nur wärmstens empfehlen!“
 
Mehr Infos auf www.yogability.de


****

O R T

YOGABILITY
STUDIO QUARTIER RUHR AUE
Mühlenstr. 9
58313 Herdecke

--------

P R E I S

DIE VERANSTALTUNG IST GRATIS
Um Voranmeldung wird gebeten.

--------

I N F O R M A T I O N   &.  A N M E L D U N G

https://www.yogability.de/ausbildungs-infoabend-2020/

Du kannst Dich auch telefonisch unter 02330.8918776, über unsere App oder einfach direkt in unserem Studio anmelden.

www.yogability.de