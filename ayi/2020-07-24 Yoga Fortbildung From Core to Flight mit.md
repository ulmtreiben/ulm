---
id: "1172404716299340"
title: Yoga Fortbildung "From Core to Flight" mit Dr. Ronald Steiner
start: 2020-07-24 08:00
end: 2020-07-29 18:00
locationName: Bavaria Filmstudios, München
address: Bavariafilmplatz 7, 82031 Munich
link: https://www.facebook.com/events/1172404716299340/
image: 74610966_2796539533699150_5554362062076903424_o.jpg
teaser: 'Was: Yoga Fortbildung "From Core to Flight" - MTC (Modular Therapy Course) X
  3 (Rücken, Schulter, Hand) Wann: 24.07.-29.07.2020 Wo: Bavaria Filmstudio'
isCrawled: true
---
Was: Yoga Fortbildung "From Core to Flight" - MTC (Modular Therapy Course) X 3 (Rücken, Schulter, Hand)
Wann: 24.07.-29.07.2020
Wo: Bavaria Filmstudios, München/Grünwald
Lehrer: Dr. Ronald Steiner
Anmeldung über: https://yogadelight.de/workshops-und-retreats/yogaausbildung-from-core-to-flight-mtc-x-3-ruecken-schulter-hand-24-29.07.2020-mit-dr-ron-steiner

YOGA-FORTBILDUNG: FROM CORE TO FLIGHT – MTC X 3 (RÜCKEN, SCHULTER, HAND)
Bei dieser Intensivwoche hast Du die Gelegenheit, 6 Tage lang in das Thema Yogatherapie einzutauchen und das Gelernte gleich praktisch umzusetzen. Wir beginnen am Körperzentrum, dem Rücken und arbeiten uns dann über Schultern bis zu den Händen vor.
Dieses Spezialformat bietet Dir die Gelegenheit, Dich eine Woche lang ausschließlich auf das Thema Yogatherapie zu konzentrieren. Tauche ein in die Welt der Anatomie und lerne mehr über Praxisvarianten und therapeutische Übungen! Dabei erhältst Du nicht nur eine Menge an theoretischen Informationen, sondern kannst das Gelernte auch direkt in der Praxis umsetzen und so für Dich selbst erproben.
In der MTC Serie “From Core to Flight” lernst Du alles Wichtige zum Thema obere Extremitäten. Ausgehend vom Rücken beschäftigen wir uns damit, welche Rolle auch Schulter/Schultergürtel sowie unsere Hände und Handgelenke in der Praxis spielen und wie es uns gelingt, diese langfristig gesund zu halten und neben den Füßen zu einem zweiten Fundament werden zu lassen. So gelingen auch forderndere Positionen sowie Armbalancen zunehmend mühelos!
Neben Ron Steiner ist in dieser Woche Claudia Müller-Ostenried, Gründerin von YOGAdelight Dein Host vor Ort.

VERTIEFEN IM E-LEARNING
Nach Deiner MTC Woche stehen Dir für jedes Thema spannende Aufgaben in Deinem E-Learning bereit. So hast Du insgesamt 6 Monate lang die Möglichkeit, das Gelernte zu reflektieren und zu erfahren. Schließt Du Deine Vertiefungen in dieser Zeit erfolgreich ab, erhältst Du Dein AYI Zertifikat!

From Core to Flight im Detail:
Bei “From Core to Flight” erwartet Dich Detailwissen zu den folgenden Themen. Du kannst diese Themen auch (auf der jeweiligen Detail-Seite) einzeln Buchen für 295€/Thema:

MODUL: RÜCKEN UND KÖRPERHALTUNG – DIE POLARITÄT DES LEBENS
Wann: 24./25.7.20 (Fr./Sa.)
Egal ob Du gerade erst mit dem Yoga begonnen hast oder viele Jahre praktizierst, immer bist du mit den Möglichkeiten und Grenzen Deines Rückens konfrontiert. Um auch fortgeschrittenere Asanas auf eine für Deinen Körper gesunde Weise zu üben, ist es wichtig, etwas über die Anatomie des Halteapparates zu wissen.

MODUL: SCHULTER UND SCHULTERGÜRTEL – FREIHEIT UND WEITE
Wann: 26./27.7.20 (So./Mo.)
Schulter und Schultergürtel sind der Gelenk-Komplex mit dem größten Bewegungsspielraum im menschlichen Körper. In dieser Weiterbildung erfährst wie Du mit einer Yogapraxis Gesundheit erhalten (Alignment) wieder herstellen (Yogatherapie) kannst.

MODUL: ARM UND HAND – VON WURZELN UND FLÜGELN
Wann: 28./29.7.20 (Die./Mi.)
Die Hände und Arme werden in unserer Yogapraxis zu Flügeln und auch zu Wurzeln. Ein MTC das hilft mehr über Arm und Hand zu verstehen.