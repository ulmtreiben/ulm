---
id: "408782263022237"
title: AYI Inspired Ausbildung Ulm ab Juni 2019
start: 2020-01-25 08:00
end: 2020-01-25 18:30
address: AshtangaYoga.info
link: https://www.facebook.com/events/408782263022237/
image: 55589298_2754726171219345_546128426243719168_n.jpg
teaser: Die Inspired Ausbildung (AYI®) ist der Einstieg in die AYInnovation®
  Yogalehrerausbildung. Sie richtet sich an Yogaübende, die ihr Wissen vertiefen
  wo
isCrawled: true
---
Die Inspired Ausbildung (AYI®) ist der Einstieg in die AYInnovation® Yogalehrerausbildung. Sie richtet sich an Yogaübende, die ihr Wissen vertiefen wollen. Mit fundiertem Fachwissen begleiten Dich Anna Trökes, Melanie Pillhofer und Ronald Steiner über ein Jahr auf Deinem persönlichen Yogaweg.

Uhrzeiten vorbehaltlich Änderungen. 