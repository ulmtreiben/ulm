---
id: "3715632438512321"
title: YOGA UND MEDITATION
start: 2021-03-08 09:00
end: 2021-03-10 14:30
locationName: AshtangaYoga.info
address: Frauenstr. 124, Ulm
link: https://www.facebook.com/events/3715632438512321/
image: 143587164_3693713210664164_3412429145859966775_n.jpg
teaser: Meditation ist ein unverzichtbarer Bestandteil des yogischen
  Übungsweges  Meditation hat die Kraft die Strukturen unseres Gehirns und
  unseres Körpers
isCrawled: true
---
Meditation ist ein unverzichtbarer Bestandteil des yogischen Übungsweges

Meditation hat die Kraft die Strukturen unseres Gehirns und unseres Körpers zu verändern. Es ist inzwischen wissenschaftlich nachgewiesen, wie Meditation im Umgang mit Stress helfen kann. Eine regelmäßige Praxis kann Dich unterstützen, im Alltag achtsamer und gelassener mit Dir selbst und Deinen Mitmenschen umzugehen.

Egal, ob Du in Deiner Familie, in Deinem Berufsleben oder sonst im Alltag vielfältig herausgefordert bist, kann Meditation Dir helfen mehr in Deiner eigenen Mitte zu bleiben und Deinen Geist nachhaltig zu stabilisieren.

 Kursinhalte werden sein:

meditative Bewegungsabläufe und Asanas
Pranayamas, die in die Meditation führen
geführte Meditationen
freie Meditationen
Betrachtung wichtiger Yoga Konzepte auf der Grundlage der Quellentexte, vor allem des Yogasutra und der Hatha Yoga Pradipika
Austausch und Gespräch

Info und Anmeldung:
https://de.ashtangayoga.info/workshops-ausbildung/workshop/210308-ulm-yoga-meditation-anna/

Preis: 245,00 €
