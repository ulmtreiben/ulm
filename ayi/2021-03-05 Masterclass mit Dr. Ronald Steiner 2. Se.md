---
id: "448538792450565"
title: "Masterclass mit Dr. Ronald Steiner: 2. Serie - ganz sanft (Hybrid oder
  online)"
start: 2021-03-05 17:00
end: 2021-03-05 21:00
locationName: Yogability
address: Mühlenstraße 9, 58313 Herdecke
link: https://www.facebook.com/events/448538792450565/
image: 78484367_2634742793252466_3715268095405719552_n.jpg
teaser: "Update: Die Veranstaltung findet (je nach aktueller Schutzverordnung) hybrid
  oder online statt. Da wir im Hybridmodus vor Ort nur mit begrenzter Teiln"
isCrawled: true
---
Update: Die Veranstaltung findet (je nach aktueller Schutzverordnung) hybrid oder online statt. Da wir im Hybridmodus vor Ort nur mit begrenzter Teilnehmerzahl arbeiten dürfen, nehmen wir nur noch Anmeldungen für die Online-Variante an. Vielen Dank für Dein Verständnis.

MASTERCLASS MIT DR. RONALD STEINER: 
DIE 2. SERIE - GANZ SANFT
 
Die zweite Serie des Ashtanga Yoga führt den Übenden in eine neue, energetische Dimension. Erlebe in dieser Masterclass wie das ganz ohne Akrobatik möglich ist.

Eine bewegte Meditation

Ashtanga Yoga ist eine vom Atem getragene bewegte Meditation. Dieser innere Aspekt der Praxis geht leider oft hinter einer sportlich dargestellten Oberfläche unter. Es scheint dann als wäre bereits die erste Serie des Ashtanga Yoga nur für Super-Sportler geeignet – die zweite Serie sogar nur für Übermenschen. Ronald gibt Dir einen Zugang zur Ashtanga Praxis, in dem die vom Atem getragene bewegte Meditation für Dich erfahrbar wird. Gerade die zweite Serie zeigt sich so in einer erstaunlichen Sanftheit. Die Bewegungen werden auf diese Weise simpel und erstrahlen in ihrer majestätischen Schönheit.

Die zweite Serie ganz sanft

Das Prinzip der zweiten Serie ist ganz simpel: „Weite Dein Herz und wende Dein Innen nach Außen.“ Das ist emotional eine intensive Arbeit. Daher heißt die zweite Serie traditionell auch „Nadi Shodhana“, was so viel heißt wie „Reinigung der Energiekanäle“. Doch körperlich ist die zweite Serie mit der richtigen Anleitung leicht zugänglich – und genau die gibt Dir Ronald an diesem Workshop.

Diese Masterclass findet bewusst in kleiner Gruppe in unserer Yogaschule statt  – eine tolle Gelegenheit, dieses wunderbare Thema im exklusiven Rahmen mit Ronald zu erarbeiten!
 
DR. RONALD STEINER
ist einer der wenigen Yogalehrer, die ganz traditionell von den indischen Meistern Sri K. Pattabhi Jois und BNS Iyengar autorisiert wurden. Er ist zudem Arzt, Wissenschaftler und Sportmediziner mit Forschungen im Bereich Prävention und Rehabilitation u.a. am Universitätsklinikum Ulm. Die von ihm begründete AYInnovation®-Methode baut eine Brücke zwischen uralter Tradition und fundierter Praxis, zwischen angewandter Anatomie und lebendiger Philosophie, zwischen präziser Technik und praktischer Erfahrung. Dadurch entsteht eine für den Einzelnen maßgeschneiderte, sehr persönliche Yogapraxis – von therapeutisch-präventiv bis hin zu sportlich-akrobatisch. Die modularen und deutschlandweit multizentrischen AYI® Yogalehrer Aus- und Weiterbildungen legen einen besonderen Schwerpunkt auf Prävention, Alignment und Yogatherapie.
 
Mehr Infos zu Ronald unter www.ashtangayoga.info


****

D A T U M  / U H R Z E I T 

Freitag, 5. MÄRZ 2021
17:00 - 21:00 Uhr

****

O R T

YOGABILITY
STUDIO QUARTIER RUHR AUE
Mühlenstr. 9
58313 Herdecke

--------

P R E I S

EUR 85.00

--------

I N F O R M A T I O N   &.  A N M E L D U N G

https://www.yogability.de/workshops/dr-ronald-steiner-masterclass-2-serie/

Du kannst Dich auch telefonisch unter 02330.8918776, über unsere App oder einfach direkt in unserem Studio anmelden.

www.yogability.de