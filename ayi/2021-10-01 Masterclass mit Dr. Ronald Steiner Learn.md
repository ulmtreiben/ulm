---
id: "567257377431426"
title: "Masterclass mit Dr. Ronald Steiner: Learn to Fly"
start: 2021-10-01 17:00
end: 2021-10-01 21:00
locationName: Yogability
address: Mühlenstraße 9, 58313 Herdecke
link: https://www.facebook.com/events/567257377431426/
image: 79379925_2634746453252100_7317004378223673344_n.jpg
isCrawled: true
---
MASTERCLASS MIT DR. RONALD STEINER: 
LEARN TO FLY

Nur fliegen ist schöner??? Bei diesem Workshop lernst Du, wie Du scheinbar mühelos abheben kannst und Deine Ashtanga Praxis auf diese Weise eine neue, spielerische Leichtigkeit erfährt.

Armbalancen, Handstände, Durchspringen…. Der Ashtanga Yoga bietet jede Menge Gelegenheiten, um zumindest kurzfristig einmal abzuheben und die Leichtigkeit des Fliegens zu erleben. Lerne, wie Du die ersten Schritte machst oder arbeite mit speziellen Technik-Tipps an einer bereits fortgeschrittenen Praxis. Egal ob Anfänger oder Profi-Flieger: Bei diesem Workshop ist für jeden etwas dabei!

Ronald gibt Dir konkrete Hilfe, die für Dich spannenden Haltungen oder Bewegungen zu erarbeiten. So kannst Du Dein persönliches Neuland betreten. Vielleicht findest Du Dich erstmals auf dem Kopf stehend oder auf den Händen balancierend?

Therapie und Akrobatik

Das mag sehr akrobatisch klingen. Doch beginnt die Arbeit an den Armbalancen und Umkehrhaltungen sehr therapeutisch. Denn gesunde Handgelenke, Schultern sind eine wichtige Voraussetzung um an diesen Haltungen Freude zu haben. Durch eine stabile Körpermitte und flexible Hüften eröffnest Du Dir viele Möglichkeiten. So beginnt die Praxis an diesem Workshop indem sie diesen Bereichen Aufmerksamkeit schenkt.

Unmerklich und ganz natürlich bewegst Du Dich, von dieser Basis ausgehend, in immer akrobatischer anmutende Haltungen. Ronald zeigt hier konkrete Techniken und Schritte wie Du dieses systematisch lernen kannst.

Du verlässt diesen Workshop mit neuen Inspirationen. Dann liegt es an Dir diese durch Deine Praxis weiter zu beleben und zu entwickeln.
 
DR. RONALD STEINER
ist einer der wenigen Yogalehrer, die ganz traditionell von den indischen Meistern Sri K. Pattabhi Jois und BNS Iyengar autorisiert wurden. Er ist zudem Arzt, Wissenschaftler und Sportmediziner mit Forschungen im Bereich Prävention und Rehabilitation u.a. am Universitätsklinikum Ulm. Die von ihm begründete AYInnovation®-Methode baut eine Brücke zwischen uralter Tradition und fundierter Praxis, zwischen angewandter Anatomie und lebendiger Philosophie, zwischen präziser Technik und praktischer Erfahrung. Dadurch entsteht eine für den Einzelnen maßgeschneiderte, sehr persönliche Yogapraxis – von therapeutisch-präventiv bis hin zu sportlich-akrobatisch. Die modularen und deutschlandweit multizentrischen AYI® Yogalehrer Aus- und Weiterbildungen legen einen besonderen Schwerpunkt auf Prävention, Alignment und Yogatherapie.
 
Mehr Infos zu Ronald unter www.ashtangayoga.info


****

D A T U M  / U H R Z E I T 

Freitag, 1. Oktober 2021
17:00 - 21:00 Uhr

****

O R T

YOGABILITY
STUDIO QUARTIER RUHR AUE
Mühlenstr. 9
58313 Herdecke

--------

P R E I S

EUR 85.00

--------

I N F O R M A T I O N   &.  A N M E L D U N G

https://www.yogability.de/workshops/dr-ronald-steiner-learn-to-fly/

Du kannst Dich auch telefonisch unter 02330.8918776, über unsere App oder einfach direkt in unserem Studio anmelden.

www.yogability.de