---
id: "2687987517892332"
title: AYI Inspired Ausbildung bei Yogability ab August 2020
start: 2021-03-20 08:30
end: 2021-03-20 18:00
locationName: Yogability
address: Mühlenstraße 9, 58313 Herdecke
link: https://www.facebook.com/events/2687987517892332/
image: 69392782_2447103868683027_1642686800147251200_n.jpg
teaser: AYI® INSPIRED YOGALEHRER-AUSBILDUNG BEI YOGABILITY *ASHTANGA YOGA INNOVATION •
  200H YOGA ALLIANCE ZERTIFIZIERT*   Diese Ausbildung richtet sich an Yog
isCrawled: true
---
AYI® INSPIRED YOGALEHRER-AUSBILDUNG BEI YOGABILITY
*ASHTANGA YOGA INNOVATION • 200H YOGA ALLIANCE ZERTIFIZIERT* 

Diese Ausbildung richtet sich an Yogaübende, die ihre eigene Praxis weiter kultivieren und ihr Verständnis vertiefen wollen. Sie beleuchtet die traditionellen Hintergründe des Ashtanga Yoga, erklärt die philosophischen Konzepte und zeigt einen Weg, Ashtanga auf innovative und gesundheitsfördernde Weise zu unterrichten.   Dabei ist es ganz egal, ob Du später unterrichten möchtest oder die Ausbildung machst, um tiefer und intensiver in die Welt des Yoga einzutauchen und dabei eine spannende Reise zu Deinem Selbst antrittst.   Die einjährige Ausbildung basiert auf der von Dr. Ronald Steiner entwickelten Ashtanga Yoga Innovation (AYI) Methode. AYInnovation baut eine Brücke zwischen dem traditionellen Übungssystem des Ashtanga Yoga nach Sri K. Pattabhi Jois und den neuesten wissenschaftlichen Erkenntnissen, insbesondere aus den Bereichen Medizin, Bewegungslehre und Psychologie. Du wirst lernen und erfahren, wie die traditionelle Übungsfolge des Ashtanga Yoga sinnvoll modifiziert werden kann, um sie für jeden Menschen (Dich eingeschlossen ;)) nutzbar zu machen. Auch wenn du aus einer anderen Yogatradition kommst, wirst du aus den Grundprinzipien der AYI® für eine gesundheitsfördernde Yogapraxis profitieren und kannst diese in deine Übungspraxis und deinen Unterricht integrieren.

HIGHLIGHTS
Dr. Ronald Steiner bietet während der Ausbildung zwei MTCs (MTC = Modular Therapy Course) an. An diesen beiden Terminen geht es speziell um angewandte Anatomie, Alignment und Yoga-Therapie. Wir freuen uns außerdem, Eberhard Bärr und Anna Trökes als Gastdozenten gewonnen zu haben. Mit Eberhard kannst Du in die alten Weisheitstexte des Yoga eintauchen, während Du mit Anna die Geschichte und Urprünge des Yoga erfährst und Entspannungstechniken erlernst. Das speziell von AYI® entwickelten E-Learning-Tool begleitet Dich während der Ausbildung. Über E-Learning vertiefst Du das Gelernte und Erfahrene. Das web-basierteTool ist obligatorischer Bestandteil der Ausbildung und MTC’s.

SCHWERPUNKTE
• Erarbeitung und Förderung einer eigenen, an den individuellen Voraussetzungen angepasste Yogapraxis
• Verständnis der Ashtanga Yoga Tradition, ihrer Geschichte und Entwicklung
• Systematische Vermittlung der Aspekte: Vinyāsa Count, Drishti, Bandha, Ujjayi
• Erste Serie des Ashtanga Yoga / Unterrichtsformen: Geführte Stunde, Mysore Style
• Unterrichtsmethodik und -didaktik
• Ausrichtung (Alignment)
• Unterrichtsgestaltung von Teilnehmer/Klassen mit besonderen Zielgruppen (z.B. 60+, Schwangere-Yoga)
• Grundlagen der Yoga-Philosophie: Vedanta, Geschichte des Yoga, das Yoga-Sûtra des Patanjali sowie die Übertragung auf das eigene Leben
• Energiekonzepte des Yoga
• Vermittlung von verschiedenen Prānāyāma Techniken und Einstieg in die Meditation
• Einführung in den Faszien Yoga
• Anatomische Grundkenntnisse

ABSCHLUSS MIT ZERTIFIKAT
Die 12-monatige Ausbildung umfasst 250 UEs (inklusive der beiden MTCs). Zertifiziert von der American Yoga Alliance (AYA) ist die Ausbildung ein qualifizierter Start auf dem Weg zum Yogalehrer. Die Ausbildung wird von Svenja Wilke geleitet. Gastdozenten sind Dr. Ronald Steiner, Anna Trökes, Eberhard Bärr und Gerda Eichhorn.  Mit erfolgreichem Abschluss erhält man ein Zertifikat zum AYI® Inspired Lehrer. Die Ausbildung ist ein Grundmodul und berechtigt zur weiterführenden Advanced-Ausbildung bei uns in Herdecke.  

------------

DEINE DOZENTEN

SVENJA WILKE
Durch ihre Mutter, selber Yogalehrerin, kam Svenja schon früh mit Yoga in Kontakt. Den dynamischen Stil des Ashtanga Yoga lernte sie 2007 in Sydney kennen. Es war Liebe auf den ersten Atemzug: Die Dynamik, die Verbindung von Atem und Bewegung sowie die kraftvolle und gleichzeitig elegante Ausdrucksweise haben Svenja sofort in ihren Bann gezogen und sie absolvierte von 2010 bis 2014 bei Anna Trökes und Dr. Ronald Steiner ihre AYI® Ausbildung. Seit 2012 unterrichtet Svenja in der eigenen Yogaschule Yogability in Herdecke (neben Dortmund). Ab April 2017 kannst Du bei Svenja an der AYI® Inspired-Yogalehrerausbildung teilnehmen. Ihr Motto: All we have is NOW.

DR. RONALD STEINER
steht für traditionelles und zugleich innovatives Ashtanga Yoga. Als fortgeschrittener Praktiker ist er fest in der Tradition des Ashtanga verwurzelt und gehört zu den wenigen von Sri K. Pattabhi Jois autorisierten und BNS Iyengar zertifizierten Lehrern in Deutschland. Ausgehend von seinem Hintergrund als Arzt legt er viel Wert auf genaue Ausrichtung für gesundes und heilsames Üben. Er hilft dir deine individuelle Praxis zu entwickeln.

ANNA TRÖKES
unterrichtet bereits seit 1974 Yoga und ist Autorin von mehr als 20 Yoga-Publikationen. Seit 1983 leitet sie Yogalehrer-Ausbildungsgänge für den BDY und lehrt als Ausbildungs-Dozentin beim BDY, der SYG , dem BÖY, der VHS, dem Kneipp-Bund und privaten Ausbildungsschulen die Fächer Hatha-Yoga, Pranayama, Meditation, Yoga-Philosophie (besonders Patañjali), medizinische Grundlagen, Unterrichtsgestaltung und Sprecherziehung für Yogalehrer.

EBERHARD BÄRR
lebte 15 Jahre in Indien und wurde dort zum Yogalehrer im Vivekananda-Institut in Bangalore ausgebildet. Er lebte 10 Jahre mit seinem Lehrer Sukumar in Südindien und hielt dort und in Europa mit ihm zusammen Seminare. Während der langen gemeinsamen Zeit mit Sukumar und durch die Unterweisung anderer indischer Lehrer vertiefte er sein Wissen in die indische Vedanta-Lehre. Er leitet seit vielen Jahren spirituelle Reisen in Indien und Nepal und gibt regelmäßig Seminare in Deutschland, Österreich und in der Schweiz und ist als Referent in vielen Yogalehrer-Ausbildungen tätig.

---------

AUSBILDUNGSTERMINE
 
Sa. 08.08.2020 (08:30) bis
Di. 11.08.2020 (15:30 Uhr)
36 UE – Svenja Wilke

Fr. 30.10.2020 (18:30 Uhr) bis
So. 01.11.2020 (15:30 Uhr)
25,2 UE – Svenja Wilke

Fr. 20.11.2020 (18:30 Uhr) bis
So. 22.11.2020 (15:30 Uhr)
25,2 UE – Svenja Wilke & Anna Trökes

Fr. 29.01.2021 (18:30 Uhr) bis
So. 31.01.2021 (15:30 Uhr)
25,2 UE – Svenja Wilke

Fr. 26.02.2021 (18:30 Uhr) bis
So. 28.02.2021 (15:30 Uhr)
25,2 UE – Svenja Wilke

Fr. 19.03.2021 (18:30 Uhr) bis
So. 21.03.2021 (15:30 Uhr)
25,2 UE – Svenja Wilke & Eberhard Bärr

Fr. 16.04.2021 (18:30 Uhr) bis
So. 18.04.2021 (15:30 Uhr)
25,2 UE – Svenja Wilke

Fr. 20.0.8.2021 (18:30 Uhr) bis
So. 22.08.2021 (15:30 Uhr)
25,2 UE – Svenja Wilke

Innerhalb der Ausbildung sind zwei MTCs mit Dr.
Ronald Steiner obligatorisch. Du kannst Dir über
www.ashtangayoga.info einen Wunsch-MTC
aussuchen oder Deine MTCs mit Ronald bei uns
machen, z.B.:
 
12./13.09.2020
Faszien – Netzwerk unseres Körpers
Info und Anmeldung:
https://www.yogability.de/workshops/dr-ronald-steiner-mtc-faszien-09-2020/

(weitere Termine in Herdecke folgen in Kürze, checke hierfür einfach unsere Webseite)

Du bist frei in der Wahl des Ortes, Themas und Termins. Das MTC sollte nur nicht auf eines der anderen Ausbildungs-Wochenenden fallen und innerhalb der Ausbildungszeit liegen.

---------

KOSTEN

Die Ausbildung kostet EUR 3.000 für AYI Mitglieder (regulärerer Preis: EUR 3.300). Es ist eine Anzahlung von 1.200 € (AYI Mitgliedschaft) bzw. 1.500 € (Regulär) zu leisten. Der Restbetrag von 1.800 € wird in 12 Monatsraten á 150 € gezahlt und muss bis zum Ende der Ausbildung vollständig beglichen sein.
 
Wir gewähren Dir einen Rabatt von EUR 150 bei Zahlung des Gesamtbetrages. 

Zusatzkosten
• 2 MTC mit Ronald Steiner – wir veranstalten während der Ausbildung zwei MTCs in Herdecke für je EUR 265 (AYI Mitglieder) / EUR 295, Preise in anderen Schulen können variieren
• Verpflegung und Unterkunft
• Ausbildungsliteratur (EUR 65)
• AYI Study Mitgliedschaft (EUR 228 jährlich / EUR 109 für Geringverdiener)

Du brauchst eine Unterkunft?
Wir arbeiten mit unserem Hospitality-Partner Zweibrücker Hof **** in Herdecke zusammen und helfen Dir gerne bei der Vermittlung von einem Zimmer zu unserer speziellen Yogability Rate. Sprich uns einfach an.

---------

BEWERBUNG

Alle Infos, den Infoflyer zum Download und das Bewerbertool findest du auf:
https://www.yogability.de/ausbildung/ayi-inspired-ausbildung-2020/

Fragen? Ruf uns an oder schreibe uns eine Email:
Tel: 02330.8918776
Email: ausbildung@yogability.de