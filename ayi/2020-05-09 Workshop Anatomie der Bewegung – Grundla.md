---
id: "2613605935586237"
title: Workshop Anatomie der Bewegung – Grundlagen mit Ronald Steiner
start: 2020-08-15 07:00
end: 2020-08-16 18:00
locationName: Element Yoga
address: Pfuelstrasse 5, 10997 Berlin
link: https://www.facebook.com/events/2613605935586237/
image: 86187897_2687204211357784_653966194528747520_n.jpg
teaser: "Intensiv Workshop Wochenende: Anatomie der Bewegung – Grundlagen für
  Yogalehrende  Mit Dr. Ronald Steiner  Samstag – Sonntag, 15.-16. August
  2020  Als"
isCrawled: true
---
Intensiv Workshop Wochenende: Anatomie der Bewegung – Grundlagen für Yogalehrende

Mit Dr. Ronald Steiner

Samstag – Sonntag, 15.-16. August 2020

Als Yogalehrer hast Du eine wichtige Aufgabe. Du hilfst Deinen Schülern mit Yoga dabei gesund und vital zu bleiben und zu werden.

Damit sich in Deiner Yogastunde das heilsame Potential von Yoga voll entfalten kann, sind ausreichende Kenntnisse über die Anatomie und Physiologie des menschlichen Körpers unerlässlich. Genau diese kannst Du auf diesem Workshop entwickeln. Tauche ein in das Wunderwerk des menschlichen Körpers und lerne häufige Schwierigkeiten in der Praxis gezielt anzugehen.

Der Tag beginnt mit angewandter Anatomie, Alignment-Prinzipien und therapeutischen Übungen. Diese werden vorgestellt und praktisch umgesetzt. Der Abschluss des Tages steht ganz im Schwerpunkt der praktischen Umsetzung.

Zeiten

Samstag: 8-18.30 Uhr
Sonntag: 7-17 Uhr
Mittagspause jeweils 2 bis 2,5 Stunden
Für Yogalehrer und interessierte Yogaschüler

Der Workshop ist ein Teil des 100 stündigen Element Vinyasa Teacher Intensivtrainings. Teilnehmer sind also auch Yogalehrer, die die +300h Element Yogalehrer Fortbildung mit Ausbildungsleiter Alexander Kröker in Berlin absolvieren.

Der Workshop ist praktisch veranlagt und somit auch offen für interessierte Yogaschüler sowie Schüler, die sich in einer Yogalehrer Ausbildung befinden und Yogalehrer aus anderen Stilen.

Preis

295€/ 265€ für AYI Mitglieder
Buchung & Infos

Onlinebuchung auf der Ashtangayoga.info Seite: Anatomie der Bewegung – Grundlagen für Yogalehrende mit Dr. Ronald Steiner im Element Yoga Studio Berlin
Infos über yoga@elementyoga.de
Mehr Informationen zu Dr. Ronald Steiner: AshtangaYoga.info
Unsere Empfehlung für Dich

Um das Intensiv Workshop Wochenende passend zu beginnen, empfehlen wir Dir den Workshop mit Dr. Ronald Steiner am Freitagabend, 14. August 2020: Movement meets Yoga.