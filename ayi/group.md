---
name: Ashtanga Yoga Institute
website: http://ashtangayoga.info
email: 
scrape:
  source: facebook
  options:
    page_id: AshtangaYoga.info
---
AYInsitute Ulm: 200m² mit über 25 Yogakursen pro Woche im Zentrum von Ulm. Home Base. Ausbildungs-Institut. Therapie-Zentrum.
