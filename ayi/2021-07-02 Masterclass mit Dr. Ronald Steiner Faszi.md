---
id: "937445673344243"
title: "Masterclass mit Dr. Ronald Steiner: Faszien Yoga"
start: 2021-07-02 17:00
end: 2021-07-02 21:00
locationName: Yogability
address: Mühlenstraße 9, 58313 Herdecke
link: https://www.facebook.com/events/937445673344243/
image: 93939737_2934982909895118_1339137847608737792_o.jpg
teaser: "MASTERCLASS MIT DR. RONALD STEINER:  FASZIEN YOGA   Faszien – ein wichtiger,
  doch oft wenig greifbarerer Teil unseres Bewegungsapparats. Erlebe in die"
isCrawled: true
---
MASTERCLASS MIT DR. RONALD STEINER: 
FASZIEN YOGA
 
Faszien – ein wichtiger, doch oft wenig greifbarerer Teil unseres Bewegungsapparats. Erlebe in diesem Workshop wie innovativer Ashtanga Yoga das Fasziengewebe aktiv erfahrbar werden lässt und mit welchen Tricks Du Deine Faszien geschmeidig und gesund erhalten kannst!

In der Theorie wissen wir inzwischen so gut wie alle, dass gesunde Faszien wichtig sind für ein balanciertes Körpergefühl und geschmeidige Bewegungen. Doch wie sieht das genau in der Praxis aus? Und können wir unsere Faszien, ein Netzwerk, das unseren ganzen Körper durchzieht, tatsächlich spüren?
 
DR. RONALD STEINER
ist einer der wenigen Yogalehrer, die ganz traditionell von den indischen Meistern Sri K. Pattabhi Jois und BNS Iyengar autorisiert wurden. Er ist zudem Arzt, Wissenschaftler und Sportmediziner mit Forschungen im Bereich Prävention und Rehabilitation u.a. am Universitätsklinikum Ulm. Die von ihm begründete AYInnovation®-Methode baut eine Brücke zwischen uralter Tradition und fundierter Praxis, zwischen angewandter Anatomie und lebendiger Philosophie, zwischen präziser Technik und praktischer Erfahrung. Dadurch entsteht eine für den Einzelnen maßgeschneiderte, sehr persönliche Yogapraxis – von therapeutisch-präventiv bis hin zu sportlich-akrobatisch. Die modularen und deutschlandweit multizentrischen AYI® Yogalehrer Aus- und Weiterbildungen legen einen besonderen Schwerpunkt auf Prävention, Alignment und Yogatherapie.
 
Mehr Infos zu Ronald unter www.ashtangayoga.info


****

D A T U M  / U H R Z E I T 

Freitag, 2. Juli 2021
17:00 - 21:00 Uhr

****

O R T

YOGABILITY
STUDIO QUARTIER RUHR AUE
Mühlenstr. 9
58313 Herdecke

--------

P R E I S

EUR 85.00

--------

I N F O R M A T I O N   &.  A N M E L D U N G

https://www.yogability.de/workshops/masterclass-faszien-ronald-steiner-07-2021/

Du kannst Dich auch telefonisch unter 02330.8918776, über unsere App oder einfach direkt in unserem Studio anmelden.

www.yogability.de