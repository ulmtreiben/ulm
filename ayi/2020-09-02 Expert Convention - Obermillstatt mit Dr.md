---
id: "127337678479900"
title: Expert Convention - Obermillstatt mit Dr. Ronald Steiner
start: 2020-09-02 07:00
end: 2020-09-02 21:00
locationName: AshtangaYoga.info
address: Ulm
link: https://www.facebook.com/events/127337678479900/
teaser: Die Expert Convention ist zum einen eine exklusive Weiterbildungs-Woche für
  die erfahrensten AYInnovation® Lehrer (AYI® Advanced). Zum anderen ist sie
isCrawled: true
---
Die Expert Convention ist zum einen eine exklusive Weiterbildungs-Woche für die erfahrensten AYInnovation® Lehrer (AYI® Advanced). Zum anderen ist sie die Gelegenheit, Dich über ein Jahr mit Assistance und neuen AYI® Learning Inhalten weiterzubilden und Up To Date zu bleiben.

Expert Convention - Unsere Exklusivwoche für die erfahrensten Lehrer und Weiterbildung zum Expert Lehrer
Die Expert Convention ist zum einen eine intensive Woche exklusiv für AYI® Advanced  und Expert Lehrer. Du möchtest die anderen Yogis wiedersehen? Gemeinsam in einer fortgeschrittenen Gruppe üben? Deine Erfahrungen austauschen und neue Inspirationen auftanken? Dann bist Du hier genau richtig.

Zum anderen ist die Expert Convention mit der zugehörigen Vertiefungsaufgabe Deine Gelegenheit, selbst als Expert Lehrer die höchste AYI Ausbildungs Qualifikation zu erreichen.

Inhalte
Du unterrichtest viel, gibst Deine Erfahrungen weiter. Hier hast Du die Gelegenheit in einer fortgeschrittenen Gruppe ganz Schüler zu sein und mit neuer Inspiration aufzutanken. Dieses Jahr erwarten Dich insbesondere folgende Inhalte:

Intensive Praxis von Asana, Pranayama, Chanten und Meditation
Hilfestellungen / Adjustments
Therapeutische Sequenzen
Unterrichtstechniken
Philosophisches Schwerpunktthema
Tagesablauf
Unterricht findet täglich von 07:00 bis 21:00 Uhr statt. Die Ausbildungs-Woche beginnt am Anreisetag um 16:00 Uhr und endet am Abreisetag um 14:30 Uhr. Es gibt zwei Mahlzeiten pro Tag um 11:00 Uhr und um 17:00 Uhr. Ein Nachmittag ist als unterrichtsfrei eingeplant. Änderungen im Stundenplan sind möglich. 

Unterkunft
Soami Retreat Center
Das gesamte SOAMI-Angebot beruht auf einfachen Prinzipien: Gesundheit, Klarheit, Naturbelassenheit und Balance. Der Ort Obermillstatt ist, mit dem nahen Millstätter See (Trinkwasserqualität) und dem angrenzenden “Nationalpark Nockberge“, der ideale Platz um diese Prinzipien auch zu leben. Dies bestimmt auch die Architektur und Einrichtung des Zentrums. Hochwertige und natürliche Materialien wie Bambus, Holz und Glas vermitteln ein angenehmes und atmendes Raumklima. Das Gebäude ist unter Bedachtnahme auf die Feng Shui Philosophie erbaut, nach Süden ausgerichtet und in einen Berghang eingebettet. Die Ruhe und unberührte Natur unterstützen die tägliche Yoga-Praxis. Zudem befindet sich Obermillstatt in einer Zone mit äusserst geringer Strahlenbelastung.

Leistungen
Kat.1: EZ 124.- inkl. Kurtaxe und Verpflegung/Person/Nacht
Kat 2: DZ 118.- inkl. Kurtaxe und Verpflegung/Person/Nacht
Kat 3: DZ zur Einzelnutzung 129.- inkl. Kurtaxe und Verpflegung/Person/Nacht
Alle Zimmer im Soami Zenstil; jedes Zimmer hat ein eigenes Bad und WC.Wir servieren vegan-balanciertes, 100% biologisches, qualitativ höchstwertiges Essen. Ohne Verwendung von Zucker, Nachtschattengewächsen (z.B. Kartoffel), Hauptallergenen und Säurebildnern wie z.B. Kaffee. Verschiedene Alternativkaffeesorten, Tees und bestes, kalkfreies Wasser aus der Bergquelle ist im Preis inbegriffen.

Weitere Informationen und Anmeldung direkt unter office@soami.at

Kosten (Retreatgebühr, ohne Unterkunft, ohne Verpflegung)
990 Euro bzw. 891 Euro für AYI Mitglieder 