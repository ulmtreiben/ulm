---
id: "300189534488083"
title: MTC Atem & Pranayama
start: 2020-11-14 08:00
end: 2020-11-15 18:30
address: The Yogastate Saarbrücken
link: https://www.facebook.com/events/300189534488083/
image: 105994875_1193601237651822_798955148433187794_o.jpg
teaser: In den Atempausen scheint die Zeit still zustehen. Doch wie wirken die
  Atemübungen des Yoga auf unseren Körper und Geist? Wie verändert sich unser
  psy
isCrawled: true
---
In den Atempausen scheint die Zeit still zustehen. Doch wie wirken die Atemübungen des Yoga auf unseren Körper und Geist? Wie verändert sich unser psychisches Befinden, wenn wir regelmäßig üben? Antwort auf diese Frage suchen und finden wir in diesem zweitägigen Workshop. Auch lernen wir, wie wir gezielt Techniken des Pranayama einsetzen können, um zu ganzheitlicher Gesundheit und Wohlbefinden zu gelangen.