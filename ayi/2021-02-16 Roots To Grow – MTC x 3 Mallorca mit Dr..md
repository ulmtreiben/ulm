---
id: "560250107908021"
title: Roots To Grow – MTC x 3 Mallorca mit Dr. Ronald Steiner
start: 2021-02-16 06:30
end: 2021-02-16 22:00
locationName: AshtangaYoga.info
address: Frauenstr. 124, Ulm
link: https://www.facebook.com/events/560250107908021/
image: 87120069_3716201385071814_2927537094358204416_o.jpg
teaser: "Starte dieses Jahr mit einem besonderen Highlight in den Frühling: Gönne Dir
  eine Woche lang Yoga und Yogatherapie pur - in idealer Lern- und Urlaubsa"
isCrawled: true
---
Starte dieses Jahr mit einem besonderen Highlight in den Frühling: Gönne Dir eine Woche lang Yoga und Yogatherapie pur - in idealer Lern- und Urlaubsatmosphäre auf der Sonneninsel Mallorca.

Anatomisches Wissen, neue yogische Inspiration, Zeit zum Lernen, Ausprobieren, Entdecken, Genießen und Entspannen. Das alles bietet Dir unsere MTC x 3 Woche "Roots to Grow" auf Mallorca. Genieße die Wohlfühl-Atmosphäre der Yoga Finca Son Mola Vell und erfahre alles zum Thema "untere Extremitäten"!

Unser MTC Spezialformat bietet Dir die Gelegenheit, Dich eine Woche lang voll und ganz auf das Thema Yogatherapie zu konzentrieren. Tauche ein in die Welt der Anatomie und lerne mehr über Praxisvarianten und therapeutische Übungen! Bei "Roots To Grow" erfährst Du alles Wesentliche über die Basis Deiner Praxis. Wir starten dabei mit den Füßen als Fundament und verfolgen den Weg Deiner Wurzeln bis hin zur Hüfte.

Im Detail
Fuß & Knöchel
Fuß und Knöchel sind das Fundament Deiner Yoga-Praxis und abseits der Yoga-Matte die Basis von gesundem Gehen und Stehen. Deine gesamte Körperhaltung wird durch die Weise wie Du mit dem Boden Kontakt aufnimmst, bestimmt. Du erhältst nicht nur einen intensiven Einblick in die anatomischen Strukturen des Fußes, sondern lernst auch, was notwendig ist, um diese gesund zu halten. Deine eigene Yogapraxis und Dein Yogaunterricht kann so ein stabileres Fundament bekommen.

Einen weiteren Schwerpunkt bilden häufige Erkrankungen bzw. Verletzungen von Fuß und Fußgelenk. Du lernst, was hier beim Praktizieren beachtet werden darf und welche therapeutischen Übungssequenzen ergänzend zu der regulären Praxis sinnvoll sein können. Der dritte Aspekt ist Alignment. Du erfährst, wie Du Deinen eigenen Fuß bei der Yogapraxis in eine harmonische und gesunde Balance führen kannst. 

Knie & Beinachse 
Das Kniegelenk ist eines der erstaunlichsten und wunderbarsten Gelenke des menschlichen Körpers. Nichtsdestotrotz ist es besonders sensitiv und kann im Alltag fehl- und überbelastet werden. In Theorie und Praxis lernst Du Übungen, um die Beanspruchung dieses Gelenkes auszugleichen. Der Zusammenhang des Kniegelenks mit einer physiologischen Beinachse wird so leicht verstanden. Das theoretische Wissen, das Dr. Ronald Steiner Dir anschaulich vermitteln wird, kann Dir helfen, das Gelenk im Alltag und auf der Yogamatte anatomisch korrekt zu benutzen.

Hüfte & Becken 
In dieser Weiterbildung geht es um die anfangs komplex anmutende Anatomie der Hüfte sowie die Möglichkeiten, im Yoga mit der Hüfte aus anatomischer Sicht zu arbeiten. Ein weiterer Schwerpunkt sind häufige Beschwerden im Hüftgelenk. Das richtige Alignment kann wahre Wunder in Deiner Zentrierung bewirken. Du veränderst dabei nicht nur die Ausrichtung Deiner Schulter und Deines Schultergürtels, sondern auch Deine Knie- und Beinachsen erhalten eine neue und mitunter gesundheitsfördernde Ausrichtung. 

Weitere Informationen
Die Umsetzung wird dabei unter anderem anhand einer Bewegungssequenz aus dem Ashtanga Yoga erfahrbar gemacht. Ashtanga-Vorkenntnisse sind dafür jedoch keine Voraussetzung - Yogis und Yogalehrer anderer Traditionen sowie alle anderen am Thema Interessierten sind herzlich willkommen!
Deine MTC-Woche startet am Samstag, 13.02.2021, um 16:00 und endet am Samstag, 20.02.2021, um 14:00. Bitte plane An- und Abreise so, dass Du entspannt ankommen und bis zum Schluss mit dabei sein kannst! Achtung: Die Woche ist nur komplett buchbar.

Vertiefen im E-Learning
Nach Deiner MTC Woche stehen Dir für jedes Thema spannende Aufgaben in Deinem E-Learning bereit. So hast Du insgesamt sechs Monate lang die Möglichkeit, das Gelernte zu reflektieren und zu erfahren. Schließt Du Deine Vertiefungen in dieser Zeit erfolgreich ab, erhältst Du Dein AYI Zertifikat!

Unterkunft und Verpflegung
Das Team der Yoga Finca Son Mola Vell tut alles, um Deinen Aufenthalt auch kulinarisch zu einem Highlight werden zu lassen. Chefkoch José verwöhnt Dich dabei mit abwechslungsreicher und schmackhafter vegetarischer Küche. Die individuellen Zimmer der Yoga Finca sind jedes für sich, komfortabel und im typisch mallorquinischen Finca-Stil eingerichtet. Alle Zimmer verfügen über ein Bad mit Dusche oder Badewanne / WC und Föhn.

Leistungen der Finca
Eine Woche Unterkunft und Verpflegung ab 630,00 €/p.P. einschließlich:
- Übernachtung im geteilten Doppelzimmer mit eigener Dusche/WC vorbehaltlich Gegenbuchungen. 
- Aufpreis für Einzelzimmer auf Anfrage (kleines EZ ohne Aufpreis)
- vegetarische Halbpension.
- mittags Früchte, ganztägig Wasser und Tee
- Betreuung durch das NEUE WEGE Finca Team - neuewege.com

Bitte habe Verständnis, dass nur eine begrenzte Anzahl an EZ zur Verfügung stehen und Du Dir evtl. ein DZ teilst, denn so können wir mehr Yogis die Möglichkeiten geben an der Ausbildung teilzunehmen. 

Zusatzleistungen 
Zimmerreservierung und Shuttle vom Flughafen (20€/je Strecke) über - neuewege.com
Ökosteuer 2€/Tag wird vor Ort bezahlt.
