---
id: "3636477073044056"
title: "Workshop: Yoga und Deine Selbstheilungskräfte mit Anna Trökes"
start: 2020-06-11 09:15
end: 2020-06-11 12:30
locationName: AshtangaYoga.info
address: Ulm
link: https://www.facebook.com/events/3636477073044056/
image: 82227520_3600240756667878_4289580079865921536_o.jpg
teaser: Lerne von Anna Trökes, wie Du mit Hilfe von Yoga Deine Selbstheilungskräfte
  aktivieren und Stress reduzieren kannst.  Stress im Alltag ist inzwischen
isCrawled: true
---
Lerne von Anna Trökes, wie Du mit Hilfe von Yoga Deine Selbstheilungskräfte aktivieren und Stress reduzieren kannst.

Stress im Alltag ist inzwischen so umfassend und weltumspannend verbreitet, dass die WHO „Stress die größte Gesundheitsgefahr des 21.Jahrhunderts“ nennt! Die Forschungen der theoretischen und vor allem klinischen Mind-Body-Medizin, der Psychoneuroimmunologie und der Neurowissenschaften machen deutlich, dass Stress uns noch viel mehr schadet, als wir je geahnt haben. Das Organsystem, das davon am meisten betroffen ist, ist unser Immunsystem – und damit unsere Fähigkeit zur Selbstheilung. 

Stressbewältigung durch Yoga
In den verschiedenen Yoga-Systemen wurden nicht nur seit über 2000 Jahren viele Konzepte entwickelt, die erklären, warum unser Geist so beschaffen ist, dass wir immer wieder Stress erfahren, sondern vor allem die vielfältigsten Übungswege, um das Erkannte in die Praxis umzusetzen. Dabei geht es vorrangig darum, uns Methoden zur Verfügung zu stellen, die helfen unseren Geist ruhiger und stabiler werden zu lassen – damit er sich nicht mehr so schnell und andauernd stressen lässt.

Die Erkenntnisse der Mind-Body-Medizin und der Psychoneuroimmunologie beweisen klar, dass unsere Fähigkeit zur Selbstheilung und damit jede Gesundheitsförderung in unserem Kopf beginnen muss.

Was erwartet Dich?
Anna Trökes möchte in diesem Workshop über 

- die Wirkmechanismen von Stress
- die Wirkung auf unser Immunsystem 
- das Finden angemessener Stress- bzw. Entspannungsantworten
- die Stärkung unserer Selbstheilungskräfte 
informieren. In Theorie und Praxis erfährst Du, was jede und jeder von uns auf der Grundlage dieser Erkenntnisse für die eigene Gesundheit tun kann.

Tagesplan
Mittwoch	   9:15 - 12:30 Uhr
Donnerstag 9:15 - 12:30 Uhr
Freitag	    9:15 - 12:30 Uhr

Kosten
145 Euro bzw. AYI Mitglieder 130,50 Euro

Veranstaltungsort
AYInstitute Ulm
Frauenstr. 124
89073 Ulm