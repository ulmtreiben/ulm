---
id: "121592565854053"
title: WWF Spendentour Deutschland - Yoga für Artenvielfalt
start: 2020-03-05 20:15
end: 2020-03-05 21:45
locationName: AshtangaYoga.info
address: Ulm
link: https://www.facebook.com/events/121592565854053/
teaser: Die Einzigartigkeit jedes einzelne Lebewesen ist wichtig für die
  Gesamt-Balance unserer Welt. Wir möchten dies unterstützen. Übe zwischen dem
  03.03.-0
isCrawled: true
---
Die Einzigartigkeit jedes einzelne Lebewesen ist wichtig für die Gesamt-Balance unserer Welt. Wir möchten dies unterstützen. Übe zwischen dem 03.03.-09.03.2020 deutschlandweit mit Dr. Ronald Steiner und unterstütze die WWF mit Deiner Spende!

Vom Delfin über den Baum bis hin zum Bienensummen steht die Natur Pate für viele Yoga-Übungen, mit denen immer mehr Menschen versuchen, innerlich Balance zu finden. Um auch im Äußeren wieder für mehr Gleichgewicht zu sorgen, ruft der WWF Deutschland eine nationale Spendenkampagne ins Leben, bei denen den natürlichen Vorbildern etwas zurück gegeben werden kann.

Dr. Ronald Steiner: Für den Pfau
Yoga für Artenvielfalt
Die Aktion startet zum internationalen Tag des Artenschutzes am 03. März 2020 unter dem Motto «Yoga für Artenvielfalt» und ruft sämtliche Yogalehrer und Yogastudios in Deutschland auf, den Kampf gegen das Artensterben mit einer oder mehreren spendenbasierten Yogastunden zu unterstützen. Wir sind dabei!  Erlebe und praktiziere mit Dr. Ronald Steiner. Alle Stunden (s. unten) finden auf Spenden-Basis statt und unterstützen die WWF Kampagne zur Artenvielfalt. 

Du möchtest die WWF Deutschland unterstützen? Hier findest Du mehr Informationen: www.wwf.de 

Die Zeit ist knapp! Laut einer Studie des Weltbiodiversitätsrats IPBES befinden wir uns heute im größten Artensterben seit dem Ende der Dinosaurier vor 65 Millionen Jahren. Eine Million von insgesamt acht Millionen Tier- und Pflanzenarten sind vom Aussterben bedroht. Der WWF gibt alles, um diese Entwicklung mit seinen Artenschutz-Programmen zu bremsen und hofft auf tatkräftige Unterstützung bei der Erhaltung eines lebenswerten Planeten.

Eine Herzensangelegenheit
„Ich bin Yogi – schon mein ganzes Leben lang. Und wenn ich Yoga lebe, so besteht es aus zwei Teilen: Der eine Teil ist meine Praxis auf der Matte. Da ziehe ich mich zurück von der Welt, nehme mir eine Zeit, wo ich nur für mich da bin. Mit einem Ziel: Meine persönliche Balance, meine persönliche Harmonie zu finden und zu kultivieren. Wenn meine Praxis dann aber beendet ist, beginnt der zweite Teil des Yogas. Nämlich der Yoga im Alltag. Ich versuche auch im Alltag diese Harmonie, die ich für mich suche, hinaus zu tragen. Hinaus zu tragen zu meinen Mitmenschen. Ich versuche harmonischer mit meinen Mitmenschen umzugehen, mit den Tieren, mit der Umwelt. Und für diese Welt ist Vielfalt wichtig! Jeder Mensch ist einzigartig. Und durch die Einzigartigkeit von jedem von uns, ist unsere Gesellschaft so reich, wie sie ist. Es ist wichtig unsere Einzigartigkeit als Mensch zu akzeptieren, zu kultivieren und auch zu leben. Bei den Tieren ist es ganz genauso. Die Einzigartigkeit jedes einzelnen Tieres, jedes einzelnen Lebewesens auf dieser Welt ist wichtig für die Gesamtbalance dieser Welt und deswegen setze ich mich ein, bei der WWF Spendenkampagne «Yoga für die Artenvielfalt»." - Dr. Ronald Steiner - 

Termine und Orte
Dienstag, 03.03.2020, 19:00 - 21:00 Uhr	
AYI, Frauenstr. 124, 89075 Ulm

Donnerstag, 05.03.2020, 09:15 - 10:45 Uhr	
Jivamukti Yogaloft Gärtnerplatz, Buttermelcherstr. 11-15, 80469 München, jivamukti.de

Donnerstag, 05.03.2020, 20:15 - 21:45 Uhr
Element Yoga, Pfuelstraße 5, 10997 Berlin-Kreuzberg, elementyoga.de

Freitag, 06.03.2020, 12:00 - 14:00 Uhr	
Yogaraum, Laeiszstrasse 15, 20357 Hamburg, yogaraum-hamburg.de

Freitag, 06.03.2020, 18:00 - 20:00 Uhr	
YOMA, Am Ziegenmarkt, Römerstr. 1, 28203 Bremen, yo-ma.info

Montag, 09.03.2020, 09:30 – 11:30 Uhr	
Privat Yoga Institute, Mörfelder Landstraße 44, 60598 Frankfurt am Main, private-yoga-frankfurt.de

