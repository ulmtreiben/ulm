---
id: "684447408925177"
title: YONAMO Online
start: 2020-11-22 08:00
end: 2020-11-22 18:15
link: https://www.facebook.com/events/684447408925177/
image: 125323510_1233079093741972_2369804398575167035_o.jpg
teaser: "Am Sonntag, 22. November, geht Yonamo zum zweiten Mal online: Von 08.00 bis
  18.15 Uhr erwarten dich alles rund um Yoga, Nachhaltigkeit und einen gesun"
isCrawled: true
---
Am Sonntag, 22. November, geht Yonamo zum zweiten Mal online: Von 08.00 bis 18.15 Uhr erwarten dich alles rund um Yoga, Nachhaltigkeit und einen gesunden, bewussten Lifestyle: abwechslungsreiche Yoga-Sessions, spannende Vorträge, interessante Inter-views und attraktive Verlosungen – und das komplett gratis.

Freu dich auf eine geballte Ladung Information und Inspiration zu Astrologie, Ayurveda, bewusstem Haare-Schneiden, Energiemedizin, Ernährung, Naturkosmetik, Meditation und 
Psychologie. Mische gemeinsam mit Lilly Galic Naturkosmetik für die kalte Jahreszeit und erlebe live, was bewusstes Haareschneiden von einem normalen Haarschnitt unterscheidet. Neu stehen die Referenten nach ihren Sessions in einem virtuellen Begegnungsraum für Fragen zur Verfügung. 

Gewinne am Morgen früh in der Asthanga-Yoga Session ein neues Körperbewusstsein mit Ronald Steiner. Christina Eggenschwiler führt dich durchs Yoga für einen gesunden Rücken. Ins Schwitzen kommst du bei der beliebten Athletic Flow Session mit Simon Kersten. Im da-rauffolgenden sanften Vinyasa Flow mit seiner Frau Nora Kersten bist du eingeladen, in Bewegung loszulassen. Danach gibt’s Yoga, das die astrologischen Gegebenheiten berücksichtigt und eine Herz öffnende und lebensbejahende Hatha-Flow Session mit Claire Dalloz. Lass den Tag mit einem ganz besonderen Highlight ausklingen: dem Yoga-Nidra mit Claudia Reinig.

Yonamo Online gibt’s live auf www.yonamo.com und YouTube.
