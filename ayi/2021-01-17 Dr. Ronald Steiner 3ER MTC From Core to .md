---
id: "451222049126543"
title: "Dr. Ronald Steiner: 3ER MTC From Core to Flight in Berlin"
start: 2021-01-17 08:30
end: 2021-01-22 17:30
address: Schützenstraße 7, 12165 Berlin, Deutschland (EGNOKA)
link: https://www.facebook.com/events/451222049126543/
image: 79602864_2634786369914775_985409177243353088_o.jpg
teaser: I N F O R M A T I O N   &   T H E M A  3ER MTC "FROM CORE TO FLIGHT" MIT DR.
  RONALD STEINER IN BERLIN   Dieses Spezialformat bietet Dir die Gelegenhei
isCrawled: true
---
I N F O R M A T I O N   &   T H E M A

3ER MTC "FROM CORE TO FLIGHT" MIT DR. RONALD STEINER IN BERLIN
 
Dieses Spezialformat bietet Dir die Gelegenheit, Dich eine Woche lang ausschließlich auf das Thema Yogatherapie zu konzentrieren. Tauche ein in die Welt der Anatomie und lerne mehr über Praxisvarianten und therapeutische Übungen! Dabei erhältst Du nicht nur eine Menge an theoretischen Informationen, sondern kannst das Gelernte auch direkt in der Praxis umsetzen und so für Dich selbst erproben. In der MTC Serie „From Core to Flight“ lernst Du alles Wichtige zum Thema obere Extremitäten. Ausgehend vom Rücken beschäftigen wir uns damit, welche Rolle auch Schulter/Schultergürtel sowie unsere Hände und Handgelenke in der Praxis spielen und wie es uns gelingt, diese langfristig gesund zu halten und neben den Füßen zu einem zweiten Fundament werden zu lassen. So gelingen auch forderndere Positionen sowie Armbalancen zunehmend mühelos!
 

MTC 1: Rücken und Körperhaltung

Egal ob Du gerade erst mit dem Yoga begonnen hast oder viele Jahre praktizierst, immer bist du mit den Möglichkeiten und Grenzen Deines Rückens konfrontiert. Um auch fortgeschrittenere Asanas auf eine für Deinen Körper gesunde Weise zu üben, ist es wichtig, etwas über die Anatomie des Halteapparates zu wissen.

In Zeiten, in denen Tätigkeiten zunehmen sitzend ausgeführt werden, sind Rückenschmerzen für viele Menschen leider ein fast schon täglicher Begleiter. Doch auch bei Yogis ist das Thema Rücken und Wirbelsäule oft ein großes Thema, gerade wenn es darum geht, den Rücken einerseits ausreichend zu kräftigen, andererseits aber auch die notwendige Flexibilität zu erhalten.

Lerne in diesem MTC, welche Übungen Du in Deine Praxis und Deinen Unterricht integrieren kannst, um Deinen Rücken langfristig gesund zu erhalten und wie Du Deine Schüler optimal bei bestehenden Problemen unterstützt.

MTC 2: Schulter und Schultergürtel

Schulter und Schultergürtel sind der Gelenk-Komplex mit dem größten Bewegungsspielraum im menschlichen Körper. In dieser Weiterbildung erfährst wie Du mit einer Yogapraxis Gesundheit erhalten (Alignment) wieder herstellen (Yogatherapie) kannst.

Die Stellung der Schultern und Schulterblätter und Schultern beeinflusst unsere Körperhaltung als Ganzes. Sie kann daher nicht isoliert gesehen werden, sondern im größeren Zusammenhang. Durch einseitige Belastung, Überlastung, Unterforderung oder Fehlhaltung kann es zu Schmerzen an der Schulter selbst aber auch an scheinbar entfernten Orten, wie dem Rücken oder dem Nacken kommen. Hier können wir mit Yoga effektiv einsetzen und über gezieltes Üben die Schultern balancieren. Andererseits stellt das Yoga auch hohe Ansprüche an die Schulter wir stützen uns auf unsere Arme, bewegen sie in ihrem maximalen Umfang. Genaue Kenntnisse der Anatomie und effektive Umsetzung dieses Wissens in die Yogapraxis sind essentiell für ein Gesundes üben und unterrichten.

MTC 3: Arm und Hand

Die Hände und Arme werden in unserer Yogapraxis zu Flügeln und auch zu Wurzeln. Ein MTC das hilft mehr über Arm und Hand zu verstehen.

Welche Rolle spielen unsere Arme und Hände in unserer Praxis – gerade dann, wenn sie plötzlich von den Flügeln zu unseren Wurzeln werden, um uns ein stabiles Fundament zu geben? Dieser Workshop beleuchtet die anatomischen Besonderheiten unserer Arme und Hand(gelenke) und geht dabei natürlich auch auf die gängigsten Beschwerden ein. Im praktischen Teil erhältst Du Tipps, wie Du Deine Gelenke langfristig gesund halten kannst und gleichzeitig Stabilität und Flexibilität gewinnst.

Der Praxisteil ist dabei so aufgebaut, dass Du kein Vorerfahrung im Bereich Ashtanga benötigst. Alle am Thema Interessierten sind herzlich willkommen!

***

Tagesablauf

Der Tag beginnt mit angewandter Anatomie, Alignment-Prinzipien und therapeutischen Übungen. Diese werden vorgestellt und praktisch umgesetzt.Der Abschluss des Tages steht ganz im Schwerpunkt der praktischen Umsetzung.Im Tagesverlauf je zwei Pausen von jeweils ca. 1 bis 1,5 Stunden.

----

FÜR WEN IST EIN MODULAR THERAPY COURSE (MTC) INTERESSANT?

Der Modular Therapy Course (MTC) richtet sich gezielt an Yogalehrer, auch anderer Tradition, und interessiert Übende. Du tauchst ein in die anatomischen und physiologischen Hintergründe Deiner Yoga Praxis. Wenn Du bereits eine eigene Yogapraxis, egal in welcher Tradition, etabliert hast und Dich Alignment, Prävention und Yogatherapie interessieren, dann bist Du hier genau richtig.Im Modular Therapy Course (MTC) erlernst Du therapeutische Sequenzen und Bandhlign® Techniken für eine gesunde Ausrichtung in Deiner Yogapraxis. Beides kannst Du nach der Weiterbildung direkt in Deine eigene Yogapraxis und Deinen Unterricht integrieren. Im Modular Therapy Course (MTC) erfährst Du die anatomischen Hintergründe und erlangst ein Verständnis über häufige Verletzungen und Erkrankungen. Zusätzlich erhältst Du ein Handbuch, in dem Du die wesentlichen Inhalte nachlesen kannst.Neben dem Kontaktunterricht hast Du in den dem Modular Therapy Course (MTC) folgenden zwei Monaten die Möglichkeit im AYI® Learning die erlernten Techniken zu vertiefen und für die persönliche Praxis und den Unterricht nutzbar zu machen. Nach Deiner erfolgreichen Teilnahme erhältst Du ein Zertifikat. Dies bestätigt Dir die Inhalte wirklich praktisch anwenden zu können. So kann das MTC ein Modul Deiner AYInnovation® Yogalehrer Ausbildung oder Weiterbildung werden.
 
----

DER DOZENT

Dr. Ronald Steiner ist einer der wenigen Yogalehrer, die ganz traditionell von den indischen Meistern Sri K. Pattabhi Jois und BNS Iyengar autorisiert wurden. Er ist zudem Arzt, Wissenschaftler und Sportmediziner mit Forschungen im Bereich Prävention und Rehabilitation u.a. am Universitätsklinikum Ulm. Die von ihm begründete AYInnovation®-Methode baut eine Brücke zwischen uralter Tradition und fundierter Praxis, zwischen angewandter Anatomie und lebendiger Philosophie, zwischen präziser Technik und praktischer Erfahrung. Dadurch entsteht eine für den Einzelnen maßgeschneiderte, sehr persönliche Yogapraxis – von therapeutisch-präventiv bis hin zu sportlich-akrobatisch. Die modularen und deutschlandweit multizentrischen AYI® Yogalehrer Aus- und Weiterbildungen legen einen besonderen Schwerpunkt auf Prävention, Alignment und Yogatherapie..
 
Mehr zu Dr. Ronald Steiner:
https://de.ashtangayoga.info

----

V O R A U S S E T Z U N G E N

Der Workshop / MTC richtet sich gezielt and Yogalehrer (auch anderer Tradition) und interessiert Übende.

----

O R T

Der MTC ist einer von drei MTCs, die nacheinander in der Egnoka Akademie in Berlin Steglitz stattfinden. Wir üben in einem lichtdurchfluteten Loft mit Umkleiden, Duschen, (Tee)Küche und Aufenthaltsraum. Die Akademie ist nur wenige Schritte vom S-Bahnhof „Rathaus Steglitz“ entfernt. In direkter Nachbarschaft gibt es zahlreiche Cafés, Restaurants, kleinere Parks und günstige Übernachtungsmöglichkeiten.

Adresse:
Schützenstraße 7
12165 Berlin-Steglitz

Haltestelle: Rathaus Steglitz

----

P R O G R A M M 

MTC1: Rücken und Körperhaltung

So. 17. Januar 2021: 08:00 Uhr – 18:30 Uhr
Theorie und Praxis
Mo 18. Januar 2021: 07:30 Uhr – 17:30 Uhr
Theorie und Praxis

MTC2: Schulter und Schultergürtel

So. 19. Januar 2021: 08:00 Uhr – 18:30 Uhr
Theorie und Praxis
Mo 20. Januar 2021: 07:30 Uhr – 17:30 Uhr
Theorie und Praxis

MTC3: Arm und Hand

So. 21. Januar 2021: 08:00 Uhr – 18:30 Uhr
Theorie und Praxis
Mo 22. Januar 2021: 07:30 Uhr – 17:30 Uhr
Theorie und Praxis
 
 ----

P R E I S  

AYI Mitglieder: 750,00 € (statt 795,00 €)
Nicht-Mitglieder: 840,00 € (statt 885,00 €)
 
Inkl. Snacks, Wasser und Tee an allen Tagen.

Übernachtungen können auf Wunsch dazu gebucht werden, wir verfügen über attraktive Deals mit Hotels in der nahen Umgebung, teils fußläufig. Fragt uns einfach!

----

A N M E L D U N G

https://www.yogability.de/workshops/dr-ronald-steiner-from-core-to-flight-berlin-2021/

Telefon: 02330.8918776
...oder im Studio in Herdecke

Download iOS: https://goo.gl/rAbzEI
Download Android: https://goo.gl/2cQ0EF