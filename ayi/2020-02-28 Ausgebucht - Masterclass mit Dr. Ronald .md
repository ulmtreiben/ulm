---
id: "387558078632154"
title: "Ausgebucht - Masterclass mit Dr. Ronald Steiner: Hüftöffner"
start: 2020-02-28 17:00
end: 2020-02-28 21:00
locationName: Yogability
address: Mühlenstraße 9, 58313 Herdecke
link: https://www.facebook.com/events/387558078632154/
image: 74378403_2556073801119366_752429878862675968_n.jpg
teaser: "UPDATE 28.10.2019: Die Masterclass ist ausgebucht und die Warteliste sehr
  voll. Wir nehmen daher keine Anmeldungen mehr an. In den nächsten Tagen geht"
isCrawled: true
---
UPDATE 28.10.2019: Die Masterclass ist ausgebucht und die Warteliste sehr voll. Wir nehmen daher keine Anmeldungen mehr an. In den nächsten Tagen geht die nächste Masterclass mit Ronald online (Thema: Movement Yoga). Bitte schaue einfach auf unsere Webseite oder abonniere unsere Wochenpost (ganz easy auf www.yogability.de). Danke für Dein Verständnis!


MASTERCLASS MIT DR. RONALD STEINER: 
HÜFTÖFFNER (NEU ENTDECKT)
 
Hüftöffner - ein alter Hut mit den ewig gleichen Übungen? Von wegen! Dieser Workshop eröffnet Dir neue Perspektiven auf bekannte Asanas und zeigt Dir, wie Du innovative Elemente in Deine Praxis integrieren kannst.

Die sogenannten "Hüftöffner", also Übungen, die die Hüfte mobilisieren und flexibler machen sollen, sind fast schon ein so fester Bestandteil der Yoga-Praxis wie die Sonnengrüße oder das Anfangs- und End-Mantra. Auch bei den Teilnehmern rangiert der Wunsch "mehr Beweglichkeit in der Hüfte" meist ganz oben, wenn man sie nach ihrer Motivation fragt, Yoga neu zu beginnen oder regelmäßig zu üben.

Gerade deshalb lohnt es sich, dieses Thema mit einem frischen Blick zu betrachten! Lerne in diesem Workshop, wie Du bekannte Asanas sinnvoll für Dich modifizieren kannst und wie Du innovative Ansätze in Deine regelmäßige Praxis integrierst.  Das praktische Erleben und Ausprobieren steht dabei im Zentrum dieses Workshops.
 
DR. RONALD STEINER
ist einer der wenigen Yogalehrer, die ganz traditionell von den indischen Meistern Sri K. Pattabhi Jois und BNS Iyengar autorisiert wurden. Er ist zudem Arzt, Wissenschaftler und Sportmediziner mit Forschungen im Bereich Prävention und Rehabilitation u.a. am Universitätsklinikum Ulm. Die von ihm begründete AYInnovation®-Methode baut eine Brücke zwischen uralter Tradition und fundierter Praxis, zwischen angewandter Anatomie und lebendiger Philosophie, zwischen präziser Technik und praktischer Erfahrung. Dadurch entsteht eine für den Einzelnen maßgeschneiderte, sehr persönliche Yogapraxis – von therapeutisch-präventiv bis hin zu sportlich-akrobatisch. Die modularen und deutschlandweit multizentrischen AYI® Yogalehrer Aus- und Weiterbildungen legen einen besonderen Schwerpunkt auf Prävention, Alignment und Yogatherapie.
 
Mehr Infos zu Ronald unter www.ashtangayoga.info


****

D A T U M  / U H R Z E I T 

Freitag, 28. September 2020
17:00 - 21:00 Uhr (diesmal 4 Stunden)

****

O R T

YOGABILITY
STUDIO QUARTIER RUHR AUE
Mühlenstr. 9
58313 Herdecke

--------

P R E I S

EUR 85.00

--------

I N F O R M A T I O N   &.  A N M E L D U N G

https://www.yogability.de/workshops/dr-ronald-steiner-masterclass-hueftoeffner/

Du kannst Dich auch telefonisch unter 02330.8918776, über unsere App oder einfach direkt in unserem Studio anmelden.

www.yogability.de