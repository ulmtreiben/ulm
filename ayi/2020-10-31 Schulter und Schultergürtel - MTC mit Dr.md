---
id: "337165200673386"
title: Schulter und Schultergürtel - MTC mit Dr. Ronald Steiner
start: 2020-10-31 08:00
end: 2020-11-01 17:30
locationName: Yogaschule SOLIS
address: Gliesmaroder Straße 1, 38106 Braunschweig
link: https://www.facebook.com/events/337165200673386/
image: 120199072_3444093075611217_8127010921567893689_n.jpg
teaser: Schulter und Schultergürtel sind der Gelenk-Komplex mit dem größten
  Bewegungsspielraum im menschlichen Körper. In dieser Weiterbildung erfährst
  wie Du
isCrawled: true
---
Schulter und Schultergürtel sind der Gelenk-Komplex mit dem größten Bewegungsspielraum im menschlichen Körper. In dieser Weiterbildung erfährst wie Du mit einer Yogapraxis Gesundheit erhalten (Alignment) wieder herstellen (Yogatherapie) kannst.

Die Stellung der Schultern und Schulterblätter beeinflusst unsere Körperhaltung als Ganzes. Sie kann daher nicht isoliert gesehen werden, sondern nur im größeren Zusammenhang. Durch einseitige Belastung, Überlastung, Unterforderung oder Fehlhaltung kann es zu Schmerzen an der Schulter selbst aber auch an scheinbar entfernten Orten, wie dem Rücken oder dem Nacken kommen. Hier können wir mit Yoga effektiv ansetzen und über gezieltes Üben die Schultern balancieren.

Andererseits stellt das Yoga auch hohe Ansprüche an die Schulter: Wir stützen uns auf unsere Arme, bewegen sie in ihrem maximalen Umfang, balancieren auf unseren Händen. Genaue Kenntnisse der Anatomie und eine effektive Umsetzung dieses Wissens in die Yogapraxis sind deshalb essentiell für ein gesundes Üben und Unterrichten.

Lerne, wie Du Schritt für Schritt den Körper gezielt auf diese Herausforderungen vorbereiten kannst und welche alternativen Formen und Vorübungen es gibt, wenn bestimmte Haltungen in der traditionellen Variante gerade nicht funktionieren. Auf diese Weise ensteht Schritt für Schritt eine individuelle, vollkommen an die Bedürfnisse der/des Übenden* angepasste wohltuende Yogapraxis.

Der Tag beginnt mit angewandter Anatomie, Alignment-Prinzipien und therapeutischen Übungen. Diese werden vorgestellt und praktisch umgesetzt.Der Abschluss des Tages steht ganz im Schwerpunkt der praktischen Umsetzung.

Der Workshop ist Bestandteil eines Modular Training Course (MTC), ein Aus- und Weiterbildungskonzept für Yogalehrer*innen und interessierte –praktiker*innen verschiedener Yogatraditionen. Zusammen mit weiteren Modulen werden systematisch alle wichtigen Aspekte der Anatomie und Physiologie besprochen. Die Module sind dabei eigenständig und unabhängig voneinander buch- und besuchbar. Jedes Modul besteht aus theoretischen und praktischen Einheiten und wird durch Selbststudium und einen Abschlusstest vervollständigt. Dazu werden Dir Unterrichtsmaterialien zur Verfügung gestellt. Die erfolgreiche Teilnahme wird mit einem Zertifikat bestätigt.

Dr. Ronald Steiner steht für traditionelles und zugleich innovatives Ashtanga Yoga. Als fortgeschrittener Praktiker ist er fest in der Tradition des Ashtanga verwurzelt und gehört zu den wenigen von Sri K. Pattabhi  Jois autorisierten und BNS Iyengar zertifizierten Lehrern. Ausgehend von seinem Hintergrund als Arzt legt er viel Wert auf genaue Ausrichtung für gesundes und heilsames Üben. Er hilft Dir Deine individuelle Praxis zu entwickeln. Weitere Informationen unter: AshtangaYoga.info

Samstag
8:00 - 18:30 Theorie und Praxis

Sonntag
7:30 - 17:30 Theorie und Praxis

Veranstaltungsort:
New Yorker Musische Akademie
Neustadtring 9
38114 Braunschweig

Kosten: 295,- EUR

Weitere Informationen auf: https://www.solis-yoga.de/workshops-und-seminare/articles/mtc-mit-dr-ronald-steiner-schulter-und-schulterguertel-freiheit-und-weite/

Wir freuen uns auf Deine Teilnahme :-)!