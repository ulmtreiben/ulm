---
id: "408782269688903"
title: AYI Inspired Ausbildung Ulm ab Juni 2019
start: 2020-03-22 08:00
end: 2020-03-22 19:00
address: AshtangaYoga.info
link: https://www.facebook.com/events/408782269688903/
image: 55589298_2754726171219345_546128426243719168_n.jpg
teaser: Die Inspired Ausbildung (AYI®) ist der Einstieg in die AYInnovation®
  Yogalehrerausbildung. Sie richtet sich an Yogaübende, die ihr Wissen vertiefen
  wo
isCrawled: true
---
Die Inspired Ausbildung (AYI®) ist der Einstieg in die AYInnovation® Yogalehrerausbildung. Sie richtet sich an Yogaübende, die ihr Wissen vertiefen wollen. Mit fundiertem Fachwissen begleiten Dich Anna Trökes, Melanie Pillhofer und Ronald Steiner über ein Jahr auf Deinem persönlichen Yogaweg.

Uhrzeiten vorbehaltlich Änderungen. 