---
id: "3110753545615178"
title: From Core to Flight - MTC x 3 Ulm mit Dr. Ronald Steiner
start: 2020-05-31 07:00
end: 2020-05-31 17:00
locationName: AshtangaYoga.info
address: Ulm
link: https://www.facebook.com/events/3110753545615178/
image: 82253822_3597042003654420_3271294753332789248_o.jpg
teaser: MTC x 3 - Du hast die Gelegenheit über ein halbes Jahr intensiv über 3 mal 2
  Tage in das Thema Yogatherapie einzutauchen. Ein E-Learning hilft Dir zwi
isCrawled: true
---
MTC x 3 - Du hast die Gelegenheit über ein halbes Jahr intensiv über 3 mal 2 Tage in das Thema Yogatherapie einzutauchen. Ein E-Learning hilft Dir zwischen den Präsenz-Terminen das Gelernte gleich praktisch umzusetzen. Wir beginnen am Körperzentrum, dem Rücken, und arbeiten uns dann über Schultern bis zu den Händen vor.

Details
Bei "From Core to Flight" erwartet Dich Detailwissen zu den folgenden Themen: 
Rücken und Körperhaltung (04.+05.04.20) -> fällt aus! Bitte wende Dich zur kostenfreien Umbuchung an unser Team: ausbildung@ayi.info
Schulter und Schultergürtle (30.+31.05.20)
Arm und Hand (17.+18.10.20) 
Du kannst diese Themen auch (auf der jeweiligen Detail-Seite) einzeln Buchen für 295€/Thema: https://de.ashtangayoga.info/workshops-ausbildung/modular-therapy-course-mtc/200404-ulm-mtc-x-3-ruecken-schulte/

MTC x 3: Dieses Spezialformat bietet Dir die Gelegenheit, Dich im Verlauf eines halben Jahres intensiv auf das Thema Yogatherapie zu konzentrieren. Tauche ein in die Welt der Anatomie und lerne mehr über Praxisvarianten und therapeutische Übungen! Dabei erhältst Du nicht nur eine Menge an theoretischen Informationen, sondern kannst das Gelernte auch direkt in der Praxis umsetzen und so für Dich selbst erproben.

In der MTC Serie "From Core to Flight" lernst Du alles Wichtige zum Thema obere Extremitäten. Ausgehend vom Rücken beschäftigen wir uns damit, welche Rolle auch Schulter/Schultergürtel sowie unsere Hände und Handgelenke in der Praxis spielen und wie es uns gelingt, diese langfristig gesund zu halten und neben den Füßen zu einem zweiten Fundament werden zu lassen. So gelingen auch forderndere Positionen sowie Armbalancen zunehmend mühelos!

Vertiefen im E-Learning
Nach jedem der drei Themen stehen Dir spannende Aufgaben in Deinem E-Learning zur Vertiefung bereit. So hast Du insgesamt drei mal zwei Monate lang die Möglichkeit, das Gelernte zu reflektieren und zu erfahren. Schließt Du Deine Vertiefungen in dieser Zeit erfolgreich ab, erhältst Du Dein AYI Zertifikat. Es bestätigt Dir das gelernte wirklich anwenden zu können. Es kann Bestandteil Deiner Yogalehrer Aus- oder Weiterbildung werden.


