---
id: "249997883000109"
title: Ecstatic Dance
start: 2020-07-11 11:00
end: 2020-07-11 13:30
locationName: Shiva Dance
address: Frauenstraße 124, 89073 Ulm
link: https://www.facebook.com/events/249997883000109/
image: 101629696_1122103218123508_5456657130960453632_n.jpg
teaser: Das Leben regt sich wieder und das Ashtanga Yoga Institute öffnet seine
  Pforten ab 2. Juni.  Ich werde an 5 Samstagen und einem Freitag (3.7.) für bis
isCrawled: true
---
Das Leben regt sich wieder und das Ashtanga Yoga Institute öffnet seine Pforten ab 2. Juni. 
Ich werde an 5 Samstagen und einem Freitag (3.7.) für bis zu 18 Leute (soviele dürfen mit den Abstandsregeln ins AYI rein) Ecstatic Dance anbieten. Wir dürfen zwar noch nicht mit direktem körperlichen Kontakt tanzen, aber es gibt so viel an und mit uns selbst im Tanz zu entdecken, dass uns der Spaß und die Ideen nicht ausgehen werden und wir trotz Abstand eine gemeinsame Energie aufbauen werden, die uns weit trägt. Ich freu mich drauf, endlich wieder mit euch tanzen zu können.
Damit das Ganze stattfinden kann, muss Ecstatic Dance als Kurs mit fester Anmeldung angeboten werden. Ihr könnt auf der Webseite des AYI entweder den ganzen Kurs buchen oder ab 24 Std. vorher einen Einzeltermin als Dropin Stunde... (wer noch nie eine Dropin Stunde gebucht hat, bekommt die erste Stunde geschenkt!)
Wie es genau funktioniert bekommt ihr auf der AYI Webseite erklärt. 
https://de.ashtangayoga.info/ulm/
