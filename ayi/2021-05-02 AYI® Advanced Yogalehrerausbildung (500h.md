---
id: "2497281083819627"
title: AYI® Advanced Yogalehrerausbildung (500h AYA) bei Yogability
start: 2021-05-02 09:00
end: 2021-05-08 19:00
address: Yogability
link: https://www.facebook.com/events/2497281083819627/
image: 88056507_2826111910782219_6517461667356344320_o.jpg
teaser: I N F O R M A T I O N   Die Advanced Ausbildung ist die zweite Phase der
  AYInnovation® Yogalehrer-Ausbildung. Sie richtet sich exklusiv an AYI® Inspi
isCrawled: true
---

I N F O R M A T I O N 

Die Advanced Ausbildung ist die zweite Phase der AYInnovation® Yogalehrer-Ausbildung. Sie richtet sich exklusiv an AYI® Inspired Yogalehrer bzw. Yogalehrer mit einem erfolgreich abgeschlossenen AYI® Inspired Quereinstieg. Mit fundiertem Fachwissen begleitet Dich Svenja Wilke (AYI® Expert) über 1 Jahr auf Deinem persönlichen Yogaweg.

SCHWERPUNKTE
Die Advanced Ausbildung bietet Dir die Möglichkeit, Deine Kompetenzen als Yogalehrender zu erweitern und mit neuen Unterrichtstechniken (Traditional Form, 1. Serie, Präzision Stunde, Adjustments) vertraut zu werden. Gleichzeitig gewinnst Du einen umfassenden Einblick in Themen und Texte der Yoga-Philosophie (Yoga Sutra Kapitel 1 bis 2, Devanagari Schrift, Sanskrit Einblicke) und entwickelst eine gewachsene, persönliche Yogapraxis.

IN DER AYI® ADVANCED AUSBILDUNG...
– entwickelst Du Deine persönliche Übungspraxis weiter und festigst diese
– tauchst Du in die Welt des Pranayama und Meditation ein –  für die eigene Übungspraxis sowie für den Unterricht
– lernst Du systematisch die Basic- und Traditional-Formen der 1. Serie inklusive Vinyasa Count
– wirst Du Adjustments nach den verschiedenen Alignment-Prinzipien erarbeiten und in der Praxis festigen
– vertiefst Du Deine Unterrichtstechniken für die erste Serie (Präzisions- Themenstunden, Led-Class und Mysore Style)
– lernst Du, Kurse für Geübte zu konzipieren und darüber die AYI® Methode sicher zu vermitteln
– stärkst Du Deine Lehrerpersönlichkeit
– vertiefst Du Deine Kenntnisse über Bandhalign® – die Kunst der Ausrichtung des Körpers
– erweiterst Du Dein Portfolio der therapeutische Yogaübungen
– vertiefst Du Deine Kenntnisse des Yoga Sutra (1. und 2. Kapitel)
– wirst Du vertraut mit den Grundlagen des Sanskrit und Devanagari

UMFANG, DAUER, ORT, TEILNEHMER
Die Ausbildung umfasst 210 Unterrichtseinheiten. Zusätzlich zu den drei Intensivwochen ist die erfolgreiche Teilnahme an 6 MTCs (Modular Therapy Course) für den erfolgreichen Abschluss obligatorisch.

Die Ausbildung ist limitiert auf 18 Teilnehmer und findet in unserer Schule in Herdecke bei Dortmund statt.

TERMINE

Woche 1:
Sonntag 02.05.2021 bis
Samstag 08.05.2021
 
Woche 2:
Sonntag 05.09.2021 bis
Samstag 11.09.2021
 
Woche 3:
Sonntag 23.01.2022 bis
Samstag 29.01.2022

ERFOLGREICHER ABSCHLUSS
Das speziell von AYI® entwickelte E-Learning-Tool begleitet Dich während der Ausbildung. Über Deinen E-Learning-Bereich kannst Du die Inhalte der Intensivwochen sowie MTCs über verschiedene Lernmaterialien, Videos und Aufgaben vertiefen. Das web-basierte Tool ist obligatorischer Bestandteil der Ausbildung und MTC’s. Neben den Vertiefungsaufgaben sind zwei Vorstellstunden (Präzisionsstunde sowie Led-Class der ersten Serie) für den erfolgreichen Abschluss der Ausbildung vorgesehen.

SVENJA WILKE
Durch ihre Mutter, selber Yogalehrerin, kam Svenja schon früh mit Yoga in Kontakt. Den dynamischen Stil des Ashtanga Yoga hat sie viel später – im Jahr 2007 – in Sydney kennengelernt. Es war Liebe auf den ersten Atemzug: Die Dynamik, die Verbindung von Atem und Bewegung sowie die kraftvolle und gleichzeitig elegante Ausdrucksweise haben Svenja sofort in ihren Bann gezogen. Seitdem hat sie Ashtanga Yoga nicht mehr losgelassen. Ihre 4-jährige Ausbildung (2010-2014) zum Yogalehrer AYI®, BDY/EYU hat sie bei den beiden profilierten Yogalehrern Anna Trökes und  Dr. Ronald Steiner absolviert. In Ihrem Unterricht legt sie sehr viel Wert auf die korrekte Ausrichtung der Teilnehmer. Bei ihr bekommst Du Modifikationen an die Hand, mit denen Du gemäß Deinen Voraussetzungen und Deinem Können Yoga praktizieren kannst. Svenjas Wunsch ist es, anderen Menschen Ashtanga Yoga nach der Methode von Ashtanga Yoga Innovation (AYI®) näher zu bringen, um sie die positiven Wirkungen auf Körper, Geist und Seele selber erfahren zu lassen.  

K O S T E N 

Option 1: Einmalzahlung
AYI® Mitglied: 2.680,00 €
Nicht-Mitglied: 2.980,00 €
 
Der Einmalbetrag ist fällg innerhalb von zwei Wochen nach der Vertragsunterzeichnung. Die Ersparnis im Vergeich zur Ratenzahlung beträgt 170,00 €.

 
***
 
Option 2: Anzahlung + 12 monatliche Raten
AYI® Mitglied: 2.850,00 €
Nicht-Mitglied: 3.150,00 €
 
Ablauf:
– Anzahlung von EUR 990.00 (EUR 870.00 als AYI Mitglied) fällig mit Bewerbung und Vertragsunterzeichnung
– 12 Raten von EUR 180.00 € ( EUR 165.00 als AYI Mitglied) ab dem Monat des ersten Ausbildungswochenendes zum 1. jeden Monats.

Z U S A T Z K O S T E N

• 6 MTCs mit Ronald Steiner (siehe oben)
• Verpflegung und Unterkunft
• Ausbildungsliteratur (EUR 65)
• AYI Study Mitgliedschaft (EUR 228 jährlich / EUR 109 für Geringverdiener)
 
Wenn Du Fragen zu den Kosten, AYI Mitgliedschaft, MTCs oder Ablauf hast, rufe uns einfach an: 02330 8918776.
 
U N T E R K U N F T 

Wir arbeiten mit unserem Hospitality-Partner Zweibrücker Hof **** in Herdecke zusammen und helfen Dir gerne bei der Vermittlung von einem Zimmer zu unserer speziellen Yogability Rate. Sprich uns einfach an.

B E W E R B U N G

Bitte nutze das Online-Formular für den Eingabe Deiner Daten und den Upload Deiner Bewerbung. Alternativ kannst Du uns Deine Bewerbung auch per Email (ausbildung@yogability.de) oder auf dem Postweg zusenden oder im Studio abgeben.

https://www.yogability.de/ausbildung
 
Bitte inkludiere (D)einen kurzen Lebenslauf mit Bezug auf Yoga und beschreibe Deine Motivation und Zielsetzung für die Ausbildung. Keine Angst, es muss kein Roman sein, wir möchten Dich nur etwas besser kennenlernen. Wir benötigen außerdem das Zertifikat Deiner abgeschlossenen AYI® Inspired Ausbildung.
 
Für Rückfragen und weitere Informationen erreichst Du uns unter 02330.8918776 oder auch persönlich im Studio in der Mühlenstraße 9.
 
Wir freuen uns darauf, Dich kennenzulernen!
 
NOCH KEINEN AYI® INSPIRED ABSCHLUSS?

Eine erfolgreich abgeschlossene AYI® Inspired Ausbildung ist Voraussetzung für den Einstieg in Deine Advanced Ausbildung. Falls Du schon Yogalehrer bist (200h Ausbildung oder mehr), kannst Du bei uns den einwöchigen Quereinstieg zum AYI® Inspired Yogalehrer machen. Die nächsten Termine vor der Advanced Ausbildung sind:

19.-25.10.2020 in Berlin
05.-11.02.2021 in Herdecke

Infos und Bewerbung unter:
https://www.yogability.de/ausbildung

BEWERBUNG/ANMELDUNG

Alle Infos, den Infoflyer zum Download und das Bewerbertool findest du auf:
https://www.yogability.de/ausbildung/ayi-advanced-yogalehrer-ausbildung-500h-aya/

Fragen? Ruf uns an oder schreibe uns eine Email:
Tel: 02330.8918776
Email: ausbildung@yogability.de



