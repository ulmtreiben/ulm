---
id: "464945897698478"
title: MTC Alignment - Ausrichtung auf das Lebendige
start: 2020-09-12 08:00
end: 2020-09-13 18:30
address: Daily Ashtanga Yoga - Wiesbaden
link: https://www.facebook.com/events/464945897698478/
image: 105031520_1191212701224009_6585546242184240771_o.jpg
teaser: Über Alignment wird ein anatomisch korrektes Üben erreicht. Die Verbindung von
  Alignment und Bandhas in dem Prinzip Bandhalign® wird in der Theorie er
isCrawled: true
---
Über Alignment wird ein anatomisch korrektes Üben erreicht. Die Verbindung von Alignment und Bandhas in dem Prinzip Bandhalign® wird in der Theorie erläutert und in vielen praktischen Übungen umgesetzt. Anhand anatomischer Modelle werden interessante Besonderheiten der Anatomie in Bezug auf die körperliche Praxis demonstriert. Es bleibt Raum individuelle Probleme zu ergründen und in praktisch anzugehen. 

Ein Thema, das nicht nur für Yogalehrer neue Aspekte eröffnen kann.