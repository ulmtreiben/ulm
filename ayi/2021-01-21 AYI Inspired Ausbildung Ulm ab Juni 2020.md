---
id: "389097858645641"
title: AYI Inspired Ausbildung Ulm ab Juni 2020
start: 2021-01-21 06:00
end: 2021-01-21 18:00
locationName: AshtangaYoga.info
address: Frauenstr. 124, Ulm
link: https://www.facebook.com/events/389097858645641/
image: 72473655_3315717175120239_4463003497704456192_o.jpg
teaser: Hintergründer erfahren - die Basis für Yogalehrer  Die Inspired Ausbildung
  (AYI®) ist der Einstieg in die AYInnovation® Yogalehrerausbildung. Sie rich
isCrawled: true
---
Hintergründer erfahren - die Basis für Yogalehrer

Die Inspired Ausbildung (AYI®) ist der Einstieg in die AYInnovation® Yogalehrerausbildung. Sie richtet sich an Yogaübende, die ihr Wissen vertiefen wollen. Mit fundiertem Fachwissen begleiten Dich Anna Trökes, Melanie Steiner und Ronald Steiner über ein Jahr auf Deinem persönlichen Yogaweg.

Womit beschäftigen wir uns?
- Was ist das genau, Yoga? Welche Philosophie steht hinter dem Ganzen? Wie hat sich Yoga über die letzten 2.000 Jahre entwickelt? 
- Wer hat sich den Ashtanga Yoga "ausgedacht"?
- Wie ist die 1. Serie des Ashtanga Yoga aufgebaut? Was bedeutet der Count? Wie heißen die einzelnen Asanas (Yogapositionen)? Und vieles mehr rund um die erste Serie, Yoga Chikitsa ( = Yoga-Therapie)
- Alignment  - Wie kann ich meinen Körper besser fühlen und ausrichten? Wie übe ich für mich selbst die erste Serie?
- Unterricht: plötzlich kommt da ein Anfänger... Wie fange ich an zu unterrichten? Was hat es mit der sogenannten "Basic Form" auf sich und wie vermittele ich diese?

Die Körper-Praxis ist ja schön und gut, aber gibt es da nicht noch mehr?
- Was verstehen wir unter Yogameditation? Wie wirkt Yoga auf unseren Geist?
- Atemübungen: Wie praktiziere ich Pranayama?
- UNS SELBST: Wer bin ich? Warum verhalte ich mich so? Welche Qualität haben meine Beziehungen? Wir entdecken gemeinsam die Antworten des Yoga und der modernen Psychologie auf diese Fragen....

Termine:
8.-13.06.20
(10.-12.6. mit Anna Trökes)
25./26.07.20
19.-20.09.20 
09.-14.11.20 
(09.-11.11. mit Anna Trökes)
18.-23.01.21
27./28.02.2021

Uhrzeiten:
Werden rechtzeitig bekannt gegebene. Ganztägig. 

Kosten:
Anzahlung 990 € (AYI Mitglied 891€)
Restzahlung 1990€ (AYI Mitglied 1790€)

Lehrer:
Dr. Melanie Steiner, Dr. Ronald Steiner, Anna Trökes
