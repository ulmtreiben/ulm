---
id: "561190651301482"
title: "Andreas Lutz: MTC Atem und Bandha - Alignment von Prana"
start: 2021-06-05 08:30
end: 2021-06-06 17:30
address: Yogability
link: https://www.facebook.com/events/561190651301482/
image: 120557322_3391180227608715_3372848024803258542_o.jpg
teaser: I N F O R M A T I O N   &   T H E M A  ATEM UND BANDHA - ALIGNMENT VON
  PRANA   Die Verbindung von Alignment und Bandha wird in der AYI Methode als
  Ban
isCrawled: true
---
I N F O R M A T I O N   &   T H E M A

ATEM UND BANDHA - ALIGNMENT VON PRANA
 
Die Verbindung von Alignment und Bandha wird in der AYI Methode als Bandhalign® bezeichnet.  Die bewußte körperliche Ausrichtung zusammen mit einer geführten Atmung kann deine Praxis in ungeahnte Tiefen führen. Schon in den alten Texten (wie z.B. der Hatha Yoga Pradipika) werden die Prinzipien von Mula-, Uddiyana- und Jalandhara-Bandha beschrieben. Die genaue Umsetzung bleibt dabei eher unklar. In diesem MTC lernst Du die Muskulatur der Atmung kennen und erspürst den Zusammenhang zwischen einer balancierten Körperhaltung und freier Atmung. Somit entsteht ein modernes Verständnis der alten Techniken in Theorie und Praxis. Diese  bewegt sich zwischen starker Verwurzelung und ungeahnter Leichtigkeit.

----

FÜR WEN IST EIN MODULAR THERAPY COURSE (MTC) INTERESSANT?

Der Modular Therapy Course (MTC) richtet sich gezielt an Yogalehrer, auch anderer Tradition, und interessiert Übende. Du tauchst ein in die anatomischen und physiologischen Hintergründe Deiner Yoga Praxis. Wenn Du bereits eine eigene Yogapraxis, egal in welcher Tradition, etabliert hast und Dich Alignment, Prävention und Yogatherapie interessieren, dann bist Du hier genau richtig.Im Modular Therapy Course (MTC) erlernst Du therapeutische Sequenzen und Bandhlign® Techniken für eine gesunde Ausrichtung in Deiner Yogapraxis. Beides kannst Du nach der Weiterbildung direkt in Deine eigene Yogapraxis und Deinen Unterricht integrieren. Im Modular Therapy Course (MTC) erfährst Du die anatomischen Hintergründe und erlangst ein Verständnis über häufige Verletzungen und Erkrankungen. Zusätzlich erhältst Du ein Handbuch, in dem Du die wesentlichen Inhalte nachlesen kannst.Neben dem Kontaktunterricht hast Du in den dem Modular Therapy Course (MTC) folgenden zwei Monaten die Möglichkeit im AYI® Learning die erlernten Techniken zu vertiefen und für die persönliche Praxis und den Unterricht nutzbar zu machen. Nach Deiner erfolgreichen Teilnahme erhältst Du ein Zertifikat. Dies bestätigt Dir die Inhalte wirklich praktisch anwenden zu können. So kann das MTC ein Modul Deiner AYInnovation® Yogalehrer Ausbildung oder Weiterbildung werden.
 
----

ANDREAS LUTZ

Seit über 30 Jahren ist Andreas als Masseur, (Sport-)-Physiotherapeut, Manualtherapeut, Heilpraktiker, Rolfer und Shiatsu-Praktiker in eigener Praxis tätig. Er setzt sein medizinisch fundiertes Wissen und seine Erfahrung ein, um über das Alignment (die physiologische Ausrichtung) ein achtsames und sorgfältiges Üben zu ermöglichen. Solltest du Einschränkungen oder Schwierigkeiten haben ist er immer in der Lage therapeutisch lösende Techniken einzubringen, die es dir ermöglichen, modifiziert weiter zu üben. Sein persönlicher Favorit wurde über die Jahre auch der Schwerpunkt in seinem Unterricht: der Atem. Atmung in Bewegung (Vinyasa, Bandha) und besonders die Pranayamas sind ihm als die Essenz der Übung in Fleisch und Blut übergegangen und erzeugen eine ungeahnte Tiefe in seiner Lehrtätigkeit.


----

V O R A U S S E T Z U N G E N

Der Workshop / MTC richtet sich gezielt and Yogalehrer (auch anderer Tradition) und interessiert Übende.

----

O R T

Der MTC findet in der Turn- und Gymnastikhalle Kalkheck in Herdecke Kirchende statt. Die Halle hat große Fenster, viel Licht und Holz – und einen modernen Schwingboden. Zur Ausstattung gehören auch moderne Umkleiden und Duschen sowie ausreichend kostenlose Parkplätze direkt vor der Tür.  Bei einer Größe von nur 15x15m ist sie ideal für unsere Workshops und Weiterbildungen geeignet. 

Adresse:
Im Kalkheck, Herdecke-Kirchende

----

P R O G R A M M 

SA. 5. JUNI 2021
08:30 Uhr – 18:30 Uhr
Theorie und Praxis
 
SO. 6. JUNI 2021
07:30 Uhr – 17:30 Uhr
Theorie und Praxis
 
 ----

P R E I S  

AYI Mitglieder: 265,00 €
Nicht-Mitglieder: 295,00 €
 
Inkl. Snacks, Wasser und Tee an allen Tagen.

Übernachtungen können auf Wunsch dazu gebucht werden, wir verfügen über attraktive Deals mit Hotels in der nahen Umgebung, teils fußläufig. Fragt uns einfach!

----

A N M E L D U N G

Online: https://www.yogability.de/workshops/andreas-lutz-mtc-atem-und-bandha/

Telefon: 02330.8918776
...oder im Studio.

Download iOS: https://goo.gl/rAbzEI
Download Android: https://goo.gl/2cQ0EF