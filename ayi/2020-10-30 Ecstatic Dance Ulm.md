---
id: "2909597825928006"
title: Ecstatic Dance Ulm
start: 2020-10-30 19:30
end: 2020-10-30 22:00
locationName: AshtangaYoga.info
address: Frauenstr. 124, Ulm
link: https://www.facebook.com/events/2909597825928006/
image: 118856326_1195728107427685_1122879474371787401_n.jpg
teaser: Ecstatic Dance im Ashtanga Yoga Institute Ulm geht in die zweite Runde. Alle
  zwei Wochen am Freitag Abend ab 18. September gibt es wieder die Möglichk
isCrawled: true
---
Ecstatic Dance im Ashtanga Yoga Institute Ulm geht in die zweite Runde. Alle zwei Wochen am Freitag Abend ab 18. September gibt es wieder die Möglichkeit für bis zu 18 Tänzer*innen sich bei Ecstatic Dance auszuprobieren, auszutoben und zur Wave von DJ Klaus zu tanzen...
Du kannst dich gerne zum ganzen Kurs oder aber auch für einzelne Termine anmelden.
Mehr Infos zum Ecstatic Dance gibt es auf der Kursseite: https://de.ashtangayoga.info/ulm/200901-ulm-ecstatic-shiva-dance-klaus/