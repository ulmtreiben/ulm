---
name: VVN-BdA
website: http://bawue.vvn-bda.de
email: 
scrape:
  source: facebook
  options:
    page_id: VVN.BdA.BW
  filter:
    address: Ulm
---
Die VVN-BdA ist eine überparteiliche antifaschistische Vereinigung. Sie wurde von Überlebenden aus den Konzentrationslagern und Zuchthäusern gegründet.
