---
id: "720933061873149"
title: Klangreise
start: 2020-12-31 16:00
link: https://www.facebook.com/events/720933061873149/
image: 132349076_1287076408292854_3512406837512912382_o.jpg
teaser: Eingebettet in die beruhigenden Sounds meines elektronischen Klangteppichs,
  kann bei dieser Klangreise Dein Körper loslassen und entspannen. Dein Geis
isCrawled: true
---
Eingebettet in die beruhigenden Sounds meines elektronischen Klangteppichs, kann bei dieser Klangreise Dein Körper loslassen und entspannen. Dein Geist findet angenehm zu tiefer Ruhe. Lass vollständig los, lass Dich auf den meditativen Klängen treiben und tauche schließlich erfrischt und gestärkt aus diesem Klangbad wieder auf.

Alles was Du dafür brauchst, ist eine Internetverbindung mit Kopfhörer oder Lautsprecher, eine Stunde Ruhe und einen gemütlichen Ort. 

Besuche die Klangreise für Dich alleine oder auch mit Deinen Herzensmenschen. Verbinde Dich mit Menschen von nah und fern für eine gemeinsame meditative Stunde. 

Der Link zum Audiostream ist auf der Veranstaltungsseite des AYI (Ashtanga Yoga Institute) zu finden. Es geht pünktlich los...
