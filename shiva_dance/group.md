---
name: Shiva Dance
website: https://www.facebook.com/tanzBarJederVernunft
email: k.fumy@gmx.de
scrape:
  source: facebook
  options:
    page_id: tanzBarJederVernunft
---
Einmal im Monat barfuß tanzen, chillen und sich zur Elektromuke von DJ tanzBarJederVernunft (aka Klaus) treiben lassen. Und das in Ulm!
