---
id: "685529018969347"
title: ROXY Sound Garten mit DJ Klaus Fumy (Shiva Dance)
start: 2020-07-30 18:00
end: 2020-07-31 22:00
address: ROXY.ulm
link: https://www.facebook.com/events/685529018969347/
teaser: Wir freuen uns, euch in unserem Sound Garten begrüßen zu dürfen. Neben dem
  regulären Biergartenbetrieb versüßen euch unterschiedliche DJs die Ohren.
isCrawled: true
---
Wir freuen uns, euch in unserem Sound Garten begrüßen zu dürfen. Neben dem regulären Biergartenbetrieb versüßen euch unterschiedliche DJs die Ohren.

HEUTE (ab 18:00): DJ Klaus Fumy (Shiva Dance)
DJ Klaus beschallt euch mit Ethnogrooves, Downbeat, Afrohouse und Unerwartetem, damit trotz Tanzverbot, die Füße wippen, der Popo wackelt und der Kopf frei wird.

Eintritt frei, Spende für die musikalische Unterhaltung wird gern entgegen genommen.

Der Biergarten ist ab 15:00 H geöffnet.

***
Alle aktuellen Bestimmungen bzgl. Corona findet ihr auf unserer Homepage www.roxy.ulm.de