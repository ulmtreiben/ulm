---
id: "2610838809224760"
title: Silent Ecstatic Shiva Dance
start: 2021-08-21 13:00
end: 2021-08-21 15:30
address: AYI Yogaraum
link: https://www.facebook.com/events/2610838809224760/
image: 237125020_1452542478412912_8082178962182365929_n.jpg
isCrawled: true
---
Silent Ecstatic Shiva Dance, 
Austoben, entfalten, auf eine Reise gehen und ganz bei Dir selbst ankommen. Dich treiben lassen, im Flow bewegen, Gefühle tanzen, … ohne Schuhe, aber dafür mit Kontakt zum Boden. Ohne Alkohol oder Drogen, dafür mit Dir selbst, ohne zu reden -  wie es sich eben richtig anfühlt. Ganz für Dich selbst, aber mit der Energie der Gruppe. All das in einem geschützten Raum, nur für Dich, ohne Beobachtung oder Bewertung. 

Du brauchst keine Vorerfahrung. Ecstatic Dance ist für Jede*n, egal ob Du erfahren auf dem Dancefloor bist oder Dich zum ersten Mal außerhalb Deiner Komfortzone bewegen möchtest. Bei uns bekommst Du einen wundervollen Rahmen und genug Raum, um Dich zwanglos auszuprobieren. Und Du wirst feststellen: Es tut so gut!

Was erwartet Dich?
Ein wunderbarer sakraler Raum und Kopfhörer für dein individuell abgestimmtes Klangerlebnis. Los geht's mit einem Warm Up, dann treffen wir uns im Opening Circle für Erklärungen und eine Einstimmung ins Thema, tanzen eine oder mehrere Waves zur Musik von DJ Klaus (Shiva Dance). Wir enden mit einem gemeinsamen Klangbad, einer Meditation oder Shavasana und treffen uns noch einmal im Closing Circle, bevor wir wieder auseinander gehen. 

Bitte komme ein wenig früher, damit wir gemeinsam pünktlich starten können. 

Was kannst Du mitbringen?
Neugierde und bequeme Klamotten in denen du Dich wohl fühlst. Bring Dir gerne etwas zu trinken (kein Alkohol) und ein Handtuch und evtl. eine Decke mit.

Anmeldungen sind über die AYI Yogaseite möglich. 
https://de.ashtangayoga.info/210807-ulm-ulm-ecstatic-shiva-dance-klaus/ Wegen Corona ist die Teilnehmer*innenzahl begrenzt...