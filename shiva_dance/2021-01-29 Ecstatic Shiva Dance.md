---
id: "2881766778810093"
title: Ecstatic Shiva Dance
start: 2021-01-29 19:30
end: 2021-01-29 22:00
link: https://www.facebook.com/events/2881766778810093/
image: 141474030_1307079886292506_5933724204099606709_n.jpg
teaser: Shiva Dance,  Nervennahrung für Bewegungshungrige,   kommt zu dir per Stream
  nach Hause. :-) Ich freue mich auf dich. Der Link oben funktioniert erst
isCrawled: true
---
Shiva Dance, 
Nervennahrung für Bewegungshungrige,  
kommt zu dir per Stream nach Hause. :-) Ich freue mich auf dich.
Der Link oben funktioniert erst ab kurz vor der Veranstaltung...
Wenn du Shiva Dance Online unterstützen und/oder einen Energieausgleich leisten möchtest, freue ich mich über deine Spende auf mein paypal Konto:
https://www.paypal.com/donate?hosted_button_id=GWS3XAK3FF8FL
