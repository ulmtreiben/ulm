---
id: "1309638669392113"
title: Klangreise zum 1. Vollmond des Jahres
start: 2021-01-28 19:30
end: 2021-01-28 20:30
link: https://www.facebook.com/events/1309638669392113/
image: 141553302_1307632032903958_2792806800123559663_o.jpg
teaser: Eingebettet in die beruhigenden Sounds meines elektronischen Klangteppichs,
  kann bei dieser Klangreise Dein Körper loslassen und entspannen. Dein Geis
isCrawled: true
---
Eingebettet in die beruhigenden Sounds meines elektronischen Klangteppichs, kann bei dieser Klangreise Dein Körper loslassen und entspannen. Dein Geist findet angenehm zu tiefer Ruhe. Lass vollständig los, lass Dich auf den meditativen Klängen treiben und vom Vollmondlicht bescheinen (wenn wir Glück haben), tauche schließlich erfrischt und gestärkt aus diesem Klangbad wieder auf.

Alles was Du dafür brauchst, ist eine Internetverbindung mit Kopfhörer oder Lautsprecher, eine Stunde Ruhe und einen gemütlichen Ort. 

Besuche die Klangreise für Dich alleine oder auch mit Deinen Herzensmenschen. Verbinde Dich mit Menschen von nah und fern für eine gemeinsame meditative Stunde. 

Den Link zum Audiostream findest Du oben. Es geht pünktlich los...

Wenn du das Shiva Dance Online Radio unterstützen und/oder einen Energieausgleich leisten möchtest, freue ich mich über deine Spende auf mein paypal Konto:
https://www.paypal.com/donate?hosted_button_id=GWS3XAK3FF8FL
