---
id: "848862622578114"
title: Ecstatic Shiva Dance
start: 2021-01-01 19:30
end: 2021-01-01 22:00
link: https://www.facebook.com/events/848862622578114/
image: 132299165_1286961074971054_3496610386363964999_n.jpg
teaser: Shiva Dance kommt auch im Neuen Jahr zu dir per Stream nach Hause. Wer
  teilnehmen möchte, schreibt mir einfach eine kurze Nachricht in den
  Kommentaren
isCrawled: true
---
Shiva Dance kommt auch im Neuen Jahr zu dir per Stream nach Hause. Wer teilnehmen möchte, schreibt mir einfach eine kurze Nachricht in den Kommentaren oder an k.fumy@gmx.de. Dann bekommst du den Link zum Radio Live Stream. 
Wenn du Shiva Dance Online unterstützen und/oder einen Energieausgleich leisten möchtest, freue ich mich über deine Spende auf mein paypal Konto:
https://www.paypal.com/donate?hosted_button_id=GWS3XAK3FF8FL
