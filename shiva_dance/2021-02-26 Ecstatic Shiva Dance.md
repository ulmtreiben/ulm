---
id: "463847717983982"
title: Ecstatic Shiva Dance
start: 2021-02-26 19:30
end: 2021-02-26 22:00
link: https://www.facebook.com/events/463847717983982/
image: 152391958_1325992774401217_2931071240350803356_n.jpg
teaser: "Shiva Dance,  Nervennahrung für Bewegungshungrige,   kommt zu dir per Stream
  nach Hause. :-) Ich freue mich auf dich.  Hier der Link zum Radiostream:"
isCrawled: true
---
Shiva Dance, 
Nervennahrung für Bewegungshungrige,  
kommt zu dir per Stream nach Hause. :-) Ich freue mich auf dich.

Hier der Link zum Radiostream:
http://dj.media.uni-ulm.de:8000/Shiva
 (der Link funktioniert erst kurz vor der Veranstaltung)
...
Wenn du Shiva Dance Online unterstützen und/oder einen Energieausgleich leisten möchtest, freue ich mich über deine Spende auf mein paypal Konto:
https://www.paypal.com/donate?hosted_button_id=GWS3XAK3FF8FL
