---
id: "4233744683305778"
title: Ecstatic Shiva Dance
start: 2021-02-12 19:30
end: 2021-02-12 22:00
link: https://www.facebook.com/events/4233744683305778/
image: 146756880_1316816528652175_8560036691667363628_n.jpg
teaser: Shiva Dance,  Nervennahrung für Bewegungshungrige,   kommt zu dir per Stream
  nach Hause. :-) Ich freue mich auf dich.  http://dj.media.uni-ulm.de:8000
isCrawled: true
---
Shiva Dance, 
Nervennahrung für Bewegungshungrige,  
kommt zu dir per Stream nach Hause. :-) Ich freue mich auf dich.

http://dj.media.uni-ulm.de:8000/Shiva

Der Link funktioniert erst kurz vor der Veranstaltung, wenn ich den Stream gestartet habe
...
Wenn du Shiva Dance Online unterstützen und/oder einen Energieausgleich leisten möchtest, freue ich mich über deine Spende auf mein paypal Konto:
https://www.paypal.com/donate?hosted_button_id=GWS3XAK3FF8FL
