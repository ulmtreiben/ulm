---
id: "398904234669426"
title: Ecstatic Shiva Dance
start: 2021-01-15 19:30
end: 2021-01-15 22:00
link: https://www.facebook.com/events/398904234669426/
image: 135751093_1298628590470969_6177736353555211645_n.jpg
teaser: Shiva Dance,  Nervennahrung für Bewegungshungrige,   kommt zu dir per Stream
  nach Hause. :-) Wer teilnehmen möchte, schreibt mir einfach eine kurze Na
isCrawled: true
---
Shiva Dance, 
Nervennahrung für Bewegungshungrige,  
kommt zu dir per Stream nach Hause. :-) Wer teilnehmen möchte, schreibt mir einfach eine kurze Nachricht in den Kommentaren oder an k.fumy@gmx.de. Dann bekommst du den Link zum Radio Live Stream. 
Wenn du Shiva Dance Online unterstützen und/oder einen Energieausgleich leisten möchtest, freue ich mich über deine Spende auf mein paypal Konto:
https://www.paypal.com/donate?hosted_button_id=GWS3XAK3FF8FL
