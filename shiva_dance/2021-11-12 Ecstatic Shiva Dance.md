---
id: "6194322523975032"
title: Ecstatic Shiva Dance
start: 2021-11-12 20:00
end: 2021-11-12 22:30
locationName: Yoga Rauhenstein
address: Rauhenstein, 87487 Wiggensbach
link: https://www.facebook.com/events/6194322523975032/
image: 243278697_1482544125412747_718503922202267694_n.jpg
isCrawled: true
---
Shiva Dance (DJ Klaus, Ulm) kommt ins Allgäu, mit seinem Mix aus Downtempo, Ethnotechno, Minimal, ..., 
Im wunderschönen Ambiente von Yoga Rauhenstein starten wir gemeinsam zu einer tänzerischen Reise zu dir selbst: erlebe eine musikalische Welle von der Sammlung zur Ecstase und zurück zur Stille. Komm also pünktlich...:-), es lohnt sich.
Es gelten die aktuellen Coronaregeln.
(auf Spendenbasis)