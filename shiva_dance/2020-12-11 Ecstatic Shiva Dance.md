---
id: "383394346225048"
title: Ecstatic Shiva Dance
start: 2020-12-11 19:30
end: 2020-12-11 22:00
link: https://www.facebook.com/events/383394346225048/
image: 128401485_1271168636550298_2795592884507149675_n.jpg
teaser: Shiva Dance kommt auch diesen Freitag zu dir per Stream nach Hause. Wer
  teilnehmen möchte, schreibt mir einfach eine kurze Nachricht in den Kommentare
isCrawled: true
---
Shiva Dance kommt auch diesen Freitag zu dir per Stream nach Hause. Wer teilnehmen möchte, schreibt mir einfach eine kurze Nachricht in den Kommentaren oder an k.fumy@gmx.de. Dann bekommst du den Link zum Radio Live Stream. 
Diesmal bin ich nicht alleine, sondern werde zusammen mit DJ Hathora auflegen. Das wird ein DJ Fest. Ich freu mich sehr darauf. Und natürlich freu ich mich auch, wenn du zuhause mittanzt. 

