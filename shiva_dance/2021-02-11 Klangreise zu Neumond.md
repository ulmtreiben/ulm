---
id: "172106801052594"
title: Klangreise zu Neumond
start: 2021-02-11 19:30
end: 2021-02-11 20:30
link: https://www.facebook.com/events/172106801052594/
image: 148069494_1318159655184529_2618501782432779191_o.jpg
teaser: Eingebettet in die beruhigenden Sounds meines elektronischen Klangteppichs,
  kann bei dieser Klangreise Dein Körper loslassen und entspannen. Dein Geis
isCrawled: true
---
Eingebettet in die beruhigenden Sounds meines elektronischen Klangteppichs, kann bei dieser Klangreise Dein Körper loslassen und entspannen. Dein Geist findet angenehm zu tiefer Ruhe. Lass vollständig los, lass Dich auf den meditativen Klängen treiben und tauche schließlich erfrischt und gestärkt aus diesem Klangbad wieder auf.

Alles was Du dafür brauchst, ist eine Internetverbindung mit Kopfhörer oder Lautsprecher, eine Stunde Ruhe und einen gemütlichen Ort. 

Besuche die Klangreise für Dich alleine oder auch mit Deinen Herzensmenschen. Verbinde Dich mit Menschen von nah und fern für eine gemeinsame meditative Stunde. 

Der Link zum Audiostream: 
http://dj.media.uni-ulm.de:8000/Shiva
Er funktioinert ca. ab 19:15 Uhr... 
Es geht pünktlich um 19:30 Uhr los...
