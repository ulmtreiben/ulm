---
name: Fischerins Kleid
website: http://www.fischerins-kleid.de
email: info@fischerins-kleid.de
scrape:
  source: facebook
  options:
    page_id: fischerins.kleid.seemanns.garn
---
Mode aus fairer und nachhaltiger Herstellung. Anfertigungen, Reparaturen & Änderungen.
ArmedAngels, bleed, eyd-clothing, Lovjoi, recolution, u.a.
Rock around my neck bijoux
