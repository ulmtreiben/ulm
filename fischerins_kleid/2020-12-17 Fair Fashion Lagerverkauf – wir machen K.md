---
id: "289269022057553"
title: Fair Fashion Lagerverkauf – wir machen Klarschiff!
start: 2020-12-17 10:00
end: 2020-12-17 15:00
address: Fischerins Kleid
link: https://www.facebook.com/events/289269022057553/
image: 122016600_1565542390294304_6730522550996297257_o.jpg
teaser: Vom 23.10. bis 23.12.2020 verkaufen wir Faires aus den letzten Wintern.
  Gegenüber von FischerinsKleid in der Fischergasse 1 immer Mi-Fr von 14-18 und
isCrawled: true
---
Vom 23.10. bis 23.12.2020 verkaufen wir Faires aus den letzten Wintern.
Gegenüber von FischerinsKleid in der Fischergasse 1 immer Mi-Fr von 14-18 und Sa von 10-15 (im Dez bis 16 Uhr).
Es gibt gemütliche Pullis und Strickjacken, Longsleeves und T-Shirts, Hosen, Mützen, Schals und vieles mehr zu reduzierten Preisen. Selbstverständlich unter Einhaltung der Hygiene- und Anstandsregeln.
Wir freuen uns auf Ihren Besuch!