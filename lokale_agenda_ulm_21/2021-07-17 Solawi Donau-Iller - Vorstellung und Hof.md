---
id: "4123392884441592"
title: Solawi Donau-Iller - Vorstellung und Hofbesuch
start: 2021-07-17 15:00
end: 2021-07-17 17:00
address: Agenda-Büro Ulm
link: https://www.facebook.com/events/4123392884441592/
image: 211789362_3920982328010927_289743509656167931_n.jpg
isCrawled: true
---
Vorstellung der Solawi Donau-Iller und Hofbesichtigung.

Der genaue Ort wird bei Anmeldung bekannt gegeben!

Solidarische Landwirtschaft ist eine innovative Strategie für eine lebendige, verantwortungsvolle Landwirtschaft, die gleichzeitig die Existenz der Menschen, die dort arbeiten, sicherstellt und einen essenziellen Beitrag zu einer nachhaltigen Entwicklung leistet.
Lerne das Konzept und die Menschen der Solawi Donau-Iller kennen!

Anmeldung im Agenda-Büro Ulm: agendabuero@ulm.de
