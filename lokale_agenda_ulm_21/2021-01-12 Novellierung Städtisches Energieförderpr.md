---
id: "391257925309162"
title: Novellierung Städtisches Energieförderprogramm
start: 2021-01-12 19:30
link: https://www.facebook.com/events/391257925309162/
image: 135454182_3409400152502483_244187636255849052_o.jpg
teaser: Holger Kissner | Klimaschutzmanager der Stadt Ulm  Ein Beitrag zum
  Online-Programm "17 Ziele für Ulm und die Welt"  Seit 30 Jahren fördert die
  Stadt U
isCrawled: true
---
Holger Kissner | Klimaschutzmanager der Stadt Ulm

Ein Beitrag zum Online-Programm "17 Ziele für Ulm und die Welt"

Seit 30 Jahren fördert die Stadt Ulm mit einem Energieförderprogramm Maßnahmen zur Energieeinsparung, zur rationellen Energieanwendung und zur Nutzung erneuerbarer Energien mit einem Volumen knapp 7.3 Mio. €. Da sich Förderungen des Landes und des Bundes regelmäßig ändern, passt sich das Ulmer Förderprogramm an die aktuellen Rahmenbedingungen an. Im Vortrag werden alle Fördermaßnahmen vorgestellt und die wichtigsten Änderungen erläutert. Schwerpunkte bilden die Bauleitplanung, nachwachsende Rohstoffe, die Umstellung eines Ölheizkessels und die Installation von Photovoltaik.

Link zum Vortrag:
https://bbb.ulm.dev/b/energiefoerderprogramm
Konferenzraum: Energieförderprogramm