---
id: "377584497274169"
title: Klimaschutz hier und jetzt
start: 2021-09-08 18:30
end: 2021-09-08 21:00
locationName: Stadthaus Ulm
address: Ulm
link: https://www.facebook.com/events/377584497274169/
image: 241123119_4079304392178719_2080019620397735325_n.jpg
isCrawled: true
---
Was erwarten wir von den politischen Parteien für den Klimaschutz in der Region? Diskussion mit den Kandidierenden zur Bundestagswahl.

Teilnehmende: Ronja Kemmer MdB (CDU), Marcel Emmerich MdB (Grüne), Jan Rothenbacher (SPD), David Rizzotto (Linke), Anke Hillmann-Richter (FDP) in Vertretung von Alexander Kulitz MdB und Daniel Wagner (Klimaliste)

Impulsvorträge: Prof. Michael Kühl, Scientists for Future; Fritz Mielert, Referent für Umweltschutz beim BUND Landesverband Baden-Württemberg

Moderation: Dana Hoffmann 

Zu den unterschiedlichen Themen haben wir weitere Fachleute eingeladen.
Der Eintritt ist frei. Es ist keine Anmeldung nötig. Wir richten uns am Veranstaltungstag nach den aktuellsten Corona-Bestimmungen (2G- oder 3G-Regel) der Stadt Ulm.  

