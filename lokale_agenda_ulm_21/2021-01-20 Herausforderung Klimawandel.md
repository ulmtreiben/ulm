---
id: "184096303453679"
title: Herausforderung Klimawandel
start: 2021-01-20 19:00
link: https://www.facebook.com/events/184096303453679/
image: 140366108_3448139838628514_2995581398844906721_o.jpg
teaser: Prof. Dr. Michael Kühl und PD Dr. Susanne Kühl | Scientists for Future  Im
  Rahmen des Online-Programms "17 Ziele für Ulm und die Welt"  Der Vortrag ge
isCrawled: true
---
Prof. Dr. Michael Kühl und PD Dr. Susanne Kühl | Scientists for Future

Im Rahmen des Online-Programms "17 Ziele für Ulm und die Welt"

Der Vortrag geht auf die Ursachen sowie die Folgen des Klimawandels ein und beleuchtet zugleich die Frage, was getan werden muss, um die Ziele des Pariser Klimaschutzabkommens noch einzuhalten. Zudem diskutieren wir, welche Konsequenzen unser Lebenswandel auf das Klima hat und zeigen Wege auf, was jeder auf persönlicher Ebene für den Klimaschutz tun kann.