---
id: "1066843197094200"
title: Sustainable Inclusion
start: 2020-12-17 18:30
link: https://www.facebook.com/events/1066843197094200/
image: 131020966_3520642181390784_5052026849833014672_o.jpg
teaser: Sustainability for all people! This workshop aims to bring the inclusion
  matter under the spotlight and investigates the participation of the marginal
isCrawled: true
---
Sustainability for all people! This workshop aims to bring the inclusion matter under the spotlight and investigates the participation of the marginalized groups in the scope of sustainability. We would like to provide the facts to answer these following questions;
 - What is done and planned to include more people from different communities to sustainability goals?
 - How through the inclusion people can be empowered?
 - What does inclusion mean in terms of sustainability?


https://uni-ulm.webex.com/uni-ulm-de/j.php?MTID=m70e277eed1dd3fb97308be106860b995

Passwort: zCHSuPhf464

Die Veranstaltung ist Teil der Ausstellung "17 Ziele für Ulm und die Welt".
In der Ausstellung „17 Ziele für Ulm und die Welt“ geben Ulmer Organisationen und Gruppen auf anschauliche Weise Antworten, präsentieren sich und ihre Aktivitäten. Zeitgleich ist die Wanderausstellung „Konsum-Kompass“ der Deutschen Bundesstiftung Umwelt zu sehen. Denn wer existiert, der konsumiert, und der Konsumkompass zeigt, worauf wir als Verbraucherinnen und Verbraucher achten können.
Mehr dazu auf: https://www.ideenwerkstatt-ulm.de/
Der Konsumkompass wurde von der Deutschen Bundesstiftung Umwelt und dem Umweltbundesamt erstellt und ist im Besitz des AK Eine Welt Nordhorn. 

