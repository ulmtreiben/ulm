---
id: "165030595406860"
title: Klimasimulation
start: 2021-01-21 19:00
link: https://www.facebook.com/events/165030595406860/
image: 140362094_3448330985276066_222374610224651284_o.jpg
teaser: Prof. Dr. Michael Kühl | Scientists for Future  Im Rahmen des Online-Programms
  "17 Ziele für Ulm und die Welt"  Wie entwickelt sich die globale Temper
isCrawled: true
---
Prof. Dr. Michael Kühl | Scientists for Future

Im Rahmen des Online-Programms "17 Ziele für Ulm und die Welt"

Wie entwickelt sich die globale Temperatur bis zum Jahre 2100? Und welchen Einfluss haben verschiedene Maßnahmen auf die globale Erwärmung, die wir ergreifen könnten, z.B. ein CO2 Preis einführen, die Förderung der erneuerbaren Energien oder der E-Mobilität, das Pflanzen von Bäume oder das Fortführen der Atomkraft? Welchen Einfluss hat das globale Bevölkerungswachstum? Diese und ähnliche Fragen diskutieren und bewerten wir mit Hilfe einer Computersimulation. Wir verwenden dazu das Simulationsprogramm En-Roads, das vom Massachusetts Institute of Technology (MIT) und Climate Interactive, beide USA, entwickelt wurde. Michael Kühl ist zertifizierter Klimabotschafter für Climate Interactive.