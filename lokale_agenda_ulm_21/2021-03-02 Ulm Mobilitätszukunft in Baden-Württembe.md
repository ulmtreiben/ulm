---
id: "183133773570228"
title: "Ulm: Mobilitätszukunft in Baden-Württemberg"
start: 2021-03-02 18:00
link: https://www.facebook.com/events/183133773570228/
image: 151594929_3841793425879389_5150634857878894480_o.jpg
teaser: Für die Vertreter*innen von VCD, ADFC, BUND, unw e.V., AK Mobilität der LA21
  Ulm, Fridays for Future und Haus der Nachhaltigkeit HdN Ulm/Neu-Ulm, spie
isCrawled: true
---
Für die Vertreter*innen von VCD, ADFC, BUND, unw e.V., AK Mobilität der LA21 Ulm, Fridays for Future und Haus der Nachhaltigkeit HdN Ulm/Neu-Ulm, spielt das Thema Mobilitätswende vor dem Hintergrund steigender CO2-Emissionen des Verkehrssektors und den mittlerweile spürbaren Veränderungen des Klimawandels eine zentrale Rolle für die Entscheidungen des nächsten Landtags bzw. der nächsten Landesregierung. Gemeinsam mit der Allianz „Mobilitätswende für Baden-Württemberg“ lädt das Bündnis zur virtuellen Gesprächsrunde mit den Landtagskandidat*innen:

Michael Joukov-Schwelling (Grüne)
Thomas Kienle (CDU)
Martin Rivoir (SPD)
Leon Genelin (FDP)
Marco Kehr (Die Linke)
Dominic Bartl (Klimaliste)