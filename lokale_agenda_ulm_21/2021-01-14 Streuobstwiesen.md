---
id: "248027750079514"
title: Streuobstwiesen
start: 2021-01-14 19:00
link: https://www.facebook.com/events/248027750079514/
image: 136436686_3419123354863496_2242757952431335342_o.jpg
teaser: "Streuobstwiesen: Ökologische Bedeutung, Gefährdung,
  Fördermöglichkeiten  Referentin: Almut Sattelberger | BUND Kreisverband
  Ulm  Was können wir tun, u"
isCrawled: true
---
Streuobstwiesen: Ökologische Bedeutung, Gefährdung, Fördermöglichkeiten

Referentin: Almut Sattelberger | BUND Kreisverband Ulm

Was können wir tun, um die Streuobstwiesen zu erhalten, die so wichtig für die Artenvielfalt und für unser Landschaftsbild sind?

Ein Vortrag im Rahmen der Reihe "17 Ziele für Ulm und die Welt". Mehr Termine unter www.ideenwerkstatt-ulm.de

