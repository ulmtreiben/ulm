---
id: "399489014636796"
title: Keine Gesundheitsversorgung für alle - Menschen ohne Papiere in Deutschland
start: 2020-12-16 19:00
link: https://www.facebook.com/events/399489014636796/
image: 131056468_3520636964724639_6618931247943250386_o.jpg
teaser: 'Referent: Robin Schöttke | Medinetz Ulm   In Deutschland können sich die
  Menschen auf eine qualitativ hochwertige medizinische Versorgung verlassen."'
isCrawled: true
---
Referent: Robin Schöttke | Medinetz Ulm


In Deutschland können sich die Menschen auf eine qualitativ hochwertige medizinische Versorgung verlassen." Das schrieb zumindest die Bundesregierung im 6. UN-Staatenberichtsverfahren. Aus dem Alltag unserer Beratungsstelle wissen wir: Das gilt zwar für die meisten, jedoch bei weitem nicht für alle Menschen in Deutschland. Rechtliche Fallstricke und praktische Hürden diskriminieren Menschen ohne Papiere in der Praxis darin, ihr Grundrecht auf gesundheitliche Versorgung wahrzunehmen. Von Nachhaltigkeit sind wir in diesem Bereich aktuell noch weit entfernt.

Robin Schöttke berichtet von der medizinischen Betreuung in der Beratungsstelle des Medinetz Ulm e.V. und schildert die zugrundeliegenden Probleme sowie mögliche Lösungsansätze.


https://uni-ulm.webex.com/uni-ulm-de/j.php?MTID=m19f9ebbc3cb861c52ba0da0acd8c9095

Passwort: U7Vf8rbuJA5

Die Veranstaltung ist Teil der Ausstellung "17 Ziele für Ulm und die Welt".
In der Ausstellung „17 Ziele für Ulm und die Welt“ geben Ulmer Organisationen und Gruppen auf anschauliche Weise Antworten, präsentieren sich und ihre Aktivitäten. Zeitgleich ist die Wanderausstellung „Konsum-Kompass“ der Deutschen Bundesstiftung Umwelt zu sehen. Denn wer existiert, der konsumiert, und der Konsumkompass zeigt, worauf wir als Verbraucherinnen und Verbraucher achten können.
Mehr dazu auf: https://www.ideenwerkstatt-ulm.de/
Der Konsumkompass wurde von der Deutschen Bundesstiftung Umwelt und dem Umweltbundesamt erstellt und ist im Besitz des AK Eine Welt Nordhorn. 

