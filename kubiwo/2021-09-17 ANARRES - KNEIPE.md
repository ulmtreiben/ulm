---
id: "370181451439675"
title: ANARRES - KNEIPE
start: 2021-09-17 19:00
address: Mähringer Weg 75, 89075 Ulm
link: https://www.facebook.com/events/370181451439675/
image: 241720859_1257015701427190_7117686074447053437_n.jpg
isCrawled: true
---
Wir wollen mit euch gemeinsam einen schönen Kneipenabend gestalten, bei dem wir uns alle wohl fühlen können. 

Deshalb: 
NO SEXISM
NO RACISM
NO ABLEISM
NO AGEISM
NO BODYSHAMING
NO HOMOPHOBIA
NO NATIONALISM
NO TRANSPHOBIA
... just don't be an Asshole!

wenn ihr Bock habt kommt vorbei. wir freuen uns!

Denkt bitte an euren Mund-Nasenschutz und Impf-/Genesenenausweis bzw eine aktuelle Testbestätigung. Wenn ihr Symptome habt bleibt bitte zu Hause und wir sehen uns dann beim nächsten mal ;)
