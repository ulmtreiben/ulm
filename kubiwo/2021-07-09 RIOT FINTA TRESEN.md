---
id: "819093775413757"
title: RIOT FINTA* TRESEN
start: 2021-07-09 19:00
address: Anarres Ulm
link: https://www.facebook.com/events/819093775413757/
image: 201832172_1203246920137402_2348577430527859631_n.jpg
isCrawled: true
---
Ein safer-space für alle Frauen, inter, nicht-binäre, trans, agender und sonstige Personen, die von cis-männlichen Privilegien ausgeschlossen sind.

Wir wollen mit euch gemeinsam einen empowernden Kneipenabend gestalten, an dem wir uns mal ein bisschen freier von den verschiedenen Einschränkungen und Unterdrückungsformen des Kackscheiß-Patriarchats bewegen können. Ein Abend, an dem wir uns mit anderen Menschen, die auch von verschiedenen Formen von Sexismus betroffen sind, vernetzen und einfach Spaß haben können.

Denkt bitte an euren Mund-Nasenschutz und Impf-/Genesenen-Ausweis bzw eine aktuelle negative Testbestätigung. Wenn ihr Symptome habt, bleibt bitte zu Hause und wir sehen uns dann beim nächsten mal 😉

