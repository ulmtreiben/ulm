---
id: "373548116916402"
title: Grund- und Menschenrechte für alle! Sofort und für immer!
start: 2020-05-16 15:00
end: 2020-05-16 17:00
address: Marktplatz, 89073 Ulm
link: https://www.facebook.com/events/373548116916402/
image: 96983619_3294867510574695_8549142702019575808_n.jpg
teaser: Die jetzige Krise trifft viele Menschen hart. Wie soll man mit
  Kurzarbeitergeld die Miete bezahlen? Wie soll man zuhause im Homeoffice
  arbeiten und ne
isCrawled: true
---
Die jetzige Krise trifft viele Menschen hart. Wie soll man mit Kurzarbeitergeld die Miete bezahlen? Wie soll man zuhause im Homeoffice arbeiten und nebenher die Kinder betreuen, mit ihnen Schulaufgaben machen oder die Pflege von Angehörigen organisieren?

In dieser Situation ist es klar, dass Rufe nach Lockerung laut werden. Aber man sollte nicht vergessen: Es ist vor allem die Wirtschaft mit ihrem Interesse an einer schnellen Wiederaufnahmeder Produktion, die die Politik jetzt Lockerungen beschließen lässt.

Das Versammlungsrecht muss auch unter Corona gelten. Es gibt inzwischen eine Reihe von "Grundgesetzt"-Demos, bei denen ganz unterschiedliche Menschen zusammenkommen. Viele Menschen fühlen sich von den politischen Entscheidungen ausgeschlossen und kritisieren auch die unsozialen Auswirkungen mancher Maßnahmen der Regierung. 

Aber: Auf diesen Demos sind vielfach auch gewaltbereite Nazis und AfD vertreten. 
Für uns steht fest: Widerstand leistet man gegen Nazis, niemals mit ihnen!

Auch wollen wir an die Situation der Geflüchteten in Camps wir Moria auf Lesbos erinnern, die dem Virus schutzlos ausgeliefert sind.

Da wir das Infektionsrisiko so gering wie möglich halten möchten, haben wir nur eine kleine Veranstaltung geplant. Außerdem bitten wir alle Teilnehmer*innen, einen Mund-Nasen-Schutz zu tragen und auf genügend Abstand zu achten.
Allerdings finden wir es falsch, die Straße Nazis und anderen Spinner*innen zu überlassen!  