---
id: "1153685941809182"
title: RIOT FINTA* TRESEN
start: 2021-08-13 19:00
address: Anarres Ulm
link: https://www.facebook.com/events/1153685941809182/
image: 231098024_1233952217066872_6801414538945658399_n.jpg
isCrawled: true
---
Ein safer-space für alle Frauen, inter, nicht-binäre, trans, agender und sonstige Personen, die von cis-männlichen Privilegien ausgeschlossen sind.

Wir wollen mit euch gemeinsam einen empowernden Kneipenabend gestalten, an dem wir uns mal ein bisschen freier von den verschiedenen Einschränkungen und Unterdrückungsformen des Kackscheiß-Patriarchats bewegen können. Ein Abend, an dem wir uns mit anderen Menschen, die auch von verschiedenen Formen von Sexismus betroffen sind, vernetzen und einfach Spaß haben können.

Denkt bitte an euren Mund-Nasenschutz und Impf-/Genesenen-Ausweis bzw eine aktuelle negative Testbestätigung. Wenn ihr Symptome habt, bleibt bitte zu Hause und wir sehen uns dann beim nächsten mal 😉

JEDEN 2. UND 3. FREITAG IM MONAT
AB 19 UHR

im Anarres: Mähringer Weg 75, Ulm