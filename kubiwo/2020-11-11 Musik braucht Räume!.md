---
id: "1434309813438873"
title: Musik braucht Räume!
start: 2021-01-20 19:00
end: 2021-01-20 22:00
address: Mähringer Weg 75, 89075 Ulm
link: https://www.facebook.com/events/1434309813438873/
image: 119899493_1012280709234025_6130902897591431472_n.jpg
teaser: Proberäume für Musiker*innen und Bands in Ulm sind knapp und teuer. Das muss
  sich ändern!  Darum laden wir alle interessierten Menschen ein, um zusamm
isCrawled: true
---
Proberäume für Musiker*innen und Bands in Ulm sind knapp und teuer. Das muss sich ändern!

Darum laden wir alle interessierten Menschen ein, um zusammen zu überlegen, was sich machen lässt.

Wir wollen:

    Bedarf und Möglichkeiten ermitteln
    Ideen und Erfahrungen austauschen
    Netzwerken und Perspektiven finden

11.11.2020/19:00/Anarres
Mähringer Weg, 89075 Ulm
www.kubiwo.blogsport.eu