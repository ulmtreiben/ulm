---
id: "255092389762162"
title: ANARRES - KNEIPE
start: 2021-08-06 19:00
address: Mähringer Weg 75, 89075 Ulm
link: https://www.facebook.com/events/255092389762162/
image: 231473878_1234030147059079_2977798156179979361_n.jpg
isCrawled: true
---
Wir wollen mit euch gemeinsam einen schönen Kneipenabend gestalten, bei dem wir uns alle wohl fühlen können. 

Deshalb: 
NO SEXISM
NO RACISM
NO ABLEISM
NO AGEISM
NO BODYSHAMING
NO HOMOPHOBIA
NO NATIONALISM
NO TRANSPHOBIA
... just don't be an Asshole!

wenn ihr Bock habt kommt vorbei. wir freuen uns!

Denkt bitte an euren Mund-Nasenschutz und Impf-/Genesenenausweis bzw eine aktuelle Testbestätigung. Wenn ihr Symptome habt bleibt bitte zu Hause und wir sehen uns dann beim nächsten mal ;)
