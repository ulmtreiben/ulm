---
name: KuBiWo
website: http://kubiwo.blogsport.eu
email: kubiwo@riseup.net
scrape:
  source: facebook
  options:
    page_id: AkPropagandaPanda
---
KuBiWo e.V. ist ein kleiner, gemeinnütziger Verein, der sich für nicht-kommerzielle Kultur, emanzipatorische Bildung und alternatives Wohnen in Ulm und Umgebung stark macht.
