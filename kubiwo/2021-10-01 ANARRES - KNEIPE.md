---
id: "1220295715119781"
title: ANARRES - KNEIPE
start: 2021-10-01 19:00
address: Mähringer Weg 75, 89075 Ulm
link: https://www.facebook.com/events/1220295715119781/
image: 243203089_1269435720185188_211189772404844503_n.jpg
isCrawled: true
---
Wir wollen mit euch gemeinsam einen schönen Kneipenabend gestalten, bei dem wir uns alle wohl fühlen können. 

Deshalb: 
NO SEXISM
NO RACISM
NO ABLEISM
NO AGEISM
NO BODYSHAMING
NO HOMOPHOBIA
NO NATIONALISM
NO TRANSPHOBIA
... just don't be an Asshole!

wenn ihr Bock habt kommt vorbei. wir freuen uns!

Denkt bitte an euren Mund-Nasenschutz und Impf-/Genesenenausweis. Solltet ihr aus wirklichen gesundheitlichen Gründen keine Impfung vertragen, bringt bitte eine ärztliche Bestätigung mit und meldet euch am bessten vor der Veranstaltung bei uns. Schwurbler können gleich zu Hause bleiben. Wenn ihr Symptome habt, bleibt auch bitte zu Hause und wir sehen uns dann beim nächsten mal ;)
