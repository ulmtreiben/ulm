---
id: "233374914442889"
title: Kritik des Antisemitismus
start: 2020-04-25 18:00
end: 2020-04-25 21:00
address: Online
link: https://www.facebook.com/events/233374914442889/
image: 94488719_2862229940479208_2636836300262473728_n.jpg
teaser: "Livestream: https://www.facebook.com/kollektiv.26/live/  Wie entsteht
  Antisemitismus allgemein, aber vor allem auch wie kann er in einer linken
  Bewegu"
isCrawled: true
---
Livestream: https://www.facebook.com/kollektiv.26/live/

Wie entsteht Antisemitismus allgemein, aber vor allem auch wie kann er in einer linken Bewegung aufkommen und welche Rolle nimmt dabei Israel und der Bezug auf diesen Staat ein?
Der Vortrag gibt einen Überblick über die historische Entwicklung des Antisemitismus und der Kritik an diesem. Der Fokus liegt dabei auf dem Antisemitismus in der deutschen Linken. Um die Entstehung und Entwicklung von linkem Antisemitismus nachzuvollziehen, wird die historische Herausbildung und Ausdifferenzierung der politisch linken Zusammenhänge in Deutschland dabei beleuchtet. Da vor allem in der jüngeren Geschichte derselben ein israelbezogener Antisemitismus eine besondere Rolle spielt, wird anhand dieser historischen Entwicklung auch versucht das Verhältnis zwischen der Linken und dem jüdischen Staat anschaulich zu machen.
Ein Vortrag in Zusammenarbeit mit Deutsch-Israelische Gesellschaft Ulm/Neu-Ulm, RLS BW und INPUT Ulm