---
id: "177346454082945"
title: Kein Bock auf rechte Corona-Kritik
start: 2020-12-19 18:15
address: Münsterplatz, 89073 Ulm
link: https://www.facebook.com/events/177346454082945/
image: 131691783_3540371505998378_1543970987872350741_n.jpg
teaser: Seit Wochen tourt der Frauenbus um die Corona-Leugner*innen Eva Rosen, Sandra
  Wesolek, Alexandra Motschmann, Janko Williams und Antonia Kellnerberger
isCrawled: true
---
Seit Wochen tourt der Frauenbus um die Corona-Leugner*innen Eva Rosen, Sandra Wesolek, Alexandra Motschmann, Janko Williams und Antonia Kellnerberger durch Deutschland. Täglich veranstalten sie Kundgebungen an verschiedenen Orten gegen die Pandemie-Maßnahmen. Dabei verbreiten sie auch verschwörungsideologische, antisemitische, reichsbürgernahe Inhalte, NS-Relativierung sowie Drohungen gegen Antifaschist*innen, Ordnungsämter und Polizei. Sie  missachten konsequent die Auflagen, tragen keine oder inadäquate Masken aus Netzstoffen.
Am Samstag den 19.12 um 18 Uhr macht dieser Schwurbelbus nun Halt in Ulm. Vermutlich auf dem Münsterplatz (die Versammlungsorte werden gerne mal kurzfristig geändert, Änderungen erfahrt ihr hier).
Das wollen wir so nicht stehen lassen. Also kommt mit Maske und Abstand, um diesen Menschen zu zeigen, dass solche Inhalte hier in Ulm keinen Platz haben.
Lasst uns ihnen ihre Tour vermiesen!

Samstag 19.12. 18:15 Uhr Münsterplatz!