---
id: "2538902593096165"
title: Gegen den Neujahrsempfang der AfD in Reutlingen
start: 2020-02-21 18:00
end: 2020-02-21 21:00
address: Marktplatz Reutlingen
link: https://www.facebook.com/events/2538902593096165/
teaser: Kundgebung des Bündnis Gemeinsam & Solidarisch gegen Rechts Reutlingen und
  Tübingen  Kundgebung Freitag, 21. Februar 17 Uhr, Marktplatz Reutlingen  Zu
isCrawled: true
---
Kundgebung des Bündnis Gemeinsam & Solidarisch gegen Rechts Reutlingen und Tübingen

Kundgebung
Freitag, 21. Februar
17 Uhr, Marktplatz Reutlingen

Zugtreffpunk aus Tübingen:
am Freitag, 21. Februar um 16:20 Uhr am Tübinger HBF 


Auch 2020 will die AfD Reutlingen das neue Jahr mit einem
Event im städtischen Spitalhof einläuten. Einen solchen
Neujahrsempfang nutzt sie unter anderem, um gemeinsam mit ihren
WählerInnen auf ein vergangenes und auch auf ein kommendes Jahr voll
rechter Hetze und menschenverachtende Politik anzustoßen und stellt somit ein für sie selbst wichtiges Element in der Bildung einer rechten Gemeinschaft dar.

Dabei bleibt es aber nicht stehen. Auch in der gewünschten Außenwirkung ist der Neujahrsempfang wichtiger Bestandteil und soll dabei Strahlkraft entwickeln. Dabei gaben sich in der Vergangenheit immer wieder rechte und offen faschistische Akteure aus der Region die Klinke in die Hand.
Daher müssen wir dem Neujahresempfang der AfD in Reutlingen als das begegnen, was er ist: Der Versuch eines Dreh- und Angelpunktes der sog. „Neuen Rechten“ für die Region.

Seit der Kommunalwahl im Mai 2019 ist die AfD nun auf sämtlichen
politischen Ebenen in Baden-Württemberg vertreten, so auch im Reutlinger
Gemeinde- und Kreisrat. Das begreifen wir als ein Zeichen der Manifestation einer gefährlichen Rechtsentwicklung, der wir uns entschieden entgegenstellen müssen. Ein Teil davon ist es, zu erkennen, dass sich die AfD, trotz demokratischer Wahlen, noch lange nicht als demokratische Partei auszeichnet, geschweige denn von uns als solche legitimiert werden darf. Denn Hetze gegen Migrant*innen, Geflüchtete, Homosexuelle oder Obdachlose kann niemals demokratisch sein. Im Gegenteil, mit dieser Politik treibt die Partei eine Spaltung all derer voran, die eigentlich ein Interesse an einer anderen Gesellschaft, jenseits von Profitgier und Umweltzerstörung, haben. Damit steht die AfD unserer Perspektive eines guten Lebens für alle fundamental entgegen.

Wir stellen uns aktiv gegen eine Entwicklung, die diese Spaltung
der Gesellschaft vorantreibt, um gesellschaftliche Errungenschaften
rückgängig zu machen. Deshalb rufen wir all diejenigen zu Protesten gegen den AfD-Neujahrsempfang in Reutlingen auf, die etwas gegen Nazis und Rechte unternehmen wollen.

Lasst uns gemeinsam und solidarisch unsere Stimmen und Hände erheben – gegen rassistische Hetze und für ein solidarisches Miteinander!

Ihr wollt den Aufruf unterstützen?
Schreibt eine Mail an gemeinsam_solidarisch@mtmedia.org

www.gemeinsam-solidarisch.de