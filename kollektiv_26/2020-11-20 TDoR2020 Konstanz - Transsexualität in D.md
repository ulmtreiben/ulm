---
id: "736880166905465"
title: TDoR2020 Konstanz - Transsexualität in Deutschland
start: 2020-11-20 19:00
end: 2020-11-20 22:00
link: https://www.facebook.com/events/736880166905465/
image: 123120951_203359217817701_671845202991742575_n.jpg
teaser: "Was: \tVortrag über Diskriminierung, Geschlechtsangleichung und das deutsche
  Transsexuellengesetz Wann: \t20. November 2020, ab 19 Uhr Wo: \t\tVia Zoom Vi"
isCrawled: true
---
Was: 	Vortrag über Diskriminierung, Geschlechtsangleichung und das deutsche Transsexuellengesetz
Wann: 	20. November 2020, ab 19 Uhr
Wo: 		Via Zoom Videokonferenz unter:  www.vdge.org/tdor2020kn

Meeting-ID: 744 727 2486
Kenncode: 78315

----------------------------

Vortrag über Diskriminierung, Geschlechtsangleichung und das deutsche Transsexuellengesetz

Am internationalen Transgendergedenktag, dem Transgender Day of Remembrance am 20. November, erzählen Christin und Michelle Löhner, Gabi Strecker und Elena Ehret von sich und ihrem Leben als transsexuelle Frauen. Sie erzählen, wie viele transsexuelle Menschen weltweit immer noch erschlagen und ermordet werden und warum es auch in Deutschland immer noch schwer ist, sich zu outen und den Weg der Transition zu gehen. Sie erklären, warum das Transsexuellengesetz abgeschafft gehört und wieso Selbstbestimmung in Deutschland immer noch ein Fremdwort ist. Die vier Frauen unterhalten sich darüber, was besser werden muss und wie man es erreichen könnte. Elena, selbst eine begnadete Künstlerin stellt dazu ihre eigenen Bilder aus und erzählt, wie sie ihre Welt in ihren Bildern ausdrückt.

Christin Löhner ist die Gründerin der Selbsthilfeinitiative VDGE e.V., des Vereins für Menschen mit Varianten der Geschlechtsentwicklung e.V., die mit zwölf Peer-Beratungsstellen und sechs Selbsthilfegruppen in ganz Deutschland und der Schweiz Aufklärungs- und Beratungsarbeit leistet. Christin Löhner war die einzige Frau, die sich für die Wahl zum Oberbürgermeisteramt in Konstanz hat aufstellen lassen. Aufgrund der Kandidatur von Luigi Pantisano hatte sie Ende Februar dann ihre Kandidatur wieder zurück gezogen.

Was: 	Vortrag über Diskriminierung, Geschlechtsangleichung und das deutsche Transsexuellengesetz
Wann: 	20. November 2020, ab 19 Uhr
Wo: 		Via Zoom Videokonferenz unter:  www.vdge.org/tdor2020kn

Bitte besuchen Sie zum angegebenen Zeitpunkt den Link, um am Vortrag teilzunehmen.