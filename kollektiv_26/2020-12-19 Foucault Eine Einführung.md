---
id: "765682980693526"
title: "Foucault: Eine Einführung"
start: 2020-12-19 15:00
end: 2020-12-19 18:00
link: https://www.facebook.com/events/765682980693526/
image: 132036129_3542818185753710_6847460298572651273_n.jpg
teaser: Der Workshop findet Online am 19.12.2020 von 15 bis 18 Uhr statt. Anmeldungen
  sind leider nicht mehr möglich, die maximale Teilnahmezahl ist erreicht.
isCrawled: true
---
Der Workshop findet Online am 19.12.2020 von 15 bis 18 Uhr statt. Anmeldungen sind leider nicht mehr möglich, die maximale Teilnahmezahl ist erreicht. Freuen uns über das große Interesse!

Das Smartphone in der Tasche, welches unser Handeln bestimmt, Kameras und Sprachassistenten, die alles, was wir tun beobachten und analysieren und arbeitende Menschen, welche so sehr in Konkurrenz zueinander sind, dass sie sich nicht mehr als Kollektive organisieren. Die Gesellschaft, in der wir leben ähnelt immer mehr den dystopischen Überwachungsgesellschaften die wir aus Romanen wie 1984, A Brave New World oder Necromancer kennen.

Den Anfang dieser Entwicklung beobachteten auch schon die 68er, den im Zuge des neuen Bedarfs an Alternativen zum Bestehenden, entwickelte die Protestgeneration auch neue Analysen. Diese blickten nicht nur auf die Industrialisierung, sondern auch auf die grauenhaften Erlebnisse des Zweiten Weltkriegs. Die autoritären und totalitären Aspekte kapitalistischer Gesellschaft gerieten dabei zunehmend in den Blick. Im Zuge dessen begann auch Michel Foucault seine Gesellschaftsanalyse, mit dem Ziel die Machtstrukturen die den Kapitalismus erhalten, zu beschreiben und Ansatzpunkt für eine Kritik dieser zu schaffen.

Der Workshop versucht, mit Hilfe des Originaltextes diese Machtanalyse von Foucault zu umreißen. Ausgehend von der Entwicklung der Disziplinargesellschaft, in der kontinuierlich überwacht und bestraft wird, betrachten wir Wissen, Macht und Institutionen, mit deren Hilfe die „Kunst des Regierens“ umgesetzt wird, eine Kunst welche den Erhalt des Kapitalismus (das Machtsystem der kapitalistischen Produktion) zum Ziel hat.

Der Workshop findet Online am 19.12.2020 von 15 bis 18 Uhr statt. Anmeldung bitte an: input-ulm@riseup.net