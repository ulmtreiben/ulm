---
id: "706798490244532"
title: "Bini Adamczak: Versprechen Der Gegenwart"
start: 2020-12-15 18:00
link: https://www.facebook.com/events/706798490244532/
image: 131321740_236490104494962_2410578069096146198_n.jpg
teaser: "uch auf BigBlueButtom: https://webinar.rosa-reutlingen.de/b/kol-kv5-uf5 Als
  1990 die Sowjetunion implodierte meinten viele, die Geschichte sei zu Ende"
isCrawled: true
---
uch auf BigBlueButtom: https://webinar.rosa-reutlingen.de/b/kol-kv5-uf5
Als 1990 die Sowjetunion implodierte meinten viele, die Geschichte sei zu Ende. Die repräsentative kapitalistische Demokratie habe für alle Zukunft gewonnen. An diese These mag heute niemand mehr glauben. Nicht zuletzt, weil in den letzten Jahren die hässlichsten Leichen der Vergangenheit wieder die Bühne der Geschichte betreten haben. Die rechtsnationalistischen, faschistischen oder neonationalsozialistischen Bewegungen, die viele für längst begraben hielten, kehren zurück. Ihre Ideologien, Organisationstrukturen und Propagandamethoden sind immer wieder beschrieben worden. Aber warum tauchen sie jetzt wieder auf? Was sind die Risse in den Verhältnissen, aus denen sie emporsteigen? Woher beziehen sie ihre Attraktivität, welches Versprechen geben sie ihren Anhängern? Und vor allem: Was lässt sich ihnen entgegen halten?