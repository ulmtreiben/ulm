---
id: "768246367149378"
title: Querdenken in Ulm – Wütend, wohlhabend und weit offen nach rechts
start: 2021-07-04 18:00
link: https://www.facebook.com/events/768246367149378/
image: 200308958_2660914907540954_5106699581780887579_n.jpg
isCrawled: true
---
 📅 04.07.21 ⌚ 18:00📌 Online 💬 deutsch

Seit April 2020 gehen in Ulm verschiedene Gruppen auf die Straße. Am bekanntesten davon ist Querdenken 731. In diesem Vortrag soll die Entstehung und Entwicklung aufgezeigt werden aus der Sicht des Kollektiv.26, das von Anfang an die lokalen Versammlungen beobachtet und dagegen protestiert hat.

Die Veranstaltung wird im folgenden Raum stattfinden:
https://webinar.rosa-reutlingen.de/b/kol-gqn-rvm

#fclr2021ulm #kollektiv26
____________________________________________

 "Querdenken" in Ulm – Furious, prosperous and wide open to the right wing

📅 04.07.21 ⌚ 8 pm 📌 Online 💬 German

Since April 2020, various groups have been taking to the streets in Ulm. The best known of these groupes is Querdenken 731. This lecture will show the emergence and development from the perspective of Kollektiv.26, which has observed and protested against the local events from the beginning.

Link:
https://webinar.rosa-reutlingen.de/b/kol-gqn-rvm