---
id: "2641549932813999"
title: BDS - Boycott Israel?
start: 2021-09-03 18:00
locationName: Bürgerzentrum Eselsberg Ulm
address: 89075 Ulm
link: https://www.facebook.com/events/2641549932813999/
image: 223186300_4176001619102027_4798932475770394764_n.jpg
isCrawled: true
---
Die BDS-Bewegung (Boycott, Divestment and Sactions against Israel) ist ein Thema, bei dem viele politische Gruppen Schwierigkeiten haben es einzuordnen. Kleine, aber insbesondere linke Gruppierungen haben selten eine Position dazu und auch oft wenig Ahnung.
Der Vortrag soll eine Einführung geben zu der Entstehung, den Inhalten, Methoden und Akteuren des BDS, anhand welcher eine Bewertung vorgenommen wird.