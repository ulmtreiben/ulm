---
id: "176188217169342"
title: Ein würdiges Leben für alle!
start: 2020-06-06 15:30
end: 2020-06-06 17:00
locationName: Ulmer Münster
address: Münsterplatz, 89073 Ulm
link: https://www.facebook.com/events/176188217169342/
image: 101338368_2968522763183258_6752494651594244096_o.jpg
teaser: "Den Rechten den (Münster)Platz nehmen:  Wir setzen uns ein für ein würdiges
  Leben, als Betroffene und für Betroffene der Einschränkungen und deren Fol"
isCrawled: true
---
Den Rechten den (Münster)Platz nehmen:

Wir setzen uns ein für ein würdiges Leben, als Betroffene und für Betroffene der Einschränkungen und deren Folgen. Während der Schutz der Gesundheit durch einige Reglungen, wie die Maskenpflicht notwendigerweise erfolgt, so sorgen andere Reglungen für die Gefährdung der Gesundheit.
Statt Sammelunterkünfte und Lager für Geflüchtete sind Wohnräume zur Verfügung zu stellen, die denen der meisten Menschen in Deutschland entsprechen. Selbes gilt für Obdachlose.
Kurzarbeit darf kein Mittel sein, mit dem sich Firmen bereichern. Diese Sollen das Kurzarbeitsgeld auf 100% aufstocken müssen.
Kündigungen von Mieter*innen und Angestellten müssen verhindert werden.
Finanzielle Unterstützung für kleine Kulturorte, Kunstschaffende und Gaststättenbetreiber*innen.
Ausbau von Frauenhäusern und Weglaufhäusern für queere Jugendliche und von Gewalt Betroffene.
Flächendeckender Zugang zu Schwangerschaftsabbrüchen, gerade jetzt.

Und schlussendlich: Ein würdiges Leben für alle, kein bisschen weniger! Kein Platz für Nazis und Verschwörungsidiologen.