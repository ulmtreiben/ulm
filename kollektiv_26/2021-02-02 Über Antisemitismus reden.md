---
id: "233966814873583"
title: Über Antisemitismus reden
start: 2021-02-02 18:30
link: https://www.facebook.com/events/233966814873583/
image: 143528403_3651345121567682_2273926699541546468_o.jpg
teaser: Auch dieses Jahr finden wieder die Friedenswochen statt. In diesem
  Zusammenhang wollen wir den BDS ("Boykott, Desinvestitionen und Sanktionen"
  gegen I
isCrawled: true
---
Auch dieses Jahr finden wieder die Friedenswochen statt. In diesem Zusammenhang wollen wir den BDS ("Boykott, Desinvestitionen und Sanktionen" gegen Israel) zum Thema machen, welcher dort Befürworter*innen zu haben scheint.

Wir laden deswegen zu einem online-Vortrag mit anschließender Diskussion von Peter Ullrich ein. Thema: "Über Antisemitismus sprechen".

Eine multidimensionale Debatte wie die Auseinandersetzung mit israelbezogenem Antisemitismus erfordert Deutungsdemut statt administrativer Diskursbeendigung. Dem Anliegen widmet sich der Vortrag durch Darstellung, was Antisemitismus ist, wie er sich zeigt und insbesondere, welche Bedingungen die Auseinandersetzungen mit Antisemitismus gerade in Deutschland und gerade in Bezug auf den israelisch-palästinensischen Konflikt so kompliziert machen. Neben der grundlegenden erinnerungspolitischen Dimension geht es auch um die drängenden aktuellen Fragen der angemessenen Definition von Antisemitismus und des Umgangs mit der BDS-Bewegung.

Der Vortrag wird von der Rosa-Luxemburg Stiftung Baden-Württemberg gefördert und vom Infoladen der KTS Freiburg und Input Freiburg organisiert.

Wenn ihr am Vortrag teilhaben wollt, dann schreibt uns eine E-Mail an infoladen[ät]kts-freiburg.org und wir senden euch den Link zur Teilnahme.