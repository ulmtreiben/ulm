---
id: "943112602844673"
title: Prozessende Antiziganistischer Mordversuch
start: 2020-09-23 18:00
end: 2020-09-23 19:00
address: Münsterplatz, 89073 Ulm
link: https://www.facebook.com/events/943112602844673/
teaser: Zum Prozessende rufen wir auf zu einer Kundgebung. Antiziganismus und rechtes
  Gedankengut sind weit verbreitet und müssen gesamtgesellschaftlich angeg
isCrawled: true
---
Zum Prozessende rufen wir auf zu einer Kundgebung. Antiziganismus und rechtes Gedankengut sind weit verbreitet und müssen gesamtgesellschaftlich angegangen werden.

Der Prozess gegen die 5 Nazi-Hooligans, die in Erbach/Dellmensingen letztes Jahr aus antiziganistischer Motivation versuchten einen bewohnten Wohnwagen anzuzünden endet am Mittwoch. Die Urteilsverkündung am kommenden Mittwoch wird hoffentlich das Tatmotiv anerkennen und auch Fußball und das rechte (Dorf)Umfeld als Probleme benennen. 
Bei vier der Angeklagten ist offensichtlich, dass eine Änderung der Sichtweise auch durch diesen Prozess und ihre U-Haft nicht stattgefunden hat. Auch das findet vermutlich Beachtung.

Mit dem Urteil ist es jedoch nicht vorbei. Weiterhin gibt es zu viele rechte Hooligans im SSV Ulm 1846 Fußball, der Verein schaut weg. Dass die Ultras sich distanzierten sollten sich einige mehr als Vorbild nehmen. In Dellmensingen fangen einzelne Leute an das Problem anzuerkennen, treten in den Dialog und veranstalteten eine Demonstration. Wir hoffen, dass diese Leute mehr Unterstützung bekommen, auch von offizieller Seite und dort ansässigen Vereinen. Auch in Ulm gab es Leute, die den Prozess verfolgten. Hier wünschen wir uns jedoch eine viel breitere Auseinandersetzung. 
(Sammlung an Texten zum Prozess: https://kollektiv26.blackblogs.org/2020/07/13/braune-brut-zwischen-rechten-doerfern-und-hooligans/)