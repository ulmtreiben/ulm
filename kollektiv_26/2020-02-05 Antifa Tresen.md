---
id: "190931202301890"
title: Antifa Tresen
start: 2020-02-05 19:30
end: 2020-02-05 22:30
address: Mähringer Weg 75, 89075 Ulm
link: https://www.facebook.com/events/190931202301890/
image: 84178754_2680916581943879_3596668766869520384_o.jpg
teaser: Kommt vorbei, um gemütlich mit uns zu quatschen, ein Getränk zu genießen oder
  einfach zum abhängen. (Im Anarres)
isCrawled: true
---
Kommt vorbei, um gemütlich mit uns zu quatschen, ein Getränk zu genießen oder einfach zum abhängen. (Im Anarres)