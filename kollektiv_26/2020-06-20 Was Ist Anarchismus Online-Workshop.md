---
id: "1385882414932477"
title: Was Ist Anarchismus? Online-Workshop
start: 2020-06-20 18:00
end: 2020-06-20 19:30
link: https://www.facebook.com/events/1385882414932477/
image: 103445731_177017807108859_4754728226867340642_o.jpg
teaser: "Online/Live über BigBlueButton:
  https://webinar.rosa-reutlingen.de/b/kol-7vm-pvr Ausweichmöglichkeit ggf.
  Facebook: https://www.facebook.com/kollektiv"
isCrawled: true
---
Online/Live über BigBlueButton: https://webinar.rosa-reutlingen.de/b/kol-7vm-pvr Ausweichmöglichkeit ggf. Facebook: https://www.facebook.com/kollektiv.26/live/

Neben der kommunistischen Bewegung stellt der Anarchismus einen zentralen Bezugspunkt der zeitgenössischen Linken dar. Die Beziehung der beiden 'verfeindeten Brüder' sorgt nach wie vor für hitzige Debatten und Zerwürfnisse. Doch wie lässt sich der Anarchismus überhaupt definieren? Staatsfeindlich, radikal, gegen Autoritäten?
In der Veranstaltung zum Anarchismus, im Rahmen der Input "Was ist ...?"-Reihe versuchen wir nach einem kurzen Input eine zeitgemäße Definition dieser nicht zu unterschätzenden politischen Strömung zu entwerfen. Außerdem lohnt es sich zu diskutieren, was Kommunismus und Anarchismus voneinander lernen können.

"Was ist..." ist eine Veranstaltungsreihe von INPUT-Ulm/Neu-Ulm zu linken Grundbegriffen.
