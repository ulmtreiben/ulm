---
id: "489059628393423"
title: Im Namen der Weltrettung - Esoterik und Tiefenökologie
start: 2020-01-25 18:00
end: 2020-01-25 21:00
address: Anarres, Mähringerweg 75, 89075 Ulm
link: https://www.facebook.com/events/489059628393423/
image: 80096051_2604190112949860_4175295355216199680_o.jpg
teaser: Im Camp von Extinction Rebellion in Berlin gab es im Oktober einen Workshop zu
  Tiefenökologie. Propagandist*innen dieser Lehre machen eine mangelnde S
isCrawled: true
---
Im Camp von Extinction Rebellion in Berlin gab es im Oktober einen Workshop zu Tiefenökologie. Propagandist*innen dieser Lehre machen eine mangelnde Spiritualität als Ursache der Umweltzerstörung aus, halten den Planeten für überbevölkert, aber den Kapitalismus für eine gute Wirtschaftsform.
So versuchen Trittbrettfahrer*innen aus dem Bereich des Obskurantismus vom Aufschwung der Umweltbewegung im Gefolge von Fridays for Future zu profitieren, Vertreter*innen einer Ökodiktatur ebenso wie Öko-Malthusianer*innen.
Esoterische Strömungen haben in der Umweltbewegung eine Tradition, die bis in die Lebensreform des Kaiserreichs zurückreicht. Die moderne Esoterik begann damals ihren Siegeszug, heute glauben Millionen an Horoskope, Wahrsagerei und Tarot-Karten. Die Anthroposophie mit all ihren Projekten von biologisch-dynamischer Landwirtschaft, Kosmetika und Heilmittel bis zur Waldorfschule, entstammt diesem Milieu. Später hatte die New-Age-Szene ihren Anteil an der modernen Umweltbewegung.

Peter Bierl gibt in seinem Vortrag einen Überblick über Geschichte und Gegenwart esoterischer Strömungen in der Umweltbewegung, wobei er sich auf Tiefenökologie und Biozentrismus konzentriert.
Bierl ist freier Journalist und lebt in der Nähe von München. Er ist Autor von „Einmaleins der Kapitalismuskritik“ (2018), „Grüne Braune: Umwelt-, Tier- und Heimatschutz von rechts“ (2014), „Schwundgeld, Freiwirtschaft und Rassenwahn. Kapitalismuskritik von rechts: Der Fall Silvio Gesell“ (2012) sowie „Wurzelrassen, Erzengel und Volksgeister. Die Anthroposophie Rudolf Steiners und die Waldorfpädagogik“ (2005).

Infos zu weiteren Vorträgen: https://kollektiv26.blackblogs.org/2019/12/22/vortraege-fuer-das-naechste-halbjahr/