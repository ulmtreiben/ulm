---
id: "881226885996666"
title: KONSEQUENT. ANTIFASCHISTISCH. Überregionale Solidaritätsdemo
start: 2021-03-20 14:00
locationName: Stuttgart Hauptbahnhof
address: Arnulf-Klett-Platz, 70173 Stuttgart
link: https://www.facebook.com/events/881226885996666/
image: 150782736_5029402627132227_5643008484534300459_o.jpg
teaser: Praktisch im Wochentakt werden zur Zeit faschistische Netzwerke aufgedeckt,
  nicht wenige davon mit Verbindungen zu Militär, Polizei und Verfassungssch
isCrawled: true
---
Praktisch im Wochentakt werden zur Zeit faschistische Netzwerke aufgedeckt, nicht wenige davon mit Verbindungen zu Militär, Polizei und Verfassungsschutz. Größere Waffenfunde und Todeslisten inklusive. Gleichzeitig erlebt der Rechtsterrorismus mit den Anschlägen in Hanau, Halle und Kassel einen lange nicht dagewesenen Aufschwung. Politisch wird diese Entwicklung mal mehr oder weniger offen von der AfD wohlwollend begleitet. Die rechte Hetze der blauen Politiker ist der Nährboden, auf dem andere zur Tat schreiten. Und im Zuge der aktuellen Krise tummeln sich bei Veranstaltungen der selbsternannten „Querdenker“ Verschwörungstheoretiker*innen und andere Rechte massenhaft auf den Straßen der Republik.
Kurz: Die gesellschaftliche Rechtsentwicklung ist sicht- und greifbar wie lange nicht. Grund genug zu handeln.

Doch, zumindest auf institutioneller Ebene, passiert das genaue Gegenteil: In den Innenministerien wird lieber über Verbote linker Gruppen diskutiert, die Mörder von Hanau, Kassel und Halle werden zu verwirrten Einzeltätern gemacht, antifaschistische Proteste werden kriminalisiert und mit der Gleichsetzung von Rechts und Links wird die faschistische Menschenverachtung relativiert.
In der Konsequenz hofiert der Staat eher die Rechten als dass er sie bekämft. Diese Erkenntnis ist nicht neu, bestätigt aber ein weiteres Mal die Notwendigkeit eines selbstorganisierten, verantwortlichen Antifaschismus. Und so ist es die antifaschistische Bewegung, die sich aktuell der Rechtsentwicklung konsequent entgegenstellt und vielschichtigen Widerstand sowie Selbstverteidigung gegen die Angriffe von Rechts organisiert. Dazu gehören gesellschaftliche Aufklärungsarbeit, Straßenproteste gegen rechte Veranstaltungen und Bündnisarbeit mit anderen Kräften. Notwendiger Teil eines effektiven Antifaschismus ist aber auch die offensive Zurückdrängen und somit die direkte Konfrontation der Faschist*innen.

Für genau solches, konsequentes Handeln soll den Antifaschisten Jo und Dy im Frühjahr 2021 in Stuttgart der Prozess gemacht werden. Beiden wird vorgeworfen an einem Angriff auf Nazis der rechten Scheingewerkschaft „Zentrum Automobil“ am Rande einer Querdenken-Demo in Stuttgart beteiligt gewesen zu sein, bei dem einige Nazis zum Teil schwer verletzt wurden.
Mehrere Woche nach der Auseinandersetzung begann eine Welle der Repression gegen die antifaschistische Bewegung in Baden-Württemberg. Es folgten zehn Hausdurchsuchungen, offene Observationen sowie staatsanwaltschaftliche Zeug*innenvorladungen. Jo und Dy wurden festgenommen und in U-Haft gesteckt. Während Jo nach sechs Monaten die JVA verlassen konnte, sitzt Dy weiter hinter Gittern.
Das Stuttgarter Verfahren ist nicht der einzige Angriff der Behörden auf die linke Bewegung im letzten Jahr. So wurde beispielsweise in Leipzig die Antifaschistin Lina ebenfalls wegen Angriffen auf Neonazis festgenommen und in den Knast gesteckt. Auch sie sitzt noch immer.

Die Behörden reagieren nicht ohne Grund mit Härte: Das direkte Infragestellen des staatlichen Gewaltmonopols durch eine organisierte antifaschistische Bewegung ist dem Staat ein Dorn im Auge. Schließlich beschleunigt die Corona-Pandemie die kapitalistische Krisenentwicklung und schafft damit eine Situation, in der der Staat gezwungen ist die Macht- und Eigentumsverhältnisse noch stärker zu schützen. Der präventive Schlag nach links ist da nur folgerichtig und soll eine antikapitalistische, selbstorganisierte Bewegung von vorne herein delegitimieren und schwächen.

Auch wenn die direkte Repression scheinbar nur Einzelne betrifft, ist sie ein Angriff auf die gesamte Bewegung, der wir uns kollektiv entgegenstellen müssen. Vor Gericht stehen Jo und Dy, angeklagt ist der entschiedene Kampf gegen die Gefahr von Rechts. Genau deswegen stehen wir als Bewegung spektrenübergreifend solidarisch zusammen.

Und: Wir lassen uns von den staatlichen Angriffen nicht einschüchtern und kämpfen weiter. Selbstbestimmter Antifaschismus ist in Zeiten von rechten Mordanschlägen und staatlicher Verstrickungen in rechte Netzwerke nicht nur legitim sondern schlicht und ergreifend lebensnotwendig!
Im Rahmen des Prozessauftaktes gegen Jo und Dy und im Kontext des Tags der politischen Gefangenen rufen wir deswegen zu einer überregionalen Demonstration auf.

Für einen konsequenten Antifaschismus!
Freiheit für alle politischen Gefangenen!
Freiheit für Dy und Lina!

Kommt am 20.März 2021 zur überregionalen Demo nach Stuttgart.
Achtet auf die sinnvollen Hygieneregeln und bringt Masken mit.