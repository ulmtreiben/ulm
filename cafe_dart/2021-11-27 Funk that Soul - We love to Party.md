---
id: "1061569291251491"
title: Funk that Soul - We love to Party
start: 2021-11-27 20:30
locationName: Café D'Art
address: Augsburgerstraße 35, 89231 Neu-Ulm
link: https://www.facebook.com/events/1061569291251491/
image: 242600614_2124613327689603_3711833657507906083_n.jpg
isCrawled: true
---
Wir freuen uns auf einen musikalischen Streifzug durch die Funk und Soul Ära im Cafe D'Art. 

Aufgrund begrenzter Plätze sind Reservierungen im Cafe D'Art bereits möglich. Eintritt 12€.

Wir freuen uns auf Euch.