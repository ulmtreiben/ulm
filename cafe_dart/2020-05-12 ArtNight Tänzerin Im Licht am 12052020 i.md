---
id: "464831941108307"
title: "ArtNight: Tänzerin Im Licht am 12/05/2020 in Ulm"
start: 2020-05-12 20:00
end: 2020-05-12 22:30
address: Augsburger Straße 35, 89231 Neu-Ulm
link: https://www.facebook.com/events/464831941108307/
image: 82374873_2571724323057836_8188321503050203136_o.jpg
teaser: "Wir haben 100 Leute gefragt: “Wie würdest du ArtNight einem Freund
  beschreiben”  “Male in 2-3 Stunden ein von dir ausgewähltes Motiv und sei
  überrasch"
isCrawled: true
---
Wir haben 100 Leute gefragt: “Wie würdest du ArtNight einem Freund beschreiben”

“Male in 2-3 Stunden ein von dir ausgewähltes Motiv und sei überrascht, dass du es in der kurzen Zeit schaffst.”

“Kreativ sein ohne Angst, was falsch zu machen.”

“Macht Spaß, ist lustig und ausnahmslos jeder geht mit einem tollen, individuellen Bild nach Hause.”

“Wie ein kleiner Urlaub.”

“Auszeit mal anders.”

Sei am 12/05/2020 von 18:00 - 20:30 Uhr bei unserer Tänzerin Im Licht-ArtNight in Ulm dabei!

https://www.artnight.com/events/taenzerin-im-licht-am-12-05-2020-in-ulm-1026443/

Bei einer ArtNight erschaffst du dein eigenes Kunstwerk, unsere Künstler zeigen dir wie's geht. Nimm deine Freunde mit, komm alleine oder hab viel Spaß bei einem ArtNight-Date - du wirst kreativ, lernst neue Leute kennen und gehst am Ende nicht nur mit deinem eigenen Kunstwerk, sondern auch mit guter Laune und tollen Erinnerungen nach Hause!

Du brauchst keinerlei Vorkenntnisse und wir bringen alle Materialien mit, damit du deine ArtNight einfach genießen kannst. Getränke und Essen sind in der Location zu den herkömmlichen Preisen erhältlich.