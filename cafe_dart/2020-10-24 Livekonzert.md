---
id: "211475096647333"
title: Livekonzert
start: 2020-10-24 20:00
end: 2020-10-24 23:00
locationName: Café D'Art
address: Augsburgerstraße 35, 89231 Neu-Ulm
link: https://www.facebook.com/events/211475096647333/
image: 84167150_2870305793028715_7985048114044076032_o.jpg
isCrawled: true
---
