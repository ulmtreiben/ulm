---
name: Young and Queer Ulm e.V.
website: https://www.facebook.com/youngandqueerulm
email: youngandqueer@gmx.net
scrape:
  source: facebook
  options:
    page_id: youngandqueerulm
---
Die Jugendinitiative young and queer Ulm ist ein Treffpunkt für junge Menschen, die Liebe und Sexualität nicht mit Heterosexualität gleichsetzen. Willkommen sind alle Mitglieder und Befürworter der LSBTTIQ Gemeinschaft im Alter von 16-27 Jahren. Wir verbringen zusammen unsere Freizeit, veranstalten Infoabende zu queeren Themen und planen gemeinsame Aktionen, die auf die geschlechtliche und sexuelle Vielfalt in Ulm aufmerksam machen sollen.
