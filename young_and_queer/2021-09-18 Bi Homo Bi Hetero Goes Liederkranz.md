---
id: "2048655478644593"
title: Bi Homo Bi Hetero Goes Liederkranz
start: 2021-09-18 16:00
locationName: Liederkranz Ulm
address: Friedrichsau 9, 89073 Ulm
link: https://www.facebook.com/events/2048655478644593/
image: 241918843_1963195550525968_7345764093953275622_n.jpg
isCrawled: true
---
⚠️⚠️ SAVE THE DATE ⚠️⚠️

Auch wir sind am kommenden Samstag, 18.09. ab 16 Uhr im Liederkranz Ulm bei der Bi Homo Bi Hetero Party am Start! 🎉🏳️‍🌈✊ Schaut bei unserem Infostand vorbei, kommt mit uns ins Gespräch & genießt die erste bihomobihetero seit Ausbruch der Pandemie!

Weitere Infos:
☑️ Es wird eine Abendkasse geben
☑️ Zutritt nur mit 3G - Getestet/Geimpft/Genesen
☑️ Bei Regen fällt die Veranstaltung ggf. aus 

Wir freuen uns schon & danken @bihomo_bihetero für die Organisation & Kooperation! ❤️🌈