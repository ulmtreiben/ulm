---
id: "2736035703346827"
title: Workshop "Vielfalt von Geschlecht und sexueller Orientierung"
start: 2020-07-15 19:00
end: 2020-07-15 21:00
link: https://www.facebook.com/events/2736035703346827/
image: 106547747_2398857547080026_1278855311008165073_o.jpg
teaser: LSBTTIQA* – Was ist das eigentlich? Workshop zum Thema „Vielfalt von
  Geschlecht und sexueller Orientierung“   Ganz nach dem Motto „Solidarität
  jetzt!“
isCrawled: true
---
LSBTTIQA* – Was ist das eigentlich? Workshop zum Thema „Vielfalt von Geschlecht und sexueller Orientierung“ 

Ganz nach dem Motto „Solidarität jetzt!“ lädt das FclR in Kooperation mit Young and Queer Ulm e.V. zu einem spannenden Workshop zum Thema „Vielfalt von Geschlecht und sexueller Orientierung“ ein. Da Menschen aus der LGBTTIQA*-Community sowohl im privaten, als auch im öffentlichen Raum tagtäglich vielen Diskriminierungsformen ausgesetzt sind, zielt der Workshop darauf ab, ein breiteres Verständnis von sexueller und geschlechtlicher Vielfalt zu schaffen. 

Hierbei soll auch aufgezeigt werden, inwiefern Lebensformen außerhalb heteronormativer Vorstellungen auch heute noch gesellschaftlich sanktioniert werden. LGBTTIQA* - was ist das eigentlich? Diesen und weiteren Fragen werden sich die drei Referentinnen Julia Heinrich, Julia Gann und Sandra Nickel vom Verein Young and Queer Ulm in ihrem Workshop annähern. Im ersten Teil des Workshops werden die Teilnehmer*innen interaktiv für sexuelle, amouröse und geschlechtliche Vielfaltsdimensionen sensibilisiert. Dazu werden sowohl die unterschiedlichen Ebenen von Geschlecht vermittelt als auch grundlegende Begrifflichkeiten innerhalb der verschiedenen Vielfaltsdimensionen erklärt und voneinander abgegrenzt.  

Mit diesem Hintergrundwissen werden sich die Teilnehmer*innen im weiteren Verlauf des Workshops den Einfluss und die Macht von Sprache im Zusammenhang mit Sexualität und Sexismus erarbeiten. Im letzten Teil des Workshops wird ein Raum für (anonyme) Fragen und Diskussion gegeben sein, in dem die Teilnehmer*innen mit den Referentinnen über Workshopinhalte sowie persönliche (Diskriminierungs-)Erfahrungen ins Gespräch kommen können. Wir möchten versuchen, den Workshop trotz des coronabedingten Onlineformats so interaktiv wie möglich gestalten. Natürlich ist das Ganze in Real Life sehr viel leichter und persönlicher umzusetzen, aber wir geben unser Bestes und freuen uns jetzt schon über alle interessierten Menschen! 

Das Ganze findet statt am Mittwoch, den 15.07.2020, um 19 Uhr online via Zoom. Das ist der Teilnahmelink: https://us02web.zoom.us/j/88214709746. Schaltet euch einfach ab kurz vor 19 Uhr dazu!

[EN]: LSBTTIQA* - What is that actually? Workshop on the topic "Diversity of gender and sexual orientation" 

According to the motto "Solidarity now!" the FclR in cooperation with Young and Queer Ulm e.V. invites to an exciting workshop on the topic "Diversity of gender and sexual orientation". As people from the LGBTTIQA* community are exposed to many forms of discrimination in both the private and public sphere on a daily basis, the workshop aims to create a broader understanding of sexual and gender diversity. 

It will also be shown to what extent forms of life outside heteronormative conceptions are still socially sanctioned today. LGBTTIQA* - what is that actually? The three speakers Julia Heinrich, Julia Gann and Sandra Nickel from the association Young and Queer Ulm will address these and other questions in their workshop. In the first part of the workshop the participants will be interactively sensitized for sexual, amorous and gender diversity dimensions. The different levels of gender will be conveyed as well as basic concepts within the different dimensions of diversity will be explained and differentiated from each other.  

With this background knowledge the participants will work on the influence and power of language in connection with sexuality and sexism. In the last part of the workshop there will be a space for (anonymous) questions and discussions, in which the participants can talk to the speakers about workshop contents and personal (discrimination) experiences. We would like to try to make the workshop as interactive as possible despite the corona-related online format. Of course, the whole thing is much easier and more personal to implement in real life, but we give our best and are already looking forward to all interested people! 

The lecture (which will be hold in german language) will take place on Wednesday, 15.07.2020 at 7 pm online via Zoom. Link for participation: https://us02web.zoom.us/j/88214709746, just sign yourself in at 19 o´clock!
