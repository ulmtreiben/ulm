---
id: "2765964863716726"
title: Online LGBTQIA+ Jugendgruppe
start: 2021-03-15 19:00
end: 2021-03-15 22:00
link: https://www.facebook.com/events/2765964863716726/
image: 158881210_1818252335020291_7244085811188561050_o.jpg
teaser: Wir gehen online! 🏳️‍🌈🎉🖥️ Am kommenden Montag, 15.03. möchten wir ab 19
  Uhr unsere erste online Jugendgruppe starten! Als flexible Altersgrenzen h
isCrawled: true
---
Wir gehen online! 🏳️‍🌈🎉🖥️ Am kommenden Montag, 15.03. möchten wir ab 19 Uhr unsere erste online Jugendgruppe starten! Als flexible Altersgrenzen haben wir dafür mal 14 - 27 Jahre festgesetzt, allerdings sehen wir das nicht so eng - wichtig ist uns vor allem, dass sich alle wohlfühlen. Da wir aktuell keine feste Gruppe haben, ist das Treffen am Montag eine super Gelegenheit für euch, ganz entspannt andere queere Jugendliche kennenzulernen und sich auszutauschen. Da wir selbst noch gar nicht so genau wissen, wie wir die Gruppe so gestalten wollen, seid ihr auch herzlich dazu eingeladen, eigene Wünsche, Ideen und Anregungen mitzubringen 💭⚡. Den Zugangslink gibt's auf Anfrage bei uns - dropt dafür einfach eine DM hier auf Facebook, auf Instagram oder via Mail! 📬 Wir freuen uns schon! 🏳️‍🌈❤️