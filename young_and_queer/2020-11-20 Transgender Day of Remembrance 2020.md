---
id: "1042986019495438"
title: Transgender Day of Remembrance 2020
start: 2020-11-20 18:00
end: 2020-11-20 19:30
link: https://www.facebook.com/events/1042986019495438/
image: 125017219_1723083204537205_888521712794200110_n.jpg
teaser: Trotz der prekären Lage um Covid -19 ist es unabdingbar, die trans* Community
  auch zum diesjährigen Transgender Day of Remembrance (TDoR) am 20.11.202
isCrawled: true
---
Trotz der prekären Lage um Covid -19 ist es unabdingbar, die trans* Community auch zum diesjährigen Transgender Day of Remembrance (TDoR) am 20.11.2020 sichtbar zu machen, zu empowern und allen Opfern von trans*feindlich motivierter Gewalt zu gedenken ⚧
Um mögliche Infektionen zu vermeiden, haben wir uns dieses Jahr dazu entschieden, den Aktionstag in einem online Format umzusetzen. Wir möchten ab 18 Uhr über Zoom zunächst einen kleinen Input darüber geben, was der TDoR genau ist und warum der Tag weltweit eine hohe Bedeutung für die trans* Community hat. Anschließend werden wir in Gedenken aller Opfer trans*feindlicher Gewalt eine Schweigeminute einlegen. Alle Teilnehmer*innen sind dazu eingeladen, vor dem Bildschirm symbolisch eine Kerze oder ein Teelicht anzuzünden🕯️
Für jede brennende Flamme werden wir einen Beitrag an die Menschenrechtsorganisation Transgender Europe (TGEU) spenden. 