---
id: "486828622579059"
title: Die Macht von Sprache – Gesellschaftliche Relevanz geschlechtergerechter
  Sprache
start: 2021-06-28 19:00
link: https://www.facebook.com/events/486828622579059/
image: 200738362_2660894264209685_2995052447776465177_n.jpg
isCrawled: true
---
 📅 28.06.21 ⌚ 19:00 📌 Online 💬 deutsch

Sprache schafft Wirklichkeit und kann sich je nach Verwendung positiv oder negativ auf queere Lebensrealitäten auswirken. Im Vortrag möchten wir euch zunächst aus einer (neuro-)psychologischen Perspektive erklären, warum eine geschlechtergerechte Sprache wichtig ist. Anschließend zeigen wir auf, dass geschlechtergerechte Sprache als Maßnahme zur Sichtbarmachung und Gleichstellung aller Geschlechter verstanden werden muss und keineswegs an einem binären Geschlechterverständnis festhalten sollte. Dabei gehen wir auch auf hetero- bzw. cisnormative Machtstrukturen in unserer Gesellschaft ein, die durch genderexklusiven Sprachgebrauch Ausschlüsse von queeren Menschen reproduzieren.  Zudem möchten wir eine Übersicht über gängige Verwendungsmöglichkeiten geschlechtergerechter Sprache geben und diskutieren, wie geschlechterbezogene Ungleichverhältnisse durch eine geschlechtergerechte Sprache dekonstruiert werden können.  

Der Input ist theorielastig und wird in deutscher Sprache angeboten. Es wird zwischendurch und abschließend ein Raum für Fragen und Diskussion geben.  

Die Veranstaltung findet in Kooperation mit Young & Queer Ulm e.V. statt.

Der Link zur Veranstaltung: https://us02web.zoom.us/j/87020889137

#youngandqueerulm #fclr2021ulm
__________________________________________

 The power of language – The relevance of gender-inclusive language for society

📅 28.06.21 ⌚ 7pm 📌 Online 💬 German

 Language creates reality and depending on how it is used it can influence queer people´s lives positively or negatively.

In this talk we will explain why gender-inclusive language is important from a (neuro-)psychological perspective. Afterwards we will demonstrate that gender-inclusive language must be understood as a measure that strives for the visibility and equality of all genders and that exceeds a binary understanding of gender. We will also talk about hetero-and cisnormative power structures in our society that reproduce the exclusion of queer people through gender-exclusive language. In the end we want to give an overview of common options for using gender-inclusive language and discuss how gender inequalities can be deconstructed through gender-inclusive language.

This talk contains a lot of theory and will be held in German. In between and in the end there will be time for questions and discussion. 

In cooperation with: Young & Queer Ulm e.V.