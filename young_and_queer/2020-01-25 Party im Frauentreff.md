---
id: "163697898374745"
title: Party im Frauentreff
start: 2020-01-25 22:00
end: 2020-01-26 01:00
locationName: Frauentreff Ulm e.V.
address: Hinter dem Brot 9, 89073 Ulm
link: https://www.facebook.com/events/163697898374745/
image: 83408009_1459082894270572_4443772489578840064_n.jpg
teaser: Hallo zusammen,   zum kommenden Samstag (25.01.) lädt der Ulmer Frauentreff
  wieder zur Frauendisko in ihren Räumen und wir werden gemeinsam als Gruppe
isCrawled: true
---
Hallo zusammen, 

zum kommenden Samstag (25.01.) lädt der Ulmer Frauentreff wieder zur Frauendisko in ihren Räumen und wir werden gemeinsam als Gruppe hingehen! :-)  Die Veranstaltung richtet sich an alle Frauen ab 18 Jahren, der Eintritt ist frei. Offiziell startet das Ganze schon um 20 Uhr, wir werden aber erst später hingehen.

Falls ihr nicht alleine hingehen wollt und Anschluss sucht, schreibt uns gerne vorher! 

Wir freuen uns auf euch! :-) 