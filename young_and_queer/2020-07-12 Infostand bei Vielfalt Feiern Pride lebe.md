---
id: "2620508128215350"
title: Infostand bei Vielfalt Feiern Pride leben
start: 2020-07-12 15:00
end: 2020-07-12 18:00
locationName: Münsterplatz Ulm
address: Münsterplatz 21, 89073 Ulm
link: https://www.facebook.com/events/2620508128215350/
image: 107376457_4165519640187261_3622353433320258076_o.jpg
teaser: "Wir beteiligen uns an der Aktion von Young and Queer Ulm am Sonntag mit einem
  Infostand.  Hier der Aufruf zur Aktion:  Liebe Community,  aufgrund von"
isCrawled: true
---
Wir beteiligen uns an der Aktion von Young and Queer Ulm am Sonntag mit einem Infostand. 
Hier der Aufruf zur Aktion: 
Liebe Community, 
aufgrund von Corona haben sich für uns alle doch ganz neue Situationen ergeben. Wir von Young and Queer Ulm e.V. laden deshalb gemeinsam Vertretern der Stadt Ulm am 12.07.20 von 15-18 Uhr auf eine Kundgebung auf demMünsterplatz ein. Im Anhang findet ihr eine ausführliche Einladung. Die wichtigsten Punkte im folgenden:
 - Hygienekonzept: Mindestabstand von 1.5 Meter, Mund-Nasen-Schutz unbedingt (!) mitbringen
- es wird ein buntes Rahmen
programm mit Infoständen, Reden und Musik geben
- wir freuen uns über jede Regenbogenflagge oder jedes sichtbare Zeichen für die Community (gern auch Schilder!)
- kommt zahlreich und setzt ein Zeichen für queere Lebenswelten und Sichtbarkeit in Ulm 
