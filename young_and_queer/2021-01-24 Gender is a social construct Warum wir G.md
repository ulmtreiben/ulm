---
id: "2975241902758450"
title: "Gender is a social construct: Warum wir Geschlecht als binäre Kategorie bei
  der Formulierung von Nachhaltigkeitszielen überdenken müssen."
start: 2021-01-24 19:00
end: 2021-01-24 20:30
link: https://www.facebook.com/events/2975241902758450/
image: 140692413_1779579842220874_7659263705738475309_o.jpg
teaser: 🏳️‍🌈🌱 Nachhaltigkeit und Gender? Ihr fragt euch, wie das zusammenpasst?
  Dann kommt zu unserem online Vortrag! ✊🏳️‍🌈 Am kommenden Sonntag, 24.01.2
isCrawled: true
---
🏳️‍🌈🌱 Nachhaltigkeit und Gender? Ihr fragt euch, wie das zusammenpasst? Dann kommt zu unserem online Vortrag! ✊🏳️‍🌈 Am kommenden Sonntag, 24.01.2020 geben wir ab 19 Uhr im Rahmen der Veranstaungsreihe "17 Ziele für Ulm und die Welt" einen online Input zu Nachhaltigkeitsziel 5 "Geschlechtergleichheit". Im Vortrag werden wir zunächst eine Einführung in dieses Nachhaltigkeitsziel geben und anschließend unter Berücksichtigung unseres queeren Geschlechtsverständnisses diskutieren, warum das Nachhaltigkeitsziel über eine formale Gleichstellung von Frauen und Männern hinausgehen sollte.

Julia Heinrich, Sandra Nickel, Julia Gann | Young & Queer Ulm e.V.

Die Veranstaltung ist Teil der Ausstellung "17 Ziele für Ulm und die Welt".
In der Ausstellung „17 Ziele für Ulm und die Welt“ geben Ulmer Organisationen und Gruppen auf anschauliche Weise Antworten, präsentieren sich und ihre Aktivitäten. Zeitgleich ist die Wanderausstellung „Konsum-Kompass“ der Deutschen Bundesstiftung Umwelt zu sehen. Denn wer existiert, der konsumiert, und der Konsumkompass zeigt, worauf wir als Verbraucherinnen und Verbraucher achten können.
Mehr dazu auf: https://www.ideenwerkstatt-ulm.de/
Der Konsumkompass wurde von der Deutschen Bundesstiftung Umwelt und dem Umweltbundesamt erstellt und ist im Besitz des AK Eine Welt Nordhorn.