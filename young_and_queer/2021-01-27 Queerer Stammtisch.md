---
id: "424218618629386"
title: Queerer Stammtisch
start: 2021-01-27 19:00
end: 2021-01-27 22:00
link: https://www.facebook.com/events/424218618629386/
image: 140731351_1779552232223635_8103097035078525739_o.jpg
teaser: Auch diesen Monat wollen wir wieder einen Queeren Online-Stammtisch
  veranstalten. Ihr könnt es euch einfach mit einem Getränk eurer Wahl zu Hause
  gemü
isCrawled: true
---
Auch diesen Monat wollen wir wieder einen Queeren Online-Stammtisch veranstalten. Ihr könnt es euch einfach mit einem Getränk eurer Wahl zu Hause gemütlich machen und so können wir auch Online einen schönen Abend gemeinsam verbringen und uns über verschiedene Themen austauschen.

Wir freuen uns auf Euch! 