---
name: ejw cvjm Ulm / Café JAM
website: https://www.cvjm-jugendwerk-ulm.de
email: info@ejw-ulm.de
scrape:
  source: facebook
  options:
    page_id: ejw.cvjm.Ulm
---
Unser Leitmotiv: Wir wollen gemeinsam mit jungen Menschen Leben gestalten und Glauben erleben, weil uns Gottes Liebe Vertrauen in das Leben schenkt.
