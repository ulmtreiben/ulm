---
id: "842203039677986"
title: ULMER DENKANSTÖSSE 2021
start: 2021-03-12 14:00
end: 2021-03-12 20:00
locationName: Stadthaus Ulm
address: Ulm
link: https://www.facebook.com/events/842203039677986/
image: 150865555_1291244234603421_6647407404210482776_o.jpg
teaser: Demokratie Auslauf- oder Zukunftsmodell?  Demokratie im Stresstest –
  Anfeindungen von innen und außen, Fragmentierung der Gesellschaft,
  Internetblasen
isCrawled: true
---
Demokratie
Auslauf- oder Zukunftsmodell?

Demokratie im Stresstest – Anfeindungen von innen und außen, Fragmentierung der Gesellschaft, Internetblasen, hate speech, fake news, Populismus, Lobbyismus, Krise der Repräsentation durch Parteien und Abgeordnete, Entfremdung großer Gruppen vom politischen System des Parlamentarismus … Die Liste der Herausforderungen ist lang und alle etablierten Demokratiemodelle weltweit finden sich im selben Bedrohungsszenario wieder. Selbst die älteste moderne Demokratie in den USA scheint in Gefahr. Wird unsere Demokratie dem Druck standhalten? An Vorschlägen für eine Demokratie der Zukunft fehlt es nicht, neue Formen der Partizipation wie Bürgerräte oder urban citizenship wollen der Krise begegnen, um Spaltungen in der Gesellschaft zu überwinden und konsensfähige Lösungen für brennende Probleme zu finden. Wie kann Demokratie gelebt und erneuert werden? Wie die Herausforderungen so annehmen, dass die demokratische Lebensform als das Ideal eines gemeinsamen und doch selbstbestimmten Handelns auch im 21. Jahrhundert weiterhin gelingt? Es steht viel auf dem Spiel: Scheitert die Demokratie, scheitert auch das Projekt der Freiheit des Menschen in politischer Gemeinschaft.


Aufgrund der aktuellen Corona-Situation werden die einzelnen Programmpunkte der Denkanstöße online als Livestream angeboten.
Stream unter: https://www.ulmer-denkanstoesse.de/



Programmübersicht

Donnerstag, 11. März 2021:
Eröffnung der Ulmer Denkanstöße 2021

- Gespräch zwischen Gunter Czisch, Oberbürgermeister Stadt Ulm, Martin Hettich, Vorstandsvorsitzender der Sparda-Bank Baden-Württemberg eG und
Prof. Dr. Michael Weber, Präsident der Universität Ulm
- Einführug: Prof. Dr. Dr. h.c. Renate Breuniger
- Eröffnungsvortrag: Diana Kinnert
- Musikalischer Ausklang


Freitag, 12. März 2021:
Demokratie heute

- Impulsreferate und Diskussionsrunde mit Prof. Dr. Hans-Martin Schönherr-Mann , Albrecht von Lucke und Rezzo Schlauch (14-16:30 Uhr)
- Lesung - Navid Kermani "Morgen ist da. Reden"  (17:00 Uhr)
- Vortrag von Christoph Sonntag (19:00 Uhr)


Samstag, 13. März 2021:
Demokratie im Wandel

- Impulsreferate und Diskussionsrunde mit Christoph Kober und Erik Albrecht (Dr. Sarah Schilliger kann leider krankheitsbedingt nicht dabei sein) (14-16:30 Uhr)
- Abschlussvortrag & Scheckübergabe: Prof. Dr. Paul Kirchhof (17:00 - 18:30 Uhr)