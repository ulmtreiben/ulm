---
id: "689697358299840"
title: Stadtteilspaziergänge am Eselsberg
start: 2020-10-10 15:00
end: 2020-10-10 17:00
address: Quartierszentrale Stifterweg 98, 89075 Ulm, Eselsberg
link: https://www.facebook.com/events/689697358299840/
image: 119973420_2013117765487306_3575785476139676567_o.jpg
teaser: Besuch in der „essbaren Stadt“ -  Die Stadt lädt ein zu drei
  Stadtteilspaziergängen am Eselsberg   Intelligente Hochbeete, die zum
  gemeinschaftlichen
isCrawled: true
---
Besuch in der „essbaren Stadt“ - 
Die Stadt lädt ein zu drei Stadtteilspaziergängen am Eselsberg 

Intelligente Hochbeete, die zum gemeinschaftlichen Gärtnern einladen, kostenlose Äpfel an den Bäumen und moderne Photovoltaik-Anlagen an Balkonen: Bei drei geführten Stadtteilspaziergängen kann man sich am Eselsberg über die Bereiche Urban Gardening, Essbare Stadt und die effektive Nutzung von Sonnenenergie informieren, die im Rahmen des städtischen Projekts Zukunftskommune@bw umgesetzt wurden. 

Los geht es jeweils am Samstag, 26. September, um 15 Uhr, am Donnerstag, 1. Oktober, um 16.30 Uhr sowie am Samstag, 10. Oktober, um 15 Uhr. Die beiden großen Runden an den Samstagen dauern etwa zwei Stunden, die kleinere Runde am 1. Oktober rund 60 Minuten. Geführt werden die Touren, die jeweils an der Quartierszentrale im Stifterweg 98 starten, von Kai Weinmüller vom Ulmer Initiativkreis nachhaltige Wirtschaftsentwicklung e.V..

Wer die Stationen selbst erkunden möchte, der findet ab Anfang Oktober auf wetterbeständigen Hinweisschildern an den einzelnen Stationen wichtige Hintergrundinformationen. Faltbroschüren mit den Routen und Beschreibungen liegen unter anderem in der Quartierszentrale im Stifterweg 98 und in der Stadtteilbibliothek Eselsberg aus. 

Im Landesförderprojekt Zukunftskommune@bw werden seit zwei Jahren unter der Leitung der Geschäftsstelle Digitale Agenda gemeinsam mit der Bürgerschaft digitale Lösungen für das Quartier Alter Eselsberg entwickelt und realisiert. 
Weitere Informationen erhält man unter www.zukunftsstadt-ulm.de/eselsberg 
