---
id: "715897652677277"
title: Klimawandel als globale Herausforderung – eine kritische venezolanische
  Perspektive
start: 2020-12-15 18:00
link: https://www.facebook.com/events/715897652677277/
image: 129407678_3499912963463706_5439392574605629225_o.jpg
teaser: Alejandro Ceballos | Kollektiv Epatu Konuko  Der Vortrag wirft einen
  kritischen Blick auf die Auswirkungen des Klimawandels in Venezuela, die
  öffentli
isCrawled: true
---
Alejandro Ceballos | Kollektiv Epatu Konuko

Der Vortrag wirft einen kritischen Blick auf die Auswirkungen des Klimawandels in Venezuela, die öffentliche Meinung zur Klimafrage, das Handeln und Nichthandeln der Regierung und die Grassroot-Bewegungen gegen den Klimawandel, die sich aus der turbulenten politischen, wirtschaftlichen und sozialen Lage ergeben. 

https://uni-ulm.webex.com/uni-ulm-de/j.php?MTID=m959176a0e5205e1359da28393ce5f802

Passwort: KBpNMPMC378

Die Veranstaltung ist Teil der Ausstellung "17 Ziele für Ulm und die Welt".
In der Ausstellung „17 Ziele für Ulm und die Welt“ geben Ulmer Organisationen und Gruppen auf anschauliche Weise Antworten, präsentieren sich und ihre Aktivitäten. Zeitgleich ist die Wanderausstellung „Konsum-Kompass“ der Deutschen Bundesstiftung Umwelt zu sehen. Denn wer existiert, der konsumiert, und der Konsumkompass zeigt, worauf wir als Verbraucherinnen und Verbraucher achten können.
Mehr dazu auf: https://www.ideenwerkstatt-ulm.de/
Der Konsumkompass wurde von der Deutschen Bundesstiftung Umwelt und dem Umweltbundesamt erstellt und ist im Besitz des AK Eine Welt Nordhorn.