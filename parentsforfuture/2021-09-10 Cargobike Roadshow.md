---
id: "218903556956226"
title: Cargobike Roadshow
start: 2021-09-10 13:00
end: 2021-09-10 18:00
locationName: Münsterplatz
address: Ulm
link: https://www.facebook.com/events/218903556956226/
image: 241621695_2316720871793659_1241962867278859268_n.jpg
isCrawled: true
---
Schon mal E-Lastenrad gefahren? Probiere es aus! Auf dem Münsterplatz in Ulm habt ihr am 10.09.21 von 13 bis 18 Uhr die Möglichkeit, 12 verschiedene E-Lastenradtypen im Rahmen der Cargobike-Roadshow kostenlos Probe zu fahren. 
Ob für den Großeinkauf, den Weg zur Kita oder den Handwerksbetrieb - das Roadshow Team hilft dabei, das passende Modell zu finden und berät hersteller- und händlerneutral. Also, vorbeikommen und testradeln!
