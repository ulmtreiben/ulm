---
id: "194116278958019"
title: "Vortrag Divest - Thema: Nachhaltige Geldanlagen"
start: 2020-12-02 18:30
link: https://www.facebook.com/events/194116278958019/
image: 128431916_2793134860898798_1880955124384978817_o.jpg
teaser: Geld regiert die Welt - und das können wir auch positiv nutzen! Am
  Mittwochabend erklärt uns Divest, wie man Geld am Besten nachhaltig
  investieren kan
isCrawled: true
---
Geld regiert die Welt - und das können wir auch positiv nutzen!
Am Mittwochabend erklärt uns Divest, wie man Geld am Besten nachhaltig investieren kann und was man dabei beachten sollte. 
Der Vortrag findet online auf Zoom über folgenden Link statt: 

https://fffutu.re/egbHWY

Wir freuen uns auf euch! :) 