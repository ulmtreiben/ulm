---
id: "385708939228999"
title: Mahnwache "Divest" am Weltspartag
start: 2020-10-30 17:30
locationName: Hans-und-Sophie-Scholl-Platz
address: 89073 Ulm
link: https://www.facebook.com/events/385708939228999/
image: 122459117_189454759321316_8244874917312572302_n.jpg
teaser: Mahnwache am Weltspartag - veranstaltet von 'Divest Ulm'.  Eine Investition,
  die das Klima zerstört, ist keine Investition!  Wofür sparst du?  Wir set
isCrawled: true
---
Mahnwache am Weltspartag - veranstaltet von 'Divest Ulm'.

Eine Investition, die das Klima zerstört, ist keine Investition!

Wofür sparst du?

Wir setzen uns für Divestment, also die Umschichtung von Geld in sinnvolle, zukunftsweisende Anlageformen ein - und das in Ulm und der Region.

Wir wenden uns an Städte, Institutionen, Vereine, Kirchen, Banken, Versicherungen und Privatpersonen.

Mach dir am Weltspartag Gedanken darüber, was dein Geld bewirkt und was du verändern kannst.

Hauptorganisation: divest ulm (www.divest-ulm.de , kontakt@divest-ulm.de)

Unterstützt vom "Ulmer Netz". U.a. Fridays For Future Ulm/Neu-Ulm, BUND Ulm, Lokale Agenda, Parents For Future Ulm/Neu-Ulm, Alb-Donau, Gemeinwohlökonomie Ulm, Extinction Rebellion Ulm, ADFC Ulm, Greenpeace Ulm

BITTE TEELICHTER MITBRINGEN UND DIE AKTUELLEN AHA-REGELN BEACHTEN!

#divestulm #divestment