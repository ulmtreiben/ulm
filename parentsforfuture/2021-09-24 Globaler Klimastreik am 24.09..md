---
id: "394530562249936"
title: Globaler Klimastreik am 24.09.
start: 2021-09-24 15:00
address: Münsterplatz, 89073 Ulm
link: https://www.facebook.com/events/394530562249936/
image: 241677642_2992161364329479_1505406908501664790_n.jpg
isCrawled: true
---
Der nächste global strike day steht an!
Am 24.09. gehen wir wieder weltweit auf die Straßen, auch hier in Ulm.
Wir treffen uns um 15 Uhr auf dem Münsterplatz.