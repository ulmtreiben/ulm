---
name: Parents For Future Ulm
website: https://ulm.parentsforfuture.de/
scrape:
  source: facebook
  options:
    page_id: parentsforfutureulm
---
Wir sind ein freier Zusammenschluss von erwachsenen Menschen und stehen als Parents For Future in Solidarität zur Fridays For Future-Bewegung. Wir unterstützen die jungen Menschen in ihrem Kampf für einen ambitionierten Klimaschutz in Deutschland und weltweit Die Klimakrise ist eine reale Bedrohung für die menschliche Zivilisation Die Bewältigung der Klimakrise ist die Hauptaufgabe des 21. Jahrhunderts. Wir fordern eine Politik, die dieser Aufgabe gerecht wird. Fridays for Future: Das sind alle, die für unser Klima auf die Straße gehen. Die Klimastreik-Bewegung ist international, überparteilich, autonom und dezentral organisiert. Mach mit und werde Teil unserer Bewegung!
