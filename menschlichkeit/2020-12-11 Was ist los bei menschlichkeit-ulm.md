---
id: "387893945763916"
title: Was ist los bei menschlichkeit-ulm?
start: 2020-12-11 19:00
end: 2020-12-11 20:00
link: https://www.facebook.com/events/387893945763916/
image: 130536816_4008878689151361_5309245777559702737_n.jpg
teaser: Wir möchten auf das vergangene Jahr zurückblicken.  Gemeinsam mit euch wollen
  wir auch überlegen, was wir im Jahr 2021 auf die Beine stellen wollen/kö
isCrawled: true
---
Wir möchten auf das vergangene Jahr zurückblicken. 
Gemeinsam mit euch wollen wir auch überlegen, was wir im Jahr 2021 auf die Beine stellen wollen/können. 
Das Ganze wird online in Microsoft Teams stattfinden. 

Wenn ihr teilnehmen möchtet, schreibt uns eine —> Nachricht im Messenger und wir schicken euch den Link für Microsoft Teams. 

Wir freuen uns auf euch und auf den Austausch mit euch.