---
name: Menschlichkeit-Ulm e.V.
website: https://menschlichkeit-ulm.de
email: menschlichkeit-ulm@outlook.de
scrape:
  source: facebook
  options:
    page_id: menschlichkeitulm
---
Menschlichkeit-Ulm e.V. ist ein eingetragener gemeinnütziger, mildtätiger und überparteilicher Verein mit dem Sitz in Ulm. Die Ziele des Vereins sind die Förderung der Hilfe für politisch, rassisch oder religiös Verfolgte sowie Hilfe für Flüchtlinge und Asylbewerber/innen, die Förderung des Völkerverständigungsgedankens sowie mildtätige Zwecke. Zum einen möchten wir die Erstaufnahme der Flüchtlinge unterstützen und durch unsere Arbeit eine Willkommenskultur schaffen. Außerdem möchten wir beispielsweise durch die Koordination von Sachspenden versuchen, Lebensbedingungen aktiv verbessern. Zum anderen möchte wir Integration vorantreiben und versuchen Kontakt zu Ulmer Bürgern/Vereinen durch gemeinsame Veranstaltungen herzustellen, um Verständnis und ein Miteinander zu schaffen. Hand in Hand möchten wir Menschlichkeit ganz praktisch leben!
