---
id: "1263335634015744"
title: Friedensfest (Ulmer Friedenswochen)
start: 2020-09-26 14:00
end: 2020-09-26 18:30
locationName: Fort Unterer Kuhberg
address: 10  Unterer Kuh berg, 89077 Ulm
link: https://www.facebook.com/events/1263335634015744/
teaser: "Bitte beachten: ‏Aufgrund der Wettervorhersage wird die Programmdauer auf
  14-18:30 Uhr verkürzt.   Hallo ihr Lieben,   Unsere Freunde (DIDF-Ulm und N"
isCrawled: true
---

Bitte beachten: ‏Aufgrund der Wettervorhersage wird die Programmdauer auf 14-18:30 Uhr verkürzt. 

Hallo ihr Lieben,
 
Unsere Freunde (DIDF-Ulm und NaturFreunde-Ulm) veranstalten am 26.09.2020 das Friedensfest im Rahmen der Ulmer Friedenswochen am Unteren Kuhberg.
Das Friedensfest beinhaltet verschiedene Programme:
‪Ab 12:00 Uhr gibt es Kulinarisches aus verschiedenen Nationen, Kuchen und Kaffee.‬
‪Ab 14:00 Uhr steht ein Programm für Kinder mit malen, Bbasteln, Kinderlieder singen und schminken.‬
Etwas ganz tolles für die Kinder:
Ein Flohmarkt, auf dem die Kinder ihre Sachen kaufen oder verkaufen können (für das Verkaufen auf dem Flohmarkt ist eine Anmeldung erforderlich).
Kindertheater „Mizzi auf großer Fahrt“
‪Ab 16:00 Uhr gibt es Live-Musik von verschiedenen Bands.‬
Wir als menschlichkeit- ulm e.V. machen mit und haben dort einen Infostand.
Einen Kreativitätsstand bieten wir auch an - es wird dort voraussichtlich Damen geben, die Henna anbieten sowie Frauen, die Zöpfe flechten.
Wir würden uns total freuen, wenn ihr dabei seid.


Wann: ‪am 26.09.2020‬ ab ‪12 – 22 Uhr‬
Adresse: Fort Unterer Kuhberg

Aufgrund der aktuellen Pandemie möchten wir euch bitten, beim Einlass/Auslass und auf dem Weg zum und vom Gästeplatz, bzw. zur und von der Bühne o.ä. einen Mund-Nasen-Schutz zu tragen.
Jeder Gast wird von den Veranstaltern aufgefordert, Vorname, Nachname und Telefonkontakt anzugeben. Die aufgenommenen Daten werden von den Veranstaltern vertraulich behandelt und 3 Wochen aufgehoben.
