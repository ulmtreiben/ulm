---
id: "1150578222095415"
title: Humanität und (Menschen-) Rechte außer Kraft? Über deutsche und europäische
  Flüchtlingspolitik
start: 2021-07-11 11:00
end: 2021-07-11 13:30
address: Roxy Ulm, Schillerstraße 1/12, 89077 Ulm
link: https://www.facebook.com/events/1150578222095415/
image: 207443183_4592203757485515_2293072096384772722_n.jpg
isCrawled: true
---
Bundestagskandidatinnen und -kandidaten im Gespräch

Allerweltsfest-Matinee am Sonntag, 11. Juli, 11 Uhr, ROXY Ulm

Am 20. März 2021 jährte sich zum fünften Mal das Inkrafttreten des EU-Türkei-Abkommens mit dem Ziel, die Überfahrten Geflüchteter über die Ägäis zu verhindern. Zur gleichen Zeit entstanden u. a. auf den griechischen Inseln Lagerkomplexe, die schnell überfüllt waren und in denen bis heute menschenunwürdige Zustände
herrschen. Das tägliche Sterben von Menschen im Mittelmeer und die Zustände in den Lagern an den Außengrenzen der EU sind aber nicht nur eine humanitäre Katastrophe, sondern ganz wesentlich ein politischer und juristischer Skandal. Sie
sind das Ergebnis der europäischen – und damit auch deutschen – Flüchtlingspolitik.

Die Pushbacks der europäischen Agentur Frontex sprechen hier eine mehr als deutliche Sprache.
Am 26. September sind Bundestagswahlen und wir haben die Kandidatinnen und
Kandidaten der Wahlkreise Ulm und Neu-Ulm zu einer Diskussionsrunde eingeladen.
Als breites Bündnis zivilgesellschaftlicher Organisationen möchten wir ihre ganz
persönlichen Positionen und Haltungen in Bezug auf die europäische und deutsche Flüchtlingspolitik erfahren und diskutieren.

Zusätzlich haben wir zwei
Experten eingeladen: Maximilian Pichl, Jurist und Verfasser der Studie „Der
Moria-Komplex“, sowie Dr. Christian Bialas, Chefarzt an der Neu-Ulmer Donauklinik. Christian Bialas unterstützt
seit 2015 das Flüchtlingslager
Moria / Kara Tepe auf Lesbos mit humanitären Mitteln.

Die Veranstaltung hat ein offenes Ende, so dass nach dem offiziellen Ende der
Gesprächsrunde noch Gelegenheit zu weiterem Austausch mit den Kandidatinnen und Kandidaten und untereinander besteht.

Wir, das Ulmer Netz für eine andere Welt e. V., das Forum Asyl und Menschenrechte und das ROXY Ulm,, freuen uns, Sie zu dieser Matinee begrüßen zu dürfen. Sie ist ein anderes Format des jährlichen Allerweltsfests, das in diesem Jahr aufgrund der
Corona-Bedingungen nicht in der gewohnten Form stattfinden kann.