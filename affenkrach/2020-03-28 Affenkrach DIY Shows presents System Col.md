---
id: "2471794656408488"
title: "Ak-Diy Shows presents: System Collapse / The Rancors / Old Runz"
start: 2020-03-28 17:30
end: 2020-03-29 01:00
address: Anarres, Mähringerweg 75, 89075 Ulm
link: https://www.facebook.com/events/2471794656408488/
image: 87478409_2594753127449244_2010877291118198784_n.jpg
teaser: 'Und erneut bekommt ihr ein grandiosen Eintopf der
  menschnverachtenden  Untergrundmusik aufgetischt!!!♥️   18 UHR: Kollektiv26.
  präsentiert:  Vortrag "'
isCrawled: true
---
Und erneut bekommt ihr ein grandiosen Eintopf der menschnverachtenden  Untergrundmusik aufgetischt!!!♥️
 
18 UHR: Kollektiv26. präsentiert:
 Vortrag "Besetzt mal wieder"

Affenkrach DIY Shows präsentiert:

ab 20:30/21 Uhr

The Rancors 
< Hardcore Punk, München>
(https://rancors.bandcamp.com/)

System Collapse 
< Ex Per Capita DBeat Imferno, München>
(FIRST SHOW EVER!!!!)

Old Runznickels
< Melodic Anarcho Punk >
(https://oldrunznickels.bandcamp.com/)


Doors: 17:30

Küfa: Tba
Entry: wie immer Spendenempfehlung oder gib was du kannst


No Animals! 
No Assholes!
No Discrimination!




Support Your Local AffenCrew
