---
id: "554156292180924"
title: Lesung mit Gérard Scappini
start: 2020-03-18 19:00
end: 2020-03-18 21:00
locationName: Kulturbuchhandlung Jastram
address: Schuhhausgasse 8, 89073 Ulm
link: https://www.facebook.com/events/554156292180924/
teaser: 'Gérard Scappini liest aus "Ungeteerte Straßen" - Band 1 der Reihe um den
  Lebensweg von Pascal.  *** Eintritt: 5,00 EUR  *** In Scappinis Gedichten erl'
isCrawled: true
---
Gérard Scappini liest aus "Ungeteerte Straßen" - Band 1 der Reihe um den Lebensweg von Pascal.

***
Eintritt: 5,00 EUR

***
In Scappinis Gedichten erlebt der Leser die Welt durch die Augen des Jungen Pascal, der in den 1950er Jahren in Frankreich aufwächst. Neben den Freuden der Kindheit wird auch deutlich, wie Pascal Armut und das konfliktbeladene Verhältnis der Eltern bewältigt.

»Gérard Scappinis Gedichte sind assoziativ und gleichzeitig direkt. Ihr Sujet ist nicht Kindheit an sich, sondern, viel unmittelbarer, es sind schwingende Erinnerungen ans Kind-Sein, ein stroboskopisches Erleben, garniert mit Wahrnehmungen, die anschaulich von einer bestimmten Zeit erzählen. Keiner der Texte handelt von einem Später: Es sind ausschließlich heraufbeschworene Fragmente eines einstigen Jetzt.« (Else Laudan) 


Gérard Scappini wurde 1947 in Toulon geboren. 1966 kam er nach Deutschland, um seinen Militärdienst zu absolvieren und blieb danach in Freiburg. Er studierte Ethnologie, gründete eine Buchhandlung und reiste viele Jahre als erfolgreicher Verlagsvertreter durch den Buchhandel.