---
id: "496570271529799"
title: Abgesagt!!! The Bombs Live im Swobsters Ulm
start: 2021-09-25 20:00
address: Frauenstraße 17, 89073 Ulm
link: https://www.facebook.com/events/496570271529799/
image: 180065384_10159291401054679_31405859976984253_n.jpg
isCrawled: true
---
Wir haben uns dazu entschlossen das Konzert abzusagen, bzw. zu verschieben.
Mit den aktuellen Regeln macht das einfach keinen Spaß. 
Sorry 😔