---
id: "616755375680319"
title: Cleanup Ulm
start: 2020-09-19 10:00
end: 2020-09-19 13:00
locationName: Metzgerturm (Ulm)
address: Ulm
link: https://www.facebook.com/events/616755375680319/
teaser: "Im Rahmen des #worldcleanupday am 19. September 2020 organisieren wir von der
  HSGN Ulm einen   >>Cleanup bei uns in Ulm - am Donauufer<<  Seid also mi"
isCrawled: true
---
Im Rahmen des #worldcleanupday am 19. September 2020 organisieren wir von der HSGN Ulm einen 

>>Cleanup bei uns in Ulm - am Donauufer<<

Seid also mit dabei, wenn es heißt auf die Müllgabeln, fertig, los! :-)

Ein paar Stunden für ein saueres Ulm und zum Schutz der Umwelt und unserer Mitmenschen!!

Für Zangen, Handschuhe und Mülltüten ist gesorgt. 

Bitte achtet auf den Mindestabstand von 1,5 m und tragt ansonsten eine Mund-Nasen-Bedeckung.  #Solidarität


(Wir wissen, dass auch der Green Parking Day in Ulm parallel stattfindet und dieser auch sehr zu empfehlen ist! 
Es gibt aber natürlich die Möglichkeit auch nur für eine oder zwei Stunde(n) bei uns mitzuhelfen und anschließend noch durch die Gässchen der #GreenParkingDay-Aktionen zu bummeln- oder vice versa)


Background:
Der stetig wachsende Müll verschmutzt und vergiftet Wälder, Wiesen und Meere. Daher haben sich 2008 zum ersten Mal für fünf Stunden 50.000 Bürger/-innen in Estland zu einer Bewegung zusammengeschlossen, um das ganze Land vom Müll zu befreien und diesen  fachkundig zu entsorgen. Diese Initiative hat sich global verbreitet und wird seither jährlich am 3. Samstag im September durchgeführt. Seitdem haben sich über 60 Millionen Menschen in
158 Ländern im Rahmen des World Cleanup Days für eine sauberere Welt engagiert. Auch in Deutschland existieren zahlreiche Initiativen auf kommunaler Ebene, um Müll im lokalen
Umfeld zu sammeln und seit 2018 auch der World Cleanup Day. 

Was ist dann das Besondere am World Cleanup Day?
Die globale Skalierung, die eine tatsächliche nachhaltige Wirkung
ermöglicht und Projektaktivitäten, die über den Aktionstag hinausgehen.

#worldcleanupday2020 

Background in English:
https://www.youtube.com/watch?v=7pHW3KEP5HU

Einen herzlichen Dank an die Volksbank Ulm-Biberach e.G. und die Entsorgungsbetriebe Ulm für die freundliche Unterstützung!