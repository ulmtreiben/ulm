---
id: "5159283104088938"
title: Cleanup Ulm
start: 2021-09-18 10:00
end: 2021-09-18 13:00
locationName: Metzgerturm (Ulm)
address: Ulm
link: https://www.facebook.com/events/5159283104088938/
image: 241362445_2043613645789535_8188988336736979301_n.jpg
isCrawled: true
---

Im Rahmen des #worldcleanupday organisieren wir von der HSGN auch dieses Jahr wieder einen Cleanup bei uns in Ulm am Donauufer!

Ihr wollt mitmachen? Dann meldet euch zuvor bei Lena an, damit wir etwas planen können. Bitte schreibt hierfür eine Mail an lena-3.schmid@uni-ulm.de.

Für Zangen, Handschuhe und Mülltüten ist gesorgt :) 

Bitte achtet auf den Mindestabstand von 1,5 m und tragt ansonsten eine Mund-Nasen-Bedeckung.

Wir freuen uns auf euch! 

Zum World Cleanup Day:
2008 haben sich zum ersten Mal 50.000 Bürger/-innen in Estland zu einer Bewegung zusammengeschlossen, um das ganze Land vom Müll zu befreien und diesen fachkundig zu entsorgen. Diese Initiative hat sich global verbreitet und wird seither jährlich am 3. Samstag im September durchgeführt. Auch in Deutschland existieren zahlreiche Initiativen auf kommunaler Ebene, um Müll im lokalen Umfeld zu sammeln.