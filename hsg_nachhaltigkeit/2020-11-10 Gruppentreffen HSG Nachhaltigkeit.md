---
id: "2929554000601585"
title: Gruppentreffen HSG Nachhaltigkeit
start: 2020-11-10 19:30
end: 2020-11-10 20:30
link: https://www.facebook.com/events/2929554000601585/
image: 123524293_1796295453854690_1610067053254184451_o.jpg
teaser: Wir laden euch recht herzlich zu unserem ersten Gruppentreffen im neuen
  Semester ein. Jeder ist willkommen, wir freuen uns aber besonders über neue In
isCrawled: true
---
Wir laden euch recht herzlich zu unserem ersten Gruppentreffen im neuen Semester ein. Jeder ist willkommen, wir freuen uns aber besonders über neue Interessierte.
So könnt ihr uns und unsere Arbeitskreise  (Kleiderkarussel, Nachhaltige Stadtführung, Nähcafé, Kino-Reihe, Gardening & Bienen) kennen lernen und gerne auch eure eigenen Ideen einbringen!
Wir freuen uns auf euch.
Bitte meldet euch im Voraus bei uns an, entweder per E-Mail an hg-nachhaltigkeit@uni-ulm.de oder schreibt uns über unsere Social Media Kanäle.
