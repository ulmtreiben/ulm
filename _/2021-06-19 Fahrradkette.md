---
id: "1234567654321"
title: Fahrradkette in Ulm
start: 2021-06-19 15:00
link: https://www.facebook.com/events/346714043250069/
image: 2021-06-19_fahrrad-kette.jpg
teaser: Eine Fahrradkette für die Umwandlung zweier Spuren der Münchner Straße in Radwege
isCrawled: true
---

Event der Ulmer Grünen: https://gruene-ulm.de/fahrradkette-in-ulm/

Am 22.06.2021 wird im Gemeinderat entschieden, ob in der aktuell vierspurigen Münchner Straße je eine Fahrspur in einen Radweg umgewandelt wird. Dadurch kann endlich eine schnelle und sichere Radverbindung zwischen Ulm und Neu-Ulm über die Gänstorbrücke entstehen.

Mit unserer bunten Fahrradkette möchten wir ein deutliches Zeichen für eine solche fortschrittliche Verkehrsplanung setzen!

Für schöne und werbewirksame Bilder eines bunten, fahrradfrohen Ulms sind neben bunter Kleidung auch farbenfrohe Fahrradhelme, Sicherheitswesten sowie bunte Masken oder bunte Fahrräder sehr willkommen. Sollten ein paar Regentropfen fallen, sehen wir das als Chance um auch entsprechend bunte Schirme aufzuspannen.

Wir sind überzeugt viele Ulmer Bürger*innen für diese Aktion zu mobilisieren und freuen uns sehr auf die Aktion mit Euch!

-- 
