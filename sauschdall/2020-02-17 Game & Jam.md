---
id: "2543877825886333"
title: Game & Jam
start: 2020-02-17 21:00
end: 2020-02-18 00:00
locationName: Jazzkeller Sauschdall
address: Prittwitzstr. 36, 89075 Ulm
link: https://www.facebook.com/events/2543877825886333/
teaser: Jeden Montag ab 21 Uhr steht Dein Sofa bei uns.  Game & Jam verbindet einen
  gemütlichen Spieleabend mit einer heißen Jamsession. Und natürlich ist der
isCrawled: true
---
Jeden Montag ab 21 Uhr steht Dein Sofa bei uns. 
Game & Jam verbindet einen gemütlichen Spieleabend mit einer heißen Jamsession. Und natürlich ist der Eintritt frei. 
Für Musiker, die jammen wollen: Komplette Backline ist vorhanden. Eigene Instrumente dürfen gerne mitgebracht werden. 
Für Spielefreunde: Wir haben mittlerweile eine große Auswahl an Gesellschaftsspielen. Auch für größere Spielrunden ist genügend Platz. Wer Lust hat, darf sein eigenes Spiel gerne mitbringen, Mitspieler finden sich garantiert!