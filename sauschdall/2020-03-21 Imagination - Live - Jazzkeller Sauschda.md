---
id: "2574228739459449"
title: Imagination - Live - Jazzkeller Sauschdall Ulm
start: 2020-03-21 20:30
end: 2020-03-21 23:30
locationName: Jazzkeller Sauschdall
address: Prittwitzstr. 36, 89075 Ulm
link: https://www.facebook.com/events/2574228739459449/
image: 81259493_2987329681279626_2986177133254017024_o.jpg
teaser: Imagination – Whale Watching Tour 2020  Imagination ist wieder unterwegs! Das
  Trio aus Innsbruck präsentiert altbekannte Songs, spontane Jams und neue
isCrawled: true
---
Imagination – Whale Watching Tour 2020

Imagination ist wieder unterwegs! Das Trio aus Innsbruck präsentiert altbekannte Songs, spontane Jams und neue Inspirationen. Nach ihrem dritten Studioalbum (III-Illegal Immigrants, BBR 2018) haben sich alle drei Musiker Solo Projekten gewidmet und kehren im März 2020, gestärkt durch neue kreative Energie, zu dritt auf Bühnen in Deutschland, Österreich und Italien zurück. Man darf wie gewohnt auf eine Mischung aus Groove und tranceartigen Soli gespannt sein, getreu dem Motto Emotion und Spielfreude, darauf kommt es an! Dem eigenwilligen Mix aus New Urban Jazz, Underground und Pop werden dabei auch die Solokompositionen von Felix Kremsner (Gitarre), Tom Hiltoplt (Bass) und Sam Siefert (Drums) angepasst, sodass die Arbeiten des letzten Jahres im Imagination Gewand präsentiert werden können! Ein abwechslungsreicher Abend mit einer breiten stilistischen Palette wird von dem Instrumentaltrio dargeboten.

https://www.youtube.com/watch?v=_aVGLwkas_0