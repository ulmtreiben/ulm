---
id: "2543877839219665"
title: Game & Jam
start: 2020-03-16 21:00
end: 2020-03-17 00:00
locationName: Jazzkeller Sauschdall
address: Prittwitzstr. 36, 89075 Ulm
link: https://www.facebook.com/events/2543877839219665/
image: 82167932_2777091808995503_8512174574178140160_o.jpg
teaser: Jeden Montag ab 21 Uhr steht Dein Sofa bei uns.  Game & Jam verbindet einen
  gemütlichen Spieleabend mit einer heißen Jamsession. Und natürlich ist der
isCrawled: true
---
Jeden Montag ab 21 Uhr steht Dein Sofa bei uns. 
Game & Jam verbindet einen gemütlichen Spieleabend mit einer heißen Jamsession. Und natürlich ist der Eintritt frei. 
Für Musiker, die jammen wollen: Komplette Backline ist vorhanden. Eigene Instrumente dürfen gerne mitgebracht werden. 
Für Spielefreunde: Wir haben mittlerweile eine große Auswahl an Gesellschaftsspielen. Auch für größere Spielrunden ist genügend Platz. Wer Lust hat, darf sein eigenes Spiel gerne mitbringen, Mitspieler finden sich garantiert!