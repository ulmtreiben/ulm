---
id: "328119698625456"
title: BenJammin' @ Open Stage Jazzkeller Sauschdall / Ulmer Kulturnacht
start: 2020-09-19 16:00
end: 2020-09-19 16:30
locationName: Jazzkeller Sauschdall
address: Prittwitzstr. 36, 89075 Ulm
link: https://www.facebook.com/events/328119698625456/
teaser: Im Rahmen der Ulmer Kulturnacht bin ich im Jazzkeller Sauschdall bei der
  OpenStage auf der Bühne. Ich singe und spiele eigene Stücke an Gitarre und Kl
isCrawled: true
---
Im Rahmen der Ulmer Kulturnacht bin ich im Jazzkeller Sauschdall bei der OpenStage auf der Bühne. Ich singe und spiele eigene Stücke an Gitarre und Klavier. Musikalisch bewege ich mich dabei in progressiven Gefilden.