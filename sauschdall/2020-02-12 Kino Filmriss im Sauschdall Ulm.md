---
id: "234028010938536"
title: Kino Filmriss im Sauschdall Ulm
start: 2020-02-12 19:00
end: 2020-02-12 23:00
locationName: Jazzkeller Sauschdall
address: Prittwitzstr. 36, 89075 Ulm
link: https://www.facebook.com/events/234028010938536/
teaser: '19:00 Küfa 20:00 Film Eintritt frei!  Film "Wir sind jung. Wir sind
  stark."  Beschreibung:  Der Film erzählt die Geschehnisse des 24. August 1992
  in R'
isCrawled: true
---
19:00 Küfa
20:00 Film
Eintritt frei!

Film "Wir sind jung. Wir sind stark."

Beschreibung: 
Der Film erzählt die Geschehnisse des 24. August 1992 in Rostock-Lichtenhagen aus den Blickwinkeln unterschiedlicher Menschen. Sie alle eint die Sehnsucht nach einer Heimat, nach Liebe und Anerkennung. Doch am Ende dieses Tages werden einige von ihnen um ihr Leben fürchten, während andere Molotov-Cocktails werfen und Interviews geben.

Beschreibung englisch:
The film tells the events of August 24, 1992 in Rostock-Lichtenhagen from the perspective of different people. They all share a longing for a home, for love and  appreciation. But at the end of the day, some of them will fear for their lives, while others will throw Molotov cocktails and give interviews.