---
id: "1023922321354358"
title: Open Stage! Kulturnacht im Sauschdall
start: 2020-09-19 15:00
end: 2020-09-19 22:00
address: Jazzkeller Sauschdall
link: https://www.facebook.com/events/1023922321354358/
teaser: Der gemütliche Sauschdall öffnet wieder und ihr seid alle willkommen <3  <3
  <3     Die krasseste Party die es seit 6 Monaten, 2 Wochen und 3 Tagen gab
isCrawled: true
---
Der gemütliche Sauschdall öffnet wieder und ihr seid alle willkommen <3  <3 <3     Die krasseste Party die es seit 6 Monaten, 2 Wochen und 3 Tagen gab. Fühl dich wie ein echter Hippie oder ein falscher Gangster. Zeige in den Katakomben des Sauschdall  dein wahres Ich. Egal ob Musik, Theater oder Zauberei. Wir wollen dich sehen.
Die Bühne steht dir offen.
Bitte vorher anmelden: direkt hier auf Facebook oder unter expo@sauschdall.de
Das ganze findet an der Kulturnacht am 19.09 von 15-22 Uhr statt.
