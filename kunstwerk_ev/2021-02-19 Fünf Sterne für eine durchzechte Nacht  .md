---
id: "241157637340982"
title: '"Fünf Sterne für eine durchzechte Nacht"   Lesung: ROK, Musik: Michael
  Moravek'
start: 2021-02-19 20:00
address: Kunstpool, Galerie am Ehinger Tor, Ulm
link: https://www.facebook.com/events/241157637340982/
image: 140320134_241147540864085_7759141927717644238_n.jpg
teaser: "Wie zu erwarten war: Wir müssen wegen der Pandemie ROKs Fotoausstellung und
  damit die Vernissage und die Konzertlesung absage bzw. verlegen, sorry.  P"
isCrawled: true
---
Wie zu erwarten war: Wir müssen wegen der Pandemie ROKs Fotoausstellung und damit die Vernissage und die Konzertlesung absage bzw. verlegen, sorry.

Poesie und Beat feiern in diesem Gedichtband Hochzeit. ROKs Lyrik zeichnet sich durch ein feines Gespür für Rhythmus und Melodie aus, beide erzeugen in seinen Gedichten einen langen Kopfkino-Fluss aus sachte bewegten Bildern und lakonischen Metaphern.  Manchmal schimmert da der früheWolf Wondratschek durch und selbst der freigeistige Allen Ginsberg schwebt hin und wieder durch ROKs Wort-Räume und Wort-Träume. Harte Realität und die Schönheit melancholischer Farben sind oft nur ein Wort voneinander entfernt. Da passen die Songs von Michael Moravek, der den November in seiner Seele hat.
Eintritt: 10 €
(Eine 2. Vorstellung findet um 22 Uhr statt.)
