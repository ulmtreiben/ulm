---
id: "1016176988887085"
title: "SZENE SCHWEIZ:   AËR  (comtempory jazz)"
start: 2021-03-06 20:00
locationName: Kornhaus Ulm
address: Kornhausplatz 1, 89073 Ulm
link: https://www.facebook.com/events/1016176988887085/
image: 151235609_5466283156723055_1968542421903313054_o.jpg
teaser: Das wäre jetzt ja schon der Ersatztermin gewesen, aber die Corona-Pandemie
  lässt immer noch keine Konzerte zu. Wir können im Moment keinen weiteren Er
isCrawled: true
---
Das wäre jetzt ja schon der Ersatztermin gewesen, aber die Corona-Pandemie lässt immer noch keine Konzerte zu. Wir können im Moment keinen weiteren Ersatztermin planen und warten mal ab, leider.

Tanzperformance oder Konzert? AËR entführt uns in ungewohnte Klangreiche. Darin bringt Ania Losinger mit den Absätzen und Spitze ihrer Schuhe, mit Stößen elegant langer Stäbe und mit ihrem ganzen Körper die Xala zum Klingen, ein riesiges Instrument, das die multibegabte Künstlerin selbst entwickelt hat: eine einzigartige Symbiose aus Musik und Bewegung. Jan Heinke lässt die gestrichenen Töne seines selbst gebauten Stahlcellos mit Ober- und Untertongesang verschmelzen. Seine Sounds segeln über die raumfüllenden und überraschenden Klanglandschaften von Mats Eser an der fünfoktavigen Marimba. Ein magisches Universum!

Ania Losinger – Xala
Jan Heinke – Stahlcello / Stimme
Mats Eser – Marimba

Dieses Konzert ist der Auftakt einer 6-teiligen Reihe, in der wichtige Bands der aktuellen Schweizer Jazzszene vorgestellt werden. 

Eintritt: 15 €, ermäßigt 10 € 

(präsentiert von: Junge Ulmer Bühne, KunstWerk e. V. und der vh Ulm)