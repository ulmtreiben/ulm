---
id: "180836677434022"
title: "Sonderveranstaltung: Solaris"
start: 2021-08-04 19:30
locationName: Mephisto Ulm
address: Rosengasse 15, 89073 Ulm
link: https://www.facebook.com/events/180836677434022/
image: 219661109_10159795592094994_4872597844835112163_n.jpg
isCrawled: true
---
Die Vorstellung läuft im Kontext des Ulmer Veranstaltungsprogramms zum 100jährigen Geburtstag des polnischen SF-Autors Stanislaw Lem. Mit kurzer Einführung von Reinhard Köhler (Musiker und Bildender Künstler) über diesen wohl bekanntesten Film Stanislaw Lems.

Der Abend ist Teil einer Veranstaltungsreihe, die vom Verein Übermorgenwelt in Zusammenarbeit mit der Kunstpool. Galerie am Ehinger Tor und dem KunstWerk e. V. durchgeführt wird, und heute ist als Veranstalter das Mephisto-Kino dabei.

Thema des heutigen Abends ist die filmische Umsetzung des gleichnamigen Romans von Stanislaw Lem, und wir freuen uns riesig, dass der Kinofilm auch tatsächlich im Kino gezeigt werden kann. Vielen Dank an das Mephisto-Kino und Sebastian Schmid von der Sailer GbR Neu-Ulm
Der russische Filmkünstler Andrej Tarkowskij hat 1972 aus dem Stoff ein poetisches und zeitloses Meisterwerk geschaffen, das man nicht oft genug sehen kann. Nach jedem Betrachten erschließen sich neue Bedeutungsdimensionen."Solaris" ist wahrscheinlich Lems bekanntester Roman und gleichzeitig auch Tarkowskijs berühmtester Film.
Steven Soderbergh hat 2002 den Stoff neu verfilmt, konnte aber bei weitem nicht die künstlerische Tiefe Tarkowskijs erreichen.

Beschreibung:
Ein Psychologe wird zum Planeten Solaris geschickt, um unerklärlichen Vorkommnissen auf der dortigen Forschungsstation nachzuspüren. Die Konfrontation mit einer absolut fremden Lebensform (der gesamte Planet spiegelt als kollektives Bewußtsein die Erinnerungen, Ängste und Wünsche der Raumfahrer zurück) wird für die Besatzung des Raumschiffs zur metaphysischen Reise in die Innenwelt ihrer eigenen Kultur.

Nach dem Science-Fiction-Roman von Stanislaw Lem erzählt Andrej Tarkowskij eine philosophische Fabel, die um die abendländischen Ideen von Tod, Liebe und Auferstehung kreist. Ein brillant inszenierter, äußerst reicher und vielschichtiger Film, der, im Gewand einer technischen Utopie, die Hybris traditionellen Fortschrittsglaubens in Frage stellt.

Der Abend ist Teil einer Veranstaltungsreihe, die vom Verein Übermorgenwelt in Zusammenarbeit mit der Kunstpool. Galerie am Ehinger Tor und dem KunstWerk e. V. durchgeführt wird.
In der Kunstpool-Galerie ist eine Ausstellung von 19 Künstler*innen aus ganz Deutschland zu sehen, die sich mit dem Werk Stanislaw Lems auseinandersetzen.