---
id: "1043055029455477"
title: Ausstellung "Drucken ist ein Abenteuer"
start: 2021-01-08 17:00
end: 2021-01-08 20:00
address: Kunstpool, Galerie am Ehinger Tor, Ulm
link: https://www.facebook.com/events/1043055029455477/
image: 124633085_5042786722406036_4207550817567052107_o.jpg
teaser: Tja, wegen des verschärften Corona-Lockdowns müssen wir die Ausstellung
  schließen und werden sie hoffentlich am 11.1. wieder öffnen!  „Im Prozess des
isCrawled: true
---
Tja, wegen des verschärften Corona-Lockdowns müssen wir die Ausstellung schließen und werden sie hoffentlich am 11.1. wieder öffnen!

„Im Prozess des Druckens kann die Dynamik aus Gewaltsamkeit, Glück und Verzweiflung ausgelotet“, schrieb der wohl bekannteste deutsche Holzschneider des 20. Jahrhunderts, HAP Grieshaber. Wir zeigen Druckgraphik unterschiedlicher Techniken: Radierungen, Linolschnitte, Lithographien, Siebdrucke, experimentelle Verfahren. 
(Zu sehen sind Arbeiten von Jörg Eberwein, Alfred Hrdlicka, Adela Knajzl, Willi Sitte, Daniel Wogenstein.)
Jörg Eberwein zeigt Arbeiten mit Linien- und Flächenätzungen (Aquatinta) sowie weitere Mischtechniken, z. B. Verbindungen von Digitaldruck und Radierung, ebenso Arbeiten bei denen Druckgrafik in Klang umgesetzt wird. Außerdem zeigt er zur Vernissage eine Performance. 
Adela Knajzl wird hauptsächlich Linolschnitte und Siebdrucke zeigen. Von Daniel Wogenstein und Willi Sitte sind Lithografien zu sehen. Die Kaltnadelradierung wird durch Alfred Hrdlicka repräsentiert.
Die Ausstellung ist dann bis zum 23.1 zu sehen, jeweils Do-Sa 17-20 Uhr. Ausnahme: die Weihnachtstage und die 3 Tage um Neujahr.)
Außerdem gibt es Sonderöffnungszeiten, wenn eine unserer vier Veranstaltungen des Begleitprogramms stattfindet.
