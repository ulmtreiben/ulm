---
id: "2863578800624598"
title: '"Ein Päckchen voll Hoffnung" - Texte aus vier Generationen'
start: 2021-09-06 20:00
locationName: Kunstpool, Galerie am Ehinger Tor, Ulm
link: https://www.facebook.com/events/2863578800624598/
image: 202366589_333321328313372_7055592393431228620_n.jpg
isCrawled: true
---
Die darstellende und bildende Künstlerin Ricarda Rommerscheidt (Bonn) beschäftigt sich seit über 20 Jahren mit dem Thema Krieg und den körperlichen sowie psychischen Folgen auf nachfolgende Generationen. Durch zahlreiche Ausstellungen in Polen liegt ihr die polnisch-deutsche Vergangenheitsbewältigung ebenso am Herzen wie die familiären Verbindungen nach Amerika.

Die Veranstaltung gehört zur Ausstellung "Der Tod ist ein Meister aus Ulm" und findet im Rahmen der Ulmer Friedenswochen statt.

Der Eintritt ist frei.