---
id: "822228451675359"
title: "SZENE SCHWEIZ: Julie Campiche Quartet  (contemporary jazz)"
start: 2021-05-08 20:00
locationName: Einsteinhaus Ulm
address: Kornhausplatz 5, D-89073  Germany, 89073 Ulm
link: https://www.facebook.com/events/822228451675359/
image: 132190410_5223026941048679_3244049633204410336_o.jpg
teaser: Julie Campiche ist eine Harfenistin, die ihr Instrument fortlaufend neu
  erfindet. In Kombination mit elektronischen Helferlein hat sie ihre Klangpalet
isCrawled: true
---
Julie Campiche ist eine Harfenistin, die ihr Instrument fortlaufend neu erfindet. In Kombination mit elektronischen Helferlein hat sie ihre Klangpalette komplettiert und so eine sehr persönliche Sprache entwickelt. Nach acht Jahren reger Konzerttätigkeit mit der Gruppe Orioxy (mit der sie auch schon bei uns zu Gast war) lancierte sie ein neues Projekt in einer rein instrumentalen Besetzung. Die großen Fragen ihrer Generation dienten als Inspirationsquelle für die Kompositionen. Wie im Fall des Stückes "Onkalo", das nach dem Standort des ersten Endlagers für radioaktiven Abfall benannt ist, oder das Stück "Flash Info", das die mediale Reizüberflutung thematisiert. 
Julie Campiche: Harfe
Leo Fumagalli: Saxophon
Manu Hagmann: Kontrabass
Clemens Kuratle: Schlagzeug

Eintritt: 15 €, ermäßigt 10 € (Mtgl./Schü/Stud/Bufdis/FSJ), bis 16. J frei

Vorverkauf: wwww.ulmtickets.de

Veranstalter: KunstWerk e. V. + Ulmer Volkshochschule