---
id: "2684799018513252"
title: "Wortschatzübungen #8: „Hoffnung“"
start: 2020-09-25 19:30
end: 2020-09-25 22:30
locationName: Stadtbibliothek Ulm
address: Ulm
link: https://www.facebook.com/events/2684799018513252/
teaser: “Hope is the thing with feathers”, heißt es bei Emily Dickinson. Sie schreibt,
  dass uns Hoffnung im Herzen liegt. Wir haben Personen des öffentlichen
isCrawled: true
---
“Hope is the thing with feathers”, heißt es bei Emily Dickinson. Sie schreibt, dass uns Hoffnung im Herzen liegt. Wir haben Personen des öffentlichen Lebens dazu eingeladen, ihre persönlichen Schätze zum Thema aus der Literatur vorzutragen. 
Es lesen: 
Martin Bendel (Finanzbürgermeister)
Judith Garcia Beier (Inhaberin, Die Apotheke)
Christoph Hantel (Leiter vh Ulm)
Stefan Lefler (Zusammenhalt Ulm)
Saskia Ochner (Moderatorin, Donau 3 FM)
Kathi Wolf (Schauspielerin, Kabarettistin)
Moderation: Paolo Percoco
Musik: Grimm Bastard

Eintritt frei 
Veranstalter: KunstWerk e.V. und Stadtbibliothek Ulm

Eine Veranstaltung im Rahmen der Ulmer Friedenswochen
