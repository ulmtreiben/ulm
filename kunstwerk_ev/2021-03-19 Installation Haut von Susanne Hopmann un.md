---
id: "239461827888890"
title: Installation "Haut" von Susanne Hopmann und Lukas Pfalzer
start: 2021-03-19 05:00
end: 2021-03-20 00:00
address: Kunstpool, Galerie am Ehinger Tor, Ulm
link: https://www.facebook.com/events/239461827888890/
image: 138384940_5314267621924610_3401564793482096893_o.jpg
teaser: „Haut“ heißt die temporäre Kunstinstallation der beiden Künstler*innen. Die
  Haltestelle Ehinger Tor hat den brachialen Charme einer Zukunft, die längs
isCrawled: true
---
„Haut“ heißt die temporäre Kunstinstallation der beiden Künstler*innen. Die Haltestelle Ehinger Tor hat den brachialen Charme einer Zukunft, die längst vergangen ist. Großflächig wird die Videoaufnahme einer Massage  in die urbane Umgebung eingefügt, kontrastiert den schweren Bau mit der Idee einer leichten Berührung: eine wohlwollende Einflussnahme. 

Die Videoinstallation ist fast rund um die Uhr an der Fassade der Galerie zu sehen, die beiden Künstler*innen zeigen auch gegenüber eine Medieninstallation im Schaufenster.
