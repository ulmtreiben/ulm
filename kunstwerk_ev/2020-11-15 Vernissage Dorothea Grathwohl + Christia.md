---
id: "938229436662179"
title: 'VERLEGT: Vernissage Dorothea Grathwohl + Christian Greifendorf "Nach
  Hausmacherart"'
start: 2020-11-15 17:00
address: Kunstpool, Galerie am Ehinger Tor, Ulm
link: https://www.facebook.com/events/938229436662179/
image: 121185488_188706199441553_3889100824150388300_o.jpg
teaser: Wir lassen die Galerie im November wegen des Corona-Lockdown geschlossen. Aus
  diesem Grund wird diese Ausstellung auf den Sommer verlegt.  Dorothea Gr
isCrawled: true
---
Wir lassen die Galerie im November wegen des Corona-Lockdown geschlossen. Aus diesem Grund wird diese Ausstellung auf den Sommer verlegt.

Dorothea Grathwohl und Christian Greifendorf setzen sich mit Habitaten auseinander. Größenverhältnisse, Farbe und Wohlfühlübertragung auf reale Wohnsituationen treiben die beiden Kunstschaffenden um.  
Die Ausstellung ist dann bis zum 4.12. zu sehen, jeweils Do-Sa 17-20 Uhr, 
Eintritt frei

Die KUNSTPOOL-Galerie befindet sich direkt auf dem Haltestellengelände Ehinger Tor, große Banner weisen auf den Eingang hin.
