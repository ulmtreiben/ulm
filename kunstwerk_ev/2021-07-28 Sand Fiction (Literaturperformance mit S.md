---
id: "247103083847752"
title: Sand Fiction (Literaturperformance mit Sandmalerei, elektronischer Live-Musik
  und Live-Projektionen)
start: 2021-07-28 20:00
address: ROXY Ulm I Kultur in den Hallen
link: https://www.facebook.com/events/247103083847752/
image: 199555486_329743432004495_8901943475153626874_n.jpg
isCrawled: true
---
Ein Erkundungsflug durch die Science-Fiction-Literatur mit den Kapitänen Stanislaw Lem, Bradbury und Bester. Dabei treffen Stimme, live gemalte, projizierte Sand-Bilder und live gemischte Sounds aufeinander und verschmelzen zu einer neuen Dimension.

Geschichten von: Ray Bradbury, Alfred Bester & Stanislaw Lem
Stimme - Sarah Gros NF 		
Sandmalerei - Chris Kaiser 			
Elektronische Sounds	- XOFORO 	

https://www.facebook.com/SANDFICTION
SANDFICTION Teaser https://vimeo.com/212823827

Der Abend ist Teil einer Veranstaltungsreihe, die vom Verein Übermorgenwelt in Zusammenarbeit mit der Kunstpool. Galerie am Ehinger Tor, dem Roxy und dem KunstWerk e. V. durchgeführt wird. In der Kunstpool-Galerie ist eine Ausstellung von 19 Künstler*innen aus ganz Deutschland zu sehen, die sich mit dem Werk Stanislaw Lems auseinandersetzen.

Weitere Veranstaltungen: 14.7., 4.8., 12.8.

Eintritt: 12 €, ermäßigt 10 € (Die Facebook-Angabe "freier Eintritt" lässt sich leider nicht abstellen)
