---
id: "162942492169558"
title: '"Haut" Heute sind die Künstler*innen anwesend.'
start: 2021-03-14 17:00
address: Kunstpool, Galerie am Ehinger Tor, Ulm
link: https://www.facebook.com/events/162942492169558/
image: 151700113_260326738946165_4389188679280940967_n.jpg
teaser: Heute sind von 17 – 20 die beiden Künstler*innen anwesend.  „Haut“ heißt die
  Installation der beiden Künstler*innen. Die Haltestelle Ehinger Tor hat d
isCrawled: true
---
Heute sind von 17 – 20 die beiden Künstler*innen anwesend.

„Haut“ heißt die Installation der beiden Künstler*innen. Die Haltestelle Ehinger Tor hat den brachialen Charme einer Zukunft, die längst vergangen ist. Großflächig wird die Videoaufnahme einer Massage in die urbane Umgebung eingefügt, kontrastiert den schweren Bau mit der Idee einer leichten Berührung: eine wohlwollende Einflussnahme. 
Die Videoinstallation ist fast rund um die Uhr an der Fassade der Galerie zu sehen, nämlich von 5 bi 24 Uhr. Außerdem zeigen wir schräg gegenüber, auf der anderen Seite des Haltestellengeländes, in einem Schaufenster eine weitere Arbeit des Künstlerpaars.
Alles ist also auch unter Coronabedingungen zu sehen, ganz easy. (Nur die Ausstellung im Innenbereich unserer Galerie kann nicht stattfinden.)

Anwesenheitszeiten von Susanne Hopmann und Lukas Pfalzer (jeweils von 17-20 Uhr):
So. 7.3 
Fr. 12.3
Sa. 13.3
So 14.3
Fr. 19.3
Sa. 20.3
So. 21.3