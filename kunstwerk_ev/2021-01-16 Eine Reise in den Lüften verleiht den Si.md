---
id: "124937746127242"
title: 'VERLEGT: "Eine Reise in den Lüften verleiht den Sinnen
  Schwingen"  (Performance: Poesie & Musik)'
start: 2021-01-16 20:00
locationName: Kornhaus Ulm
address: Kornhausplatz 1, 89073 Ulm
link: https://www.facebook.com/events/124937746127242/
image: 137530168_5303577199660319_8187830979440360353_n.jpg
teaser: "LEIDER, leider muss die Show wegen der Corona-Pandemie verlegt werden. Neuer
  Termin: 29. April.    Höhenflüge, Abstürze und alles dazwischen. In den T"
isCrawled: true
---
LEIDER, leider muss die Show wegen der Corona-Pandemie verlegt werden. Neuer Termin: 29. April.



Höhenflüge, Abstürze und alles dazwischen. In den Texten geht es um den Traum vom Fliegen und um Fliegen im Traum. Um das Fliegen als eine schon viele tausende Jahre alte Metapher, um exotische Flugapparate, abenteuerliche Ballonfahrten, Reisen zu fernen Welten des Universums und ins eigene Ich. 
Die literarisch-poetischen Texte werden durch einen Sprecher vorgetragen, atmosphärisch umgesetzt durch fünf Musiker. 

Unterhaltsam, aber mit Tiefsinn. 
Philosophisch, aber mit Leichtigkeit. 
Abenteuerlich und ergreifend. -
Abhebend !

Thomas Hohnerlein: Sprecher
Helga Kölle-Köhler: Gesang + Akustikgitarre
Johannes Honnef: Schlagzeug + Elektronik
Sebastian Jooß: Tasteninstrumente, E-Gitarre, Saxophon
Georg Daucher: Akustikgitarre und experimentelle Musikinstrumente
Reinhard Köhler: E-Bass u.a.

Eintritt: 12 €, ermäßigt 8 € 

(präsentiert von: Junge Ulmer Bühne, KunstWerk e. V. und der vh Ulm)