---
id: "360729981853676"
title: 'Vernissage entfällt, Verkaufsausstellung findet statt:  "Drucken ist ein
  Abenteuer"'
start: 2020-12-06 17:00
address: Kunstpool, Galerie am Ehinger Tor, Ulm
link: https://www.facebook.com/events/360729981853676/
image: 124691837_197697725209067_782436873638086066_o.jpg
teaser: LEIDER, LEIDER muss die Vernissage ausfallen. Daher startet unsere
  Verkaufsausstellung ohne Eröffnung am 10.12.  „Im Prozess des Druckens kann
  die Dyn
isCrawled: true
---
LEIDER, LEIDER muss die Vernissage ausfallen. Daher startet unsere Verkaufsausstellung ohne Eröffnung am 10.12.

„Im Prozess des Druckens kann die Dynamik aus Gewaltsamkeit, Glück und Verzweiflung ausgelotet“, schrieb der wohl bekannteste deutsche Holzschneider des 20. Jahrhunderts, HAP Grieshaber. Wir zeigen Druckgraphik unterschiedlicher Techniken: Radierungen, Linolschnitte, Lithographien, Siebdrucke, experimentelle Verfahren. 
(Zu sehen sind Arbeiten von Jörg Eberwein, Alfred Hrdlicka, Adela Knajzl, Willi Sitte, Daniel Wogenstein.)
Jörg Eberwein zeigt Arbeiten mit Linien- und Flächenätzungen (Aquatinta) sowie weitere Mischtechniken, z. B. Verbindungen von Digitaldruck und Radierung, ebenso Arbeiten bei denen Druckgrafik in Klang umgesetzt wird. Außerdem zeigt er zur Vernissage eine Performance. Adela Knajzl wird hauptsächlich Linolschnitte und Siebdrucke zeigen. Von Daniel Wogenstein und Willi Sitte sind Lithografien zu sehen. Die Kaltnadelradierung wird durch Alfred Hrdlicka repräsentiert.
Die Ausstellung ist dann bis zum 23.1 zu sehen, jeweils Do-Sa 17-20 Uhr. Ausnahme: die Weihnachtstage und die 3 Tage um Neujahr.)
Außerdem haben mehrere Veranstaltungen im Rahmenprogramm
Eintritt frei
