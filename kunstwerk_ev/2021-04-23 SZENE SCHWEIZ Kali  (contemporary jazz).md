---
id: "149364459991969"
title: "SZENE SCHWEIZ: Kali  (contemporary jazz)"
start: 2021-04-23 20:00
locationName: Kornhaus Ulm
address: Kornhausplatz 1, 89073 Ulm
link: https://www.facebook.com/events/149364459991969/
image: 132335605_5223012797716760_7611885319756106556_o.jpg
teaser: "Wundersam dunkle Pattern-Räume, rohe Minimal-Groove-Verzahnungen und
  abstrakt-mystische Trips: Kali repräsentiert den frischen und seriösen Geist
  eine"
isCrawled: true
---
Wundersam dunkle Pattern-Räume, rohe Minimal-Groove-Verzahnungen und abstrakt-mystische Trips: Kali repräsentiert den frischen und seriösen Geist einer neuen Generation von post-genre Musikern. Die drei Musiker sind hellhörig in einer musikalischen Welt aufgewachsen, in der Prog Groove, Noise, New Minimal, Ambient und zeitgenössischen klassische Kammermusik einfach verschiedene Dialekte einer gemeinsamen Sprache geworden sind. Kein Wunder, dass ihr Debut-Ablum „Riot“ von Nik Bärtsch produziert wurde.
Raphale Loher: Flügel
Urs Müller: Gitarre
Nicolas Stocker: Schlagzeug

Dieses Konzert das fünfte einer 6teiligen Reihe, in der wichtige Bands des aktuellen Schweizer Jazz vorgestellt werden.

Eintritt: 15 €, ermäßigt 10 € 

(präsentiert von Junge Ulmer Bühne, KunstWerk e. V. und der vh Ulm)