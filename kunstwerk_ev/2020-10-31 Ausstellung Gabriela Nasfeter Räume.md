---
id: "873763166361372"
title: Ausstellung Gabriela Nasfeter "Räume"
start: 2020-10-31 17:00
end: 2020-10-31 20:00
address: Kunstpool, Galerie am Ehinger Tor, Ulm
link: https://www.facebook.com/events/873763166361372/
image: 121164495_188705212774985_1666228362763411482_o.jpg
teaser: Die 1950 in Polen geboren Künstlerin lebt und arbeitet seit 40 Jahren in Ulm.
  Vielfach preisgekrönt, stellt sie seit 1975 in ganz Europa aus. Erinnert
isCrawled: true
---
Die 1950 in Polen geboren Künstlerin lebt und arbeitet seit 40 Jahren in Ulm. Vielfach preisgekrönt, stellt sie seit 1975 in ganz Europa aus. Erinnert sei an ihre riesigen Textilobjekte, die sie als „Lichtpyramiden“ in Kathedralen in Deutschland, Polen, England, Frankreich, Israel, Holland, Armenien, Türkei und Deutschland zeigte. 
In der KUNSTPOOL-Ausstellung dagegen zeigt sie neue Arbeiten, aber auch einen Rückblick auf ihr Schaffen der letzten Jahre, zu sehen sind Objektkunst und Wandarbeiten.
Die Ausstellung ist  bis zum 31.10. zu sehen, jeweils Do-Sa 17-20 Uhr.
Eintritt frei.