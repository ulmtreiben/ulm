---
id: "426748408457130"
title: 'statt Vernissage: Beginn "Haut" (Lukas Pfalzer + Susanne Hopmann)'
start: 2021-03-07 17:00
address: Kunstpool, Galerie am Ehinger Tor, Ulm
link: https://www.facebook.com/events/426748408457130/
image: 132359195_5222920474392659_535014625645410366_o.jpg
teaser: Die Haltestelle Ehinger Tor hat den brachialen Charme einer Zukunft, die
  längst vergangen ist. „Haut“ heißt die temporäre Kunstinstallation der beiden
isCrawled: true
---
Die Haltestelle Ehinger Tor hat den brachialen Charme einer Zukunft, die längst vergangen ist. „Haut“ heißt die temporäre Kunstinstallation der beiden Künstler*innen aus Leipzig und Halle. Großflächig wird die Videoaufnahme einer Massage in die urbane Umgebung eingefügt, kontrastiert den schweren Bau mit der Idee leichter Berührungen.: eine wohltuende Einflussnahme. 

Die Videoinstallation ist an der Fassade der Galerie zu sehen, außerdem gibt es eine Schaufensterinstallation gegenüber. 

Ab 17 Uhr sind die beiden Künstler*innen vor Ort.

Die Installation ist bis 21.3. fast rund um die Uhr zu sehen, nur tief in der Nacht schläft sie ein paar Stunden