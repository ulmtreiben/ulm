---
id: "1597804790429433"
title: Sag Nein! (Lesung & Musik)
start: 2021-09-21 20:00
locationName: Kunstpool, Galerie am Ehinger Tor, Ulm
link: https://www.facebook.com/events/1597804790429433/
image: 187694794_5925144670836899_976508115455549116_n.jpg
isCrawled: true
---
Passend zur laufenden Kunstausstellung und im Rahmen der Ulmer Friedenswochen lesen Thomas Hohnerlein und Reinhard Köhler Lyrik, die sich mit dem Tod durch Rüstung beschäftigt und damit, wie wir Menschen jetzt und in Zukunft damit umgehen wollen. 
Die Gedichte gruppieren sich rund um Wolfgang Borcherts berühmtes "Dann gibt es nur eins".
 Andreas Heizmann begleitet und sorgt für Zwischenspiele auf der Bassklarinette und der Kontrabassklarinette.

Veranstalter:
- KUNSTPOOL. Galerie am Ehinger Tor
- Verein für Friedensarbeit e. V.
- KunstWerk e. V.

Eintritt frei

Ort:
Kunstpool-Galerie auf dem Haltestellengelände Ehinger Tor, 89077 Ulm  

Mehr Informationen über die Ulmer Friedenswochen:
https://www.friedenswochen-ulm.de
