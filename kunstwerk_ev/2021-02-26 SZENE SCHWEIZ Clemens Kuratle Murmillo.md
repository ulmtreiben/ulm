---
id: "213412920426061"
title: "SZENE SCHWEIZ: Clemens Kuratle Murmillo"
start: 2021-03-21 20:00
locationName: Kornhaus Ulm
address: Kornhausplatz 1, 89073 Ulm
link: https://www.facebook.com/events/213412920426061/
image: 132592415_5222900831061290_7716881518061489521_o.jpg
teaser: Eine psychedelische Rave-Night? Der murmelnde Ambientsound, der das neue Album
  der Band eröffnet, klingt fast so.  Aber Murmullo pauken sich nicht zwi
isCrawled: true
---
Eine psychedelische Rave-Night? Der murmelnde Ambientsound, der das neue Album der Band eröffnet, klingt fast so. 
Aber Murmullo pauken sich nicht zwischen Spannung und Entladung zur Ekstase. Vielmehr spinnt die Band ein feines Gewebe, das sich akustisch differenziert entfaltet und dynamisiert. Im jazzkantigen Flow ist hin und wieder Folkfeeling oder eine fette E-Gitarre zu hören, klare Bläserriffs und ein pochendes Schlagzeug: Eine faszinierend vielschichtige Klanglandschaft, verfeinert mit elektroakustischen Elementen, treibenden Grooves und vielfältigen Klangfarben.

Clemens Kuratle: Schlagzeug
Jonathan Maag: Tenorsaxophon
Florian Weiss: Posaune
Franz Hellmüller: Gitarre
Rafael Jerjen: Bass

Dieses Konzert ist das dritte einer 6teiligen Reihe, in der wichtige Bands der neuen Schweizer Jazzszene vorgestellt werden.

Eintritt: 15 €, ermäßigt 10 € 

(präsentiert von Junge Ulmer Bühne, KunstWerk e. V. und der vh Ulm)