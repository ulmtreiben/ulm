---
id: "2764142700489888"
title: ENTFÄLLT WG. CORONA  Kunstsammeln für Arbeitnehmer
start: 2020-12-13 19:00
address: Kunstpool, Galerie am Ehinger Tor, Ulm
link: https://www.facebook.com/events/2764142700489888/
image: 123766648_196275998684573_4190076396817125132_o.jpg
teaser: LEIDER, LEIDER darf diese Veranstaltung nicht stattfinden (wegen des
  Corona-Lockdown). Aber die Verkaufsausstellung "Drucken ist eine Abenteurer"
  darf
isCrawled: true
---
LEIDER, LEIDER darf diese Veranstaltung nicht stattfinden (wegen des Corona-Lockdown). Aber die Verkaufsausstellung "Drucken ist eine Abenteurer" darf besucht werden, ab 10.13. zu unseren Öffnungszeiten.

Die Entdeckung der Druckkunst revolutionierte die Welt und auch die Kunst. Originale, handsignierte künstlerische Druckgrafik gibt es schon zu erschwinglichen Preisen für den Normalverdiener. Informationen für Grafikkäufer und Tipps für Neueinsteiger in diesen Markt werden heute Abend kombiniert mit Erläuterungen druckgrafischer Techniken und einer kleinen Auktion, bei der jeder zu sehr günstigen Preisen Werke mit nach Hause nehmen kann. 
Bitte informieren Sie sich über das weitere Rahmenprogramm zur Ausstellung „Drucken ist ein Abenteuer“ auf unserer Website.
Eintritt frei.
