---
id: "868554234093543"
title: 'Vernissage: "Der Tod ist ein Meister aus Ulm"'
start: 2021-09-05 17:00
address: Kunstpool, Galerie am Ehinger Tor, Ulm
link: https://www.facebook.com/events/868554234093543/
image: 238992304_6652669634758409_4144960040436709007_n.jpg
isCrawled: true
---
Ulm ist eine Rüstungs- und Militärstadt. Todbringende Waffen und ihr Zubehör werden produziert, für den Export vorbereitet oder der Armee zugeführt. Logistische Planungen der Bundeswehr und der NATO finden hier statt, viele Institutionen werden von der Bundeswehr betrieben oder arbeiten ihr zu. Künstler setzen sich mit diesen für sie doch recht ungewöhnlichen Themen auseinander und zeigen gemeinsam eine Ausstellung ihrer Arbeitsergebnisse. 

Einführung: Dorothea Grathwohl + Reinhard Köhler
Musikalische Umrahmung: Helga Kölle-Köhler

Beteiligte Künstler:
Adams Myrah (Ulm)
Bartl Bertram (Ulm)
Besier Ann (Eltville)
Busch Ursula (Ulm)
Debusi Peter (Darmstadt)
Faber Ursula (Großniedesheim)
Feistle Josef (Weißenhorn)
Gaydoul Dirk (Weiterstadt)
Grathwohl Dorothea (Ulm)
Greifendorf Christian (Ulm)
Hilbert ANRA (Lottstetten)
Hölz Elisabeth (Amtzell)
Jung-Wiesenmayer Silvia (Opfenbach)
Klennert Hagen (Berlin)
Köhler Reinhard (Ulm)
Kolibri (Jestetten)
Kurzawe Fritz (Freiberg)
Laar Kalle Aldis (Krailling)
Löhr Ralf (Bensheim)
Paetzold Dietmar (Köln)
Pape Roswitha (Heidelberg)
Reul Horst (Illertissen)
Riche Sandra (Berlin)
Rockenbauch Walter (Heidenheim)
Röttger Lena (Berlin)
Ricarda Rommerscheidt (Bonn)
Schmeckenbecher Eva (Stuttgart)
Schulz Andreas Paul (Augsburg)
Schwalt-Scherer Helga (Mainaschaff)
Seruset Lothar (Lentzke)
Silber Gerhard (Wittmund)
Smirnova Julia (München)
Sommer-Meyer Anne (Weinheim)
Stolz Gabriele (München)
Thiele-Zoll Ursula + Dietmar (Stuttgart)
von Klein Kerstin (Krefeld)



Veranstalter: 
Verein für Friedensarbeit
KunstWerk e. V.
KUNSTPOOL. Galerie am Ehinger Tor

 Eintritt frei

Mehr Informationen über die Ulmer Friedenswochen:
https://www.friedenswochen-ulm.de

