---
id: "387720162332082"
title: "SZENE SCHWEIZ: Christy Doran´s Sound Fountain (contemporary rock jazz)"
start: 2021-03-12 20:00
locationName: Kornhaus Ulm
address: Kornhausplatz 1, 89073 Ulm
link: https://www.facebook.com/events/387720162332082/
image: 151779004_5466332173384820_7245657781879936393_o.jpg
teaser: Auch dieses Konzert kann wegen der Corona-Pandemie leider nicht stattfinden.
  Wir hatten uns schon soooo gefreut!  Ein energetischer Klangbrunnen mit U
isCrawled: true
---
Auch dieses Konzert kann wegen der Corona-Pandemie leider nicht stattfinden. Wir hatten uns schon soooo gefreut!

Ein energetischer Klangbrunnen mit Unterströmungen, vor denen Baywatch warnen würde! Christy Doran, Gitarren-Ikone aus der Schweiz, steht schon immer für das ständige Ausloten kreativer Grenzen. Was von der Besetzung her wie ein klassisches Jazz- oder Rocktrio daherkommt, konfrontiert uns aber durchaus mit einem energiegeladenen Mix aus unkonventionellen Ideen zwischen den Polen komponierter und improvisierter Musik, der eine faszinierende Sogwirkung entfaltet.
Christy Doran: E-Gitarre + Komposition 
Lukas Mantel: Schlagzeug + Perkussion
Wolfgang Zwiauer: E-Bass

Dieses Konzert das vierte einer 6teiligen Reihe, in der wichtige Bands der aktuellen Schweizer Jazzszene vorgestellt werden.

Eintritt: 15 €, ermäßigt 10 € 

(präsentiert von Junge Ulmer Bühne, KunstWerk e. V. und der vh Ulm)