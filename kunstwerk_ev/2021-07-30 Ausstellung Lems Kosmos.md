---
id: "4235948169781354"
title: Ausstellung "Lems Kosmos"
start: 2021-07-30 17:00
end: 2021-07-30 20:00
locationName: Kunstpool, Galerie am Ehinger Tor, Ulm
link: https://www.facebook.com/events/4235948169781354/
image: 209907596_340468374265334_3007408676719880522_n.jpg
isCrawled: true
---
2021 jährt sich zum 100. Mal der Geburtstag von Stanislaw Lem. Der studierte Mediziner wurde als Schriftsteller und Sachautor weltberühmt und mit Preisen überhäuft, seine Arbeiten wurden in 57 Sprachen übersetzt. Die Science-Fiction-Texte unter ihnen gehören zu den meistgelesenen dieses Genres.
Lem gilt als brillianter Denker und Visionär, bereits in den 60er Jahren schrieb er über Nanotechnolgie, neuronale Netze, KI und Virtuelle Realität. Philosophische und ethische, aber auch wissenschaftstheoretische und kosmogonische Fragestellungen ziehen sich sowohl durch seine ernsthaften als auch seine satirischen und humoristischen literarischen Werke.

Wir freuen uns über Ausstellungsbeiträge, die genauso vielfältig, abgedreht, witzig und aberwitzig die Gedanken- und Bilderwelt Stanislaw Lems aufgreifen, weiterentwickeln, auf die Spitze treiben, umkehren, ad absurdum führen, sonstwas damit tun oder sie einfach illustrieren.
Lems Phantasie war überbordend und nicht einzuschränkend, so haben wir auch für diese Ausstellung keinerlei Bewerbungseinschränkungen vorgenommen.

Ausstellende Künstler:
Myrah Adams (Neu-Ulm)
Wolfgang Bernert (Nürnberg)
Oleg Breininger (Alfter)
Ursula Busch (Ulm)
Maren Diedrich (Georgensgmünd)
Dorothea Grathwohl (Ulm)
Reinhard Köhler (Ulm)
Matthias Kraus (Hasselroth)
Volker Kurz (Gröbenzell)
Carola Lantermann (Hauenstein)
Hans Liebl (Neu-Ulm)
Nikolaus Mohr (Ostrach)
Gabriela Morschett (Müllheim)
Elisabeth Schlanstein (Krefeld)
Stefan Stock (Kastl)
Rolf Thiemann (Münster)
Marion Uphues-Klee (Biberach)
Karen Van Dooren (Nijmegen, NL)
Julia Wally Wagner (Helmstedt)


Wir danken der Stadt Ulm, Abteilung Kultur, für die finanzielle Unterstützung aus Mitteln der Projektförderung.