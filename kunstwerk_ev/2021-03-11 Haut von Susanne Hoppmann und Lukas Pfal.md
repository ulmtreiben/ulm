---
id: "214907400344333"
title: '"Haut" von Susanne Hopmann und Lukas Pfalzer'
start: 2021-03-11 17:00
end: 2021-03-11 20:00
address: Kunstpool, Galerie am Ehinger Tor, Ulm
link: https://www.facebook.com/events/214907400344333/
image: 138384940_5314267621924610_3401564793482096893_o.jpg
teaser: „Haut“ heißt die temporäre Kunstinstallation der beiden Künstler*innen. Die
  Haltestelle Ehinger Tor hat den brachialen Charme einer Zukunft, die längs
isCrawled: true
---
„Haut“ heißt die temporäre Kunstinstallation der beiden Künstler*innen. Die Haltestelle Ehinger Tor hat den brachialen Charme einer Zukunft, die längst vergangen ist. Großflächig wird die Videoaufnahme einer Massage  in die urbane Umgebung eingefügt, kontrastiert den schweren Bau mit der Idee einer leichten Berührung: eine wohlwollende Einflussnahme. 
Entsprechend den Auflagen und Bedürfnissen im März wird es ein weiterführendes Angebot der beiden Kunstschaffenden zum Gespräch und Vermittlung im Innen- und Außenraum der Galerie geben.
Im Rahmen der Ausstellung gibt es zwei Begleitveranstaltungen am 13.3. und 20.3.
