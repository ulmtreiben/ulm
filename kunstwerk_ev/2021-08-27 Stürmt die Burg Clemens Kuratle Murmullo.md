---
id: "220449742934286"
title: "Stürmt die Burg: Murmullo + Sonar [Konzert | Kunstwerk e.V.]"
start: 2021-08-27 20:15
address: Wilhelmsburg Ulm
link: https://www.facebook.com/events/220449742934286/
image: 210351958_1385439448517232_7262138829477601941_n.jpg
isCrawled: true
---
Konzert: Murmullo + Sonar
Vor die Bandinfos kommen:

ACHTUNG: Unser morgiges Doppelkonzert findet auf jeden Fall statt, im Fall der Fälle wird es im Konzertraum, also inside, durchgeführt.
WICHTIG: Da dort nur eine begrenzte Platzzahl vorhanden ist, empfiehlt sich frühzeitige Ankunft !!!
UND, klar, im Innern: GGG (geimpft, genesen oder geteste) und Maskenpflicht, wie bei allen indoor-Veranstaltungen.

Zwei Schweizer Spitzenbands des aktuellen, zeitgenössischen Jazz

20.30 Uhr | Bühne im Innenhof

Dieser Abend eröffnet die „Szene Schweiz“ – Reihe des KunstWerk e. V., in der die Creme de la Creme der aktuellen Schweizer Jazzszene vorgestellt wird. Die Reihe wird auch in 2022 fortgesetzt.

Clemens Kuratle Murmullo
Eine psychedelische Rave-Night? Der murmelnde Ambientsound, der das neue Album der Band eröffnet, klingt fast so. Aber Murmullo pauken sich nicht zwischen Spannung und Entladung zur Ekstase. Vielmehr spinnt die Band ein feines Gewebe, das sich akustisch differenziert entfaltet und dynamisiert. Im jazzkantigen Flow ist hin und wieder Folkfeeling oder eine fette E-Gitarre zu hören, klare Bläserriffs und ein pochendes Schlagzeug: Eine faszinierend vielschichtige Klanglandschaft, verfeinert mit elektroakustischen Elementen, treibenden Grooves und vielfältigen Klangfarben.

Murmullo sind:
Clemens Kuratle: Schlagzeug
Jonathan Maag: Tenorsaxophon
Florian Weiss: Posaune
Franz Hellmüller: Gitarre
Rafael Jerjen: Bass

Sonar
"Begib dich auf die Reise in das Getriebe, die komplexe Anordnung von Zahnrädern, und lass dich hoch, hinunter und seitwärts schleudern, kraftvoll und präzis zugleich."
Richtig: Die Genauigkeit einer Schweizer Uhr: Eine komplexe Anordnung von Zahnrädern. Davon hat auch Sonar etwas. Die Band kommt nicht nur aus der Schweiz und saugt den Zuhörer wie in ein analoges Uhrwerk. Diese Band schleudert ihn auch direkt ins Getriebe. In ihrem „minimal groove“ vereinen sie Stilmittel aus Jazz, Progressive Rock und Minimal Music. Das Resultat sind Stücke voller komplexer Rhythmen und ineinandergreifenden Muster in einer überraschenden Breite an Texturen und Klängen. Das alles ist verdammt schwer zu spielen, aber der Zuhörer merkt davon nichts. Er will sich einfach zum Sound bewegen oder verfällt in Trance oder beides, je nach Charakter. Auf jeden Fall sind Sonar ein Erlebnis!

Sonar sind:
Stephan Thelen: guitar
Bernhard Wagner: guitar
Christian Kuntner: electric bass
Manuel Pasquinelli: drums


#StürmtdieBurg2021 
Vom 30. Juli bis 29. August 2021 darf die Wilhelmsburg jeweils von Mittwoch bis Sonntag gestürmt werden! Wie im vergangenen Sommer steht „Stürmt die Burg" im Zeichen von #kulturerhalten und bietet Kulturschaffenden eine langersehnte Bühne und Räumlichkeiten zur Darbietung von Musik, Literatur, Theater, Tanz, Kunst und Kinderprogramm.
Nicht nur auf der Bühne, sondern auch neben der Bühne ist in diesem Jahr viel geboten: Audiovisuelle Performances, Audiowalks, ein Theaterprojekt der adk, FabienneYoga über den Dächern Ulms und sechs Installationen in den Innenräumen der Bundesfestung bieten ein abwechslungsreiches Programm. Immer sonntags gibt es ein abwechslungsreiches Programm für die ganze Familie. Über 80 verschiedene Akteure beteiligen sich am Pop up Space auf der Wilhelmsburg, mit dabei sind das Ulmer Zelt, die popbastion.ulm, die vh ulm, die Literaturwoche Donau, das Internationale Donaufest, der Arbeitskreis Kultur, der Förderkreis Bundesfestung Ulm e.V., Bands gegen Rechts und Broken Stage.

#Gastronomie
Auch in diesem Sommer bietet die Burgbar No. XII erfrischende Drinks und leckeres Essen an allen Veranstaltungstagen. Für weitere Informationen zur Gastronomie, besucht die Burgbar-Webseite: www.burgbar12.de

#Shuttlebus
Wie gehabt bringt Euch ein Shuttlebus kostenlos vom Hans-und-Sophie-Scholl-Platz in der Stadtmitte oder von der Haltestelle Frauenstraße (Höhe Gold Ochsen Brauerei) auf die Wilhelmsburg und bis 24 Uhr wieder zurück in die Stadt.

#Corona_Hinweis
Zur Sicherheit aller haltet euch bitte an die aktuell gültigen Hygiene- und Abstandsregelungen – weitere Informationen veröffentlichen wir auf der Website. Bei Krankheitssymptomen bleibt bitte zuhause. Stay safe & stay tuned!

#WeitereInfos
www.die-wilhelmsburg.de

#Unterstützung
Der Pop up Space wird von der Kulturabteilung der Stadt Ulm koordiniert und von der Brauerei Gold Ochsen unterstützt. Alle Infos unter: www.die-wilhelmsburg.de