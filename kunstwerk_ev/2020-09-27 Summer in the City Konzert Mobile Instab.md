---
id: "625401741436160"
title: "Summer in the City: Konzert: Mobile Instabile und Gäste"
start: 2020-09-27 20:00
end: 2020-09-27 21:30
address: Kunstpool, Galerie am Ehinger Tor, Ulm
link: https://www.facebook.com/events/625401741436160/
teaser: Die Musik bewegt sich langsam wie ein Mobile von Alexander Calder. Und oft ist
  sie fragil und zerbrechlich, zart und luftig. In manchmal freier Improv
isCrawled: true
---
Die Musik bewegt sich langsam wie ein Mobile von Alexander Calder. Und oft ist sie fragil und zerbrechlich, zart und luftig. In manchmal freier Improvisation, aber auch in komponierten Stücken schaffen die Musiker instabile Gebilde, die wie Seifenblasen aufscheinen und dann wieder vergehen, mobil und instabil eben.
Die Band steht in unterschiedlichen Zusammensetzungen auf der Bühne, manchmal als Duo, als Trio…., manchmal ohne, nie aber mit zwei Schlagzeugern gleichzeitig. Was alle eint, ist die Faszination für ungewöhnliche Klänge bis hin zu Geräuschen sowie die Lust aufs Experimentieren.

Andreas Heizmann: Bassklarinette, Kontrabassklarinette
Isolde Werner: Gesang
Armin Egenter: Schlagzeug
Johannes Honnef: Schlagzeug
Reinhard Köhler: E-Bass
und evtl. weitere Gäste
Eintritt frei

Diese Veranstaltung findet im Rahmen unserer Reihe "Summer in the City" statt.
Vielen Dank an das Ministerium für Wissenschaft, Forschung und Kunst Baden-Württemberg für die Unterstützung.