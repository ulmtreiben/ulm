---
id: "141445368144887"
title: Ehinger Tor Utopien (Lesung mit Marco Kerler)
start: 2021-10-07 19:00
locationName: Kunstpool, Galerie am Ehinger Tor, Ulm
link: https://www.facebook.com/events/141445368144887/
image: 239337787_6397452456939449_665867248516872186_n.jpg
isCrawled: true
---

Das Ehinger Tor als Ausgangspunkt für eine utopische Liebe. Trotz unser aller Schmutz ein Ort der Schönheit, solange man träumen kann. Und dann die Busfahrten vor oder nach der Arbeit. Wirklichkeit mit kleinen Glücksmomenten, Merkwürdigkeiten.
Marco Kerlers Aufzeichnungen, tagebuchartige Lyrik, lassen fragen, ob all das real gewesen ist oder nicht. Am Ende aber bleibt Hoffnung, und das tut gut.

Eintritt frei, Spenden erwünscht.

Wir danken für die Unterstützung durch die Stadt Ulm, Abteilung Kultur. 