---
id: "379676196719315"
title: "Der Künstlerin über die Schulter schauen: Adela Knajzl"
start: 2020-12-22 12:00
end: 2020-12-22 18:00
address: Kunstpool, Galerie am Ehinger Tor, Ulm
link: https://www.facebook.com/events/379676196719315/
image: 123735000_196283468683826_6263454161913812572_n.jpg
teaser: Wegen des verschärften Corono-Lockdowns können wir diese Aktion momentan nicht
  durchführen.  Last-Minute-Weihnachtsgeschenk fällig? Heute bedruckt Ade
isCrawled: true
---
Wegen des verschärften Corono-Lockdowns können wir diese Aktion momentan nicht durchführen.

Last-Minute-Weihnachtsgeschenk fällig? Heute bedruckt Adela Knajzl wieder Stofftaschen mit wunderbar schlicht-poetischen Motiven, aber auch Grafiken auf Papier. Wer will, darf ihr dabei zuschauen, evtl. eine Tasche erwerben, aber natürlich kann man sich auch mit der Künstlerin über ihre Tätigkeit unterhalten oder sich das Druckverfahren erläutern lassen. Außerdem kann man sich natürlich die Ausstellung anschauen.
(Die Ausstellung ist in dieser Zeit geöffnet; Eintritt frei)
