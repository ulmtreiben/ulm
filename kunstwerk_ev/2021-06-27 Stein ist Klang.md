---
id: "418343176078664"
title: Stein ist Klang
start: 2021-06-27 15:00
address: Atelier Daucher in 88422 Oggelshausen (Haldenstraße 84)
link: https://www.facebook.com/events/418343176078664/
image: 132410016_5223044837713556_889645967907812365_n.jpg
isCrawled: true
---
Der Steinbildhauer Elmar Daucher schuf 1970 das wuchtig-schlichte Mahnmal für die Opfer des Faschismus, das in Stuttgart zwischen dem Alten Schloss und dem Karlsplatz steht. In den 1970er und 1980er Jahren war er Pionier und Wegbereiter bei der Entwicklung von Klangsteinen. Tonnenschwere Granite und Serpentine können mit nassen Händen zum Schwingen und Vibrieren gebracht werden, so dass der mächtige magische Klang des uralten Gesteins körperlich fühlbar und hörbar wird. Im kleinen Privatpark bei seinem ehemaligen Atelier in Oggelshausen am Federsee stehen viele stehen viele Steinskulpturen und Klangsteine in freier Natur. Dort gibt es auch einen Ausstellungsraum bzw. ein Klanghaus mit Pyramidendach, welches eine beeindruckende Verbindung herstellt. 
Wir erleben in einer Führung durch den Steingarten und einem anschließenden Klangsteinspiel die kraftvolle Energie der Steine.

Wir empfehlen Tickets im Vorverkauf, da nur eine sehr begrenzte Zahl an Plätzen zur Verfügung steht. 
Vorverkauf: www.ulmtickets.de

Eintritt: 14 €, ermäßigt 10 € (mit Ausweis: Mitglieder/Schüler/Studenten/Bufdis/FSJ), bis 16 Jahre freier Eintritt