---
id: "446449320091817"
title: 'Ausstellung ROK: "One Second Of Silence"'
start: 2021-02-19 17:00
end: 2021-02-19 20:00
address: Kunstpool, Galerie am Ehinger Tor, Ulm
link: https://www.facebook.com/events/446449320091817/
image: 140591618_241146614197511_9077509348602688270_n.jpg
teaser: "Wie zu erwarten war: Wir müssen wegen der Pandemie ROKs Fotoausstellung und
  damit die Vernissage und die Konzertlesung absage bzw. verlegen, sorry.  R"
isCrawled: true
---
Wie zu erwarten war: Wir müssen wegen der Pandemie ROKs Fotoausstellung und damit die Vernissage und die Konzertlesung absage bzw. verlegen, sorry.

ROK ist eine feste Größe in der Ulmer Kulturszene, in der er auf verschiedene Weise in Erscheinung tritt; viele kennen ihn beispielsweise als stillen Musikenthusiasten, der immer wieder die Musikszene als Veranstalter mitprägt. 
Heute stellen wir den Fotografen ROK vor. In seinem ganz eigenen Stil begleitet er sein Jahren Menschen und Events mit der Kamera, technisch äußerst sparsam und oft unter bewusster Missachtung fotografischer Regeln. So entstehen ungewöhnliche Fotografien, manchmal bizarr, manchmal sehr intim, immer aber behutsam und atmosphärisch dicht. In unserer Ausstellung legen wir den Schwerpunkt auf schwarz-weiße Porträtfotos. 
