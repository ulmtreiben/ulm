---
id: "590144515242731"
title: "Kulturnacht: Unter Wilden + Manawa"
start: 2020-09-19 17:00
end: 2020-09-20 00:15
address: Kunstpool, Galerie am Ehinger Tor, Ulm
link: https://www.facebook.com/events/590144515242731/
teaser: (bei gutem Wetter outdoor!)  UNTER WILDEN ist eine neue Ulmer Band um Sänger
  Simon Kombrink, der seine Texte in deutscher Sprache schreibt. Schlagzeug
isCrawled: true
---
(bei gutem Wetter outdoor!)

UNTER WILDEN ist eine neue Ulmer Band um Sänger Simon Kombrink, der seine Texte in deutscher Sprache schreibt. Schlagzeug, Bass, Keyboard und natürlich Stimmbänder. Die Band sitzt mit ihre, Sound zwischen allen Stilen und Stühlen. Musik und Texte mit Feinheiten, anspruchsvolle Unterhaltung mit Tiefgang, aber ohne intellektuelle Überfrachtung.

MANAWA Musik, kreisend um Stille, Stimme, Wort und Saiten. Mit den Ohren des Herzens lauschen. „Kann mir einer sagen, wohin ich mit meinem Leben reiche?“ (Rilke)
Manawa bedeutet: Jetzt ist der Augenblick der Macht. Die Sängerin Helga Kölle-Köhler verwebt eigene Texte mit Mantren, mit Aussagen der hawaiianischen Kahunas, der Cherokeeindianer, aber auch Lyrikern wie R. M. Rilke. Das Duo schreibt seine Musik selbst, Georg Daucher spielt speziell gestimmte Gitarren, zugleich kraftvoll und zart-sensibel.

(im Rahmen der Ulmer Kulturnacht)

