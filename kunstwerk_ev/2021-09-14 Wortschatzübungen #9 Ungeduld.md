---
id: "1432302853778140"
title: 'Wortschatzübungen #9: "Ungeduld"'
start: 2021-09-14 19:30
locationName: Stadtbibliothek Ulm
address: Ulm
link: https://www.facebook.com/events/1432302853778140/
image: 189239158_5940336589317707_3141676176584233760_n.jpg
isCrawled: true
---
Warten und Stillstand werden oft begleitet von Ungeduld. Wann wird es besser, anders oder wieder so wie früher? In der Ungeduld kann Unzufriedenheit und Unsicherheit stecken, aber auch Energie und Mut zur Veränderung.
An diesem Abend erfahren wir, wie Personen des öffentlichen Lebens aus Ulm und um Ulm herum dieses Thema verstehen und wo sie es in der Literatur wiederfinden.

Mit dabei: 
Sven Wisser (Junge Ulmer Bühne)
Katharina Gräfin Reuttner von Weil (Hospiz-Verein)
Samy Wiltschek (Kulturbuchhandlung Jastram)
Nancy Hecker-Denschlag (Albert Einstein Discovery Center Ulm e.V.) Marco Kerler (Lyriker)
Ariane Müller (Musikerin)
Moderation: Nathalie Wenzel (Radio Free FM)
Musik-Zwischenspiele: The Müller Sisters

Die Veranstaltung findet im Rahmen der Ulmer Friedenswochen statt.

Veranstalter: 
KunstWerk e . V. + Stadtbibliothek Ulm

Mehr Informationen über die Ulmer Friedenswochen:
https://www.friedenswochen-ulm.de

Eintritt frei