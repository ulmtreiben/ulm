---
id: "207817074559432"
title: Ausstellung "Der Tod ist ein Meister aus Ulm"
start: 2021-09-24 17:00
end: 2021-09-24 20:00
locationName: Kunstpool, Galerie am Ehinger Tor, Ulm
link: https://www.facebook.com/events/207817074559432/
image: 239345801_375959807382857_6664933980814302402_n.jpg
isCrawled: true
---
Vernissage: 
So 5.9 um 17 Uhr
Ausstellung "Der Tod ist ein Meister aus Ulm"
Ausstellung bis 25.09. Geöffnet jeweils Do-Sa 17:00-20:00

Eintritt frei
Ulm ist eine Rüstungs- und Militärstadt. Todbringende Waffen und ihr Zubehör werden produziert, für den Export vorbereitet oder der Armee zugeführt. Logistische Planungen der Bundeswehr und der NATO finden hier statt, viele Institutionen werden von der Bundeswehr betrieben oder arbeiten ihr zu. Künstler aus ganz Deutschland setzen sich mit diesen
für sie doch recht ungewöhnlichen Themen auseinander und zeigen gemeinsam eine Ausstellung ihrer Arbeitsergebnisse.

Beteiligte Künstler:
Adams Myrah (Ulm)
Bartl Bertram (Ulm)
Besier Ann (Eltville)
Busch Ursula (Ulm)
Debusi Peter (Darmstadt)
Faber Ursula (Großniedesheim)
Feistle Josef (Weißenhorn)
Gaydoul Dirk (Weiterstadt)
Grathwohl Dorothea (Ulm)
Greifendorf Christian (Ulm)
Hilbert ANRA (Lottstetten)
Hölz Elisabeth (Amtzell)
Jung-Wiesenmayer Silvia (Opfenbach)
Klennert Hagen (Berlin)
Köhler Reinhard (Ulm)
Kolibri (Jestetten)
Kurzawe Fritz (Freiberg)
Laar Kalle Aldis (Krailling)
Löhr Ralf (Bensheim)
Paetzold Dietmar (Köln)
Pape Roswitha (Heidelberg)
Reul Horst (Illertissen)
Riche Sandra (Berlin)
Rockenbauch Walter (Heidenheim)
Röttger Lena (Berlin)
Ricarda Rommerscheidt (Bonn)
Schmeckenbecher Eva (Stuttgart)
Schulz Andreas Paul (Augsburg)
Schwalt-Scherer Helga (Mainaschaff)
Seruset Lothar (Lentzke)
Silber Gerhard (Wittmund)
Smirnova Julia (München)
Sommer-Meyer Anne (Weinheim)
Stolz Gabriele (München)
Thiele-Zoll Ursula + Dietmar (Stuttgart)
von Klein Kerstin (Krefeld)

Wir danken für die Unterstützung der Stadt Ulm, Abteilung Kultur
