---
id: "398543254824767"
title: 'Verlegt: "Fünf Sterne für eine durchzechte Nacht"   Lesung: ROK, Musik:
  Michael Moravek'
start: 2021-02-19 22:00
address: Kunstpool, Galerie am Ehinger Tor, Ulm
link: https://www.facebook.com/events/398543254824767/
image: 151086220_258539382458234_2278391084982089998_o.jpg
teaser: Leider müssen wir die Ausstellung von Rok und damit auch die
  Begleitveranstaltungen auf später im Jahr verlegen.  Poesie und Beat feiern in
  diesem Ged
isCrawled: true
---
Leider müssen wir die Ausstellung von Rok und damit auch die Begleitveranstaltungen auf später im Jahr verlegen.

Poesie und Beat feiern in diesem Gedichtband Hochzeit. ROKs Lyrik zeichnet sich durch ein feines Gespür für Rhythmus und Melodie aus, beide erzeugen in seinen Gedichten einen langen Kopfkino-Fluss aus sachte bewegten Bildern und lakonischen Metaphern.  Manchmal schimmert da der früheWolf Wondratschek durch und selbst der freigeistige Allen Ginsberg schwebt hin und wieder durch ROKs Wort-Räume und Wort-Träume. Harte Realität und die Schönheit melancholischer Farben sind oft nur ein Wort voneinander entfernt. Da passen die Songs von Michael Moravek, der den November in seiner Seele hat.
Eintritt: 10 €
(Eine 2. Vorstellung findet um 20 Uhr statt.)
