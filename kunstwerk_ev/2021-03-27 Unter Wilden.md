---
id: "245782760229796"
title: Unter Wilden + We Say So
start: 2021-03-27 20:00
locationName: Kornhaus Ulm
address: Kornhausplatz 1, 89073 Ulm
link: https://www.facebook.com/events/245782760229796/
image: 133731147_5246038918747481_2943338987713410694_o.jpg
teaser: Unter Wilden zu leben ist nicht immer leicht. Vor allem, wenn man nicht so
  richtig weiß, ob man selbst auch zu den Wilden gehört. Mit solchen Gedanken
isCrawled: true
---
Unter Wilden zu leben ist nicht immer leicht. Vor allem, wenn man nicht so richtig weiß, ob man selbst auch zu den Wilden gehört. Mit solchen Gedanken im Koffer betrat die Ulmer Band UNTER WILDEN letzten Sommer die Bühnen. Ihr Sound sitzt dabei zwischen allen Stilen und Stühlen. Die bunte Welt des Indie Pop, die nostalgische Welt des Chansons oder die des gutgelaunten Reggae – für UNTER WILDEN schließt sich das alles nicht aus. Was kann die Band also zu einem stimmungsvollen Frühlingstag beitragen? Musik und deutsche Texte mit Feinheiten, anspruchsvolle Unterhaltung mit Tiefgang, aber ohne intellektuelle Überfrachtung. Und zuletzt den Wunsch nach mehr. 
Simon Kombrink: Gesang + Trompete
Manuel Staib: Keyboard
Reinhard Köhler: Bass
Johannes Honnef: Schlagzeug

Mit im Boot sind außerdem WE SAY SO, knackiger Indiesound mit deutschen Texten. Die sagen: Außergewöhnliche Umstände erfordern außergewöhnliche Ausdrucksarten und so werden Fabi, Mike und Locke die deutschsprachige Musiklandschaft aufmischen!

Eintritt: 12 €, ermäßigt 8 €

(präsentiert von Junge Ulmer Bühne, KunstWerk e. V. und der vh Ulm)