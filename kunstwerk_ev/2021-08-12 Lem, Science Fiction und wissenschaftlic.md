---
id: "190515536195613"
title: Lem, Science Fiction und wissenschaftliche Realität
start: 2021-08-12 20:00
address: Übermorgenwelt, Fort Albeck 14, 89075 Ulm
link: https://www.facebook.com/events/190515536195613/
image: 199205322_329759668669538_1032798853286924210_n.jpg
isCrawled: true
---
Dieses Jahr wäre der berühmte polnische Schriftsteller 100 Jahr alt geworden.

Man weiß nicht so genau, ob diese Veranstaltung zu seinen Ehren ein literaturwissenschaftlich-philosophisches Colloquium, eine Literaturlesung, eine Kunstperformance oder eine Podiumsdiskussion mit Publikumsbeteiligung ist. Wahrscheinlich hat sie, so wie es Lems phantastischer Literatur entspricht, von allem etwas.

Der Abend ist Teil einer Veranstaltungsreihe, die vom Verein Übermorgenwelt in Zusammenarbeit mit der Kunstpool. Galerie am Ehinger Tor und dem KunstWerk e. V. durchgeführt wird. In der Kunstpool-Galerie ist eine Ausstellung von 19 Künstler*innen aus ganz Deutschland zu sehen, die sich mit dem Werk Stanislaw Lems auseinandersetzen.

Es wird eine Eintrittsgebühr erhoben. (Die Facebook-Angabe "freier Eintritt" lässt sich leider nicht abstellen)