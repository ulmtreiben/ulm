---
name: Frauentreff Ulm e.V.
website: http://www.frauentreff-ulm.de/
email: info@frauentreff-ulm.de
scrape:
  source: facebook
  options:
    page_id: frauentreffulm
---
Das Ziel: Raum für Frauen zu schaffen, Frauenkulturzu fördern, sich gegen Benachteiligung von Frauen sowie gesellschaftliche und juristische Anerkennung lesbischer Lebensweise zu engagieren. 
