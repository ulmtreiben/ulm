---
id: "198223144561853"
title: Disco im Frauentreff
start: 2020-01-25 20:00
locationName: Frauentreff Ulm e.V.
address: Hinter dem Brot 9, 89073 Ulm
link: https://www.facebook.com/events/198223144561853/
image: 83604580_2635563753250352_4888773565786095616_n.jpg
teaser: 🎶🏳️‍🌈💃🏻 Am Samstag, 25.1.2020 ist wieder Disco im Frauentreff.   Ladies
  only!!! 👩‍❤️‍👩  Wir freuen uns, wenn ihr wieder mit uns und DJ onepiece
isCrawled: true
---
🎶🏳️‍🌈💃🏻
Am Samstag, 25.1.2020 ist wieder Disco im Frauentreff. 

Ladies only!!! 👩‍❤️‍👩

Wir freuen uns, wenn ihr wieder mit uns und DJ onepiece feiert. 

Eintritt ist frei! 🎫 ❌

Euer Team vom Frauentreff