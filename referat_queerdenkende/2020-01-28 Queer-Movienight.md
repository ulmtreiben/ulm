---
id: "2590013077948366"
title: Queer-Movienight
start: 2020-01-28 19:00
end: 2020-01-28 23:00
address: Blauer Hörsaal Uni West (H45.1)
link: https://www.facebook.com/events/2590013077948366/
image: 84330593_2687807011505868_4813016278329458688_n.jpg
teaser: Hallo ihr Lieben, unser nächstes Treffen wir ein Filmabend sein. Wir haben ein
  paar Filme dabei und würden abstimmen welchen wir anschauen. Wenn ihr e
isCrawled: true
---
Hallo ihr Lieben, unser nächstes Treffen wir ein Filmabend sein. Wir haben ein paar Filme dabei und würden abstimmen welchen wir anschauen. Wenn ihr einen bestimmten Filmwunsch habt, könnt ihr natürlich auch selber Filme mitbringen. 
Außerdem kann man sich Kissen oder ähnliches mitnehmen, dass es schön bequem wird :D

Wir freuen uns auf euch! :) 