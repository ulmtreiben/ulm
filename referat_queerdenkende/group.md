---
name: Referat für QUEERdenkende
website: https://www.facebook.com/queeruniulm
email: 
scrape:
  source: facebook
  options:
    page_id: queeruniulm
---
Das Referat für QUEERdenkende an der Uni Ulm ist Anlauf- und Ansprechpunkt für alle queer denkenden Studierenden. D.h. für Menschen, die Liebe und Sexualität nicht heteronormativ definieren und/oder für die Geschlecht mehr als zwei biologisch determinierte Kategorien umfasst. Wir sind Teil der Studierendenvertretung (StuVe). In der Regel treffen wir uns einmal im Monat, um gemeinsam unsere Freizeit zu verbringen. Außerdem möchten wir durch unsere Präsenz an der Uni Ulm auf das Themengebiet der geschlechtlichen und sexuellen Vielfalt aufmerksam machen.
