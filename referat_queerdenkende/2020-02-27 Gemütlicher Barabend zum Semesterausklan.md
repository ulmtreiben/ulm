---
id: "642664986535892"
title: Gemütlicher Barabend zum Semesterausklang
start: 2020-02-27 19:00
end: 2020-02-28 03:00
locationName: Herr Berger
address: Marktplatz 14, 89073 Ulm
link: https://www.facebook.com/events/642664986535892/
teaser: Hallo ihr Lieben! Um das Semester gemeinsam ausklingen zu lassen, wollen wir
  uns mit euch im Herr Berger treffen, um gemütlich zusammen zu sitzen und
isCrawled: true
---
Hallo ihr Lieben!
Um das Semester gemeinsam ausklingen zu lassen, wollen wir uns mit euch im Herr Berger treffen, um gemütlich zusammen zu sitzen und noch was zu trinken. Wir freuen uns auf euch!

**********************English version below**********************

To end the semester together with all of you we want to meet at the bar Herr Berger. We will sit together a bit and talk and drink some drinks!
We are looking forward to seeing you there!