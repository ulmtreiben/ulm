---
name: Zugvögel Ulm
website: http://www.zugvoegel.org
email: info@zugvoegel.org
scrape:
  source: facebook
  options:
    page_id: zugvoegelulm
---
Der gemeinnützige Verein "Zugvögel e.V." hat es sich zum Ziel gemacht, institutionellen Rassismus und globale Ungleichheiten zu bekämpfen. Dafür werden, neben vielen anderen Aktionen, vor allem Freiwilligendienste für Menschen aus dem globalen Süden in Deutschland organisiert.
