---
id: "289562822032759"
title: Heiter bis Wolkig vs. Kommando Walter
start: 2020-05-01 21:00
end: 2020-05-02 03:00
locationName: Hemperium
address: Zinglerstraße 1, 89073 Ulm
link: https://www.facebook.com/events/289562822032759/
image: 87024225_555136001754509_1185354658084814848_o.jpg
teaser: HEITER BIS WOLKIG, PUNK KABARETT AUS KÖLN, GIBT SICH DIE EHRE!    Bereits vor
  über zwanzig Jahren haben sie die Subkultur nachhaltig durcheinandergew
isCrawled: true
---
HEITER BIS WOLKIG, PUNK KABARETT AUS KÖLN, GIBT SICH DIE EHRE!  
 Bereits vor über zwanzig Jahren haben sie die Subkultur nachhaltig durcheinandergewirbelt. Mit einem ganz eigenen Mix im Sinne der Spaßguerilla und dem Motto "Lacht kaputt was Euch kaputt macht" wurden Songs wie "Hey Rote Zora" und "10 kleine Nazischweine" mit Slime zu Szenehits. Nach zahlreichen Skandalen, etlichen Eklats und vielen Jahren fast vollständiger Bühnenabstinenz kehren die beiden HbW-Urgesteine Marco und Micha nun zurück zu ihren Wurzeln, einem politisch unkorrekten und völlig schmerzfreien Kleinkunstprogramm. 

KOMMANDO WALTER

Sechs Musiker ergeben Kommando Walter. Sie sind eine Mischung aus 80´ Punk, Ska, Reggae und NDW mit deutschen selbst komponierten mehrstimmig vorgetragenen Texten und einer gehörigen Portion Spaß auf der Bühne, den sie dem Publikum genüsslich um die Ohren hauen. Entertainment pur.