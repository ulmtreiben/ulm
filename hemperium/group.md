---
name: Hemperium
website: http://www.hemperium.de
email: info@hemperium.de
scrape:
  source: facebook
  options:
    page_id: hemperium.ulm
---
Das Hanf-Restaurant in Ulm.
Café / Live-Club / Bar / Biergarten
