---
id: "992892428144783"
title: Tanz mit dem Tiger
start: 2021-09-11 19:30
end: 2021-09-11 20:15
address: ROXY.ulm
link: https://www.facebook.com/events/992892428144783/
image: 227734393_10158701028872756_8992218179947637820_n.jpg
isCrawled: true
---
Eine Umarmung von Virtualität und Realität durch Tanz.

Tanz mit dem Tiger* entführt euch in eine Welt, in der Augmented Reality und Tanz zu einem Performance-Erlebnis verschmelzen. Durch die Zusammenführung mit neuen Technologien verbindet die Kunst des Tanzes virtuelle Choreografien auf innovative Weise mit zwei Live-Darsteller:innen.  
Drei internationale „distant digital dance makers“ – aus Brasilien, Hong Kong und den USA – entwickeln und performen Choreografien und transformieren sie in Augmented Reality. 

In Ulm erarbeitet Choreograf Pablo Sansalvador gemeinsam mit zwei Tänzer:innen eine Live-Performance zu eigens für dieses Projekt erschaffener Musik. Die übliche Bühnen-Publikum-Distanz wird aufgelöst: Pro Vorstellung können bis zu zehn Personen wortwörtlich in die Performance eintauchen. Mithilfe von Tablets bewegen sich die Teilnehmenden im Raum zwischen den „digitalen Tänzer:innen“ in Form ihrer Avatare sowie dem realen Geschehen. 
Darüber hinaus kann dieses innovative Experiment bequem von zu Hause aus als 360° Live-Stream erlebt werden. Auch die Online-Zuschauer:innen können die Aufführung mitgestalten, indem sie die Beleuchtung oder die Geschwindigkeit des Sounds an ihren Rechnern beeinflussen.   

*Der Titel TMDT stammt aus einem Zitat von Dr. Christian Drosten im Mai 2020, in dem er den deutschen Ansatz zur Bewältigung der Corona-Pandemie beschrieb: "Es gelte jetzt Stückchen für Stückchen herauszufinden, wo man dem Tier die Leine lösen kann, ohne dass es gleich über einen herfällt.“   

Die Corona-Pandemie hat das Leben aller angegriffen, insbesondere das von Performance-Künstlern und -Künstlerinnen. Die Stärke von Künstlerinnen und Künstlern besteht darin, ihre Kreativität einzusetzen und einen Weg zu finden, um mit diesem gemeinsamen Trauma umzugehen und einen Raum für Verständnis, Solidarität und Empathie zu schaffen. Ironischerweise wäre ein solches Projekt sonst nicht möglich.  

👁 Wichtige Info: Wer vor Ort an der Performance teilnimmt, kann unter Umständen im jeweiligen Live-Stream gesehen werden. Mit dem Kauf des Tickets wird dem zugestimmt. 👁

✨
Künstlerische Leitung / Choreografie: Pablo Sansalvador     
Musik & Sound Komposition: Max Levy     
3D Digitale Animation: Guido Stuch    
Softwareentwicklung und Code Kunst: Alpay Artun     
Produktionsmanagement: Raphaëlle Polidor    
Dramaturgie: Sandra Schumacher     
Live-Tänzer:in: Claire De Caluwe (Amsterdam), Miguel Toros (Saarbrücken)   
Distant Digital Dance Makers: Claudia Mwabasili & Roges Doglas (São Paulo), Zelia ZZ Tan (Hong Kong), Kelsey Paschich (Michigan)   

✨ 
Created by TanzLabor Ulm and ROXY Ulm  

Gefördert vom Ministerium für Wissenschaft, Forschung und Kunst Baden-Württemberg

In Zusammenarbeit mit Widerstand und Söhne GmbH