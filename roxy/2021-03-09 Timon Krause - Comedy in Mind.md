---
id: "292817191727163"
title: Timon Krause - Comedy in Mind (wird verschoben)
start: 2021-03-09 20:00
end: 2021-03-09 22:00
address: ROXY.ulm
link: https://www.facebook.com/events/292817191727163/
image: 99436258_3683196445030077_5505023109868027904_o.jpg
teaser: Die Veranstaltung wird verschoben. Aktuell sind wir noch auf der Suche nach
  einem neuen Datum - sobald das bekannt ist, informieren wir euch hier, auf
isCrawled: true
---
Die Veranstaltung wird verschoben. Aktuell sind wir noch auf der Suche nach einem neuen Datum - sobald das bekannt ist, informieren wir euch hier, auf unserer Homepage und auf unseren Social Media Kanälen. 

Comedy in Mind

It's funny! It's mindblowing!

Ihr glaubt, eure persönliche Pin wäre sicher in eurem Kopf? Niemand könnte einfach so eure Handynummer erraten? Eure Unterhosenfarbe kennt nur ihr oder euer Tinder-Date? Timon Krause beweist das Gegenteil. Er kennt alle Geheimnisse und Daten, die man eigentlich niemals teilen würde.
Comedy in Mind ist das neue Entertainmentprogramm des 25-jährigen Mentalisten Timon Krause. Seine Fähigkeiten im Gedankenlesen und seine Stories zeigen die strange Welt des Showbiz. Ob es eine gute Idee ist, bei Auftritten im KitKat Club Berlin oder bei verschwörerischen Angeboten auf Instagram in die Köpfe der Menschen einzutauchen? Unbedingt, findet Timon Krause. Er verblüfft, unterhält und überrascht mit seinen Geschichten, Skills und Tipps zum Gedankenlesen im Alltag. Und findet Dinge heraus, die noch nicht mal eure besten Freundinnen und Freunde wissen.
Comedy in Mind nimmt die Zuschauer mit auf eine visuelle und unterhaltsame Zeitreise von Timons Weg über fragwürdige Angebote und turbulente Nächte in Las Vegas bis hin zu einem Treffen mit Uri Geller - und einem unerwarteten Finale.

In seinem neuen Live-Programm vereint Timon Krause seine mentalen Fähigkeiten mit der Kunst des Stand-Ups.