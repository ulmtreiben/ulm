---
id: "1232018433898717"
title: Die Happy * ROXY Soundgarten
start: 2021-09-04 20:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/1232018433898717/
image: 193725859_10158544063902756_2217506441217616177_n.jpg
isCrawled: true
---
LIVEMUSIK UNTER FREIEM HIMMEL
EINTRITT FREI! 💛

DIE HAPPY - GUESS WHAT! TOUR 2020

Sechs Jahre sind vergangen seit „EVERLOVE“, dem letzten Album von Die Happy und GUESS WHAT? Im April 2020 erscheint mit GUESS WHAT! die neunte Studio-Platte der Band, die seit über 25 Jahren und mehr als 1200 Konzerten die großen und kleinen Bühnen Europas rockt.

Und GUESS WHAT?
Es ist immer noch Popcore mit mächtigen Grooves, harten Riffs und Refrains zum Mitgröhlen.

Und GUESS WHAT?
Es geht wieder auf Tour! Nach der letztjährigen „Love Suicide“ Tour, die schon einen Vorgeschmack auf das neue Songmaterial bot, geht es im Herbst 2020 mit dem neuen Album im Gepäck weiter. 

Und GUESS WHAT?
Aus Vier wurden Fünf! Mit dem langjährigen Freund Robert Kerner (ex-Bakkushan) an der Gitarre hat das Quartett Live-Verstärkung bekommen und ballert den Fans die Riffs zukünftig in „Stereo“ um die Ohren! 

Also GUESS WHAT?
Es wird laut und energiegeladen, wie man das von DIE HAPPY gewohnt ist. Zeit zum durchatmen gibt es während der spontanen und legendären Geschichten von Frontfrau Marta. Mit ihrer mitreißenden Art macht sie jedes Konzert zu einem einzigartigen Ereignis, wobei man nie so richtig weiß, was man zu hören bekommt.