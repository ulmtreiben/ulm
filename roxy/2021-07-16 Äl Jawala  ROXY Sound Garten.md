---
id: "1245129519237331"
title: Äl Jawala * ROXY Sound Garten
start: 2021-07-16 20:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/1245129519237331/
image: 202662519_10158600109867756_4059264832790084338_n.jpg
isCrawled: true
---
Wenn auf der Bühne ihr Feuerwerk zündet, gibt es kein Halten mehr. Auf unzähligen Percussions und zwei Saxophonen entfachen Äl Jawala das Feuer einer ganzen Gypsy Brass Band. Mal arabesk, orientalisch, mal urban und Club-orientiert, bieten sie eine einzigartige Mischung aus Party und Konzerterlebnis. Garantiert tanzbar, ohne dabei musikalischen Tiefgang und Facettenreichtum zu verlieren.

Außergewöhnliche Musiker, gemeinsam unterwegs in Sachen Balkan-Brass, treibenden Beats, ansteckender Lebensfreude und einem unbeirrbarem Instinkt für gelungene Stilmixe. Die Botschaft ist klar: Wir sind alle Eins, gehören zusammen, tanzen zusammen. Vielfalt ist unser größter Reichtum, lasst sie uns feiern! Äl Jawala gehören zu den Vorreitern der Balkan Beats-Szene. Anfang der 2000er Jahre sind sie unter den Ersten, die urbane Beats mit wilden, orientalischen Bläsersätzen kombinieren. Schon bei den ersten Strassenkonzerten in ihrer Heimatstadt Freiburg scharen sie beeindruckende Menschentrauben um sich.

Sie touren vom Atlantik bis zum Schwarzen Meer, gewinnen 2007 den Deutschen Creole Preis für Globale Musik, werden zu Konzerten nach China, Kanada, Jordanien, Indien eingeladen, veröffentlichen fünf Live- und vier Studioalben, starten einen weltweiten Remix-Contest und liefern den Soundtrack zur MTV Reihe "Rebel Music".

Synthies, Surfsounds, und Gesang flechten sie nach und nach als neue Klangfarben ein.

Doch unverwechselbarer Mittelpunkt ihres Sounds sind nach wie vor Saxophone, Percussions und Didgeridoo. Eine Band die immer wieder Neues erschafft, Ursprüngliches zelebriert und vor allem ständig in Bewegung ist.

Äl Jawala (Arabisch) = die Wandernden

www.jawala.de