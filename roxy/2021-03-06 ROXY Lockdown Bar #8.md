---
id: "131800728844884"
title: "ROXY Lockdown Bar #8"
start: 2021-03-06 20:30
link: https://www.facebook.com/events/131800728844884/
image: 156342299_10158337956022756_657558643580889822_o.jpg
teaser: Wow - schon unsere achte Lockdown Bar! Wahnsinn, wie schnell die Zeit vergeht.
  Wir freuen uns sehr, wenn ihr wieder Platz an unserer virtuellen Theke
isCrawled: true
---
Wow - schon unsere achte Lockdown Bar! Wahnsinn, wie schnell die Zeit vergeht. Wir freuen uns sehr, wenn ihr wieder Platz an unserer virtuellen Theke nehmt. 

Hier geht's zum Livestream:
Facebook: https://www.facebook.com/roxy.kultur/posts/10158343087072756
YouTube: https://www.youtube.com/watch?v=uZxIKHGsVuo 
ROXY Homepage: https://www.roxy.ulm.de/
ZOOM Aftershow (ab ca. 22 Uhr): https://zoom.us/j/92165431088?pwd=S3QvQm5jVGV3djBDN3ZLaVlpWFhXUT09 

Live-Musik mit Roadstring Army

Gitarren und Stimmen so rau wie das Kopfsteinpflaster von dem sie kommen. Roadstring Army ist die warme Tasse schwarzer Kaffee für den von Selbstzweifeln getriebenen Träumer. Mit dem Herzen denkend. Auf der Suche nach sich selbst. Der Bolzenschneider zum Anderssein.  In den vergangenen 3 Jahren hat die Ulmer Band auf über 200 Konzerten bewiesen, dass Bühnenleidenschaft gepaart mit Originalität ein breites Publikum berühren.  

Im Gespräch: Die Gesundheits-Clownninen und Glückslehrerinnen Katrin Jantz und Hanna Münch 

Liesel alias Katrin Jantz, 49 Jahre alt und Lotti alias Hanna Münch, 42 Jahre alt sind zusammengenommen schon 91 Jahre alt! Genauso wie der ein oder andere im Seniorenheim, wo die beiden regelmäßig aufmarschieren. Als Clownspaar besuchen die beiden Clowninnen schon seit über drei Jahren regelmäßig Senioreneinrichtungen, Behindertenheime und Kinderkliniken. Außerdem haben sie den Verein „Gute Clowns e.V.“ und besuchen zudem Schulen und Kindergärten um "Glücksunterricht" zu geben. Die beiden Frauen geben auch Glücks - und Humorseminare für Erwachsene, damit das innere Kind wieder zum Leben erweckt wird. 

Wibke-J. Richter, die Moderatorin der ROXY Lockdown Bar

Dem ROXY-Publikum ist Wibke-J. Richter seit vielen Jahren bekannt, denn normalerweise spielt sie regelmäßig mit den „Showbuddies" (Ulms erstem Improtheater-Ensemble) in den Kulturhallen.  Wibke-J. Richter inszeniert mit Schüler:innen Theaterstücke und hilft jungen Erwachsenen, ihren eigenen Weg der Kreativität auf der Bühne zu finden. Diese Woche moderiert sie zum zweiten Mal die ROXY Lockdown Bar. 

Mix-Drink-Tutorial mit Michl Brenner

Im letzten Sommer hat ROXY-Barchef Michl Brenner im ROXY Soundgarten gemeinsam mit seinem Team mehrere tausend Longdrinks gemixt. Im achten Teil seines Tutorials verrät er heute Abend weitere Tipps für Profi-Drinks. Das benötigt ihr dazu:

// White Lady 
6 cl London Dry Gin
2 cl Cointreau
1 cl Zuckersirup
3 cl Zitronensaft
1 Eiweiß
Orange Bitters

// Gin Basil Smash
6 cl Gin
10-15 Basilikumblätter
2 cl Zuckersirup
2 cl Zitronensaft

// Zubehör
Boston Shaker
Eis
Strainer (Sieb)

Noch Fragen? Dann bleib zum Zoom-Thementalk und Aftershow-Bargespräch

Im Anschluss an den Livestream (ca. 22 Uhr) könnt ihr euch wieder via Zoom mit allen Beteiligten unterhalten. Hier geht's zum Meetingraum: https://zoom.us/j/92165431088?pwd=S3QvQm5jVGV3djBDN3ZLaVlpWFhXUT09 

Meeting-ID: 921 6543 1088
Kenncode: 953363