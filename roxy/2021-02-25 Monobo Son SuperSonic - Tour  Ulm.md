---
id: "597643610948036"
title: Monobo Son "SuperSonic" - Tour | Ulm
start: 2021-02-25 20:00
end: 2021-02-25 23:00
address: ROXY.ulm
link: https://www.facebook.com/events/597643610948036/
image: 126899957_4944899898884036_6357729743289325546_n.jpg
teaser: Monobo Son feiert Geburtstag! Auf 10 bewegte Jahre kann die Band um Posaunist
  Manuel Winbeck nun zurückblicken. Viele Metamorphosen hat Monobo Son auf
isCrawled: true
---
Monobo Son feiert Geburtstag! Auf 10 bewegte Jahre kann die Band um Posaunist Manuel Winbeck nun zurückblicken.
Viele Metamorphosen hat Monobo Son auf diesem Weg vollzogen - stilistisch und personell war und bleibt die Combo stets in Bewegung. Und dennoch ist es gelungen einen ganz unverwechselbaren Eigensound zu kreieren, der sich nicht an Genregrenzen hält, sondern diese liebevoll überschreitet und Elemente zusammenführt, die auf den ersten Blick so gar nicht zusammenpassen wollen.
Die Geschichte von Monobo Son ist nicht zuletzt auch eine Geschichte von Freundschaft. Hier haben sich fünf Musiker zusammengefunden, die stilistisch aus völlig verschiedenen Baustellen kommen, die sich menschlich aber soviel zu sagen haben, dass sich eine neue Ebene des musikalischen Austauschs auftut. Was sie eint, ist die bedingungslose Liebe zur Livemusik. Die Magie des Bühnenmoments wird zelebriert egal ob im verschwitzten Berliner Undergroundclub, im Jugendzentrum tief im Bayerischen Wald oder auf dem Stadtplatz von Wolgograd. Der Zauber der diesem Augenblick innewohnt, vermag es, Menschen zusammenzubringen, egal wo sie herkommen und welche Sprache sie sprechen, und darin sehen die fünf Männer, wenn man sie fragt, ihre höchste und ehrenvolle Aufgabe. Aber Monobo Son sind keine Nostalgiker, auch wenn man bei ihrer Musik gern die Augen schließt, und so manche Erinnerung hochkommt. Der Blick ist immer nach vorne gerichtet. Neue Herausforderungen stehen an, und pünktlich zum Jubiläum kommt ein brandneues Album („SuperSonic“ VÖ 05.02.21). In diesem Sinne: Auf die nächsten Zehn!

Der Vorverkauf startet am 17.08.20 um 12:00 Uhr.