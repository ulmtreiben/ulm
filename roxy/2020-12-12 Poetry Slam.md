---
id: "3700864799965612"
title: Poetry Slam - verschoben auf 23.10.2021
start: 2021-10-23 20:00
end: 2021-10-23 22:15
locationName: ROXY.ulm
link: https://www.facebook.com/events/3700864799965612/
image: 122968017_10158019314827756_3695038167895629313_n.jpg
isCrawled: true
---
Und weiter geht die Verschieberei: Der Poetry Slam wurde vom 12.12.2020, 23.01.2021, 13.03.2021 und 11.09.2021 auf den 23.10.2021 verschoben. Tickets behalten ihre Gültigkeit oder können an den jeweiligen Vorverkaufsstellen zurückgegeben werden.
***
Da die Poetry Slams im November, Dezember, Januar und März nicht stattfinden konnten, bleiben die Tickets gültig für diesen Termin. Beide Novembertermine werden zu einem Termin zusammengelegt. Tickets behalten ihre Gültigkeit oder können an den jeweiligen Vorverkaufsstellen zurückgegeben werden.
***
Einlass 19:00 H
Beginn 20:00 H


Ein Poetry Slam ist der Dichterwettstreit unserer Zeit und macht Poesie zum Erlebnis. Gleichzeitig ist ein Poetry Slam aber auch eine Plattform für Jedermann - man muss sich nur trauen. Das Publikum ist mittendrin statt nur dabei und bestimmt durch seinen Applaus den Ausgang des Wettbewerbs. 

Wer selbst Gedichte, Geschichten oder Rap Texte schreibt, kann sich unter: poetry-slam@roxy.ulm.de anmelden und steht dann mit den Stars der Szene aus dem In- und Ausland auf der Bühne. Wichtig ist nur, dass man eigene Texte vorträgt, der Auftritt nicht länger als 6 Minuten dauert, man nicht auf Hilfsmittel wie Kostüme oder Instrumente angewiesen ist und man auch für den Fall der Fälle gerüstet ist und einen weiteren Text für das Finale dabei hat. 

Schreien, flüstern, jaulen, keuchen, pfeifen, rappen – all das ist jedoch erlaubt, um im Wettstreit gegeneinander anzutreten. Die Waffen: Poesie, Stimme und Körper. 

Zu befürchten hat man nichts, steht doch beim Poetry Slam der Respekt vor dem Poeten im Mittelpunkt.

***
Die Poetry Slams finden aufgrund der aktuellen Lage mit weniger Slammern und ohne Pause statt. Bitte beachtet: Unsere Bestuhlung ist mit ausreichend Abständen aufgebaut. Sucht euch entsprechend eurer Gruppengröße geeignete Sitzgruppen, ggf. muss sich eure Gruppe für die Zeit der Vorstellung trennen. Stühle dürfen nicht verschoben werden. Tragt eure eigene Mund-Nasen-Bedeckung, die ihr auf eurem Sitzplatz abnehmen könnt. Wir werden eure Kontaktdaten aufnehmen (Telefonnummer oder Adresse), für vier Wochen verwahren und nur im Falle eines Coronafalls an das Gesundheitsamt aushändigen. Danke für Geduld und Rücksicht auf andere Gäste.

Mit freundlicher Unterstützung von
scanplus GmbH
ibis budget Ulm City

Medienpartner
swp.de