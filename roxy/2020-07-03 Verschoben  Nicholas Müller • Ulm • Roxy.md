---
id: "206337437414170"
title: Verschoben * Nicholas Müller • Ulm • Roxy
start: 2020-07-03 21:00
end: 2020-07-04 00:00
address: ROXY.ulm
link: https://www.facebook.com/events/206337437414170/
image: 83106866_10157197133903719_3613223701132083200_o.jpg
teaser: Die Veranstaltung wurde vom 29.04.20 auf den 03.07.20 verschoben! Die Tickets
  behalten ihre Gültigkeit. **  Nicholas Müller Zum Sterben Zuviel – Ein A
isCrawled: true
---
Die Veranstaltung wurde vom 29.04.20 auf den 03.07.20 verschoben!
Die Tickets behalten ihre Gültigkeit.
**

Nicholas Müller
Zum Sterben Zuviel – Ein Abend mit Nicholas Müller & dem Tod.
 
03.07.2020 · ROXY.ulm
Beginn: 21 Uhr
 
 NICHOLAS MÜLLER, ehemals Sänger von „Jupiter Jones“ und später der Band „von Brücken“, Autor des Spiegel-Bestsellers „Ich bin mal eben wieder tot“ (2017), in dem er seine Angststörung verarbeitete, kommt mit dem Programm „Zum Sterben Zuviel – Ein Abend mit dem Tod & Nicholas Müller“ auf die Bühnen der Bundesrepublik.
 
 „Wenn’s ein Thema gibt, auf das wir uns in seiner Unausweichlichkeit einigen müssen, dann ist es der Tod. Kann jedem mal passieren. Kommt mir jetzt bitte nicht mit den dreieinhalb kryogenisch eingefrorenen, armen Irren. Der mitteleuropäische Tod ist ein Flüsterthema, die große vorgehaltene Hand, findet nur in Filmen statt und muss immer mit maximal zugekniffener Haltung abgearbeitet werden. Wir staksen durch den Tod, wie wir’s auch im Leben tun: Trauerdogmen, Geschlechterrollen, Erdmöbelshopping in Eiche-massiv, Frikadellchen zum Leichenschmaus, allein das Wort schon! Ich sag noch nicht mal, dass das zwingend falsch wäre. Ich finde nur, wir sollten mehr darüber sprechen. Und singen! Damit wir endlich mal klarkommen, weil’s uns alle angeht.“
 
 „Zum Sterben zu viel“ begrüßt das Publikum mit einem bunten Strauß aus Schwarz: Lieder über den Tod, geschrieben von anderen, intoniert von NICHOLAS MÜLLER und musikalisch begleitet vom grandiosen Neonorchester, hervorragende Talkgäste, musikalische Freunde am Mikrofon, jeden Abend frische Gedanken übers große Gammeln, eine Mischung aus kleiner Revue und großer Geste, Konzert und Konzept, Talkshow und Schweigeminute. Immer voller Respekt, aber nie ohne Humor. Entspannen wir uns vorm letzten, großen Entspannen. Schaden kann’s nicht. Tod und Spiele!