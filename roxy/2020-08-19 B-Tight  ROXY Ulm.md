---
id: "2495662390654194"
title: B-Tight * ROXY Ulm
start: 2021-03-21 20:00
end: 2021-03-21 22:00
address: ROXY.ulm
link: https://www.facebook.com/events/2495662390654194/
teaser: Best of Tour 2020  B-Tight der alte Sack wird 40. Seit über 20 Jahren nun auf
  der Bühne hat er Bock 2020 nochmal richtig abzureißen. Einmal eine Reise
isCrawled: true
---
Best of Tour 2020

B-Tight der alte Sack wird 40. Seit über 20 Jahren nun auf der Bühne hat er Bock 2020 nochmal richtig abzureißen. Einmal eine Reise durch alle Livehits von 2000-2020 – und das Publikum darf live mitbestimmen was es hören möchte.

Vor jeder Show gibt es wie immer ein Meet&Greet eine Stunde vor offiziellem Einlass. Diesmal ein bisschen besonderer, da er sein neues Album im Gepäck hat, was die Fans vorab schon mal hören dürfen und vielleicht ist ja was dabei, was man dann bereits live mal sehen möchte.

Mit dem neuen Album geht´s wieder zurück zu den Wurzeln und die Oldschooler werden sich auf jeden Fall freuen.

Hier schon mal die erste Albumkritik zu „Bobby Dick“:

B-Tight - Bobby Dick

Wir schreiben das Jahr 2019. Die Deutschrap-Szene hat endlich die Formel gefunden, um sicher in die Charts zu kommen. Ein bisschen Autotune, ein bisschen Marken-Namedropping, ein bisschen drauf achten, dass der Song nicht zu lang ist - zack, die Kasse klingelt.

Und alle machen mit. Moment mal, alle? Nein. Es gibt da einen, den das alles nicht interessiert. Einen, der auch nach 20 Jahren am Mic keinen Bock hat, sich zu verbiegen, um irgendwelchen Erwartungen zu entsprechen. Und dieser eine ist B-Tight, auch bekannt als Bobby Dick.

Und genau so heißt auch sein neues Album: Bobby Dick. Anstatt sich vorsichtig an den Zeitgeist anzubiedern, zieht B-Tight sein Ding weiter voll durch. Anstatt auf die vielen gutgemeinten Ratschläge zu hören, die ihm von allen möglichen Seiten vor dem neuen Album gemacht wurden, vertraut er lieber auf sein eigenes Herz, sein eigenes Gehirn und seine eigenen Eier. 

Denn die haben ihn bisher immer am besten beraten. Sollen die anderen doch zu “Marionetten” werden - im gleichnamigen Song lässt er seinem Hass auf Major-Labels und deren verlogene Strippenzieher-Spielchen freien Lauf. Direkt aus dem Leben gegriffen - wie immer.

Eins wollte Bobby noch nie sein: Everybody’s Darling. Er macht “Keine Gefangenen”, ist mal ein Kind, mal ein Pimp, manchmal auch der Sündenbock für die Verlogenheit der bürgerlichen Gesellschaft, ab und zu auch einfach mal high. In einer Szene voller Klone und fremdgesteuerter Karrieristen zieht er es vor, Außenseiter zu bleiben.

Und von außen sieht er so manches klarer  - vor allem aber ist er freier und vollkommen unabhängig. Ein rockiger Song mit Tamas, Swiss und Ferris? Einfach machen. Ein Song über das eigene Geschlechtsorgan mit Kitty Kat? Einfach machen. Mit Rhymin Simon den “Rentnerstyle” fahren und über alles und jeden herziehen? Machen, einfach mal machen. 

Wenn B-Tight aber mal ernst wird, dann richtig. Mit “Mach’s wieder gut” unterstützt er die Helfer-Community GoVolunteer, die für ehrenamtliches Engagement steht und ohne finanzielle Interessen Begegnungen und Austausch ermöglicht. 

So isser halt, der Bobby Dick. Große Klappe, großes Herz, großer… na, ihr wisst schon. Zu groß für Deutschrap und seine täglichen Intrigen jedenfalls. Und das ist auch gut so.

***
Das Konzert wurde vom 22.03. und 19.08.2020 verschoben! Tickets behalten ihre Gültigkeit.