---
id: "1679748888879916"
title: Balagan! am Kornhausplatz * Ulmer Kultursommer
start: 2021-07-24 20:00
address: Kornhausplatz, 89073 Ulm
link: https://www.facebook.com/events/1679748888879916/
image: 205798057_10158616032767756_5498083305815500320_n.jpg
isCrawled: true
---
Nach einer gefühlten Ewigkeit von pandemischen Einschränkungen setzt Helena Waldmann beim Neustart der Live-Kultur auf ein wahrhaft ungezähmtes Konzept: Balagan. Wörtlich übersetzt am ehesten mit „Unordnung“ wiederzugeben, steht Balagan für das ursprüngliche Volkstheater: ein stürmisches, wildes Jahrmarkt-Spektakel, auf der Wanderschaft von Stadt zu Stadt wie die historische Commedia dell’arte.

Für Helena Waldmanns Pop-up-Fest für Wagenlenker und alle, die dabei sind, sind u.a. kurze Auftritte auf motorisierten Lastenrädern geplant, die viel Sympathie ausstrahlen, Lebensfreude signalisieren und nach denen man sich schmunzelnd umdreht - Marke Piaggio Ape. Sie sind Vehikel, Bühne und Thron zugleich.

Begleitend zur 30-minütigen Performance im öffentlichen Raum läuft die fantastische Musik der Hamburger Band MEUTE im DJ-Set von Daniel Stenger – das Ganze wird ein Jahrmarkt, ein Fest der Haltung, lebendig, bunt, emotional.

Eintritt frei!

Gefördert von der Beauftragten der Bundesregierung für Kultur und Medien.