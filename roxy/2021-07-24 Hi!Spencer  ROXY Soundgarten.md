---
id: "313170543814035"
title: Hi!Spencer * ROXY Soundgarten
start: 2021-07-24 20:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/313170543814035/
image: 192705234_10158544137677756_949250413552773673_n.jpg
isCrawled: true
---
LIVEMUSIK UNTER FREIEM HIMMEL
EINTRITT FREI! 💛

Hi! Spencer: 
Eine Indie-Punk-Band ergreift die Flucht nach innen und beweist sich 2020 als gereifte Einheit. 
Nach der Veröffentlichung ihres zweiten Albums „Nicht raus, aber weiter“ und der dazugehörigen Tour, mit ausverkauften Konzerten in ganz Deutschland, legen die fünf Osnabrücker jetzt nach. 
Mit neuer Single und neuer Booking-Agentur zuendstoff booking nehmen Hi! Spencer den Rückenwind des letzten Jahres auf und entwickeln sich als Band weiter.
Mehr denn je blickt die Band ihren Ängsten in die Augen, versucht standzuhalten und sich mit ihnen zu versöhnen – Sie thematisieren Unzulänglichkeit und Scheitern und machen dabei selbiges zur Überwindungsstrategie. 
So entsteht auch durch ihre neuen Single eine tosende Indie- Explosion, die kein Shirt trocken und keine Füße stillstehen lässt. Dabei verwehrt sich der Song des reinen Indie-Stigmas. Hi! Spencer finden neue Ohrwurm-Nischen eines jeden Kettcar- und Muff Potter-Fans und sprechen da weiter, wo Jupiter Jones einst aufgehört haben. 
Bandgründung 2012, Studioalben 2015 und 2019 und frische Singles 2020: Die Spotify-Klicks haben für Hi! Spencer die Millionen-Marke längst hinter sich gelassen. Mit der Unterstützung ihres Labels Uncle M sorgt der Hi! Spencer-Sound regelmäßig für strahlende Gesichter in diversen Radio-Redaktionen sowie auf Clubshows und auf zahlreichen Festivals in diesem Sommer. 