---
id: "551148685698867"
title: Unter meinem Bett * ROXY Ulm
start: 2021-02-20 15:00
end: 2021-02-20 17:00
address: ROXY.ulm
link: https://www.facebook.com/events/551148685698867/
image: 84324576_1507004476129734_7734003285568258048_o.jpg
teaser: "Live mit Sven van Thom, Francesco Wilking, Pauken & Planeten, Deniz
  Jaspersen, Martha Wilking, Ove & Larissa Pesch. Moderation: Bernd
  Begemann  Endlic"
isCrawled: true
---
Live mit Sven van Thom, Francesco Wilking, Pauken & Planeten, Deniz Jaspersen, Martha Wilking, Ove & Larissa Pesch.
Moderation: Bernd Begemann

Endlich wieder coole Lieder von Deutschlands beliebtesten Singer/Songwritern: Die neue „Unter meinem Bett“ erschien am 18.11.2019!

Nach inzwischen schon vier sehr erfolgreichen Alben erscheint nun das fünfte, welches Kinder und Eltern wieder gleichermaßen begeistern wird. Die Lieder sind mal frech und laut, mal leise und tiefsinnig – und dabei immer einfallsreich und überraschend anders! Auf dem Album sind dieses mal dabei: Pohlmann, Sebastian Block, Oliver Minck, Gereon Klug & Andreas Dorau, Neufundland u. v. m.

„Unter meinem Bett“ aus dem Hause Oetinger ist mittlerweile aus keinem Kinderzimmer mehr wegzudenken.

2020 geht „Unter meinem Bett“ auch wieder auf Tour, die großartige UMB-Liveband und viele tolle Gastsänger präsentieren ihre Songs live. Moderiert von Bernd Begemann.

Ein Konzerterlebnis für alle Altersklassen!

***

Empfindlichen Kindern empfehlen wir das Tragen eines Kapselgehörschutzes.

***
Die Veranstaltung wurde vom 04.04.2020  auf den 20.02.2021 verschoben. Die Tickets behalten ihre Gültigkeit.