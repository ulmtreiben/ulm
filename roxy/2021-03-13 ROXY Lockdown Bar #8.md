---
id: "207927271126347"
title: "ROXY Lockdown Bar #9"
start: 2021-03-13 20:30
end: 2021-03-13 22:30
link: https://www.facebook.com/events/207927271126347/
image: 158924082_10158352107647756_8629132259295292239_o.jpg
teaser: Samstag ist Lockdown Bar Tag! Setz dich zu uns und genieße einen weiteren
  tollen Abend mit interessanten Gesprächen, mitreißender Live-Musik, eindruck
isCrawled: true
---
Samstag ist Lockdown Bar Tag! Setz dich zu uns und genieße einen weiteren tollen Abend mit interessanten Gesprächen, mitreißender Live-Musik, eindrucksvollem Tanz und natürlich herrlichen Cocktails. Dieses Mal wird die Lockdown Bar wieder von Ariane Müller moderiert. 

Hier kannst du zuschauen:
YouTube: https://www.youtube.com/watch?v=t-yJtgmLGIo
Facebook: https://www.facebook.com/roxy.kultur/posts/10158358673157756?notif_id=1615553226168960&notif_t=live_video_schedule_broadcaster&ref=notif
ROXY-Homepage: https://www.roxy.ulm.de/
ZOOM (Aftershow nach dem Livestream): 
https://zoom.us/j/98997933065?pwd=b283THZ4b2FWc2E1ZWt2RFRvcXZnZz09
 
Meeting-ID: 989 9793 3065
Kenncode: 771726

Live-Musik mit Andreas Kümmert

Die Musik von Andreas Kümmert ist erdig, ehrlich, straight und unter die Haut gehend. Kurzum: Rundum gelungen. Seine neue Single "Sweet Oblivion" beschreibt ein Grundgefühl, das tausend unterschiedliche Facetten haben kann – doch jeder von uns kennt es. Wie leicht wird man in den Wirren unserer Zeit als Mensch übersehen? So vieles passiert im Trubel des Alltags, aber man selbst fühlt sich, als würde man irgendwie darin untergehen, unsichtbar sein. Und am Ende ist jeder für sich einsam. Und obwohl die Single sowohl musikalisch wie auch thematisch absolut zeitlos ist, scheint das Thema mehr denn je in unser aktuelles Geschehen zu passen.

Im Gespräch: Magdi Aboul-Kheir, Chefreporter der Südwest Presse

Magdi Aboul-Kheir ist den Zeitungsleser:innen in Ulm und über Ulm hinaus seit Langem bekannt. Seit 20 Jahren ist er Redakteur bei der „Südwest Presse“, der größten Zeitung in der Region. Viele Jahre davon hat er im Kulturressort gearbeitet. Seit 2019 ist er Chefreporter beim lokalen und regionalen Ressort der „Südwest Presse“ und kommentiert dort immer wieder auch die Tagespolitik. In der ROXY Lockdown Bar wird er gemeinsam mit Moderatorin Ariane Müller über die Angebote der verschiedenen Parteien und Kandidat:innen zur Wahl reden – und dabei natürlich auch auf die Aussagen der Parteien zum Kulturbereich achten. 

#ShowMeYourMoves mit Jasmine Ellis und Crew

Mit dem Konzept für ihren neuen Tanzfilm "Onomateopoeia" begeisterte die kanadische Choreografin Jasmine Ellis die Jury des TanzLabors und konnte sich somit die erste Residenz im ResearchLab sichern. Hier wird sie mit den Tänzer:innen Adaya Berkovich (Israel) und Woosang Jeon (Südkorea) an „Onomatopoeia“ arbeiten und ermöglicht in der ROXY LOCKDOWN BAR einen ersten exklusiven Probeneinblick in ihr künstlerisches Schaffen.

Mix-Drink-Tutorial mit Michl Brenner

Im letzten Sommer hat ROXY-Barchef Michl Brenner im ROXY Soundgarten gemeinsam mit seinem Team mehrere Tausend Longdrinks gemixt. Für die Drinks am Samstag benötigt ihr:

//Aperol Sour
6cl Aperol
3cl Zitronensaft
3cl Zuckersirup
Ei

// Manhattan
6cl Rye Whiskey
3cl roter Wermut
3 dashes Angostura Bitter

//Zubehör
Boston Shaker
Eis
Rührglas
Barlöffel
Strainer

Noch Fragen? Dann bleib zum Zoom-Thementalk und Aftershow-Bargespräch

Im Anschluss an den Livestream (ca. 22 Uhr) ist wieder ein persönliches Bargespräch mit den Beteiligten über Zoom geplant. Der Link dafür findet sich am Samstagabend hier und in den Kommentaren zum Livestream.