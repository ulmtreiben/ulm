---
id: "527079121940810"
title: DJ Klaus Fumy * Roxy Sound Garten
start: 2021-07-15 18:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/527079121940810/
image: 212081702_10158638047277756_2386699966891698020_n.jpg
isCrawled: true
---
Wir freuen uns, euch in unserem Sound Garten begrüßen zu dürfen. Neben dem regulären Biergartenbetrieb versüßen euch unterschiedliche DJs die Ohren.

Eintritt frei, Spenden für die musikalische Unterhaltung wird gern entgegen genommen.

Öffnungszeiten:
Mittwoch - Samstag: ab 17:00 H
Sonntag: ab 15:00 H

Alle aktuellen Bestimmungen bzgl. Corona findet ihr auf unserer Homepage www.roxy.ulm.de
