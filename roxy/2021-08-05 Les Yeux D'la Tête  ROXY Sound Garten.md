---
id: "1165694190566024"
title: Les Yeux D'la Tête * ROXY Sound Garten
start: 2021-08-05 20:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/1165694190566024/
image: 206196721_10158609567507756_1489031751757202010_n.jpg
isCrawled: true
---
Musik unter freiem Himmel!
Eintritt frei! 💛

Die Musiker, die die Gruppe bilden, haben alle einen unterschiedlichen Hintergrund, und das macht ihre Musik so reichhaltig.

Benoit Savard und Guillaume Jousselin, eher Autodidakten, gefüttert mit Rock und Gesang, trafen sich an der Schule für aktuelle Musik ATLA in dem Wunsch, ihre Ausbildung zu perfektionieren.

Antoine Allièse am Akkordeon ist ebenfalls Mitbegründer der Gruppe. Er lernt Akkordeon bei ATLA und in der Zirkus- und Straßenkunst. Er ist der Initiator der Gründung des Vereins Fais et Ris.

Im Jahr 2016 wurde er am Akkordeon für die Liberté Chérie-Tournee durch Eric Allard-Jacquin ersetzt, der am Konservatorium und am Didier Loockwood Music Centre ausgebildet wurde. Er ist bekannt für seine zahlreichen internationalen Auszeichnungen.

Eddy Lopez, Saxophonist seit seinem 6. Lebensjahr, kommt von der klassischen Musik. Anerkannt durch seine zahlreichen Preise im Fach Saxophon, wurde er Mitglied des CNSM in Paris.

All diese Einflüsse und Erfahrungen machen Les Yeux D'la Tête zu einer Gruppe, die Musik als einen Austausch, als eine Begegnung, eine privilegierte Beziehung voller Sensationen sieht, sowohl zwischen den Musikern als auch mit dem Publikum.