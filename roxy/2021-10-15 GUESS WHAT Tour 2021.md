---
id: "718640481996862"
title: GUESS WHAT Tour 2021
start: 2021-10-15 19:00
end: 2021-10-15 23:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/718640481996862/
image: 82458106_10157109086136025_3732689668169269248_n.jpg
isCrawled: true
---
DIE HAPPY - GUESS WHAT! TOUR 2020. Sechs Jahre sind vergangen seit „EVERLOVE“, dem letzten Album von Die Happy und GUESS WHAT? Am 10.04.2020 erscheint mit GUESS WHAT! die neunte Studio-Platte der Band, die seit über 25 Jahren und mehr als 1200 Konzerten die großen und kleinen Bühnen Europas rockt.