---
id: "542825256565357"
title: Jochen Malmsheimer * ROXY Ulm
start: 2021-05-14 20:00
end: 2021-05-14 23:00
address: ROXY.ulm
link: https://www.facebook.com/events/542825256565357/
image: 78579339_10157002014187756_7814255874775449600_o.jpg
teaser: Ersatztermin vom 27.11.2019 und vom 26. April 2020 Bereits gekaufte Tickets
  behalten ihre Gültigkeit. ***  Dogensuppe Herzogin - ein Austopf mit Einla
isCrawled: true
---
Ersatztermin vom 27.11.2019 und vom 26. April 2020
Bereits gekaufte Tickets behalten ihre Gültigkeit.
***

Dogensuppe Herzogin - ein Austopf mit Einlage

Machen wir uns doch nichts vor: Kabarett ist dieser Tage wichtiger denn je!

Die gefühlte Arschlochdichte und Idiotenkonzentration, die Sackgesichtsüberfülle und Kackbratzendurchseuchung, die allgemeine, bimssteinerne Generalverblödung und präsenile Allgemeinabstumpfung, kurz: die cerebrale Fäulnis in diesem Land war, subjektiv gefühlt, immer schon hoch, aber nun läßt sich dieses trübe Faktum nicht mehr nur im Experiment nachweisen, sondern ist für uns alle, die wir über ein entwickeltes humanoides Sensorium, über Geist, Witz, Verstand und Geschmack verfügen, fühlbar im Freiland angekommen.

Und dort im Freiland zeltet Jochen Malmsheimer, bereit, sich diesem geradezu enzephalen Unsinn in den Weg zu stellen, gegürtet mit dem Schwert der Poesie, gewandet in die lange Unterhose tröstenden Mutterwitzes und weiterhin unter Verzicht auf jegliche Pantomime.

Denn wie schon Erasco von Rotterdam wußte: Wer oft genug an's Hohle klopft, der schenkt der Leere ein Geräusch.

***

Jochen Malmsheimer, geboren 1961 in Essen, ist ein vielfach preisgekrönter Kabarettist (u.a. Deutscher Kleinkunstpreis, Deutscher Kabarettpreis). In zahlreichen Bühnenprogrammen hat er eine ganz eigene Kunstform geschaffen: das epische Kabarett. Einem größeren Publikum wurde er vor allem durch seine regelmäßigen Auftritte bei »Neues aus der Anstalt« (ZDF) bekannt. Sein künstlerisches Werk ist in dem jüngst erschienenen Band »Gedrängte Wochenübersicht - Ein Vademecum der guten Laune« nachzulesen.