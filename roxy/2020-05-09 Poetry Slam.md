---
id: "183974122905818"
title: Poetry Slam
start: 2020-05-09 20:00
end: 2020-05-09 22:30
address: ROXY.ulm
link: https://www.facebook.com/events/183974122905818/
image: 88261189_10157301493212756_5259963700571275264_n.jpg
teaser: Ein Poetry Slam ist der Dichterwettstreit unserer Zeit und macht Poesie zum
  Erlebnis. Gleichzeitig ist ein Poetry Slam aber auch eine Plattform für Je
isCrawled: true
---
Ein Poetry Slam ist der Dichterwettstreit unserer Zeit und macht Poesie zum Erlebnis. Gleichzeitig ist ein Poetry Slam aber auch eine Plattform für Jedermann - man muss sich nur trauen. Das Publikum ist mittendrin statt nur dabei und bestimmt durch seinen Applaus den Ausgang des Wettbewerbs. 

Wer selbst Gedichte, Geschichten oder Rap Texte schreibt, kann sich unter: poetry-slam@roxy.ulm.de anmelden und steht dann mit den Stars der Szene aus dem In- und Ausland auf der Bühne. Wichtig ist nur , dass man eigene Texte vorträgt, der Auftritt nicht länger als 6 Minuten dauert, man nicht auf Hilfsmittel wie Kostüme oder Instrumente angewiesen ist und man auch für den Fall der Fälle gerüstet ist und einen weiteren Text für das Finale dabei hat. 

Schreien, flüstern, jaulen, keuchen, pfeifen, rappen – all das ist jedoch erlaubt, um im Wettstreit gegeneinander anzutreten. Die Waffen: Poesie, Stimme und Körper. 

Zu befürchten hat man nichts, steht doch beim Poetry Slam der Respekt vor dem Poeten im Mittelpunkt.

Mit freundlicher Unterstützung von
scanplus GmbH
ibis budget Ulm City

Medienpartner
swp.de