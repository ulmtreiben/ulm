---
id: "872692303638126"
title: DJ BenJammin' Van der Strack * ROXY Sound Garten
start: 2021-08-25 18:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/872692303638126/
image: 239494994_10158743978472756_2969301048074340942_n.jpg
isCrawled: true
---
Wir freuen uns, euch in unserem Sound Garten begrüßen zu dürfen. Neben dem regulären Biergartenbetrieb versüßen euch unterschiedliche DJs die Ohren.

Heute: DJ Ben Jammin' Van der Strack

Eintritt frei, Spende für die musikalische Unterhaltung wird gern entgegen genommen.
Der Biergarten ist ab 17:00 H geöffnet.
***
Alle aktuellen Bestimmungen bzgl. Corona findet ihr auf unserer Homepage www.roxy.ulm.de