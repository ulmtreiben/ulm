---
id: "114202587408975"
title: ANTIHELD * ROXY Soundgarten
start: 2021-08-20 20:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/114202587408975/
image: 190621508_10158535089082756_1592892605404792177_n.jpg
isCrawled: true
---
LIVEMUSIK UNTER FREIEM HIMMEL
EINTRITT FREI! 💛

ANTIHELD, ursprünglich eine Band von der Stuttgarter Straße. Von heut auf morgen findet man sich plötzlich im richtig dicken Major Business wieder, schießt beide Alben in die Charts,
spielt auf Southside, Hurricane und co, spielt Touren mit ausverkauften Shows,
um dann keine 3 Jahre später mit allem zu brechen und sich zu erinnern, wo man eigentlich herkommt. Um mit "disturbia" ein Album zu schreiben, dass es so in der deutschsprachigen Welt bisher noch nicht gab. 
Der erste Eindruck der Plattenfirma "Arising Empire" skeptisch, nahezu kritisch. Zu wenig Pop, zu wenig Radio.
Der zweite Eindruck; Zitat: "Das kann das Album des Jahres werden." 
Man kennt diese Entwicklung von vielen Bands. Man strotzt vor Credibility und schreibt seine Musik nicht für die Geld-machende-anti-Kunst-Industrie. Dann merkt man, dass es immer schwerer wird den Kühlschrank voll zu machen und springt mit dem Kopf voraus in die inhaltslose, austauschbare Radio-Landschaft. Den fünf Stuttgartern erging es ähnlich, nur eben anders herum. 
Nachdem die Band mit dem ersten großen Major-Deal so unverhofft wie heftig in das Haifischbecken krachte, folgte mit dem 2. Album "Goldener Schuss" und dem damit verbundenen Labelwechsel zu Arising Empire eine erste musikalische Emanzipation. Raus aus der Welt voller Einheits-Pop, hin zu einem Album, für das es noch keine vordefinierten Zielgruppen gibt. Die Lyrics deep & bitter, der Sound nicht der einer klassisch deutschen Rockband. Ein Mix aus Highly Suspect & Thrice, mit ein bisschen Grunge, Punk und Rap-Attitüde. 
Ende 2020 bringen ANTIHELD ihr drittes und bisher wichtigstes Album.
Ein Album wie ein Film von Scorsese - düster, geschrieben auf Weißwein in der Isolation einer Pandemie. Das politische Bullauge der in Privilegien lebenden mit Blick auf das Elend scheinbar unendlich weit entfernter Menschen, die Selbstreflexion des menschlichen Versagens, das Ersticken der Ängste im exzessiven Rausch, die Abrechnung mit der Kirche, die Auseinandersetzung mit dem Tod des eigenen Managers, bis hin zur Hoffnung auf Gretas vermeintliche Rettung der Welt und unser längst überfälliges Umdenken. Belanglose Popmusik ist tot. 