---
id: "284164832877634"
title: "Kinderprogramm des SJR Ulm: Kreativecke mit Buttonmaschine"
start: 2020-08-30 15:00
end: 2020-08-30 19:00
address: ROXY.ulm
link: https://www.facebook.com/events/284164832877634/
teaser: Wer Lust auf Malen oder Basteln hat, ist hier genau richtig. Egal ob klassisch
  mit Stift und Papier oder auch kompliziertere Bastelaufgaben, für jeden
isCrawled: true
---
Wer Lust auf Malen oder Basteln hat, ist hier genau richtig. Egal ob klassisch mit Stift und Papier oder auch kompliziertere Bastelaufgaben, für jeden ist was dabei.

Maximale Teilnehmerzahl: 10
15:00-19:00 H

Das Angebot ist kostenlos (und findet unter der Berücksichtigung der derzeitigen Hygiene- und Sicherheitsmaßnahmen statt). Wer mag, kann sich gerne im Voraus mit Name, Adresse und Telefonnummer unter knehr@sjr-ulm.de anmelden.

Kooperation von Stadtjugendring e.V. und ROXY gGmbH