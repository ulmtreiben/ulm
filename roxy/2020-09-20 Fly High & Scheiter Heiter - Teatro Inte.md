---
id: "519751158730110"
title: Fly High & Scheiter Heiter - Teatro International * ROXY Ulm
start: 2020-09-20 19:30
end: 2020-09-20 21:30
address: ROXY.ulm
link: https://www.facebook.com/events/519751158730110/
image: 88261074_10157302101792756_1370695733996421120_o.jpg
teaser: Verschoben vom 14.05.2020. *** Jedem Scheitern wohnt ein Träumen inne …
  Migranten sind Experten des Träumens und Scheiterns. Sie brechen auf mit ihren
isCrawled: true
---
Verschoben vom 14.05.2020.
***
Jedem Scheitern wohnt ein Träumen inne … Migranten sind Experten des Träumens und Scheiterns. Sie brechen auf mit ihren Zielen und Visionen, stoßen an Grenzen, überwinden sie oder suchen neue Wege und bringen durch ihren Mut und ihre Kreativität bereichernde Perspektiven und Ideen in die Gesellschaft. 
Ausgehend von der Figur Berblingers und seines Traums vom Fliegen beschäftigt sich Teatro International in seinem neuen Stück mit dem Scheitern auf der Probebühne des Lebens. Im Vorfeld des Theaterprojekts hat Teatro International dafür „Scheiter-Geschichten“ von Menschen allen Alters und unterschiedlicher sozialer und kultureller Herkunft gesammelt. Diese werden Teil eines Geschichten-Parcours am 15. und 30. Mai entlang der Donau im Rahmen des Berblinger-Jubiläums. 

Regie: Claudia Schoeppl
Mitwirkende: Ensemble des Teatro International
Im Rahmen des Berblinger-Jubiläums der Stadt Ulm

Kooperationspartner: 
Ulmer Volkshochschule
Buchhandlung Aegis
Kulturabteilung Stadt Ulm
Landesamateurtheaterverband Baden-Württemberg