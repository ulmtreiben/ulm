---
id: "204831858007181"
title: Cavewoman * ROXY Ulm
start: 2021-09-28 20:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/204831858007181/
image: 142744983_10158248997787756_7768854057091847532_n.jpg
isCrawled: true
---
Ursprünglich war Cavewoman für den 18.04.2020 geplant, was auf den 24.10.2020 verschoben werden musste. Aufgrund von Kapazitätsbeschränkungen wurde dieser Termin gesplittet: alle bis dahin verkauften EVENTIM-Tickets gingen auf den 10.02.2021 (zuletzt 04.06.), alle reservix-Tickets auf den 11.02.2021 (zuletzt 05.06.).

Der neue Ersatztermin für den 11.02.2021 und 05.06.2021 ist nun der 28.09.

Die Tickets behalten ihre Gültigkeit oder können an der jeweiligen Vorverkaufsstelle zurückgeben werden, an der sie gekauft wurden.



Die Tickets behalten ihre Gültigkeit oder können an der jeweiligen Vorverkaufsstelle zurückgeben werden, an der sie gekauft wurden.

****

Praktische Tipps zur Haltung und Pflege eines beziehungstauglichen Partners
Gespielt von Heike Feist

Sex, Lügen & Lippenstifte! In dieser fulminanten Solo-Show rechnet Cavewoman Heike mit den selbsternannten "Herren der Schöpfung" ab. Mal mit der groben Steinzeitkeule, mal mit den spitzen, perfekt gepflegten Nägeln einer modernen Höhlenfrau aber immer treffend und zum Brüllen komisch!

Doch keine Sorge: Cavewoman ist kein feministischer Großangriff auf die gemeine Spezies Mann. Freut euch vielmehr auf einen vergnüglichen Blick auf das Zusammenleben zweier unterschiedlicher Wesen, die sich einen Planeten, eine Stadt und das Schlimmste: Eine Wohnung teilen müssen!

Hierzulande haben mehr als 600.000 Zuschauer in knapp 2.000 Shows und in etwa 50 verschiedenen Spielorten das Stück gesehen. Damit gehört Cavewoman zu den erfolgreichsten One-Woman-Shows überhaupt und feiert zunehmend auch internationale Erfolge. Cavewoman ist eine Produktion der Theater Mogul GmbH.

Regie: Adriana Altaras
Buch: Emma Peirson