---
id: "346153836930918"
title: Guacáyo * ROXY Soundgarten
start: 2021-07-22 20:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/346153836930918/
image: 193703205_10158544134127756_5629628814628904747_n.jpg
isCrawled: true
---
LIVEMUSIK UNTER FREIEM HIMMEL
EINTRITT FREI! 💛

Wer sich mitnehmen lässt von den Songtexten und den Vibes, der spürt die Kraft dahinter und versteht die Verbundenheit. Mit der Natur, mit allen Menschen und mit sich selbst. Der spürt die Gänsehaut, die Freiheit und natürlich die fetten Bässe. 
Die Hamburger Band Guacáyo feiert Toleranz und Authentizität. Guacáyos Songs bewegen das Publikum innerlich und äußerlich. Verbundenheit und Bässe sind spürbar und bei Themen wie dem Umweltschutz oder dem „Unshaming“ von Körperbehaarung bleibt niemand unberührt. Musikalisch bedienen Guacáyo dabei keine Schubladen: Einflüsse aus Reggae, Dub und Hip-Hop münden in einen frechen Indie-Pop-Sound. Während Sophies unverwechselbare Stimme über den farbenfrohen Beats erklingt, groovt die Menge und singt mit. Die quirlige 29-Jährige wirkt auf der Bühne sehnsüchtig oder provozierend, als weise Wegbegleiterin oder wütende Amazone. „Sag aus vollem Herzen Nein!“, ruft sie ins aufmerksame Publikum und kündigt damit den nächsten Song „Lady“ an. Es ist ein Aufruf zum „Nein“ zur Norm und zum „Ja“ zu sich selbst. „Nein!“, schallt es durch den Club und im Publikum herrscht Gänsehautstimmung. 
Die im Mai erscheinende 7-Track-EP „Lemonade“ erzählt vom Kampf gegen die eigene emotionale Realität, von Selbstliebe, von dem wilden inneren Kind, das keine Lust mehr auf Lockdown hat und von der finalen, kraftvollen Erkenntnis, nichts kontrollieren zu können, außer der eigenen Einstellung. Ganz nach dem Motto: „When life gives you lemons, make lemonade.“