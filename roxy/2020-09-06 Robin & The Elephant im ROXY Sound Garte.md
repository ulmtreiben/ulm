---
id: "652981668953280"
title: Robin & The Elephant im ROXY Sound Garten
start: 2020-09-06 19:00
end: 2020-09-06 22:00
address: ROXY.ulm
link: https://www.facebook.com/events/652981668953280/
teaser: „Robin & the Elephant“, eine Combo aus der Ulmer Region, spielen einen
  unverwechselbaren Sound, irgendwo zwischen Indie Pop und Singer Songwriter,
  zwi
isCrawled: true
---
„Robin & the Elephant“, eine Combo aus der Ulmer Region, spielen einen unverwechselbaren Sound, irgendwo zwischen Indie Pop und Singer Songwriter, zwischen Melancholie und Lebensfreude. Mehrstimmiger Gesang, der die Liebe und eine neue Langsamkeit der Welt besingt, wird von Klavier, Gitarre, Bass und Schlagzeug getragen. Die Melodien der vier Musiker gehen schnell ins Ohr des Zuhörers und laden zum Verweilen ein. Der erste Longplayer 'Memories of a Memory' ist bereits auf Vinyl verewigt und wartet darauf, auf die Plattenteller der Hörer gelegt zu werden.

Der Biergarten öffnet um 17:00 H.
Eintritt frei.