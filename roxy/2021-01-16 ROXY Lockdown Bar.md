---
id: "2797026777202548"
title: ROXY Lockdown Bar
start: 2021-01-16 20:30
link: https://www.facebook.com/events/2797026777202548/
image: 138367061_10158207218097756_2317086864743533409_o.jpg
teaser: "Zum Anfang mal zwei rhetorische Fragen: Am Samstagabend schon was vor? Nein,
  wieder nicht? Dann haben wir da was für euch. Als wirksames Gegenmittel z"
isCrawled: true
---
Zum Anfang mal zwei rhetorische Fragen: Am Samstagabend schon was vor? Nein, wieder nicht? Dann haben wir da was für euch. Als wirksames Gegenmittel zum Trübsal blasen am Lockdown-Wochenende empfiehlt euer freundliches Kulturzentrum: Besucht doch unsere neue ROXY LOCKDOWN BAR! Kostenlos und ganz bequem vom heimischen Sofa aus! 


Ab 20:30 H öffnen sich zum ersten Mal die virtuellen Türen der LOCKDOWN BAR - online, live. Gastgeberin Ariane Müller (ja, die eine Hälfte von Suchtpotenzial) führt durch einen unterhaltsamen Abend mit Gästen aus der Region, Live-Musik und Drink-Tutorial.


Pop, Rock, Punk & Drama mit KANDA
Für die beste Live-Musik in der LOCKDOWN BAR sorgt die beim Deutschen Rock & Pop Preis 2020 gleich mehrfach ausgezeichnete Band KANDA. Das Duo aus Stuttgart wurde dort als beste Pop Band mit dem 1. Platz ausgezeichnet. KANDA wurden zudem Zweiter in der Kategorie ,,Bestes Pop Album" und kamen auf den 1. Platz beim ,,Kulturpreis für die Förderung der Rock- und Popmusik in Deutschland".


Talk mit Mario Schneider vom Theatro Ulm
Die Ulmer Stadtmusikantin und Musikcomedienne Ariane Müller (mit ihrem Duo Suchtpotenzial erhielt sie im letzten Jahr den Deutschen Kleinkunstpreis) muss man in der Region wirklich nicht mehr vorstellen. Für den Premierenabend in der ROXY LOCKDOWN BAR hat sie Mario Schneider vom Theatro als Talk-Gast eingeladen. Schneider hat im Frühjahr 2020 kurz vor dem ersten Lockdown viel Zustimmung für seinen Entschluss bekommen, ab sofort keinen Deutsch-Rap mehr mit frauenverachtenden, diskriminierenden oder homophoben Texten im Theatro zu spielen. Den vielfachen, mit der Zeit wechselnden Corona-Beschränkungen des letzten Jahres begegnete er kreativ durch Umwandlung seines Clubs u.a. ins Projekt „Supermarkt Mitte" oder aktuell zur Corona-Teststation.


Mach's dir selbst - das Cocktail-Tutorial
Im letzten Sommer hat ROXY-Barchef Michl im ROXY Soundgarten gemeinsam mit seinem Team wahrscheinlich mehrere tausend Drinks gemixt. Heute Abend mixt er mit euch zwei feine Cocktails:

WHISKEY SOUR
Whiskey / Bourbon
Zitronen oder Zitronensaft
Zuckersirup
Angostura Bitter
1 frisches Ei pro Cocktail (wer kein Ei isst, kann stattdessen Puderzucker nehmen)

GIN FIZZ
Gin
Zitronen oder Zitronensaft
Soda
Zuckersirup

Eis und Cocktailshaker

(Natürlich nur für Ü18-Jährige!)


Noch Fragen? Dann bleib zum Zoom-Thementalk und Aftershow Bargespräch
Im Anschluss an den Livestream ist eure Mitwirkung gefragt: die Musiker von KANDA, Michl, Mario Schneider und Ariane Müller stehen euch für ein persönliches Bargespräch via Zoom zur Verfügung.


Hier könnt ihr teilnehmen:

FACEBOOK
https://www.facebook.com/roxy.kultur/posts/10158207004367756

YOUTUBE
https://youtu.be/ebH7HBM-cA0

ZOOM
https://zoom.us/j/91735323962

ROXY-HOMEPAGE
https://www.roxy.ulm.de