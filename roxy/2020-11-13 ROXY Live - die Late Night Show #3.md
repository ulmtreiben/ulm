---
id: "2666070736979538"
title: "ROXY Live - die Late Night Show #3"
start: 2020-11-13 21:00
address: ROXY.ulm
link: https://www.facebook.com/events/2666070736979538/
image: 122252639_10158003435952756_5990174834770488953_o.jpg
teaser: Reisewarnung! Auf nichts kann man sich mehr einigen in diesen polarisierten
  Zeiten. Außer darauf, dass Reisen uns das Jahr versüßt. Reisen bildet, Rei
isCrawled: true
---
Reisewarnung!
Auf nichts kann man sich mehr einigen in diesen polarisierten Zeiten. Außer darauf, dass Reisen uns das Jahr versüßt. Reisen bildet, Reisen schafft Perspektive, beim Reisen baumelt die Seele, und ob Backpacker in den Anden oder Poolliegen-Reserviererin auf den Malediven, wir schwören auf den jährlichen Trip in die Ferne als Ausgleich und Atempause. 2020 hat vielen da jedoch einen Strich durch die Rechnung gemacht. Und ROXY Live – die Late Night Show ist hier, um euch zu trösten! Denn Andreas Rebholz und Niklas Ehrentreich finden: Besser ist das. Warum Fernreisen über- und der Bummel durch die eigene Region unterschätzt werden, was das Wildgehege Krefeld dem Großaquarium Auckland voraus hat und warum guten Gewissens eigentlich nur Daheimbleibende entspannen können, erforschen sie in einer neuen Ausgabe des Formats mit Texten, Einspielern, Musik, Interviews und vielem mehr. Live zu Besuch ist Jason Bartsch, Songwriter aus Bochum; mit ihm, dem Studiopublikum und Gesprächspartner:innen aus der Urlaubsbranche wird das Phänomen „Reise“ auf hohlen Klang hin abgeklopft. Siehe da: Es hallt ganz schön…

Eintritt frei