---
id: "573159333368805"
title: ROXY Sound Garten mit LMAA
start: 2020-08-15 17:30
end: 2020-08-15 23:55
address: ROXY.ulm
link: https://www.facebook.com/events/573159333368805/
teaser: Wir freuen uns, euch in unserem Sound Garten begrüßen zu dürfen. Neben dem
  regulären Biergartenbetrieb versüßen euch unterschiedliche DJs die Ohren.
isCrawled: true
---
Wir freuen uns, euch in unserem Sound Garten begrüßen zu dürfen. Neben dem regulären Biergartenbetrieb versüßen euch unterschiedliche DJs die Ohren.

HEUTE (ab 17:30 H): L.M.A.A. - Liedabend mit Andi und Achim
Diesen Abend werden die zwei musikalischen Füchse Andi und Achim aka Purple Haze und Deejot Roterfreibeuter begleiten.

Eine Spende für die musikalische Unterhaltung wird gern entgegen genommen.

Der Biergarten öffnet um 15:00 H
***
Alle aktuellen Bestimmungen bzgl. Corona findet ihr auf unserer Homepage www.roxy.ulm.de