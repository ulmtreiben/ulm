---
id: "1134566860234461"
title: "Kinderprogramm des SJR Ulm: Kreativecke"
start: 2020-07-26 15:00
end: 2020-07-26 19:00
address: ROXY.ulm
link: https://www.facebook.com/events/1134566860234461/
image: 107091189_10157712795997756_5359938471260006041_o.jpg
teaser: Wer Lust auf Malen oder Basteln hat, ist hier genau richtig. Egal ob klassisch
  mit Stift und Papier oder auch kompliziertere Bastelaufgaben, für jeden
isCrawled: true
---
Wer Lust auf Malen oder Basteln hat, ist hier genau richtig. Egal ob klassisch mit Stift und Papier oder auch kompliziertere Bastelaufgaben, für jeden ist was dabei.

Maximale Teilnehmerzahl: 10
15:00-19:00 H

Das Angebot ist kostenlos (und findet unter der Berücksichtigung der derzeitigen Hygiene- und Sicherheitsmaßnahmen statt). Wer mag, kann sich gerne im Voraus mit Name, Adresse und Telefonnummer unter knehr@sjr-ulm.de anmelden.

Kooperation von Stadtjugendring e.V. und ROXY gGmbH