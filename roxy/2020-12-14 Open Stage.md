---
id: "427058737996804"
title: Open Stage
start: 2021-03-15 20:00
end: 2021-03-15 23:00
address: ROXY.ulm
link: https://www.facebook.com/events/427058737996804/
image: 73388443_10156898440467756_3195330899424051200_o.jpg
teaser: Aufgrund der aktuellen Lage musste die Open Stage vom 14.12.2020 und
  18.01.2021 auf den 15.03.2021 verschoben werden. Tickets behalten ihre
  Gültigkeit
isCrawled: true
---
Aufgrund der aktuellen Lage musste die Open Stage vom 14.12.2020 und 18.01.2021 auf den 15.03.2021 verschoben werden. Tickets behalten ihre Gültigkeit oder können an den jeweiligen VVK-Stellen zurückgegeben werden.
***
Hier kann man einmal im Monat aufstrebende Talente, charmante Dilettanten und unverbrauchte Profis beim Schwitzen, Zittern, Triumphieren, aber auch beim Abstürzen erleben — ein Schauplatz kultiger Auftritte von Darstellern aus den Bereichen Kabarett, Comedy, Musik und Tanz, die sich berufen fühlen, sich der Gunst des Publikums zu stellen. Möglich ist (fast) alles! Unterhaltsam und spannend ist es immer!

Moderation: Matthias Matuschik

Mit freundlicher Unterstützung von
EDAG
Transporeon