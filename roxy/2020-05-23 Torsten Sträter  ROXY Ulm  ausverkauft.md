---
id: "2889151647976041"
title: Torsten Sträter * ROXY Ulm * ausverkauft
start: 2021-05-15 20:00
end: 2021-05-15 23:00
address: ROXY.ulm
link: https://www.facebook.com/events/2889151647976041/
image: 62349539_10156563905932756_8305590083214376960_o.jpg
teaser: "Guten Tag, Sträter hier.    Mein drittes Programm. Titel: »Schnee, der auf
  Ceran fällt.«.    Worum geht’s? Nun ja:   Da sind epische Exkursionen über"
isCrawled: true
---
Guten Tag, Sträter hier. 
 
Mein drittes Programm. Titel: »Schnee, der auf Ceran fällt.«. 
 
Worum geht’s? Nun ja:
 
Da sind epische Exkursionen über Moral und Verstand, wie immer einem strengen roten Faden folgend, eine angenehm kompakte Darreichungsform, an den Rändern verbrämt mit einigen wenigen Zwischenbemerkungen, am Ende eine zutiefst beseelende Botschaft und Punkt 22:00 fällt mir das Mikrophon aus der Hand … 
 
QUATSCH. 
 
Kommen Sie, ernsthaft jetzt. Wollen Sie das wirklich schon vorher wissen? Doch wohl nicht. Das Leben folgt ohnehin schon strengen Regeln, immer will wer was, man kommt zu nichts, man gönnt sich kaum was - also ist es ja wohl das MINDESTE, mal einen Abend locker zu lassen. Und das machen wir zwei Hübschen. Sie und ich. Sie wissen doch, wie das bei mir läuft: 
 
Ich bringe ganz ganz frische Geschichten mit, nichts, was Sie vorab schon aus dem TV kennen, und zwischendurch erzähle ich Ihnen, was sonst noch war. Eine Führung durch die ganze Welt der Idiotie, die Einsicht, dass nichts menschlicher ist als das Missgeschick, seltsame Berichte vom Rand der schiefen Ebene, dann ergänze ich den Abend noch mit Schilderungen, die ich mir auf gar keinen Fall verkneifen kann, mache den Sack zum Ende hin mit einer sehr guten Geschichte zu, und wenn Sie dann noch können, hagelts Zugaben. Ein seriöses Konzept.  Und ich gelobe, es sehr lustig zu gestalten. Und mich so gut zu amüsieren wie Sie. 
 
Klingt erstmal ein bisschen krude.
 
Wird aber verhältnismäßig überwältigend.
 
Beste Grüße, Torsten Sträter