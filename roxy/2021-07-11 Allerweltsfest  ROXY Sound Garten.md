---
id: "651827259034023"
title: Allerweltsfest * ROXY Sound Garten
start: 2021-07-11 11:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/651827259034023/
image: 212452850_10158636261292756_4100946449514566653_n.jpg
isCrawled: true
---
Am 20. März 2021 jährte sich zum fünften Mal das Inkrafttreten des EU-Türkei-Abkommens mit dem Ziel, die Überfahrten Geflüchteter über die Ägäis zu verhindern. Zur gleichen Zeit entstanden u.a. auf den griechischen Inseln Lagerkomplexe, die schnell überfüllt waren und in denen bis heute menschenunwürdige Zustände herrschen. Das tägliche Sterben von Menschen im Mittelmeer und die Zustände in den Lagern an den Außengrenzen der EU sind aber nicht nur eine humanitäre Katastrophe, sondern ganz wesentlich ein politscher und juristischer Skandal. Sie sind das Ergebnis der europäischen - und damit auch deutschen - Flüchtlingspolitik. Die Pushbacks der europäischen Agentur Frontex sprechen hier eine mehr als deutliche Sprache.

Am 26. September sind Bundestagswahlen und und wir haben die Kandidat:innen der Wahlkreise Ulm und Neu-Ulm zu einer Diskussionsrunde eingeladen. Als breites Bündnis zivilgesellschaftlicher Organisationen möchten wir ihre ganz persönliche Positionen und Haltungen in Bezug auf die europäische und deutsche Flüchtlingspolitik erfahren und diskutieren. Zusätzlich haben wir zwei Experten eingeladen: Maximilian Pichl, Jurist und Verfasser der Studie "Der Moria-Komplex" sowie Dr. Christian Bialas, Chefarzt an der Neu-Ulmer Donauklinik. Er unterstützt seit 2015 das Flüchtlingslager Moria / Kara Pepe auf Lesbos mit humanitären Mitteln.

Die Veranstaltung hat ein offenes Ende, sodass nach dem offiziellen Ende der Gesprächsrunde noch Gelegenehit zu weiterem Austausch mit den Kandidat:innen und untereinander besteht.

Wir, das Ulmer Netz für eine andere Welt e.V., das Forum Asyl und Menschenrechte und das ROXY Ulm freuen uns, euch zu dieser Matinee begrüßen zu dürfen. Sie ist ein anderes Format des jährlichen Allerweltsfests, das in diesem Jahr aufgrund der Corona-Bedingungen nicht in der gewohnten Form stattfinden kann.

Eine Kooperation mit Ulmer Netz für eine andere Welt e.V. und Forum Asyl und Menschenrechte