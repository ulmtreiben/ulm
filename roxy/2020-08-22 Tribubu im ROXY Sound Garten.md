---
id: "265830784715153"
title: Tribubu im ROXY Sound Garten
start: 2020-08-22 20:30
end: 2020-08-22 23:30
address: ROXY.ulm
link: https://www.facebook.com/events/265830784715153/
image: 107099649_10157713045887756_8125090926820482962_o.jpg
teaser: Die Musik von Tribubu ist eine vielseitige Mischung aus Rumba, Folk, Blues und
  afrikanischem Beat. Live-Shows sind dynamisch, fröhlich und optimistisc
isCrawled: true
---
Die Musik von Tribubu ist eine vielseitige Mischung aus Rumba, Folk, Blues und afrikanischem Beat. Live-Shows sind dynamisch, fröhlich und optimistisch. Mit Texten, mit denen sich viele Menschen identifizieren können, begeistert die Band ihr Publikum. Freut euch auf eine Mischung aus starken Worten und Botschaften mit unendlich vielen Melodien und Rhythmen.

Lucas’ Songwriting-Fähigkeiten und Qualitäten an der Gitarre machen ihn zu einem sehr vielseitigen Musiker.
Dani Torres ist stark von südlichen Rhythmen beeinflusst, hat aber auch westliche Musikstile wie Blues, Country und Folk studiert.
Brahima Diabate ist Balafon-Spieler und die Melodie-Produktionsmaschine, die das Hören dieser Band zu einem einzigartigen Erlebnis macht.