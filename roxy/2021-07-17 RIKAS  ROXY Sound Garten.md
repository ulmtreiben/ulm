---
id: "830077061274415"
title: RIKAS * ROXY Sound Garten
start: 2021-07-17 20:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/830077061274415/
image: 198085958_10158563169507756_7086657776179860207_n.jpg
isCrawled: true
---
LIVEMUSIK UNTER FREIEM HIMMEL
EINTRITT FREI! 💛

Weil man den Verlauf der Pandemie weder vorhersagen noch die aktuelle Krise klein sprechen kann, möchten Rikas mit der Verschiebung der Tour ein kleines Bisschen Hoffnung schenken. Energiegeladene Shows im verschwitzten Club sind gerade ferner denn je, doch alleine der Gedanke daran kann helfen durch die aktuelle Krise zu kommen und den Blick nach vorne zu richten. Die ursprünglich für 2020 angesetzten Shows bekommen einen neuen Rahmen und das Positive wird in Aussicht gestellt.

Eine weitere, lose Verschiebung ihrer „Showtime Tour 2020“ wäre der Band zu simpel und resignativ gewesen. „Wir befinden uns gerade in einem leeren Raum, welcher über das Jahr mit jedem neuen Song, jeder neuen Grafik, jedem neuen Video gefüllt werden soll. Wir möchten unsere Vision einer Welt nach dem Lockdown wiedergeben. Mit dem Ziel genau das auf die Bühnen zu bringen. Hoffentlich in diesem Herbst, hoffentlich mit der Hilfe der Fans!“

„Stereo“ heißt die neue Single von Rikas, die am 28. Mai via Columbia Records erscheint und es scheint keinen besseren Zeitpunkt für diese energiegeladene Nummer zu geben als den blühenden Frühling. Im dazugehörigen Video heißt es „Rikas For Sale“ und gesagt, getan - Rikas starteten parallel eine Auktion und versteigern sich in Zeiten von NFT, fehlenden Shows und Kulturkoma selbst.

„Stereo“ feiert für die Band einen neuen Aufbruch. Nach dem Erscheinen Ihres Debütalbums „Showtime“ und einer ausverkauften Tournee Ende 2019 durchkreuzte die Pandemie auch ihre Pläne und die Band fand sich in einer Schreibblockade wieder. Ende 2020 katapultierten sich Rikas mit Overthinking, ihrer Ode an und gegen den Selbstzweifel, jedoch zurück auf den Radar. Mit neu entdeckter Motivation und Spielfreude sind Rikas jetzt mit frischer Musik und Energie bereit für einen Sommer.