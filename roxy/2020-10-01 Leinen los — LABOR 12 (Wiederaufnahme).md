---
id: "316455902734140"
title: Leinen los — LABOR 1|2 (Wiederaufnahme)
start: 2020-10-01 20:00
end: 2020-10-01 22:00
address: ROXY.ulm
link: https://www.facebook.com/events/316455902734140/
teaser: Eine musikalischer Theaterreise Das Tolle am Reisen ist der Zustand
  dazwischen, nicht mehr hier und noch nicht dort zu sein, in einer Gemeinschaft
  auf
isCrawled: true
---
Eine musikalischer Theaterreise
Das Tolle am Reisen ist der Zustand dazwischen, nicht mehr hier und noch nicht dort zu sein, in einer Gemeinschaft auf Zeit, die gerade auf Schiffsreisen sehr besonders sein kann. Alle kommen mit unterschiedlichem Gepäck an Bord, manche mit Fernweh, manche vom Heimweh geplagt. Wenn die Leinen los sind, sitzen alle im selben Boot, in tropischen Nächten wie bei rauer See. Wir wollen mit unserem Publikum in See stechen und den Bauch eines Ozeanriesen zum Klingen bringen - natürlich mit dem, was unsere Mitspieler am Liebsten singen und mit den Klängen, die uns die Räume zur Verfügung stellen (zum Beispiel Percussion im Maschinenraum, a capella zu den Filmsequenzen und eine Show-Band auf dem Promenadendeck im ROXY). Zu den Besonderheiten, die wir jetzt schon versprechen können, gehört ein einzigartige Kombination aus Filmfragmenten aus dem alten Kohlenkeller der HfG und und Live-Erlebnis im ROXY Studio.

Beteiligte
Heyoka Ensemble
Regie: Eva Ellerkamp, Simon Reimold
Bühne / Kostüme: Hans Poll

Gefördert durch 
* den Landesverband Freie Tanz- und Theaterschaffende Baden-Württemberg e.V. aus Mitteln des Ministeriums für Wissenschaft, Forschung und Kunst des Landes Baden-Württemberg ( LAFT BW )
* die Kulturabteilung der Stadt Ulm
