---
id: "803512750438611"
title: Suchtpotenzial * ROXY Ulm
start: 2021-11-12 20:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/803512750438611/
image: 125469140_10158067207012756_4256605060734779931_n.jpg
isCrawled: true
---
Die Show wurde vom 29.01.2021 auf den 12.11.2021 verschoben. Tickets behalten ihre Gültigkeit oder können an den jeweiligen Vorverkaufsstellen zurückgegeben werden.
***

ALARM!!! Sie sind wieder da! Die Musik-Comedy-Queens von Suchtpotenzial mit ihrem 3. Programm „Sexuelle Belustigung“: 

Julia Gámez Martín aus Berlin und Ariane Müller aus Ulm sind zwei preisgekrönte Musikerinnen und bundesweit bekannt für ihre Shows voll rabenschwarzen Humors. 

Wenn diese beiden Ladies ihrer Albernheit freien Lauf lassen, kann auf der Bühne einfach alles passieren: virtuose Gesangsduelle, derbe Wortgefechte und kluges Pointengewitter. Suchtpotenzial werfen dabei alle Konventionen und Klischees über Bord und nichts ist vor ihnen sicher. Von hippen Instagram-Trends über Wagner-Opern und feministischen Anbagger-Tipps bis zum finalen Weltfrieden werden die wirklich wichtigen Themen bearbeitet. 

Suchtpotenzial sind Meisterinnen der gelebten Neurosen, von absurden Gedankengängen und bewegen sich parkettsicher in allen Musik-Genres. Ihre Musik und Comedy-Texte schreiben Ariane und Julia selbst und machen auch sonst alle Stunts selbst. 

Lasst auch ihr euch sexuell belustigen von Suchtpotenzial, dem besten Alkopop-Duo der Welt!