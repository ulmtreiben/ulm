---
id: "586014428828690"
title: Starbugs Comedy * ROXY Ulm
start: 2021-11-10 20:00
end: 2021-11-10 23:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/586014428828690/
image: 79686368_10157015408547756_6440030369151451136_n.jpg
isCrawled: true
---
Die weltweit erfolgreichste Schweizer Comedy-Show kommt nach Deutschland!

Starbugs Comedy sind die Überflieger der Schweizer Show-Szene und die weltweit erfolgreichste Schweizer Comedy-Show. Von New York bis Tokio haben sich die drei Komiker in die Herzen des Publikums gespielt. Ihr neues Programm "Crash Boom Bang" ist erfrischend, verrückt und ungeheuer lustig! Unter der Regie von Nadja Sieger "Ursus&Nadeschkin" ist ein fulminantes Spektakel entstanden, das fast ohne Worte auskommt. Comedy auf einem neuen Level! Die Lachmuskeln sind im Dauereinsatz. Eine Feelgood-Show, wie es sie bis jetzt noch nicht gegeben hat.

Nun sind Fabian Berger, Martin Burtscher und Wassilis Reigel mit ihrer Comedy-Show "Crash Boom Bang" in Deutschland auf Tour. Der Name ist Programm. Starbugs Comedy lassen es so richtig krachen. Sie spielen mit allem, was ihnen in die Hände kommt. Und das sind nicht nur Klischees. Wie lebendige Cartoons springen, tanzen und reiten die Comedians durch ihre Sketche und machen dabei vor nichts Halt, außer vor der Pause. Die drei Schweizer tanzen auf vielen Hochzeiten, gelegentlich auch mit falschen Damen. Eben noch auf dem Tanzparkett, findet man sich im Wilden Westen und in der nächsten Sekunde auf einer großen Konzertbühne wieder. Wer für diese Reise einen roten Faden möchte, muss diesen selber mitbringen. Brauchen tut man ihn allerdings nicht.

***
Die Show wurde vom 16.10.2020 auf den 10.11.2021 verschoben. Tickets behalten ihre Gültigkeit oder können an den jeweiligen VVK-Stellen zurückgegeben werden.