---
id: "654262791966976"
title: "Summer in the city: Tabula Rasa Orchestra im ROXY Sound Garten"
start: 2020-08-09 19:00
end: 2020-08-09 22:00
address: ROXY.ulm
link: https://www.facebook.com/events/654262791966976/
image: 107096122_10157712994907756_3340307194925249752_o.jpg
teaser: Der Sommer liegt in den letzten Zügen, doch wir wollen nochmal aufdrehen.
  Unbändige Energie, unglaubliche Vielfalt und ein gehöriges Maß an Kreativitä
isCrawled: true
---
Der Sommer liegt in den letzten Zügen, doch wir wollen nochmal aufdrehen. Unbändige Energie, unglaubliche Vielfalt und ein gehöriges Maß an Kreativität kennzeichnen die Musik und die Show der Band. Kein Wunder, dass dann in der Zeitung steht: „Das Tabula Rasa Orchestra brachte die Zuschauer zum Kochen“ (Augsburger Allgemeine) oder „Tolle Stimmung – mit Offbeat und guter Laune“ (Gmünder Tagespost). „Dieser Mix.... muss unbegrenzt eingenommen werden“, so schrieb die Remszeitung. Zweimal waren die Stuttgarter schon zu Gast bei KunstWerk, und die wissen: Da wird so mancher Schweißtropfen fliegen!

Eintritt frei.
Der Biergarten ist ab 15:00 H geöffnet.

***
Konzerte in der Reihe "Summer in the city" sind eine Kooperation von Kunstwerk e.V. und ROXY gGmbH.
Wir danken für die Unterstützung durch das Ministerium für Wissenschaft, Forschung und Kunst Baden-Württemberg.