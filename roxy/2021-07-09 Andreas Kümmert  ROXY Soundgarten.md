---
id: "847103019484161"
title: Andreas Kümmert * ROXY Soundgarten
start: 2021-07-09 20:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/847103019484161/
image: 191665349_10158535083897756_8618342440617775163_n.jpg
isCrawled: true
---
LIVEMUSIK UNTER FREIEM HIMMEL
EINTRITT FREI! 💛

Seit einigen Jahren hat Andreas Kümmert seine Geschicke komplett selbst in die Hand genommen und kann sich auf seinem eigenen Label Vomit Records künstlerisch voll und ganz verwirklichen. Auf ihm wird auch „Sweet Oblivion“ als digitaler Single-Release erscheinen.
Mit ihm hat sich Andreas in ganz besonderer Weise verwirklicht: Der Track ist erstmals in Zusammenarbeit mit seinen beiden Live-Musikern und befreundeten Kollegen Stefan Kahne und Michael Germer entstanden. Die drei haben aus der Corona-Not eine Tugend gemacht und die bühnenfreie Zeit für Studioarbeiten genutzt. 
Da die drei bereits seit längerem live und beim Produktionsprozess zusammenarbeiten, versuchte man sich kurzerhand auch erstmals am gemeinsamen Songwriting – eine Konstellation, die sich als perfektes Match bestätigt. 
„Sweet Oblivion“ gibt musikalisch einen guten Vorgeschmack auf das derzeit im Entstehungsprozess befindliche Album – authentisch, echt und voller Persönlichkeit. Die ist von dem starken Credo eines starken Mannes und Künstlers geprägt: „Man ist niemandem außer sich selbst Rechenschaft schuldig.“
