---
id: "427028025416420"
title: "Gästeliste #11 * ROXY Ulm"
start: 2021-11-11 20:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/427028025416420/
image: 149344411_10158291023852756_198676540467698378_n.jpg
isCrawled: true
---
Das Konzert wurde vom 16.01.2021 auf den 11.11.2021 verschoben (ACHTUNG, neue Uhrzeit 20:00 H) Tickets bahlaten ihre Gültigkeit oder können an den jeweiligen Vorverkaufsstellen zurückgegeben werden.
***
Patrick Wieland lädt ein! Auf seiner Gästeliste sind diesmal gleich zwei Gäste, nämlich Luke Noa und Lemony Rug. Beides Preisträger des jungen Ulmer Kunstpreises. Sie sind Singer Songwriter, die der großen internationalen Konkurrenz durchaus Stand halten können.

Luke Noa : Luke Noa schreitet voran, ohne sich im gegenwärtigen Geschwindigkeitswahn zu verlieren. Als Kind der Neuzeit zeichnet er einen Weg fernab von musikalisch vorgemalten und allzu definierten Bildern. Vielmehr macht er sich den Stilpluralismus der Moderne zunutze, um Grenzen aufzuweichen, und kreiert im Songwriting eine Collage aus Pop, Soul, Hip Hop und Folk.

Lemony Rug : Mit seinem Soloprojekt Lemony Rug setzte der Songwriter Léon Rudolf alles auf Null und Neustart, denn nach Jahren der Suche und des Ausprobierens hat er endlich einen für ihn authentischen Weg gefunden, sich musikalisch auszuleben. Auf seiner kommenden EP verschmelzen zwischen erfrischenden Arrangements und vertrauten Gitarrenklängen kantige Indie-Rock- mit verträumten Dream-Pop- Elementen zu einem Sound, der sich am besten in einer Stimmung zusammenfassen lässt: Lemony Rug.