---
id: "793038914693609"
title: Balagan! am ROXY * Ulmer Kultursommer
start: 2021-07-23 18:30
locationName: ROXY.ulm
link: https://www.facebook.com/events/793038914693609/
image: 207102381_10158616028987756_7513039983289481717_n.jpg
isCrawled: true
---
Nach einer gefühlten Ewigkeit von pandemischen Einschränkungen setzt Helena Waldmann beim Neustart der Live-Kultur auf ein wahrhaft ungezähmtes Konzept: Balagan. Wörtlich übersetzt am ehesten mit „Unordnung“ wiederzugeben, steht Balagan für das ursprüngliche Volkstheater: ein stürmisches, wildes Jahrmarkt-Spektakel, auf der Wanderschaft von Stadt zu Stadt wie die historische Commedia dell’arte.

Für Helena Waldmanns Pop-up-Fest für Wagenlenker und alle, die dabei sind, sind u.a. kurze Auftritte auf motorisierten Lastenrädern geplant, die viel Sympathie ausstrahlen, Lebensfreude signalisieren und nach denen man sich schmunzelnd umdreht - Marke Piaggio Ape. Sie sind Vehikel, Bühne und Thron zugleich.

Begleitend zur 30-minütigen Performance im öffentlichen Raum läuft die fantastische Musik der Hamburger Band MEUTE im DJ-Set von Daniel Stenger – das Ganze wird ein Jahrmarkt, ein Fest der Haltung, lebendig, bunt, emotional.

Eintritt frei!

Gefördert von der Beauftragten der Bundesregierung für Kultur und Medien.