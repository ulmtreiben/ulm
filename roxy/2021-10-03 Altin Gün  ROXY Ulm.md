---
id: "515250889695315"
title: Altin Gün * ROXY Ulm
start: 2021-10-03 20:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/515250889695315/
image: 241504351_10158764171247756_7106823435779758698_n.jpg
isCrawled: true
---
Die Amsterdamer Band Altin Gün hat sich mit ihrem Grammy-nominierten zweiten Album Gece einen guten Ruf für die Verschmelzung von Vergangenheit und Gegenwart zu brillant eingängiger, beschwingter Popmusik erworben. Yol, ihr drittes Album in ebenso vielen Jahren, setzt diesen Trend fort, während es eine Reihe von klanglichen Überraschungen bietet. 

Es dürfte keine Überraschung sein, dass die Band erneut aus den reichen und unglaublich vielfältigen Traditionen der anatolischen und türkischen Volksmusik schöpft. Doch so vertraut die Geschichte auch sein mag, Yol ist nicht einfach nur ein Album, das traditionelle Klänge für ein zeitgenössisches Publikum umgestaltet. Das Album, das oft einen stark international geprägten, poppigen Sound aufweist, signalisiert auch eine ganz andere Herangehensweise bei der Produktion und den Aufnahmen der Band. Sängerin Merve Dasdemir greift die Geschichte auf: "Wir saßen im Grunde drei Monate lang zu Hause fest und haben Demos gemacht, zu denen jeder seinen Teil beigetragen hat. Das transnationale Gefühl kommt vielleicht von diesem Prozess des Austauschs von Demos über das Internet, einige der Musik haben wir im Studio gemacht, aber der Lockdown bedeutete, dass wir einen anderen Ansatz verfolgen mussten." 

Yol zeigt eine spürbare Verträumtheit, die vielleicht aus dieser erzwungenen Zeit des Nachdenkens stammt. Und auch ausgewählte Elemente des "Euro"-Synthiepop der späten 1970er oder frühen 1980er Jahre scheinen durch. Diese neue musikalische Landschaft wurde durch die Wahl bestimmter Instrumente gefördert, insbesondere durch das Omnichord, das in 'Arda Boylari', 'Kara Toprak' und 'Sevda Olmasaydi' zu hören ist, und die Drum-Machine, ein Instrument, das in der wunderschönen Schlussnummer 'Esmerim Güzelim' eine zentrale Rolle spielt. Dasdemir noch einmal: "Bassist Jasper Verhulst liebte den Song. Er sagte: 'Das klingt nicht nach Altın Gün, das klingt wie eine türkische Kindergarten-Musiklehrerin aus den 1980er Jahren, die eine 808 benutzt!' 

Altın Gün sind: 
Merve Dasdemir - Gesang, Keyboards 
Erdinç Ecevit - Gesang, Saz, Keyboards 
Jasper Verhulst - Bass 
Ben Rider - Gitarre 
Daniel Smienk - Schlagzeug 
Gino Groeneveld - Schlagzeug 