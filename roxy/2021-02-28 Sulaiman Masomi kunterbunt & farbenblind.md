---
id: "4045114188986555"
title: wird verschoben - Sulaiman Masomi "kunterbunt & farbenblind" (Ulm)
start: 2021-02-28 19:00
end: 2021-02-28 22:00
address: ROXY.ulm
link: https://www.facebook.com/events/4045114188986555/
image: 97422112_1843475809128561_5165232166723911680_o.jpg
teaser: "*** Die Veranstaltung wird aufgrund der Corona Krise auf den 17.02.2022
  verschoben. Karten behalten ihre Gültigkeit ***   Wissen Sie, warum die
  Mensch"
isCrawled: true
---
*** Die Veranstaltung wird aufgrund der Corona Krise auf den 17.02.2022 verschoben. Karten behalten ihre Gültigkeit ***


Wissen Sie, warum die Menschen auf den alten Schwarz-Weiß-Fotos immer so ernst guckten? Nein? Nicht schlimm, denn Sulaiman Masomis neues Programm „kunterbunt & farbenblind“ beantwortet diese und andere noch nie gestellte Fragen.

Wenn Sie schon immer wissen wollten, wie man als Ausländer eine Wohnung bekommt, ob man als Künstler mehr Sex hat und warum Farbenblindheit eigentlich eine gute Sache sein kann, dann sind Sie bei ihm gut aufgehoben.

Mit einer kunterbunten Mischung aus Slam Poetry, Musik, Comedy und Kabarett wandelt das preisgekrönte Multitalent Masomi zwischen allen Genres und lässt sich mit seiner stets humoristisch-feinsinnigen Art nicht in irgendeine Schublade packen.

Dabei widmet sich der studierte Literaturwissenschaftler allen Aspekten der menschlichen Psyche und des alltäglichen Zusammenlebens in einer multikulturellen Welt.

Masomi malt Bilder und sein drittes Programm „kunterbunt & farbenblind“ ist sein neuestes Werk, in dem jedes Wort zum Pinselstrich eines facettenreichen Gemäldes wird.

Ein Bild, das Sie staunen und schenkelklopfend lachen lässt, um sich direkt danach daran zu verschlucken.

Foto: Marvin Ruppert 


