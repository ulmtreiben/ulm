---
id: "155453676717784"
title: ChoreoLab - Made in Ulm - HIGHER! * ROXY Ulm
start: 2021-08-21 19:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/155453676717784/
image: 230187873_10158711248197756_6881312004889123445_n.jpg
isCrawled: true
---
Für den Tanzabend HIGHER! gehen vier internationale Choreograf:innen höher und weiter bis an Ihre Grenzen!

Der Tanzabend „ChoreoLab - Made in Ulm“, dessen Themen von der Ulmer Kulturgeschichte inspiriert sind, findet nun im dritten Jahr in Folge im ROXY statt. In diesem Jahr sind wir inspiriert von dem menschlichen Streben nach Höherem – und nennen den Tanzabend daher „Higher!“.   

Das Ulmer Münster hat bekanntlich mit 161,53 Metern den höchsten Kirchturm der Welt. Eines Tages in nicht allzu ferner Zukunft wird es diesen Weltrekord, den es seit 1890 hält, verlieren. Mit der Fertigstellung der Kirche Sagrada Familia in Barcelona wird es dann einen neuen höchsten Kirchturm mit 170 Metern Höhe geben.  

Das diesjährige ChoreoLab hat jeweils zwei Choreografen aus Ulm und zwei aus Barcelona beauftragt, mit ihren Arbeiten zu untersuchen, zu reflektieren und zu hinterfragen, was die Menschen eigentlich antreibt, immer „höher“ zu bauen? Geht es um Macht, Prestige, Status, Identität oder ist es der menschliche Wunsch, sich zu vereinen und an etwas Größeres als uns selbst zu glauben? 

Der Tanzabend wird aus vier jeweils höchstens 15-minütigen Choreografien bestehen, die von vier Tänzer:innen präsentiert werden. Zwei Tänzer:innen sind in Deutschland ansässig, die beiden anderen in Spanien. 

Wie gewohnt, gibt es während der „ChoreoLab“-Probenphase wieder jeden Freitag von 17 – 18 Uhr kostenlose öffentliche Probeneinblicke in die Arbeit der verschiedenen Choreograf:innnen – mit der Möglichkeit, Fragen zu stellen und mit den Mitwirkenden zu diskutieren. Termine hierfür sind der 23. und 30. Juli, sowie der 6. und 13. August 2021.  

Für den Probenbesuch ist eine Anmeldung bis spätestens 24 Stunden vorher unter anmeldung@roxy.ulm.de erforderlich. 

Anders als in den letzten Jahren finden die Aufführungen in der großen ROXY Werkhalle statt, damit alle Abstandsregelungen eingehalten werden können. 

Mit dem diesjährigen „ChoreoLab – Made in Ulm“ wird zugleich ein kultureller Austausch zwischen den Städten Ulm und Barcelona begonnen. Als Teil dieses Austausches wurde der „ChoreoLab“-Abend „Higher!“ für ein Gastspiel im Rahmen des FESTIVAL DANSAT am 18. September 2021 nach Barcelona eingeladen  

Choreograf:innen: Paloma Muñoz, Pablo Sansalvador, Thomas Noone und Smadar Goshen  

Tänzer:innen: Tanit Cobas, Anna Raiola, Carlos Roncero, Adrian Ros  