---
id: "577320096770596"
title: Kulturnacht 2021 * Lesung, Hausführung, Ausstellung * ROXY Ulm
start: 2021-09-18 17:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/577320096770596/
image: 223841667_10158681733572756_7560007393939902205_n.jpg
isCrawled: true
---
Wir freuen uns, euch während der Kulturnacht 2021 im ROXY begrüßen zu dürfen. 

LESUNG
17:00 und 19:00 H: "Unter Palmen aus Stahl".
(Einlass jeweils 15 Minuten vorab)
Das ist die Geschichte eines Straßenjungen. Dominik Bloh – gebürtiger Neu-Ulmer - lebte über zehn Jahre auf der Straße. Schon als Teenager geriet er dahin, wo man Deutschland von „ganz unten“ betrachtet. Nun mit Anfang dreißig (Jahrgang 1988) hat er seine Geschichte aufgeschrieben, auf kleinen Zetteln, noch als Obdachloser lebend. Er erzählt in seinem Buch  wie es auf der Straße zugeht, wie man dort lebt und wie man es schafft, sich mit Mut und Courage herauszuarbeiten. Dominik lebt heute in einer kleinen Wohnung in Hamburg.
Das Buch ging in die Spiegel Bestsellerliste, etliche Talkshoweinladungen folgen, Lesungen ohne Ende, Ruhm und Reichtum. Naja, obwohl… Und was macht Dominik?  Er gründet mit einigen Mitstreitern zusammen den Verein Hanseatic Help und engagiert sich in der Hilfe für die, denen es so geht wie ihm bis vor kurzem.
Eine Runde Lesung dauert 60 Minuten.

HAUSFÜHRUNG
21:00 H: Wer schon immer mal hinter die Kulissen im ROXY blicken wollte, hat heute die Gelegenheit. Einmal in alle Ecken blicken und erfahren, wie das Kulturzentrum aufgestellt ist. Fragen sind herzlichst erwünscht.

AUSSTELLUNG
TanzLabor Photo Exhibition
In der Galerie können spektakuläre Fotografien von unseren TanzLabor-Projekten bewundert werden, die wir das vergangene Jahr über durchgeführt haben.
Zu sehen sind Aufnahmen von 
Ray Demski www.raydemski.com / Jasmine Ellis Projects 
Martina Dach www.mitbedacht.com
Guido Stuch www.guidostuchphoto.com
Marina Weishaupt www.marinaweishaupt.com

GASTRONOMISCHES ANGEBOT
Unser Biergarten feiert seinen Saisonabschluss und ist ab 14:00 H für alle Besucher:innen geöffnet.
