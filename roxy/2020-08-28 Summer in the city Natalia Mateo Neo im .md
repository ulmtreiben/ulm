---
id: "3138010352935398"
title: "Summer in the city: Natalia Mateo Neo im ROXY Sound Garten"
start: 2020-08-28 20:00
end: 2020-08-28 23:00
address: ROXY.ulm
link: https://www.facebook.com/events/3138010352935398/
image: 107333695_10157713063957756_7227789632326154140_o.jpg
teaser: Verstörend und intim. Schön, wütend, rau. Weich, verletzlich und verletzend.
  Reif. Zerbrechlich. Betörend. Natalia Mateo hat Wurzeln und ist wie der J
isCrawled: true
---
Verstörend und intim. Schön, wütend, rau. Weich, verletzlich und verletzend. Reif. Zerbrechlich. Betörend.
Natalia Mateo hat Wurzeln und ist wie der Jazz doch gleichzeitig entwurzelt. Die polnische Sängerin singt und säuselt, definiert aber gleichzeitig Dissonanzen, sie liebt den Kitsch genauso wie sein Gegenteil. 
Von 2014 – 2017 war sie jedes Jahr bei KunstWerk Ulm zu Gast und hat sich eine treue Fangemeinde erspielt. Dann löste sich leider ihre Band auf. Wir freuen uns auf ein Wiedersehen, denn jetzt kommt Natalia Mateo wieder nach Ulm , diesmal mit ihrer neuen Band NEO.
Ein spannendes Unterfangen, fusionieren hier doch tiefe Songwriter-Inhalte mit den Klängen des elektronischen Schlagzeugs sowie Synthesizern und Elektronik. Das Ergebnis ist eine treibend getriebene, spacige Soundlandschaft, manchmal voller Tristesse, aber auch Humor, manchmal voller Partylaune.
Aus dem Jazz kommend und ohne Angst vor einem tanzbaren Endprodukt, zieht die Band entschieden an einem Strang.
"How to unfuck truth"!
Natalia Mateo: Vocals
Christoph Bernewitz: Guitar
Markus Daßau: Drums & Sound Design

Eintritt frei.
Der Biergarten ist ab 15:00 H geöffnet.

***
Konzerte in der Reihe "Summer in the city" sind eine Kooperation von Kunstwerk e.V. und ROXY gGmbH.

Wir danken für die Unterstützung durch das Ministerium für Wissenschaft, Forschung und Kunst Baden-Württemberg.