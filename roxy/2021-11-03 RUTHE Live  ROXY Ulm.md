---
id: "1370287833133207"
title: RUTHE Live * ROXY Ulm
start: 2021-11-03 20:00
end: 2021-11-03 23:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/1370287833133207/
image: 74784430_10156895824272756_6460327156231700480_n.jpg
isCrawled: true
---
Die Veranstaltung wurde vom 21.11.2020 und 28.04.2021 auf den 03.11.2021 verschoben. Tickets behalten ihre Gültigkeit oder können an den jeweiligen Stellen zurückgegeben werden.
***
Shit Happens!

Ein Cartoonist auf der Bühne? Was macht der da? Na, was jeder gute Komiker auch tut: Die Zuschauer zum Lachen bringen! Ununterbrochen, zwei volle Stunden lang. Und das ist das einzige hier, was bei Ralph kein Witz ist!

In seiner Show „RUTHE LIVE“ verbindet Ralph Ruthe Cartoons, Animationsfilme, Lesung und Musik (ja, Ralph singt leider auch - da müssen Sie durch) zu etwas völlig Neuem: eine Live-Comic-Comedy- Show, bzw. eine Live-Comedy-Comic-Show - ganz wie Sie wollen. So oder so gibt es nicht nur das Beste seiner Cartoons in ganz neuen Zusammenhängen zu sehen - seine Fans dürfen sich auch auf Welt- Premieren neuester Folgen von Ruthes Erfolgsserien „Flossen“, „HNO-WG“, „Biber und Baum“ und den „Werbeparodien“ freuen. Denn Ralph Ruthe ist nicht nur Deutschlands erfolgreichster Cartoonist, sondern auch der Produktivste: Keiner veröffentlich konstant in einer derart hohen Schlagzahl und auf immer gleich genialem Niveau neues Material. Egal, ob kleine Alltagsbeobachtungen oder große politische Zusammenhänge - Ralph Ruthe schafft es immer, den Irrsinn pointiert, intelligent und schreiend komisch in einem Cartoon einzufangen. Kein Wunder, dass er so erfolgreich ist: Buchfans verschlingen seine Bestseller, Millionen von Pendlern versüßt er auf den U-Bahn-Screens den Weg zur Arbeit und wer sich regelmäßig auf YouTube (hier hat er 730.000 Abonnenten), Facebook (1,2 Mio. Likes) und Instagram (520.000 Follower) tummelt, kommt ohnehin nicht an ihm vorbei. Und wer ihn jetzt auch live erleben möchte, der hat bald Gelegenheit dazu, denn Ralph geht im Herbst 2019 wieder auf Bühnentour! Garantiert wird es ein großartiger Abend, bei dem garantiert durchgehend gelacht wird! Und wer anschließend noch etwas Geduld hat, bekommt noch etwas ganz Besonderes: Denn in seinen schon legendären Signierstunden hat es bisher noch keinen Zuschauer gegeben, der nicht von Ralph Ruhte selbst eine persönliche und signierte Zeichnung bekommen hat. Viel Spaß!
