---
id: "474048600633979"
title: ImproVision - Kulturaugenblicke im Stream
start: 2021-04-09 20:30
link: https://www.facebook.com/events/474048600633979/
image: 159450682_10158356244672756_1333765702873225053_o.jpg
teaser: EIN NEUES FORMAT DER KULTURHALLEN  Wibke-J. Richter (Showbuddies Ulm) lädt
  Solokabarettisten, sowie Koryphäen der Impro-Comedy Szene ein. Zusammen ges
isCrawled: true
---
EIN NEUES FORMAT DER KULTURHALLEN

Wibke-J. Richter (Showbuddies Ulm) lädt Solokabarettisten, sowie Koryphäen der Impro-Comedy Szene ein. Zusammen gestalten sie einen Abend der Abwechslung und vereinen Talk, Musik, Kaberett und Impro-Comedy auf der ROXY Bühne. Die Veranstaltung wird über unsere Homepage sowie Facebook und YouTube gestreamt. Die Links zu den Streams folgen kurz vor der Veranstaltung. Am ersten ImproVision-Abend freut sich Wibke-J. Richter auf ihre Gäste Markus Kapp, Adrian Klein und Robert Lansing.

Wibke-J. Richter
Dem ROXY-Publikum ist Wibke-J. Richter seit vielen Jahren bekannt, denn normalerweise spielt sie regelmäßig mit den „Showbuddies" (Ulms erstem Improtheater-Ensemble) in den Kulturhallen.  Wibke-J. Richter inszeniert mit Schüler:innen Theaterstücke und hilft jungen Erwachsenen, ihren eigenen Weg der Kreativität auf der Bühne zu finden. 

Markus Kapp
Markus Kapp ist nicht nur Diplom Theologe, sondern auch Musiker, Komponist, Arrangeur, Texter und Schauspieler. In den letzten Jahren hat er sich auf Musikkabarett konzentriert und mit verschiedenen Schauspiel- und Kabarett-Kollegen überwiegend „Duo-Programme“ entwickelt. Mit seinem Soloprogramm „Wir schweifen App“ unter der Regie von Erik Rastetter hat er in den letzten beiden Jahren zahlreiche Kabarettpreise gewonnen.

Adrian Klein
Adrian ist in München geboren und aufgewachsen. Nach der Schule hat er als Tänzer und Schlagzeuger im Münchner Hofbräuhaus gearbeitet. Danach ging es für ihn nach Nürnberg. Dort studierte er Jazz Schlagzeug und entdeckte für sich das Improvisationstheater. Nach seinem Studium zog es ihn wieder nach München und er wurde Teil des Bühnenpolka Ensembles. Er arbeitet als freischaffender Schauspieler und Musiker in verschiedensten Projekten und gibt Deutschlandweit Workshops für Erwachsene und Jugendliche. Während des ersten Lockdowns im Jahr 2020 erfand er Storyteller Quests, ein Online Escape Game mit Live Schauspieler. Dort verbindet er Rätsel, Comedy und Popkultur. 

Robert Lansing
Robert Lansing hat 1998 sein Schauspielstudium in München abgeschlossen. Seitdem ist er als freier Schauspieler, Sprecher und Moderator tätig. Seit 1999 ist er festes Mitglied im Ensemble des "Fastfood Improvisationstheaters". In diesem Rahmen spielt er zahlreiche Auftritte im In- und Ausland und ist als Workshopleiter tätig. Daneben spielt er auch in klassischen Theaterproduktionen. Nach einem mehrjährigen Aufenthalt in Wien ist er seit 2016 wieder in München beheimatet. 