---
id: "2793757147386551"
title: "Hashtag: woman - Wiederaufnahme * ROXY Ulm"
start: 2020-05-22 19:00
end: 2020-05-22 22:00
address: ROXY.ulm
link: https://www.facebook.com/events/2793757147386551/
image: 89706587_10157302288552756_970324869847711744_o.jpg
teaser: "#woman  Das Internet setzt sich durch und Merkel ist immer noch #mutti aber
  Trump ist #president obwohl #metoo Karrieren ruiniert wollen Mädchen #heid"
isCrawled: true
---
#woman

Das Internet setzt sich durch und Merkel ist immer noch #mutti aber Trump ist #president obwohl #metoo Karrieren ruiniert wollen Mädchen #heidisgirl sein und Miss America muss nicht mehr im Bikini raus trotz #burkaverbot aber was haben wir überhaupt #gemeinsam?
#women ist ein multidisziplinäres und internationales Performance Kollektiv aus Ulm. Zeitgenössischer Tanz, Videoinstallation, Dokumentarisches Theater und kabarettistische Ansätze fügen sich hashtagartig zusammen.
Bei einem Workshop mit Frauen aus Deutschland, Chile, Argentinien, Afghanistan, Saudi- Arabien, Indien, Finnland, Belgien und Brasilien stand die Frage „Warum fühlst du dich als Frau?“ im Vordergrund und wurde Grundlage der Performance.

Mitwirkende: 
Video und Dramaturgie: Lisa Miller
Choreografie: Daniela Molina Garfias
Performer: Kathi Wolf, Daniela Ventuiz, Corinna Kuttner, Daniela Molina Garfias
Musik: Beatriz Vaca Campayo

Kooperationspartner: 
Kulturabteilung Stadt Ulm
Flüchtlingsrat Ulm
Haus der Begegnung
Strado Compagnia Danza
Künstlerhaus Ulm