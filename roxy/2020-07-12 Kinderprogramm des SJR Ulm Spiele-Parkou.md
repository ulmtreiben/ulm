---
id: "751923095612103"
title: "Kinderprogramm des SJR Ulm: Spiele-Parkour"
start: 2020-07-12 15:00
end: 2020-07-12 17:00
address: ROXY.ulm
link: https://www.facebook.com/events/751923095612103/
image: 107043444_10157712789322756_3835628192631576562_o.jpg
teaser: Von Bobby-Cars über Hula Hoops bis hin zu Legos, beim Spiele Parkour gibt es
  alles, was das Kinderherz begehrt. Zwei Stunden lang können sich die Klei
isCrawled: true
---
Von Bobby-Cars über Hula Hoops bis hin zu Legos, beim Spiele Parkour gibt es alles, was das Kinderherz begehrt. Zwei Stunden lang können sich die Kleinen austoben und ausprobieren.

Maximale Teilnehmerzahl: 10
15:00-17:00 H

Das Angebot ist kostenlos (und findet unter der Berücksichtigung der derzeitigen Hygiene- und Sicherheitsmaßnahmen statt). Wer mag, kann sich gerne im Voraus mit Name, Adresse und Telefonnummer unter knehr@sjr-ulm.de anmelden.

Eine Kooperation von Stadtjugendring Ulm e.V. und ROXY gGmbH