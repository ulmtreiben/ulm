---
id: "357616582702059"
title: Maike Miendientje * ROXY Sound Garten
start: 2021-09-05 18:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/357616582702059/
image: 241322223_10158764569252756_8364081617389137252_n.jpg
isCrawled: true
---
Wir freuen uns, euch in unserem Sound Garten begrüßen zu dürfen. Neben dem regulären Biergartenbetrieb versüßen euch unterschiedliche DJs die Ohren.

Heute: Maike Miendientje

Eintritt frei, Spende für die musikalische Unterhaltung wird gern entgegen genommen.
Der Biergarten ist ab 15:00 H geöffnet.
***
Alle aktuellen Bestimmungen bzgl. Corona findet ihr auf unserer Homepage www.roxy.ulm.de