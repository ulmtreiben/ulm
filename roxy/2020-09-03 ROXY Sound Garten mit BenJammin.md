---
id: "752554585326561"
title: ROXY Sound Garten mit BenJammin
start: 2020-09-03 19:00
link: https://www.facebook.com/events/752554585326561/
teaser: Wir freuen uns, euch in unserem Sound Garten begrüßen zu dürfen. Neben dem
  regulären Biergartenbetrieb versüßen euch unterschiedliche DJs die Ohren.
isCrawled: true
---
Wir freuen uns, euch in unserem Sound Garten begrüßen zu dürfen. Neben dem regulären Biergartenbetrieb versüßen euch unterschiedliche DJs die Ohren.

HEUTE: BenJammin
Wenn Benny an den Plattentellern jammt, entsteht ein musikalischer Hörgenuß mit herrlich anmutenden Gesangsharmonien, fetzigen Gitarrenklängen, präzisen Bläsersätzen und in die Adern fließenden Rythmen.
Sein Repertoire reicht von Latino-Sounds eines Compay Segundo und Funk der Dave Matthews Band oder Tower of Power über Klassiker der Beatles und Jefferson Airplane, rockigeren Tönen der Beatsteaks und Franz Ferdinand bis hin zu Balkan Beatz sowie Reggae Sounds von 311 und Bob Marley.

Für den ROXY Sound Garten hat er ein spezielles Set vorbereitet.
Genießt zusammen mit Freunden im gemütlichen Schatten der Platanenbäume eure gekühlten Getränke und Gaumenfreuden im ROXY Sound Garten.

"Nod your head to a beat. Blast some music and enjoy life."

Eintritt frei, Spende für die musikalische Unterhaltung wird gern entgegen genommen.

Der Biergarten ist bereits ab 17:00 H geöffnet.

***
Alle aktuellen Bestimmungen bzgl. Corona findet ihr auf unserer Homepage www.roxy.ulm.de