---
id: "303736884336344"
title: ROXY Sound Garten mit Ray Riddler
start: 2020-07-05 18:00
end: 2020-07-06 01:00
address: ROXY.ulm
link: https://www.facebook.com/events/303736884336344/
image: 106494006_10157703119822756_5419539916399825988_o.jpg
teaser: Wir freuen uns, euch in unserem Sound Garten begrüßen zu dürfen. Neben dem
  regulären Biergartenbetrieb versüßen euch unterschiedliche DJs die Ohren.
isCrawled: true
---
Wir freuen uns, euch in unserem Sound Garten begrüßen zu dürfen. Neben dem regulären Biergartenbetrieb versüßen euch unterschiedliche DJs die Ohren.

HEUTE (ab 18:00 H): Ray Riddler
Feeel gooood im Sound Garten!
Funk, Disco, Electronica, Soul, 80s Pop & Rock, Synthiepop, Indie Classics. Abseits von Einheitsbrei gibt es Gutes, Grooviges, Seltenes, Kurioses und auch die Hits aus vergangenen Tagen.
Alles kann, nichts muss...

Eintritt frei, Spende für die musikalische Unterhaltung wird gern entgegen genommen.

Unser Biergarten ist ab 15:00 H geöffnet.

***
Alle aktuellen Bestimmungen bzgl. Corona findet ihr auf unserer Homepage www.roxy.ulm.de