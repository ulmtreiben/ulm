---
id: "228692299092490"
title: Robotermärchen (Literatur und Musik)
start: 2021-07-14 20:00
address: ROXY Ulm I Kultur in den Hallen
link: https://www.facebook.com/events/228692299092490/
image: 195530226_329767645335407_6920182647282192895_n.jpg
isCrawled: true
---
Die ROBOTERMÄRCHEN von Stanislaw Lem sind kybernetische Märchen, wie sie sich Roboter zu erzählen pflegen, wenn sie abends an Sternenfeuern sitzen. Die das ganze Universum bevölkernden Roboterzivillisationen haben zahlreiche Märchen und Legenden. Ihre Themen sind klassisch: Es geht um despotische Könige, mutige Prinzen, schöne Prinzessinnen, Drachen, strahlende Elektritter, Denkmaschinen und andere Wesen voll Weisheit und Güte. Die Geschichten handeln von den Irrtümern, Nöten und Freuden der anorganischen Gesellen - und immer wieder von dem als "Bleichling" bezeichneten, nichtmechanischen Lebewesen namens Mensch, einem unvorstellbar schrecklichen Wesen, das hauptsächlich aus Wasser und Schlonz besteht und absolut nichts von der Reinheit der äußerst sympathischen, denkenden Maschinen hat.

Die letzte Realität der Robotermärchen ist indessen nicht die Wissenschaft, sondern die Sprache. Die Robotermärchen sind brilliante Sprachkunstwerke, die an Variationsreichtum und Schlüssigkeit der Parodie, an Wortwitz und sprachlicher Virtuosität ihresgleichen suchen: raffiniert ausgesponnene Lügenmärchen vom Wunderbaren und Kuriosen, das sich in den Zwischensternländern findet. „Jedes einzelne von Lems Robotermärchen ist ein Meisterstück spielerischen und anspielungsreichen, beispiellos erfinderischen und souverän parodistischen Erzählens" (Heinrich Vormweg).
Die Märchen strotzen nur so vor Wortspielen und Anspielungen und fast immer ist eine kräftige Prise Humor zu finden: ein Klassiker der  humoristischen Science Fiction.

Thomas Hohnerlein: Stimme
Reinhard Köhler: Bass, Coron 
Manuel Staib:  live processing, Keyboard, Synth

Der Abend ist Teil einer Veranstaltungsreihe, die vom Verein Übermorgenwelt in Zusammenarbeit mit der Kunstpool. Galerie am Ehinger Tor, dem Roxy und dem KunstWerk e. V. durchgeführt wird. In der Kunstpool-Galerie ist eine Ausstellung von 19 Künstler*innen aus ganz Deutschland zu sehen, die sich mit dem Werk Stanislaw Lems auseinandersetzen.

Weitere Veranstaltung: 28.7., 4.8., 12.8.

Es wird eine Eintrittsgebühr erhoben: 12 €, ermäßigt 10 €
(Die Facebook-Angabe "freier Eintritt" lässt sich leider nicht abstellen)