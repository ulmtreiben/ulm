---
id: "237273971217240"
title: Amistat * ROXY Sound Garten
start: 2021-08-27 20:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/237273971217240/
image: 203259245_10158600251582756_6213671710005378448_n.jpg
isCrawled: true
---
LIVEMUSIK UNTER FREIEM HIMMEL
EINTRITT FREI! 💛

Die Liebe zum Leben, zur Welt liegt den Jungs im Blut. Geboren in Deutschland (als Söhne einer Tschechin und eines Australiers), verbrachten Jan und Josef ihre Jugend in Italien, gingen später nach Melbourne. Erst der Liebe wegen.

Dann startete dort ihre Musik-Karriere als „Amistat“.

Der Name bedeutet übrigens Freundschaft auf Katalanisch. Freunde, Brüder - Josef und Jan sind beides. Sie sind sehr unterschiedlich, ergänzen sich aber stimmlich und in ihren Botschaften. Auch die Verbindung mit dem Publikum oder anderen Musikern ist es, die Amistat so sympathisch macht.

Und sie reflektieren offen, was sie erlebt und erreicht haben – und was nicht. Live spürst du den Amistat-Zauber am stärksten. An der Seite der australischen Band „Sons of the East“ gaben Amistat 2019 innerhalb von acht Wochen 49 Konzerte in 15 Ländern.

Die Musik von Amistat erinnert an eine moderne Version des Kult-Folk-Duos Simon and Garfunkel. Josef und Jan ließen sich besonders durch Künstler wie Ben Howard, Kings of Convenience oder Kodaline beeinflussen. Mit ihrem Debütalbum Parley, veröffentlicht 2016, tourten sie durch Australien und Europa und erregten dabei derart die Aufmerksamkeit der Sängerin Tash Sultana oder der Band Sheppard, dass sie sofort deren Shows eröffnen durften.

Ihre kommende, fünfte EP  ́ Still Alive ́ erscheint Ende diesen Jahres.

„Still Alive “ ist Amistat‘s Tribut an die Welt und an die Liebe weiterzumachen und sich auch in schweren Zeiten an das gute zu erinnern . Die Brüder wollen Raum geben - zum Fühlen und um Kraft zu schöpfen.

www.amistatmusic.com/