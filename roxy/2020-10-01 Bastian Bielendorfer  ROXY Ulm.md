---
id: "445391146385532"
title: Bastian Bielendorfer * ROXY Ulm
start: 2021-10-02 20:00
end: 2021-10-02 23:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/445391146385532/
image: 79870715_10157022499847756_1609384761900400640_n.jpg
isCrawled: true
---
Lustig, aber wahr!
Perücken aufziehen, in Rollen schlüpfen oder Witze erzählen braucht Bastian Bielendorfer nicht, denn sein Programm ist „Lustig, aber wahr!“ Nach seinem ersten Soloprogramm „Das Leben ist kein Pausenhof“, mit dem er bundesweit zehntausenden Zuschauern Lachtränen in die Augen trieb, kehrt Bielendorfer nun mit seinem zweiten Programm „Lustig, aber wahr!“, das die Schraube des privaten Wahnsinns noch einen Tacken weiterdreht, zurück. Denn wo andere Komiker nur Witze machen, bohrt Bielendorfer tief im eigenen Leben als kinderloser Mopsbesitzer Mitte 30, der das einzige studiert hat, was man in seiner Familie noch mehr braucht als Lehramt: Psychologie. Bielendorfer erzählt von einer Lehrerkindheit unter dem permanenten Rotstift, mit einem Vater, der zwar streng, aber ungerecht war und bis heute ein nicht enden wollender Quell von Absurditäten ist, von seinem Waldorf-Neffen Ludger, der immer eine selbstgehäkelte Kappe aus Lamaschamhaar auf dem Kopf hat und von seiner Frau Nadja, die ihn täglich rettet, meist vor sich selbst... Aber auch abseits seiner Familie zerpflückt Bielendorfer den Alltag – er erzählt davon, wie man einen Heiratsantrag wirklich niemals einleiten sollte, warum er eigentlich nur von Senioren erkannt wird und was die größte Gemeinsamkeit ist, die wirklich alle Menschen teilen.
***
Die Veranstaltung wurde vom 01.10.2020 auf den 02.10.2021 verschoben. Tickets behalten ihre Gültigkeit oder können an den jeweiligen VVK-Stellen zurückgegeben werden.