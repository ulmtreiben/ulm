---
id: "2575848595998379"
title: ROXY Sound Garten mit DJ RéMark
start: 2020-07-23 18:30
end: 2020-07-23 22:00
address: ROXY.ulm
link: https://www.facebook.com/events/2575848595998379/
image: 107375994_10157719007572756_530860666883745025_o.jpg
teaser: Wir freuen uns, euch in unserem Sound Garten begrüßen zu dürfen. Neben dem
  regulären Biergartenbetrieb versüßen euch unterschiedliche DJs die Ohren.
isCrawled: true
---
Wir freuen uns, euch in unserem Sound Garten begrüßen zu dürfen. Neben dem regulären Biergartenbetrieb versüßen euch unterschiedliche DJs die Ohren.

HEUTE (ab 18:30 H): DJ RéMark
Nach langer "Live-Pause" kehrt RéMark außerhalb seiner Radioshow auf free FM wieder einmal an die Plattenteller im ROXY zurück. Besondere Herausforderung: Es gibt ausschließlich 7" Vinyl-Singles auf die Ohren. Das wird sehr soulful mit wohldosierten Ausflügen von Reggae bis Disco und ein einzigartiger Abend für "Singles".

Eine Spende für die musikalische Unterhaltung wird gern entgegen genommen.

Der Biergarten öffnet bereits um 15.00 H.
***

Alle aktuellen Bestimmungen bzgl. Corona findet ihr unter www.roxy.ulm.de .