---
id: "504859840788410"
title: BRTHR * ROXY Sound Garten
start: 2021-07-23 20:30
locationName: ROXY.ulm
link: https://www.facebook.com/events/504859840788410/
image: 218640294_10158667459052756_8362600212734070057_n.jpg
isCrawled: true
---
„High Times For Loners“ ist der beinahe prophetische Titel des neuen Albums von BRTHR - betitelt und aufgenommen, kurz bevor im März 2020 fast überall auf der Welt Menschen die Supermarktregale leerkauften und sich in ihre Wohnungen zurückzogen.

Die Aufnahmen fanden einmal mehr in Max Brauns Studio im Westen Stuttgarts statt, wo die Band wochenlang an Sounds und Arrangements feilte. BRTHR klingen auf ihrem drittem Album frisch und energetisch - kompakte, von Folk und Soul beeinflusste Songs stehen neben luftigen, offener arrangierten Stücken. Philipp Eissler singt unaufgeregt und ohne falsche Sentimentalität von Einsamkeit, Vergänglichkeit und Mauern in Köpfen und an Landesgrenzen. Und doch schwingt in allem das hoffnungsvolle Versprechen hellerer Tage mit - ein Album wie das Licht am Ende des Tunnels. Beim Entstehungsprozess der nunmehr dritten Platte ging es der Band darum, alte Wege und Denkmuster zu hinterfragen. Einfach weiter machen wie bisher? "Wir haben uns damit nicht leicht getan, viel verworfen und überarbeitet, gestritten und dann doch einen gemeinsamen Weg gefunden," so Sänger und Texter Philipp Eissler.