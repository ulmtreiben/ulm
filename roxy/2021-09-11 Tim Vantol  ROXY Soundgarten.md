---
id: "166173198786159"
title: Tim Vantol * ROXY Soundgarten
start: 2021-09-11 20:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/166173198786159/
image: 241486327_10158771233687756_3291411697889435455_n.jpg
isCrawled: true
---
Das Konzert von Tim Vantol, das eigentlich für den 17.09. geplant war, findet jetzt am 11.09. statt. 

Achtung: Sollte das Konzert wegen des Wetters in die Werkhalle verschoben werden, findet es erst um 21:00 H statt. Einlass ist dann ab 20:00 H.

Wenn man den Niederländer Tim Vantol privat besuchen möchte, muss man bis zum Alpensaum in Berchtesgaden reisen: 800 Meter über Null und eine wohltuende Dimension entfernt vom stressigen Trubel der Großstadt.

Berchtesgaden, das sei „Next Level Bayern“, sagt der Sänger. Drei Meter Schnee vor der Haustür gab es letzten Winter, Vantol fühlt sich hier wohl und das hört man in seinen Songs. Der Ort mit seinen netten und zugänglichen Einwohnern trägt wohl auch seinen Teil dazu bei. Die Musik des Albums „Better Days“ lässt sich ebenfalls als „Next Level“ bezeichnen: wie die letzte Perle auf der Schnur muskulöser Rockalben, die dem Niederländer in den letzten elf Jahren eine treue Fanbase sichern konnte.

„Better Days“ ist vermutlich das mit Abstand persönlichste Werk des Musikers. „Meine Songs sind immer aus der Perspektive meines Lebens geschrieben, aber sie sollen gleichzeitig so offen wie möglich bleiben“, sagt der Sänger. Gleich der erste Song „No More“ enthält eine dieser typischen Tim-Vantol-Zeilen, die klingen, als wären sie frisch auf die Seele tätowiert worden: „Brand new directions are waiting for you“ heißt es da, aber es kommt einmal mehr auf die Stimme an, die diese Zeilen singt. Denn Tim Vantols Stimme klingt immer all in. Sie klingt wie der Arm, der einen zurück über die Brüstung ins Fenster zieht, aus dem man gefallen ist. Wie der Rudelführer einer gutartigen Gang oder der letzte verantwortungsvolle Typ in einer verrückten Welt.

Sie klingt, als ob der Sozialismus mit Leuten wie ihm funktionieren würde. Rau, aber eben nur so rau wie ein Wollpullover und nicht wie berstendes Schmirgelpapier. Vor allem klingt sie mitreißend – und das wiederum passt zur einer Musik, die nur den Vorwärtsgang kennt. „Sei dankbar darüber, dass du Familie und Freunde hast und sag es ihnen“, scheint etwa „Tell Them“ zu sagen. Nichts im Leben ist selbstverständlich, umso wichtiger erscheint es, positive Gedanken auch regelmäßig auszusprechen