---
id: "2006807326123962"
title: Jesus George * ROXY Soundgarten
start: 2021-07-19 18:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/2006807326123962/
image: 213811744_10158638211582756_1798136664285639576_n.jpg
isCrawled: true
---
LIVEMUSIK UNTER FREIEM HIMMEL
EINTRITT FREI! 💛

Jesus George ist… 
… wenn sich an einem Abend Lenny Kravitz Selig an Amy Winehouse lehnt – gestützt durch die Kings Of Leon, gezogen von den Red Hot Chili Peppers, gedrückt von The Knack und abgeholt durch die Sportfreunde Stiller.“ 
Vor zehn Jahren als Weihnachtsprojekt geboren, mit Musikern aus dem Raum Ulm, schnürt Jesus George, deren Name aus einem Zitat von Michael J. Fox aus „Zurück in die Zukunft" stammt, ein  überraschendes Paket mit Rock und Roll von den 1960ern bis heute.  
Bei sechs Geschmäckern kommt ein Mix zustande, der  unverkrampft und mit Kultcharakter auf der  Bühne gespielt wird. Cover  –  „eigene Ideen und Kreativität einbringen“ lautet die Devise von Jesus George, die mit Klassikern wie „Come together", „Gimme Shelter“, den Ramones , Clash und den Pixies jede Menge Spaß haben... 

 Wie?  
Gitarre - Gitarre – Bass - Schlagzeug - Gesang - Gesang 
