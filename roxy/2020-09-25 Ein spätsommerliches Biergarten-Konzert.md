---
id: "1243855679285375"
title: Ein spätsommerliches Biergarten-Konzert
start: 2020-09-25 19:00
end: 2020-09-25 19:45
address: ROXY.ulm
link: https://www.facebook.com/events/1243855679285375/
teaser: Mitglieder vom Kammerchor der Universität Ulm haben zusammen mit ihrem Leiter
  Manuel Sebastian Haupt nach Beendigung des Singverbots ein sommerliches,
isCrawled: true
---
Mitglieder vom Kammerchor der Universität Ulm haben zusammen mit ihrem Leiter Manuel Sebastian Haupt nach Beendigung des Singverbots ein sommerliches, kleines Programm einstudiert, das sie zweimal im öffentlichen Raum präsentierten .

In leicht modifizierter Fassung und um neue Lieder erweitert, singt das Ensemble in anderer Besetzung französische Chansons, deutsche Lieder und englische Songs in dem etwa 40-minütigen A-Cappella-Programm.

Die Veranstaltung ist kostenlos.
