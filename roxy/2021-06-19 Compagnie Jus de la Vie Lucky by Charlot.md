---
id: "576599759946146"
title: "Compagnie Jus de la Vie: Lucky by Charlotta Öfverholm"
start: 2021-06-19 20:00
end: 2021-06-19 21:30
address: ROXY.ulm
link: https://www.facebook.com/events/576599759946146/
image: 101955425_2868067909909370_9074506318596800512_o.jpg
teaser: Lucky sehnt sich nach Glück, jung zu sterben, für immer zu leben oder nur im
  Moment. In einer tragikomischen Landschaft tanzen und lachen und das Publ
isCrawled: true
---
Lucky sehnt sich nach Glück, jung zu sterben, für immer zu leben oder nur im Moment.
In einer tragikomischen Landschaft tanzen und lachen und das Publikum ins Rampenlicht rücken lassen. Die One-Woman-Show über das Verlangen nach unendlichem Leben mischt Features wie Tanz, Zirkus, physikalisches Theater, Texte und Musik in einem einzigartigen Cocktail, der das Leben und alles, was folgt, feiert. 
https://ulmmoves.de/programm/jus-de-la-vie-lucky/ 