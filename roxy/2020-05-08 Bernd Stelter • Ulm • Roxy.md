---
id: "1888052317968073"
title: Bernd Stelter • Ulm • Roxy (Verlegt)
start: 2021-04-21 20:00
end: 2021-04-21 23:00
address: ROXY.ulm
link: https://www.facebook.com/events/1888052317968073/
image: 56398360_10156378328973719_7589780006050463744_n.jpg
teaser: 'Bernd Stelter "Hurra, ab Montag ist wieder Wochenende!" Tour  21.04.2021
  (Verlegter Termin vom 08.05.2020) Roxy, Ulm Beginn: 20 Uhr  Aufgrund der aktu'
isCrawled: true
---
Bernd Stelter
"Hurra, ab Montag ist wieder Wochenende!" Tour

21.04.2021 (Verlegter Termin vom 08.05.2020)
Roxy, Ulm
Beginn: 20 Uhr

Aufgrund der aktuellen Situation muss diese Veranstaltung verschoben werden.
Bereits erworbene Tickets behalten ihre Gültigkeit.

Montag morgen. Man steigt in den Wagen und schaltet das Radio an. Mit an Sicherheit grenzender Wahrscheinlichkeit sagt der Moderator spätestens nach dem dritten Lied: „Montag, das ist natürlich nicht unser Tag, aber keine Bange! Nur noch fünf Tage, dann haben wir wieder Wochenende.“ Meine Güte, wenn der keine Lust hat, Radiomoderator zu sein, soll er doch umschulen. Frisöre z.B. haben Montags frei. 

„Hurra, ab Montag ist wieder Wochenende!“ heißt das neue Programm von BERND STELTER. Ein Programm für alle, die über Montage mosern, über Dienstage diskutieren, die Mittwoche mies und Donnerstage doof finden. Übrigens: Auch an Montagen finden Aufführungen statt, obwohl Herr Stelter da Wochenende hat.

Bernd Stelter offizielle Fanseite