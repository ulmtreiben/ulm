---
id: "2819754724905823"
title: Komplex! - Außer M. weiß niemand, dass Barbie Feministin ist
start: 2021-10-06 20:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/2819754724905823/
image: 165117300_10158392334092756_4945802664812727820_n.jpg
isCrawled: true
---
Nachholtermin für den 02.12.2020. Tickets behalten ihre Gültigkeit oder können an den jeweiligen VVK-Stellen zurückgegeben werden.

Tickets, die für den 01.12.2020 oder 05.10.2021 gekauft wurden, sind für den 06.10.2021 gültig (aus zwei Terminen wurde einer).
**********

„Komplex! – Außer M. weiß niemand, dass Barbie Feministin ist“ erzählt von Barbie und der Geschichte des Feminismus, von gesellschaftlich normierten Rollen- und Körperbildern und dem Traum, sich davon zu befreien. 2019 feierte Barbie ihren 60. Geburtstag, seit 1959 bevölkert sie die Kinderzimmer, prägt unsere Vorstellung von Weiblichkeit und festigt ein idealisiertes Schönheitsbild.

Laut Spielzeughersteller Mattel sollte Barbie seit Markteinführung Mädchen ein neues Vorbild geben. Statt Babypuppen wickeln, sollten sie sich mit Barbie eine Zukunft als unabhängige Frau erträumen können. Mit Wespentaille, überlangen Beinen und übergroßen Brüsten hat sie jedoch eine Körperform, die keine Frau* erreichen kann. Trotzdem bestimmt sie bis heute das Idealbild von Weiblichkeit vieler Menschen. Die Lecture Performance „Komplex!“ nimmt die popkulturelle Figur Barbie als Aufhänger, um gesellschaftlich normierte Körper- und Rollenbilder zu thematisieren und ihre Wirkung zu hinterfragen.

Was assoziieren wir mit Barbie? Können wir mit ihr Stereotype entlarven und Klischees unterwandern? Schaffen wir es, dieser Puppe eine raue Seite zu verleihen? Zwei Spieler*innen, ein Koffer voller Barbies, eigens angefertigte Barbie- Modelle aus Alu, Silikon oder Haaren, Musik, selbst geschrieben und live performt auf einem rosaroten Keyboard, und Texte gespickt mit Hintergrundwissen und Selbstironie. Das sind die Elemente des Stückes, die einen vielförmigen Zugang zu den Themen ermöglichen.

Eine Begegnung mit Barbie – endlich Anlass, sich gegenseitig die Meinung zu sagen, sich wertzuschätzen oder Kritik zu üben. Und ganz nebenbei gesellschaftliche Idealbilder unter die Lupe zu nehmen, kapitalistische Vermarktungsstrukturen zu hinterfragen und insbesondere junge Menschen zu ermutigen ein selbstbestimmtes Leben zu führen!


Mitwirkende
Spiel: Coline Petit, Li Kemme | Regie: Iris Keller | Dramaturgie: Anna Renner
Öffentlichkeitsarbeit: Marie-Christine Kesting | Musikalische Beratung: Marius Alsleben
Grafik: Lisa Haberer | Fotos, Video und Trailer: Katharina Kemme 

Koproduktionspartner
FITZ! Stuttgart

Förderer
Gefördert durch den Landesverband Freie Tanz- und Theaterschaffende Baden-Württemberg e.V. aus Mitteln des Ministeriums für Wissenschaft, Forschung und Kunst des Landes Baden-Württemberg.
Gefördert vom Fonds Darstellende Künste aus Mitteln der Beauftragten der Bundesregierung für Kultur und Medien.

Kooperationspartner
Komma, Esslingen – Raumstation, Stuttgart – Figurentheater Eppingen

www.kompanie110.de