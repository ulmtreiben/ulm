---
id: "518889152033373"
title: "Crystal Ball - Support: Dust & Bones * ROXY Ulm"
start: 2021-04-17 21:00
end: 2021-04-17 23:00
address: ROXY.ulm
link: https://www.facebook.com/events/518889152033373/
image: 79278358_10157041056867756_1362383331601154048_o.jpg
teaser: Jubiläumstour 20 Jahre Crystal Ball Präsentiert von Musix und Hardline
  Magazin   Die Melodic Hard Rock/Metal Formation CRYSTAL BALL aus der Schweiz
  ze
isCrawled: true
---
Jubiläumstour 20 Jahre Crystal Ball
Präsentiert von Musix und Hardline Magazin 

Die Melodic Hard Rock/Metal Formation CRYSTAL BALL aus der Schweiz zelebriert ihr 20. Jubiläum mit „2020“ – ihrem ersten Best-Of Album, das am 2. August bei MASSACRE RECORDS erschienen ist! Das Album beinhaltet auf 2 CDs insgesamt 20 Songs – neben 10 neu eingespielten und neu interpretierten Songs aus der ersten Banddekade gibt es auch 10 melodiöse Metaltracks der aktuellen Schaffensphase, die entweder neu eingespielt, neu arrangiert und/oder mit zusätzlichen Aufnahmen aufgewertet wurden. Alle Songs wurden auf Grundlage von Fanbefragungen und Streaming-Analysen ermittelt. Stefan Kaufmann ist abermals für die Produktion, die Aufnahmen und das Mastering sowie in Zusammenarbeit mit Mattes auch für den Mix des Albums im ROXX Studio verantwortlich. Das Artwork stammt von Thomas Ewerhard. „2020“ ist ein Album sowohl für die Fans der ersten Stunde als auch für CRYSTAL BALLNeulinge, das in keiner seriösen Plattensammlung fehlen darf!

***
Das Konzert wurde vom 02.05.2020 auf den 17.04.2021 verschoben! Die Tickets behalten ihre Gültigkeit.