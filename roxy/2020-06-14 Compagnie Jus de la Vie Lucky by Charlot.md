---
id: "869009096830252"
title: "Compagnie Jus de la Vie: Lucky by Charlotta Öfverholm"
start: 2020-06-14 20:00
end: 2020-06-14 21:30
address: ROXY.ulm
link: https://www.facebook.com/events/869009096830252/
image: 78421210_2473102086072623_2828881742813724672_o.jpg
teaser: Lucky sehnt sich nach Glück, jung zu sterben, für immer zu leben oder nur im
  Moment. In einer tragikomischen Landschaft tanzen und lachen und das Publ
isCrawled: true
---
Lucky sehnt sich nach Glück, jung zu sterben, für immer zu leben oder nur im Moment.
In einer tragikomischen Landschaft tanzen und lachen und das Publikum ins Rampenlicht rücken lassen. Die One-Woman-Show über das Verlangen nach unendlichem Leben mischt Features wie Tanz, Zirkus, physikalisches Theater, Texte und Musik in einem einzigartigen Cocktail, der das Leben und alles, was folgt, feiert. 
https://ulmmoves.de/programm/jus-de-la-vie-lucky/ 