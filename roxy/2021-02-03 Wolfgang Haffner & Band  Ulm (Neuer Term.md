---
id: "409264449755105"
title: Wolfgang Haffner & Band // Ulm (verschoben auf 08.03.2022)
start: 2021-08-08 20:00
end: 2021-08-08 22:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/409264449755105/
image: 90718176_10157592324804934_203352210245943296_n.jpg
isCrawled: true
---
👉 Tickets: https://roxyulm.reservix.de/p/reservix/event/1447384 

Weltklasse-Schlagzeuger mit Programm ›Haffner plays Haffner‹ auf Tour
Neu arrangierte Eigenkompositionen in Triobesetzung

Wolfgang Haffner ist ein exzellenter, ja ein begnadeter Schlagzeuger, das weiß man längst nicht nur in der Jazzwelt. Zahlreiche prominente Zusammenarbeiten mit unterschiedlichsten Künstler*innen und Bands, bei denen er seine Spuren hinterlassen hat, national wie international, sprechen für sich. Heute ist der zweifache ECHO-JazzPreisträger ohne Zweifel der bekannteste deutsche Schlagzeuger und zugleich einer
der wenigen deutschen Musiker, die auch weltweit erfolgreich sind.
3500 Konzerte in 100 Ländern, von Japan bis Brasilien, von Südafrika bis Australien, von USA bis Russland. Seine markanten Beats sind auf 400 Alben zu hören. Die Liste der Musiker*innen, mit denen Haffner gemeinsam getourt und aufgenommen hat, ist schier endlos. Al Jarreau, Chaka Khan, Pat Metheny, Gregory Porter, Jan Garbarek, Nils
Landgren, Nils Petter Molvaer, Till Brönner, Esbjörn Svensson, Roy Ayers, Bugge Wesseltoft, The Brecker Brothers, Bill Evans, Bob James, Nightmares on Wax, Ivan Lins,
The Manhattan Transfer, Die Fantastischen Vier, Thomas Quasthoff, Albert Mangelsdorff, Konstantin Wecker, Klaus Doldinger, Ricardo Villalobos, um nur ein paar zu nennen. Wie viele der Besten seines Metiers gab sich Wolfgang Haffner jedoch nie
mit dem reinen Schlagzeugspiel zufrieden, er begann bald mit dem Komponieren und Produzieren. Er gehört heute zu den erfolgreichsten europäischen Jazzmusiker*innen
und Bandleader*innen, er produzierte Künstler wie Max Mutzke und die isländische Fusion-Band Mezzoforte. 
Wolfgang Haffner hat bisher 18 Alben unter eigenem Name veröffentlicht, darunter Erfolgsalben wie ›Zooming‹, ›Shapes‹, ›Round Silence‹, ›Heart
of the Matter‹, ›Kind of Cool‹, ›Kind of Spain‹ und ›Kind of Tango‹.

2021 wird Wolfgang Haffner sein neues Programm ›Haffner plays Haffner‹ präsentieren, in welchem ausschließlich eigene Kompositionen in komplett überarbeiteten Arrangements zu hören sein werden. Für dieses besondere Projekt hat er sich mit Simon Oslender (keys) und Thomas Stieger (bass) gleich zwei Ausnahmemusiker an seine Seite geholt: Simon Oslender gilt als einer der Shooting-Stars der deutschen
Jazzszene. Sein Debüt ›About Time‹, u.a. mit Gaststars Randy Brecker und Bill Evans, schaffte es auf Anhieb in die Top 10 der deutschen Jazzcharts. Er ist Mitglied von Bill Evans & the Spykillers und tourte in seiner noch jungen Karriere bereits in Asien, Europa, Afrika und Australien. Thomas Stieger zählt zu den führenden Bassist*innen
Deutschlands. Er tourte erstmals 2019 mit der Band von Wolfgang Haffner in
Südostasien. Stieger sorgt seit Jahren für das Fundament in der Band von Sarah Connor, darüber hinaus ist der Wahl-Berliner aber auch ein gefragter Sessionmusiker.
