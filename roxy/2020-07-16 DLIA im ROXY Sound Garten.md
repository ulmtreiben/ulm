---
id: "705870753309902"
title: DLIA im ROXY Sound Garten
start: 2020-07-16 20:00
end: 2020-07-16 22:00
address: ROXY.ulm
link: https://www.facebook.com/events/705870753309902/
image: 107093951_10157712660652756_1079490282962100858_o.jpg
teaser: Ein Rapper, eine Sängerin und eine Liveband  DLIA („Das Leben ist Art“) steht
  für handgemachten deutschen Hiphop mit ehrlichen und positiven, aber auc
isCrawled: true
---
Ein Rapper, eine Sängerin und eine Liveband

DLIA („Das Leben ist Art“) steht für handgemachten deutschen Hiphop mit ehrlichen und positiven, aber auch kritischen Texten über persönliche Geschichten, soziale Ungerechtigkeit oder das Leben an sich, gespielt von einer echten Liveband und weit weg vom momentan vorherrschenden Mainstream-Autotune-Deutschrap. Die sechs leidenschaftlichen Musiker vereint vor allem Eins: die Liebe zu Rap, Soul und Funk. Dabei sind die erfahrenen Bandmitglieder auch in der Vergangenheit nicht untätig gewesen. Mit ihren Bands traten sie bereits auf den großen Bühnen Deutschlands, wie etwa dem Southside Festival, Chiemsee Reggae Summer oder als Support für die Beginner auf.

Unser Biergarten hat bereits ab 15:00 H geöffnet.
Eintritt frei.