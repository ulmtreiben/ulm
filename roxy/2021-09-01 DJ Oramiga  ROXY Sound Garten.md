---
id: "917446195510752"
title: DJ Oramiga * ROXY Sound Garten
start: 2021-09-01 18:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/917446195510752/
image: 241138425_10158760499112756_7306649615619013741_n.jpg
isCrawled: true
---
Wir freuen uns, euch in unserem Sound Garten begrüßen zu dürfen. Neben dem regulären Biergartenbetrieb versüßen euch unterschiedliche DJs die Ohren.

Eintritt frei, Spende für die musikalische Unterhaltung wird gern entgegen genommen.
Der Biergarten ist ab 15:00 H geöffnet.
***
Alle aktuellen Bestimmungen bzgl. Corona findet ihr auf unserer Homepage www.roxy.ulm.de