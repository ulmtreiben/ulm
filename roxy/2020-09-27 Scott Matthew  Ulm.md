---
id: "453822088659613"
title: Scott Matthew | Ulm
start: 2021-10-17 20:00
end: 2021-10-17 23:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/453822088659613/
image: 78889535_10162742593090261_8999158693710790656_n.jpg
isCrawled: true
---
Scott Matthew

17.10.21
ROXY.ulm
Einlass: 19 Uhr | Beginn: 20 Uhr

Tickets sind ab 22€ zzgl. Geb. online unter www.schoneberg.de und an ausgewählten VVK-Stellen erhältlich.

#makeitbeautifulnow 

► www.scottmatthewmusic.com
► https://cutt.ly/8EY61TT ♫ (The Wish)
► https://cutt.ly/GEUatLy ♫ (Lyric Clip)

Der Singer-/Songwriter stammt aus Queensland, Australien und lebt seit vielen Jahren in New York. Er beschreibt sich selbst als 'Quiet-Noise-Maker' mit großer Leidenschaft für Balladen. Im Jahr 2006 wurde Scott Matthew dank mehrerer Soundtrack-Beiträge zum Erotikdrama SHORTBUS von Regisseur John Cameron Mitchell einem breiteren Publikum bekannt. Neben dem von ihm komponierten Titelsong 'In The End' schrieb und performte er fünf weitere Songs für diesen Film.

Fans der japanischer Animations-Serie GHOST IN THE SHELL kamen jedoch schon früher in den Genuß seiner eindringlichen Stimme. Für die Folgen der Staffel 'Stand Alone Complex' , die in Deutschland durch die Ausstrahlungen bei MTV zu Ruhm kam, hat er insgesamt sieben Titel des von YOKO KANO komponierten Soundtracks gesungen.

Für die Aufnahmen seiner Christmas-EP SILENT NIGHTS (2008) lud er seine Australische Freundin und Kollegin SIA zum Duett ins Studio. Für den heutigen Mega-Star durfte Scott Matthew bereits in England und Australien mehrere Konzerte eröffnen.

Der begnadete Songpoet hat sich seit seinem selbstbetitelten Debut von 2008 mit seinen bisherigen sieben Solo-Alben höchsten Respekt unter Kritikern und Fans gleichermaßen erarbeitet. Er schreibt und singt vom Herbeisehnen und Verschwinden der Liebe, wie kaum jemand sonst – immer zutiefst einfühlsam, berührend arrangiert und in ganz eigenem Stil.

Wer Scott Matthew live erlebt, ist seltsam ergriffen. Er macht Leid zu Lied. Er entkleidet sein Inneres. Er lässt die Hörer teilhaben, teilnehmen, Teil werden. Scott Matthew berührt. Die Intensität seines Gesangs ist greifbar. Auf seiner Tournee im Oktober 2021 in Deutschland und Österreich wird er musikalisch begleitet von seinen treuen Wegbegleitern Marisol Limon Martinez (Piano) und Sam Taylor (Cello + Gitarre). 

Fotocredit: Michael Mann