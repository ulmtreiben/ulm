---
id: "488864115508601"
title: Diana Ezerex * ROXY Soundgarten
start: 2021-08-19 20:30
locationName: ROXY.ulm
link: https://www.facebook.com/events/488864115508601/
image: 237566758_10158730894852756_991792383042885679_n.jpg
isCrawled: true
---
LIVEMUSIK UNTER FREIEM HIMMEL
EINTRITT FREI! 💛

„Um einen Staat zu beurteilen, muss man sich seine Gefängnisse von innen ansehen.“ (Leo Tolstoi) 
Seit Mai 2017 spielt Diana Ezerex ehrenamtlich Konzerte in Gefängnissen. Ihr Engagement wurde bisher mit drei Auszeichnungen gewürdigt. Im März 2020 wurde sie dazu inspiriert, über Themen, die im Zusammenhang mit dem Gefängnis stehen, ihr Debüt-Album zu schreiben, welches am 25.06.2021 veröffentlicht wird. Die Songs befassen sich mit Themen der Vergangenheit, Gegenwart und Perspektive. Sie greifen Tabu-Themen, strukturelle Probleme und soziale Ungerechtigkeiten auf. Fragen nach mentaler Gesundheit, Suizid, Akzeptanz und Zurechtfinden in der Gesellschaft werden gestellt. Aus der Auseinandersetzung mit dem Setting Gefängnis entstanden Songs, die für Gesellschaft Relevanz haben. 
Die Musik bewegt sich zwischen (Urban) Pop, Hip-Hop/Trap & Soul. Produziert von Thorsten Rheinschmidt entstanden Sounds, die gemeinsam mit ihrer tiefen, rauchigen Stimme der Energie von Rag’n’Bone Man, der Intensität von Bon Iver und der Gelassenheit von Anderson .Paak ähneln – trotz der Schwere der Themen. 
Mithilfe von Musikvideos zu jedem Song, eines Buchs, Theaterstücks und Kurzfilms, kreativer Workshops für Jugendliche, Projekten mit Jugendorchestern, einer Remix-Version des ganzen Albums sowie einer Benefizausstellung zugunsten kreativer Resozialisierungsmaßnahmen sollen die Themen greifbar und auf verschiedenen Ebenen zugänglich machen. 
In Zusammenarbeit mit Künstler*innen aus ganz Europa entsteht so ein künstlerisch breit aufgestelltes Projekt, welches auf verschiedenen Ebenen durch unterschiedliche Medien soziale Missstände aufzeigt. Am Setting Gefängnis – denn „nirgendwo anders lässt sich der Grad der gesellschaftlichen Entwicklung eindeutiger fassen“ (Rudi Dutschke) – sollen allgemeine gesellschaftliche & strukturelle Themen verarbeitet, veranschaulicht und zu Veränderung bewegt werden. 
