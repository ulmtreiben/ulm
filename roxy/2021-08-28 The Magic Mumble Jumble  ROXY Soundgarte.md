---
id: "1354567324925519"
title: The Magic Mumble Jumble * ROXY Soundgarten
start: 2021-08-28 20:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/1354567324925519/
image: 191443767_10158535090702756_7150783774913666132_n.jpg
isCrawled: true
---
LIVEMUSIK UNTER FREIEM HIMMEL
EINTRITT FREI! 💛

“Es passiert so viel Gutes, das ist doch geil! Wir leben in einer spannenden Zeit" - sagt Paul Istance der Songwriter und Bandleader. The Magic Mumble Jumble ist schon immer eine Band mit positiver Message und wahrer Lebensfreude - und sie scheinen damit ins Schwarze zu treffen, denn der Tourkalender ist von April bis November bis oben hin voll! 

Seit ihrer Gründung veröffentlichte die Band mehrere Singles und der Song "Home" erschien 2015 auf einer Sony Compilation. Diesen Veröffentlichungen folgte 2016 ihr Debüt Album “The Magic Mumble Jumble” - ein einzigartiger Live-Mitschnitt der Band zusammen mit mehr als 60 Musikern. Die CD wurde aufgrund der hohen Nachfrage schon mehrfach reproduziert. Im Herbst 2017 veröffentlichten sie die EP “We All Want Sunshine” mit der gleichnamigen Singleauskopplung und brachten daraufhin im November 2018, ihr erfolgreiches zweites Album 'Show your Love’ auf den Markt. Dieses Album nahm die Band zusammen mit dem Produzenten Tom Gelissen auf (Ennio Morricone, Woodkid, Armin van Buuren). 2019 wurde THE MAGIC MUMBLE JUMBLE eine große Ehre zu Teil: Das “Burg Herzberg Festival” bat die Band die sensationellen Live Aufnahmen des Vorjahres zusammen als Live Album zu veröffentlichen. “Live at Burg Herzberg Festival” ist ein Werk, dass natürlicher nicht sein könnte und und die stärkste Seite der Band zeigt: eine Live-Performance die ihres Gleichen sucht.  

Paul Istance präsentiert ein kontinuierlich tiefgründiges und vielschichtiges Songwriting, das auch die gesellschaftlichen Themen unserer Zeit widerspiegelt. 2020 ist für ihn besonders aufregend und spannend, weil sich in unserer Gesellschaft in vielerlei Hinsicht ein Wandel abzeichnet. “Wir haben beispielsweise eine starke Nachhaltigkeitsbewegung. Menschen durch alle Schichten sind davon betroffen, sorgen sich um den Klimawandel und reagieren darauf im privaten Leben! Ich habe es noch nie erlebt, dass so viele Menschen an einem Strang gezogen haben. Ich bin stolz Teil einer solchen Zeit zu sein. Ist doch Wahnsinn! Times are changing!" 
