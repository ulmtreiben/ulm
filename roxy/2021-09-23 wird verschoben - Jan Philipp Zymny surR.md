---
id: "422695155841861"
title: wird verschoben - Jan Philipp Zymny "surREALITÄT" (Ulm)
start: 2021-09-23 20:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/422695155841861/
image: 141337728_10158227178097756_4872132986215474338_n.jpg
isCrawled: true
---
*** Die Veranstaltung wird auf den 12. Mai 2022 verschoben. Karten behalten ihre Gültigkeit ***

Wer würde es in Zeiten alternativer Fakten wagen die Wirklichkeit selbst zu kritisieren? In Zeiten, in denen die Menschen den Blick für die Realität immer mehr verlieren? In Zeiten, in denen man sich Esoterik und pseudowissenschaftlichen Theorien hingibt und wieder anfängt rückwärts zu denken? Nur jemand, der gedanklich völlig außerhalb davon steht!
Der junge Künstler Jan Philipp Zymny präsentiert in seinem mittlerweile vierten abendfüllenden Soloprogramm unter dem Titel „surREALITÄT“ Betrachtung, Kritik und Verbesserungsvorschlag der Wirklichkeit, wobei er Stand Up, Kurzgeschichten, philosophische Überlegungen und surreale Absurditäten der Bauart Nonsens wild, aber keines Falls planlos durchmischt. Klassischer Zymny eben. Ein Abend für alle, denen gewöhnliche Comedy zu doof, Philosophie zu anstrengend und die Realität zu langweilig ist.