---
id: "260523055290670"
title: TanzLokal
start: 2021-06-20 20:00
end: 2021-06-20 22:00
address: ROXY.ulm
link: https://www.facebook.com/events/260523055290670/
image: 82243157_2868064309909730_1627180331826151424_o.jpg
teaser: "Hier trifft Kondition auf Beweglichkeit, Impro auf Jazz und Spitze auf
  Streetdance!  „Ulm Moves!“ mal wieder ganz wörtlich genommen: An diesem Abend
  g"
isCrawled: true
---
Hier trifft Kondition auf Beweglichkeit, Impro auf Jazz und Spitze auf Streetdance! 
„Ulm Moves!“ mal wieder ganz wörtlich genommen: An diesem Abend gewähren Amateurgruppen und professionelle Compagnies aus Ulm und Neu-Ulm mit ihren aktuellen Choreografien einen wunderbaren Einblick in die Tanzszene unserer Region! 
Der Abend wird moderiert von Domenico Strazzeri.
VVK und AK 10,00 €