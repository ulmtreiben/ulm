---
id: "233770767815742"
title: Sarah Bosetti * ROXY Ulm
start: 2021-04-22 20:00
end: 2021-04-22 23:00
address: ROXY.ulm
link: https://www.facebook.com/events/233770767815742/
image: 92115738_10157401279302756_8029022776654299136_o.jpg
teaser: Nachholtermin vom 18.03.2020. Tickets behalten ihre Gültigkeit. **** Ich hab
  nichts gegen Frauen, du Schlampe! - Mit Liebe gegen Hasskommentare  Sarah
isCrawled: true
---
Nachholtermin vom 18.03.2020. Tickets behalten ihre Gültigkeit.
****
Ich hab nichts gegen Frauen, du Schlampe! - Mit Liebe gegen Hasskommentare

Sarah Bosetti hat eine Superkraft: Sie kann Hass in Liebe verwandeln! Während sich ganz Deutschland fragt, was wir gegen die Wut und Feindseligkeit in unserer Gesellschaft tun können, versammelt sie die schönsten Hasskommentare, die sie bekommt, und macht aus ihnen lustige Liebeslyrik und witzige Geschichten. Misogynie wird zur Pointe, Sexismus zu Schmalz und irgendwo dazwischen wird das Patriarchat zu Poesie.

Sarah Bosetti findet Feminismus anstrengend und ist zugleich eine der präsentesten und witzigsten feministischen Stimmen auf Deutschlands Kabarettbühnen. Sie ist bekannt aus Die Anstalt im ZDF, Nuhr im Ersten, der ARD Ladies Night, den Mitternachtsspitzen im WDR und als Kolumnistin bei radioeins (RBB) und WDR2. Das Buch zum neuen Programm erscheint bei Rowohlt.

"Bosetti ist nicht nur witzig. Ihr Kommentar zum Feminismus ist fantastisch." – Cornelius Oettle, Stuttgarter Nachrichten

"Sarah Bosetti zeigt, was entsteht, wenn Hasskommentare auf klugen Witz und einen scharfen Verstand treffen." – Horst Evers

"Frau Bosetti ist ein Genre für sich!" – Gerburg Jahnke

***
Die Veranstaltung wurde von 09.10.2019 auf den 18.03.2020 verschoben. Tickets behalten ihre Gültigkeit oder können an den jeweiligen Vorverkaufsstellen zurückgegeben werden.