---
id: "868306507381004"
title: Bienen 1 - zwischen Himmel und Erde. Heyoka Theater * ROXY Ulm
start: 2021-10-24 17:00
end: 2021-10-24 19:00
address: ROXY.ulm
link: https://www.facebook.com/events/868306507381004/
image: 243120660_10158809715742756_26350494870635262_n.jpg
isCrawled: true
---
"Letzte Nacht als ich schlief da träumte ich – wundervoller Irrtum – ich hätte einen Bienenstock hier in meinem Herzen. Und die goldenen Bienen verwandelten meine alten Fehler in weiße Waben und süßen Honig." (Antonio Machado)

'Räume zwischen Himmel und Erde' ist der erste Teil unserer BIENEN-Trilogie. Es folgen im Sommer 2022 die Produktion 'Delight' und im Herbst 2022 der Dokumentarfilm 'Von Bienen, Narren und Menschen'.

Wenn wir sagen "wir erzählen euch in diesen Produktionen was wir über die Bienen wissen" stimmt das nicht wirklich. Eher können wir sagen: wir wollen euch erzählen, was wir über die Bienen gelesen, was wir beobachtet, im Umgang mit ihnen erfahren und dabei über uns selbst herausgefunden haben. nUnsere Bilder, unsere Sprache.

In diesem Sinne gibt es viele Wahrheiten, die nebeneinanderstehen dürfen.

Und was haben die Bienen davon?!
Wir können uns vorstellen, wir hoffen und wünschen, dass die Arbeit mit diesen erstaunlichen Wesen positive Saiten in uns Menschen zum Klingen bringt.
Wenn wir uns erlauben, offen zu sein für Neues, für das, was wir ganz persönlich als WAHR empfinden.
In dieser Form könnte die Hinwendung zu den Bienen tatsächlich ein 'Entwicklungsweg' für uns werden, auch wenn wir nie mit Sicherheit sagen können, wie die Bienen diese Art der Zusammenarbeit erleben. Möge sie für alle Seiten fruchtbar und voller Freude sein.

mit: Finja Bursiek, Lotti Conzelmann, Finja und Greta Ellerkamp, Martin Gah, Gisa Göser, Paul und Thomas Greulich, Jakob Hochstrasser, Johann Lautenschläger, Elizaweta Lerman, Mareike Lorenz, Georg Metzenrat, Johannes Mohn, Gisela Stummer, Heike Tiessen, Birgit Walcher, Florian Wanke

Musik: Thomas Ellerkamp, Finn Kiefl, Alexander Jassner, Eva Klopp, Daniel Köhl, Elizaweta Lerman

Bühne: Patricia Langner

Kostüme: Sylvie Bursiek, Thomas Greulich, Beate vom Hagen Prem, Rosiane de Menezes

Bienen und Webstuhl: Holger Balfanz

Film: Simon Reimold

Stück und Regie: Eva Ellerkamp

DANKE an: Bärbel Bentele, Andrea Kögel, Rübe Prem, Felix Springer
***
Mit freundlicher Unterstützung von Stadt Ulm, SWU und Otto Kässbohrer-Stiftung