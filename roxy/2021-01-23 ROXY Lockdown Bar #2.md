---
id: "1701983963308542"
title: "ROXY Lockdown Bar #2"
start: 2021-01-23 20:30
link: https://www.facebook.com/events/1701983963308542/
image: 141328965_10158227216527756_5527534280858517174_o.jpg
teaser: Bei der Premiere am letzten Samstag hat sich die neue ROXY LOCKDOWN BAR als
  probates Gegenmittel gegen das depressive Trübsalblasen am Lockdown-Wochen
isCrawled: true
---
Bei der Premiere am letzten Samstag hat sich die neue ROXY LOCKDOWN BAR als probates Gegenmittel gegen das depressive Trübsalblasen am Lockdown-Wochenende erwiesen. Gleich mehrere hundert Barbesucher:innen waren online vom heimischen Sofa aus mit dabei - mega!


Ab 20:30 H öffnen sich zum zweiten Mal die virtuellen Türen der LOCKDOWN BAR - online, live. Ariane Müller hat wieder interessante Gäste engeladen:
Livemusik mit und von Joo Kraus & Band
Seit vielen Jahren zählt Joo Kraus nun schon völlig zu Recht zu den besten Jazztrompetern in unseren Breitengraden. Als Bandmitglied, Solist, vielgebuchter Sessionprofi (BAP, DePhazz, Jazzkantine, Soulounge) und Sideman namhafter Künstler (Pee Wee Ellis, Johannes Enders, Peter Fessler, Omar Sosa, Paula Morelenbaum) hat er gleichermaßen überzeugt. Mit seinem mal butterweichen, mal staccato-spitzen Trompetenton konnte er im In- und Ausland viele Freunde gewinnen. Ihn begleiten an diesem Abernd die Musiker Ralf Schmid (Klavier), Veit Hübner (Bass) und Torsten Krill (Schlagzeug).


Talk mit Roland Baisch
Roland Baisch blickt inzwischen auf 40 erforgreiche Dienstjahre im Unterhaltungsgeschäft mit mehr als 4000 Shows als Komiker, Musiker und Schauspieler zurück. Der Senior Comedian war Mitglied des Scherbentheaters und der legendären Shy Guys, machte bei der Comedy Factory mit, tourte mit „Caveman“ Martin Luding im „Männerabend“ oder eigenen Programmen wie „Der graue Star“, „Der goldene Roland“, mit den Countryboys, dem Countbaischy Orchester oder „KGB – Kuhnle Gaedt Baisch“ durch die Lande.  Baisch ist Träger des „Goldenen Rolands“ und  Preisträger des Kleinkunstpreises Baden-Württemberg.
In seinem jüngsten Bühnenprogramm „Wie haben Sie das gemacht, Herr Baisch“ geht Roland Baisch den Fragen nach, wie es gelingt und was es bedeutet, andere zum Lachen zu bringen. Braucht man einen genetischen Defekt? Wird man als Komiker geboren? Hat man gute Dauerlaune? War man schon als Kind Klassenclown? Was treibt einen Menschen auf die Bühne?

Wer also wäre ein besserer Gesprächspartner für Ariane Müller, wenn es um die Frage geht, wie man in Pandemiezeiten seinen Humor behält?


Mach's dir selbst - das Cocktail-Tutorial
Im letzten Sommer hat ROXY-Barchef Michl im ROXY Soundgarten gemeinsam mit seinem Team wahrscheinlich mehrere tausend Drinks gemixt. Auch heute wird er mit euch zusammen was Feines zusammenbrauen (nur für Volljährige):

// Touchdown
4,0 cl Vodka
2,0 cl Apricot Brandy
2,0 cl Zitronensaft
1,0 cl Grenadinesirup
10,0 cl Maracujasaft
* Zubehör
Boston Shaker
Strainer (Sieb)
Eis
Longdrinkglas

// Cosmopolitan
4,0 cl Vodka mit Zitronenaroma
1,5 cl Triple Sec
1,5 cl Limettensaft
3,0 cl Cranberrysaft oder Nektar
*Zubehör
Boston Shaker
Strainer (Sieb)
Eis
Martiniglas oder Coupette


Noch Fragen? Dann bleib zum Zoom-Thementalk und Aftershow Bargespräch
Im Anschluss an den Livestream ist eure Mitwirkung gefragt: Joo Kraus, Michl, Roland Baisch und Ariane Müller stehen euch für ein persönliches Bargespräch via Zoom zur Verfügung (Link kommt dann via Livechat).

Hier könnt ihr teilnehmen:

YOUTUBE
https://youtu.be/MYu3igujRFM

FACEBOOK
https://www.facebook.com/roxy.kultur/posts/10158225065292756

HOMEPAGE
https://www.roxy.ulm.de