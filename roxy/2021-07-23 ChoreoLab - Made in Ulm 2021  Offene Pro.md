---
id: "522780005731527"
title: ChoreoLab - Made in Ulm 2021 * Offene Probe
start: 2021-07-23 17:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/522780005731527/
image: 215374727_10158649235632756_4187842311990024890_n.jpg
isCrawled: true
---
ChoreoLab - Made in Ulm ist eine kostenlose offene Probe für Tanzbegeisterte und Neugierige. Die einstündige Probe schafft einen offenen Raum für den Diskurs zwischen Publikum, Choreograf und Tänzern und gibt Tanzinteressierten die Möglichkeit, Einblicke in den choreografischen Prozess zu erhalten. Jede Open Rehearsal wird von einem anderen Choreographen geleitet. Kommt vorbei, schaut hinter die Kulissen und bekommt einen Eindruck davon, wie sich der kreative Prozess gestaltet und bei jedem Choreografen anders ist.

Ein negativer Test oder der Nachweis eines vollständigen Impfschutzes ist erforderlich.

Eine Anmeldung unter anmeldung@roxy.ulm.de ist erforderlich.

***

The ChoreoLab – Made in Ulm is a free open rehearsal for dance enthusiasts and people of curious nature. The one hour rehearsal creates an open space for discourse between the audience, choreographer and dancers, giving dance lovers the opportunity to acquire insight into the choreographic process. Each Open Rehearsal will be led by a different choreographer. Come and check out behind the scenes and get a feeling of how each creative process takes shape and is different for each choreographer.

A negative test or proof of full vaccination required.

 People need to register to anmeldung@roxy.ulm.de