---
id: "1045731639198830"
title: "Gästeliste #11 * ROXY Ulm (verschoben auf unbekannt)"
start: 2021-01-16 20:30
address: ROXY.ulm
link: https://www.facebook.com/events/1045731639198830/
image: 122086404_10157998712047756_176670230220310072_o.jpg
teaser: Das Konzert muss verschoben werden. Leider konnte noch kein Ersatztermin
  gefunden werden. Dieser wird so bald wie möglich nachgereicht. Tickets bahelt
isCrawled: true
---
Das Konzert muss verschoben werden. Leider konnte noch kein Ersatztermin gefunden werden. Dieser wird so bald wie möglich nachgereicht. Tickets bahelten ihre Gültigkeit oder können an den jeweiligen Vorverkaufsstellen zurückgegeben werden.
***
Patrick Wieland lädt ein! Auf seiner Gästeliste sind diesmal gleich zwei Gäste, nämlich Luke Noa und Lemony Rug. Beides Preisträger des jungen Ulmer Kunstpreises. Sie sind Singer Songwriter, die der großen internationalen Konkurrenz durchaus Stand halten können.

Luke Noa : Luke Noa schreitet voran, ohne sich im gegenwärtigen Geschwindigkeitswahn zu verlieren. Als Kind der Neuzeit zeichnet er einen Weg fernab von musikalisch vorgemalten und allzu definierten Bildern. Vielmehr macht er sich den Stilpluralismus der Moderne zunutze, um Grenzen aufzuweichen, und kreiert im Songwriting eine Collage aus Pop, Soul, Hip Hop und Folk.

Lemony Rug : Mit seinem Soloprojekt Lemony Rug setzte der Songwriter Léon Rudolf alles auf Null und Neustart, denn nach Jahren der Suche und des Ausprobierens hat er endlich einen für ihn authentischen Weg gefunden, sich musikalisch auszuleben. Auf seiner kommenden EP verschmelzen zwischen erfrischenden Arrangements und vertrauten Gitarrenklängen kantige Indie-Rock- mit verträumten Dream-Pop- Elementen zu einem Sound, der sich am besten in einer Stimmung zusammenfassen lässt: Lemony Rug.