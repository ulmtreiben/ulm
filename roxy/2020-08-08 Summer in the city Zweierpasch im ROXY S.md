---
id: "275524433722973"
title: "Summer in the city: Zweierpasch + Cris Cosmo im ROXY SoundGarten"
start: 2020-08-08 20:00
end: 2020-08-08 23:00
address: ROXY.ulm
link: https://www.facebook.com/events/275524433722973/
image: 107108714_10157712948712756_9014707736276203071_o.jpg
teaser: "MAIN ACT: Zweierpasch Poetisch, politisch, polyphon: Zweierpasch gehen über
  Grenzen - sprachlich und geografisch. Mit ihrem rebellischen World HipHop"
isCrawled: true
---
MAIN ACT: Zweierpasch
Poetisch, politisch, polyphon: Zweierpasch gehen über Grenzen - sprachlich und geografisch. Mit ihrem rebellischen World HipHop prägt die Band aus Freiburg und Straßburg die Szene auf ihre ganz eigene Weise. Das haben die lyrischen Grenzgänger bei Tourneen in Europa, Afrika und Asien bewiesen. 2017 waren sie "Freiburgs Band des Jahres". Im November 2018 haben sie in Paris den Adenauer-de-Gaulle-Preis erhalten. 2019 spielten sie im westafrikanischen Mali vor 10.000 Fans. Im Zeichen von Frieden, Austausch und Toleranz touren sie mit deutsch-französischen Tracks durch die Welt. Begleitet werden die zweisprachigen Zwillinge und Frontmänner Till und Felix von ihrer fünfköpfigen Band, die Zweierpasch-Shows zum virtuosen Spektakel machen: Rap trifft auf souligen Gesang, Reggaerhythmen auf funkige Bläser, rockige Riffs auf flüsternde Melodien.

Felix Neumann (Gesang)
Till Neumann (Gesang)
Christian Haber (Keys)
Moritz Ulrich (Drums)
Michael Holland (Bass)
Stefan Harth (Gitarre/Gesang)
Valentin Matt (Saxofon)
Patrick Schäfer (Sousaphon/Posaune)

Hörproben
>> "Farbenrausch": bit.ly/farbenrausch_yt
>> "Fake": bit.ly/fake_zp
>> "Lichter": bit.ly/lichter_zp

OPENING: Cris Cosmo
Einst Straßenmusiker, dann Echo- und German-Songwriting-Award Nominee, Bundesvision Songcontest Teilnehmer und BlitzSongwriter – Cris Cosmo schreibt positive, reflektierte deutsche Texte und mischt sie mit Pop, Reggae und HipHop zu einem Sommercocktail voller guter Vibes!

Eintritt frei.
Der Biergarten ist ab 15:00 H geöffnet.

***
Konzerte in der Reihe "Summer in the city" sind eine Kooperation von Kunstwerk e.V. und ROXY gGmbH.
Wir danken für die Unterstützung durch das Ministerium für Wissenschaft, Forschung und Kunst Baden-Württemberg.
