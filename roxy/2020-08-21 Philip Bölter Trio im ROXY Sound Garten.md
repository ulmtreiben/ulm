---
id: "3228373740550753"
title: Philip Bölter Trio im ROXY Sound Garten
start: 2020-08-21 20:30
end: 2020-08-21 23:00
address: ROXY.ulm
link: https://www.facebook.com/events/3228373740550753/
teaser: Mit ihrem Debut-Album “BÖLTER” feierte das Trio bereits vor einem Jahrzehnt
  erste Radioerfolge. In 2012 gewannen sie den Deutschen Rock & Pop Preis in
isCrawled: true
---
Mit ihrem Debut-Album “BÖLTER” feierte das Trio bereits vor einem Jahrzehnt erste Radioerfolge. In 2012 gewannen sie den Deutschen Rock & Pop Preis in der Kategorie 
„Beste Country Band“. Vom Country ist heute nicht mehr viel übrig, von den Live-Band-Qualitäten dafür umso mehr. „Es dauert nur wenige Takte, bis die drei mit ihrem natürlichen Auftreten und einer einzigartigen Mischung aus Charme, Esprit und Können die Zuhörer auf ihre Seite gezogen haben. Jeder Song ﬂießt locker und leicht, die Band ist fast schon symbiotisch aufeinander eingespielt. Alles wirkt mühelos und kommt von Herzen.“ - hieß es damals.
Weit mehr als 1.000 Konzerte, 10 Alben und unzählige gerissene Saiten - sein halbes Leben verbachte Frontmann Philip Bölter als Musiker auf der Bühne. Über die Jahre machte sich der nun 31 jährige als Sänger und, ausgezeichnet mit dem „Robert Johnson Guitar Award“ als einer der besten Gitarristen Deutschlands, einen Namen in der Live-Szene. 2013 präsentierte er sich als Sänger im TV einem Millionenpublikum und arbeitete an seiner Solokarriere. Er war unterwegs mit u.a. „Ryan Sheridan“, „Tom Walker“, „Tito & Tarantula“, „Django3000“ und den „Fiddlers Green“. Nach der langen Solo-Tour kehrt er nun gemeinsam mit seinen Kollegen Heiko Peter (ehem. „Wendrsonn“, „Submarien“) und Steffen Knaus (u.A. Yasi Hofer) im Trio zurück auf die Open Air-Bühnen.

Unser Biergarten öffnet bereits um 15:00 H.
Eintritt frei.