---
id: "1073948039606974"
title: TanzLokal
start: 2020-06-13 19:00
end: 2020-06-13 20:00
address: ROXY.ulm
link: https://www.facebook.com/events/1073948039606974/
image: 82837730_2570958282953669_5212823028588609536_o.jpg
teaser: "Hier trifft Kondition auf Beweglichkeit, Impro auf Jazz und Spitze auf
  Streetdance!  „Ulm Moves!“ mal wieder ganz wörtlich genommen: An diesem Abend
  g"
isCrawled: true
---
Hier trifft Kondition auf Beweglichkeit, Impro auf Jazz und Spitze auf Streetdance! 
„Ulm Moves!“ mal wieder ganz wörtlich genommen: An diesem Abend gewähren Amateurgruppen und professionelle Compagnies aus Ulm und Neu-Ulm mit ihren aktuellen Choreografien einen wunderbaren Einblick in die Tanzszene unserer Region! 
Der Abend wird moderiert von Domenico Strazzeri.
VVK und AK 10,00 €