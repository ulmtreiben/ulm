---
id: "776018436225194"
title: LOTTE | ROXY - Ulm
start: 2021-05-29 20:00
end: 2021-05-29 23:00
address: ROXY.ulm
link: https://www.facebook.com/events/776018436225194/
image: 116741211_177938617063635_8411544441826300737_o.jpg
teaser: "LOTTE 29.05.2021 Ulm /// ROXY Einlass: 19:00 /// Beginn: 20:00  Präsentiert
  von kulturnews /// Schall. Musikmagazin /// MTV Germany  Nachdem LOTTE im"
isCrawled: true
---
LOTTE
29.05.2021
Ulm /// ROXY
Einlass: 19:00 /// Beginn: 20:00 
Präsentiert von kulturnews /// Schall. Musikmagazin /// MTV Germany

Nachdem LOTTE im Februar 2020 vor ausverkauften Häusern die Songs ihres zweiten Albums “Glück“ erstmalig auf Tour präsentierte, wird die Sängerin aus Ravensburg im Frühjahr 2021 in weiteren deutschen Städten und in Luxemburg spielen. Mit dabei auch ihre neue Single „Mehr davon“.

Bereits auf ihrem letzten Album war zu spüren, wie wichtig es LOTTE ist, sich ganz dem Augenblick zu widmen, um im Hier und Jetzt das meiste aus dem Leben zu machen. Der Nachfolger von LOTTEs Debüt „Querfeldein“ baut auf dem organischen Sound des ersten Albums auf und führt es mit Synthesizern der Achtziger und modernen, elektronischen Elementen zusammen. LOTTE durchquert die Höhen und Tiefen des menschlichen Glücksstrebens, mit präzisem Gespür für Dramaturgie und mitreißenden Refrains. In „Mehr Davon“ trägt LOTTE dieses Sich-Hingeben noch weiter, Pauken und Bläser kreieren einen unwiderstehlichen Tanzsong.

Diese Spannung zwischen Melancholie und großen Euphorie-Momenten findet sich auch bei ihren Auftritten wieder. Nachdenkliche Lieder, bei denen LOTTE nur von einem Klavier begleitet wird, wechseln sich ab mit mitreißenden Pop-Songs. LOTTE liebt die Nähe zum Publikum: Sie feiert nicht nur auf der Bühne mit ihren Fans, sondern verlässt diese auch häufig, um zusammen mit ihnen zu tanzen. In diesen Augenblicken findet man dann tatsächlich: Glück.