---
id: "533922371263175"
title: Bernd Gieseking - Finne dein Glück! * Ulmer Leseorte
start: 2021-08-29 20:00
link: https://www.facebook.com/events/533922371263175/
image: 235596254_10158733150407756_6418011914237754053_n.jpg
isCrawled: true
---
Ulmer Leseorte - Wo Geschichten lebendig werden
Im Me And All Hotel Ulm

Finne dein Glück! Eine kabarettistische Spurensuche im Land der Mitternachtssonne Neues vom Finnland-Flüsterer: Bernd Gieseking ist erneut unterwegs im Land der Glücksweltmeister. Er reist von Süd nach Nord, von Helsinki nach Inari und dann über Rovaniemi, Oulu, die Insel Hailuoto und Turku wieder zurück. Er besucht langjährige Freunde und trifft Künstlerinnen und einen Bierbrauer, eine Bischöfin, einen Tierarzt, eine Mumin-Expertin, einen ehemaligen Musiker der Leningrad Cowboys, einen Mitarbeiter des samischen Parlaments, die finnische Vize-Meisterin im Hobby Horsing in der Disziplin Dressur, er sitzt in zahlreichen Saunen, spricht mit einem Lakritz-Hersteller und reist zum Endlauf der finnischen Meisterschaften im Watercross.

Gieseking fragt Finnen, warum sie glücklich sind, aber auch, ob Deutsche in Finnland und Finnen in Deutschland glücklich sein können. Und er fragt sich selbst, warum er in Finnland immer wieder so glücklich ist. Das Programm zum Buch!

Bernd Gieseking gilt als der König der Jahresrückblicke und tourt seit nunmehr 30 Jahren erfolgreich mit seinen Bühnenprogrammen durch die gesamte Republik. Seiner treuen Leserschaft ist er wahlweise als taz-Kolumnist oder als Finnlandkenner oder beides bekannt. Seine Bücher »Finne Dich Selbst!« und »Das kuriose Finnland-Buch« sowie »Gefühlte Dreißig - ein Hoffnungskabarett für Männer um die Fünfzig« und »Früher hab' ich nur mein Motorrad gepflegt« wurden Bestseller, und die gleichnamigen Kabarettprogramme sind wahre Meisterwerke der Komik. Auch in seinem neuesten Erzählband »Finne dein Glück« besticht der begnadete Geschichtenerzähler mit seinem typischen Humor.

Eintritt frei! Anmeldung per Mail an moritz@roxy.ulm.de. 

Ulmer Leseorte - Alle Termine

29.08.2021 | 20:00 H | Me and all Hotel
Bernd Gieseking

31.08.2021 | 20:00 H | Jugendfarm Ulm 
Ernst Paul Dörfler

15.09. | 20:00 H | Club Action
Fabian Sommavilla

16.09. | 20:00 H | Cabaret Eden
Katapult-Verlag

21.09.2021 | 20:00 H | Donaustadion Ulm
Bernd Beyer

24.09.2021 | 20:00 H | Kinderladen Ulm
Sarah Straub

26.09.2021 | 16:00 H | Heyoka Zaubergarten
Patrick Salmen

***

Die Veranstaltungsreihe "Ulmer Leseorte" wird im Programm Kultursommer 2021 durch die Beauftrage der Bundesregierung für Kultur und Medien (BKM) mit Mitteln aus NEUSTART KULTUR gefördert.

Der Ulmer Kultursommer ist eine Initiative der Kulturabteilung der Stadt Ulm in Zusammenarbeit mit der Ulmer Kulturszene. Die einzelnen Programmpunkte werden von Gleis 44, ROXY Ulm oder HfG Archiv veranstaltet. Alle Beteiligten bedanken sich bei den Stadtwerke Ulm / Neu-Ulm und bei der Kulturstiftung des Bundes für die freundliche Unterstützung.

***
https://www.roxy.ulm.de/programm/programm.php?m=8&j=2021&vid=3820