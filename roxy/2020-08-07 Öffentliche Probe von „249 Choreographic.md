---
id: "349637076023519"
title: Öffentliche Probe von „249 Choreographic Failures“
start: 2020-08-07 17:00
end: 2020-08-07 18:00
address: ROXY.ulm
link: https://www.facebook.com/events/349637076023519/
teaser: Auf die internationale Ausschreibung für das ChoreoLab im ROXY gingen 45
  unterschiedliche Bewerbungen aus 24 Ländern ein. Die Tänzer und Choreografen
isCrawled: true
---
Auf die internationale Ausschreibung für das ChoreoLab im ROXY gingen 45 unterschiedliche Bewerbungen aus 24 Ländern ein. Die Tänzer und Choreografen reichten dabei einen 60 Sekunden langen Tanzfilm ein, der ein „choreografisches Scheitern“ dokumentierte. Eine Jury mit Karla Nieraad (Leiterin Stadthaus), Reiner Feistel (Ballettdirektor des Theaters Ulm), Tommi Brem (freier Künstler und Bühnenbildner) und Michael Mutschler (ROXY Ulm) wählte aus allen Einsendungen zunächst zehn Finalisten und schließlich drei Gewinner aus.

Die drei ausgewählten Choreografen sind Aina Lanas (Spanien), Pablo Girolami (Italien) und Axel Loubette (Frankreich). Sie werden in einer intensiven Probenfase im Juli und August im ROXY mit dem Ulmer Choreografen Pablo Sansalvador (Moving Rhizomes) zusammenarbeiten und jeweils ein 15-minütiges Werk für den „249 Choreographic Failures“-Tanzabend schaffen.   
Weitere Informationen unter https://www.roxy.ulm.de/programm/programm.php?m=8&j=2020&vid=3397 
Bei der öffentlichen Probe erhalten Sie bereits einen Vorgeschmack auf die Tanzaufführungen vom 21. bis 23. August. 

Zu den öffentlichen Proben ist der Eintritt frei.
Aufgrund der Corona-Regeln bitten wir um Anmeldung für die offenen Proben unter anmeldung@roxy.ulm.de

Veranstalter: Moving Rhizomes e.V.
------
Die Veranstaltung findet im Rahmen des Berblinger Jubiläums der Stadt Ulm statt. Weitere Informationen zum Jubiläum unter www.berblinger.ulm.de
------
We live in a society that constantly seeks and rewards success. Even though triumph is often the end result of an endless string of failures, we regard failure as a blemish. In “249 Choreographic Failures” international and local artists address the issue of failure, carefully identifying, describing, creating, sharing and performing failure in the form of dance.The public rehearsal will give you a forecast of the dance performances from August 21 to 23.
Please note: Due to the current situation, it‘s not clear whether and how the rehearsals can be made accessible to viewers. Please check the website in advance  for current information: http://movingrhizomes.com/choreolab2020/