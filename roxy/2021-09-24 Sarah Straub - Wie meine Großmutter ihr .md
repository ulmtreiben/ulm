---
id: "338104144681706"
title: Sarah Straub - Wie meine Großmutter ihr ICH verlor * Ulmer Leseorte
start: 2021-09-24 20:00
address: Kinderladen Ehinger Tor, Ehinger Straße 27, 89077 Ulm
link: https://www.facebook.com/events/338104144681706/
image: 236607933_10158743918782756_6528720170219018075_n.jpg
isCrawled: true
---
Ulmer Leseorte - Wo Geschichten lebendig werden

Auf der Dachterrasse des Kinderladens, Ehinger Straße 27, 89077 Ulm (Ehinger Tor).

Wenn aus Vergesslichkeit Demenz wird

Es war ein Schock. Als Sarah Straubs Großmutter dement wurde, veränderte das auch das Leben der damals 20-jährigen Enkelin. Neben ihrer bereits gestarteten Musikkarriere begann sie ein Studium der Psychologie, promovierte über Demenzerkrankungen und klärt heute über das Thema auf, das immer drängender wird. Allein in Deutschland leben etwa 1,6 Millionen Demenzkranke. Und jeden Tag kommen rund 900 Neuerkrankte dazu.

Sarah Straub zeigt mit ihrem ersten Buch „Wie meine Großmutter ihr Ich verlor“ in vielen Beispielen, was es bedeutet, wenn aus Vergesslichkeit Demenz wird, welche Aufgaben, aber auch Hilfsmöglichkeiten mit dieser Diagnose verbunden sind. Sie schreibt, wie der Lebensalltag mit einem Demenz-Erkrankten geregelt und das Endstadium dieser Erkrankung würdevoll gestaltet werden kann: „Ich lernte, dass die Menschen oft monate- oder sogar jahrelange Odysseen von Arzt zu Arzt hinter sich haben, bis sie beim richtigen Spezialisten vorstellig werden. Ich lernte, dass unser Gesundheitssystem nicht dafür ausgelegt ist, Betroffenen in jedem Fall ein würdevolles Leben zu ermöglichen. Ich lernte, dass diese Erkrankung dramatische Folgen für ganze Familien hat.“ Einfühlsam und konkret beschreibt sie, wie der Lebensalltag mit Demenzerkrankten geregelt werden kann, wenn beispielsweise das Autofahren zur Gefahr wird, wenn die Körperhygiene nachlässt oder sich Stürze häufen.

Sarah Straub gewährt auch einen Blick hinter die Fassaden der Kliniken und erklärt, warum es für die Forschung im Moment noch so schwierig ist, ein Heilmittel zu finden. Und so ist „Wie meine Großmutter ihr Ich verlor“ auch eine feinfühlig verfasste Orientierungshilfe, die hilft, den Verlauf, aber auch das Endstadium dieser Erkrankung für Patienten wie Angehörige würdevoll zu gestalten.

„Dieses großartige Buch wird dazu beitragen, die Herzen der Menschen zu öffnen für eine Krankheit, die so unermesslich viel Leid mit sich bringen kann.“ Liedermacher Konstantin Wecker

***

Sarah Straub, geboren 1986, ist promovierte Diplom-Psychologin und arbeitet in der Forschungsabteilung des Universitätsklinikums Ulm. Sie hält für unterschiedliche Organisationen regelmäßig Vorträge zum Thema »Frontotemporale Demenz«. Daneben ist sie eine erfolgreiche Musikerin. Sie veröffentlichte bis jetzt drei Alben, das letzte in Zusammenarbeit mit dem deutschen Liedermacher Konstantin Wecker.

***

Ulmer Leseorte - Alle Termine

29.08.2021 | 20:00 H | Me and all Hotel
Bernd Gieseking

31.08.2021 | 20:00 H | Jugendfarm Ulm 
Ernst Paul Dörfler

15.09. | 20:00 H | Club Action
Fabian Sommavilla

16.09. | 20:00 H | Cabaret Eden
Katapult-Verlag

21.09.2021 | 20:00 H | Donaustadion Ulm
Bernd Beyer

24.09.2021 | 20:00 H | Kinderladen Ulm
Sarah Straub

26.09.2021 | 16:00 H | Heyoka Zaubergarten
Patrick Salmen

***

Die Veranstaltungsreihe "Ulmer Leseorte" wird im Programm Kultursommer 2021 durch die Beauftrage der Bundesregierung für Kultur und Medien (BKM) mit Mitteln aus NEUSTART KULTUR gefördert.

Der Ulmer Kultursommer ist eine Initiative der Kulturabteilung der Stadt Ulm in Zusammenarbeit mit der Ulmer Kulturszene. Die einzelnen Programmpunkte werden von Gleis 44, Roxy Ulm oder HfG Archiv veranstaltet. Alle Beteiligten bedanken sich bei den Stadtwerke Ulm / Neu-Ulm und bei der Kulturstiftung des Bundes für die freundliche Unterstützung.

www.sarah-straub.de/