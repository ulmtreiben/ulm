---
id: "537468180817553"
title: "Bernd Beyer - 71/72: Die Saison der Träumer * Ulmer Leseorte"
start: 2021-09-21 20:00
locationName: Donaustadion
address: Stadionstraße 17, 89073 Ulm
link: https://www.facebook.com/events/537468180817553/
image: 235512521_10158733176947756_6321528744191653526_n.jpg
isCrawled: true
---
Ulmer Leseorte - Wo Geschichten lebendig werden
Im Donaustadion Ulm

Bernd-M. Beyer, Jahrgang 1950, studierte Volkswirtschaft und Politik und arbeitete zunächst als Tageszeitungsredakteur, dann viele Jahre als Programmleiter im Verlag Die Werkstatt mit dem Schwerpunkt Fußballliteratur. Seit seinem altersbedingten Ausscheiden ist er vor allem als Autor im Bereich Fußballgeschichte tätig.

Veröffentlichungen (Auswahl):
- Der Mann, der den Fußball nach Deutschland brachte. Das Leben des Walther Bensemann (2014)
- Das Goldene Buch der Fußball-Weltmeisterschaft (2014; als Herausgeber mit Dietrich Schulze-Marmeling)
- Helmut Schön. Eine Biografie (2017; ausgezeichnet als Fußballbuch des Jahres)

Über das Buch „1971/72 – Die Saison der Träumer“:
1971/72: Der Bundesligaskandal erschüttert das Land, die Nationalelf um Beckenbauer und Netzer spielt Zauberfußball und wird Europameister, Willy Brandt will mehr Demokratie wagen, die RAF hält Deutschland in Atem, die Band „Ton Steine Scherben“ liefert den Sound für Aufbruch und Protest. Dieses Buch schildert eine einzigartige Fußballsaison und zeichnet zugleich ein „großartiges Porträt einer aufmüpfigen Gesellschaft“ (FAZ). Der stille Fußballstar Stan Libuda und der rebellische Rockpoet Rio Reiser sind die Träumer, die die Leser*innen durch ein wildes Jahr begleiten.

Ulmer Leseorte - Alle Termine

29.08.2021 | 20:00 H | Me and all Hotel
Bernd Gieseking

31.08.2021 | 20:00 H | Jugendfarm Ulm 
Ernst Paul Dörfler

15.09. | 20:00 H | Club Action
Florian Sommavilla

16.09. | 20:00 H | Cabaret Eden
Katapult-Verlag

21.09.2021 | 20:00 H | Donaustadion Ulm
Bernd Beyer

24.09.2021 | 20:00 H | Kinderladen Ulm
Sarah Straub

26.09.2021 | 16:00 H | Heyoka Zaubergarten
Patrick Salmen

***

Die Veranstaltungsreihe "Ulmer Leseorte" wird im Programm Kultursommer 2021 durch die Beauftrage der Bundesregierung für Kultur und Medien (BKM) mit Mitteln aus NEUSTART KULTUR gefördert.

Der Ulmer Kultursommer ist eine Initiative der Kulturabteilung der Stadt Ulm in Zusammenarbeit mit der Ulmer Kulturszene. Die einzelnen Programmpunkte werden von Gleis 44, ROXY Ulm oder HfG Archiv veranstaltet. Alle Beteiligten bedanken sich bei den Stadtwerke Ulm / Neu-Ulm und bei der Kulturstiftung des Bundes für die freundliche Unterstützung.

www.autor-bernd-beyer.de/privates