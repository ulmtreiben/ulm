---
id: "884782445780552"
title: DJ Klaus Fumy * ROXY Soundgarten
start: 2021-08-18 18:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/884782445780552/
image: 235392402_10158730878092756_5179643720511980515_n.jpg
isCrawled: true
---
LIVEMUSIK UNTER FREIEM HIMMEL
EINTRITT FREI! 💛

Wir freuen uns, euch in unserem Sound Garten begrüßen zu dürfen. Neben dem regulären Biergartenbetrieb versüßen euch unterschiedliche DJs die Ohren.

DJ Klaus beschallt euch mit Ethnogrooves, Downbeat, Afrohouse und Unerwartetem, damit trotz Tanzverbot, die Füße wippen, der Popo wackelt und der Kopf frei wird.

Eintritt frei, Spende für die musikalische Unterhaltung wird gern entgegen genommen.

Öffnungszeiten:
Mittwoch - Samstag: Ab 17:00 H
Sonntag: Ab 15:00 H