---
id: "336606458194194"
title: Dota * ROXY Soundgarten
start: 2021-08-01 20:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/336606458194194/
image: 223110053_10158688104317756_4188158065113596600_n.jpg
isCrawled: true
---
Das Konzert von Dota wird wegen des Wetters in die Werkhalle verlegt  🌧

Bitte beachte: 
- Das Konzert kostet 10,— € Eintritt (Abendkasse)
- Einlass ab 19:00 H übers Foyer 
- Ihr braucht derzeit keinen Test-, Impf- oder Genesenennachweis
- Im Haus herrscht Maskenpflicht!
- Wir registrieren eure Kontaktdaten per luca-App oder Kontaktdatenformular
- Das Sound Garten Areal bleibt geschlossen; es gibt kein Essen

***

Für den „Deutschlandfunk“ ist sie die deutsche Joni Mitchell, für den „Tagesspiegel“ beweist ihre Musik, dass „Klang auch Inhalt verträgt“. Dota singt über all das, was sie umtreibt, beobachtet und hinterfragt. Die deprimierende Entwicklung der Weltpolitik, eine verblichene Liebe, Sexismus, die  Leistungsgesellschaft – jedoch immer wieder durchbrochen von einem ironischem Augenzwinkern und lebensfrohen Capriolen.  Als Band haben DOTA über die letzten Jahre ihren ganz eigenen Sound zwischen Chanson und Tanzbarkeit entwickelt, und ihre Songschreiberin überzeugt mit Authentizität und Natürlichkeit. Mit anderen Worten: DOTA fällt ins Herz wie in ein verlassenes Haus. Und füllt es mit Geschichten, mit Kontrapunkten, mit Witz, mit sanften, schlauen, aber auch schonungslosen Texten. Die „ZEIT“ schreibt, DOTAs Musik sei „mal wie Trampolinspringen an einem taufrischen Morgen, mal wie Hängemattengammeln an einem Frühlingstag, mal wie Parolenpinseln in einer Mondscheinnacht“.

Im April 2020 ist das neue Album “Kaléko” erschienen, mit Vertonungen von Gedichten der Lyrikerin Mascha Kaléko, die einen Teil des aktuellen Programms bilden. Aber auch ältere Stücke und Songs vom neuesten Album “Wir rufen Dich, Galktika”, das am 28.05.21 erscheint, werden zu hören sein.

Besetzung:

Dota Kehr: Gesang, Gitarre
Jan Rohrbach: E-Gitarre