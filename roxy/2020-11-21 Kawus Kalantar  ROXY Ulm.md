---
id: "4746683178682640"
title: Kawus Kalantar * ROXY Ulm - verschoben auf 05.11.2021!
start: 2021-11-05 20:00
end: 2021-11-05 23:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/4746683178682640/
image: 119085194_10157895369707756_1689305037221305062_n.jpg
isCrawled: true
---
Die Veranstaltung wurde vom 21.11.2020 und 25.04.2021 auf den 05.11.2021 verschoben. Tickets behalten ihre Gültigkeit oder können an den jeweiligen Vorverkaufsstellen zurückgegeben werden.
***

Lang lebe Kawus Kalantar

Kawus Kalantar ist ein Glücksfall für originelle Stand Up Comedy. Direkt, ehrlich und absolut witzig - Frischer Stand Up nach US-Vorbild. Straight aus der Neuen Vahr Süd bis nach Berlin und jetzt bundesweit: Erlebt Einhundertprozent (100%) KALANTAR bei seinem ersten Soloprogramm, damit ihr später mal sagen könnt „Ich hab den schon gesehen, da hat er noch sinnlos seinen Programmtitel geändert.“ Eigene Unsicherheiten, Überwachungskameras, Elyas M’Barek oder andere Symptome einer kranken Welt – Kawus hat keine Lösungen, aber am Ende des Abends euer Geld (technisch gesehen schon am Nachmittag). Setzt also ein Zeichen für Toleranz und kauft Tickets für die Show dieses prima Ausländers. Lang lebe Kawus Kalantar.


***

Die Veranstaltung findet wie geplant statt. Bitte beachtet: Unsere Bestuhlung ist mit ausreichend Abständen aufgebaut. Sucht euch entsprechend eurer Gruppengröße geeignete Sitzgruppen, ggf. muss sich eure Gruppe für die Zeit der Vorstellung trennen. Stühle dürfen nicht verschoben werden.
Tragt eure eigene Mund-Nasen-Bedeckung, die ihr auf eurem Sitzplatz abnehmen könnt. Wir werden eure Kontaktdaten aufnehmen (Telefonnummer oder Adresse), für vier Wochen verwahren und nur im Falle eines Coronafalls an das Gesundheitsamt aushändigen.
Danke für Geduld und Rücksicht auf andere Gäste.