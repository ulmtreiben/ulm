---
id: "1125178714619033"
title: Wunschkonzert mit Suchtpotenzial
start: 2021-02-12 20:00
link: https://www.facebook.com/events/1125178714619033/
image: 145726772_263444675146220_2237997924650764986_n.jpg
teaser: "Das 9. Wunschkonzert live aus dem Roxy Ulm!  Motto: Lovesongs vs. Partyhits!!
  🥳  Virtueller Eintritt via Paypal an suchtpotenzial.music@gmail.com"
isCrawled: true
---
Das 9. Wunschkonzert live aus dem Roxy Ulm! 
Motto: Lovesongs vs. Partyhits!! 🥳

Virtueller Eintritt via Paypal an suchtpotenzial.music@gmail.com