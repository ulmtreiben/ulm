---
id: "308084483670911"
title: "Summer in the city: El Flecha Negra im ROXY Sound Garten"
start: 2020-08-07 20:00
end: 2020-08-07 23:00
address: ROXY.ulm
link: https://www.facebook.com/events/308084483670911/
image: 107354124_10157712895507756_5458985384921887632_o.jpg
teaser: Fünf Länder, zwei Kontinente, eine Band. Mit grenzenloser Energie und
  mitreißender Magie erfüllen El Flecha Negra den Raum. Sind die Chicos aus
  Chile,
isCrawled: true
---
Fünf Länder, zwei Kontinente, eine Band. Mit grenzenloser Energie und mitreißender Magie erfüllen El Flecha Negra den Raum. Sind die Chicos aus Chile, Peru, Spanien, Kolumbien und Deutschland erstmal auf der Bühne, gibt’s kein Halten mehr. Vamos, lasst uns feiern. El Flecha Negra (Der Schwarze Pfeil) ist ein Zusammenfließen aus vielen Quellen: Die Musiker verbinden traditionellen Cumbia und Reggae mit modernem Mestizo und peruanischen Chicha Sounds. Wild und ausgelassen - die ungewöhnliche Mischung geht direkt ins Herz.
Mit ihrem Album “Tropikal Passport” liefern die lässigen Latinos ein lebensfrohes und zugleich tiefgreifendes Statement: Südamerikanische Roots treffen auf karibische Leichtigkeit, rockige Gitarren auf dröhnende Trompeten, spanische Stimmen auf feurige Botschaften.

Eintritt frei.
Der Biergarten ist ab 15:00 H geöffnet.

***
Konzerte in der Reihe "Summer in the city" sind eine Kooperation von Kunstwerk e.V. und ROXY gGmbH.

Wir danken für die Unterstützung durch das Ministerium für Wissenschaft, Forschung und Kunst Baden-Württemberg.