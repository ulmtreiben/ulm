---
id: "310969896629720"
title: Fuckup Night Ulm
start: 2020-09-29 20:00
end: 2020-09-29 21:30
address: ROXY.ulm
link: https://www.facebook.com/events/310969896629720/
teaser: Die Absicht der Fuckup Night ist es, zu zeigen, dass ein Fehlschlag wie
  Berblingers Sturz in die Donau, nicht zwangsläufig das Ende bedeuten muss, son
isCrawled: true
---
Die Absicht der Fuckup Night ist es, zu zeigen, dass ein Fehlschlag wie Berblingers Sturz in die Donau, nicht zwangsläufig das Ende bedeuten muss, sondern auch die Chance für einen Neuanfang bieten kann. Nicht unsere Misserfolge, sondern der Umgang mit ihnen sowie der Mut und Wille zur Veränderung der Situation sind entscheidend. Hier beichten mehrere Speaker ihre Fehler und erklären, wie man aus Zitronen Limonade macht. Die Geschichten der Fuckup Night sollen inspirieren, ermutigen und zu einem entspannten und positiven Umgang mit dem allgegenwärtigen Scheitern verhelfen.
Die Veranstaltung wurde vom 28. Mai auf den 29. September 2020 verschoben. Tickets behalten ihre Gültigkeit. Eintritt: 6,50 Euro pro Person

Caroline Schwarz
Die aus Ulm stammende Caroline Schwarz arbeitet heute als Coach in Berlin und berät und trainiert Unternehmer, Führungskräfte, kreative Menschen und Teams. Ihre Karriere begann sie als Schauspielerin, wurde dann Vollzeitmutter, baute die Marketingabteilung der Schmuckfirma ihrer Familie „Ehinger Schwarz“ auf, übernahm die Vertriebsmannschaft der Firma, später dann das Design und schließlich die Gesamtleitung in fünfter Generation des 150 Mitarbeiter starken Familienunternehmens. Zwei Jahre später musste das Unternehmen an Investoren verkauft werden. Carolin Schwarz beschreibt diese Zeit so: „Ich ging durch eine riesige Krise, kämpfte, stürzte, lies los und kam zurück auf meine Füße.“ In Ulm wird sie ihr Geschichte zum ersten Mal öffentlich im Rahmen der Fuckup Night Ulm erzählen.

Robert Radloff
Robert Radloff ist im Bereich Strategie & Veränderung bei Airbus tätig, sieht sich als Aktivist für eine positive Fehlerkultur und tritt regelmäßig als Speaker zu diesem Thema auf. In seiner Laufbahn hat er bereits unterschiedlichste berufliche Rollen durchlebt. Unter anderem als Soldat, Bänker, Informatiker und Abteilungsleiter. Wie seine Karriere als Athlet in Scherben zerbrach und sich aus dem einen Beruf der nächste entwickelte, erzählt Robert Radloff auf der Fuckup Night in Ulm.

Irmeli Gnilka
Irmeli Gnilka lebt in Weidenstädten, in der Nähe von Ulm, und ist als Coach und Beraterin im Einzelhandel für das Thema Direktvertrieb tätig. Mit einem Startkapital von 10.000 Euro gründete sie ihre erste Firma "samuli.de", stemmte ihren Hausbau und ihr Privatleben auf einmal. Rückblickend beschreibt sie ihre Gründungserfahrung so: „Ich würde sagen, dass ich als Unternehmerin alle klassischen Fehler gemacht habe, die man so ohne Erfahrung, ohne Mentoring und ohne Unterstützung machen kann. Der Unternehmergeist in mir ist nicht gestorben und eigentlich möchte ich genau dieselbe Firma erneut gründen.“

Dr. Dennis Schlippert
Der promovierte Experimentalphysiker Dr. Dennis Schlippert arbeitet als Forschungsgruppenleiter an der Leibniz Universität Hannover. Auch im Wissenschaftsbetrieb warten viele Stolperfallen. Eine Stipendienprüfung, die er verantwortete, verlief völlig anders als geplant. Wie es dann doch noch zu einem Happy End kam, erzählt Dennis Schlippert auf der Fuckup Night Ulm.

------Die Veranstaltung findet im Rahmen des Berblinger Jubiläums der Stadt Ulm statt. Weitere Informationen zum Jubiläum unter www.berblinger.ulm.de
------
It’s not our failures that are important, but how we deal with them, and our courage and will to make a change. Speakers at the Fuckup Night confess their mistakes and explain how they have learned to turn lemons into lemonade.
The stories are meant to inspire and encourage listeners, helping to create a positive approach toward handling the ubiquitous failure.
