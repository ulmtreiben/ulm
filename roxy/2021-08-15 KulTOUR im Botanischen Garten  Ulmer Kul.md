---
id: "198909962165096"
title: KulTOUR im Botanischen Garten * Ulmer Kultursommer
start: 2021-08-15 11:00
end: 2021-08-15 14:30
locationName: Botanischer Garten Ulm
address: Ulm
link: https://www.facebook.com/events/198909962165096/
image: 236929930_10158720805177756_2571358173606441285_n.jpg
isCrawled: true
---
Am 15. Juli geht es auf KulTOUR durch den Botanischen Garten der Universität Ulm Von bunten Wiesenflächen, schattigen Wäldern, Feuchtbiotopen bis hin zu verschiedenen Themengärten gibt es im Botanischen Garten vieles zu sehen.

Im Rahmen des Kultursommers lassen sich an diesem Tag zusätzlich sechs verschiedene Stationen mit Unterhaltung aus den Bereichen Kabarett, Zauberei, Musik und Akrobatik im Botanischen Garten entdecken

Programm:
11.00 Bernd Kohlhepp | Katalyn Hühnerfeld | Jens Ohle
11.30 Markus Zink | Uli Boettcher | John Garner Trio
12.00 Bernd Kohlhepp | Katalyn Hühnerfeld | Jens Ohle
12.30 Markus Zink | Uli Boettcher | John Garner Trio
13.00 Bernd Kohlhepp | Katalyn Hühnerfeld | Jens Ohle
13.30 Markus Zink | Uli Boettcher | John Garner Trio
14.00 Bernd Kohlhepp | Katalyn Hühnerfeld | Jens Ohle
14.30 Markus Zink | Uli Boettcher | John Garner Trio

Der Kultursommer ist eine Initiative der Kulturabteilung der Stadt Ulm in Zusammenarbeit mit der Ulmer Kulturszene. Die einzelnen Programmpunkten werden von Gleis 44, Roxy Ulm oder HfG Archiv veranstaltet. Alle Beteiligten bedanken sich bei den Stadtwerke Ulm / Neu-Ulm und bei der Kulturstiftung des Bundes für die freundliche Unterstützung. Die Veranstaltungsreihe wird im Programm Kultursommer 2021 durch die Beauftrage der Bundesregierung für Kultur und Medien (BKM) mit Mitteln aus NEUSTART KULTUR gefördert.