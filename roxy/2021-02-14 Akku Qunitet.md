---
id: "386016809391968"
title: "SZENE SCHWEIZ: Akku Quintet * ROXY Ulm"
start: 2021-02-14 20:00
address: ROXY.ulm
link: https://www.facebook.com/events/386016809391968/
image: 141288922_5349127391771966_8977973221849701538_n.jpg
teaser: cineastic minimal jazz  Das Konzert wurde vom 11.11.2020 auf den 14.02.2021
  verschoben. Jetzt muss es nochmal verschoben werden, sorry  Karten behalte
isCrawled: true
---
cineastic minimal jazz

Das Konzert wurde vom 11.11.2020 auf den 14.02.2021 verschoben. Jetzt muss es nochmal verschoben werden, sorry

Karten behalten ihre Gültigkeit oder können an den jeweiligen Vorverkaufsstellen zurückgegeben werden.
***

Sphärisch – atmosphärisch. Komplex und federleicht rollt, gleitet, hüpft und tanzt ihre Musik durch die Takte, schwingt sich auf in luftige Höhen und gleitet nieder in erdige Sphären, wo sie mit rollenden Bässen und messerscharfen Beats Anlauf für den nächsten Wachtraum nimmt. 

Die Schweizer Band um den Schlagzeuger Manuel Pasquinelli vereint Elemente aus Jazz, Minimal Music und Rock/Pop zu einem eigenständigen Bandsound.

Auf der aktuellen Tour präsentiert das Quintett sein neues Album «Depart». Dieses vierte Studioalbum der Band ist auf dem Label 7dMedia von Trey Gunn (langjähriges Mitglied von King Crimson) erschienen.

Musik zum Eintauchen mit Live-Visuals.

Manuel Pasquinelli - Drums
Michael Gilsenan - Sax
Maja Nydegger - Piano
Markus Ischer - Guit
Andi Schnellmann - Bass
Jonas Fehr - Visuals

Dieses Konzert ist das zweite einer 6-teiligen Reihe, in der wichtige Bands der aktuellen Schweizer Jazzszene vorgestellt werden.

Ermäßigungen gibt es an der Abendkasse für Kunstwerk-Mitglieder/ Schüler/ Studierende/ Bufdis/ FSJ. Personen bis einschließlich 15 Jahre haben freien Eintritt und werden gebeten, sich an der Abendkasse zu melden.