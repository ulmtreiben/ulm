---
id: "845704272581545"
title: Olaf Schubert * ROXY Ulm * ausverkauft
start: 2021-03-03 20:00
end: 2021-03-03 23:00
address: ROXY.ulm
link: https://www.facebook.com/events/845704272581545/
image: 96680178_10157538681642756_7323061596371025920_o.jpg
teaser: Die Veranstaltung wurde vom 04.06.2020 auf den 03.03.2021 verschoben. Tickets
  behalten ihre Gültigkeit oder können an den jeweiligen VVK-Stellen zurüc
isCrawled: true
---
Die Veranstaltung wurde vom 04.06.2020 auf den 03.03.2021 verschoben. Tickets behalten ihre Gültigkeit oder können an den jeweiligen VVK-Stellen zurückgegeben werden.
***

Dass Olaf Schubert national wie international zu den ganz Großen gehört. Nun…: Das gilt als unumstößlich. Schließlich hat er nicht nur die Wende im Osten eingeleitet, sondern auch alle anderen Umwälzungen der Welt live im TV verfolgt. Doch jetzt, jetzt ist Schuberts Zeit wirklich gekommen: die Zeit der Rebellen! Rebell war Schubert freilich schon immer. Niemals schwamm er mit dem Strom! Aber auch nicht dagegen. Ein Schubert schwimmt neben dem Strom. Auf dem Trockenen, denn dort kann er laufen. Er ist eben vor allem ein sanfter Rebell. Und einer mit Augenmaß obendrein. Einer, der nicht vorsätzlich unter die Gürtellinie geht, sondern dort zu Hause ist. Schließlich gehören auch diese Körperregionen für einen aufgeklärten jungen Mann seines Alters mittlerweile zum Alltag. Einer, der zwar zur sofortigen Revolution aufruft - allerdings nicht vor 11.00 Uhr, sein Schönheitsschlaf ist wichtiger.

Wie kaum ein Zweiter versteht sich Olaf zudem darauf, die Sorgen und Nöte der Frauen ernst zu nehmen. Auf der Bühne gibt er eben immer alles. Versetzt Berge. Nur um damit Gräben zuzuschütten. Man könnte es auch einfacher sagen: Schubert macht alles platt! Indem er redet, singt und gelegentlich auch tanzt. Und so verwundert es kaum, dass die überwältigende Mehrheit seiner zahlreichen weiblichen Fans mittlerweile Frauen sind. Dennoch bleibt Olaf bescheiden: Während andere Künstler schier explodieren und Feuerwerk auf Feuerwerk abfackeln, begnügt sich Schubert damit, einfach so zu verpuffen. Sich mit Madonna oder Justin Biber zu vergleichen hält er deshalb noch für verfrüht. Er hat ja auch noch einiges zu tun: auf große "Zeit für Rebellen" Tournee zu gehen. Großherzig wie er ist, verkauft Olaf die Tickets an fast alle, denn ihn live zu erleben, ist Menschenrecht!