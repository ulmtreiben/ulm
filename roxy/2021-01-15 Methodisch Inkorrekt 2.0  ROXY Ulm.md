---
id: "1480877182050740"
title: Methodisch Inkorrekt 2.0 * ROXY Ulm
start: 2021-01-15 20:00
end: 2021-01-15 23:00
address: ROXY.ulm
link: https://www.facebook.com/events/1480877182050740/
image: 78539859_10156985370542756_8907111931035254784_o.jpg
teaser: Die „Rockstars der Wissenschaft“ – Tour 2021  Keine Panik?! Das sehen die
  beiden Physiker Dr. Nicolas Wöhrl und Dr. Reinhard Remfort, bekannt aus dem
isCrawled: true
---
Die „Rockstars der Wissenschaft“ – Tour 2021

Keine Panik?! Das sehen die beiden Physiker Dr. Nicolas Wöhrl und Dr. Reinhard Remfort, bekannt aus dem Kultpodcast „Methodisch inkorrekt!“, ganz anders. Überall lauern die Gefahren: Kometen, Naturgewalten, der menschgemachte Klimawandel und völlig irrationale Politiker. Wieso hört denn niemand mehr auf die Stimme der Wissenschaft?

Ende 2020 ist es wieder Zeit für „Science not Silence!“. Die Rockstars der Wissenschaft packen den Tourbus voll mit neuen Experimenten, um den Wissenschaftsskeptikern den Kampf anzusagen. Denn spätestens, wenn die zwei Physiker auf der Bühne die Fakten und Feuerexperimente auspacken, ist auch dem letztem im Saal klar: es ist Zeit für „Panik“!

Aus dem Netz auf die Bühne

Seit 2013 begeistern die Physiker mit ihrem ebenso informativen wie unterhaltsamen Podcast „Methodisch inkorrekt!“ eine stetig wachsende Fangemeinde. Über 80.000 Hörer verfolgen regelmäßig den lockeren Plausch, bei dem neben Neuigkeiten aus der Wissenschaft („Warum orientieren sich Hunde beim Gassi gehen am Magnetfeld der Erde?“) auch Experimente („Plasma in der Mikrowelle“, „Feuertornado“), neue Chinagadgets sowie Musiksongs (man glaubt gar nicht wie viele schlechte Image-Videos es von deutschen Universitäten gibt) präsentiert werden, die einen Bezug zur Wissenschaft haben.

Nachdem die beiden spontan entschieden, ihre 100. Podcastfolge als Live-Show auf die Bühne zu bringen, für die es binnen weniger Stunden keine Karten mehr gab, war die Idee geboren, den Ruhrpott zu verlassen und die Wissenschaft dahin zu bringen, wo sie gebraucht wird: Raus aus den Universitäten, rein in unsere Köpfe für die gute Abendunterhaltung und die nächsten Partygespräche!

***
Wer steckt hinter „Methodisch inkorrekt!“?

Nicolas Wöhrl ist Familienvater, begeisterter Kletterer, im Kopf Physiker, im Herzen Schalker und bildet den strukturierten Teil des Wissenschaftlerduos.

Reinhard Remfort dagegen ist Katzenpapa, Autor, Ex-WG-Bewohner, hat keine Ahnung von Fußball (EM 2016: „Sind wir die in den weißen Trikots?“) und ist die chaotische Hälfte von Minkorrekt.

Kennengelernt haben sich die beiden (Ruhr)Pottkinder in den Laboren der Universität Duisburg. Spätestens nach mehreren Jahren gemeinsamer universitärer Forschung, haarsträubenden Dienstreisen bis nach Indien und fast fünf Jahren Podcast, verbindet die beiden eine enge Freundschaft, die Außenstehende häufiger an die Streitereien eines alten Ehepaares erinnern. Gerade diese Mischung aus ernsthafter Forschung, Fachwissen, Humor und Ruhrpottschnauze macht aus den beiden sympathischen Wissenschaftlern das, was sie sind …methodisch inkorrekt!