---
id: "2645200782376385"
title: Jonas Greiner * ROXY Ulm
start: 2021-04-16 20:00
end: 2021-04-16 23:00
address: ROXY.ulm
link: https://www.facebook.com/events/2645200782376385/
image: 86972613_10157250575942756_2685227348894154752_o.jpg
teaser: In voller Länge  Jonas Greiner ist die Nachwuchshoffnung der ostdeutschen
  Comedy- und Kabarettszene. "Diesen Namen sollte man sich merken, denn der sy
isCrawled: true
---
In voller Länge

Jonas Greiner ist die Nachwuchshoffnung der ostdeutschen Comedy- und Kabarettszene. "Diesen Namen sollte man sich merken, denn der sympathische 20-jährige hat nicht nur körperlich das Potential, ein Großer in der Deutschen Kabarettszene zu werden.", schrieb der Münchner Merkur bereits 2017. Der mittlerweile 22-jährige Thüringer ist seit etwa 2 Jahren auf den Bühnen Deutschlands unterwegs und inzwischen weit über seine Heimat hinaus bekannt. So war er bereits 2018 beim NDR Comedy Contest zu Gast und im Frühjahr 2019 in der HumorZone-Gala auf ARD und MDR zu sehen, wo er von Olaf Schubert als Newcomer des Jahres mit dem "Güldenen August" ausgezeichnet wurde.

Greiner beschäftigt sich in seinem Solo-Debüt mit den Fragen, die einen jungen Mann in der heutigen Zeit umtreiben: "Was nützt mir dieses Abitur?", "Was soll bloß aus mir werden?", oder "Wer sind Sie und was machen Sie in meiner Wohnung?" Der mit 207 cm Körpergröße wohl größte Kabarettist Deutschlands nimmt seine Zuhörer mit auf eine Reise vom Hier und Jetzt und den Problemen unserer Zeit bis hin zur ganz großen Weltgeschichte. Dabei betrachtet er die Welt mit viel Ironie, manchmal spitz und manchmal frech. Jonas Greiner schafft es, Gesellschaftskritik und scharfsinnige Beobachtungen mit alltäglichen, lustigen Geschichten zu verknüpfen und liefert so eine erfrischende und einzigartige Kombination aus Inhalt und Humor - echtes Stand-Up-Kabarett sozusagen.

Am meisten im Visier des jungen Mannes ist immer er selbst. Egal ob Körpergröße, Schullaufbahn oder Berufswahl - Jonas Greiner nimmt es mit Humor. Getreu dem Motto: Das Leben ist zu schön um kurz zu sein.