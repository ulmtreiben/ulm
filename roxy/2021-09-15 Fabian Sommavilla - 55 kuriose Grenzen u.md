---
id: "1526039271071985"
title: Fabian Sommavilla - 55 kuriose Grenzen und 5 bescheuerte Nachbarn * Ulmer
  Leseorte
start: 2021-09-15 20:00
locationName: Club Action
address: Beim Alten Fritz 3, 89075 Ulm
link: https://www.facebook.com/events/1526039271071985/
image: 236498009_10158733173092756_4713087578319934694_n.jpg
isCrawled: true
---
Ulmer Leseorte - Wo Geschichten lebendig werden
Im Club Action

Grenzen sind nichts Natürliches, sondern von Menschen geschaffen und deshalb auch genauso menschlich: unfair, kurios, albern und manchmal auch unfassbar bescheuert. Auf Karten sind sie schnurgerade, in Wirklichkeit oft krumm und schief. Bei Grenzstreitigkeiten bleibt auch mal Land übrig. Während viele Grenzen für Tragödien und Kriege verantwortlich sind, wird an anderen das Leben gefeiert und der Grenzzaun zum Volleyballnetz umfunktioniert. Fabian Sommavilla kennt zahlreiche solcher Geschichten und versammelt sie in seinem Buch „55 kuriose Grenzen und 5 bescheuerte Nachbarn“, KATAPULT liefert die Karten und Grafiken dazu. 

Fabian Sommavilla wurde 1993 in eine 2.500-Seelen-Gemeinde in Tirol hineingeboren. Das lädt quasi zum Verreisen ein. So entwickelte er sein Interesse für Grenzen. Er studierte Politik in Innsbruck, Crisis & Security Management in Den Haag und Geopolitics, Territory & Security in London. Zwischendurch war er auch Praktikant bei KATAPULT und fast alle hatten Probleme mit seinem wunderschönen Dialekt. Seit 2018 ist er Redakteur bei der österreichischen Tageszeitung DER STANDARD.

***

29.08.2021 | 20:00 H | Me and all Hotel
Bernd Gieseking

31.08.2021 | 20:00 H | Jugendfarm Ulm 
Ernst Paul Dörfler

15.09. | 20:00 H | Club Action
Fabian Sommavilla

16.09. | 20:00 H | Cabaret Eden
Katapult-Verlag

21.09.2021 | 20:00 H | Donaustadion Ulm
Bernd Beyer

24.09.2021 | 20:00 H | Kinderladen Ulm
Sarah Straub

26.09.2021 | 16:00 H | Heyoka Zaubergarten
Patrick Salmen

www.twitter.com/fasommavilla?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor

