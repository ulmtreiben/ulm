---
id: "760515334410098"
title: Moritz Neumeier - Lustig. | Ulm
start: 2021-03-24 20:00
end: 2021-03-24 22:30
address: ROXY.ulm
link: https://www.facebook.com/events/760515334410098/
image: 75246729_1900725156740304_1026448143807414272_o.jpg
teaser: Das Leben ist selten lustig. In vielen Momenten ist es nervig, anstrengend,
  niederschmetternd, traurig, zermürbend, blutdruckerhöhend, langweilig und
isCrawled: true
---
Das Leben ist selten lustig. In vielen Momenten ist es nervig, anstrengend, niederschmetternd, traurig, zermürbend, blutdruckerhöhend, langweilig und vor Allem normal. Und aus genau diesen Momenten macht Moritz Neumeier Stand-Up Comedy.

Moritz’ Humor hat amerikanisches Vorbild. Also weniger flache Gags, keine lustigen Hüte und kein Versteckspiel hinter einer einstudierten Rolle. Er sagt Sachen. Sie tun weh. Meistens überschreitet er die Grenze der politischen Korrektheit - immer macht es Spaß ihm dabei zuzuhören.

Es gibt so viele Themen, die jeder kennt und bei denen man nicht auf den Gedanken kommt zu lachen. Sei es die strukturelle Benachteiligung von Frauen, der neue Nationalismus, plötzlicher Kindstod oder der Zoo.

Er ist ehrlich, böse, zynisch und manchmal verletzend. Natürlich auf positive Art. Ein Mann, den man gesehen haben muss. Worte, die gehört werden wollen. Geh einfach dahin – Du hast doch sonst eh nichts Besseres zu tun.

***
Die Veranstaltung wurde vom 10.09.2020 auf den 24.03.2021 verschoben! Die Tickets behalten ihre Gültigkeit.