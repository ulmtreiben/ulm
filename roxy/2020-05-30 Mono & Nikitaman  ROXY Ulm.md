---
id: "556905721774224"
title: Mono & Nikitaman - Live - Ulm
start: 2021-05-23 20:00
end: 2021-05-23 23:00
address: ROXY.ulm
link: https://www.facebook.com/events/556905721774224/
image: 117842101_10157852374307984_994666853104497789_o.jpg
teaser: Mono & Nikitaman kommen für ein paar exklusive Konzerte aus dem Studio und
  unterbrechen ihre Arbeit am 7. Album, das 2021 fertig werden soll.   Sie ha
isCrawled: true
---
Mono & Nikitaman kommen für ein paar exklusive Konzerte aus dem Studio und unterbrechen ihre Arbeit am 7. Album, das 2021 fertig werden soll. 

Sie haben also die Zeit des Wahnsinns sinnvoll genutzt um uns bald mit vielen neuen Songs zu beglücken.

Natürlich werden sie auf ihrer kommenden Tour neben bekannten Liedern auch schon ein paar Stücke daraus präsentieren.
Zwischen Abriss und Upliftment, Lichtermeer und Eskallation wird wieder alles dabei sein.

Mit Faust hoch für Sozialkritik und Mucke mit Inhalt, zu der im Kollektiv eskalliert und im Off-Beat getanzt warden darf.

Ihr könnt euch jetzt schon ein Ticket dafür sichern. 