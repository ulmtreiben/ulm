---
id: "727201311442891"
title: Ray Riddler * ROXY Sound Garten
start: 2021-09-18 18:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/727201311442891/
image: 241725256_10158775228927756_3772221026757395665_n.jpg
isCrawled: true
---
Heute feiern wir mit der Kulturnacht auch den letzten Öffnungstag des ROXY Sound Gartens. Neben dem regulären Biergartenbetrieb versüßt euch DJ Ray Riddler den Abend.

Eintritt frei, Spenden für die musikalische Unterhaltung wird gern entgegen genommen. Geöffnet ist ab 14:00 H! 

Alle aktuellen Bestimmungen bzgl. Corona findet ihr auf unserer Homepage www.roxy.ulm.de
