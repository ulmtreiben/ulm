---
id: "193024359235155"
title: "ROXY Lockdown Bar #6"
start: 2021-02-20 20:30
link: https://www.facebook.com/events/193024359235155/
image: 151128392_10158308361877756_5289438000727238659_o.jpg
teaser: Wow - sechste Ausgabe schon!  Livemusik von Porter Percussion Duo Seit 2009
  steht das Porter Percussion Duo gemeinsam auf der Bühne und fasziniert sei
isCrawled: true
---
Wow - sechste Ausgabe schon!

Livemusik von Porter Percussion Duo
Seit 2009 steht das Porter Percussion Duo gemeinsam auf der Bühne und fasziniert sein Publikum durch Stilvielfalt, Virtuosität und musikalischem Ausdruck. Mit ihrem ganz eigenen Stil jonglieren die beiden Schwestern zwischen Barock, Romantik und Impressionismus und geben den – für ihr Instrumentarium fremden Epochen – einen ganz neuen und dennoch vertrauten Klang. Ihr Studium schlossen Jessica und Vanessa Porter – nach Auslandsaufenthalten an der Royal College of Music, London – mit Bestnote an der Hochschule für Musik und darstellende Kunst in Stuttgart ab.
(Dank an NEUSTART KULTUR und die Initiative Musik)


Talk mit Prof. Dr. med. Dr. phil. Manfred Spitzer
Professor Dr. med Dr. phil Manfred Spitzer ist fraglos der bekannteste Forscher der Universität Ulm. Er wurde 1958 geboren und studierte Medizin, Psychologie und Philosophie in Freiburg, wo er sich auch zum Psychiater weiterbildete. Von 1990 bis 1997 war er als Oberarzt an der Psychiatrischen Universitätsklinik Heidelberg tätig. Zwei Gastprofessuren an der Harvard-Universität und ein weiterer Forschungsaufenthalt am „Institut for Cognitive and Decision Sciences“ der Universität Oregon prägten seinen Forschungsschwerpunkt im Grenzbereich der kognitiven Neurowissenschaft und Psychiatrie. 1998 übernahm er als jüngster Psychiatrie-Professor Deutschlands die Leitung der „Universitätsklinik für Psychiatrie und Psychotherapie III“ am Ulmer Safranberg.
Einem großen Publikum bekannt wurde Prof. Spitzer jedoch durch seine Buchpublikationen wie „Digitale Demenz“ oder „Einsamkeit – die unerkannte Krankheit“, die zu Bestsellern avancierten und gesellschaftlich breit diskutiert wurden. Im letzten Jahr erschienen seine Bücher „Digitales Unbehagen. Risiken, Nebenwirkungen und Gefahren der Digitalisierung“ und – von besonderer Aktualität – „Pandemie – Was die Krise mit uns macht und was wir aus ihr machen“.


Das Cocktail-Tutorial
Im letzten Sommer hat ROXY-Barchef Michl im ROXY Soundgarten gemeinsam mit seinem Team wahrscheinlich mehrere tausend Drinks gemixt. Auch heute wird er mit euch zusammen wieder was Feines zusammenbrauen (nur für Volljährige). Heute ist Tequila-Abend:

//Mai Tai
6 cl brauner Rum (8+ Jahre)
2 cl Orangenlikör
2 cl Limettensaft
1 cl Mandelsirup
1cl Zucker- oder Kandissirup
frische Minze

//Old Cuban
4-5 cl brauner Rum
6 cl Champagner
2 cl Limettensaft
3 cl Zuckersirup
Angostura Bitter
Frische Minze

//Zubehör
Eis
Bostonshaker
Strainer (Sieb)


Noch Fragen? Dann bleibt zum Zoom-Thementalk und Aftershow Bargespräch
Im Anschluss an den Livestream könnt ihr euch mit den Beteiligten hier via ZOOM unterhalten: https://zoom.us/j/92877753199?pwd=VnYwZ1ViUXlxdFZuOGNER3ZKSHFzQT09


HIER KÖNNT IHR TEILNEHMEN:

FACEBOOK
https://www.facebook.com/roxy.kultur/posts/10158313335212756

YOUTUBE
https://youtu.be/-odR4xUtSdM

ROXY HOMEPAGE
https://www.roxy.ulm.de