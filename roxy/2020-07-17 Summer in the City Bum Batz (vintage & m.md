---
id: "3287773914580098"
title: "Summer in the City: Bum Batz (vintage & modern funk)"
start: 2020-07-17 20:00
end: 2020-07-17 23:00
address: ROXY.ulm
link: https://www.facebook.com/events/3287773914580098/
image: 107004180_10157712646417756_1336416061984625806_o.jpg
teaser: "Eintritt frei  Die vier Musiker der Band mit dem lustigen Namen Bum Batz wird
  euch die Funken um die Ohren hauen, hört nur: „E Berischd ausm Radio ver"
isCrawled: true
---
Eintritt frei

Die vier Musiker der Band mit dem lustigen Namen Bum Batz wird euch die Funken um die Ohren hauen, hört nur: „E Berischd ausm Radio verschreggt die Leid. Vier Vebrescher machen die Stadt unsischer! Woher se kummen wäß Kenner unn doch hat mer schun e paar Mol was geheert. Innerviews unn Zeuscheberischde in pälzer Manier ertönen im Rahmen vun gruuvischer Rock’n’Roll Mussigg. Dregg unn Raffinesse in g’sunder Mischung.
BumBatz machen kä Gefangene! Wer Angscht hat, bleibt besser dehäm oder bringt noch e paar Leid mit, damit er net so als is.’’
Alles klar? 

Eintritt frei
Der Biergarten öffnet bereits um 15:00 H

Wir danken für die Unterstützung durch das Ministerium für Wissenschaft, Forschung und Kunst Baden-Württemberg
