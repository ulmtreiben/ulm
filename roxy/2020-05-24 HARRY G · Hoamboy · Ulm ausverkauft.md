---
id: "283218659287567"
title: HARRY G · Hoamboy · Ulm · Nachholtermin vom 24.05.20
start: 2021-02-09 19:00
end: 2021-02-09 21:30
address: ROXY.ulm
link: https://www.facebook.com/events/283218659287567/
image: 59398471_2180086615415618_5758710799224799232_n.jpg
teaser: Harry G Live _______ Obacht! Aufgrund der aktuellen Situation wird die Show in
  Ulm im Roxy vom 24.05.20 auf den 09.02.2021 verschoben. Bereits erworbe
isCrawled: true
---
Harry G Live
_______
Obacht! Aufgrund der aktuellen Situation wird die Show in
Ulm im Roxy vom 24.05.20 auf den 09.02.2021
verschoben.
Bereits erworbene Karten behalten ihre Gültigkeit und müssen nicht umgetauscht werden.
Danke für euer Verständnis.
______

HOAMBOY – so der Titel des neuesten Programms von Harry G – ist mehr als nur ein Wortspiel, es ist eine exakte Beschreibung seiner Person. Auf der einen Seite ein Bayer mit Vorliebe für Tradition, auf der anderen Seite ein weltoffener und neugieriger Kosmopolit, der mit großer Leidenschaft und offenen Augen und Ohren sein jeweiliges Umfeld nach Themen durchsucht die es „wert" sind, auf der Bühne zerlegt zu werden. Und das tut er auf ganz eigene und unnachahmliche Weise. Ob in seiner typisch grantigen Art, süffisant, zynisch oder einfach ganz still betrachtend: Wenn sich Harry G auf der Bühne mit Trends, Zeitgeschehen und Menschen auseinandersetzt braucht sein Publikum starke Lachmuskeln. Ein Abend mit Harry G ist nicht zum Ausruhen, es ist ein Abend voller Energie und Intensität, der man sich als Zuschauer nicht entziehen kann. 
 
Aktuelle Tourdaten und Tickets auf: www.harry-g.com
