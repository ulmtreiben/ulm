---
id: "359807009018256"
title: DJ BenJammin Van der Strack * ROXY Sound Garten
start: 2021-09-12 18:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/359807009018256/
image: 240708778_10158760504672756_5850395628463492541_n.jpg
isCrawled: true
---
Wir freuen uns, euch in unserem Sound Garten begrüßen zu dürfen. Neben dem regulären Biergartenbetrieb versüßen euch unterschiedliche DJs die Ohren.

Packt die Tanzschuhe ein - Tanzen am Tisch ist erlaubt.
Freut euch auf Rock, Indie, Funk'n'Soul, Alternative, Reggae, Salsa Cubana, Oldie- und Hippie-Classics.

Eintritt frei, Spenden für die musikalische Unterhaltung wird gern entgegen genommen.

Öffnungszeiten:
Mittwoch - Samstag: ab 17:00 H
Sonntag: ab 15:00 H

Alle aktuellen Bestimmungen bzgl. Corona findet ihr auf unserer Homepage www.roxy.ulm.de
