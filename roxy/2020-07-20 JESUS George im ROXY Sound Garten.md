---
id: "271072324157081"
title: JESUS George im ROXY Sound Garten
start: 2020-07-20 18:00
end: 2020-07-20 22:00
address: ROXY.ulm
link: https://www.facebook.com/events/271072324157081/
image: 107463552_10157728150037756_4084313072286247256_o.jpg
teaser: Konzert mit Coronaspielregeln  Jesus George ist…   … wenn sich an einem Abend
  Lenny Kravitz Selig an Amy Winehouse lehnt – gestützt durch die Kings Of
isCrawled: true
---
Konzert mit Coronaspielregeln

Jesus George ist… 

… wenn sich an einem Abend Lenny Kravitz Selig an Amy Winehouse lehnt – gestützt durch die Kings Of Leon, gezogen von den Red Hot Chili Peppers, gedrückt von The Knack und abgeholt durch die Sportfreunde Stiller.

Vor zehn Jahren als Weihnachtsprojekt geboren, mit Musikern aus dem Raum Ulm, schnürt Jesus George, deren Name aus einem Zitat von Michael J. Fox aus „Zurück in die Zukunft" stammt, ein überraschendes Paket mit Rock und Roll von den 1960ern bis heute.  

Bei sechs Geschmäckern kommt ein Mix zustande, der  unverkrampft und mit Kultcharakter auf der  Bühne gespielt wird. Cover  –  „eigene Ideen und Kreativität einbringen“ lautet die Devise von Jesus George, die mit Klassikern wie „Come together", „Gimme Shelter“, den Ramones , Clash und den Pixies jede Menge Spaß haben ... 

 Wie? Gitarre - Gitarre – Bass - Schlagzeug - Gesang - Gesang 