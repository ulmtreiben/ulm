---
id: "305845111027240"
title: Georg auf Lieder * ROXY Soundgarten
start: 2021-07-10 20:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/305845111027240/
image: 190705142_10158535085552756_1964184964886217853_n.jpg
isCrawled: true
---
LIVEMUSIK UNTER FREIEM HIMMEL
EINTRITT FREI! 💛

Nostalgie hat Zukunft. Zumindest klingt es so, als habe Georg auf Lieder mit seinem dritten Studioalbum den Blick in den Rückspiegel vertont, mit dem Fuß auf dem Gas. Es ist eine melancholische Reise, denn der 32-Jährige traut sich zum ersten Mal, auch die Lebensabschnitte zu besingen, die zu wahr sind, um schön zu sein. Und zum ersten Mal verpackt er’s nicht in einen sympathischen Scherz über sich selbst. Kein Schutzschild, keine Bremse. So lernen wir in Sprachnachrichten seine Familie kennen, begreifen die Umstände der Kindheit des Bolivianers. Aufwachsen fern vom Vater, nah an der Mutter, das rastlose Herz, wenig Kohle, viele Träume. 

Platte ohne Namen. 

Kein Titel würde der Bandbreite an Persönlichem gerecht werden, so Georg auf Lieder, „deshalb heißt das Album wohl automatisch ’Georg auf Lieder’. Es ist für mich der schlüssigste Titel. Dass es einen Song für meine Mutter und einen für meinen leiblichen Vater auf ein Album geschafft haben, lässt es zur Hälfte fast schon wie ein Konzeptalbum wirken. Es ist eine Sammlung prägender Momente, die mich zu dem gemacht haben, der ich heute bin. Es ist gefühlt wie mein Debüt- Album.“ 

Sein erstes Album „Alexanderplatz“ kam 2014, nachdem er als Straßenmusiker entdeckt wurde. Zwei Jahre später lieferte er dann die Platte“ManoGrande”nach.MajorDeal,HeadlinerbeiFestivalsund im Vorprogramm von internationalen Größen wie Amy MacDonald, ImagineDragons,ReaGarvey,MIA.,Milow,350Wohnzimmerkonzerte – er hat Erfolge gefeiert als Singer-Songwriter und trotzdem ist für Georg auf Lieder mit seinem aktuellen Werk alles anders, alles neu. 

100 Prozent Ich. 