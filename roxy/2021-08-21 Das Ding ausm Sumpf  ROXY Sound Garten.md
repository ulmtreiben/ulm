---
id: "550475412636668"
title: Das Ding ausm Sumpf * ROXY Sound Garten
start: 2021-08-21 20:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/550475412636668/
image: 202998361_10158600135397756_1934276488624249143_n.jpg
isCrawled: true
---
LIVEMUSIK UNTER FREIEM HIMMEL
EINTRITT FREI! 💛

„Als würden Freundeskreis Tracks von Curse covern, als jammten Marteria und Käptn Peng an einer Strandbar, oder als sei dieses Ding ausm Sumpf der Gesellschaft vielleicht Teil jener Rap-Reformation, nach der dieses Land schon so lange lechzt“, so beginnt das Reeperbahn Festival seine Vorstellung von Das Ding ausm Sumpf. Aber wer ist dieses Ding ausm Sumpf? 

Stefan Mühlbauer hat erst Operngesang studiert, dann VWL, dort auch seinen Doktor gemacht, Preis ge- krönt, und anschließend den Aufsichtsratsvorsitz bei einem Maschinenbauunternehmen übernommen, al- les klar? Jetzt rappt er über die Sinnlosigkeit einer kapitalgedeckten Welt, den ganz normalen Wahnsinn zwischen neun und fünf, die Leere eines Lebens, das immer mehr will – und wie Mensch sich all dem er- folgreich entzieht. Des Sumpfdings Texte sind nämlich nicht nur Anklage, sondern auch Ansatz für Lösun- gen, Aufruf zum Aufbruch, dialektisch unterwandert von jeder Menge bissiger Ironie, fetttriefenden Beats und bittersüßen Gänsehautgrooves. 

„Im weitesten Sinne HipHop, wie man ihn selten findet: versponnen und klug getextet und im Sound dicht und doch leicht“ meinte die Süddeutsche Zeitung. Andere nennen es „HipHop für Leute die kein HipHop hören“. Die Puls-Redaktion des bayrischen Rundfunks schwärmt „intelligente Texte und fette Beats (...) als würden Marteria und Käptn Peng ein Kind bekommen“. 

www.dasdas.org/