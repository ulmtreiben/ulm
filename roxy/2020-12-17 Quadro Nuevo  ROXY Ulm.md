---
id: "878722622664946"
title: Quadro Nuevo * ROXY Ulm
start: 2020-12-17 20:00
address: ROXY.ulm
link: https://www.facebook.com/events/878722622664946/
image: 121587237_10157984239927756_2069015737565852912_o.jpg
teaser: Das Weihnachtskonzert  Erinnerungen an die eigene Kindheit. Draußen die
  Winterluft, drinnen der Kerzenduft. Bratäpfel schmoren im Ofen. Klänge der fre
isCrawled: true
---
Das Weihnachtskonzert

Erinnerungen an die eigene Kindheit. Draußen die Winterluft, drinnen der Kerzenduft. Bratäpfel schmoren im Ofen. Klänge der freudigen Erwartung. Quadro Nuevo spielt Weihnachtslieder. Ungewohnt und doch vertraut. Bekannte und selten gehörte Stücke werden von den vier Virtuosen charmant interpretiert. Einzigartig, filigran, gefühlvoll.
Mit Liedern wie Es ist ein’ Ros’ entsprungen, Maria durch ein’ Dornwald ging und Die dunkle Nacht ist nun dahin blickt Quadro Nuevo zurück in die Jahrhunderte und setzt des Menschen Suche nach Trost und Liebe musikalisch um.

Das akustisch-instrumentale Quartett spielt zur Vorweihnachtszeit eine sehr persönliche Auswahl an stimmungsvollen Melodien. Ein besonderes Konzert in der vierten Jahreszeit!

Alle Alben von Quadro Nuevo wurden mit dem Deutschen Jazz Award ausgezeichnet und kletterten in die Top Ten der Jazz- und Weltmusik-Charts. Darüber hinaus erhielt das Quartett zwei Mal den ECHO und eine Goldene Schallplatte.

Mulo Francel: Saxophone, Klarinetten, Mandoline
Dietmar Lowka: Kontrabass, Percussion
Andreas Hinterseher: Akkordeon, Bandoneon, Trompete
Chris Gall: Piano  