---
id: "624161981697331"
title: Heavysaurus ● Ulm ● Roxy
start: 2021-04-24 15:00
end: 2021-04-24 18:00
address: ROXY.ulm
link: https://www.facebook.com/events/624161981697331/
image: 87031862_874502709645333_4633620867379625984_o.jpg
teaser: ++VERSCHOBEN - NEUER TERMIN!++  Retter der Welt Tour 2020  Heavysaurus ist die
  weltweit erste Kinder-Rock-Band mit musikalischen Anleihen aus Hard Roc
isCrawled: true
---
++VERSCHOBEN - NEUER TERMIN!++

Retter der Welt Tour 2020

Heavysaurus ist die weltweit erste Kinder-Rock-Band mit musikalischen Anleihen aus Hard Rock und Heavy Metal, verpackt in eine spektakuläre Bühnenshow mit einer echten Live-Band – in Dinosaurierkostümen. Entstanden 2009 in Finnland, hat Heavysaurus schnell Kinder (und ihre Eltern) begeistert: Mit über einer Viertelmillion verkaufter Alben haben die rockenden Saurier bereits mehrere Platinauszeichnungen erhalten. Das Debüt stieg auf Platz 5 der nationalen Charts ein und wurde sogar für den Emma Award 2010 nominiert, den höchsten finnischen Musikpreis. Die Live-Band hat weit über 400 Konzerte gespielt in Finnland, Schweden, Dänemark und UK.
Dabei bringen Heavysaurus die kleinen Rocker immer wieder zum Tanzen, Singen und Headbangen. Denn Dinosaurier sind natürlich cool für alle 3- bis 10-Jährigen, sie sind stark, wild und auch ein kleines bisschen lustig. Und Krachmachen ist immer ein Spaß. Die Konzerte mit großer Show, tollen Kostümen und kindgerechten Texten lassen Kinderaugen leuchten, gleichzeitig freuen sich alle rockenden Eltern über echte Metal-Songs.

Heavysaurus in Deutschland
Nun kommt Heavysaurus über Sony Music nach Deutschland, Österreich und in die Schweiz: Ab Frühjahr 2018 erscheint das erste Album – sowohl physisch als auch zum Download und im Streaming. Die finnischen Songs des Originals wurden ins Deutsche übertragen, zudem gibt es zwei eigens für deutsche Kids komponierte Lieder. Die Texte stammen von Frank Ramond, der bereits für Udo Lindenberg, Roger Cicero und andere Größen geschrieben hat. Gesungen werden sie von Michael Voss, der seit mehreren Dekaden als Musiker, Produzent und Songwriter Erfolge feiert, etwa bei und für Mad Max, Wolfpakk und Michael Schenker’s Temple of Rock.
Das erste Konzert fand bereits im Sommer 2017 statt, mit einem Prime-Time-Auftritt beim Mounds-Festival in der österreichischen Tourismusregion Serfaus-Fiss-Ladis. Dazu haben sich erfahrene Musiker aus der deutschen Szene in rockende Dinosaurier verwandelt und haben eine authentische Metal-Show live auf die Bühne gebracht – mit viel Action, Spaß und Mitmachmöglichkeiten.

Wie alles begann…
Die Anfänge von Heavysaurus liegen in Finnland, wo der fünfjährige Sohn des Schlagzeugers Mirka Rantanen der Band Thunderstone begann, sich für harte Musik von Bands wie Kiss, Guns N’ Roses und Metallica zu interessieren. Um diesen Klängen einen kindgerechten Rahmen und passende Texte zu geben, startete Rantanen 2009 eine neue Band und erschuf die Legende der fünf urzeitlichen Riesen, die seit Millionen von Jahren unter der Erde verborgen lagen und mit Hilfe von Zauberhexen nun geschlüpft sind – als die Metal-Dinos „Mr. Heavysaurus“ (Gesang), „Riffi Raffi“ (Gitarre), „Komppi Momppi“ (Schlagzeug) und das Drachenmädchen „Milli Pilli“ (Keyboards).
Die Figuren und ihre Geschichte begeistern die Kids, die Songs mit Titeln wie „Dinosauri Yeah“ beschäftigen sich mit Themen, die kleine Rocker und Dino-Fans bewegen: Schatzsuche per Flaschenpost, riesige Kaugummiblasen und die aufmunternde Botschaft, dass jeder cool ist, genauso, wie er ist. Sie erzählen vom wilden Leben der Dinos und der Freude an lautem Rock.
Dieses Konzept verbindet Musik, Lernen, Spielen und Bewegung und erstreckt sich mittlerweile von Tonträgern, Merchandise und Mobile Games bis zu einem 90-minütigen Film – zum Sammeln, Spielen und vor allem Mitrocken.
Deshalb hört man wohl demnächst in vielen Kinderzimmern: „Mama, das ist Heavysaurus! Das muss so laut!“