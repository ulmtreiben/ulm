---
id: "390283381972650"
title: CATT | Ulm - Why, Why-Tour 2021
start: 2021-09-17 20:30
end: 2021-09-17 23:30
locationName: ROXY.ulm
link: https://www.facebook.com/events/390283381972650/
image: 140926360_1895237450651314_7043138322677643381_n.jpg
isCrawled: true
---
(Nachholtermin vom 20.02.2021. Bereits erworbene Tickets behalten ihre Gültigkeit.)


CATT | Why, Why-Tour 2021

Präsentiert von Golden Ticket, ListenRecords, taz und Bedroomdisco


17.09.2021

Ulm - ROXY.ulm (Cafebar)

Einlass: 20:00 Uhr • Beginn: 20:30 Uhr

Tickets: https://catt.reservix.de/p/reservix/event/1592596


CATTs Debut-Album "Why, Why“ (VÖ: 20.11.2020) entsteht umgeben von Natur. Im Gartenhaus der Roger Willemsen Stiftung am Rande Hamburgs richtet sie sich Anfang 2020 ein kleines Studio mit all ihren Instrumenten ein. In unmittelbarer Nähe zum Wald, dem Ort, wo sie sich seit ihrer Kindheit am wohlsten fühlt - experimentiert sie mit ihrer Stimme, Bläsern, Beats. Die Magie dieses Ortes ist bereits in den Singles “Again” und “Willow Tree” deutlich zu hören.

 Die Ausrichtung eines jeden Songs bestimmt dabei nach wie vor CATTs charakteristisches Klavierspiel, das schon die erste EP “Moon” (Listenrecords 2019) geprägt hat: Es schiebt an, tastet sich vor, schlägt Richtungen und Stimmungen ein und begleitet beim Hören wie ein verlässlicher Gefährte beim Wandern auf unbekanntem Terrain. CATT entführt mit ihrem schwebenden Wesen mal in weite Landschaften, mal lädt sie zum Tanz auf Hinterhöfen versteckter Seitengassen.

Auch wenn CATT bereits mit so einigen Wassern des Musikbetriebes gewaschen ist - ein Gefühl fast kindlichnaiver Freude an der Musik, am Song, versprüht die 25-Jährige noch immer. Das spürt man auch und vor allem bei ihren Konzerten, wo sie mal mit ausufernden Bläser-Loop- und Stimmen-Arrangements, mal fragil am Klavier in ihren Bann zieht. Ihre größte Gabe, eine intensive Verbindung zwischen Publikum und ihrer Musik schaffen zu können, macht ihre Live-Auftritte so besonders - sie überrascht, berührt und es bleibt etwas, das vor allem in dieser Zeit wichtiger nicht sein könnte: Hoffnung.

Order Album “Why, Why” (VÖ: 20.11.2020): https://bit.ly/CATT_WhyWhy
Spotify: http://bit.ly/catt_moon_spotify
Website: www.catt-music.com
