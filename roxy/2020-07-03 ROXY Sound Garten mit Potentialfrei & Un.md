---
id: "634217510503083"
title: ROXY Sound Garten mit Potentialfrei & Ungefährlich
start: 2020-07-03 17:00
end: 2020-07-04 00:00
link: https://www.facebook.com/events/634217510503083/
image: 105910912_3338999816119723_3154838457853272619_o.jpg
teaser: Wir freuen uns, euch in unserem Sound Garten begrüßen zu dürfen. Neben dem
  regulären Biergartenbetrieb versüßen euch unterschiedliche DJs die Ohren.
isCrawled: true
---
Wir freuen uns, euch in unserem Sound Garten begrüßen zu dürfen. Neben dem regulären Biergartenbetrieb versüßen euch unterschiedliche DJs die Ohren.

HEUTE (ab 19:00 H): Potentialfrei & Ungefährlich
Das Kollektiv Potentialfrei & Ungefährlich versorgt euch mit entspannter elektronischer Musik im unteren BPM Bereich.

Es legen auf für Euch:

🐿️ https://soundcloud.com/macobee
🐿️ https://soundcloud.com/paukoe

Eintritt frei, Spende für die musikalische Unterhaltung wird gern entgegen genommen.

***
Alle aktuellen Bestimmungen bzgl. Corona findet ihr auf unserer Homepage www.roxy.ulm.de