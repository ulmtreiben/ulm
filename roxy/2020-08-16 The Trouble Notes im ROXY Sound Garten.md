---
id: "720639355442672"
title: The Trouble Notes im ROXY Sound Garten
start: 2020-08-16 19:00
end: 2020-08-16 22:00
address: ROXY.ulm
link: https://www.facebook.com/events/720639355442672/
teaser: "Auch wenn viele lautstarke Kehlen aktuell gern das Gegenteil propagandieren:
  Es gibt immer mehr, das uns vereint, als das uns trennt. Um diese simple"
isCrawled: true
---
Auch wenn viele lautstarke Kehlen aktuell gern das Gegenteil propagandieren: Es gibt immer mehr, das uns vereint, als das uns trennt. Um diese simple Wahrheit geht es The Trouble Notes nicht nur auf der kommenden Tour, sondern auch seit ihrer Gründung vor über fünf Jahren. Mit ihrem pan-kulturellen Mix aus verschiedenen musikalischen Einflüssen begeistern Geiger Bennet Cerven, Gitarrist Florian Eisenschmidt und Perkussionist Oliver Maguire eine stetig wachsende Schar an Menschen auf der ganzen Welt. Egal ob als Festival-Headliner oder Straßenmusiker - The Trouble Notes spielen über 150 Shows pro Jahr und geben sich als abenteuerlustige Künstler, die weder musikalisch noch gesellschaftlich Grenzen kennen.

“Unity In Diversity” lautet das Motto der Tour, welche dem Publikum gleichermaßen Songs ihres 2017er Debütalbums “Lose Your Ties”, wie neue Stücke präsentiert, mit dem Ziel die Gemeinschaft zu stärken, genauer gesagt Europa. “Diese Tour ist ein Zeugnis über die Wichtigkeit des Kontinents,” erklärt Bennet Cerven. Die europäische Identität hat uns Kooperation und Innovation gebracht und jedem nationalistischem Egoismus sollte mit Entschlossenheit entgegengetreten werden. The Trouble Notes sind das musikalische Äquivalent zu diesem Kulturverständnis und laden das Publikum dazu ein, sich diesem Credo anzuschließen und zum “Troublemaker” zu werden. Zusammen ist das Leben halt einfach eine Spur besser, oder?

Der Biergarten öffnet bereits um 15:00 H.
Eintritt frei