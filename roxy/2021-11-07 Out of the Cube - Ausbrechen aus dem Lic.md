---
id: "403503194677583"
title: Out of the Cube - Ausbrechen aus dem Lichtkubus * ROXY Ulm
start: 2021-11-07 20:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/403503194677583/
image: 243434813_10158811497242756_657371616526923389_n.jpg
isCrawled: true
---
Nach einer erfolgreichen Erstaufführung von „In the Cube - Das Leben im Lichtkubus“ im September 2019 kehrt die Trilogie „Woman’s World“ (Welt der Frau) mit dem 2. Teil - „Out of the Light Cube - Hinaus aus dem Lichtkubus“ in das ROXY Labor 1/12 zurück.

Im Mittelpunkt steht die innere und äußere Befreiung einer Künstlerin (Artista) aus der Zeit des Barock. Ausgehend von den Werken der beiden bis heute viel zu wenig beachteten Komponistinnen Barbara Strozzi und Francesca Caccini wird die Geschichte einer Frau erzählt, die sich danach sehnt, mit ihren Kompositionen in das Licht der Öffentlichkeit zu treten. Ihr inneres Ich (Anima) wagt sich zunächst jedoch nicht hinaus aus dem ihm von der Gesellschaft errichteten Gefängnis – dem Lichtkubus. Der Weg zu mehr Eigenständigkeit führt über die Begegnung mit den starken, selbstbewussten Frauengestalten des Barock, mit den Heldinnen und Hosenrollen Händels und Vivaldis. Denn auf der Barockbühne wurde die Utopie einer Gleichheit von Mann und Frau, einer Konfrontation auf Augenhöhe gelebt, die im realen Alltag so nicht denkbar war. Die Bühne wird zum Ort einer seelischen und emotionalen Emanzipation, die es der Anima schließlich ermöglicht, aus dem Lichtkubus auszubrechen und sich mit ihrem äußeren Ich, der Artista, zu versöhnen und zu vereinen.

Gesang: Oxana Arkaeva und Cornelia Lanz
Cello: Mariana Vodita-Gluth
Klavier: Fay Neary
Regie und dramaturgisches Konzept: Birgit Kronshage
Gesamtkonzeption: Oxana Arkaeva