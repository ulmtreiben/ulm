---
id: "522620492179313"
title: Nico Bulla * ROXY Sound Garten
start: 2021-09-10 18:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/522620492179313/
image: 241559924_10158775213642756_8346651492102993834_n.jpg
isCrawled: true
---
Wir freuen uns, euch in unserem Sound Garten begrüßen zu dürfen. Neben dem regulären Biergartenbetrieb versüßen euch unterschiedliche DJs die Ohren.

Eintritt frei, Spenden für die musikalische Unterhaltung wird gern entgegen genommen.

Öffnungszeiten:
Mittwoch - Samstag: ab 17:00 H
Sonntag: ab 15:00 H

Alle aktuellen Bestimmungen bzgl. Corona findet ihr auf unserer Homepage www.roxy.ulm.de