---
id: "2914874141957268"
title: "Summer in the city: Harrycane Orchestra im ROXY Sound Garten"
start: 2020-08-02 19:00
end: 2020-08-02 22:00
address: ROXY.ulm
link: https://www.facebook.com/events/2914874141957268/
image: 107163408_10157712872912756_5789986174283416958_o.jpg
teaser: Wegen Regen findet die Veranstaltung indoor statt, wir werden die Halle schön
  gemütlich für euch herrichten, um euch einen herrlichen Abend mit einer
isCrawled: true
---
Wegen Regen findet die Veranstaltung indoor statt, wir werden die Halle schön gemütlich für euch herrichten, um euch einen herrlichen Abend mit einer klasse Band zu gewährleisten, und das bei freiem Eintritt.
Achtung: Es geht schon um 19 Uhr los!!!!

Die Liebe zur orientalischen Melodieführung, die Faszination für arabische Rhythmik sowie die Leidenschaft für Improvisation bilden die Basis der Kompositionen, die Schlagzeuger/Komponist Harry Alt speziell für dieses Ensemble geschrieben hat. Das klingt oft fast volksliedhaft, im nächsten Moment eher weltmusikalisch nach Arabic-Jazz. Mal bestimmt der türkische Gesang von Tarkan Yesil, mal die pure Improvisationsfreude aller beteiligten Musiker das Geschehen. Dabei stehen sich strenge Kompositionen und freie Gestaltung nicht gegenüber, sondern ergänzen sich hypnotisch-anmutig.
Harry Alt: Schlagzeug/Komposition
Kay Fischer: Saxophon/Klarinette/ Flöte
Tarkan Yesil: Gesang/Riq
Joe T. Aykut: Cümbüc
Giuseppe Puzzo: Bass
David Kremer: Klavier

Eintritt frei.
***
Konzerte in der Reihe "Summer in the city" sind eine Kooperation von Kunstwerk e.V. und ROXY gGmbH.
Wir danken für die Unterstützung durch das Ministerium für Wissenschaft, Forschung und Kunst Baden-Württemberg.