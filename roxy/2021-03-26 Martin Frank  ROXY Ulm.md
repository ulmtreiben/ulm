---
id: "545286386101053"
title: Martin Frank * ROXY Ulm
start: 2021-03-26 20:00
end: 2021-03-26 23:00
address: ROXY.ulm
link: https://www.facebook.com/events/545286386101053/
image: 87283093_10157250915807756_6395238893955842048_o.jpg
teaser: „Einer für alle – Alle für keinen“ fasst das momentane Weltbild ganz gut
  zusammen. Denn wie sonst nennt man eine Welt, in der ein Mädchen gegen den Kl
isCrawled: true
---
„Einer für alle – Alle für keinen“ fasst das momentane Weltbild ganz gut zusammen. Denn wie sonst nennt man eine Welt, in der ein Mädchen gegen den Klimawandel kämpft und dafür von erwachsenen Menschen angefeindet wird, deren  Klimaziele erreicht sind sobald sie sich einen Furz (bair. Schoaß, politisch korrekt Darmwind) verdrücken. Wie sonst nennt man eine Welt, in der sich jeder wegen der kleinsten Kleinigkeit diskriminiert fühlt. Sei es ein Mann aufgrund von Frauenparkplätzen oder ein Mädchen, das nicht im Knabenchor singen darf. Aber verständlich, Martin wollte früher auch immer Mitglied im Frauenbund werden, dann hat ihm seine Mutter die Krampfader gezeigt und gesagt: „Ohne die geht es nicht!“, dann war die Sache für ihn erledigt. In seinem dritten Soloprogramm spitzt Martin
Frank über die Baumkronen seines Bayerwaldes und sinniert gewohnt frech, hintersinnig und bitterböse über unser teils absurdes Leben auf dieser Erde. Die fahren wir sowieso bald mit voller Geschwindigkeit gegen die Wand, wenn wir uns nicht endlich wieder den wirklich wichtigen Themen widmen und dabei eines nicht vergessen: Die Liebe! Dabei braucht man weder Kitsch noch schmachtendes Sehnsüchteln oder gar Esofirlefanz zu fürchten. Schließlich stammt Martin Frank aus dem emotional eher zurückhaltenden Niederbayern. Da bleibt auch die Liebe bodenständig. Selbst wenn noch mehr Arien von der Bühne geschmettert werden – er kann halt nicht anders. Aber das wusste die Oma schon: „Ohne Liab, is na koana oid woan!“