---
id: "284034672768062"
title: "ROXY Live - Die Late Night Show #1"
start: 2020-05-16 21:00
end: 2020-05-16 22:30
link: https://www.facebook.com/events/284034672768062/
image: 96684346_10157538862217756_1839450491906424832_o.jpg
teaser: 'Livestream auf Youtube: https://youtu.be/se0if6bIxF4 oder direkt über
  www.roxy.ulm.de  Mit "ROXY Live - Die Late Night Show" geht ein neues
  Veranstalt'
isCrawled: true
---
Livestream auf Youtube: https://youtu.be/se0if6bIxF4
oder direkt über www.roxy.ulm.de

Mit "ROXY Live - Die Late Night Show" geht ein neues Veranstaltungsformat an den Start, das sich in jeder Ausgabe intensiv auf ein Schwerpunktthema stürzt. Die beiden Moderatoren Andreas Rebholz und Niklas Ehrentreich, beide bekannt von den Poetry Slam-Bühnen der Republik, verbinden dabei Information mit Entertainment, immer angetrieben von der eigenen Neugier auf die Sache. Zu Gast werden Gesprächspartner*innen aus Kultur, Wissenschaft, Politik und Wirtschaft sein. Die Debütausgabe befasst sich mit Kulturstreaming in Zeiten des Lockdown und wird am 16.5. um 21:00 live auf dem Youtube-Kanal des ROXY zu sehen sein. 

Niklas Ehrentreich steht unter seinem Künstlernamen "Nik Salsflausen" seit 2011 auf der Bühne. Seitdem erreichte er zweimal das Finale der deutschsprachigen Meisterschaften im Poetry Slam und gewann die baden-württembergische Meisterschaft 2014. Der gelernte Gymnasiallehrer lebt in Esslingen und arbeitet unter anderem als Moderator sowie als Trainer für öffentliche Vorträge und Präsentationen.

Andreas Rebholz ist seit 2013 auf Poetry Slams im gesamten deutschsprachigen Raum unterwegs. Seit dem Beginn seiner wissenschaftlichen Tätigkeit im Themenbereich „Nachhaltige Mobilität“ tritt er zudem auf Science Slams auf. Derzeit promoviert der gebürtige Oberschwabe am Insitut für Nachhaltige Unternehmensführung an der Universität Ulm.