---
id: "395623467802753"
title: Kurt Krömer * ROXY Ulm
start: 2021-06-03 20:00
end: 2021-06-03 22:30
address: ROXY.ulm
link: https://www.facebook.com/events/395623467802753/
image: 71757697_10156831933277756_8046257187627991040_o.jpg
teaser: Die Veranstaltung wurde vom 10.11.2020 auf den 03.06.2021 verschoben. Tickets
  behalten ihre Gültigkeit oder können an den jeweiligen VVK-Stellen zurüc
isCrawled: true
---
Die Veranstaltung wurde vom 10.11.2020 auf den 03.06.2021 verschoben. Tickets behalten ihre Gültigkeit oder können an den jeweiligen VVK-Stellen zurückgegeben werden.
***
Stresssituation

Wohnhaft in Neukölln, zu Hause auf der Bühne: Kurt Krömer ist ein schrulliger, gnadenloser
Kabarettist mit eigenwilligem Modebewusstsein und Berliner Schnauze: ein Punk im Körper eines Sparkassenangestellten.

Stresssituation ist das mittlerweile sechste Stand-up-Comedy Programm, mit dem Krömer seit Anfang 2018 überaus erfolgreich durch Deutschland reist. Eine garantierte Stresssituation für Tränensäcke, Lachmuskeln und ein tatsächlich ungewohnt provokantes Programm, mit dem Krömer selten ein gutes Haar an Politik, Gesellschaft und dem anwesenden Publikum lässt. Hier kommt keiner ungeschoren davon: weder Omas Couchtisch noch Krömer selbst.

http://www.kurtkroemer.de/