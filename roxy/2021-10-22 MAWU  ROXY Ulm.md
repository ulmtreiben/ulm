---
id: "341427060877346"
title: MAWU * ROXY Ulm
start: 2021-10-22 20:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/341427060877346/
image: 215470608_10158649487462756_39187733225491735_n.jpg
isCrawled: true
---
MAWU im ROXY Ulm
Beginn: 20:00 H
Einlass: 19:30 H

MAWU bringt die Weiblichkeit auf die Bühne, durch die Figur von sechs freien und ermächtigten Frauen und ihre Suche nach Identität im Kollektiv, in der Bewegung und in ihrem Ritual. MAWU reist vom Licht zur Dunkelheit, beginnend mit der untergehenden Sonne und allmählich in die Nacht, das Mysterium, den Konflikt und die Wiedergeburt im Morgengrauen. Die Macht dieser Göttin, wie sie im Matriarchat bekannt war, wird nicht als Bedrohung der Individualität aufgefasst, sondern als ein angeborenes Verständnis der Naturgesetze.

Sie ist die weibliche Quelle des Universums und symbolisiert Fruchtbarkeit, Überfluss und schöpferische Phantasie.