---
id: "197303472369939"
title: Schöne Scheiße - Piero Masztalerz´ Cartoons aus dem Lockdown * ROXY Ulm
start: 2021-08-05 12:00
address: ROXY.ulm
link: https://www.facebook.com/events/197303472369939/
image: 232687269_10158701121557756_3806579427260489129_n.jpg
isCrawled: true
---
DIGITALE KUNST IM LOCKDOWN

Piero Masztalerz ist ein vielfach prämierter Cartoonist. Im Lockdown hat er mit seinen aktuellen Cartoons fast das Internet zum Explodieren gebracht. Um den ROXY Soundgarten werden ein paar seiner Werke ausgestellt und er kommt auch persönlich vorbei. Er liest dabei nicht nur einfach seine Cartoons und Comics vor, sondern spricht mit seinen animierten Figuren auf der Leinwand! Ein total neues Konzept in der Comedy, sehr skurril und lustig.

***
Die Veranstaltungsreihe "Digitale Kunst im Lockdown" wird im Programm Kultursommer 2021 durch die Beauftrage der Bundesregierung für Kultur und Medien (BKM) mit Mitteln aus NEUSTART KULTUR gefördert.
Der Ulmer Kultursommer ist eine Initiative der Kulturabteilung der Stadt Ulm in Zusammenarbeit mit der Ulmer Kulturszene. Die einzelnen Programmpunkte werden von Gleis 44, Roxy Ulm oder HfG Archiv veranstaltet. Alle Beteiligten bedanken sich bei den Stadtwerke Ulm / Neu-Ulm und bei der Kulturstiftung des Bundes für die freundliche Unterstützung.