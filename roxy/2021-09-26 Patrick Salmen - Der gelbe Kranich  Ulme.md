---
id: "402631714540650"
title: Patrick Salmen - Der gelbe Kranich * Ulmer Leseorte
start: 2021-09-26 16:00
link: https://www.facebook.com/events/402631714540650/
image: 239236892_10158743916287756_4914426032235594258_n.jpg
isCrawled: true
---
Ulmer Leseorte - Wo Geschichten lebendig werden
Im Heyoka Zaubergarten

Als eines Morgens ein schier monströser, bis tief in die Wolkendecke ragender Kran in seinem Vorgarten steht, wird das Leben von Herrn Faber auf den Kopf gestellt. So ein Kran kann doch nicht einfach in seinem geliebten Blumenbeet abgestellt werden! Der verwitwete Rentner macht sich auf die Suche nach dem Besitzer des riesigen Ungetüms und lernt zahlreiche skurrile Menschen kennen: Den dubiosen Schuster ohne eigene Schuhe, die geheimnisvolle Bertha Blechbüchse, den Gelehrten Kubilay Kugelkopf oder die im Regen schmelzende Konditormeisterin Zusa Zuckerguss. Und sie alle erzählen ihm, dass sie zwar keinen Kran, dafür aber durchaus andere Dinge vermissen. Nach einigen Tagen beschließt Herr Faber, den stählernen Koloss zu besteigen und macht sich samt Kletterausrüstung und Bockwürstchen auf eine waghalsige Expedition. Er ahnt schon bald: Hüte und Regenschirme lassen sich mit ein wenig Geschick zurückbringen, doch mit Dingen wie Liebe oder Heimat verhält es sich schwieriger.

Patrick Salmen erzählt in seinem ersten Kinderbuch vom Vermissen und Wiederfinden, von den Widrigkeiten des Alters und der hohen Kunst des Mittagsschlafes.

***

Patrick Salmen (*1985 in Wuppertal), lebt mit seiner Familie im Ruhrgebiet. Nach dem Geschichts- und Germanistikstudium gewann er 2010 die deutschsprachigen Poetry-Slam-Meisterschaften und wirkt seitdem als Schriftsteller, Sprecher und Bühnenkünstler. Neben klassischen Bühnentexten hat er zahlreiche Lyrik- und satirische Kurzgeschichtenbände geschrieben. Seit vielen Jahren ist er regelmäßig auf Lesereise im gesamten deutschsprachigen Raum. „Der gelbe Kranich“ ist sein erstes Kinderbuch.

***

Ulmer Leseorte - Alle Termine

29.08.2021 | 20:00 H | Me and all Hotel
Bernd Gieseking

31.08.2021 | 20:00 H | Jugendfarm Ulm 
Ernst Paul Dörfler

15.09. | 20:00 H | Club Action
Fabian Sommavilla

16.09. | 20:00 H | Cabaret Eden
Katapult-Verlag

21.09.2021 | 20:00 H | Donaustadion Ulm
Bernd Beyer

24.09.2021 | 20:00 H | Kinderladen Ulm
Sarah Straub

26.09.2021 | 16:00 H | Heyoka Zaubergarten
Patrick Salmen
***
Die Veranstaltungsreihe "Ulmer Leseorte" wird im Programm Kultursommer 2021 durch die Beauftrage der Bundesregierung für Kultur und Medien (BKM) mit Mitteln aus NEUSTART KULTUR gefördert.

Der Ulmer Kultursommer ist eine Initiative der Kulturabteilung der Stadt Ulm in Zusammenarbeit mit der Ulmer Kulturszene. Die einzelnen Programmpunkte werden von Gleis 44, ROXY Ulm oder HfG Archiv veranstaltet. Alle Beteiligten bedanken sich bei den Stadtwerke Ulm / Neu-Ulm und bei der Kulturstiftung des Bundes für die freundliche Unterstützung.

www.patricksalmen.de/