---
id: "523196555705266"
title: Stereo Naked & Ballad of Crows * ROXY Soundgarten
start: 2021-07-04 19:30
locationName: ROXY.ulm
link: https://www.facebook.com/events/523196555705266/
image: 205998176_10158615708672756_2948845676497717774_n.jpg
isCrawled: true
---
LIVEMUSIK UNTER FREIEM HIMMEL
EINTRITT FREI! 💛

Stereo Naked, ein Kontrabass, ein Banjo, Harmoniegesang. Und dann die Rastlosigkeit und dann das Herz. Julia Zech und Pierce Black sind ständig unterwegs, was sie seit dem Erscheinen ihre Debuts "Roadkill Highway" an Auftritten gespielt haben, gleicht einem Marathon. Ihre Musik, eine Mischung aus Folk und Indie, bei der mädchenhafter Charme aus dunklen Zynismus prallen und jodelnde Oden an die Liebe auf nicht mehr weiter wissen.

Jeder Song ist eine kleine handgeformte Geschichte, die sich nie in Phrasen verliert, an der man klebt, für drei Minuten oder noch viel länger. Was Stereo Naked schaffen, ist ihre gemeinsame Leidenschaft, nämlich den Bluegrass aus seiner Nische zu holen und ihn mit gefühlsgenauem Songwriting unprätentiös, aber prägnant und berührend für ein breites Publikum zu verwerten. 

Was die Beiden da machen hat kein Kalkül, das hüpft aus der einen Brust raus und in die nächste rein. Ohne Filter. Eben genau wie die Beiden sind und klingen. 

Ballad of Crows bieten akustisch handgemachte Musik mit honig-süssem mehrstimmigen Gesang. Traditionelles Singer/Songwriting aus ihrer Heimat Schottland wechselt ins Moderne und begegnet Einflüssen aus dem Celtic-Folk und der Americana-Musik aus den USA. Es sind die gefühlvollen Balladen und schweißtreibenden Tunes, sowie virtuose Geige, Mandoline und begleitendes Gitarrenspiel, die das Repertoire des Trios prägen.

Mit ihrem Debütalbum hat die Band es unter die 15 besten Folk-Alben 2015 des Daily Telegraph (GB) geschafft. Auch der legendäre BBC Radio Moderator Mike Harding preist die Band.

„Fantastisches Album. Sie machen richtig tolle Musik!” sagte er in seiner Folk Sendung. Seit ihrer Gründung 2013 sind Ballad of Crows durch ganz Europa getourt. Dabei haben sie viele beachtliche Reviews gesammelt und sich an den verschiedensten Orten, an denen sie spielten, eine große Anhängerschaft aufgebaut. Ihre Live-Shows werden durch ihren charmanten, schottischen Humour und ‚the good craic‘ komplementiert.

Ballad of Crows kündigen für 2018 eine brandneue Besetzung und ein neues Album an. Der verbleibende schottische Kern der Band, Steve Crawford (Gitarre, Gesang) und Pete Coutts (Mandoline, Gesang), die bereits seit über 20 Jahren miteinander musizieren, bekommen Verstärkung vom Kölner Geigenspieler Paul Bremen, der einen großen Reichtum an Erfahrung mit Old-time-fiddle, sowie Jazz und klassischer Geige mit sich bringt.