---
id: "298222584892350"
title: "Summer in the city: Wally & Ami Warning im ROXY Sound Garten"
start: 2020-07-31 20:00
end: 2020-07-31 22:00
address: ROXY.ulm
link: https://www.facebook.com/events/298222584892350/
image: 106774641_10157703975132756_7491905465268886373_o.jpg
teaser: "groove and soul  Zwei Generationen treffen aufeinander: menschlich,
  musikalisch und emotional. Vater Wally ( Aruba/niederl. Antillen ) und Tochter
  Ami"
isCrawled: true
---
groove and soul

Zwei Generationen treffen aufeinander: menschlich, musikalisch und emotional. Vater Wally ( Aruba/niederl. Antillen ) und Tochter Ami Warning sind sich vertraut und harmonieren, natürlich und unverkrampft, eine perfekte Symbiose. Multiinstrumentalist und Sänger Wally („No Monkey“) strahlt Leichtigkeit aus, wenn er lässig zwischen verschiedenen Stilen, Sprachen und Klangkörpern hinund herspringt. Tochter Ami setzt Sanftheit, Erdung und Neugier dazu; vor allem aber ihren tiefen, rauen, ganz besonderen Gesang. Gemeinsam schwingen sich die beiden ein, setzen ihre Stimmen in Szene und experimentieren virtuos mit Rhythmen und Instrumenten. Schon nach wenigen Takten spürt man ein blindes Verstehen und eine tiefe gemeinsame musikalische Basis.

"Und beider Songs berühren die Zuhörer gleichermaßen – in ihrer eingängigen Mischung aus Soul, Pop, Reggae und einem Hauch Gospel, mit ihren gefühlvollen, aufrichtigen, manchmal aufrüttelnden Texten und vor allem in der Authentizität, die Vater wie Tochter versprühen." ( Weilheimer Tagblatt )

Eintritt frei.
Der Biergarten öffnet bereits um 15:00 H.

***
Konzerte in der Reihe "Summer in the city" sind eine Kooperation von Kunstwerk e.V. und ROXY gGmbH.

Wir danken für die Unterstützung durch das Ministerium für Wissenschaft, Forschung und Kunst Baden-Württemberg