---
id: "587534855410207"
title: ++VERSCHOBEN++ Ich bin ein Berblinger - Das Musical
start: 2021-01-06 20:00
end: 2021-01-06 23:00
address: ROXY.ulm
link: https://www.facebook.com/events/587534855410207/
image: 80236784_10157043090122756_5621464444971253760_o.jpg
teaser: Auf Grund der aktuellen Situation können die Januartermine des Musicals nicht
  stattfinden. Momentan wird nach Ersatzterminen im Sommer gesucht. Gekauf
isCrawled: true
---
Auf Grund der aktuellen Situation können die Januartermine des Musicals nicht stattfinden.
Momentan wird nach Ersatzterminen im Sommer gesucht. Gekaufte Tickets behalten solange ihre Gültigkeit.

***
Der „Schneider von Ulm“ lebt! Den 250. Geburtstag von Albrecht Ludwig Berblinger feiert seine Heimatstadt mit einem Festival. Ein Höhepunkt wird das Musical „Ich bin ein Berblinger. Der Traum vom Fliegen 2.0“ sein, das die Geschichte des Flugpioniers in die Gegenwart transponiert.

Wer kennt ihn nicht, den „Schneider von Ulm“, der am 31. Mai 1811 mit seinem selbst konstruierten Flugapparat bei einem öffentlichen Flugversuch über der Donau einen so schmählichen Absturz erlitt. Tatsächlich basierten die Gleitflug-Forschungen von Albrecht Ludwig Berblinger auf hervorragenden Kenntnissen in Sachen Mechanik und Thermik. Nur eine unglückliche Verkettung von Umständen  führten zum Misserfolg, mit dem er traurige Berühmtheit erlangte und die auch seinen sozialen Absturz zur Folge hatte. Am 24. Juni 2020 jährt sich der Geburtstag Berblingers zum 250. Mal.
 
Scheitern als Chance
In der Tradition von Musicals wie „Rocky Horror Show“ und „Hair“ produzieren der Verein Patchwork Kultur, das Roxy und die Kulturabteilung der Stadt Ulm die Geschichte um einen Nachfahren Berblingers, der seinen Urahn rehabilitieren möchte. Dabei verzettelt sich der moderne Berblinger, macht Schulden, erleidet einen Burn-Out und landet schließlich in der Psychatrie. Dort trifft  er nicht nur einen dubiosen Betrüger und einen korrupten Arzt, sondern auch die große Liebe. Doch es wäre kein Berblinger-Musical, käme nicht auch das Thema Scheitern zum Tragen. Die gute Botschaft lautet: Scheitern ist eine Chance, die Voraussetzung für Entwicklung und die wahre Triebfeder menschlicher Kreativität. Nur wer scheitert, findet auch Lösungen.

Eine Produktion von patchwork kultur e.V. in Kooperation mit der Kulturabteilung der Stadt Ulm und dem ROXY.
Unterstützt von der Sparda-Bank BW