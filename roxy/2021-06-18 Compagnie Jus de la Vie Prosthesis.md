---
id: "286941699356024"
title: "Compagnie Jus de la Vie: Prosthesis"
start: 2021-06-18 20:00
end: 2021-06-18 21:30
address: ROXY.ulm
link: https://www.facebook.com/events/286941699356024/
image: 102342748_2868061043243390_4414104474700742656_o.jpg
teaser: “You can check out at anytime, but you can never leave” (nach “Hotel
  California”/EAGLES) Eine surreale Tragikomödie, verortet in einer Hotellobby,
  in
isCrawled: true
---
“You can check out at anytime, but you can never leave”
(nach “Hotel California”/EAGLES)
Eine surreale Tragikomödie, verortet in einer Hotellobby, in der die tiefsten Geheimnisse gelüftet werden. Wo die Götter und Gurus versuchen, Anhänger zu finden und wo die Menschheit in Gefahr ist. Mit einer Sprache, die künstlerische Grenzen mit einem schnellen Wechsel zwischen Humor und Dunkelheit, Tanz, Text und Live-Musik überschreitet. Mitwirkende sind unter anderem 5 Künstler mit über 70 Jahren. 
https://ulmmoves.de/programm/jus-de-la-vie-prosthesis/ 