---
id: "506439333582991"
title: The Magic Mumble Jumble & tba * ROXY Ulm
start: 2021-04-30 20:00
end: 2021-04-30 23:00
address: ROXY.ulm
link: https://www.facebook.com/events/506439333582991/
image: 94481913_10157461343002756_104626876504342528_o.jpg
teaser: Das Doppelkonzert von Jaya The Cat und The Magic Mumble Jumble vom 30.04.2020
  wird auf zwei Termine gesplittet.  Die Tickets behalten ihre Gültigkeit
isCrawled: true
---
Das Doppelkonzert von Jaya The Cat und The Magic Mumble Jumble vom 30.04.2020 wird auf zwei Termine gesplittet.

Die Tickets behalten ihre Gültigkeit für das Konzert von Jaya The Cat am 18.12.2020. Natürlich können sie auch an den jeweiligen VVK-Stellen zurückgegeben werden.

The Magic Mumble Jumble werden im kommenden Jahr am 30.04.2021 mit einer noch nicht bekannten Band zusammen auftreten. Hierfür sind Tickets demnächst separat erhältlich. Sobald die zweite Band feststeht, werden auch Tickets verfügbar sein.

Wir tanzen in den Mai mit einem schweißtreibenden Doppelkonzert. 

***

The Magic Mumble Jumble ist schon immer eine Band mit positiver Message und wahrer Lebensfreude - und sie scheinen damit ins Schwarze zu treffen, denn der Tourkalender ist von April bis November bis oben hin voll! 

Sie haben Ende 2019 erfolgreich ihre erste eigene Headliner Tour in Deutschland gespielt. "Wir konnten unseren Augen nicht trauen, die Leute strömten förmlich herein und jeder Club war voll!" - so der Leadsänger. Aber das achtköpfige Ensemble teilt mehr als diese Lebenserfahrungen, nämlich eine ganz eigene Klangwelt aus Jazz, Pop, Indie und Folk und zwar geschüttelt, nicht gerührt. Mit seiner charismatischen Bühnenpräsenz und seiner ‘Live-to-Love’ Einstellung lässt Bandleader Paul Istance die Shows zu einem magischen Erlebnis werden, bei dem die Kluft zwischen Publikum und Band komplett verschwindet. Mit acht Singstimmen, Blasinstrumenten, Percussion, Piano, elektrischen sowie akustischen Gitarren und besonders viel positiver Energie, zieht THE MAGIC MUMBLE JUMBLE die Menschen in ihren Bann. Alles an dieser Band ist echt: der Sound, die Message und die Menschen dahinter. Mit diesem authentischen Auftreten und dem Talent der Musiker erschafft die Band jedes mal aufs Neue eine Atmosphäre, in der sich der Zuschauer zugleich frei und verbunden fühlt. 

Seit ihrer Gründung veröffentlichte die Band mehrere Singles und der Song "Home" erschien 2015 auf einer Sony Compilation. Diesen Veröffentlichungen folgte 2016 ihr Debüt Album “The Magic Mumble Jumble” - ein einzigartiger Live-Mitschnitt der Band zusammen mit mehr als 60 Musikern. Die CD wurde aufgrund der hohen Nachfrage schon mehrfach reproduziert. Im Herbst 2017 veröffentlichten sie die EP “We All Want Sunshine” mit der gleichnamigen Singleauskopplung und brachten daraufhin im November 2018, ihr erfolgreiches zweites Album 'Show your Love’ auf den Markt. Dieses Album nahm die Band zusammen mit dem Produzenten Tom Gelissen auf (Ennio Morricone, Woodkid, Armin van Buuren). 2019 wurde THE MAGIC MUMBLE JUMBLE eine große Ehre zuteil: Das “Burg Herzberg Festival” bat die Band die sensationellen Live Aufnahmen des Vorjahres zusammen als Live Album zu veröffentlichen. “Live at Burg Herzberg Festival” ist ein Werk, dass natürlicher nicht sein könnte und und die stärkste Seite der Band zeigt: eine Live-Performance die ihres Gleichen sucht. 2020 wurden sie vom ZDF eingeladen um beim 'Moma' dem ZDF Morgenmagazin live aufzutreten. 2020 ein Jahr, dass für uns alle anders kam als erwartet. Der ausgebuchte Tourkalender (u.a. Herzberg Festival, Tollwood Festival, Open Ohr, Lott Festival, Woodstock Forever, U&D Würzburg) von The Magic Mumble Jumble hatte sich auf 2021 verschoben. Doch wer denkt, dass es demnach ruhig um die Band geworden würde, irrte sich. Die letzten Wochen und Monate waren geprägt von erfolgreichen Single-Veröffentlichungen wie “We Are One”, eine Ode an den globalen Zusammenhalt mit Musikern aus der ganzen Welt oder “Don’t Forget”, eine Erinnerung auch während des Lockdowns mal das Tanzbein zu schwingen. Mit viel Emotion und Engagement hat die Band mit diesen Songs den Zeitgeist getroffen und tausende Menschen auf der ganzen Welt berührt. Letzterer hat auf Spotify gerade die 100.000 plays Marke überschritten. 

“Diese Zeit hat uns mit unseren Fans noch enger verbunden und da kam auf einmal die Idee: Wenn unsere Fans nicht zu uns kommen können, dann müssen wir eben zu ihnen kommen”, sagt der Leadsänger Paul Istance. Aus der Idee wurde kurzerhand ein Konzept und so spielte die Band 2020 doch 25 Gartenparty Konzerte und besuchte Fans in ganz Deutschland. Im Frühling 2021 will The Magic Mumble Jumble an den Erfolg des letzten Jahres anknüpfen und zahlreiche Tourneen sowie einen prall gefüllten Festivalsommer spielen. 