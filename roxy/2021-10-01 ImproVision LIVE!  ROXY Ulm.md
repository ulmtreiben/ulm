---
id: "976657063171308"
title: ImproVision LIVE! * ROXY Ulm
start: 2021-10-01 20:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/976657063171308/
image: 241665828_10158774704592756_793866348949696529_n.jpg
isCrawled: true
---
Impro Vision startete während der Lockdownzeit und nach dem Sommer lädt Wibke Richter (Showbuddies Ulm) endlich wieder ein - zu einem Abend der ganz besonderen Art. Dieses Mal sind Wibke und ihre Gäste das erste Mal live auf der ROXY-Bühne zu sehen.

Ihre Gäste sind Solokabarettisten und Koryphäen aus der Impro- Comedy Szene. 

Mit dabei ist dieses Mal Bernd Waldeck, ein Zauberkünstler und Finanzkabarettist. Bernd Waldeck plaudert gerne aus dem Nähkästchen „Geldanlage“ und verknüpft seine Beobachtungen aus zwölf Jahren Berufserfahrung mit überraschender Erkenntnissen der Verhaltenspsychologie. 

Lisa Spranz (Showbuddies / Ulm) und Adrian Klein (Bühnenpolka / München) sind die Impro-Comedians, die sich an diesem Abend aus dem Publikum und den Themen des Kabaretts Ihre Inspiration holen.

Ein Abend der abwechslungsreicher nicht sein könnte - auf der ImproVisions-Bühne. Dazu darf natürlich die Band nicht fehlen. Christian Grässlin, an der Trompete und Hartmut Semar, am Klavier sorgen für den dazugehörigen musikalischen Rahmen. 

Talk, Musik, Kabarett und Impro-Comedy

***

Die Veranstaltungsreihe "Digitale Kunst im Lockdown" wird im Programm Kultursommer 2021 durch die Beauftrage der Bundesregierung für Kultur und Medien (BKM) mit Mitteln aus NEUSTART KULTUR gefördert.

Der Ulmer Kultursommer ist eine Initiative der Kulturabteilung der Stadt Ulm in Zusammenarbeit mit der Ulmer Kulturszene. Die einzelnen Programmpunkte werden von Gleis 44, ROXY Ulm oder HfG Archiv veranstaltet. Alle Beteiligten bedanken sich bei den Stadtwerke Ulm / Neu-Ulm und bei der Kulturstiftung des Bundes für die freundliche Unterstützung.

Veranstalter: ROXY gemeinnützige GmbH
