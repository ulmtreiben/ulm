---
id: "571304814323448"
title: Lisa Eckhart * Lesung "Omama" * ROXY Ulm
start: 2021-11-21 14:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/571304814323448/
image: 241973901_10158786120277756_1201761422861229364_n.jpg
isCrawled: true
---
„Helga, schnell, die Russen kommen!“ 1945 ist Oma Helga in der Pubertät und kämpft mit ihrer schönen Schwester Inge um die Gunst der Besatzer. 1955 schickt man Helga dann aufs Land. Den Dorfwirt soll sie heiraten. Sowohl Helga als auch die Wirtin haben damit wenig Freude. 1989 organisiert die geschäftstüchtige Oma Busreisen nach Ungarn, um Tonnen von Fleisch über die Grenze schmuggeln. Bevor sie – inzwischen schon über achtzig – in See sticht und mit der Enkelin im handgreiflichen Wettbewerb um den Kreuzfahrtkapitän buhlt.
Lisa Eckhart unternimmt einen wilden Ritt durch die Nachkriegsgeschichte: tabulos, intelligent, böse, geschliffen – und sehr, sehr komisch.