---
id: "358863435743003"
title: ROXY Lockdown Sessions - Die Doku * ROXY Ulm
start: 2021-11-18 20:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/358863435743003/
image: 227582344_10158701482777756_1661964803151086926_n.jpg
isCrawled: true
---
DIGITALE KUNST IM LOCKDOWN

4 days, 4 songs, 4 dancers, 40 musicians & technicians
Mitte März 2020: durch den ersten Corona-Lockdown ist der Kulturbetrieb zum Stillstand gekommen. Viele tausend Veranstaltungen und hunderte Tourneen wurden abgesagt oder mussten in eine ungewisse Zukunft verlegt werden. Das Ulmer Kulturzentrum ROXY ist geschlossen. Musiker:innen, Tänzer:innen und Künstler:innen aus allen Genres und die Agenturen, Veranstalter sowie die unzähligen Freelancer:innen, die hinter den Kulissen arbeiten und die Shows und Tourneen mitgestalten und möglich machen, sind von heute auf morgen auf unbestimmte Zeit ohne Jobs. 
Zeit zu resignieren? Im Gegenteil: wir im ROXY fanden, diese ungewöhnliche Lockdown-Phase muss künstlerisch dokumentiert und in ein bleibendes, möglichst facettenreiches Zeitdokument umgesetzt werden. Also verordneten wir „dem eigenen Team und der Musikszene der Stadt ein Gegengift zur wachsenden Niedergeschlagenheit“ – die ROXY LOCKDOWN SESSIONS.
Dazu luden wir viele bekannte Musiker:innen ins geschlossene ROXY ein, das temporär in ein Aufnahmestudio verwandelt wurde. An vier Tagen im Mai 2020 entstanden hier berührende, live mitgeschnittene Musikaufnahmen, die den Spirit dieser Zeit einfangen und inzwischen auch als limitiertes Vinylalbum erschienen sind. 
Diese Tracks dienten Choreograf:innen, Tänzer:innen und Videokünstler:innen als Basis für vier spannende Tanzvideos. Aus den Filmaufnahmen der Sessions und den Tanzvideos entstand schließlich eine fesselnde 85-minütige Film-Dokumentation, welche im Rahmen der Reihe „Digitale Kunst im Lockdown“ gezeigt wird.
***
Die Veranstaltungsreihe "Digitale Kunst im Lockdown" wird im Programm Kultursommer 2021 durch die Beauftrage der Bundesregierung für Kultur und Medien (BKM) mit Mitteln aus NEUSTART KULTUR gefördert.
Der Ulmer Kultursommer ist eine Initiative der Kulturabteilung der Stadt Ulm in Zusammenarbeit mit der Ulmer Kulturszene. Die einzelnen Programmpunkte werden von Gleis 44, ROXY Ulm oder HfG Archiv veranstaltet. Alle Beteiligten bedanken sich bei den Stadtwerke Ulm / Neu-Ulm und bei der Kulturstiftung des Bundes für die freundliche Unterstützung.