---
id: "2340969779366616"
title: Women Against Violence * ROXY Ulm
start: 2021-09-25 20:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/2340969779366616/
image: 234370210_10158713733257756_3096869178327935932_n.jpg
isCrawled: true
---
Die Veranstaltungen wurden vom 05.02.2021 auf den 25.09.2021 verschoben. Tickets behalten ihre Gültigkeit oder können an den Vorverkaufsstellen zurückgegeben werden.

*************

„I’m no longer accepting the things I cannot change. I’m changing the thing I cannot accept.” Angela Davis

Wusstet ihr, dass laut einer aktuellen Studie der Europäischen Union, jede dritte Frau in Deutschland von Gewalt betroffen ist? Mit "WOMEN AGAINST VIOLENCE" gibt der italienische Choreograf Carmine Romano ein klares Statement über die aktuelle Lage der Frauen weltweit ab. In der Vorstellung erzählen drei Tänzerinnen von Frauen, die stellvertretend für alle betroffenen Frauen in der Welt stehen. Das Publikum lernt den Alltag jener kennen, die von Mobbing, Stalking bis hin zu Häuslicher Gewalt betroffen sind. Wie sie kämpfen, wie sie sich selbst verlieren und wie sie am Ende erschöpft sind - aber auch, wie sie ihre Kraft wieder sammeln und einen Ausweg finden.

Lasst euch mit diesem schwierigen Thema konfrontieren. Es ist Zeit, dass wir sprechen, dass wir tanzen, dass wir laut schreien: "DON’T GIVE UP TO VIOLENCE!"

Die Choreografie ist eine Zusammenarbeit zwischen Carmine Romano und den drei Tänzerinnen.

Künstlerische Leitung: Carmine Romano
Tänzerinnen: Jia Bao Beate Chui / Sophia Ebenbichler / Sara Grüber
Stimmen: Anke Siefken / Clemens Grote
Musik: Arne Herrmann
Assistenz: Melissa Lucatello
Management: Raphaëlle Polidor / Petra J Stotz
Dramaturgie: Maria Winter 
Bühnenbild: Agnete Winter
Unterstützung: Ray Lacsamana

Laboratorio Danza ist seit 2008 eine Tanzplattform für Ulm und die Region. Wir leiten Workshops und Tanzunterricht an, aber unsere Hauptschwerpunkte sind es Tanzaufführungen mit professionellen Tänzerinnen und Tänzern anzubieten.

Partner & Sponsoren:

Landesbank Baden-Württemberg
Sound Circle
Dansarts Ballett Centrum
Schulz Design
Töchter Ulms
Stadt Ulm

laboratorio-danza.de