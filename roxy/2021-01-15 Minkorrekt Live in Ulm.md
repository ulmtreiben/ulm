---
id: "849658485453300"
title: Minkorrekt Live in Ulm
start: 2021-01-15 20:00
end: 2021-01-15 23:00
address: ROXY.ulm
link: https://www.facebook.com/events/849658485453300/
image: 74380904_2621484567943849_1741647382132555776_o.jpg
teaser: Methodisch Inkorrekt - Die Rockstars der Wissenschaft live in Ulm \o/  Keine
  Panik?! Das sehen die beiden Physiker Dr. Nicolas Wöhrl und Dr. Reinhard
isCrawled: true
---
Methodisch Inkorrekt - Die Rockstars der Wissenschaft live in Ulm \o/

Keine Panik?! Das sehen die beiden Physiker Dr. Nicolas Wöhrl und Dr. Reinhard Remfort, bekannt aus dem Kultpodcast „Methodisch inkorrekt!“, ganz anders. Überall lauern die Gefahren: Kometen, Naturgewalten, der menschgemachte Klimawandel und völlig irrationale Politiker. Wieso hört denn niemand mehr auf die Stimme der Wissenschaft? 

Ende 2020 ist es wieder Zeit für „Science not Silence!“. Die Rockstars der Wissenschaft packen den Tourbus voll mit neuen Experimenten, um den Wissenschaftsskeptikern den Kampf anzusagen. Denn spätestens, wenn die zwei Physiker auf der Bühne die Fakten und Feuerexperimente auspacken, ist auch dem letztem im Saal klar: es ist Zeit für „Panik“!

Aus dem Netz auf die Bühne
Seit 2013 begeistern die Physiker mit ihrem ebenso informativen wie unterhaltsamen Podcast „Methodisch inkorrekt!“ eine stetig wachsende Fangemeinde. Über 80.000 Hörer verfolgen regelmäßig den lockeren Plausch, bei dem neben Neuigkeiten aus der Wissenschaft („Warum orientieren sich Hunde beim Gassi gehen am Magnetfeld der Erde?“) auch Experimente („Plasma in der Mikrowelle“, „Feuertornado“), neue Chinagadgets sowie Musiksongs (man glaubt gar nicht wie viele schlechte Image-Videos es von deutschen Universitäten gibt) präsentiert werden, die einen Bezug zur Wissenschaft haben. 

Nachdem die beiden spontan entschieden, ihre 100. Podcastfolge als Live-Show auf die Bühne zu bringen, für die es binnen weniger Stunden keine Karten mehr gab, war die Idee geboren, den Ruhrpott zu verlassen und die Wissenschaft dahin zu bringen, wo sie gebraucht wird: Raus aus den Universitäten, rein in unsere Köpfe für die gute Abendunterhaltung und die nächsten Partygespräche!
