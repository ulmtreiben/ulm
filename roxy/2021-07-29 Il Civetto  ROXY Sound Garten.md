---
id: "546401340058851"
title: Il Civetto * ROXY Sound Garten
start: 2021-07-29 20:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/546401340058851/
image: 202524342_10158600130987756_2475677989059529157_n.jpg
isCrawled: true
---
LIVEMUSIK UNTER FREIEM HIMMEL
EINTRITT FREI! 💛

Global Pop mit Tiefgang - das ist Il Civetto. 

Nach ihrem Debütalbum vor drei Jahren ist ihre Fangemeinde stetig gewachsen.  Es hat sich herumgesprochen, dass Il-Civetto-Konzerte mitreißend und magisch sind. Ein ekstatisch-modernes Musikerlebnis, nie zu brachial, immer tanzbar -auf Platte, wie im Konzertsaal. Nach mehreren Deutschlandtourneen mit insgesamt mehr als 300 Konzerten, Auftritten beim MS Dockville, dem Fusion Festival, Montreux Jazz Festival, dem 3000° oder bei den Anti-G20 Protesten in Hamburg, begeistert die Band inzwischen auch ein internationales Publikum von Istanbul bis Kopenhagen. Il Civettos Reise begann 2010 in der Berliner U-Bahn.  Guerilla-Taktik:  für zwei Stationen den Waggon zum Beben gebracht, die Security mit Döner bestochen -dann wieder weiter.  In der Berliner Clubszene mit Auftritten im Kater Holzig, Ritter Butzke oder Sisyphos groß geworden, verzeichnete die Band nach ihrem Debütalbum 2015 eine beeindruckende Erfolgsgeschichte. Und nun Album Nr.  2:  Facing the Wall.  Geschrieben in Marokko, Griechenland und Andalusien, gesungen auf Englisch, Deutsch und Französisch, aufgenommen in Berlin - mit viel Liebe zum Detail haben il Civetto ihre ekstatisch-magische Spielwiese von der Bühne ein zweites Mal ins Studio getragen. Dabei hat die verträumte Leichtigkeit des 2016 erschienenen Debütalbums „il Civetto“ der harten Realität Platz eingeräumt: 

Facing the Wall ist ein zutiefst berührendes Fragment unserer Zeit. Der Ausdruck einer Generation im Umbruch, auf der Suche. Ein Album zwischen Euphorie und Melancholie -schwelgen, schwitzen, tanzen -mit einem Kribbeln im Bauch und weit aufgerissenen Augen. Il Civetto sind ernster geworden, irgendwie realer, und haben doch ihren ganz eigenen, charakteristischen Sound behalten. Tief berührend, immer tanzbar.

www.ilcivetto.de/