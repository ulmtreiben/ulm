---
id: "1705531709550216"
title: Die Heinz-Erhardt-Show
start: 2021-04-22 20:00
end: 2021-04-22 23:00
address: ROXY.ulm
link: https://www.facebook.com/events/1705531709550216/
image: 59960344_10156481639067756_629353034035494912_o.jpg
teaser: Ersatztermin für den 23.04.2020 Tickets behalten ihre Gültigkeit. **  »HEINZ
  lebt!«  Die drei haben ihren erfolgreichen Klassiker »HEINZ!« noch einmal
isCrawled: true
---
Ersatztermin für den 23.04.2020
Tickets behalten ihre Gültigkeit.
**

»HEINZ lebt!«

Die drei haben ihren erfolgreichen Klassiker »HEINZ!« noch einmal ausgepackt, poliert und ergänzt und machen uns den HEINZ. Alle drei und jeder für sich nähern sich dem Genie mit eigenem Schalk. Sie kneten Gedichte bis sie quietschen; sie knödeln, mimen, hiphoppen und würden auch nicht zögern, für »HEINZ« nackt in eine Torte zu springen.

Herr Sauer trägt dazu sein Haar meist offen, Herr Staub ist wohlbeleibt – noch´n Gewicht – Herr Fortmeier lässt die Puppen tanzen. Auch in einer Kleinkunstversion mit zwei Künstlern möglich.

Heinz Erhardt – immer aktuell, das muss gewürdigt werden! Eine kabarettistische Revue für den Urgroßvater deutscher Comedy! Denn was war Heinz Erhardt für ein Schelm! Selbst stocknüchtern hat man bei seinen Sketchen das Gefühl, man habe schon einige Korn in der Kimme ...

Günter Fortmeier:
ist Schattenspieler, Bauchredner und Handtheater-Komiker. Pantomime und Schauspiel hat der vielseitige Improvisationskünstler an der Folkwang-Schule Essen studiert und ist seit 20 Jahren in der Kleinkunst, im Varieté und auf Festivals zuhause. Seine skurrilpoetischen Geschichten bringt der Meister des Handspiels mit trockenem Witz auf die Bühne.

Frank Sauer:
ist Schauspieler, Kabarettist und Radiosketcher. Der Mann jongliert mit Worten wie ein Brasilianer mit dem Ball. Rasant und variantenreich. Mit dem Duo »Nestbeschmutzer« gewann er die begehrte St. Ingberter Pfanne, als Solist die Tuttlinger Krähe und den Reinheimer Satirelöwen. Auf renommierten Kabarettbühnen in Deutschland und der Schweiz, wie auch im Galabereich ist Frank Sauer ein Garant für gute Unterhaltung.

Volkmar Staub:
gilt als der Wortspielphilosoph unter den politischen Kabarettisten. Hintersinnproduzierender Quergedankler mit kreativer Liebe zum Unsinn. Er ist Autor vieler Kabarettproduktionen, Hörspielverfasser und –versprecher. Seit Jahren versucht er die Figur von Heinz Erhardt zu erreichen und schießt dabei manchmal übers Ziel hinaus. Ansonsten bei den Pointen treffsicher.

Heinz Erhardt:
erblickte am 20. Februar 1909 in Riga das Licht der Welt. Schon als Kind erwachte sein Talent zur spaßigen Unterhaltung am Klavier. Erhardts spitzbübischer Humor und sein spielend leichter Umgang mit der deutschen Sprache sind bis zum heutigen Tage legendär und unvergessen. Viele seiner Gedichte sind in den allgemeinen Sprachschatz übergegangen und werden heute noch gerne rezitiert. Seine Wortspiele, oft mit einer versteckten und kritischen Pointe versehen, haben nichts von ihrer Aktualität verloren.