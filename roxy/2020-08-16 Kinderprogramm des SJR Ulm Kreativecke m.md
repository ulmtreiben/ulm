---
id: "3529029023776816"
title: "Kinderprogramm des SJR Ulm: Kreativecke mit Ytong"
start: 2020-08-16 15:00
end: 2020-08-16 19:00
address: ROXY.ulm
link: https://www.facebook.com/events/3529029023776816/
teaser: Wer Lust auf Malen oder Basteln hat, ist hier genau richtig. Egal ob klassisch
  mit Stift und Papier oder auch kompliziertere Bastelaufgaben, für jeden
isCrawled: true
---
Wer Lust auf Malen oder Basteln hat, ist hier genau richtig. Egal ob klassisch mit Stift und Papier oder auch kompliziertere Bastelaufgaben, für jeden ist was dabei.

Ytong? Was ist das denn?!
Ytong ist Porenbeton, das sind diese weißen Blöcke, aus welchen man zum Beispiel auch Wände baut. Wir haben allerdings was ganz anderes vor: Gemeinsam werden wir verschiedene Figuren schleifen und sägen, dabei ist euer handwerkliches Geschick gefragt! 

Maximale Teilnehmerzahl: 10
15:00-19:00 H

Das Angebot ist kostenlos (und findet unter der Berücksichtigung der derzeitigen Hygiene- und Sicherheitsmaßnahmen statt). Wer mag, kann sich gerne im Voraus mit Name, Adresse und Telefonnummer unter knehr@sjr-ulm.de anmelden.

Kooperation von Stadtjugendring e.V. und ROXY gGmbH