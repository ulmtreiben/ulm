---
id: "488856748993357"
title: "Go Go Gazelle * ROXY Soundgarten * Support: Grundhass"
start: 2021-07-01 19:30
locationName: ROXY.ulm
link: https://www.facebook.com/events/488856748993357/
image: 192682839_10158543599382756_3094596968855459146_n.jpg
isCrawled: true
---
LIVEMUSIK UNTER FREIEM HIMMEL
EINTRITT FREI! 💛

GO GO GAZELLE
Support: Grundhass

Go Go Gazelle. Aus Augsburg.
Bestehend aus Sebastian (Gesang, Gitarre), Andreas (Bass, Gesang) und Hutti (Schlagzeug, Gesang). Nach dem Split ihrer gemeinsamen Band Benzin, mit denen sie schon einige Underground-Punkrock-Meriten eingeheimst hatten (u.a. Support-Tour für Madsen, Festivals wie Serengeti, Deichbrand oder Open Flair), dachten sich Sebastian und Hutti: das kann es ja wohl noch nicht gewesen sein. Die Zeit für ein neues gemeinsames Projekt war 2017 gekommen. Die Beiden fanden mit Andreas einen Bassisten, der ebenfalls mit langjähriger Banderfahrung (Leerlauf) glänzen konnte und darüber hinaus auch nochmal neuen musikalischen Input bringt. 
Erstes Ergebnis der Zusammenarbeit war die EP Schleuder, die in bester DIY-Manier innerhalb kürzester Zeit geschrieben, zusammen live aufgenommen und veröffentlicht wurde.

Was sie aber sind: umtriebig. Und so wird man Go Go Gazelle auch wieder in jeder Menge Clubs und auf Festivals begegnen – sobald das eben wieder erlaubt ist. Versprochen.

Grundhass:

Seit 2013 spielte GRUNDHASS über 100 Konzerte im gesamten Deutschsprachigen Raum. Egal ob in kleinen Kneipen, auf Festivalbühnen oder als Support von Künstlern wie Montreal, Frank Turner, The Baboon Show oder Abstürzende Brieftauben. Auf diesem Weg konnte GRUNDHASS Erfahrungen sammeln, die das Album „Wenig Los“ nicht zum hingerotzten Schnellschuss, sondern zum gut durchdachten Erstlingswerk machen.