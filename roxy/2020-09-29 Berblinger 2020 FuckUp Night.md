---
id: "181274969838082"
title: "Berblinger 2020: FuckUp Night"
start: 2020-09-29 20:00
end: 2020-09-29 23:00
locationName: kultur in ulm
address: Frauenstraße 19, 89073 Ulm
link: https://www.facebook.com/events/181274969838082/
image: 87511797_1005372083190639_8361243968788234240_n.jpg
teaser: Die Veranstaltung wurde vom 28.05.2020 verschoben. Tickets behalten ihre
  Gültigkeit. *** Die Absicht der FuckUp Night ist es zu zeigen, dass ein Fehsc
isCrawled: true
---
Die Veranstaltung wurde vom 28.05.2020 verschoben. Tickets behalten ihre Gültigkeit.
***
Die Absicht der FuckUp Night ist es zu zeigen, dass ein Fehschlag, wie Berblingers Sturz in die Donau, nicht zwangsläufig das Ende bedeuten muss, sondern auch die Chance für einen Neuanfang bieten kann. Nicht unsere Misserfolge, sondern der Umgang mit ihnen sowie der Mut und Wille zur Veränderung der Situation sind entscheidend. Hier beichten mehrere Speaker ihre Fehler und erklären, wie man aus Zitronen Limonade macht. Die Geschichten der FuckUp Night sollen inspirieren, ermutigen und zu einem entspanntem und positiven Umgang mit dem allgegenwärtigen Scheitern verhelfen.