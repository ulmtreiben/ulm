---
id: "217670893252248"
title: Hugh Coltman * ROXY Soundgarten
start: 2021-08-06 20:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/217670893252248/
image: 191979636_10158535087342756_8815999156734769756_n.jpg
isCrawled: true
---
LIVEMUSIK UNTER FREIEM HIMMEL
EINTRITT FREI! 💛

Einst war er Kopf einer aufstrebenden englischen Bluesrock-Band The Hoax. Als der Erfolg irgendwann abbrach, fand er sich auf einmal als Straßenmusiker in Paris wieder. Heute lebt Hugh Coltman fest in der französischen Metropole. Für seine Hommage an Nat King Cole "Shadows - Songs of Nat King Cole“ wurde der von den Franzosen adoptierte Brite und von den Jazzern eingemeindete Blueser bei den renommierten Victoires du Jazz 2017 als Sänger des Jahres ausgezeichnet. Mit dem Folgeprojekt "Who's Happy?" erkundet Hugh Coltman nun den farbenreichen Sound des New Orleans Jazz. Auf den war er in Paris gestoßen, als er 2012 bei einem Konzert des belgischen Groove-Pianisten Éric Legnini als Sänger aushalf.  Coltman entdeckte "die Nonchalance von Jazzmusikern, die mehr Rock'n'Roll sind als viele Rockmusiker" und damit die Stars seiner Kindheit wieder, wie Kid Ory, Sidney Bechet, Fats Domino, Dr. John und The Meters. Alle diese Musiker haben ihre musikalischen Wurzeln im New Orleans Jazz. Die Aufnahmen von "Who's Happy?" entstanden in New Orleans, womit sich Coltman einen Traum erfüllte. Bei seinen Konzerten tritt er mit einer hochkarätigen Band im New Orleans Format auf. An der Gitarre zum Beispiel ist Freddy Koella, der ebenso fest bei Bob Dylan spielte wie er ein Jahrzehnt als Gitarrist von Willy DeVille wirkte. Am Schlagzeug sitzt mit Raphael Chassin einer der bekanntesten Session-Musiker des als Wiege des Jazz geltenden New Orleans.