---
id: "405814233952515"
title: Dominic Miller * ROXY Ulm
start: 2021-04-13 20:00
address: ROXY.ulm
link: https://www.facebook.com/events/405814233952515/
image: 130390831_10158117365112756_2568420134754291470_o.jpg
teaser: Dominic Miller, bekannt als Stings treuer Side-Kick-Gitarrist, ist auch ein
  sehr erfolgreicher Solo-Künstler, der 9 Alben unter seinem eigenen Namen v
isCrawled: true
---
Dominic Miller, bekannt als Stings treuer Side-Kick-Gitarrist, ist auch ein sehr erfolgreicher Solo-Künstler, der 9 Alben unter seinem eigenen Namen veröffentlicht hat. 

Bandleader Miller genießt sowohl in der professionellen Welt der Musik als auch bei Musikfans überall einen beneidenswerten Ruf.

Geboren in Buenos Aires als Sohn eines amerikanischen Vaters und einer irischen Mutter studierte er Gitarre am renommierten Berklee College in Boston und an der London Guildhall School of Music. Er ist seit den späten 80er Jahren ein gefragter Session-Musiker und seine Liste der Engagements passt in keine Schublade. Er arbeitete mit Phil Collins, The Chieftains, Eddi Reader, Katie Melua, Bryan Adams, Paul Young, Nigel Kennedy, Peter Gabriel und Tina Turner ein, um nur einige zu nennen.

Dominic Miller ist seit dem 1991 erschienenen Album "The Soul Cages" an jedem Sting-Album beteiligt und gab über tausend Konzerte mit dem ehemaligen "Police"-Sänger. Hits wie "Shape Of My Heart" und "La Belle Dame sans regrets" hat er mitgeschrieben.


Mit seinem neuen Album Absinthe wird er nun seiner Solo-Diskografie ein weiteres Highlight hinzufügen.  Aufgenommen im Studio "La Buissonne" in Südfrankreich mit Weltklasse-Musikern - Manu Katché am Schlagzeug, Nicolas Fiszman am Bass, Mike Lindup am Klavier, Keyboards und Santiago Arias auf Bandoneon - hat er eine faszinierende Klangmischung aus Jazz, Pop, Acoustic Folk, Contemporary Classical, Latin und Tango Elementen geschaffen. "Absinthe" vereint all diese Stile, die etwas völlig Neues hervorbringen. Einen passenden Genrebegriff für eine solche Symbiose gibt es noch nicht, aber wir glauben, dass es diesen bald geben wird.

Dominic Miller wird mit diesem faszinierenden Line-up im April 2021 auf Tour gehen.

Dominic Miller – g
Rhani Krija – Perc
Nicolas Fiszman – b
Jacob Karlzon – p
***
Santiago Arias – bandoneon (nicht sicher, ob er dabei sein kann)