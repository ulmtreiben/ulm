---
id: "353829735600723"
title: "Summer in the city: Otto Normal im ROXY Sound Garten"
start: 2020-07-25 20:00
end: 2020-07-25 23:00
address: ROXY.ulm
link: https://www.facebook.com/events/353829735600723/
image: 106922805_10157703593747756_4837822733494550206_o.jpg
teaser: Die letzten Jahre waren Otto Normal in einem Paralleluniversum gefangen. Dort
  haben sie Schirmmützen getragen, im ZDF Märchensongs performt, Preise ab
isCrawled: true
---
Die letzten Jahre waren Otto Normal in einem Paralleluniversum gefangen. Dort haben sie Schirmmützen getragen, im ZDF Märchensongs performt, Preise abgesahnt, rückwärts gerappt und einen Major-Deal unterschrieben. Bis sie beim Erklimmen des Gipfels der Musikindustrie in den Spiegel schauten und sich dabei selbst nicht mehr erkannten. Schnell wurde klar: Es muss wieder geflasht werden – und zwar authentisch und ohne Rücksicht auf Wenauchimmer. Also haben sie sich das Brett unter die Füße geschnallt, die Abfahrt genossen und sich anschließend zu viert auf die Suche zurück zu sich selbst gemacht. Dabei wurden böse Zungen überhört und der inneren Stimme Raum gegeben. Die vier Freiburger haben sich 2010 zusammengeschlossen, um die deutschsprachige Musikszene aufzumischen. Otto Normal machen authentischen, treibenden Indie-Pop. Dabei lässt sich die Band Innovatives einfallen und nimmt z.B. eine Gebärdendolmetscherin mit auf ihre Konzerte oder zum Videodreh.

Der Biergarten ist ab 15:00 H geöffnet.
Eintritt frei.
***
Konzerte in der Reihe "Summer in the city" sind eine Kooperation von Kunstwerk e.V. und ROXY gGmbH.
Wir danken für die Unterstützung durch das Ministerium für Wissenschaft, Forschung und Kunst Baden-Württemberg.