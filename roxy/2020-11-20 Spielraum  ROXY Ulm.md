---
id: "861823397889249"
title: Spielraum * ROXY Ulm
start: 2020-11-20 20:00
address: ROXY.ulm
link: https://www.facebook.com/events/861823397889249/
image: 122101984_10157997083537756_3656803294715619871_o.jpg
teaser: "Fußballkultur, Talk & Musik: Wie Corona den Fußball verändert  Mit Lars M.
  Vollmering (Buchautor von „Wir stellen fest, was wirklich zählt – Wie Coron"
isCrawled: true
---
Fußballkultur, Talk & Musik: Wie Corona den Fußball verändert

Mit Lars M. Vollmering (Buchautor von „Wir stellen fest, was wirklich zählt – Wie Corona unseren Fußball verändert“) und Tim Göhler (Ex-Fußballprofi beim FC Heidenheim und Arzt)

Live-Musik: Ingo Illerdelta
Moderation: Andreas Kullick

Das Corona-Virus verursachte die größte globale Krise seit dem Zweiten Weltkrieg. Welche Auswirkungen hat diese Pandemie auf den Fußball – hat sie auch ihn und seine Wahrnehmung verändert? Moderator Andreas Kullick hat sich Experten zum Thema in den „Spielraum“ eingeladen: auf der Großbildleinwand zugeschaltet aus Wolfsburg ist der Autor Lars M. Vollmering, der bereits im Juni das erste Buch über Fußball und Corona herausgebracht hat. In einer spannenden Momentaufnahme hat er darin die ersten verrückten Wochen von Corona und Fußball eingefangen. Vollmering ist gelernter Journalist und hat fast 20 Jahre lang für die RTL-Mediengruppe als Reporter und Redakteur gearbeitet. Er wird Auszüge aus seinem Buch lesen und im Gespräch mit Andreas Kullick die aktuelle Situation diskutieren.

Zweiter Gesprächspartner – und live im ROXY – ist der Ex- Fußballprofi und Arzt Tim Göhlert. Göhlert spielte mehr als ein Jahrzehnt beim FC Heidenheim, von der Oberliga bis in die 2. Bundesliga bestritt er über 300 Partien für den Verein. Parallel studierte er in Ulm zwölf Semester Medizin und tauschte 2016 sein Trikot gegen den Arztkittel. Göhlert kann aus erster Hand fundiert sowohl zum Thema Sport wie auch zu Corona Auskunft geben.

Musikalisch begleitet wird diese „Spielraum“-Folge vom Blues-Pianisten und Sänger Ingo Illerdelta.