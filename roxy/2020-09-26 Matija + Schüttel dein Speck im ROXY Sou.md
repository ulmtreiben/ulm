---
id: "645796809669856"
title: Matija im ROXY Sound Garten (indoor)
start: 2020-09-26 20:00
end: 2020-09-26 22:00
address: ROXY.ulm
link: https://www.facebook.com/events/645796809669856/
teaser: Wir schließen unseren Sound Garten aufgrund des kalten Wetters im Haus.
  Deshalb können auch unsere Lieblinge von Schüttel dein Speck leider nicht aufl
isCrawled: true
---
Wir schließen unseren Sound Garten aufgrund des kalten Wetters im Haus. Deshalb können auch unsere Lieblinge von Schüttel dein Speck leider nicht auflegen.
Trotzdem freuen wir uns auf ein schönes September-Abschluss-Konzert mit:

Matija
Lässig und voller Gefühl, mal fiebrig, mal melancholisch, gerne auch grenzenlos ekstatisch und schwebend, getragen von Matijas weicher, androgyner und sinnlicher Stimme. Eine Stimme, mit der man Musikgeschichte schreibt. Indie Pop, wie sie ihn spielen, kennt man kaum aus Deutschland und verortet Matija eher bei Bands wie Foals, Two Door Cinema Club oder The Mars Volta.

Einlass 19:00 H
Beginn 20.00 H
Eintritt frei