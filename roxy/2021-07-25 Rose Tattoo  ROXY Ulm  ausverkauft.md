---
id: "220831905891594"
title: Rose Tattoo * ROXY Ulm * ausverkauft
start: 2021-07-25 20:00
end: 2021-07-25 23:00
address: ROXY.ulm
link: https://www.facebook.com/events/220831905891594/
image: 92411127_10157404599032756_3858920556590006272_o.jpg
teaser: Nachholtermin vom 28.03.2020. Ticket behalten ihre Gültigkeit.  ****  Über 40
  Jahre Rock’n’Roll Outlaws – und noch immer nicht genug!  „All I need is
isCrawled: true
---
Nachholtermin vom 28.03.2020. Ticket behalten ihre Gültigkeit.

****

Über 40 Jahre Rock’n’Roll Outlaws – und noch immer nicht genug!

„All I need is a Rock'n'Roll band and somewhere new to play“ – heißt es in „Rock’n’Roll Outlaw“, dem ersten Song des Albums, mit dem die Australier von Rose Tattoo ihr gleichnamiges und zorniges Debüt gaben. Und auch heute, über 40 Jahre später, haben weder der Song oder das legendäre Album, noch die Spielfreude und die Neugier auf neue Bühnen nachgelassen. Und weil vor den europäischen Bühnen die treuesten Anhänger stehen, erwarten uns im März 2020 gleich 20 neue Termine, in denen Rose Tattoo sich durch Deutschland, Tschechien, Norwegen, Schweden, die Niederlande, Frankreich, England und die Schweiz wüten werden. 

Die Hardrock-Ikonen haben 1976 den Grundstein gelegt für eine Karriere, die das Attribut „legendär“ wirklich verdient hat. Die dienstälteste noch aktive Hardrock-Band hat sich trotz einiger Auszeiten und Besetzungswechsel immer wieder wie der oft zitierte Phoenix aus der Rock’n’Roll-Asche erhoben, um anschließend noch kräftiger reinzuhauen. Die „extreme Band für extreme Leute“, wie Frontmann Gary „Angry“ Anderson es rückblickend auf den Punkt bringt, ist der energiegeladene Soundtrack inzwischen mehrerer Generationen, musikalischer Ausdruck eines markanten Lifestyles und nicht zuletzt so wichtige Inspiration für zahlreiche Bands wie beispielsweise Guns ‘N’ Roses. Der besondere Mix, den Rose Tattoo musikalisch auffahren, spricht heute eine sehr weite Bandbreite an Zuschauern an, die diese pure Energie feiern und es roh und laut mögen zwischen Rock’n’Roll, Blues, Punk und Heavy Metal. Die starke Rhythmus-Fraktion um Bassist Mark Evans, Rhythmusgitarrist Bob Spencer, die Rose-Tattoo-typische Slide-Gitarre von Dai Pritchard und natürlich das unnachahmliche Kaliber eines Angry Anderson setzen jede Location ab dem ersten Ton unter Strom. Auch wenn die Wut von früher sich verändert hat, in die Jahre gekommen ist sie auf keinen Fall und sucht sich immer noch ihren Ausdruck in der beeindruckend wuchtigen Power, die diese Band live mit Songs wie „Bad Boy for Love, „The Butcher and fast Eddy“, „Nice Boys“ oder – natürlich! – „Rock’n’Roll Outlaw“ entfacht.
So auch im kommenden Frühjahr, wenn sie anlässlich des 40-Jährigen ihres kultigen Albums wieder nach Europa kommen. Um das sagenhafte Jubiläum der 4 Outlaw-Jahrzehnte auch weiterhin ausgiebig zu feiern, den Fans wieder mit all ihren unsterblichen Songs, ihrer Hingabe und der schweißtreibenden Live-Show alles abzuverlangen – und einmal mehr eindrucksvoll zu beweisen: Long live Rock’n’Roll!
