---
id: "547308886177405"
title: "Marion Sparber: Shell Shock / Porzellan Haus"
start: 2021-06-22 20:00
end: 2021-06-22 22:00
address: ROXY.ulm
link: https://www.facebook.com/events/547308886177405/
image: 101553902_2868071229909038_4708617619207356416_o.jpg
teaser: lustigen oder neurotischen Zügen denselben Raum teilen. Je näher sie sich
  kommen, müssen sie starre Überzeugungen und fixe Schemata loslassen, um ihre
isCrawled: true
---
lustigen oder neurotischen Zügen denselben Raum teilen. Je näher sie sich kommen, müssen sie starre Überzeugungen und fixe Schemata loslassen, um ihrem wahren Ich auf den Grund zu gehen, es zu teilen und sich anderen zu öffnen. Die Geschichte spricht uns als Gesellschaft und als Individuum an. Es geht darum, Menschen und Dinge so zu sehen wie sie sind und nicht wie wir sie gerne hätten, deren Wandel anzunehmen und uns mit ihnen zu entwickeln.

Das sehr physische, vom Partnering geprägte Tanzstück „Porzellan-Haus“ von und mit Marion Sparber und Alan Fuentes Guerra ist ein Versuch, die Komplexität einer Beziehung zwischen Mann und Frau zu versinnbildlichen.
Die Idee der Konstruktion mit einem Partner – sei es ein Haus, ein Leben, eine Zukunft oder ein gemeinsamer Traum – ist oft sehr fragil wie Porzellan. Ein Material, das aus Handarbeit entsteht, viel Geduld und Feingefühl benötigt. Porzellan ist auch wegen seines hohen Werts beliebt, hat aber auch seine zerbrechliche Seite – eine Metapher für die zwischenmenschlichen Beziehungen. Man fragt sich immer wieder, wie es trotz aller Schwierigkeiten möglich ist, dieses tolle Gefühl zwischen zwei Menschen zu konservieren, zu hegen und pflegen und einen Versuch zu starten, ein Porzellan-Haus zu bauen.

https://ulmmoves.de/programm/pu-sparber/ 
