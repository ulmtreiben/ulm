---
id: "179051543555386"
title: "ROXY Lockdown Bar #4"
start: 2021-02-06 20:30
link: https://www.facebook.com/events/179051543555386/
image: 145708083_10158270426857756_3980550297945415182_o.jpg
teaser: Wow - schon unsere vierte Lockdown Bar. Wir freuen uns sehr, wenn ihr wieder
  einschaltet, fleißig kommentiert und im Vorfeld euren Freunden einen Plat
isCrawled: true
---
Wow - schon unsere vierte Lockdown Bar. Wir freuen uns sehr, wenn ihr wieder einschaltet, fleißig kommentiert und im Vorfeld euren Freunden einen Platz an der digitalen Theke freihaltet.
***
Livemusik von Yasi Hofer & Band

Yasi Hofer ist Ulm 25jährige Ausnahmegitarristin. Als Absolventin der weltweit renommiertesten Music-School, dem „Berklee College of Music“ in Boston USA, klettert sie beständig die Karriereleiter empor und hat sich mittlerweile international einen Namen gemacht. Heute spielt sie mit ihren Trio-Kollegen Steffen Knauss (E-Bass, Vocals) und Christoph Scherer (Drums).
***
Talk mit Julia Gámez Martín & Patrizia Moresco

Seit ihrem Musical-Studium an der Universität der Künste in Berlin ist Julia Gámez Martín an diversen Theatern regelmäßig in Hauptrollen zu sehen und zu hören. Parallel ist sie deutschlandweit erfolgreich mit ihrer Kollegin Ariane Müller (ja - unsere Bar-Moderatorin) unter dem Namen "Suchtpotenzial" unterwegs. Zudem tobt sich Julia Gámez Martín in verschiedenen Bands aus, wie unter anderem in der Soul- und Funkband "The Black Diamonds", der Frauen-Pocketbigband "Die Damenkapelle" oder in ihrem Soloprogramm "Lieder lieber ungewöhnlich" mit Damian Omansen am Klavier.

Patrizia Moresco ist Italienerin, mit schwäbischen Wurzeln und Berliner Schnauze. Eine Frau, ein Wort, ein Gag, immer groß und niemals artig. Seit vierzig Jahren mischt die vielseitige Kabarettistin die deutschsprachige Comedy Szene auf und begeistert ihr Publikum immer wieder aufs Neue. Provokant, mit Haltung und dabei brüllend komisch, überzeugt sie mit scharfsinnigen Beobachtungen und erfreulicher Authentizität. Ob Stand-Up, Schauspiel oder Gesang, die Moresco beherrscht ihr Handwerk.
***
Mach's dir selbst - das Cocktail-Tutorial

Im letzten Sommer hat ROXY-Barchef Michl im ROXY Soundgarten gemeinsam mit seinem Team wahrscheinlich mehrere tausend Drinks gemixt. Auch heute wird er mit euch zusammen wieder was Feines zusammenbrauen (nur für Volljährige). 

// Daiquiri
4,5 cl weißer Rum
2,5 cl Limettensaft
1,5cl Zuckersirup
** Zubehör
Boston Shaker
Martiniglas
Eis
Strainer (Sieb)
***
Noch Fragen? 

Dann bleibt zum Zoom-Thementalk und Aftershow Bargespräch
Im Anschluss an den Livestream könnt ihr euch mit den Beteiligten via Zoom unterhalten. Der Link wird spätestens im Stream-Chat bekannt gegeben.

Hier könnt ihr teilnehmen:

FACEBOOK
https://www.facebook.com/roxy.kultur/posts/10158270415292756

YOUTUBE
https://youtu.be/3RKsQquS1Kw

ROXY
https://www.roxy.ulm.de