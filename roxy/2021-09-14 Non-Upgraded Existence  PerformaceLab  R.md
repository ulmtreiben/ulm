---
id: "433367407958746"
title: Non-Upgraded Existence * PerformaceLab * ROXY Ulm
start: 2021-09-14 20:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/433367407958746/
image: 239294189_10158744030627756_8162189953413612409_n.jpg
isCrawled: true
---
Cia. Nadine Gerspacher zeigt „Non-Upgraded Existence“, den dritten und letzten Teil ihrer Trilogie, die sich mit digitalen Technologien und deren Einfluss auf den Menschen auseinandersetzt. Während „The Age of Aquarium“ den Fokus auf die trennenden Aspekte moderner Technologie legte und sich „Let it Rain“ mit künstlicher Intelligenz beschäftigte, rückt »Non-Upgraded Existence« die Unfähigkeit des Menschen ins Rampenlicht, sich mit seinem Körper und mit seiner Umwelt zu versöhnen. Inspiriert von der globalen Mobilisierung der jungen Generation gegen die Ausbeutung der ökologischen Ressourcen inszeniert das Stück ein Endzeitszenario, das die Rückbesinnung auf menschliche Grundbedürfnisse wie Liebe, Nähe und ein soziales Miteinander als unablässlich erscheinen lässt.
Fünf Tänzerinnen und Tänzer gruppieren sich um einen langen Tisch und wohnen dem Abspann der fetten Jahren bei, bis sie der ohrenbetäubende Schrei eines Kindes in eine Art harmonischen Urzustand überführt. Was ist Glück und was macht uns Menschen menschlich? In gewohnter Manier inszeniert die Cia. Nadine Gerspacher ein multimediales, von kraftvollem Tanz und akrobatischen Elementen geprägtes Tanztheater.