---
id: "468112337537161"
title: "ROXY Lockdown Bar #7"
start: 2021-02-27 20:30
link: https://www.facebook.com/events/468112337537161/
image: 152834019_10158320412887756_5799675191305988783_o.jpg
teaser: "Zum siebten Mal: Musik, Tanz & Talk live und online aus dem ROXY - dieses Mal
  mit fliegendem Wechsel an der Moderatoren-Front!  Hier sind die Links zu"
isCrawled: true
---
Zum siebten Mal: Musik, Tanz & Talk live und online aus dem ROXY - dieses Mal mit fliegendem Wechsel an der Moderatoren-Front!

Hier sind die Links zur Veranstaltung:
Facebook: https://www.facebook.com/roxy.kultur/posts/10158327178547756
YouTube: https://www.youtube.com/watch?v=j-Th0dl-Q5w 
ROXY-Homepage: https://www.roxy.ulm.de/ 
**********************

Wibke-J. Richter, die neue Moderatorin der ROXY LOCKDOWN BAR
Dem ROXY-Publikum ist Wibke-J. Richter seit vielen Jahren bekannt, denn normalerweise spielt sie regelmäßig mit den „Showbuddies", Ulms erstem Improtheater-Ensemble, in den Kulturhallen.

Im Gespräch: Felix Achberger von Donau3FM
Seit fast 15 Jahren moderiert Felix Achberger mit wechselnden Moderationspartnern jeden Werktag von 05:00 bis 10:00 H morgens die Gute Laune Morningshow – und kümmert sich als Redaktionsleiter auch um die Inhalte der Show. Nach der Sendung ist er mit Raza, seiner Straßenhündin aus Rumänien, in Schwaben und in der Region unterwegs, um Material für den Hundepodcast „Kalte Schnauze“ zu sammeln, der es inzwischen auf über 60 Folgen gebracht hat. 

Live-Musik: Gypsy-Soul-Brass von Monobo Son  
Erdig und direkt kommen die fünf Musiker von "Monobo Son", die von LaBrassBanda-Posaunist Manuel Winbeck gegründete bayerische Gipsy-Soul-Brassband, daher. Die Zuschauer erwarten hier keine überbordenden Produktions-Orgien erwarten, sondern geballte Bandpower, die in Ihrer unverkünstelten Ästhetik bisweilen Erinnerungen an Rock- und Jazzaufnahmen aus den 60er und 70er Jahren aufkommen lassen. Liebhaber von Instrumentalmusik dürfen sich über vier nagelneue Stücke freuen, aber auch inhaltlich nimmt sich Sänger Manuel Winbeck kein Blatt vor den Mund und bezieht bisweilen deutlich Stellung zu aktuellen Gesellschaftsthemen, ohne dabei die Leichtigkeit und Energie zu verlieren, die das ganze Album versprüht. Denn Monobo Son sind an erster Stelle dem unwiderstehlichen Groove verpflichtet, und das wird bei jedem Lied des neuen Albums deutlich.

Tanz-Special mit „#ShowMeYourMoves“
Seit November leitet der spanisch-neuseeländische Choreograf und Tänzer Pablo Sansalvador das neue TanzLabor im ROXY. Für den heutigen Abend wird er mit dem Tänzer Bogdan Muresan und der Tänzerin Nutan Ferletic live auf die von unserer Moderatorin Wibke-J. Richter eingeworfenen Themenvorschläge tänzerisch reagieren und erneut beweisen, wieviel Spaß Tanz machen kann. Beatboxer Manuel Stahl wird mit seinem Können die musikalische Basis liefern.

Mix-Drink-Tutorial mit Michl Brenner
Im letzten Sommer hat ROXY-Barchef Michl Brenner im ROXY Soundgarten gemeinsam mit seinem Team mehrere tausend Longdrinks gemixt. Im siebten Teil seines Tutorials verrät er heute Abend weitere Tipps für Profi-Drinks. Wenn ihr diese Drinks auch zuhause probieren wollt: Das braucht ihr dafür:

//Piña Colada
- 6 cl dunkler Rum
- 2 Barlöffel Cream of Coconut
- 10 cl Ananassaft
- 1,5 cl Limettensaft
- 2cl Sahne
 
//Treacle
- 6 cl gereifter Rum
- 1 Barlöffel Zuckersirup
- 2 Dashes Angostura Bitter
- 2 cl Apfelsaft
- Orangenzeste

 // Zubehör
- Bostonshaker
- Eis
- Barlöffel
- Strainer (Sieb)
- Whiskeytumbler, Longdrinkglas

Noch Fragen? Dann bleibt zum Zoom-Thementalk und Aftershow-Bargespräch!

Im Anschluss an den Livestream (ca. 22:00 H) ist erneut ein persönliches Bargespräch mit den Beteiligten über Zoom geplant. Hier ist der Link dafür:

https://zoom.us/j/96189896576?pwd=NmhWRmVWSk1TYTFrdFo4WjZ4Ync2UT09
 
Meeting-ID: 961 8989 6576
Kenncode: 796609