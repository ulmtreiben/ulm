---
id: "3743068259084508"
title: Women Against Violence * ROXY Ulm
start: 2021-02-05 20:00
address: ROXY.ulm
link: https://www.facebook.com/events/3743068259084508/
image: 125534247_10158067381557756_6885150196567383213_o.jpg
teaser: „I’m no longer accepting the things I cannot change. I’m changing the thing I
  cannot accept.” Angela Davis   Wusstet ihr, dass laut einer aktuellen S
isCrawled: true
---
 „I’m no longer accepting the things I cannot change. I’m changing the thing I cannot accept.” Angela Davis

 Wusstet ihr, dass laut einer aktuellen Studie der Europäischen Union, jede dritte Frau in Deutschland von Gewalt betroffen ist? 
Mit "WOMEN AGAINST VIOLENCE" gibt der italienische Choreograf Carmine Romano ein klares Statement über die aktuelle Lage der Frauen weltweit ab. Im Tanzstück wird in neun verschiedenen Szenen von drei Tänzerinnen erzählt, die stellvertretend für alle Frauen in der Welt stehen. Das Publikum lernt den Alltag jener kennen, die von Mobbing, Stalking bis hin zu Häuslicher Gewalt betroffen sind. 
Lasst euch mit diesem schwierigen Thema konfrontieren. Es wird nicht immer einfach sein: teilweise aufregend, Angst einflößend, traurig und Wut auslösend zugleich; aber gemeinsam schaffen wir das. Gemeinsam ist man stärker. Es ist Zeit, dass wir sprechen, dass wir tanzen, dass wir laut schreien: "DON’T GIVE UP TO VIOLENCE!" Die Choreografie ist eine Zusammenarbeit zwischen Carmine Romano und den drei Tänzerinnen. 

Künstlerische Leitung: Carmine Romano
Tänzerinnen: Jia Bao Beate Chui / Sophia Ebenbichler / Sara Grüber
Stimmen: Anke Siefken / Clemens Grote
Musik: Arne Herrmann
Assistenz: Kathrin Knöpfle
Management: Raphaëlle Polidor / Petra J Stotz
Dramaturgie: Marie Winter
Backstage/Kostüme: Vincenzo Larocca
Bühnenbild: Agnete Winter
Unterstützung: Ray Lacsamana

Danke für die Unterstützung:
LBBW
Sound Circle
Dansarts
Schulz Design
Töchter Ulms
Stadt Ulm