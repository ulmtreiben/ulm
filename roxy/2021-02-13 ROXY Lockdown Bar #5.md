---
id: "130970695562387"
title: "ROXY Lockdown Bar #5"
start: 2021-02-13 20:30
link: https://www.facebook.com/events/130970695562387/
image: 150078867_10158294006002756_1475407589854216135_o.jpg
teaser: "Heute gibt es einen roten Faden in unserer Lockdown Bar: Die Hammond
  Orgel!  Livemusik von Matchtape Lebensfreude. Energie. Spielen gehen.
  Funkyness."
isCrawled: true
---
Heute gibt es einen roten Faden in unserer Lockdown Bar: Die Hammond Orgel!

Livemusik von Matchtape
Lebensfreude. Energie. Spielen gehen. Funkyness. Erdbeeren mit Pfeffer. Freiheit.
Wenn die Röhren in der Hammond Orgel zu glühen beginnen und das Horn im Leslie Verstärker seine Pirouetten dreht dann gibt es kein Halten mehr. Das ist nur ein Grund, warum der Stuttgarter Hammond Spieler Martin Meixner seinem neuen Trio den Namen „MATCHTAPE“ gegeben hat, was soviel bedeutet wie Zündschnur oder Anzündband.

Talk mit Michael Falkenstein
Schon in jüngsten Jahren kam Michael durch seine Eltern mit Hammond-Orgeln in Berührung. Mit 11 Jahren spielte er zum ersten Mal in der Öffentlichkeit, und gab mit 15 sein erstes Solo-Konzert. 1987 entdeckte der japanische Konzernchef Mr. Suzuki sein musikalisches und kaufmännisches Talent und engagierte ihn 1991 als Manager für die deutschsprachigen Länder der Firma Hammond. (Hammond Chicago gehört seit 1987 zu Suzuki Corporation Japan.) 1995 traf Michael Falkenstein in Montreux zum ersten Mal mit dem „Godfather Of Soul“ James Brown zusammen, der ihn bei einem Konzert in Frankfurt zum „Prince Of Soul“ schlug. Im Jahr 2000 traf er zum ersten Mal mit Jimmy Smith zusammen mit dem ihn seitdem eine Freundschaft verbindet. Begegnungen mit fast allen namhaften Organisten wie Jon Lord (Deep Purple), Keith Emerson u.v.m. waren die Bonbons des Lebens in der Musikbranche. Michael sieht es als seine Aufgabe an, dass Wissen um diese Instrumente weiterzugeben und hält auch Seminare und Workshops ab. FunFact: seine Tochter heißt Leslie (wer den Text zu Matchtape gelesen hat, kann erahnen, woher die Inspiration zu diesem Namen kam).

#ShowMeYourMoves
Pablo Sansalvador holt euch runter vom Sofa. Lasst ihm über den Chat eure eigenen Moves oder eure Wünsche zukommen und er wird daraus einen kleinen Tanz kreieren. Beats gibt es dieses Mal von Matchtape-Drummer Christian, die Tänzerinnen Raphaëlle Polidor und Ruth Weigel werden Pablo tänzerisch unterstützen. Eure Vorschläge könnte ihr ab Start der Show einbringen. Über die Hashtag-Nutzung #ShowMeYourMoves freuen wir uns sehr.

Das Cocktail-Tutorial
Im letzten Sommer hat ROXY-Barchef Michl im ROXY Soundgarten gemeinsam mit seinem Team wahrscheinlich mehrere tausend Drinks gemixt. Auch heute wird er mit euch zusammen wieder was Feines zusammenbrauen (nur für Volljährige). Heute ist Tequila-Abend:

// Margarita
6cl Tequila blanco
2cl Cointreau oder Triple sec
3cl frischer Limettensaft
1cl Agavensirup oder Zuckersirup
Flockiges Salz
**Zubehör
Eis
Bostonshaker
Coupette oder Martiniglas

// Tequila Sunrise
6cl Tequila
1,5 cl Cointreau oder Triple sec
7,5 cl Orangensaft
2cl Limettensaft
1cl Grenadine
**Zubehör
Eis
Bostonshaker
Longdrinkglas

Noch Fragen? Dann bleibt zum Zoom-Thementalk und Aftershow Bargespräch
Im Anschluss an den Livestream könnt ihr euch mit den Beteiligten via ZOOM unterhalten.

HIER könnt ihr teilnehmen:

FACEBOOK
https://www.facebook.com/roxy.kultur/posts/10158296218197756

ROXY HOMEPAGE
https://www.roxy.ulm.de

YOUTUBE
https://youtu.be/wvBcgWt0f6I