---
id: "674379523320554"
title: Bukahara • Roxy • Ulm
start: 2020-10-19 20:00
end: 2020-10-19 23:00
address: ROXY.ulm
link: https://www.facebook.com/events/674379523320554/
image: 90697672_10158021340553433_2586516063990579200_o.jpg
teaser: Nicht nur, dass mit den vier Musikern drei Kontinente vertreten sind, nicht
  nur, dass es zwischen Folk, Reggae und Arabic-Balkan in ihren Songs kein H
isCrawled: true
---
Nicht nur, dass mit den vier Musikern drei Kontinente vertreten sind, nicht nur, dass es zwischen Folk, Reggae und Arabic-Balkan in ihren Songs kein Halten gibt: Als feierten die Instrumente Maskenball, darf schon mal eine Geige die Harmonie bestimmen, eine Posaune sich als Tuba ausprobieren und plötzlich hockt der singende Gitarrist auch noch hinterm Schlagzeug. Drei aus der Viererbande haben in Köln Jazz studiert, die meisten von ihnen waren eine Zeit lang als Straßenmusiker unterwegs. Unter dem Namen Bukahara treten sie nun seit zehn Jahren auf. Sie haben sich in dieser Zeit vom Straßenrand in die Bühnenmitte von Festivals und bei Konzerten international hochgespielt. Alle Bandmitglieder sind Multiinstrumentalisten. Als solche sind sie kaum zu fassen – wozu freilich auch ihr rätselhafter Bandname gut passt, der in keinem Wörterbuch dieser Welt zu finden ist. Sicher ist: wo sie spielen, wird getanzt. 