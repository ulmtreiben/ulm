---
id: "814750999216182"
title: Die Reimpatrouille * ROXY Ulm
start: 2021-09-08 20:00
address: ROXY.ulm
link: https://www.facebook.com/events/814750999216182/
image: 228398120_10158701163162756_2266710923623158355_n.jpg
isCrawled: true
---
DIGITALE KUNST IM LOCKDOWN

Die einzige gereimte Comedy Sendung der Welt geht live auf die Bühne!
Im ersten Corona-Lockdown haben Helge Thun, Mirjam Woggon, Jakob Nacken und Johann Theisen begonnen, ihr eigenes Youtube-Format zu senden. Jeweils 30 Minuten vollgepackt mit originellen Reimen, filmreifen Sketchen und Humor auf höchstem Niveau. Wie Fernsehen nur besser und mit Reimvorteil. Zu 100 % selbst geschrieben und produziert.

30 Sendungen später bringen sie die Reimpatrouille nun live auf die Bühne! Reime, Sketche, Lieder, Sprechgesang und Improvisation. Frau Roberta, Promireporterin Stefanie von Stuttgart-Stammheim und Skandal-Coach Jürgen S. live im ROXY. Zwischendurch gibt es kurze Film-Highlights auf der Leinwand und wer weiß ... vielleicht sogar einen gereimten Zaubertrick mit Manni Mirakel ...?

***
Die Veranstaltungsreihe "Digitale Kunst im Lockdown" wird im Programm Kultursommer 2021 durch die Beauftrage der Bundesregierung für Kultur und Medien (BKM) mit Mitteln aus NEUSTART KULTUR gefördert.

Der Ulmer Kultursommer ist eine Initiative der Kulturabteilung der Stadt Ulm in Zusammenarbeit mit der Ulmer Kulturszene. Die einzelnen Programmpunkte werden von Gleis 44, Roxy Ulm oder HfG Archiv veranstaltet. Alle Beteiligten bedanken sich bei den Stadtwerke Ulm / Neu-Ulm und bei der Kulturstiftung des Bundes für die freundliche Unterstützung.