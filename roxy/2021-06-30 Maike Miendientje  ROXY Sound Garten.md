---
id: "349549110118019"
title: Maike Miendientje * ROXY Sound Garten
start: 2021-06-30 18:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/349549110118019/
image: 206427512_10158617768767756_4999526370685197256_n.jpg
isCrawled: true
---
Wir freuen uns, euch in unserem Sound Garten begrüßen zu dürfen. 
Neben dem regulären Biergartenbetrieb versüßen euch unterschiedliche DJs die Ohren.
Am Mittwoch spielt DJ Maike Miendientje Feelgood Indie - happy Songs für euch.
Eintritt frei, Spende für die musikalische Unterhaltung wird gern entgegen genommen.
Öffnungszeiten: 
Mittwoch - Samstag: Ab 17:00 H 
Sonntag: Ab 15:00 H
