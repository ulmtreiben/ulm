---
id: "596354904519629"
title: "Strado Compagnia Danza: FRIDA"
start: 2020-06-13 20:30
end: 2020-06-13 21:30
address: ROXY.ulm
link: https://www.facebook.com/events/596354904519629/
image: 83035444_2570971369619027_4180343612531277824_o.jpg
teaser: „Wozu brauche ich Füße, wenn ich Flügel habe?“  In diesem einen Satz der
  mexikanischen Malerin Frida Kahlo (1907 - 1954) liegen all ihre Verletzlichke
isCrawled: true
---
„Wozu brauche ich Füße, wenn ich Flügel habe?“ 
In diesem einen Satz der mexikanischen Malerin Frida Kahlo (1907 - 1954) liegen all ihre Verletzlichkeit, ihre Kraft und ihr Mut. Sie war nicht die Frau, die sich von Schicksalsschlägen unterkriegen ließ. Ihrem Schmerz verlieh sie Ausdruck mit der Malerei, ihre emanzipierte Kämpfernatur zeigte sich unbeeindruckt von den Konventionen im Mexiko des letzten Jahrhunderts. 

Leben und Werk Frida Kahlos sind Vorlagen des rund einstündigen Tanzabends. Die Performance erzählt wie eine Collage – gleichsam den Bildern der mexikanischen Künstlerin – in nahtlos ineinandergreifenden Sequenzen von einem zerrissenen, stürmischen Leben voller Leidenschaft und Schmerz, voller Lebenslust und Revolution, voller Schicksalsschläge, Zweifel und Selbstbewusstsein.  

„Die Künstlerin Frida Kahlo ist so ambivalent, die Figur so komplex, dass es eine echte Herausforderung war, aus der Lust zu einer tänzerischen Auseinandersetzung mit dem Leben und dem Werk dieser faszinierenden Frau eine Dramaturgie zu entwickeln“, so Domenico Strazzeri, Chef der Compagnia und Choreograf des Abends. 
Jede Sequenz der Performance, jedes Bild rückt Frida in den Mittelpunkt und umgibt sie mit Liebe und Betrug, mit Exzessen, Schicksalsschlägen und immer wieder Kunst. Dabei ist der Tod – mal tanzend, mal als stiller Betrachter – allgegenwärtig. 

https://ulmmoves.de/programm/pu-frida/ 