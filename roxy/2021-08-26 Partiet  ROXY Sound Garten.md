---
id: "205889354742658"
title: Partiet * ROXY Sound Garten
start: 2021-08-26 20:00
end: 2021-08-26 23:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/205889354742658/
image: 206881376_10158615939922756_1810941469961761219_n.jpg
isCrawled: true
---
LIVEMUSIK UNTER FREIEM HIMMEL
EINTRITT FREI! 💛

Die Rampensau gehört bekanntlich zur Gattung gutmütiger, aber – einmal losgelassen - doch recht wilder Live-Tiere.

Partiet darf man getrost dazuzählen, denn die Band verspricht eine atemberaubende Show, voller Energie, Ska, Reggae, Schweiß und verblüffenden Texten auf schwedisch. Seit 2010 lassen sie das Publikum tanzen und haben bereits über 300 Auftritte in elf verschiedenen Ländern absolviert. Alles begann außerhalb von Melbourne auf einem Hinterhof, denn dort trafen sich zwei junge schwedische Musiker der heutigen Band Partiet. Als sie ein Jahr später nach Schweden zurückkehrten, beschlossen sie, eine Band zu gründen. Schnell fanden sie weitere Mitstreiter, zogen alle in ein Haus nach Småland und geboren war Partiet – die Partei. Die Achtköpfige Band hat ihren Namen nicht umsonst, denn mit ihren Songs und Statements beziehen sie u.a. Position gegen Rassismus und setzen sich für die Rechte von Lesben und Schwulen ein.