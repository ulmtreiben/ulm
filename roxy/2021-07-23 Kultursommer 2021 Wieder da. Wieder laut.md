---
id: "338573481072808"
title: 'Kultursommer 2021: "Wieder da. Wieder laut" am Blaubeurer Tor'
start: 2021-07-23 17:00
end: 2021-07-24 00:00
address: Blaubeurer Tor, Ulm
link: https://www.facebook.com/events/338573481072808/
image: 219331981_1144592122704331_1112611359281336696_n.jpg
isCrawled: true
---
Vereint macht Ulms Kultur- und Gastronomie Szene das Unmögliche möglich! Denn wir sind wieder da, wieder laut - 23.-25.07 im Blaubeurer Ring.

Mitten im Kreisverkehr entsteht eine kleine, bunte Oase, mit neuen Gästen und altbekannten Ulmer Kultur- und Gastronomie-Koryphäen. Gemeinsam und drei Tage lang, wird gefeiert, was so lang verloren schien. 

Am Freitag startet mit Mixed-, World- und Hip-Hop-Music unser Ulmer Lieblingssender Radio free FM, der mit seinen Haus und Hof DJ's anrückt. Darunter DJ T-Rex, bekannt aus der Frau Berger und der Sendung "Turn it Up!" oder C.Paight der als Teil der Rythym-Fellows die Ulmer Club- und Lounge-Szene bespielt und regelmäßig bei "Dope on Radio" feinste Musik auf dem Plattenteller serviert. Garniert mit Balkan Beats von Matikku von Volxdub. DJ Montaro liefert für die Kopfnicker-Sektion den Hip-Hop Sound, ehe euch DJ Beeman mit feinsten Afrobeats in den Feierabend entlässt. 

Am Samstag geht der Kultursommer 2021 in die zweite Runde mit Gleis 44 und DECADANCE, die zu diesem Anlass hohen Besuch eingeladen haben: das DJ-Duo MONKEY SAFARI, das weltweit auftritt und zuletzt zwei EPs beim Label Anjunadeep veröffentlicht hat. Neben MONKEY SAFARI steht Marius Maier aka RE.YOU an den Decks. Als gebürtigen Ulmer zog es ihn vor über 10 Jahren nach Berlin. Das Kollektiv Schillerstraße rundet den Samstag musikalisch ab. 

Am Sonntag legen Soundcircus Betreiber Martin mit seiner Plattensammlung bei Soul A Go Go und Roy Bichay mit seinem Funktastic-Set auf. Er hat sein musikalisches Können bereits viele Jahre als Betreiber des Club Eden unter Beweis gestellt.

Für spannende Blickfänge sorgen die Ulmer Künstler vom Farbstoffkollektiv mit digitalen Kunstobjekten, die erst durch Algorithmen zum Leben erweckt werden und über den Abend eine ganz eigene Geschichte erzählen. Sie sind das ganze Wochenende zu sehen.
Neben dem musikalischen Programm wird Meister Eckards Kuriosenkabinett unterhalten mit Kleinkunst und Zaubershows.

Parallel dazu finden täglich zwei Vorstellungen des außergewöhnlichen Tanz-, Performance- und Jahrmarktspektakels BALAGAN! auf verschiedenen Plätzen der Innenstadt  statt. Hierzu gibt es extra Veranstaltungen auf Facebook. 

#Gastronomie:
Serviert wird euch Spritziges, Sanftes, Fruchtiges und alles mit Wumms von der Crew des Stadt- und Subkul-turmagazin Pocket.

Für den guten Tropfen und richtiges Urlaubsgefühl sorgt außerdem der Weinstand vom portugiesischen Einzel-handel Portugal Flavour aus der Weststadt. Den großen und kleinen Hunger könnt ihr mit asiatischer Fusion-küche beim Pacifico Food Truck stillen.  Verdauungskaffee und Wachmacherespresso gibt es das ganze Wochenende von der regio Kaffee-Rösterei Röstschmiede. Am Samstag und Sonntag wird außerdem Eisrausch dabei sein. 

Der #Zugang zum Blaubeurer Tor erreicht ihr aus der Richtung Dichterviertel / Schillerstraße. Der Zugang auf das Veranstaltungsgelände ist nur mit einem #3-G Nachweis (genesen, geimpft oder getestet) möglich. Es gibt vor Ort ein Testzentrum!

Beim IKEA gibt es während der Geschäftszeiten keine Parkmöglichkeiten. Am Sonntag ist der Parkplatz sauber zu halten. 

#Einlass: kostenlos / Fist-come, first-served. 


Der Kultursommer Ulm ist ein Projekt der der Kulturabteilung der Stadt Ulm in Kooperation mit den Kulturzentren Roxy Ulm und Gleis 44 sowie dem HfG Archiv Ulm. Wir bedanken uns bei den Stadtwerke Ulm / Neu-Ulm und bei der Kulturstiftung des Bundes für die freundliche Unterstützung. Die Veranstaltungsreihe wird im Programm Kultursommer 2021 durch die Beauftrage der Bundesregierung für Kultur und Medien (BKM) mit Mitteln aus NEUSTART KULTUR gefördert.

Mehr Infos unter kultursommer.ulm.de