---
id: "1160224827821928"
title: Bassportation * ROXY Sound Garten
start: 2021-08-13 18:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/1160224827821928/
image: 223599976_10158686246247756_5159201439500240736_n.jpg
isCrawled: true
---
Die Bassportation hat sich in den vergangenen Jahren von einem lockeren Event unter Freunden zur heiß begehrtesten Partyreihe des Ulmer Nachtlebens entwickelt.

Auf zwei Floors verwöhnen die DJs im Jazzkeller Sauschdall ihr Publikum bereits seit 8 Jahren mit den besten Tracks die ihr Genre zu bieten hat. Wer noch nie auf der Bassportation war, weiß nicht was eine Party aus Leidenschaft zur Kunst ist. Dieses mal in einer Freiluft-Ausgabe . Sommerlich entspannter Sound aus der Techhouse / Techno / Drum'n'Bass Kiste. Draußen statt im Gemäuer - wir freuen uns! Kommt vorbei!"

Eintritt frei, Spenden für die musikalische Unterhaltung wird gern entgegen genommen.

Öffnungszeiten:
Mittwoch - Samstag: ab 17:00 H
Sonntag: ab 15:00 H

Alle aktuellen Bestimmungen bzgl. Corona findet ihr auf unserer Homepage www.roxy.ulm.de
