---
id: "359363865594504"
title: Soulgül * Roxy Sound Garten
start: 2021-07-18 18:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/359363865594504/
image: 216311733_10158641511827756_3075200062533466830_n.jpg
isCrawled: true
---
Wir freuen uns, euch in unserem Sound Garten begrüßen zu dürfen. Neben dem regulären Biergartenbetrieb versüßen euch unterschiedliche DJs die Ohren.

Eintritt frei, Spenden für die musikalische Unterhaltung wird gern entgegen genommen.

Öffnungszeiten:
Mittwoch - Samstag: ab 17:00 H
Sonntag: ab 15:00 H

Alle aktuellen Bestimmungen bzgl. Corona findet ihr auf unserer Homepage www.roxy.ulm.de
