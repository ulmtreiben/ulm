---
id: "2941583526053135"
title: Matikku * ROXY Sound Garten
start: 2021-07-28 18:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/2941583526053135/
image: 223839203_10158682420092756_6485619778679915849_n.jpg
isCrawled: true
---
Wir freuen uns, euch in unserem Sound Garten begrüßen zu dürfen. Neben dem regulären Biergartenbetrieb versüßen euch unterschiedliche DJs die Ohren.

Balkan Beats, Gypsy Jungle & World Dub schleudert euch Matikku wie Sliwowitz um die Ohren. Bringt eure Babuschka mit, haltet eure Hüte fest, das wird eine rasante Fahrt! 

Eintritt frei, Spenden für die musikalische Unterhaltung wird gern entgegen genommen.

Öffnungszeiten:
Mittwoch - Samstag: ab 17:00 H
Sonntag: ab 15:00 H

Alle aktuellen Bestimmungen bzgl. Corona findet ihr auf unserer Homepage www.roxy.ulm.de

Volxdub, a Balkan Trip:  