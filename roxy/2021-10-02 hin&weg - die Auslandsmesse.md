---
id: "3084736928429731"
title: hin&weg - die Auslandsmesse
start: 2021-10-02 10:00
end: 2021-10-02 15:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/3084736928429731/
image: 240988233_10158758392027756_6378495087066054397_n.jpg
isCrawled: true
---
Du möchtest nach der Schule eine Zeit im Ausland verbringen? Schule im Ausland - das wäre was für dich? Oder fragst du dich, welche Möglichkeiten du nach deiner Ausbildung hast? Dann bist du bei der "hin&weg" genau richtig! Organisationen und Agenturen informieren vor Ort über Ihre Angebote, es gibt unabhängige Beratungsangebote und viele spannende Vorträge.
 
Einlass (nach jetzigem Stand, 31.08.2021) nur mit gültigem 3G-Nachweis – kostenlose Teststation vor Ort

Eintritt frei
Veranstalter: Stadtjugendring Ulm e.V