---
id: "310564230707716"
title: Takeshi's Cashew * ROXY Soundgarten
start: 2021-08-07 20:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/310564230707716/
image: 196378288_10158555177927756_6120073150585777766_n.jpg
isCrawled: true
---
LIVEMUSIK UNTER FREIEM HIMMEL
EINTRITT FREI! 💛

Geht zu eurem Plattenregal und holt die schrullige (und unvergessliche!) Folklore-Schallplatte, die ihr einst auf einem Flohmarkt irgendwo auf Reisen gefunden habt, hervor. Und nun stellt euch vor, dass dieser Sound von Vintage-Synthesizern, Flöten, schrägen Surf-Gitarren, jeder Menge Space-Echo und elektronischen Downtempo-Beats begleitet wird!

Takeshi's Cashew (Laut&Luise, Berlin) sind eine neu gegründete Psych-Funk
Gruppe, die die Grenzen der Clubkultur, Weltmusik & 70er Jahre Psychedelik auslotet. Aus unterschiedlichen musikalischen Richtungen kommend, kreieren sie eine bunte Verschmelzung ihrer Genres, eingebettet in ein clubtaugliches Beatgerüst und erheben
ihre oft komplexen Arrangements zu einem schönen und tanzbaren Potpourri.

Im Sommer 2020 nahmen Takeshi's Cashew ihr Debütalbum "Humans In A Pool" in ihrem winzigen Diy-Studio in Wien auf. Diese selbst produzierte Platte nimmt euch mit auf eine Reise in kosmischen Krautrock, Cumbia, Afrobeat & Disco, die nur nur noch von einem ihrer Live-Auftritte übertroffen werden kann.
