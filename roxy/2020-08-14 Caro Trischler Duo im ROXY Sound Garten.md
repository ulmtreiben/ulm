---
id: "622621488608271"
title: Caro Trischler Duo im ROXY Sound Garten
start: 2020-08-14 20:00
end: 2020-08-14 23:00
address: ROXY.ulm
link: https://www.facebook.com/events/622621488608271/
image: 107087188_10157712726092756_5715439030052983412_o.jpg
teaser: Wer ihre Stimme einmal gehört hat, vergisst sie so schnell nicht wieder. Caro
  Trischler steht mit ihren 24 Jahren regelmäßig auf deutschen Jazzbühnen.
isCrawled: true
---
Wer ihre Stimme einmal gehört hat, vergisst sie so schnell nicht wieder. Caro Trischler steht mit ihren 24 Jahren regelmäßig auf deutschen Jazzbühnen. Ihre Spezialität sind die leisen Töne, die ganz gelassen und souverän auf Englisch und Portugiesisch klingen. Anfang 2019 hat die Sängerin ihr Studium an der Hochschule für Musik in Mainz im Fach Jazzgesang abgeschlossen, sie ist festes Mitglied in drei Bands und Gastsängerin bei diversen Projekten. Am 3.Juli 2020 erschien ihr erstes Album „North e Sul“ in Zusammenarbeit mit dem Mainzer Jazzpianisten Ulf Kleiner.

Unser Biergarten öffnet bereits um 15:00 H.
Eintritt frei.