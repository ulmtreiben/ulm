---
id: "447307929302114"
title: Jean-Philippe Kindler • Deutschland umtopfen • Ulm
start: 2021-10-14 20:00
end: 2021-10-14 22:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/447307929302114/
image: 104678682_10157650856007756_5424594857845730633_n.jpg
isCrawled: true
---
Die Veranstaltung wurde vom 18.04.2020, 20.06.2020 und 27.02.2021 auf den 14.10.2021 verschoben. Ursprünglich war das Programm "mensch ärgere dich" geplant. Tickets behalten ihre Gültigkeit.
***

Eine Satireshow

"Liebe Bürger*Innen, liebes Volk, liebe Umweltsäue!
Ich weiß, ich weiß. Es macht Sie alles so traurig. All das Leid der Welt. Die Ungerechtigkeit. Die Armut. Die AfD. Ich sehe Ihre Betroffenheit ja auf Instagram. Und dennoch: In solchen Zeiten ist es wichtig, dass Sie auch mal auf sich achten! Lassen Sie sich vom Leid dieser Welt nicht das eigene Glück versauen. Denn für jede politische Krise gibt es eine passende Atemübung. Der Gender Pay Gap lässt sich wunderbar wegtanzen. Es ist doch schön, wenn Sie Ihre Überstunden nicht bezahlt bekommen, denn dafür dürfen Sie Ihren Chef ja „Bro“ nennen. Ausbeutung darf endlich nur dann Ausbeutung sein, wenn sie bemerkt wird! Denn, wie Konrad Adenauer einst sagte: „GOOD VIBES ONLY“, liebe Freundinnen und Freunde! Ich verstehe Sie. Die Schere zwischen Arm und Reich lässt Sie unruhig schlafen. Da müssen Politiker wie ich natürlich sofort aktiv werden. Zeit für den Herabschauenden Hund! Spannen Sie die Zehen an. Atmen Sie tief ein. Und wenn Sie die Knie durchdrücken, dann lassen Sie mal alles los! Zeitarbeit. Lohndumping. Raus damit. Der Nahostkonflikt? Nicht mit Ihnen. Sie sind im Moment. Sie sind authentisch. Denken Sie daran, was die Positive Psychologie sagt: Werden arme Menschen ausgebeutet, dann will der Karmagott sie dazu bewegen, sich endlich selbst zu verwirklichen!
Es ist nun endlich mal wieder Zeit für SIE. SIE haben sich ihr Glück verdient. SIE werden aufblühen. Für den harten Weg nach oben hatten SIE nämlich nichts außer das richtige Geschlecht, eine satte Erbschaft und einen deutschen Namen. SIE sind self-made! Finden Sie, dass das ironisch klingt? Ja, so ist es auch gemeint. Denn in unserer Gesellschaft blühen Menschen nur deswegen auf, weil anderen konsequent das Wasser abgegraben wird. Wenn Sie das auch scheiße finden, bissige Satire, Albernheit und Kritik aber mögen, dann kommen Sie doch einfach zu meinem neuen Soloprogramm. Liebe Freundinnen und Freunde, umverteilen war gestern. Ich will blühende Landschaften für alle! Dementsprechend meine Forderung: Wir müssen „DEUTSCHLAND UMTOPFEN“!"

Jean-Philippe Kindler geht ab Oktober 2020 mit seiner Satireshow „DEUTSCHLAND UMTOPFEN.“ auf Tour durch Deutschland und Österreich. Der deutschsprachige Meister im Poetry Slam bleibt sich treu und kritisiert über 90 Minuten die ungerechte Gesellschaft. Er tut dies wie immer mit reichlich Humor, Ernsthaftigkeit und Albernheit. Für sein erstes Programm „Mensch ärgere Dich“ wurde er mit der St. Ingberter Pfanne ausgezeichnet – einem der renommiertesten Preise des deutschsprachigen Kabaretts."