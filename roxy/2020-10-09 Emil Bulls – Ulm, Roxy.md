---
id: "639999953215896"
title: Emil Bulls – Ulm, ROXY
start: 2021-10-08 19:00
end: 2021-10-08 23:59
locationName: ROXY.ulm
link: https://www.facebook.com/events/639999953215896/
image: 119930806_10157921446782756_7345115932372714144_n.jpg
isCrawled: true
---
Einlass: 19:00 Uhr | Beginn: 20:00 Uhr

Die Veranstaltung wurde vom 09.10.2020 verschoben. Tickets behalten ihre Gültigkeit.

Immer wieder aufs Neue beweisen sich Emil Bulls als eine der wandlungsfähigsten und im positiven Sinne unberechenbarsten Bands ihres  Genres. Das Jahr 2021 steht bei den Musikern aus Süddeutschland unter dem Motto „Party Hard“. Erfahrungsgemäß teilen Emil Bulls derartige Vorhaben liebend gern mit ihren Fans - vorzugsweise auf großen Bühnen und einer ausgedehnten Tour. Letztere können sich die Fans schon jetzt im Kalender mit einem großen Ausrufezeichen vermerken, denn im Oktober und Dezember rocken Emil Bulls auf ihrer „25 TO LIFE“-Jubiläumstour wieder die Konzerthallen hierzulande. Finaler Höhepunkt des Jubiläumsjahres wird dann am 19. Dezember der Emil Bulls Birthday Bash im Münchner Zenith sein. In diesem Sinne: „HAPPY BIRTHDAY AND FAMILY MEANS FAMILY FOREVER!“.