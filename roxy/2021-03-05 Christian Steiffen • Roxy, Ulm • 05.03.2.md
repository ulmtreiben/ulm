---
id: "1730290043780702"
title: Christian Steiffen • Roxy, Ulm • 05.03.2021
start: 2021-03-05 20:00
end: 2021-03-05 23:00
address: ROXY.ulm
link: https://www.facebook.com/events/1730290043780702/
image: 93189201_10157176187402361_5340155597595607040_o.jpg
teaser: +++ Liebe Konzertbesucher, das Konzert vom 13.05.2020 muss aufgrund der
  aktuellen Situation und der behördlichen Anordnungen auf den 05.03.2021 versch
isCrawled: true
---
+++ Liebe Konzertbesucher, das Konzert vom 13.05.2020 muss aufgrund der aktuellen Situation und der behördlichen Anordnungen auf den 05.03.2021 verschoben werden. Bereits gekaufte Karten behalten ihre Gültigkeit. +++
 
Christian Steiffen
Einlass: 19:00 Uhr | Beginn: 20:00 Uhr
Eintritt VVK ab 32,95€
 
Der „Gott Of Schlager“ gibt neue Termine für die gleichnamige Tour im kommenden Jahr bekannt. Die Gelegenheit die Legende aus Osnabrück mit seinem dritten Meisterwerk auf der Bühne live zu erleben, sollten Fans sich nicht entgehen lassen – denn wo der Steiffen ist, da ist die Party!
Nüchtern und sachlich, wie immer gewohnt reflektiert. „Gott of Schlager“ - so der Titel des dritten Ergusses - ist ein weiterer Höhepunkt im Steiffen-Oeuvre. Schon in der Wahl des Albumtitels greift Christian auf das von ihm perfekt inszenierte und zelebrierte Stilmittel der Untertreibung zurück und fügt hinzu: „Worte können mich und dieses Album nicht beschreiben. Der Begriff „Gott of Schlager“ kann nur ein Versuch sein, wird aber für immer eine Krücke bleiben...“. Und er hat natürlich recht: Schon der erste Titel macht deutlich, dass die Ferien vom Rock´n Roll vorbei sind: „Hier ist Party“ verbindet Elemente der klassischen Discomusik mit modernem Sprechgesang. Bei „Wie der Wind“ geht es um Freiheit, Verdauung und Vergänglichkeit, beleuchtet in einem Country-Song, der sich in psychodelische Sphären steigert. Mit „Schöne Menschen“ widmet sich Christian der hässlichen Fratze des Schönheitswahns von den Gebrüdern Grimm bis heute. Das autobiografische Stück „Ich breche in die Nacht“ beschreibt eine Nacht und lotet dabei die Gegensätze von Durst und Heimweh sowie Übelkeit und Gesellschaft aus.
„Verliebt Verlobt Verheiratet Vertan“ ist selbsterklärend und gefolgt von fälligen klassischen Themen des deutschsprachigen Chansons wie Schützenfest, Seefahrt, Sylvester, Punkmusik und natürlich dem Karneval. Alles dargebracht im wunderbar
weichen Timbre von Christians Stimme, das wie immer perfekt korreliert mit den Harmonien und Arrangements seines Freundes und Mitproduzenten Dr. Martin Haseland (jetzt neu mit Umhänge-Keyboard). Das sind neue große Melodien eines
sich stetig selbstübertreffenden Poeten und Entertainers.
Die Christianisierung hat wohl noch lange nicht ihren Höhepunkt erreicht, aber dass der Steiffen immer grösser wird, stört den Steiffen nicht: „Ich zwinge ja niemanden, zu meinen Konzerten zu kommen – die kommen alle freiwillig. (…)“. Und so wird er auch mit diesem Album wieder auf große Fahrt gehen.