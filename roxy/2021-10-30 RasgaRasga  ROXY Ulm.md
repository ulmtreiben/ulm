---
id: "290593012597380"
title: RasgaRasga * ROXY Ulm
start: 2021-10-30 20:00
end: 2021-10-30 22:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/290593012597380/
image: 243840192_10158815187372756_8899015974190234621_n.jpg
isCrawled: true
---
Tickets: https://roxyulm.reservix.de/p/reservix/event/1735790 

RasgaRasga sind pure Energie. Sechs musikalische Grenzgänger*innen zwölf Instrumente, fünf Sprachen und ein Feuerwerk aus Emotionen – einer Stromschnelle gleich tragen RasgaRasga ihr Publikum an einen bunten, glitzernden Ort. Einen Ort des Sich-Fallenlassens, des Miteinanders und des Moments.

Mit ihrem neuen Programm „Aperitif“ zelebrieren sie ihren eigenen Stil populärer Musik mit immer spannenden neuen Genreverschmelzungen und transkulturellen Einflüssen. Mitten in der Pandemie kreativ geworden, drücken RasgaRasga in der kommenden Veröffentlichung ihren Herzschlag, ihre Stimmungen, Visionen und Gefühle aus: mal euphorisch ausbrechend und dann wieder hypnotisierend monoton, mal melancholisch weit um dann wieder energisch nach vorne zu treiben. Dabei entwickeln die sechs Musiker*innen einen Sog, der das Publikum mitreißt und auf die Tanzfläche zieht. RasgaRasga sind die aktuellen Preisträger des Creole NRW Weltmusikwettbewerbs, Teil des Programms Musikkulturen NRW und werden durch die Initiative Musik gefördert.

Rasga Rasga sind:

Daria Assmus – Gesang, Akkordeon
Lukas Fischer – Trompete, Bellfront, Bassposaune
Jonas Krause – Geige, Posaune, Gesang
Benedikt Fischer – Gitarre, Banjo, Gesang
Gregor Brändle – Bass, Gesang
Felix Kuthe – Schlagzeug

www.rasgarasga.de