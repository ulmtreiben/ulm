---
id: "504682526891268"
title: DeWolff | Ulm (verschoben)
start: 2021-04-15 20:00
end: 2021-04-15 23:00
address: ROXY.ulm
link: https://www.facebook.com/events/504682526891268/
image: 90503020_3092978610723203_6723458142937546752_o.jpg
teaser: "Präsentiert von: ROCKS Magazin & Betreutes Proggen  Special Guest: The Grand
  East  Verschoben vom 03.10.2020 - Bereits gekaufte Tickets behalten ihre"
isCrawled: true
---
Präsentiert von: ROCKS Magazin & Betreutes Proggen

Special Guest: The Grand East

Verschoben vom 03.10.2020 - Bereits gekaufte Tickets behalten ihre Gültigkeit.