---
id: "826976611250693"
title: Oimara * ROXY Soundgarten
start: 2021-07-30 20:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/826976611250693/
image: 192676801_10158544143777756_2828717114281041297_n.jpg
isCrawled: true
---
LIVEMUSIK UNTER FREIEM HIMMEL
EINTRITT FREI! 💛

Der Oimara kommt mit neuem Album „A Quantum Prost“ zurück! Direkt von der Alm am Tegernsee, wo er aufgewachsen ist, ist Bayerns lässigster Songwriter und Musikkabarettist vor zwei Jahren herabgestiegen und haut dem Publikum seitdem mit seinem schrägen Charme eine derart vogelwilde Impro-Show um die Ohren, dass zwischen dem Tegernseer Tal und dem Hamburger Hafen kein Auge trocken bleibt. 
Ein Stenz vom Berg, wie der Oimara einer ist, schert sich nichts um Konventionen und Genres, und auf der Alm muss man eh immer alles können. Deshalb mixt der gelernte Koch munter drauflos: relaxte Gitarrensounds im Stil von John Mayer treffen auf fast schon valentineske Wortspiele und eine Verschmitztheit, die an eine jugendliche und extrem coole Version von Fredl Fesl denken lässt. Das Ganze selbstverständlich mehrsprachig - auf Bayrisch und Hochdeutsch - und mit einer Stimme, in der eine Extraportion Blues und Soul steckt! 
Im Oktober 2020 erschien bereits die erste Single-Auskopplung „Busheislparty“ samt Video als Vorbote zum zweiten Album, das im Frühjahr 2021 zur gleichnamigen Tour erscheint. Hier geht’s als Einstimmung zur ultimativen OIMARA „Busheislparty“: https://www.youtube.com/watch?v=vVH-kRlGhJQ 
Die CD ist hier bereits bestellbar: https://www.bognermusik.de/oimara/a-quantum-prost.html 
Getragen von den Vibes des Publikums lässt sich der Hafner Beni (so steht’s in seinem Pass) treiben und besingt alles, was das Leben ihm bietet, von seinem Lieblingsgetränk, dem „Bierle in da Sun“ und seinem Lieblingskleidungsstück, der „Lederhosn“ bis hin zur politischen Weltlage oder der heimischen Situation im Schlafzimmer („Schnucki-Putzi“ meets „Lieblingsdepp“). Dazwischen improvisiert der Oimara singend und erzählend Geschichten, die das Leben so spielt, und man könnte das Gefühl haben er stünde innerlich noch in einer Gourmet-Küche. 
Äußerlich hat die Gitarre aber die Pfanne ersetzt, womit er nun seine musikalischen Menüs auftischt. Er entwickelt dabei einen Sog, der vermeintlich angestaubte Genres wie Musikkabarett und klassische Liedermacherei schwungvoll ins nächste Jahrtausend schleudert. Seine Live-Qualitäten konnte der 28-jährige bereits auf diversen Festivals sowie bei Auftritten im „Vereinsheim“ (BR), bei „Nuhr ab 18“ (ARD), und als Support von LaBrassBanda, DeSchoWieda und Martin Frank unter Beweis stellen. 
Man muss den Oimara also erlebt haben, denn jede seiner Shows ist einzigartig wie er selbst, dieser bunte Hund vom Tegernsee, der angetreten ist, um der bayrischen Musik- und Kabarettszene den Blues einzuhauchen. 