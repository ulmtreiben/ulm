---
id: "183486679648984"
title: Ines Geipel liest "Umkämpfte Zone" * ROXY Ulm
start: 2020-06-18 20:00
end: 2020-06-18 23:00
address: ROXY.ulm
link: https://www.facebook.com/events/183486679648984/
image: 86186132_10157227362597756_2348015237258543104_o.jpg
teaser: 'Umkämpfte Zone. Mein Bruder, der Osten und der Hass  Fremdenfeindlichkeit und
  Hass auf „den Staat": Verlieren wir den Osten Deutschlands? Das Buch suc'
isCrawled: true
---
Umkämpfte Zone.
Mein Bruder, der Osten und der Hass

Fremdenfeindlichkeit und Hass auf „den Staat": Verlieren wir den Osten Deutschlands? Das Buch sucht Antworten auf das Warum der Radikalisierung, ohne die aktuell bestimmende Opfererzählung nach 1989 zu bedienen. Es erzählt von den Schweigegeboten nach dem Ende der NS-Zeit, der Geschichtsklitterung der DDR und den politischen Umschreibungen nach der deutschen Einheit. Verdrängung und Verleugnung prägen die Gesellschaft bis ins Private hinein, wie die Autorin mit der eigenen Familiengeschichte eindrucksvoll erzählt.

„Ein wirklich grandioses Buch. Kein Wort zu viel und jeder einzelne Satz ein Volltreffer. Eins der wichtigsten Bücher des Jahres.“ Markus Lanz, ZDF - Markus Lanz, 26.02 und 11.04.2019

Seit 2015 haben sich die politischen Koordinaten unseres Landes stark verändert – insbesondere im Osten Deutschlands. Was hat die breite Zustimmung zu Pegida, AfD und rechtsextremem Gedankengut möglich gemacht? Ines Geipel folgt den politischen Mythenbildungen des neu gegründeten DDR-Staates, seinen Schweigegeboten, Lügen und seinem Angstsystem, das alles ideologisch Unpassende harsch attackierte. Seriöse Vergangenheitsbewältigung konnte unter diesen Umständen nicht stattfinden. Vielmehr wurde eine gezielte Vergessenspolitik wirksam, die sich auch in den Familien spiegelte – paradigmatisch sichtbar in der Familiengeschichte der Autorin. Gemeinsam mit ihrem Bruder, den sie in seinen letzten Lebenswochen begleitete, steigt Ines Geipel in die „Krypta der Familie“ hinab. Verdrängtes und Verleugnetes in der Familie korrespondiert mit dem kollektiven Gedächtnisverlust. Die Spuren führen zu unserer nationalen Krise in Deutschland.

„Das Buch ‘Umkämpfte Zone‘ hat mich sehr beeindruckt - durch die Fülle treffender Beobachtungen und scharfsinniger Analysen [...]. Insbesondere die Ausführung zum Buchenwald-Mythos, zur AfD und zur Blockade des ostdeutschen Wegs in eine Verantwortungsgesellschaft finde ich treffend. Und wie recht Ines Geipel hat: "50 Jahre Diktatur-Welt kann mit Pampern, Regionalismus und Rückzug aus dem Politischen nicht bewältigt werden"!“ Wolfgang Thierse, Bundestagspräsident a. D.

„Geipel aber verknüpft die eigene Familiengeschichte so gekonnt und kühl mit der Geschichte der DDR, wechselt derart einleuchtend zwischen intimen Mikro- und historischen Makrokosmos hin und her, dass daraus ein beeindruckendes Buch über die jahrzehntelange Mehrfachvergletscherung einer Gesellschaft wurde.“ Alex Rühle, Süddeutsche Zeitung, 01.03.2019

***

Ines Geipel, geboren 1960, ist Schriftstellerin und Professorin für Verssprache an der Berliner Hochschule für Schauspielkunst »Ernst Busch«. Die ehemalige Weltklasse-Sprinterin floh 1989 nach ihrem Germanistik-Studium aus Jena nach Westdeutschland und studierte in Darmstadt Philosophie und Soziologie.
2000 war sie Nebenklägerin im Prozess gegen die Drahtzieher des DDR-Zwangsdopings. Ihr Buch „Verlorene Spiele“ (2001) hat wesentlich dazu beigetragen, dass die Bundesregierung einen Entschädigungs-Fonds für DDR-Dopinggeschädigte einrichtete. 2005 gab Ines Geipel ihren Staffelweltrekord zurück, weil er unter unfreiwilliger Einbindung ins DDR-Zwangsdoping zustande gekommen war.
Ines Geipel hat neben Doping auch vielfach zu anderen gesellschaftlichen Themen wie Amok, der Geschichte des Ostens und auch zu Nachwendethemen publiziert.