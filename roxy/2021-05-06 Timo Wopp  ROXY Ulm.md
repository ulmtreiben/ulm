---
id: "2484778941734732"
title: Timo Wopp * ROXY Ulm
start: 2021-05-06 20:00
end: 2021-05-06 23:00
address: ROXY.ulm
link: https://www.facebook.com/events/2484778941734732/
image: 87320161_10157267769122756_1977730125097074688_o.jpg
teaser: ULTIMO (Die Jubiläumstour)  Nach zehn Jahren körperbetonter Kabarettarbeit
  zieht Timo Wopp in die vorerst letzte Schlacht um seine humoristische Dasei
isCrawled: true
---
ULTIMO (Die Jubiläumstour)

Nach zehn Jahren körperbetonter Kabarettarbeit zieht Timo Wopp in die vorerst letzte Schlacht um seine humoristische Daseinsberechtigung. ULTIMO ist nicht nur eine auf die Bühne gebrachte Work-Hard-Play-Hard-Show, sondern auch ein tiefes Eintauchen in seine bisherigen Programme „Passion – Wer lachen will muss leiden“, „Moral – Eine Laune der Kultur“ und „Auf der Suche nach dem verlorenen Witz“. Der Geisterfahrer auf deutschen Humorautobahnen, der Businesskasper der Comedy, der Jonglierheinz des Kabaretts wird sein Publikum in Grund und Boden und sich selbst um Kopf und Kragen coachen, nur um sich kurz vor knapp am eigenen Schopf aus dem Sumpf zu ziehen, den er sich selbst zuvor geschaffen hat. Als letztes Mittel der Wahl wird er sicherlich auch wieder kräftig was in die Luft werfen. Ist ja schließlich Jubiläumstour.

***

Timo Wopp hat Betriebswirtschaftslehre studiert und war als Unternehmensberater tätig. Parallel zum Studium arbeitete er als professioneller Jongleur auf deutschen und internationalen Varietébühnen. 2007 war er der erste deutsche Jongleur mit einem Solovertrag beim Cirque du Soleil. Schließlich entdeckte er sein kabarettistisches Talent und ist mittlerweile als vielfach preisgekrönter Kabarettist Stammgast im TV, u.a. bei: „Die Anstalt“ / ZDF, „Pufpaffs Happy Hour“ /3sat, „Schlachthof“ / BR, u.v.m.