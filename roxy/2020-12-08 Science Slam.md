---
id: "380615599999326"
title: Science Slam - Verschoben auf 08.06.2021
start: 2021-06-08 20:00
end: 2021-06-08 23:00
address: ROXY.ulm
link: https://www.facebook.com/events/380615599999326/
image: 120118673_10157932345352756_167724778261343252_n.jpg
teaser: Der Slam musste vom 02.03.2021 auf den 08.06.2021 verschoben werden. Tickets
  behalten ihre Gültigkeit oder können an den jeweiligen VVK-Stellen zurück
isCrawled: true
---
Der Slam musste vom 02.03.2021 auf den 08.06.2021 verschoben werden. Tickets behalten ihre Gültigkeit oder können an den jeweiligen VVK-Stellen zurückgegeben werden.
***
Das Prinzip ist einfach: Jeder Slammer hat 10 Minuten Zeit, ein an sich wissenschaftliches, komplexes Thema seiner Wahl einem breiten Publikum verständlich zu machen. Egal ob Schüler, Student, Lehrer, Wissenschaftler oder Laie mit speziellen Fachkenntnissen: jeder darf auf die Bühne. Im Anschluss wird der Vortrag vom Publikum bewertet. Das Publikum bildet die Jury und bestimmt, wer am Ende des Abends zum Science-Slam-Sieger gekürt wird.

Moderiert von Dana Hoffmann

Du hast selbst ein wissenschaftliches Thema, das du unserem Publikum näher bringen willst? Dann melde dich an und nutze deine 10 Minuten! scienceslam@roxy.ulm.de

Danke an scanplus für die Unterstützung!