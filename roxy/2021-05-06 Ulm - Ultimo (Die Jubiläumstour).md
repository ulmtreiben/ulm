---
id: "315480163190533"
title: Ulm - Ultimo (Die Jubiläumstour)
start: 2021-05-06 20:00
end: 2021-05-06 23:00
address: ROXY.ulm
link: https://www.facebook.com/events/315480163190533/
image: 109565378_10157192859996630_3660413154335253978_n.jpg
teaser: ULTIMO ist nicht nur eine auf die Bühne gebrachte Work-Hard-Play-Hard-Show,
  sondern auch ein tiefes Eintauchen in seine bisherigen Programme. Der Geis
isCrawled: true
---
ULTIMO ist nicht nur eine auf die Bühne gebrachte Work-Hard-Play-Hard-Show, sondern auch ein tiefes Eintauchen in seine bisherigen Programme. Der Geisterfahrer auf deutschen Humorautobahnen wird sein Publikum in Grund und Boden und sich sich selbst um Kopf und Kragen coachen. 