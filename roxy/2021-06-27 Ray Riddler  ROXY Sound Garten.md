---
id: "532273144802697"
title: Ray Riddler * ROXY Sound Garten
start: 2021-06-27 18:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/532273144802697/
image: 204371473_10158606698717756_6979398813973481650_n.jpg
isCrawled: true
---
Wir freuen uns, euch in unserem Sound Garten begrüßen zu dürfen. Neben dem regulären Biergartenbetrieb versüßen euch unterschiedliche DJs die Ohren.

Eintritt frei, Spende für die musikalische Unterhaltung wird gern entgegen genommen.

Öffnungszeiten:
Mittwoch - Samstag: Ab 17:00 H
Sonntag: Ab 15:00 H