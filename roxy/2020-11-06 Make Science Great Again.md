---
id: "1454036771432887"
title: Make Science Great Again
start: 2020-11-06 20:00
end: 2020-11-06 23:00
address: ROXY.ulm
link: https://www.facebook.com/events/1454036771432887/
teaser: 90 Prozent der Amerikaner halten die Evolutionstheorie für unbewiesenen
  Mumpitz. 34   Prozent bezweifeln, dass die Erde eine Kugel ist und sieben Proz
isCrawled: true
---
90 Prozent der Amerikaner halten die Evolutionstheorie für unbewiesenen Mumpitz. 34
  Prozent bezweifeln, dass die Erde eine Kugel ist und sieben Prozent glauben, dass braune
  Kühe Schokomilch geben.
  Alles Fake News aus dem Land der unbegrenzten Möglichkeiten? Vince Ebert wollte es
  genau wissen und startete ein spektakuläres Experiment: Ein ganzes Jahr in den USA!
  Er traf auf IT-Spezialisten aus dem Silicon Valley und in New York auf Wallstreet-Banker. In
  Cape Canaveral sprach er mit Raketenwissenschaftlern und in Harvard mit GenderforscherUnterstrich-Studenten-Unterstrich-Sternchen-Innen.
  Stets auf der Suche nach den elementarsten Fragen: Wie kann es sein, dass eine Nation, die
  zum Mond flog, nicht in der Lage ist, eine funktionsfähige Duscharmatur herzustellen?
  Woher kommt die uramerikanische Angst vor unpasteuristierter Milch? Und kann man
  wirklich vom Tellerwäscher zum Millionär werden? Oder doch nur zum Geschirrspüler?
  Amerika - ein Land, das polarisiert und irritiert. Dort gibt es schwarz und weiß, Liebe und
  Hass, Freiheit und Todesstrafe, Prüderie und Brustvergrößerungen, Eliteunis und Käse in
  Sprühdosen.
  Amerikanische Schüler halten Hitler für eine Staubsaugermarke und glauben, die Ukraine
  liegt in Portugal. In Montana kommen auf jeden Einwohner drei Kühe, jeder achte
  Amerikaner hat schon einmal bei MacDonalds gejobbt und ein Viertel glaubt an
  Wiedergeburt. Trotzdem sind die USA die führende Wissenschaftsnation der Welt. Oder
  vielleicht sogar deshalb?
  Ab Herbst 2020 ist Vince Ebert zurück auf Deutschlands Bühnen. Mit vielen Antworten,
  Einsichten und einem brandneuen Programm: Make Science Great Again! – eine witzige und
  kulturübergreifende Abrechnung mit Irrationalität, Denkfehlern und gegenseitigem
  Überlegenheitsgefühl.