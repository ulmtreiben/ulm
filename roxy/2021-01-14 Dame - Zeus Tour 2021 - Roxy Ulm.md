---
id: "213761703368617"
title: "Abgesagt: Dame - Zeus Tour 2021 - Roxy Ulm"
start: 2021-01-14 19:00
end: 2021-01-14 23:00
address: ROXY.ulm
link: https://www.facebook.com/events/213761703368617/
image: 90565342_2877403559005662_8441639404312723456_o.jpg
teaser: Liebe DameArmy,  Ich hoffe, es geht Euch allen den Umständen entsprechend gut
  und Ihr seid wohlauf und gesund! Leider müssen wir aufgrund der anhalten
isCrawled: true
---
Liebe DameArmy,

Ich hoffe, es geht Euch allen den Umständen entsprechend gut und Ihr seid wohlauf und gesund!
Leider müssen wir aufgrund der anhaltenden Auflagen nicht ganz so einfache Entscheidungen treffen und haben uns nun dazu entschieden nach vorne zu blicken - um das Pflaster schnell abzuziehen: Die Ersatztermine der „ZEUS“ Tour im Jänner 2021 werden abgesagt und nicht nachgeholt, aber keine Sorge - es gibt natürlich auch eine gute Nachricht:

2021 feiern wir das 10jährige Jubiläum meiner ersten Youtube Uploads und somit quasi auch den Start meiner Musikkarriere. Aus diesem Grund haben wir uns dazu entschieden mit der „Auf die guten alten Zeiten“ Anniversary Tour 2021 die bisher größte und längste Tour meiner Karriere zu spielen. Wir freuen uns schon darauf mit Euch im September und November die Konzerthallen abzureissen!

Aus logistischen Gründen können die Tickets der „ZEUS“ Tour leider nicht auf die AnniversaryTour umgelegt werden, aber wir haben mit allen Veranstaltern gesprochen und Ihr bekommt bei den jeweiligen VVK Stellen natürlich Euer Geld zurück. Auch im Falle einer Verschiebung der Anniversary Tour verzichten unsere Partner auf Gutscheinlösungen und die Tickets behalten ihre Gültigkeit für die Ersatztermine bzw. können auch zurückgegeben werden.

Die Termine werden diesen Freitag um 15:00 bekanntgegeben. Tickets bekommt Ihr dann wie gewohnt unter https://tickets.damestream.at

Wir freuen uns schon darauf Euch endlich wiederzusehen!
In diesem Sinne: AH UH!