---
id: "970276950074959"
title: ROXY Sound Garten mit DJ hoerbeispiel und DJ bl
start: 2020-07-28 18:00
end: 2020-07-29 22:00
address: ROXY.ulm
link: https://www.facebook.com/events/970276950074959/
teaser: Wir freuen uns, euch in unserem Sound Garten begrüßen zu dürfen. Neben dem
  regulären Biergartenbetrieb versüßen euch unterschiedliche DJs die Ohren.
isCrawled: true
---
Wir freuen uns, euch in unserem Sound Garten begrüßen zu dürfen. Neben dem regulären Biergartenbetrieb versüßen euch unterschiedliche DJs die Ohren.

HEUTE (ab 18:00): DJ hoerbeispiel und DJ bl
Musik an einem Sommerabend.

Eintritt frei, Spende für die musikalische Unterhaltung wird gern entgegen genommen.

Der Biergarten ist ab 15:00 H geöffnet.

***
Alle aktuellen Bestimmungen bzgl. Corona findet ihr auf unserer Homepage www.roxy.ulm.de