---
id: "2682148031805568"
title: Open Stage
start: 2020-11-16 20:00
end: 2020-11-16 23:00
address: ROXY.ulm
link: https://www.facebook.com/events/2682148031805568/
image: 73333343_10156898438342756_3610321900967821312_o.jpg
teaser: Hier kann man einmal im Monat aufstrebende Talente, charmante Dilettanten und
  unverbrauchte Profis beim Schwitzen, Zittern, Triumphieren, aber auch be
isCrawled: true
---
Hier kann man einmal im Monat aufstrebende Talente, charmante Dilettanten und unverbrauchte Profis beim Schwitzen, Zittern, Triumphieren, aber auch beim Abstürzen erleben — ein Schauplatz kultiger Auftritte von Darstellern aus den Bereichen Kabarett, Comedy, Musik und Tanz, die sich berufen fühlen, sich der Gunst des Publikums zu stellen. Möglich ist (fast) alles! Unterhaltsam und spannend ist es immer!

Moderation: Matthias Matuschik

Mit freundlicher Unterstützung von
EDAG
Transporeon