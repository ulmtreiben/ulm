---
id: "822195018477793"
title: '"Wabi-Sabi" & "REVA" (Weltpremiere) * Sofia Nappi * ROXY Ulm'
start: 2021-10-04 20:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/822195018477793/
image: 243537786_10158815341692756_7685360319864220283_n.jpg
isCrawled: true
---
"REVA" (Weltpremiere)

REVA, aus dem Hindi: "Kraft tanken", "Regen", stellt die Frage: Was wäre, wenn du morgen aufwachst und nichts mehr von dem hast, was wir im Alltag oft als selbstverständlich ansehen? Heutzutage sind wir oft durch unsere Interpretation der Vergangenheit und unsere Erwartungen an die Zukunft so konditioniert, dass wir in einem ständigen Überlebensmodus leben und verzweifelt versuchen, einen Sinn und ein Ziel für unser begrenztes Leben zu finden. Wenn du plötzlich nackt dastehen würdest, ohne etwas zu verlieren, würdest du dich wahrscheinlich stattdessen dem universellen Tanz des Lebens und der bedingungslosen Liebe des gegenwärtigen Augenblicks anschließen. Es ist, wie wenn man in den Regen gerät: Entweder versucht man vergeblich davor wegzulaufen und nicht nass zu werden oder man bleibt stehen und entscheidet sich bewusst dafür, den Regen zu spüren und so eine Verbindung mit dem Universum einzugehen.

Choreografie: Sofia Nappi
Tänzer: Paolo Piancastelli
Kostüme: Sofia Nappi
Licht: Sofia Nappi 

Produktion: Sosta Palmizi mit KOMOCO/ Sofia Nappi
Koproduktion: TanzLabor - ROXY Ulm

Mit der Unterstützung von Opus Ballet, DeDa Productions, Teatro Affratellamento
Dauer: 20 min.

 

"Wabi-Sabi"

Wabi-Sabi, aus dem Japanischen stammend, ist ein ästhetisches Konzept, eine Weltanschauung, die sich auf die Akzeptanz der Vergänglichkeit konzentriert und Schönheit im "Unvollkommenen, Unbeständigen und Unvollständigen" unseres Lebens findet. Wabi-Sabi erforscht unsere Lebensreise als Individuen, die sich meist in ständiger Unruhe und verschiedenen Zuständen der Trauer bewegen, und schlägt ein tieferes Verständnis für unsere Existenz vor. Die Akzeptanz des Wesens unserer Natur und der Schönheit, die in ihrer offensichtlichen Unvollkommenheit zu finden ist, bringt ständiges Wachstum, Erneuerung und Freude durch den Prozess, indem wir uns mit ihr vorwärts bewegen und nicht von ihr weg.

Choreografie: Sofia Nappi
Tänzer:innen: Sofia Nappi, Adriano Popolo Rubbio, Paolo Piancastelli
Kostüme: Sofia Nappi
Lichtregie: Emiliano Minoccheri
Produktion: Sosta Palmizi mit KOMOCO / Sofia Nappi

Mit der Unterstützung von New Master Ballet mit der Stadtverwaltung von Sestri Levante, KOMMTANZ/Passo Nord Residencies Abbondanza/Bertoni Company in Zusammenarbeit mit der Stadtverwaltung von Rovereto.

Auszeichnungen: Besondere Erwähnung für den Theodor Rawyler Preis 2020
Dauer: 20 min.

***
Gefördert von der Beauftragten der Bundesregierung für Kultur und Medien.

www.sofianappi.com

Veranstalter: ROXY gemeinnützige GmbH