---
id: "2477665785604068"
title: Hazel Brugger - Kennen Sie diese Frau? * Neu-Ulm
start: 2021-10-07 20:00
end: 2021-10-07 23:00
locationName: ratiopharm arena
address: Europastr. 25, 89231 Neu-Ulm
link: https://www.facebook.com/events/2477665785604068/
image: 72578362_10156859013927756_4330583539935346688_n.jpg
isCrawled: true
---
Die Veranstaltung wurde vom 02.10.2020 auf den 07.10.2021 verschoben. Tickets behalten ihre Gültigkeit.
***
Hazel Brugger ist nicht nur bekannt geworden, sondern auch erwachsen: Mittlerweile ist Deutschlands „Beste Komikerin 2020“ Hausbesitzerin, Ehefrau und Mutter. Aber der Lockdown hat auch Hazel nicht völlig kalt gelassen und so muss sich die deutsch- amerikanische Schweizerin im Chaos zwischen YouTube, Shitstorms, eigener Firma, älter werdenden Eltern und bankrotten Künstlerkolleg*innen immer wieder die Frage stellen: Was will ich eigentlich wirklich – und inwiefern ist das weiblich? 

Einlass: 18:30 H
Veranstalter: ROXY.ulm