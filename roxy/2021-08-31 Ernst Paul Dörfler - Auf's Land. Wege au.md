---
id: "366145528412397"
title: Ernst Paul Dörfler - Auf's Land. Wege aus der Klimakrise, Monokultur und
  Konsumzwang *Ulmer Leseorte
start: 2021-08-31 20:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/366145528412397/
image: 235304567_10158733159997756_5189994988190658606_n.jpg
isCrawled: true
---
Ulmer Leseorte - Wo Geschichten lebendig werden
Treffpunkt: ROXY 

Das Landleben als Chance für Klima und Umwelt? Inspirierende Perspektiven auf Nachhaltigkeit und Selbstversorgung von Ökologe Ernst Paul Dörfler

Wir haben den Blick für das Wesentliche verloren: unser Wohlergehen und das der Natur. Wir leben in engen Städten. Wir arbeiten viel, um immer mehr zu konsumieren. Leidenschaftlich und kompetent ruft der Ökologe Ernst Paul Dörfler dazu auf, endlich auszubrechen und nachhaltige Lösungen zu finden. Der Weg dorthin führt aufs Land. Als unbequemer Umweltschützer schon in der DDR vermittelt er glaubhaft wie kein Zweiter, was freies und selbstbestimmtes Leben bedeutet und wie es gehen kann. Wer weniger braucht, muss weniger arbeiten und verdienen, schont zugleich die natürlichen Lebensgrundlagen, lebt zufriedener und gesünder. Ob Stadt- oder Landmensch, dieses Buch rüttelt auf und zeigt Perspektiven für ein freies, umwelt- und klimafreundliches Leben.

***

Ulmer Leseorte - Alle Termine

29.08.2021 | 20:00 H | Me and all Hotel
Bernd Gieseking

31.08.2021 | 20:00 H | Jugendfarm Ulm 
Ernst Paul Dörfler

15.09. | 20:00 H | Club Action
Fabian Sommavilla

16.09. | 20:00 H | Cabaret Eden
Katapult-Verlag

21.09.2021 | 20:00 H | Donaustadion Ulm
Bernd Beyer

24.09.2021 | 20:00 H | Kinderladen Ulm
Sarah Straub

26.09.2021 | 16:00 H | Heyoka Zaubergarten
Patrick Salmen

***

Die Veranstaltungsreihe "Ulmer Leseorte" wird im Programm Kultursommer 2021 durch die Beauftrage der Bundesregierung für Kultur und Medien (BKM) mit Mitteln aus NEUSTART KULTUR gefördert.

Der Ulmer Kultursommer ist eine Initiative der Kulturabteilung der Stadt Ulm in Zusammenarbeit mit der Ulmer Kulturszene. Die einzelnen Programmpunkte werden von Gleis 44, ROXY Ulm oder HfG Archiv veranstaltet. Alle Beteiligten bedanken sich bei den Stadtwerke Ulm / Neu-Ulm und bei der Kulturstiftung des Bundes für die freundliche Unterstützung.
***
https://www.roxy.ulm.de/programm/programm.php?m=8&j=2021&vid=3818