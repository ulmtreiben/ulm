---
name: ROXY
website: http://www.roxy.ulm.de
email: info@roxy.ulm.de
scrape:
  source: facebook
  options:
    page_id: roxy.kultur
---
Soziokulturelles Zentrum mit Schwerpunkt auf Veranstaltungen.
Musik, Theater, Tanz, Kunst, Literatur, Forum für Debatten und Diskussionen, Treffpunkt für unterschiedliche gesellschaftliche Gruppen, Plattform für Firmen und Institutionen
