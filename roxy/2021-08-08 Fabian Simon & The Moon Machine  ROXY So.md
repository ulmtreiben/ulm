---
id: "320487153033435"
title: Fabian Simon & The Moon Machine * ROXY Sound Garten
start: 2021-08-08 20:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/320487153033435/
image: 201885115_10158585221242756_6746053562597792683_n.jpg
isCrawled: true
---
LIVEMUSIK UNTER FREIEM HIMMEL
EINTRITT FREI! 💛
BEGINN: 20.00 Uhr

Bitte kommt geimpft oder genesen oder getestet und mit einem entsprechenden Nachweis!

Was passiert, wenn sich die Harmonieverliebtheit eines Elliott Smith mit der Lust des Tropicalismo am klanglichen Wahnsinn verbindet und dann im mystischen Zwielicht von Twin Peaks erscheint? Voilà: Das ungefähr best-behütete Geheimnis der deutschen Indieszene, Fabian Simon & The Moon Machine.
Das Trio um Tausendsassa Fabian Simon nennt seinen Stil augenzwinkernd „utopistischen Kraut-Folk" und genau so klingt es auch: Wie eine wilde Mélange von 60's Psychedelia, klassischer Kammermusik und Songwritertum von Nick Drake bis Syd Barrett. Spacige Synths rangeln sich mit Wüsten-Gitarren während Schweine-Orgeln auf rumpelnde Surf-Drums treffen und zum beschwipsten Walzer bitten. Feinsinnig, witzig, poetisch und immer wieder überraschend. 
Getragen wird diese rätselhafte Mixtur von Fabian Simons Baritonstimme. Sie erzählt mal mit brüchigem Timbre, mal mit impulsiver Emotion in schönen schiefen Bildern von Liebe, Tod und dem absurden Dazwischen. Live wird die Band durch filigrane Arrangements, außergewöhnliche Sounds und eine spürbare Freude am Experiment zu einem ebenso berührenden wie anarchischen Erlebnis.
