---
id: "277975146722169"
title: "Tanzcompagnie/Stadttheater Gießen: Auftaucher"
start: 2021-06-26 20:00
end: 2021-06-26 21:30
address: ROXY.ulm
link: https://www.facebook.com/events/277975146722169/
image: 101647585_2868081343241360_9040605188246732800_o.jpg
teaser: Ein leerer Bühnenraum, zehn Tänzerinnen und Tänzer, acht Stühle und zwanzig
  Rasseln – mehr braucht es in AUFTAUCHER nicht, um die Vorstellungskraft de
isCrawled: true
---
Ein leerer Bühnenraum, zehn Tänzerinnen und Tänzer, acht Stühle und zwanzig Rasseln – mehr braucht es in AUFTAUCHER nicht, um die Vorstellungskraft der Zuschauerinnen und Zuschauer herauszufordern. Innerhalb weniger Minuten entsteht im Zusammenspiel von Bewegung, Licht und Rhythmus ein atmosphärischer Sog, der alle in seinen Bann zieht. Es ist ein Feuerwerk an Bewegungen und Ausdrucksstärke.
Choreografin Henrietta Horn setzt emotionale Vorgänge in physische Bilder um. Spielarten des menschlichen Miteinanders finden sich so auf der Bühne wieder: das Umwerben der Geschlechter, Konkurrenz, Ablehnung, Eifersucht, aber auch Humor, Scham, Sinnlichkeit und Lebenslust. In seiner jetzigen Form wurde das Stück 2001 vom Folkwang Tanzstudio in Jakarta uraufgeführt und international gezeigt. Nach Neueinstudierungen in Essen (2013) und Braunschweig (2016) jetzt mit der Tanzcompagnie Gießen: fast schon ein ›Klassiker des Zeitgenössischen Tanzes‹.

https://ulmmoves.de/programm/stadttheater-giessen/ 