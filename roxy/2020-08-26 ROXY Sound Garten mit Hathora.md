---
id: "624159125206102"
title: ROXY Sound Garten mit Hathora
start: 2020-08-26 18:00
end: 2020-08-28 22:00
address: ROXY.ulm
link: https://www.facebook.com/events/624159125206102/
teaser: Wir freuen uns, euch in unserem Sound Garten begrüßen zu dürfen. Neben dem
  regulären Biergartenbetrieb versüßen euch unterschiedliche DJs die Ohren.
isCrawled: true
---
Wir freuen uns, euch in unserem Sound Garten begrüßen zu dürfen. Neben dem regulären Biergartenbetrieb versüßen euch unterschiedliche DJs die Ohren.

HEUTE (ab 18:00): Hathora
und wenn einem der Alltag aus den Ohren wächst, dann wird es Zeit für eine musikalische Wäsche für die Sinne.
Auf dem Programm stehen verträumte Electro Beats mit einer Geheimrezeptur an Feurigen Klängen für die Ohren und die Seele.
Hathora freut sich auf einen gemeinsamen Abend mit euch!

Eintritt frei, Spende für die musikalische Unterhaltung wird gern entgegen genommen.

Der Biergarten ist ab 15:00 H geöffnet.

***
Alle aktuellen Bestimmungen bzgl. Corona findet ihr auf unserer Homepage www.roxy.ulm.de