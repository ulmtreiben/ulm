---
id: "2849017638756213"
title: Rococons - Ein Bericht über Marie A. * ROXY Ulm
start: 2021-01-21 20:00
address: ROXY.ulm
link: https://www.facebook.com/events/2849017638756213/
image: 130605507_10158120337357756_3367357096519241762_o.jpg
teaser: „Mein Herr, ich bitte Sie um Verzeihung, ich tat es nicht mit Absicht.“  Dem
  Stück Rococons / Ein Bericht über Marie A. liegt das Buch „Marie Antoinet
isCrawled: true
---
„Mein Herr, ich bitte Sie um Verzeihung, ich tat es nicht mit Absicht.“

Dem Stück Rococons / Ein Bericht über Marie A. liegt das Buch „Marie Antoinette: Bildnis eines mittleren Charakters“ von Stefan Zweig zugrunde. Es wird darin die Entwicklung einer kindlich unwissenden Persönlichkeit erzählt, die rigoros den Blick nach außen verweigert. Eine Frau „ohne besonderer Kraft zum Guten und ohne den geringsten Willen zum Bösen“ (Zweig), die erst im Sturm der französischen Revolution Umstände findet, um persönlich wachsen und Größe gewinnen zu können. Ein menschliches Dilemma, das die Zeiten überdauert; die Bastion Versailles als die Bastion Europa. Wie viel Verantwortung tragen wir für eine Welt, deren Elendhaftigkeit wir doch stets mitverursachen? Wie viel Schuld laden wir durch Desinteresse auf uns? Wie viel Handlungsspielraum hat der Mensch? Marie Nüzels Projekte bewegen sich an der Schnittstelle zwischen Performance, Bewegung und Dokumentation. In ihrem Stück Rococons verschränkt sie eine Auseinandersetzung mit Musik und Tanz des „le siècle des petitesses“ mit aktuellen Fragen nach Macht und Ohnmacht, individueller Reife und kollektiver Verantwortung.

Choreografie: Marie Nüzel
Künstlerische Assistenz: Rose Fock
Ausstattung: Luzia Ehrmann
Performance: Louisa Difliff, Paula Dominguez, Kathrin Knöpfle, Marie Nüzel
Produktion: Christel Salem
Licht und technischer Support: Andi Kern  

Mit freundlicher Unterstützung des Kulturreferats der LH München und dem HochX Theater und Live Art