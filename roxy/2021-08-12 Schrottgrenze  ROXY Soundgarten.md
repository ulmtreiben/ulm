---
id: "390192912235342"
title: Schrottgrenze * ROXY Soundgarten
start: 2021-08-12 20:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/390192912235342/
image: 193188947_10158544147292756_6852986444203790511_n.jpg
isCrawled: true
---
LIVEMUSIK UNTER FREIEM HIMMEL
EINTRITT FREI! 💛

Zwischen Rockfestivalbühnen und Dragshow. Zwischen Punkrockgitarren und eingängigen Popmelodien. Zwischen kleinen Alltagsgeschichten und unmissverständlichen Statements. Überall bewegt sich die Hamburger Band Schrottgrenze, auch mit ihrem neuen Album Alles zerpflücken: „Somewhere in Between.“ 
Was wird hier zerpflückt? In erster Linie die verdammte Heteronormativität. Hier werden von Anfang an keine Gefangenen gemacht und Sänger Alexander Tsitsigias stellt sich gleich als „kräftige Schwester“ Saskia Lavaux in einem politischen Körper vor. 
Rückblick: Mit dem viel beachteten Schrottgrenze-Comeback-Album Glitzer auf Beton öffnete die bereits 1994 gegründete Band 2017 ein neues Kapitel. Denn es war nicht nur ein Comeback nach sieben Jahren Pause, sondern auch das erste Album nach dem späten Coming Out von Sänger Alex. Von nun an bearbeiteten Schrottgrenze ein überraschend unbearbeitetes Feld in der Indieszene: Queere Themen. 
Alles zerpflücken setzt genau dort an, wo der Vorgänger aufgehört hat – und geht dabei noch einen bedeutenden Schritt weiter. Denn Alles zerpflücken ist einen Schritt weiter und will Fragen aufwerfen. Dekonstruieren, oder Zerpflücken eben. Fragen gestellt werden nach kritischer Männlichkeit, Geschlechterrollen, Freiräumen, Rassismus und Antisemitismus. So heißt es in Traurige Träume: „Wie der Humanismus hier zerfällt/Und wenn diese Welt /Wirklich zerfällt/hilft dir auch kein Beten und kein nach unten treten.“ Keine Frage, Alles zerpflücken ist das bisher politischste und wütendste Album der Band. Klare Standpunkte ohne einfache Parolen. 
