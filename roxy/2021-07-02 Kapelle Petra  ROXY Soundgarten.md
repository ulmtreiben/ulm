---
id: "5929905573716674"
title: Kapelle Petra * ROXY Soundgarten
start: 2021-07-02 20:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/5929905573716674/
image: 191631666_10158535076717756_155150955020088238_n.jpg
isCrawled: true
---
LIVEMUSIK UNTER FREIEM HIMMEL
EINTRITT FREI! 💛

Fast wie Vivaldi: Kapelle Petra und „Die Vier Jahreszeiten“ 
Das Trio um Sänger Opa, Bassist Der Tägliche Siepe und Schlagzeuger Ficken Schmidt (das live um die Bühnenskulptur Gazelle zum Quartett erweitert wird), ist umtriebig und kreativ wie nur wenige andere hiesige Acts. Und so kommen auch “Die Vier Jahreszeiten” zustande. 
Ein Konzept, das sich musikalisch, inhaltlich und auch hinsichtlich der Veröffentlichungs- Zyklen im weitesten Sinne mit Frühling, Sommer, Herbst und Winter befasst. Jeweils zum Beginn der Saison wird - startend mit “Der Frühling” am 19. März - eine EP mit jeweils vier Songs erscheinen. 
Dabei gelingt Kapelle Petra das Kunststück, die Songs nicht zu monothematisch auszugestalten, beim Durchhören staunt man nicht schlecht, wie variantenreich und intelligent die Band mit dieser Grundidee spielt. Gibt auf “Der Frühling” ein Song wie “Reißt die Fenster auf” noch ziemlich explizit Hinweis auf die saisonale Tonalität, ist bei Stücken wie “Wirtschaftsflüchtling” oder “Meine Zeit” ein bisschen mehr Transferleistung nötig, der Bezug erschließt sich eher zwischen den Zeilen: 
„Im 4-Jahreszeitenkontext wollten wir textlich auf keinen Fall bloß das Wetter- und die quartalstypischen Baumzyklen eines Jahres beschreiben“, betont der für die Texte verantwortlich zeichnende Opa. „Vor allem ging es um persönliche Momentaufnahmen, die tatsächlich jahreszeitlich bedingt sind. Wir haben die Songs dann auch bewusst in der jeweiligen Jahreszeit geschrieben und aufgenommen.“ 
Auch Sommer, Herbst und Winter warten mit entsprechendem textlichen und musikalischen Facettenreichtum auf. Und so gelingt Kapelle Petra hier ein Konzept, das liebevoll und gleichzeitig frei interpretiert ist, auf dem man die Kompositions-und Spielfreude der Band aber förmlich hören kann. 
