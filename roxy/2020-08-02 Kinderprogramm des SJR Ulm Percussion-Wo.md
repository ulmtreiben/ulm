---
id: "633328577276699"
title: "Kinderprogramm des SJR Ulm: Percussion-Workshop"
start: 2020-08-02 15:00
end: 2020-08-02 17:00
address: ROXY.ulm
link: https://www.facebook.com/events/633328577276699/
image: 107086738_10157712811432756_7820416323093793861_o.jpg
teaser: Lust auf Trommeln? Innerhalb von zwei Stunden haben Musikbegeisterte die
  Möglichkeit, verschiedene Rhythmen auf Cajon und Percussion Instrumenten zu e
isCrawled: true
---
Lust auf Trommeln? Innerhalb von zwei Stunden haben Musikbegeisterte die Möglichkeit, verschiedene Rhythmen auf Cajon und Percussion Instrumenten zu erlernen.

Maximale Teilnehmerzahl: 6
15:00-17:00 H

Das Angebot ist kostenlos (und findet unter der Berücksichtigung der derzeitigen Hygiene- und Sicherheitsmaßnahmen statt). Wer mag, kann sich gerne im Voraus mit Name, Adresse und Telefonnummer unter knehr@sjr-ulm.de anmelden.

Kooperation von Stadtjugendring e.V. und ROXY gGmbH