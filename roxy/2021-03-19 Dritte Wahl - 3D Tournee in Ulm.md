---
id: "1159504041071654"
title: Dritte Wahl - 3D Tournee in Ulm
start: 2021-03-19 20:00
end: 2021-03-19 23:00
address: ROXY.ulm
link: https://www.facebook.com/events/1159504041071654/
image: 110017242_10158740782363679_6194996793615006026_o.jpg
teaser: VVK START AB FREITAG, 24.7.2020, 10:00h exklusiv auf headlineconcerts.de
  (Orginal Hardtickets) und eventim.de  DRITTE WAHL - 3D TOURNEE 2021  Es wurde
isCrawled: true
---
VVK START AB FREITAG, 24.7.2020, 10:00h exklusiv auf headlineconcerts.de (Orginal Hardtickets) und eventim.de

DRITTE WAHL - 3D TOURNEE 2021

Es wurde gefeiert, aber wie!

Ein halbes Leben Bandgeschichte durfte man natürlich nicht spurlos an sich vorüber gehen lassen und so wurde aus der „Bergfest“-Tour der Dritten Wahl zum 30-jährigen Bestehen gleich noch der Endspurt für die nächsten 30 Jahre eingeläutet - in Form einer weiteren Tournee zusammen mit alten und neuen Freunden. 

Und jedes Mal war es ein Stück erfolgreicher, kamen noch mehr Zuschauer, die in Erinnerungen schwelgten und sich des Lebens freuten, welches diese Band schon zu einem guten Stück begleitet hat.

Nach 2 Jahren recht erfolgreicher Geburtstags-Tourneen erlebt die Rostocker Punkrock Institution Dritte Wahl nun so etwas wie ihren zweiten Frühling und macht sich daran nach vorne zu gehen: am 18.09.2020 erscheint endlich das neue, mittlerweile 11. Album der Band, „3D".

Wie immer geistreich und charmant, nie langweilig und verpackt in großartige Songwriter Kunst, darf man sich ganz sicher auf das neue Werk freuen. 
Und das wird natürlich gefeiert! Aber wie! Auf Tournee! Live! In 3D!