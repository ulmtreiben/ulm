---
id: "933077814202943"
title: TONI TORTELLINI - WELTPREMIERE
start: 2021-08-16 19:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/933077814202943/
image: 209537929_2903343639906647_8615348003979284240_n.jpg
isCrawled: true
---
Inspiriert von den Stream-Shows aus dem Hoftheater kamen Ariane Müller und Uli Boettcher im Sommer 2020 auf die Idee eine Live-Sitcom zu produzieren. Unterstützt von einem Crowdfunding der Fans sollte im Februar vor Publikum aufgezeichnet werden. Aufgrund des 2. Lockdowns musste das Publikum leider draußen bleiben, dafür hatten aber plötzlich die besten Komiker:innen des Landes Zeit. Und so konnte eine kleine Truppe im Quarantäne-Lager des leerstehenden Hoftheater Baienfurt Innerhalb einer Woche im März 6 Folgen abdrehen.

Ganz ohne lachendes Publikum musste nun aber ein kompletter Soundtrack in monatelanger Studioarbeit im Werkall Ulm produziert werden. Das Genre hat sich nun zur einer sehr musikalischen Comedy Soap gewandelt, und bereits zwei Singles des Soundtracks wurden noch im Lockdown veröffentlicht:

Toni Tortellini Theme - Roland Baisch feat. The Müller Sisters
Oben ohne in der Zone - Jäcky feat. Suchtpotenzial

Handlung: Der weltberühmte Jongleur Toni Tortellini kommt nach 25 Jahren aus Las Vegas zurück in sein Heimatdorf. Er möchte den Lockdown in der alten Heimat verbringen.

Doch dort ist nichts, wie er es erwartet. In der Wohnung seiner Mutter empfängt ihn seine seltsame Jugendfreundin Edeltraut, oben wohnt Jäcky, die immer zum Duschen runterkommt, das ganze Haus ist total marode und Justins Kneipe im Erdgeschoss hat auch schon bessere Zeiten gesehen.

Dann taucht noch seine Ex-Frau Ginger mit dem gemeinsamen Sohn Brüno auf.

Bald stellen sich Toni viele Fragen: Warum fährt der Hausmeister einen Porsche, wer ist Rainer Schäufele, was will der Zauberer Bruno Brasiliani? Und wo zum Teufel ist seine Mutter?

Pasta Production proudly presents

Toni Tortellini, eine Comedy Soap

mit

Uli Boettcher, Julia Gamez Martin, Brian Lausund, Dagmar Schönleber, Roland Baisch, Patrizia Moresco, Pablo Konrad, Heinrich Del Core, Helge Thun

Drehbuch, Regie, Musik: Ariane Müller
Schnitt: Andreas Schneider, Herrengedeck Filme
Postproduction Sound Mix, Music Production: Daniel Konold Werkall Studio Ulm

Nun kommt es zur großen Filmpremiere im ROXY.

Am 16.08. werden alle 6 Folgen zum ersten Mal gezeigt!

Aber wenn Komiker:innen eine Filmpremiere feiern, wird nicht mal die Feier ernstgenommen. Deshalb wird es ein Abend mit Glitzerkleid und Adiletten, es wird einen roten Teppich, eine Fotowand und alle Stars der Serie geben Autogramme!

The Müller Sisters werden live spielen, Roland Baisch und Julia Gámez Martín werden singen, es wird emotionale Reden, sehr viel Selbstbeweihräucherung, Prosecco und Popcorn geben.
Es wird eine Toni-Verleihung der Comedy!

EINLASS NUR FÜR GELADENE GÄSTE
Einlass ab 18:00 H

***

Die Veranstaltungsreihe "Digitale Kunst im Lockdown" wird im Programm Kultursommer 2021 durch die Beauftrage der Bundesregierung für Kultur und Medien (BKM) mit Mitteln aus NEUSTART KULTUR gefördert.

Der Ulmer Kultursommer ist eine Initiative der Kulturabteilung der Stadt Ulm in Zusammenarbeit mit der Ulmer Kulturszene. Die einzelnen Programmpunkte werden von Gleis 44, ROXY Ulm oder HfG Archiv veranstaltet. Alle Beteiligten bedanken sich bei den Stadtwerke Ulm / Neu-Ulm und bei der Kulturstiftung des Bundes für die freundliche Unterstützung.