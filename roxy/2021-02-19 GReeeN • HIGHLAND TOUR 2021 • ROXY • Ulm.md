---
id: "987589948345395"
title: GReeeN • HIGHLAND TOUR 2021 • ROXY • Ulm
start: 2021-02-19 18:00
end: 2021-02-19 23:00
address: Roxy ulm
link: https://www.facebook.com/events/987589948345395/
image: 109689649_4084122628296811_5515060773355419375_o.jpg
teaser: "Willkommen im Highland! GReeeN hat nicht lange auf sein neues Album (VÖ:
  28.08.2020) warten lassen und setzt mit „Highland“ einen neuen Meilenstein in"
isCrawled: true
---
Willkommen im Highland! GReeeN hat nicht lange auf sein neues Album (VÖ: 28.08.2020) warten lassen und setzt mit „Highland“ einen neuen Meilenstein in seiner Karriere.  Der Nachfolger vom "Smaragd" Album hat nun nicht weniger Hits in petto und bietet musikalisch die ideale Mischung aus Hip-Hop, Reggae und Pop.

Keine Frage das die passende Tour zum  GReeeN Sound nicht fehlen darf. Zusammen mir seiner „GReeeN Fam“  geht es auf die Reise ins „Highland“. Sein persönlicher Wohlfühl-Ort, wo er mit der Natur eins ist, der Glaube an sich selbst Berge versetzen kann und das Leben an sich mit einem unbeirrbaren positiven Blick beleuchtet wird. Seid dabei, es wird eine unvergessliche Reise.

Einlass: 18:00 Uhr
Beginn: 19:30 Uhr

Tickets unter: https://www.ibb-booking.com/greeen/