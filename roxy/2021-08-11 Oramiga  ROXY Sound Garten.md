---
id: "1156091294802415"
title: Oramiga & Klaus Fumy * ROXY Sound Garten
start: 2021-08-11 18:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/1156091294802415/
image: 229483928_10158710668487756_1013306042427559618_n.jpg
isCrawled: true
---
Wir freuen uns, euch in unserem Sound Garten begrüßen zu dürfen. Neben dem regulären Biergartenbetrieb versüßen euch unterschiedliche DJs die Ohren.

Diesen Mittwoch freuen wir uns auf ein Double Feature der DJs Oramiga und Klaus Fumy!

Eintritt frei, Spenden für die musikalische Unterhaltung wird gern entgegen genommen.

Öffnungszeiten:
Mittwoch - Samstag: ab 17:00 H
Sonntag: ab 15:00 H

Alle aktuellen Bestimmungen bzgl. Corona findet ihr auf unserer Homepage www.roxy.ulm.de
