---
id: "627603958203087"
title: Antistatik * ROXY Sound Garten
start: 2021-08-04 18:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/627603958203087/
image: 225918020_10158686243762756_3236731847399892995_n.jpg
isCrawled: true
---
Wir freuen uns, euch in unserem Sound Garten begrüßen zu dürfen. Neben dem regulären Biergartenbetrieb versüßen euch unterschiedliche DJs die Ohren.

Eintritt frei, Spenden für die musikalische Unterhaltung wird gern entgegen genommen.

Öffnungszeiten:
Mittwoch - Samstag: ab 17:00 H
Sonntag: ab 15:00 H

Alle aktuellen Bestimmungen bzgl. Corona findet ihr auf unserer Homepage www.roxy.ulm.de
