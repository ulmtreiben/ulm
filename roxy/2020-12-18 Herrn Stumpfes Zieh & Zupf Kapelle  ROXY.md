---
id: "667406967241341"
title: Herrn Stumpfes Zieh & Zupf Kapelle * ROXY Ulm
start: 2020-12-18 20:00
address: ROXY.ulm
link: https://www.facebook.com/events/667406967241341/
image: 122114022_10157998615032756_4738303250015046744_o.jpg
teaser: "Jippie-ja-je!  OBACHT: Skrupellose Hausmusik hier in der Nähe: Manne, Benny,
  Flex und Selle - HERRN STUMPFES ZIEH & ZUPF KAPELLE spielen musikalische"
isCrawled: true
---
Jippie-ja-je!

OBACHT: Skrupellose Hausmusik hier in der Nähe: Manne, Benny, Flex und Selle - HERRN STUMPFES ZIEH & ZUPF KAPELLE spielen musikalische Kracher, von denen sie zum Teil selbst bis vor kurzem noch gar nichts wussten. Selbst Wiederholungstäter (und es werden immer mehr) wissen nicht, was auf sie zukommt und Neulinge erst recht nicht. Aber soviel sei verraten: 
Astreiner Satzgesang mit Spitzenbegleitung anhand von etwa 20 Instrumenten, kurzweilig, meist schwäbisch dargeboten. Lieder aus dem schier unermesslichen Erfahrungsschatz der 4 Unterhaltungsminister. Nix wie no!
Ja, richtig gelesen: Man sollte sich sputen, wenn man die Stumpfes vor Ort sehen, hören und fühlen will.
Obwohl der kapelleneigene Fuhrpark natürlich bereits mit den streng geheimen - offiziell nicht erhältlichen - rosa Plaketten ausgestattet ist, bleibt die Zukunft dieser weltweit beliebtesten und erfolgreichsten aller Zieh & Zupf Kapellen wohl ungewiss.
Wird diese musikalisch einzigartige Karriere letztendlich durch Fahrverbote ein unrühmliches Ende nehmen? Oder werden die Musiker gar durch brand- und sicherheitsschutztechnische Auflagen an der Ausübung ihres Unterhaltungsauftrages gehindert werden? 
Und das, obwohl in allen Instrumenten bereits Brandmelder der neuesten Generation installiert wurden, die Bühnenkleidung und vorhandene Haarteile vorauseilend schon mit feuerresistenten Lotionen behandelt wurden?
Auch zündende Hits wurden vorsichtshalber umarrangiert - und unvermeidbare Zigaretten teilt man sich ja eh schon seit Jahren.
Also auf geht‘s: Unterstützt durch Kauf von Eintrittskarten eine Gegenbewegung! Bildet Fahrgemeinschaften! Wer nachweislich zu sechst im Auto kommt wird gewinnen. Und gerne aussteigen. Und wachsen. Zumindest innerlich.