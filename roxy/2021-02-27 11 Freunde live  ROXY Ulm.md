---
id: "846584132474254"
title: 11 Freunde live * ROXY Ulm
start: 2021-02-27 20:00
end: 2021-02-27 23:00
address: ROXY.ulm
link: https://www.facebook.com/events/846584132474254/
image: 87189617_10157258154932756_2042229220939137024_o.jpg
teaser: Köster & Kirschneck lesen vor und zeigen Filme  „Fußball ist nicht nur Ding!
  Fußball ist Ding Dang Dong!“ Schöner als Altmeister Trapattoni hat noch k
isCrawled: true
---
Köster & Kirschneck lesen vor und zeigen Filme

„Fußball ist nicht nur Ding! Fußball ist Ding Dang Dong!“ Schöner als Altmeister Trapattoni hat noch keiner formuliert, wie vielfältig, bunt und lustig Fußballsein kann. Und niemand bringt die komischen Seiten dieses Sports so temporeich und humorvoll auf die Bühne wie die Redakteure des preisgekrönten Fußballmagazins 11FREUNDE.
Chefredakteur Philipp Köster und Chef vom Dienst Jens Kirschneck lesen die besten Texte aus dem Magazin, erzählen aberwitzige Anekdoten aus der großen Fußballwelt und zeigen Filme von dreisten Schwalben und heillos versemmelten Interviews am Spielfeldrand. Das ist urkomisch und lässt die Presse urteilen: „Humorvoller Trip durch das Kuriositätenkabinett dieses Sports, den das Publikum mit geradezu lustvollen Lachern begleitet.“ (Osnabrücker Zeitung). Und der „Gießenener Anzeiger“ konstatiert: „Ein echtes Heimspiel! Mit ihrem Unterhaltungswert können nicht viele Bundesligapartien konkurrieren.”
Ab 2020 sind die beiden mit brandneuem Programm unterwegs. Sie feiern 20 Jahre 11FREUNDE mit bisher ungezeigten Videos, neuen Texten und unglaublichen Geschichten aus der Bundesliga. Da haben wir, um es mit Andi Möller zu sagen, „vom Feeling her ein gutes Gefühl“.

Jens Kirschneck, geboren 1966 in Minden. Journalist seit den frühen 90ern, zuerst bei der Wochenzeitung Bielefelder StadtBlatt, später für die Süddeutsche Zeitung, Frankfurter Rundschau und andere. Parallel langjährige Erfahrungen als Lesebühnenautor. Seit 2005 bei 11FREUNDE in Berlin, seitdem als „11 Freunde Lesereise“ regelmäßig mit Philipp Köster auf Tour.

Philipp Köster, geboren 1972 in Bobingen bei Augsburg. 1995 Mitbegründer des Arminia Bielefeld-Fanzines „Um halb vier war die Welt noch in Ordnung“, seit 2000 Chefredakteur des Fußballmagazins 11FREUNDE. Nebenher immer mal wieder Kolumnist, mal für den Tagesspiegel, Spiegel Online, mal für den RBB-Sender Radio Eins. Am 21. Dezember 2010 wurde Philipp Köster vom Medium Magazin zum „Sportjournalisten des Jahres 2010“ gewählt.