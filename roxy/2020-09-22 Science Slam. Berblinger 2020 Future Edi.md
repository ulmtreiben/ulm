---
id: "1059771454415500"
title: Science Slam. Berblinger 2020 Future Edition
start: 2020-09-22 20:00
end: 2020-09-22 22:30
address: ROXY.ulm
link: https://www.facebook.com/events/1059771454415500/
image: 83673513_10157205280137756_2834974248412381184_n.jpg
teaser: "Verschoben vom 19.05.2020 auf den 22.09.2020. Tickets behalten ihre
  Gültigkeit. *** Die Formel ist einfach: Science Slam + Berblinger 2020 =
  Future Sl"
isCrawled: true
---
Verschoben vom 19.05.2020 auf den 22.09.2020. Tickets behalten ihre Gültigkeit.
***
Die Formel ist einfach: Science Slam + Berblinger 2020 = Future Slam. Inmitten der Feierlichkeiten zum 250. Geburtstags des Ulmer Erfinders Albrecht Ludwig Berblinger dreht sich beim Science Slam alles um Zukunftsthemen. Der sogenannte Schneider von Ulm hätte damals sicherlich einiges zur Zukunft der Luftfahrt beitragen können, hätte man ihm zugehört. Den teilnehmenden Speakern am Future Slam im ROXYsoll es anders gehen. Wie bei einem Science Slam hat jeder Slammer zehn Minuten Zeit, ein an sich wissenschaftliches oder auch nur komplexes Zukunftsthema einem breiten Publikum verständlich zu machen. Egal ob Schüler, Student, Lehrer, Wissenschaftler oder Laie mit speziellen Fachkenntnissen: Jeder und jede darf auf die Bühne. Im Anschluss wird der Vortrag vom Publikum bewertet. Das Publikum bildet die Jury und bestimmt, wer am Ende des Abends zum Future-Slam-Sieger gekürt wird.

In Kooperation mit der Kulturabteilung der Stadt Ulm.
Im Rahmen des Berblinger-Jubiläums

Moderiert von Dana Hoffmann

Du hast selbst ein wissenschaftliches Thema, das du unserem Publikum näher bringen willst? Dann melde dich an und nutze deine 10 Minuten! scienceslam@roxy.ulm.de

Danke an scanplus GmbH