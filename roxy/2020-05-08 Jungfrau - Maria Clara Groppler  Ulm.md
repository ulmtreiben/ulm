---
id: "2694558753897629"
title: VERSCHOBEN - Jungfrau - Maria Clara Groppler // Ulm (verschoben auf 24.09.21)
start: 2021-01-27 20:00
end: 2021-01-27 22:00
address: ROXY.ulm
link: https://www.facebook.com/events/2694558753897629/
image: 79162678_2504918349621533_7358814506165731328_o.jpg
teaser: Der Termin wurde vom 08.05.20 auf den 27.01.21 verschoben. Die Tickets
  behalten ihre Gültigkeit.  Was sie ausspricht, trauen sich viele nicht mal zu
  d
isCrawled: true
---
Der Termin wurde vom 08.05.20 auf den 27.01.21 verschoben.
Die Tickets behalten ihre Gültigkeit.

Was sie ausspricht, trauen sich viele nicht mal zu denken. Maria Clara Groppler’s Comedy ist unerschrocken und ehrlich, ihr Humor - derb und trocken.
 
Vor 3 Jahren fing sie in Berlin mit Stand-up Comedy an, seitdem ist sie überall in Deutschland unterwegs. In ihrem ersten Solo-Programm “Jungfrau” gibt Maria Einblicke in den Kopf einer 20-Jährigen jungen Frau. 

Egal ob sie versucht ihrer Mutter den neuen Freund auszuspannen, vermehrt den Frauenarzt aufsucht, um endlich mal wieder angefasst zu werden, oder die Männerwelt mit ihrem Konzept „Freundschaft Minus“ bei Laune hält. Der Zuschauer kann sich sicher sein, dass Maria Clara Groppler, das so harmlos wirkende Mädchen von nebenan, die Gesellschaft und Moralisten an den richtigen Stellen zu triggern weiß.

Ein Abend mit Maria ist intensiver als jeder Gottesdienst, geiler als jede Predigt und berauschender als Weihwasser.

Weitere Termine unter: www.mariaclaragroppler.de
