---
id: "618165905482102"
title: Rikas + Luke Noa im ROXY Sound Garten
start: 2020-07-18 20:00
end: 2020-07-18 23:00
address: ROXY.ulm
link: https://www.facebook.com/events/618165905482102/
image: 107667963_10157728162787756_1848595377348345556_o.jpg
teaser: RIKAS Derart lässigen Soul-Pop hat man aus Deutschland lange nicht gehört. Das
  Schwierige leicht klingen lassen. Die Songs von Rikas machen einen Aben
isCrawled: true
---
RIKAS
Derart lässigen Soul-Pop hat man aus Deutschland lange nicht gehört. Das Schwierige leicht klingen lassen. Die Songs von Rikas machen einen Abend ohne Plan, einen Film ohne Plot im Handumdrehen zu etwas Unvergesslichem. Melodien wie Stevie Wonder, der Pop-Drive von Phoenix, die Selbstironie eines Jarvis Cocker. Dazu so viel Souveränität, dass man sich fragt, woher, zum Teufel, haben die Jungs die?

LUKE NOA
„Adaptation“ - bereits im Namen seiner Debut EP gibt Newcomer Luke Noa den ersten Hinweis auf einen Sound, der mit nur einem Wort schwer greifbar ist: Die erste Phase seines künstlerischen Schaffens nutzt der Süddeutsche um sich anzupassen, sich völlig von verschiedenen Genres inspirieren und formen lassen. Was daraus am Ende entsteht trotzt dem schlichten Bild eines gewöhnlichen Singer-Songwriters. In einer Collage aus Folk, Indie, Pop und Soul entsteht ein einzigartiger, kraftvoller Sound, dessen Ankerpunkt letztendlich eine unverkennbare Stimme ist, die Luke Noa zu Luke Noa macht.

Eintritt frei.

Der Biergarten öffnet bereits um 15:00 H.
Wir können keine Reservierungen entgegennehmen. First come, first serve.