---
id: "355259426064971"
title: Sidi Wacho * ROXY Sound Garten
start: 2021-09-03 20:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/355259426064971/
image: 236231849_10158732904667756_1744694977531489335_n.jpg
isCrawled: true
---
LIVEMUSIK UNTER FREIEM HIMMEL
EINTRITT FREI! 💛

Sidi Wacho sind aus Valparaiso, Lima und Paris aber fühlen sich überall zu Hause. Sidi Wacho besteht aus zwei MCs, einem Akkordeonspieler, einem Percussionisten, und einem Trompetenspieler. 

Durch die Melange von Cumbia, Hiphop und Balkan entsteht bei der Band ein äußerst heißer Mix aus Klassenkampf und ausufernder Latinoparty. 

Mit ihrem zwei ersten Album Libre (2016) und Bordeliko (2018) wird die Band eine Menge Konzerte in ganz Europa, Chile und Kanada spielen und tausende Zuschauer mit ihrer « Buena Onda » begeistern. In 2020 hat die Band ihre drittes Album „Elegancia Popular“ veröffentlicht. 

https://www.sidiwacho.com/?fbclid=IwAR0JlbpPwYN6dPoEA0bwyDH7Me7Bl9O1lbgczl2JN2E5us98jPdf7gZomVY