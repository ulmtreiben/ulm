---
id: "497157884916158"
title: Matikku * ROXY Sound Garten
start: 2021-07-21 18:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/497157884916158/
image: 218111194_10158667757272756_5957239421174101925_n.jpg
isCrawled: true
---
Wir freuen uns, euch in unserem Sound Garten begrüßen zu dürfen. Neben dem regulären Biergartenbetrieb versüßen euch unterschiedliche DJs die Ohren.

Schüttel deine Maracas in Ulms vernebeltem Amazonasdelta zu:  Cumbia, Mestizo, Latin-Ska, Dub & Afro-Colombiano. Wer Manu Chao mag wird diesen Abend lieben!

Eintritt frei, Spenden für die musikalische Unterhaltung wird gern entgegen genommen.

Öffnungszeiten:
Mittwoch - Samstag: ab 17:00 H
Sonntag: ab 15:00 H

Alle aktuellen Bestimmungen bzgl. Corona findet ihr auf unserer Homepage www.roxy.ulm.de