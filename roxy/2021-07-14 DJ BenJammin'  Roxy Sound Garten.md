---
id: "214638933878697"
title: DJ BenJammin' * Roxy Sound Garten
start: 2021-07-14 18:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/214638933878697/
image: 212499500_10158638046372756_4708273308464549190_n.jpg
isCrawled: true
---
Wir freuen uns, euch in unserem Sound Garten begrüßen zu dürfen. Neben dem regulären Biergartenbetrieb versüßen euch unterschiedliche DJs die Ohren.

Aus seinem Musikarchiv führt euch DJ BenJammin' wie gewohnt in eine musikalische Reise von 70's Funk'n'Soul über Alternative, Blues, Oldies, Rock, Reggae und Salsa Cubana

Eintritt frei, Spenden für die musikalische Unterhaltung wird gern entgegen genommen.

Öffnungszeiten:
Mittwoch - Samstag: ab 17:00 H
Sonntag: ab 15:00 H

Alle aktuellen Bestimmungen bzgl. Corona findet ihr auf unserer Homepage www.roxy.ulm.de
