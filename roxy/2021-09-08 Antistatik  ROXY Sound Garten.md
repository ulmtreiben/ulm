---
id: "1212980075854377"
title: Antistatik * ROXY Sound Garten
start: 2021-09-08 18:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/1212980075854377/
image: 241054417_10158760500417756_2785443400105322247_n.jpg
isCrawled: true
---
Wir freuen uns, euch in unserem Sound Garten begrüßen zu dürfen. Neben dem regulären Biergartenbetrieb versüßen euch unterschiedliche DJs die Ohren.

Eintritt frei, Spenden für die musikalische Unterhaltung wird gern entgegen genommen.

Öffnungszeiten:
Mittwoch - Samstag: ab 17:00 H
Sonntag: ab 15:00 H

Alle aktuellen Bestimmungen bzgl. Corona findet ihr auf unserer Homepage www.roxy.ulm.de
