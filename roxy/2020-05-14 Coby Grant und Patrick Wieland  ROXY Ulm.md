---
id: "451189022484298"
title: Coby Grant und Patrick Wieland * ROXY Ulm
start: 2020-10-29 20:00
end: 2020-10-29 23:00
address: ROXY.ulm
link: https://www.facebook.com/events/451189022484298/
image: 80393179_10157069015767756_7606954467870638080_o.jpg
teaser: Das Konzert wurde vom 14.05.2020 auf den 29.10.2020 verschoben. Tickets
  behalten ihre Gültigkeit. *** Small Tits Big Dreams Coby Grant ist eine unabhä
isCrawled: true
---
Das Konzert wurde vom 14.05.2020 auf den 29.10.2020 verschoben. Tickets behalten ihre Gültigkeit.
***
Small Tits Big Dreams
Coby Grant ist eine unabhängige Musikerin, die ihre Karriere selbst koordiniert und plant. Sie spielt internationale Touren, schreibt unterwegs neue Songs und vermarktet erfolgreich ihre Musik ohne sich an ein Plattenlabel zu binden. Im Jahre 2018 nahm sie an The Voice of Germany teil und erreichte dort das Halbfinale.
Cobys neustes Album "Small Tits Big Dreams", das im Herbst 2019 erschienen ist, wurde in vier verschiedenen Studios in Australien und Holland aufgenommen. Den Song "Let go of me" schrieb sie zusammen mit the Voice of Germany Gitarrist Patrick Wieland - zusammen bilden sie ein Duo mit viel Charme, Witz und einer beeindruckenden Bühnenpräsenz.