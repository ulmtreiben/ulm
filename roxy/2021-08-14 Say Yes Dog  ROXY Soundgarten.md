---
id: "3777484412362399"
title: Say Yes Dog * ROXY Soundgarten
start: 2021-08-14 20:00
locationName: ROXY.ulm
link: https://www.facebook.com/events/3777484412362399/
image: 195645419_10158555245052756_4775534910427730018_n.jpg
isCrawled: true
---
LIVEMUSIK UNTER FREIEM HIMMEL
EINTRITT FREI! 💛

Durch die Welt, durch das All, über die Tanzfläche – direkt in die Beine: Straight from dog space floaten Say Yes Dog auf die Bühne und nehmen uns mit auf ihre ganz eigene musikalische Reise! Nicht nur musikalisch, auch thematisch vermischen sich auf ihrem neuen Album VOYAGE viele faszinierende Aspekte zu einem kosmischen Ganzen: die Pause seit ihrem Debütalbum, die mit Reisen gefüllte Zwischenzeit, das Heranwagen an ein neues musikalisches Universum. Mit schwebender Leichtigkeit, treibenden Hooks und melodischen Höhenflügen zeigt das deutsch- luxemburgische Trio die musikalische Weiterentwicklung ihres brillanten Elektro-Pops. Dank verstärkt analogem Sound und 80ies Space Nostalgia präsentieren sie Songs, die wärmer, direkter, atmosphärischer und irgendwie auch reifer klingen und gleichzeitig nah dran an der ungezügelt- tanzbaren Tradition ihrer EP A FRIEND und des Debütalbums PLASTIC LOVE bleiben. Next stop: the moon. 

Das aus Deutschland und Luxemburg stammende Trio traf sich beim Musikstudium in den Niederlanden, lebte bereits an vielen Orten und hat jetzt seine gemeinsame Base seit einigen Jahren in Berlin gefunden. In den letzten Jahren entwickelte sich jeder von ihnen auch in anderen Musikprojekten weiter, um aber schließlich immer wieder zusammen zu kommen: „Say Yes Dog ist für uns das Projekt, wo wir zusammen als Freunde arbeiten können, ohne persönliche Kompromisse eingehen zu müssen. Wir haben musikalisch eine unausgesprochene Übereinstimmung und können uns dennoch immer wieder neu untereinander beeinflussen und inspirieren, wodurch sich die Band stets organisch weiterentwickelt und verändert.“ 
