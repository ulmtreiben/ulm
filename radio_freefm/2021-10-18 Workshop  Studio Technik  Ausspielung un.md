---
id: "1550806898594243"
title: "Workshop :: Studio Technik / Ausspielung und Jingles"
start: 2021-10-18 19:00
address: Radio free FM
link: https://www.facebook.com/events/1550806898594243/
image: 243425568_10159520894091668_7477384495313493774_n.jpg
isCrawled: true
---
Technisch hat sich viel im Studio geändert. Seit diesem Jahr wird unser alter Jingleplayer durch
eine neue Cartwall ersetzt.
Diese Software kann jedoch mehr als nur unsere Jingles spielen. Sie dient auch als Ausspielung
über unsere Song Mediathek.
Eine Übersicht über das Programm, die Mediathek und wie Ihr eure eigene Cartwall mit eurem
Opener und Jingles erstellt erhaltet ihr in diesem Workshop.

Referent: Timo Freudenreich
Online-Link: https://online-seminar.bz-bm.de/bbb/rad-tqf-njz

Pflicht für Neueinsteiger, kostenlos für Mitglieder

Anmeldung unter: ausbildung[at]freefm.de 