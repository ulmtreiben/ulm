---
id: "214569689687527"
title: Wenn aus Spaß Ernst wird – Ein 2. Blick auf deutschen Rap
start: 2020-03-16 19:00
end: 2020-03-16 21:00
address: Münsterplatz 25, 89073 Ulm
link: https://www.facebook.com/events/214569689687527/
image: 87796702_1073191313067630_4133424608105725952_n.jpg
teaser: Vor allem rechte Grauzonenbands aus dem Rockbereich mit ihrem möglichen
  Einfluss auf ihre Hörerschaft sind in den letzten Jahren verstärkt in den Foku
isCrawled: true
---
Vor allem rechte Grauzonenbands aus dem Rockbereich mit ihrem möglichen Einfluss auf ihre Hörerschaft sind in den letzten Jahren verstärkt in den Fokus (pädagogischer Arbeit) gerückt. Mit dem enormen Erfolg deutschsprachiger Rapmusik wurde der Blick aber immer stärker auch auf dieses Musikgenre gelenkt. Dabei reicht das problematische Spektrum von NS-Rap, über sexistischen und gewaltverherrlichenden Gangsta-Rap bis hin zu religiös-extremistischen Rap. Für Jugendliche sind die propagierten Inhalte nicht immer sofort zu erkennen und Erwachsene schießen manchmal mit ihrem Wunsch nach Verboten gehörig über das Ziel hinaus. Aber wann lohnt es sich Texte zu hinterfragen? Wann ist ein Verbot wirklich angebracht und wo verläuft der schmale Grat zwischen künstlerischer Freiheit und einer nicht tolerierbaren Grenzüberschreitung? 
Zur Klärung dieser Fragen möchte der Vortrag einen Beitrag leisten. Eingeladen sind alle, die sich für deutschsprachigen Rap und Hip-Hop im Allgemeinen interessieren.

Referenten: Otto Sommer und Cord Dette 
