---
id: "1692166060970915"
title: "Online-Workshop :: Redaktion und Recherche"
start: 2021-05-29 11:00
end: 2021-05-29 13:00
link: https://www.facebook.com/events/1692166060970915/
image: 135274967_10158927732361668_8824670069234343530_o.jpg
teaser: "Online-Workshop :: Redaktion und Recherche  Wie recherchiere ich Themen im
  Radio und texte sie verständlich? Wo finde ich spannende Geschichten? Was m"
isCrawled: true
---
Online-Workshop :: Redaktion und Recherche

Wie recherchiere ich Themen im Radio und texte sie verständlich? Wo finde ich spannende Geschichten? Was macht gute "Radiosprache" aus? Und: Wie bereite ich meine Inhalte auf, bringe sie auf den Punkt und entwickle sie weiter? Wie baue ich ein Themen-Netzwerk auf?

Referent: K.W.

Online-Link:
https://online-seminar.bz-bm.de/bbb/rad-tqf-njz

Pflicht für Neueinsteiger, kostenlos für Mitglieder!

Anmeldung: ausbildung[at]freefm.de