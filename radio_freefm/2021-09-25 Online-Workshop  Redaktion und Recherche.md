---
id: "371169151381017"
title: "Online-Workshop :: Redaktion und Recherche"
start: 2021-09-25 11:00
end: 2021-09-25 13:00
address: Radio free FM
link: https://www.facebook.com/events/371169151381017/
image: 233707444_10159431702226668_4784713273286807695_n.jpg
isCrawled: true
---
Online-Workshop :: Redaktion und Recherche

Wie recherchiere ich Themen im Radio und texte sie verständlich? Wo finde ich spannende Geschichten? Was macht gute "Radiosprache" aus? Und: Wie bereite ich meine Inhalte auf, bringe sie auf den Punkt und entwickle sie weiter? Wie baue ich ein Themen-Netzwerk auf?
Referent: K.W.

Online-Link:
https://online-seminar.bz-bm.de/bbb/rad-tqf-njz

Pflicht für Neueinsteiger, kostenlos für Mitglieder!
Anmeldung: ausbildung[at]freefm.de