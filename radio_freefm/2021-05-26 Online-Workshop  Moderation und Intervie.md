---
id: "732080841047797"
title: "Online-Workshop :: Moderation und Interview"
start: 2021-05-26 17:00
end: 2021-05-26 19:00
link: https://www.facebook.com/events/732080841047797/
image: 135450309_10158927634041668_300790812021505556_o.jpg
teaser: "Online-Workshop :: Moderation und Interview  Regeln für die Vorbereitung und
  für die Fragestellung eines Interviews; Technik (Umgang mit Mikro; Studio"
isCrawled: true
---
Online-Workshop :: Moderation und Interview

Regeln für die Vorbereitung und für die Fragestellung eines Interviews; Technik (Umgang mit Mikro; Studio-Technik für Telefon-Interview). Theoretische Vorbereitung auf die moderierte Sendung: Aufteilung, Jingles, Musikeinsatz, Präsentation und Nachrichten. "Bei Radio free FM beobachtete Fehler" mit Hörbeispielen. (Nicht: Moderationsschulung im Studio.)

Referent: Clemens Grote

Online-Link: https://online-seminar.bz-bm.de/bbb/rad-tqf-njz

Pflicht für Neueinsteiger, kostenlos für Mitglieder

Anmeldung: ausbildung[at]freefm.de