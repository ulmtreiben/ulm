---
id: "894614014361523"
title: "Online-Workshop: Grundkurs Studiotechnik"
start: 2020-11-27 17:00
end: 2020-11-27 19:00
link: https://www.facebook.com/events/894614014361523/
image: 118652701_10158630085511668_880529621389524957_o.jpg
teaser: "Online-Workshop: Grundkurs Studiotechnik In diesem Baustein lernt ihr die
  Technik im Sendestudio kennen und bedienen. Mischpult, CD-Player, Plattenspi"
isCrawled: true
---
Online-Workshop: Grundkurs Studiotechnik
In diesem Baustein lernt ihr die Technik im Sendestudio kennen und bedienen. Mischpult, CD-Player, Plattenspieler, Tapedeck, Mikrofone, Durchführung von Telefoninterviews, Mitschnitte der Sendung.
Referent: Julius Taubert
Online-Link:
https://meeting.bz-bm.de/radiobaukastenfreefm

Pflicht für Neueinsteiger, kostenlos für Mitglieder!