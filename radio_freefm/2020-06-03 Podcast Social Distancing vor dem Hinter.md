---
id: "724895251677605"
title: Podcast "Social Distancing vor dem Hintergrund soz. Ausgrenzung"
start: 2020-06-03 15:00
end: 2020-06-03 15:30
locationName: Ulm - Internationale Stadt
address: Donaustr. 5, 89073 Ulm
link: https://www.facebook.com/events/724895251677605/
image: 101318769_1142833242770103_3299101655802314752_o.jpg
teaser: Das Projekt "Vielfalt Leben in Ulm in der einen Welt" möchte Stereotypen
  abbauen und für Vielfalt und gegen Rassismus und Diskriminierung eintreten. D
isCrawled: true
---
Das Projekt "Vielfalt Leben in Ulm in der einen Welt" möchte Stereotypen abbauen und für Vielfalt und gegen Rassismus und Diskriminierung eintreten. Die Podcast-Reihe behandelt Themen wie Diversity, Menschenrechte, Demokratie oder Extremismus. Heute zu Gast ist Andreas Foitzik vom Projekt klever-iq, adis e.V.- Antidiskriminierung, Empowerment, Praxisentwicklung.
Mit ihm reden wir über "Social Distancing vor dem Hintergrund sozialer Ausgrenzung". Sind die Coronamaßnahmen wirklich für alle gleich? Was heißt überhaupt systemrelevant? Wo werden vulnerable Gruppen diskriminiert?
Live zu hören bei Radio free FM, 102.6 MHz, am 03.06.2020 um 15.00 Uhr oder danach als Podcast bei www.freefm.de