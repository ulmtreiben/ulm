---
id: "1031933050913475"
title: "Online- + Präsenzworkshop :: Grundkurs Studiotechnik"
start: 2021-09-27 19:00
address: Radio free FM
link: https://www.facebook.com/events/1031933050913475/
image: 241716763_10159487907246668_1932706381162969846_n.jpg
isCrawled: true
---
Online-Workshop: Grundkurs Studiotechnik

ACHTUNG:
Die Veranstaltung findet sowohl online, als auch im Studio mit Präsenzmöglichkeit statt. Bitte beachtet die 3G-Regel, wenn Ihr vorhabt, den Workshop im Studio zu besuchen.

In diesem Baustein lernt ihr die Technik im Sendestudio kennen und bedienen. Mischpult, Jingle-Player, CD-Player, Plattenspieler, Tapedeck, Mikrofone, Durchführung von Telefoninterviews, Mitschnitte der Sendung.
Referent: Julius Taubert
Online-Link:
https://online-seminar.bz-bm.de/bbb/rad-tqf-njz
Pflicht für Neueinsteiger, kostenlos für Mitglieder!
Anmeldung: ausbildung[at]freefm.de