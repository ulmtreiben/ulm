---
id: "381138073292885"
title: "Online-Workshop :: free FM Datenbank"
start: 2021-03-09 18:00
end: 2021-03-09 20:00
link: https://www.facebook.com/events/381138073292885/
image: 150427672_10159041800381668_7160397965858162750_n.jpg
teaser: "Datenbank Workshop:  Einführung in die neue free FM Datenbank für Nutzer,
  Plattform-Koordinatoren, -moderatoren und weitere Multiplikatoren.  Referent"
isCrawled: true
---
Datenbank Workshop:

Einführung in die neue free FM Datenbank für Nutzer, Plattform-Koordinatoren, -moderatoren und weitere Multiplikatoren.

Referent: Sabine Fratzke

Online-Link: 
https://online-seminar.bz-bm.de/bbb/rad-tqf-njz

Anmeldung unter: ausbildung[at]freefm.de