---
id: "318915033111752"
title: Oststadt-Olympiade
start: 2021-06-27 12:00
address: Olgastraße 143, 89073 Ulm
link: https://www.facebook.com/events/318915033111752/
image: 202013891_10159335082111668_1146109341993795506_n.jpg
isCrawled: true
---
Was passiert an diesem Tag?

Über das Live-Programm von Radio free FM werden Hinweise rund um den Ort der Ente veröffentlicht. Die Teams müssen aufgrund dieser Tipps die grüne Ente suchen und für das nächste Team in ein neues Versteck schieben. Weitere Disziplinen sind:

Fragen zur Geschichte und Besonderheiten der Oststadt

Mobilisierung von Anwohnern für spaßige Kurzaktionen

Wetten zur Krafteinwirkung des Menschen auf leblose Gegenstände

Was wollen wir?

Einen spannenden, coronakonformen Wettkampf. Wir wollen mit der gemeinsamen Handlung, der Spontanität und dem sinnlosen Quatsch die Kreativität der Bürger:innen feiern.

Wer kann mitmachen?

Gesucht werden Teams mit ungerader Personenzahl. Gruppen, die an der Oststadt Olympiade teilnehmen möchten, geben sich einen markanten Namen und melden sich bei affaerekoenigwilhelm@posteo.de an.