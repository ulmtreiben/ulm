---
id: "830674777754766"
title: 'SPEZIALSENDUNG "THE BEATLES - through the years Part 1 : 1962"'
start: 2020-12-23 20:00
end: 2020-12-23 22:00
link: https://www.facebook.com/events/830674777754766/
image: 132500972_3276203129152807_5949220339965368244_o.jpg
teaser: "Radio-special: Zwei Stunden über den Beginn der Beatles, das Jahr 1962. Mit
  historischen Aufnahmen, Interviews und  O-Tönen. Nur bei www.freefm.de. St"
isCrawled: true
---
Radio-special: Zwei Stunden über den Beginn der Beatles, das Jahr 1962. Mit historischen Aufnahmen, Interviews und  O-Tönen. Nur bei www.freefm.de.
Stream über die Mediathek, dann PlayButton--> https://www.freefm.de/mediathek  