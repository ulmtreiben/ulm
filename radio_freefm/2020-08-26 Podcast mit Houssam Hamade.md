---
id: "311971630087774"
title: Podcast mit Houssam Hamade
start: 2020-08-26 15:00
end: 2020-08-26 15:30
locationName: Radio free FM
address: Platzgasse 18, 89073 Ulm
link: https://www.facebook.com/events/311971630087774/
teaser: Das Projekt "Vielfalt Leben in Ulm in der einen Welt" möchte Stereotypen
  abbauen und für Vielfalt und gegen Rassismus und Diskriminierung eintreten. D
isCrawled: true
---
Das Projekt "Vielfalt Leben in Ulm in der einen Welt" möchte Stereotypen abbauen und für Vielfalt und gegen Rassismus und Diskriminierung eintreten. Die Podcast-Reihe behandelt Themen wie Diversity, Menschenrechte, Demokratie oder Extremismus. 
Heute zu Gast ist heute der Journalist und Schriftsteller Houssam Hamade. 
Er erklärt uns den Klassismus. Was ist überhaupt Klassismus? Er berichtet über persönliche Erfahrungen und klärt den Mythos Bildung in Deutschland. Haben wirklich alle die gleichen Chancen?

Live zu hören bei Radio free FM, 102.6 MHz, am 26.08.2020 um 15.00 Uhr oder danach als Podcast bei www.freefm.de 