---
id: "714184549433297"
title: Listening to the Universe – Radiophonien des Alls
start: 2020-08-11 14:04
end: 2020-08-13 14:00
address: 13158 Berlin
link: https://www.facebook.com/events/714184549433297/
teaser: Every year in August, the Northern Hemisphere’s night sky is graced by meteor
  showers – the Perseids. Datscha Radio is using this astronomical spectac
isCrawled: true
---
Every year in August, the Northern Hemisphere’s night sky is graced by meteor showers – the Perseids. Datscha Radio is using this astronomical spectacle as the departure point for a 48-hour festival of radio art. Detailed Info: https://datscharadio.de/en/11863-2/