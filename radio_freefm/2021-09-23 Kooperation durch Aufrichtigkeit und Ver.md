---
id: "829937944579073"
title: Kooperation durch Aufrichtigkeit und Verständnis
start: 2021-09-23 18:00
link: https://www.facebook.com/events/829937944579073/
image: 237883810_6634947946530578_2702794529925335257_n.jpg
isCrawled: true
---
Eine Einführung in die Gewaltfreie Kommunikation von Marshall B. Rosenberg.

Wenn wir mit jemandem Schwierigkeiten haben, ist die Versuchung groß, die Schuld beim anderen zu suchen. Oder wir nehmen etwas persönlich und sind tagelang verschnupft. Erfolgreicher ist es aber, selbst dazuzulernen. In diesem praxisnahen Workshop lernst du ein Modell kennen, das dir hilft, eine wertschätzende Haltung zu leben. 

Referentin: Adelheid Schmidt

Veranstalter: Radio Free FM

Online Veranstaltung. 
Anmeldungen an radio@freefm.de Eintritt frei

Link zur Veranstaltung: 
https://online-seminar.bz-bm.de/bbb/rad-tqf-njz

Mehr Informationen über die Ulmer Friedenswochen:
https://www.friedenswochen-ulm.de
