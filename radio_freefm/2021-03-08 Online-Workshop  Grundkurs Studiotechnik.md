---
id: "262642051969229"
title: "Online-Workshop :: Grundkurs Studiotechnik"
start: 2021-03-08 19:00
end: 2021-03-08 22:00
link: https://www.facebook.com/events/262642051969229/
image: 154281085_10159061090841668_8569190672888662836_o.jpg
teaser: "Online-Workshop: Grundkurs Studiotechnik In diesem Baustein lernt ihr die
  Technik im Sendestudio kennen und bedienen. Mischpult, Jingle-Player, CD-Pla"
isCrawled: true
---
Online-Workshop: Grundkurs Studiotechnik
In diesem Baustein lernt ihr die Technik im Sendestudio kennen und bedienen. Mischpult, Jingle-Player, CD-Player, Plattenspieler, Tapedeck, Mikrofone, Durchführung von Telefoninterviews, Mitschnitte der Sendung.

Referent: Julius Taubert
Online-Link:
https://online-seminar.bz-bm.de/bbb/rad-tqf-njz

Pflicht für Neueinsteiger, kostenlos für Mitglieder!

Anmeldung: ausbildung[at]freefm.de