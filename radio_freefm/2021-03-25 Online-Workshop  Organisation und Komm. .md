---
id: "240604690766910"
title: "Online-Workshop :: Organisation und Komm. bei Radio free FM"
start: 2021-03-25 18:00
end: 2021-03-25 20:00
link: https://www.facebook.com/events/240604690766910/
image: 135764181_10158927723256668_1248933932148338559_o.jpg
teaser: "Online-Workshop :: Organisation und Kommunikation bei Radio free FM Es werden
  die rechtlichen Strukturen bei Radio free FM nach außen sowie der innere"
isCrawled: true
---
Online-Workshop :: Organisation und Kommunikation bei Radio free FM
Es werden die rechtlichen Strukturen bei Radio free FM nach außen sowie der innere Aufbau (Gremien, Ämter) dargestellt. Zentrale Fragen dieses Bausteins sind: Wie komme ich zu einer eigenen Sendung? Welche Bedeutung hat die Houseordnung? Welche Infrastruktur (Schnittplätze, Rechtsreader etc.) ist im Sender vorhandenen? Wie und wann kann ich diese nutzen?

Referent: Rainer Walter

Online-Link: https://online-seminar.bz-bm.de/bbb/rad-tqf-njz

Pflicht für Neueinsteiger, kostenlos für Mitglieder

Anmeldung unter: ausbildung[at]freefm.de 