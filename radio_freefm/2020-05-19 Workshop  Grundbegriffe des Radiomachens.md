---
id: "2277437105880064"
title: "Online-Workshop :: Grundbegriffe des Radiomachens"
start: 2020-05-19 17:00
end: 2020-05-19 19:00
locationName: Radio free FM
address: Platzgasse 18, 89073 Ulm
link: https://www.facebook.com/events/2277437105880064/
image: 95537348_10158269651016668_1283444895012356096_o.jpg
teaser: "Online-Workshop Grundbegriffe des Radiomachens und rechtliche
  Grundlagen:  Nichtkommerzieller, öffentlich-rechtlicher, kommerzieller Hörfunk
  - die Anf"
isCrawled: true
---
Online-Workshop Grundbegriffe des Radiomachens und rechtliche Grundlagen:

Nichtkommerzieller, öffentlich-rechtlicher, kommerzieller Hörfunk - die Anfänge der freien Radios als Piratenradios, Frequenzen & Marktanalysen, warum die Politiker lieber sogenannte Jugendradios lizenzieren als die Hörfunk produzierende Jugend zu fördern, Werbung und Schleichwerbung und Freunde und Feinde im Ätherdschungel.

Referent: Friedrich Hog

Online-Link: https://meet.jit.si/radiobaukastenfreefm

Für Mitglieder kostenlos, Pflicht für Neueinsteiger!

Anmeldung: ausbildung[at]freefm.de