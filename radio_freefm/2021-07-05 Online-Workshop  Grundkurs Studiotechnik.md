---
id: "427136948367921"
title: "Online-Workshop :: Grundkurs Studiotechnik"
start: 2021-07-05 19:00
end: 2021-07-05 21:00
link: https://www.facebook.com/events/427136948367921/
image: 153829377_10159061108921668_3582077677159455356_n.jpg
isCrawled: true
---
Online-Workshop: Grundkurs Studiotechnik

In diesem Baustein lernt ihr die Technik im Sendestudio kennen und bedienen. Mischpult, Jingle-Player, CD-Player, Plattenspieler, Tapedeck, Mikrofone, Durchführung von Telefoninterviews, Mitschnitte der Sendung.

Referent: Julius Taubert

Online-Link:
https://online-seminar.bz-bm.de/bbb/rad-tqf-njz

Pflicht für Neueinsteiger, kostenlos für Mitglieder!

Anmeldung: ausbildung[at]freefm.de