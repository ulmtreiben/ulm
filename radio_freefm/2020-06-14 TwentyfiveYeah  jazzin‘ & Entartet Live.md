---
id: "545367339413236"
title: TwentyfiveYeah / jazzin‘ & Entartet Live
start: 2020-06-14 20:00
end: 2020-06-15 00:00
locationName: Cafe Naschkatze in Neu-Ulm
address: Marienstraße 6, 89231 Neu-Ulm
link: https://www.facebook.com/events/545367339413236/
image: 87021297_10158004158736668_2121279962332790784_o.jpg
teaser: "#25JahrefreeFM Die Beiden Redaktionen  jazzin‘ & entartet senden Live aus der
  Nachkatze in Neu-Ulm. In gemütlicher Atmosphäre ein kaltgetränk zu sich"
isCrawled: true
---
#25JahrefreeFM
Die Beiden Redaktionen  jazzin‘ & entartet senden Live aus der Nachkatze in Neu-Ulm. In gemütlicher Atmosphäre ein kaltgetränk zu sich nehmen, oder auf der 102,6 Mhz und freefm.de zuhören.
  
Mit
Christian Clement 
Entartet 
- Alternativ 

Roland Jo Jetter 
Carsten Belan 
Uwe Brennenstuhl 
jazzin‘ 
- Jazz Soul Swing 

& Gäste 

Thema;
Von Anfang an ... freeFm!❤

Beginn:
20.00 - 22.00 Uhr - jazzin‘ 
22.00 - 23.00 Uhr - entartet