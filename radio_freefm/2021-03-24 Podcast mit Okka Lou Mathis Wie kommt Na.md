---
id: "186537749654970"
title: Podcast mit Okka Lou Mathis "Wie kommt Nachhaltigkeit in die Politik?"
start: 2021-03-24 15:00
end: 2021-03-24 15:30
link: https://www.facebook.com/events/186537749654970/
image: 161218719_1366550040398421_7472525616740167179_o.jpg
teaser: Die Podcast-Reihe der internationalen Stadt Ulm. Heute zu Gast ist Okka Lou
  Mathis vom DEUTSCHEN INSTITUT FÜR ENTWICKLUNGSPOLITIK (DIE).  Sie erklärt
isCrawled: true
---
Die Podcast-Reihe der internationalen Stadt Ulm.
Heute zu Gast ist Okka Lou Mathis vom DEUTSCHEN INSTITUT FÜR ENTWICKLUNGSPOLITIK (DIE).  Sie erklärt uns, wie die angestrebte Nachhaltigkeit in die Politik kommt. Durch wen wird Nachhaltigkeit in den nationalen politischen Entscheidungsstrukturen verankert? Welche nationale Nachhaltigkeitsgremien gibt es? Wie wirkungsvoll sind diese Gremien? Welches Potenzial haben geloste Bürger*innenräte für mehr Nachhaltigkeit in der Politik?

Live zu hören bei Radio free FM, 102.6 MHz, am 24.03.2021 um 15.00 Uhr oder danach als Podcast bei www.freefm.de 
