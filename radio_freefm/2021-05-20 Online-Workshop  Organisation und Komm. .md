---
id: "132545578592440"
title: "Online-Workshop :: Organisation und Komm. bei Radio free FM"
start: 2021-05-20 18:00
end: 2021-05-20 20:00
link: https://www.facebook.com/events/132545578592440/
image: 135856941_10158927727036668_1933218860921460115_o.jpg
teaser: "Online-Workshop :: Organisation und Kommunikation bei Radio free FM  Es
  werden die rechtlichen Strukturen bei Radio free FM nach außen sowie der
  inner"
isCrawled: true
---
Online-Workshop :: Organisation und Kommunikation bei Radio free FM

Es werden die rechtlichen Strukturen bei Radio free FM nach außen sowie der innere Aufbau (Gremien, Ämter) dargestellt. Zentrale Fragen dieses Bausteins sind: Wie komme ich zu einer eigenen Sendung? Welche Bedeutung hat die Houseordnung? Welche Infrastruktur (Schnittplätze, Rechtsreader etc.) ist im Sender vorhandenen? Wie und wann kann ich diese nutzen?

Referent: Rainer Walter

https://online-seminar.bz-bm.de/bbb/rad-tqf-njz

Pflicht für Neueinsteiger, kostenlos für Mitglieder

Anmeldung unter: ausbildung[at]freefm.de 