---
id: "514281859974295"
title: "Online-Workshop :: Homepageworkshop"
start: 2021-03-29 17:30
end: 2021-03-29 19:30
link: https://www.facebook.com/events/514281859974295/
image: 159039478_10159095919456668_4230413624082701592_o.jpg
teaser: "Einführung in die neue free FM Webseite  Workshopinhalte sind unter anderem:
  - Anlegen von Usern - Tagging von Inhalten - Erstellung von Audios, Artik"
isCrawled: true
---
Einführung in die neue free FM Webseite

Workshopinhalte sind unter anderem:
- Anlegen von Usern
- Tagging von Inhalten
- Erstellung von Audios, Artikeln und Playlisten

Referent: Dominic Köstler

Online-Kanal:
https://online-seminar.bz-bm.de/bbb/rad-tqf-njz

Anmeldung unter: ausbildung[at]freefm.de