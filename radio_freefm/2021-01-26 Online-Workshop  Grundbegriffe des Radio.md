---
id: "683058619052428"
title: "Online-Workshop :: Grundbegriffe des Radiomachens"
start: 2021-01-26 17:00
end: 2021-01-26 19:00
link: https://www.facebook.com/events/683058619052428/
image: 136027404_10158927607216668_3771007133457573693_o.jpg
teaser: "Online-Workshop Grundbegriffe des Radiomachens und rechtliche Grundlagen:
  Nichtkommerzieller, öffentlich-rechtlicher, kommerzieller Hörfunk - die Anfä"
isCrawled: true
---
Online-Workshop Grundbegriffe des Radiomachens und rechtliche Grundlagen:
Nichtkommerzieller, öffentlich-rechtlicher, kommerzieller Hörfunk - die Anfänge der freien Radios als Piratenradios, Frequenzen & Marktanalysen, warum die Politiker lieber sogenannte Jugendradios lizenzieren als die Hörfunk produzierende Jugend zu fördern, Werbung und Schleichwerbung und Freunde und Feinde im Ätherdschungel.
Referent: Friedrich Hog
Online-Link:
https://meeting.bz-bm.de/radiobaukastenfreefm
Für Mitglieder kostenlos, Pflicht für Neueinsteiger!
Anmeldung: ausbildung[at]freefm.de 