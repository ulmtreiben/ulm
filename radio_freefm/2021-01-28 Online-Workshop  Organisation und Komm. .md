---
id: "412543786464901"
title: "Online-Workshop :: Organisation und Komm. bei Radio free FM"
start: 2021-01-28 18:00
end: 2021-01-28 20:00
link: https://www.facebook.com/events/412543786464901/
image: 135421943_10158927647726668_7484457474488781934_o.jpg
teaser: "Online-Workshop :: Organisation und Kommunikation bei Radio free FM Es werden
  die rechtlichen Strukturen bei Radio free FM nach außen sowie der innere"
isCrawled: true
---
Online-Workshop :: Organisation und Kommunikation bei Radio free FM
Es werden die rechtlichen Strukturen bei Radio free FM nach außen sowie der innere Aufbau (Gremien, Ämter) dargestellt. Zentrale Fragen dieses Bausteins sind: Wie komme ich zu einer eigenen Sendung? Welche Bedeutung hat die Houseordnung? Welche Infrastruktur (Schnittplätze, Rechtsreader etc.) ist im Sender vorhandenen? Wie und wann kann ich diese nutzen?
Referent: Rainer Walter
Online-Link: https://meeting.bz-bm.de/radiobaukastenfreefm
Pflicht für Neueinsteiger, kostenlos für Mitglieder
Anmeldung unter: ausbildung[at]freefm.de 