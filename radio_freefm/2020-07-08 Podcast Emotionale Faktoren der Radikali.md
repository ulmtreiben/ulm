---
id: "3239403782746802"
title: Podcast "Emotionale Faktoren der Radikalisierung"
start: 2020-07-08 15:00
end: 2020-07-08 15:30
locationName: Radio free FM
address: Platzgasse 18, 89073 Ulm
link: https://www.facebook.com/events/3239403782746802/
image: 104371229_1160169814369779_3018209125177664100_o.jpg
teaser: Das Projekt "Vielfalt Leben in Ulm in der einen Welt" möchte Stereotypen
  abbauen und für Vielfalt und gegen Rassismus und Diskriminierung eintreten. D
isCrawled: true
---
Das Projekt "Vielfalt Leben in Ulm in der einen Welt" möchte Stereotypen abbauen und für Vielfalt und gegen Rassismus und Diskriminierung eintreten. Die Podcast-Reihe behandelt Themen wie Diversity, Menschenrechte, Demokratie oder Extremismus. Heute zu Gast ist Moema Smago von Inside Out. Sie berichtet über die emotionalen Faktoren, die bei Radikalisierungsprozessen eine wichtige Rolle spielen. Was bedeutet eigentlich Radikalisierung? Welche Faktoren spielen hier eine Rolle? Was bedeutet das für die Präventionsarbeit?
Live zu hören bei Radio free FM, 102.6 MHz, am 08.07.2020 um 15.00 Uhr oder danach als Podcast bei www.freefm.de