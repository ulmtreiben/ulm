---
id: "400423897906087"
title: "Online-Workshop :: Organisation und Komm. bei Radio free FM"
start: 2021-07-29 18:00
end: 2021-07-29 20:00
address: Radio free FM
link: https://www.facebook.com/events/400423897906087/
image: 136051392_10158927728041668_4356847480373391704_n.jpg
isCrawled: true
---
Online-Workshop :: Organisation und Kommunikation bei Radio free FM

Es werden die rechtlichen Strukturen bei Radio free FM nach außen sowie der innere Aufbau (Gremien, Ämter) dargestellt. Zentrale Fragen dieses Bausteins sind: Wie komme ich zu einer eigenen Sendung? Welche Bedeutung hat die Houseordnung? Welche Infrastruktur (Schnittplätze, Rechtsreader etc.) ist im Sender vorhandenen? Wie und wann kann ich diese nutzen?

Referent: Rainer Walter
Online-Link: https://online-seminar.bz-bm.de/bbb/rad-tqf-njz
Pflicht für Neueinsteiger, kostenlos für Mitglieder

Anmeldung unter: ausbildung[at]freefm.de 