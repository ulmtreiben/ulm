---
id: "273789967108197"
title: Podcast "Ramadan in Coronazeiten"
start: 2020-05-13 15:00
end: 2020-05-13 15:30
locationName: Radio free FM
address: Platzgasse 18, 89073 Ulm
link: https://www.facebook.com/events/273789967108197/
image: 95643238_1126999334353494_1477401688204640256_o.jpg
teaser: Das Projekt "Vielfalt Leben in Ulm in der einen Welt" möchte Stereotypen
  abbauen und für Vielfalt und gegen Rassismus und Diskriminierung eintreten. D
isCrawled: true
---
Das Projekt "Vielfalt Leben in Ulm in der einen Welt" möchte Stereotypen abbauen und für Vielfalt und gegen Rassismus und Diskriminierung eintreten. Die Podcast-Reihe behandelt Themen wie Diversity, Menschenrechte, Demokratie oder Extremismus. Heute zu Gast ist die Islamwissenschaftlerin Derya Sahan von der Fachstelle Extremismusdistanzierung. Sie erklärt uns die Ideen des Ramadan und was die aktuellen Situation für die Muslime bedeutet.
Live zu hören bei Radio free FM, 102.6 MHz,  am 13.05.2020 um 15.00 Uhr oder danach als Podcast bei www.freefm.de