---
id: "323643868662010"
title: 'Podcast: "Vorurteile: Frauen und technische Berufe?"'
start: 2020-08-05 15:00
end: 2020-08-05 15:30
locationName: Radio free FM
address: Platzgasse 18, 89073 Ulm
link: https://www.facebook.com/events/323643868662010/
teaser: Das Projekt "Vielfalt Leben in Ulm in der einen Welt" möchte Stereotypen
  abbauen und für Vielfalt und gegen Rassismus und Diskriminierung eintreten. D
isCrawled: true
---
Das Projekt "Vielfalt Leben in Ulm in der einen Welt" möchte Stereotypen abbauen und für Vielfalt und gegen Rassismus und Diskriminierung eintreten. Die Podcast-Reihe behandelt Themen wie Diversity, Menschenrechte, Demokratie oder Extremismus.
Heute zu Gast ist Prof. Dr. Yves Jeanrenaud von der Uni Ulm.
Es geht um Vorurteile gegenüber Frauen im Arbeitsfeld der MINT-Berufe (Naturwissenschaften und Technik). Welche Stereotype gibt es hier? Welches sind die Ursachen dahinter? Was kann dagegen getan werden und was wird bereits getan?
Antwort auf diese Fragen gibt es live zu hören bei Radio free FM, 102.6 MHz, am 05.08.2020 um 15.00 Uhr oder danach als Podcast bei www.freefm.de
(Foto: Elvira Eberhardt) 