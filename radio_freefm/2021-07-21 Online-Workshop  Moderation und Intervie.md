---
id: "1860039554148284"
title: "Online-Workshop :: Moderation und Interview"
start: 2021-07-21 19:00
end: 2021-07-21 21:00
address: Radio free FM
link: https://www.facebook.com/events/1860039554148284/
image: 136026422_10158927637556668_6761649336396424516_n.jpg
isCrawled: true
---
Online-Workshop :: Moderation und Interview

Regeln für die Vorbereitung und für die Fragestellung eines Interviews; Technik (Umgang mit Mikro; Studio-Technik für Telefon-Interview). Theoretische Vorbereitung auf die moderierte Sendung: Aufteilung, Jingles, Musikeinsatz, Präsentation und Nachrichten. "Bei Radio free FM beobachtete Fehler" mit Hörbeispielen. (Nicht: Moderationsschulung im Studio.)

Referent: Clemens Grote

Online-Link: https://online-seminar.bz-bm.de/bbb/rad-tqf-njz

Pflicht für Neueinsteiger, kostenlos für Mitglieder
Anmeldung: ausbildung[at]freefm.de