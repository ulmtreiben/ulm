---
id: "808047649921885"
title: "Online-Workshop :: Grundkurs Studiotechnik"
start: 2021-05-10 19:00
end: 2021-05-10 21:00
link: https://www.facebook.com/events/808047649921885/
image: 154253660_10159061106716668_6050879176145103330_o.jpg
teaser: "Online-Workshop: Grundkurs Studiotechnik  In diesem Baustein lernt ihr die
  Technik im Sendestudio kennen und bedienen. Mischpult, Jingle-Player, CD-Pl"
isCrawled: true
---
Online-Workshop: Grundkurs Studiotechnik

In diesem Baustein lernt ihr die Technik im Sendestudio kennen und bedienen. Mischpult, Jingle-Player, CD-Player, Plattenspieler, Tapedeck, Mikrofone, Durchführung von Telefoninterviews, Mitschnitte der Sendung.
Referent: Julius Taubert

Online-Link:
https://online-seminar.bz-bm.de/bbb/rad-tqf-njz

Pflicht für Neueinsteiger, kostenlos für Mitglieder!

Anmeldung: ausbildung[at]freefm.de