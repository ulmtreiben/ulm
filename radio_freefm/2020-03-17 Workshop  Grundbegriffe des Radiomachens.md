---
id: "2277437102546731"
title: "Workshop :: Grundbegriffe des Radiomachens"
start: 2020-03-17 19:00
end: 2020-03-17 21:00
locationName: Radio free FM
address: Platzgasse 18, 89073 Ulm
link: https://www.facebook.com/events/2277437102546731/
image: 80803637_10157827333691668_5866712197173870592_o.jpg
teaser: "Workshop Grundbegriffe des Radiomachens und rechtliche
  Grundlagen:  Nichtkommerzieller, öffentlich-rechtlicher, kommerzieller Hörfunk
  - die Anfänge de"
isCrawled: true
---
Workshop Grundbegriffe des Radiomachens und rechtliche Grundlagen:

Nichtkommerzieller, öffentlich-rechtlicher, kommerzieller Hörfunk - die Anfänge der freien Radios als Piratenradios, Frequenzen & Marktanalysen, warum die Politiker lieber sogenannte Jugendradios lizenzieren als die Hörfunk produzierende Jugend zu fördern, Werbung und Schleichwerbung und Freunde und Feinde im Ätherdschungel.

Referent: Friedrich Hog

Für Mitglieder kostenlos, Pflicht für Neueinsteiger!

Anmeldung: ausbildung[at]freefm.de