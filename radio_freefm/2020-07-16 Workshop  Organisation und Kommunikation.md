---
id: "2455595891357478"
title: "Online-Workshop :: Organisation und Komm. bei Radio free FM"
start: 2020-07-16 18:00
end: 2020-07-16 20:00
locationName: Radio free FM
address: Platzgasse 18, 89073 Ulm
link: https://www.facebook.com/events/2455595891357478/
image: 95897737_10158269671616668_4701678816072826880_o.jpg
teaser: "Online-Workshop :: Organisation und Kommunikation bei Radio free FM  Es
  werden die rechtlichen Strukturen bei Radio free FM nach außen sowie der
  inner"
isCrawled: true
---
Online-Workshop :: Organisation und Kommunikation bei Radio free FM

Es werden die rechtlichen Strukturen bei Radio free FM nach außen sowie der innere Aufbau (Gremien, Ämter) dargestellt. Zentrale Fragen dieses Bausteins sind: Wie komme ich zu einer eigenen Sendung? Welche Bedeutung hat die Houseordnung? Welche Infrastruktur (Schnittplätze, Rechtsreader etc.) ist im Sender vorhandenen? Wie und wann kann ich diese nutzen?

Referent: Rainer Walter

Online-Link: https://meeting.bz-bm.de/radiobaukastenfreefm 

Pflicht für Neueinsteiger, kostenlos für Mitglieder 

Anmeldung unter: ausbildung[at]freefm.de