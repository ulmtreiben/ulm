---
id: "580724182825995"
title: "Online-Workshop: Grundkurs Studiotechnik"
start: 2020-07-24 17:00
end: 2020-07-24 19:00
link: https://www.facebook.com/events/580724182825995/
image: 96743727_10158302154726668_6430164962416525312_o.jpg
teaser: "Online-Workshop: Grundkurs Studiotechnik  In diesem Baustein lernt ihr die
  Technik im Sendestudio kennen und bedienen. Mischpult, CD-Player, Plattensp"
isCrawled: true
---
Online-Workshop: Grundkurs Studiotechnik

In diesem Baustein lernt ihr die Technik im Sendestudio kennen und bedienen. Mischpult, CD-Player, Plattenspieler, Tapedeck, Mikrofone, Durchführung von Telefoninterviews, Mitschnitte der Sendung. 

Referent: Julius Taubert

Online-Link: 
https://meeting.bz-bm.de/radiobaukastenfreefm


Pflicht für Neueinsteiger, kostenlos für Mitglieder!