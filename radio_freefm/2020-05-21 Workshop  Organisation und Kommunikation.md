---
id: "2455595898024144"
title: "Workshop :: Organisation und Kommunikation bei Radio free FM"
start: 2020-05-21 18:00
end: 2020-05-21 20:00
locationName: Radio free FM
address: Platzgasse 18, 89073 Ulm
link: https://www.facebook.com/events/2455595898024144/
image: 80803629_10157827344246668_2476633663639388160_o.jpg
teaser: Es werden die rechtlichen Strukturen bei Radio free FM nach außen sowie der
  innere Aufbau (Gremien, Ämter) dargestellt. Zentrale Fragen dieses Baustei
isCrawled: true
---
Es werden die rechtlichen Strukturen bei Radio free FM nach außen sowie der innere Aufbau (Gremien, Ämter) dargestellt. Zentrale Fragen dieses Bausteins sind: Wie komme ich zu einer eigenen Sendung? Welche Bedeutung hat die Houseordnung? Welche Infrastruktur (Schnittplätze, Rechtsreader etc.) ist im Sender vorhandenen? Wie und wann kann ich diese nutzen?

Referent: Rainer Walter
Pflicht für Neueinsteiger, kostenlos für Mitglieder 

Anmeldung unter: ausbildung[at]freefm.de