---
id: "280642900536857"
title: Podcast mit Mirjam Keita-Schlosser "Weibliche Genitalbeschneidung"
start: 2021-08-18 15:00
end: 2021-08-18 15:30
address: Radio free FM
link: https://www.facebook.com/events/280642900536857/
image: 234700219_1472839306436160_3078083818954052815_n.jpg
isCrawled: true
---
Die Podcast-Reihe des Sachgebiets "Chancengerechtigkeit und Vielfalt" der Stadt Ulm.

Mit Mirjam Keita-Schlosser vom Projekt  WeMuFra (Weg der mutigen Frau) bei der Gleichstellungsstelle des Landratsamts Neu-Ulm reden wir über das Thema weibliche Genitalbeschneidung.

Live zu hören bei Radio free FM, 102.6 MHz, am 18.08.2021 um 15.00 Uhr oder danach als Podcast bei www.freefm.de


In der Podcast-Reihe "Diversity" des Teams Chancengerechtigkeit und Vielfalt der @stadtulm und @radio_freefm geht es um Themen wie Vielfalt, Inklusion, Menschenrechte, Demokratie oder Extremismusprävention. 
Dazu produzieren wir mit Expert*innen zu dem jeweiligen Thema regelmäßig einen Podcast.