---
id: "1906581449511600"
title: "Online-Workshop: Einführung in die Gewaltfreie Kommunikation"
start: 2021-09-23 18:00
end: 2021-09-23 20:00
address: Radio free FM
link: https://www.facebook.com/events/1906581449511600/
image: 241974615_10159495697601668_8197983947781578126_n.jpg
isCrawled: true
---
Kooperation durch Aufrichtigkeit und Verständnis

Eine Einführung in die Gewaltfreie Kommunikation von Marshall B. Rosenberg

Wenn wir mit jemandem Schwierigkeiten haben, ist die Versuchung groß, die Schuld beim anderen zu suchen. Oder wir nehmen etwas persönlich und sind tagelang verschnupft. Die Lösung ist klar: Änderte sich der andere, gäbe es keine Probleme – glauben wir. Erfolgreicher ist es allerdings, selbst dazuzulernen.

Gewaltfreie Kommunikation  - auch Wertschätzende Kommunikation genannt - zeigt, wie es gelingt:

– uns aufrichtig mitzuteilen – ohne Kritik oder Vorwurf

– den Gegenüber zu verstehen – auch wenn ich nicht einverstanden bin

– selbstbewusst klare Bitten auszusprechen – statt Forderungen zu stellen

– mich für meine Werte stark zu machen – ohne andere zu verurteilen

– sich für eigene Anliegen einzusetzen – ohne Recht haben zu müssen

– unerwünschtes Verhalten anzusprechen – ohne andere zu kränken

– Kritik, Angriffe und Vorwurfe zu hören – ohne sie persönlich zu nehmen.

In diesem praxisnahen Workshop lernst du ein leicht verständliches Modell kennen, das dir hilft, eine wertschätzende Haltung (dir selbst und anderen gegenüber) zu leben. Lust mitzumachen? Dann komm gerne!

Workshop-Kanal:
https://online-seminar.bz-bm.de/bbb/rad-tqf-njz

Referent/-in: 

Dipl.-Psych. Adelheid Schmidt
Trainerin und Coach (Gewaltfreie Kommunikation)

Anmeldung:
ausbildung[at]freefm.de