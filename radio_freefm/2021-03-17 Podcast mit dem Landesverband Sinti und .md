---
id: "350806026145830"
title: Podcast mit dem Landesverband Sinti und Roma
start: 2021-03-17 15:00
end: 2021-03-17 15:30
link: https://www.facebook.com/events/350806026145830/
image: 158447903_1363087490744676_1738532641838820899_o.jpg
teaser: Die Podcast-Reihe der internationalen Stadt Ulm. Heute zu Gast sind Andreas
  Hoffmann-Richter, Melanie Bächle, Jessica Kemfelja, Iris Rüsing vom Landes
isCrawled: true
---
Die Podcast-Reihe der internationalen Stadt Ulm.
Heute zu Gast sind Andreas Hoffmann-Richter, Melanie Bächle, Jessica Kemfelja, Iris Rüsing vom Landesverband deutscher Sinti & Roma. Wir reden über die Arbeit des Verbandes und der Beratungsstelle in Ulm, sowie über das Pilotprojekt Refit (Regionale Förderung und Inklusion) in Ulm. Außerdem über die Bildungsstudie des RomnoKher (Haus für Bildung, Kultur und Antiziganismusforschung) und über die Lernangebote des Verbandes.

Live zu hören bei Radio free FM, 102.6 MHz, am 17.03.2021 um 15.00 Uhr oder danach als Podcast bei www.freefm.de 
