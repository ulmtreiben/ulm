---
id: "3496223300398908"
title: Studio Technik / Ausspielung und Jingles
start: 2020-11-30 17:00
end: 2020-11-30 19:00
link: https://www.facebook.com/events/3496223300398908/
image: 121310765_10158735503696668_1507011692626490331_o.jpg
teaser: "Online-Workshop: Studio Technik/ Ausspielung und Jingles  Technisch wird sich
  viel im Studio ändern. Ab 31.12.20 wird unser jetziger Jingleplayer durc"
isCrawled: true
---
Online-Workshop: Studio Technik/ Ausspielung und Jingles

Technisch wird sich viel im Studio ändern. Ab 31.12.20 wird unser jetziger Jingleplayer durch eine neue Cartwall ersetzt.
Diese Software kann jedoch mehr als nur unsere Jingles spielen. Sie dient auch als Ausspielung über unsere Song Mediathek.
Eine Übersicht über das Programm, die Mediathek und wie Ihr eure eigene Cartwall mit eurem Opener und Jingles erstellt erhaltet ihr in diesem Workshop.

Online-Kanal für den Workshop:
https://meet.google.com/qqo-owmv-tho

Pflicht-Workshop für alle Redakteure!

Referent: Timo Freudenreich

Anmeldung: 
ausbildung[at]freefm.de
