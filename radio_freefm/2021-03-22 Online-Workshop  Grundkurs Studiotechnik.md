---
id: "237664068030445"
title: "Online-Workshop :: Grundkurs Studiotechnik"
start: 2021-03-22 19:00
end: 2021-03-22 21:00
link: https://www.facebook.com/events/237664068030445/
image: 154485359_10159061104861668_3028343811333866798_o.jpg
teaser: "Online-Workshop: Grundkurs Studiotechnik In diesem Baustein lernt ihr die
  Technik im Sendestudio kennen und bedienen. Mischpult, Jingle-Player, CD-Pla"
isCrawled: true
---
Online-Workshop: Grundkurs Studiotechnik
In diesem Baustein lernt ihr die Technik im Sendestudio kennen und bedienen. Mischpult, Jingle-Player, CD-Player, Plattenspieler, Tapedeck, Mikrofone, Durchführung von Telefoninterviews, Mitschnitte der Sendung.

Referent: Julius Taubert
Online-Link:
https://online-seminar.bz-bm.de/bbb/rad-tqf-njz

Pflicht für Neueinsteiger, kostenlos für Mitglieder!

Anmeldung: ausbildung[at]freefm.de