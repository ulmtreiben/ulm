---
id: "695877991086553"
title: "Online-Workshop :: Grundbegriffe des Radiomachens"
start: 2021-07-20 19:00
end: 2021-07-20 21:00
address: Radio free FM
link: https://www.facebook.com/events/695877991086553/
image: 135753233_10158927616061668_1329505506823758103_n.jpg
isCrawled: true
---
Online-Workshop Grundbegriffe des Radiomachens und rechtliche Grundlagen:
Nichtkommerzieller, öffentlich-rechtlicher, kommerzieller Hörfunk - die Anfänge der freien Radios als Piratenradios, Frequenzen & Marktanalysen, warum die Politiker lieber sogenannte Jugendradios lizenzieren als die Hörfunk produzierende Jugend zu fördern, Werbung und Schleichwerbung und Freunde und Feinde im Ätherdschungel.
Referent: Friedrich Hog

Online-Link:
https://online-seminar.bz-bm.de/bbb/rad-tqf-njz

Für Mitglieder kostenlos, Pflicht für Neueinsteiger!

Anmeldung: ausbildung[at]freefm.de 