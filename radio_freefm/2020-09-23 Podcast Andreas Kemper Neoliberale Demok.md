---
id: "996436657502122"
title: Podcast Andreas Kemper "Neoliberale Demokratiefeindlichkeit"
start: 2020-09-23 15:00
end: 2020-09-24 15:30
locationName: Radio free FM
address: Platzgasse 18, 89073 Ulm
link: https://www.facebook.com/events/996436657502122/
teaser: Das Projekt "Vielfalt Leben in Ulm in der einen Welt" möchte Stereotypen
  abbauen und für Vielfalt und gegen Rassismus und Diskriminierung eintreten. D
isCrawled: true
---
Das Projekt "Vielfalt Leben in Ulm in der einen Welt" möchte Stereotypen abbauen und für Vielfalt und gegen Rassismus und Diskriminierung eintreten. Die Podcast-Reihe behandelt Themen wie Diversity, Menschenrechte, Demokratie oder Extremismus. 
Heute zu Gast der Soziologe Andreas Kemper. Er klärt uns über die aktuelle neoliberale Demokratiefeindlichkeit auf. Wie ist die AfD entstanden und wie ist sie finanziert? Wer steckt alles dahinter? Wie ist die aktuelle Lage innerhalb der AfD? Welche Rolle spielt der Verfassungsschutz?

Live zu hören bei Radio free FM, 102.6 MHz, am 23.09.2020 um 15.00 Uhr oder danach als Podcast bei www.freefm.de 