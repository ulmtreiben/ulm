---
id: "528102451222117"
title: Podcast "Verschwörungstheorien und Fake News"
start: 2020-04-29 15:00
end: 2020-04-29 15:30
locationName: Radio free FM
address: Platzgasse 18, 89073 Ulm
link: https://www.facebook.com/events/528102451222117/
image: 93806151_1116575858729175_6156479067810955264_o.jpg
teaser: Das Projekt "Vielfalt Leben in Ulm in der einen Welt" möchte Stereotypen
  abbauen und für Vielfalt und gegen Rassismus und Diskriminierung eintreten. D
isCrawled: true
---
Das Projekt "Vielfalt Leben in Ulm in der einen Welt" möchte Stereotypen abbauen und für Vielfalt und gegen Rassismus und Diskriminierung eintreten. Die neue Podcast-Reihe behandelt Themen wie Diversity, Menschenrechte, Demokratie oder Extremismus. Heute zu Gast ist Mathieu Coquelin von der Fachstelle Extremismusdistanzierung. Es geht um
"Verschwörungstheorien und Fake News".
Live zu hören bei Radio free FM am 29.04.20 um 15.00 Uhr oder danach als Podcast bei www.freefm.de