---
id: "903458153752659"
title: "Online-Workshop :: free FM Datenbank"
start: 2021-02-23 18:00
end: 2021-02-23 20:00
link: https://www.facebook.com/events/903458153752659/
image: 151280470_10159041796676668_244373622595766746_n.jpg
teaser: "Datenbank Workshop  Einführung in die neue free FM Datenbank für Nutzer,
  Plattform-Koordinatoren, -moderatoren und weitere Multiplikatoren.  Referent:"
isCrawled: true
---
Datenbank Workshop

Einführung in die neue free FM Datenbank für Nutzer, Plattform-Koordinatoren, -moderatoren und weitere Multiplikatoren.

Referent: Sabine Fratzke

Online-Link: https://meeting.bz-bm.de/radiobaukastenfreefm

Anmeldung unter: ausbildung[at]freefm.de