---
id: "1098762423902085"
title: "Online-Workshop :: Grundkurs Studiotechnik"
start: 2021-02-12 19:00
link: https://www.facebook.com/events/1098762423902085/
image: 144110201_10158991314261668_6739369715645763047_o.jpg
teaser: "Online-Workshop: Grundkurs Studiotechnik  In diesem Baustein lernt ihr die
  Technik im Sendestudio kennen und bedienen. Mischpult, CD-Player, Plattensp"
isCrawled: true
---
Online-Workshop: Grundkurs Studiotechnik

In diesem Baustein lernt ihr die Technik im Sendestudio kennen und bedienen. Mischpult, CD-Player, Plattenspieler, Tapedeck, Mikrofone, Durchführung von Telefoninterviews, Mitschnitte der Sendung.
Referent: Julius Taubert
Online-Link:
https://meeting.bz-bm.de/radiobaukastenfreefm
Pflicht für Neueinsteiger, kostenlos für Mitglieder!