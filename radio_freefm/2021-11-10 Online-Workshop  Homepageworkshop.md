---
id: "555348619044543"
title: "Online-Workshop :: Homepageworkshop"
start: 2021-11-10 19:00
end: 2021-11-10 21:00
address: Radio free FM
link: https://www.facebook.com/events/555348619044543/
image: 241660449_10159487858261668_7425809857742840672_n.jpg
isCrawled: true
---

Einführung in die neue free FM Webseite
Workshopinhalte sind unter anderem:

- Anlegen von Usern
- Tagging von Inhalten
- Erstellung von Audios, Artikeln und Playlisten

Referent: Dominic Köstler

Online-Kanal:
https://online-seminar.bz-bm.de/bbb/rad-tqf-njz

Anmeldung unter:
ausbildung[at]freefm.de