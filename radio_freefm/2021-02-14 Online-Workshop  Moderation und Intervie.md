---
id: "238239504634057"
title: "Online-Workshop :: Moderation und Interview"
start: 2021-02-14 17:00
end: 2021-02-14 19:00
link: https://www.facebook.com/events/238239504634057/
image: 149319266_10159029776496668_7992103180278030388_o.jpg
teaser: "Online-Workshop :: Moderation und Interview  Regeln für die Vorbereitung und
  für die Fragestellung eines Interviews; Technik (Umgang mit Mikro; Studio"
isCrawled: true
---
Online-Workshop :: Moderation und Interview

Regeln für die Vorbereitung und für die Fragestellung eines Interviews; Technik (Umgang mit Mikro; Studio-Technik für Telefon-Interview). Theoretische Vorbereitung auf die moderierte Sendung: Aufteilung, Jingles, Musikeinsatz, Präsentation und Nachrichten. "Bei Radio free FM beobachtete Fehler" mit Hörbeispielen. (Nicht: Moderationsschulung im Studio.)
Referent: Clemens Grote

Online-Link: https://meeting.bz-bm.de/radiobaukastenfreefm

Pflicht für Neueinsteiger, kostenlos für Mitglieder

Anmeldung: ausbildung[at]freefm.de