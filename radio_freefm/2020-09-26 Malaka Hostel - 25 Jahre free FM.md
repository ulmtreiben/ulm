---
id: "593631981264050"
title: "Radio-/Wohnzimmerkonzert: Malaka Hostel - 25 Jahre free FM"
start: 2020-09-26 19:00
end: 2020-09-26 22:00
locationName: Gleis 44
address: Schillerstraße 44, 89077 Ulm
link: https://www.facebook.com/events/593631981264050/
teaser: "Radio free FM präsentiert:  Malaka Hostel   Radio-/Wohnzimmerkonzert in der
  Gleis-Bar und im Radio! Es gibt nur begrenzte Tickets, die ihr bei Radio f"
isCrawled: true
---
Radio free FM präsentiert:

Malaka Hostel 

Radio-/Wohnzimmerkonzert in der Gleis-Bar und im Radio! Es gibt nur begrenzte Tickets, die ihr bei Radio free FM On Air und auf dem free FM Instagram gewinnen könnt – auf geht´s! <3 

Die Sendung zur Show wird am Samstag ab 19 Uhr auf der 102,6 MHz und im Livestream auf www.freefm.de zu hören sein! 

Ab 21.30 Uhr ist die Gleis-Bar regulär geöffnet.

-----------------

Wenn der Wahnwitz brodelt, fühlen sich die Weltenbummler zuhause. Und ihr Zuhausedas ist zuallererst die Bühne. Doch was heißt hier Bühne? Wenn Malaka Hostel auftreten, reißen sie spielend die unsichtbare Mauer zwischen Band und Publikum nieder.
Sie singen spanisch, deutsch, tschechisch oder englisch – Musik ist ihre ‚World Language‘. Folkloristisches, egal ob vom Balkan oder aus den Anden, findet seinen Platz – getragen von Malaka Hostels groovenden Beats, während die aus Trompeten und Mundharmonika bestehende Brass Section lossprudelt. 
Da finden Ska, Polka, BalkanBeats und Rockriffs genauso gut zusammen wie Gipsy-Swing, Folk oder griechischer Rebetiko.

-------------------------

Radio free FM gem. GmbH
Platzgasse 18
89073 Ulm

Wir danken für die Unterstützung durch das Ministerium für Wissenschaft, Forschung und Kunst Baden-Württemberg.

