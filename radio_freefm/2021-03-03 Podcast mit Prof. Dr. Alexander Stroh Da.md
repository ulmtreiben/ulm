---
id: "133724355320922"
title: 'Podcast mit Prof. Dr. Alexander Stroh "Das SDG 16: Frieden, Gerechtigkeit am
  Beispiel Afrika"'
start: 2021-03-03 15:00
end: 2021-03-03 15:30
link: https://www.facebook.com/events/133724355320922/
image: 156394938_1357542744632484_4117463130164310627_n.jpg
teaser: Die Podcast-Reihe der internationalen Stadt Ulm. Heute zu Gast ist Professor
  Dr. Alexander Stroh von der Kulturwissenschaftliche Fakultät der Universi
isCrawled: true
---
Die Podcast-Reihe der internationalen Stadt Ulm.
Heute zu Gast ist Professor Dr. Alexander Stroh von der Kulturwissenschaftliche Fakultät der Universität Bayreuth.
Was bedeutet das SDG 16? Wie kann man verlässlichen Zugang zur Justiz erreichen? Was sind inklusive Institution? Gibt es einen inklusiven Prozess dieser Gestaltung? Welche afrikanischen Ländern  haben diese Prozesse bereits begonnen?

Live zu hören bei Radio free FM, 102.6 MHz, am 03.03.2021 um 15.00 Uhr oder danach als Podcast bei www.freefm.de 
