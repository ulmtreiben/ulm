---
id: "168791071219353"
title: "Workshop :: Grundkurs Studiotechnik"
start: 2020-02-20 18:00
end: 2020-02-20 19:00
locationName: Radio free FM
address: Platzgasse 18, 89073 Ulm
link: https://www.facebook.com/events/168791071219353/
teaser: In diesem Baustein lernt ihr die Technik im Sendestudio kennen und bedienen.
  Mischpult, CD-Player, Plattenspieler, Tapedeck, Mikrofone, Durchführung v
isCrawled: true
---
In diesem Baustein lernt ihr die Technik im Sendestudio kennen und bedienen. Mischpult, CD-Player, Plattenspieler, Tapedeck, Mikrofone, Durchführung von Telefoninterviews, Mitschnitte der Sendung.

Pflicht für Neueinsteiger, kostenlos für Mitglieder!

Anmeldung: ausbildung[at]freefm.de

Referent: Rainer Walter