---
id: "829307164579442"
title: "Online-Workshop :: Redaktion und Recherche"
start: 2021-01-30 11:00
link: https://www.facebook.com/events/829307164579442/
image: 135922500_10158927731161668_1228699376995709318_o.jpg
teaser: "Online-Workshop :: Redaktion und Recherche  Wie recherchiere ich Themen im
  Radio und texte sie verständlich? Wo finde ich spannende Geschichten? Was m"
isCrawled: true
---
Online-Workshop :: Redaktion und Recherche

Wie recherchiere ich Themen im Radio und texte sie verständlich? Wo finde ich spannende Geschichten? Was macht gute "Radiosprache" aus? Und: Wie bereite ich meine Inhalte auf, bringe sie auf den Punkt und entwickle sie weiter? Wie baue ich ein Themen-Netzwerk auf?

Referent: K.W.

Online-Link:
https://meeting.bz-bm.de/radiobaukastenfreefm

Pflicht für Neueinsteiger, kostenlos für Mitglieder!

Anmeldung: ausbildung[at]freefm.de