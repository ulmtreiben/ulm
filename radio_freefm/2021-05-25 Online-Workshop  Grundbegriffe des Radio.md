---
id: "151490529856842"
title: "Online-Workshop :: Grundbegriffe des Radiomachens"
start: 2021-05-25 17:00
end: 2021-05-25 19:00
link: https://www.facebook.com/events/151490529856842/
image: 135745341_10158927614911668_8808384587542613494_o.jpg
teaser: "Online-Workshop Grundbegriffe des Radiomachens und rechtliche
  Grundlagen:  Nichtkommerzieller, öffentlich-rechtlicher, kommerzieller Hörfunk
  - die Anf"
isCrawled: true
---
Online-Workshop Grundbegriffe des Radiomachens und rechtliche Grundlagen:

Nichtkommerzieller, öffentlich-rechtlicher, kommerzieller Hörfunk - die Anfänge der freien Radios als Piratenradios, Frequenzen & Marktanalysen, warum die Politiker lieber sogenannte Jugendradios lizenzieren als die Hörfunk produzierende Jugend zu fördern, Werbung und Schleichwerbung und Freunde und Feinde im Ätherdschungel.
Referent: Friedrich Hog

Online-Link:
https://online-seminar.bz-bm.de/bbb/rad-tqf-njz

Für Mitglieder kostenlos, Pflicht für Neueinsteiger!

Anmeldung: ausbildung[at]freefm.de 