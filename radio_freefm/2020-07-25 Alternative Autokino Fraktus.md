---
id: "287792939125805"
title: "Alternative Autokino: Fraktus"
start: 2020-07-25 20:15
end: 2020-07-26 00:00
address: Bodelschwinghweg 1, 89160 Dornstadt
link: https://www.facebook.com/events/287792939125805/
teaser: "#25JahrefreeFM Radio free FM und der Obstwiesenfestival e.V.
  präsentieren:  Alternative Autokino: Fraktus Von Westbam bis Scooter, von
  Blixa Bargeld b"
isCrawled: true
---
#25JahrefreeFM
Radio free FM und der Obstwiesenfestival e.V. präsentieren:

Alternative Autokino: Fraktus
Von Westbam bis Scooter, von Blixa Bargeld bis Dieter Meier: Die Electronic-Szene ist sich einig - FRAKTUS waren es. FRAKTUS haben Techno erfunden. Haben seine Ästhetik, seine Klangrevolte, seine Technik vorweggenommen. Aber dennoch sind FRAKTUS ein Mythos geblieben. Trotz vielversprechender Anfangserfolge in den 80er-Jahren und ihrem einzigartigen Sound. Zwar enorm einflussreich und hochgeschätzt von Szene-Kollegen weltweit, aber als Band vor über 25 Jahren im Streit auseinander gegangen und heute nur noch Insidern ein Begriff. Was wurde aus ihnen? Was machen die drei heute? Kann es ein Comeback dieser Pioniere geben? Die Dokumentation spürt die drei grundverschiedenen Musiker auf: In Hamburg, in Brunsbüttel und auf Ibiza findet Musikproduzent Roger Dettner die deutschen Techno-Urväter. Und ihm gelingt die pophistorische Sensation: Er bringt sie an einen Tisch und FRAKTUS wieder ins Studio. Nach 25 Jahren schließt sich ein Kreis: FRAKTUS treten wieder auf. Die Musikhistorie wird neu geschrieben. Diesmal mit FRAKTUS als zentralem Kapitel. Geschichte vergisst wahre Erfinder nie. Selbst FRAKTUS nicht.

Eine Abendkasse ist eingerichtet! 

Spielregeln:
Um einen reibungslosen Ablauf zu garantieren benötigen wir eure Unterstützung und bitten um die Einhaltung unserer Spielregeln.
Schon jetzt vielen Dank für eure Unterstützung.

1. Pro Fahrzeug sind maximal 2 Personen zugelassen

2. Die Teilnahme ist in jedem Fahrzeug und auch mit dem Fahrrad, Campingstuhl und Picknickdecke möglich!

3. Die Altersfreigabe der Filme ist zu beachten und wird bei der Einfahrt kontrolliert

4. Die Tonübertragung erfolgt über Radio free FM. Bitte schaltet kurz vor dem Film die Frequenz von free FM 102,6 ein, dort informieren wir euch über die Frequenz für die Tonübertragung

5. Bitte schaltet während des Filmes die Beleuchtung eurer Fahrzeuge aus

6. Das Verlassen des Geländes während der Vorstellung ist nicht gestattet

7. Das Verlassen des Fahrzeuges ist nur für Toilettengänge sowie den Erwerb von Getränken und Snacks gestattet. Wir empfehlen euch bereits mit dem Ticket Getränke und Snacks zu erwerben. Die aktuell gültigen Hygieneregeln sind dabei einzuhalten. Bitte haltet mind. 1,5 m Abstand und tragt beim Verlassen des Fahrzeuges unbedingt eine Mund- und Nasenmaske

8. An- und Abfahrt
Die Zu- und Abfahrt erfolgt über den Hubertusweg. Bei Ankunft werdet ihr und euer Fahrzeug auf den Parkplatz geleitet und dort durch unsere Ordner auf die Parkfläche eingewiesen. Die Einfahrt der Fahrzeuge ist für ein Zeitfenster von 20:15 Uhr - 21:15 Uhr geplant. Die Abfahrt des Platzes erfolgt reihenweise nach Anweisung durch das Ordnerpersonal.

9. Für eventuelle Pannen (in erster Linie leere Autobatterien) ist ein KFZ-Techniker vor Ort

10. Was passiert, wenn es regnet?
Das Gute ist, wir können den Film auch bei Regen spielen. Tritt während der Veranstaltung Starkregen oder Sturm auf, werden wir die Vorstellung abbrechen oder bei entsprechender Vorhersage rechtzeitig absagen. Wenn der Film bereits begonnen hat, erfolgt keine Erstattung. Sollte der Film vorsorglich abgesagt werden müssen, erhaltet ihr den Eintrittspreis zurück, da es keinen Ersatztermin geben kann. Cool wäre es natürlich, wenn ihr das Tickets dann an Radio free FM spenden würdet. 
