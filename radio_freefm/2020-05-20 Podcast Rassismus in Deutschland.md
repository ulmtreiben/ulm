---
id: "307786150210471"
title: Podcast "Rassismus in Deutschland"
start: 2020-05-20 15:00
end: 2020-05-20 15:30
locationName: Radio free FM
address: Platzgasse 18, 89073 Ulm
link: https://www.facebook.com/events/307786150210471/
image: 96208505_1129270854126342_3777287075267608576_o.jpg
teaser: Das Projekt "Vielfalt Leben in Ulm in der einen Welt" möchte Stereotypen
  abbauen und für Vielfalt und gegen Rassismus und Diskriminierung eintreten. D
isCrawled: true
---
Das Projekt "Vielfalt Leben in Ulm in der einen Welt" möchte Stereotypen abbauen und für Vielfalt und gegen Rassismus und Diskriminierung eintreten. Die Podcast-Reihe behandelt Themen wie Diversity, Menschenrechte, Demokratie oder Extremismus. Heute zu Gast ist der Journalist und Referent Eric Bassene. Er berichtet über seine Erfahrungen mit Rassismus in Deutschland. Wir erhalten anhand seiner persönlichen Lebensgeschichte einen Einblick in Alltagsrassismus.
Live zu hören bei Radio free FM, 102.6 MHz,  am 20.05.2020 um 15.00 Uhr oder danach als Podcast bei www.freefm.de