---
id: "370845657723490"
title: "Online-Workshop :: Moderation und Interview"
start: 2021-11-24 19:00
end: 2021-11-24 21:00
link: https://www.facebook.com/events/370845657723490/
image: 235314965_10159431690311668_8552005694652845338_n.jpg
isCrawled: true
---
Online-Workshop :: Moderation und Interview

Regeln für die Vorbereitung und für die Fragestellung eines Interviews; Technik (Umgang mit Mikro; Studio-Technik für Telefon-Interview). Theoretische Vorbereitung auf die moderierte Sendung: Aufteilung, Jingles, Musikeinsatz, Präsentation und Nachrichten. "Bei Radio free FM beobachtete Fehler" mit Hörbeispielen. (Nicht: Moderationsschulung im Studio.)
Referent: Clemens Grote

Online-Link: https://online-seminar.bz-bm.de/bbb/rad-tqf-njz

Pflicht für Neueinsteiger, kostenlos für Mitglieder
Anmeldung: ausbildung[at]freefm.de