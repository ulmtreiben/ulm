---
id: "1825113527640510"
title: Podcast mit Andreas Zumach zur "Gerechten Verteilung von Impfstoffen"
start: 2021-02-10 15:00
end: 2021-02-10 15:30
link: https://www.facebook.com/events/1825113527640510/
image: 148864929_1343823866004372_7492266130950623982_n.jpg
teaser: Die Podcast-Reihe mit Themen wie Diversity, Menschenrechte, Demokratie oder
  Extremismus.  Heute zu Gast ist der Journalist Andreas Zumach. Wie ist die
isCrawled: true
---
Die Podcast-Reihe mit Themen wie Diversity, Menschenrechte, Demokratie oder Extremismus. 
Heute zu Gast ist der Journalist Andreas Zumach. Wie ist die bisherige weltweite Verteilung von Impfstoffen geregelt?  Welche international vereinbarten Maßnahmen gibt es und greifen diese? Wie wird eine gerechte Verteilung sichergestellt?
Live zu hören bei Radio free FM, 102.6 MHz, am 10.02.2021 um 15.00 Uhr oder danach als Podcast bei www.freefm.de 
