---
id: "1121644928229218"
title: "Online-Workshop :: Redaktion und Recherche"
start: 2020-09-26 11:00
end: 2020-09-26 13:00
link: https://www.facebook.com/events/1121644928229218/
teaser: "Online-Workshop :: Redaktion und Recherche Wie recherchiere ich Themen im
  Radio und texte sie verständlich? Wo finde ich spannende Geschichten? Was ma"
isCrawled: true
---
Online-Workshop :: Redaktion und Recherche
Wie recherchiere ich Themen im Radio und texte sie verständlich? Wo finde ich spannende Geschichten? Was macht gute "Radiosprache" aus? Und: Wie bereite ich meine Inhalte auf, bringe sie auf den Punkt und entwickle sie weiter? Wie baue ich ein Themen-Netzwerk auf?
Referent: K.W.
Online-Link:
https://meeting.bz-bm.de/radiobaukastenfreefm
Pflicht für Neueinsteiger, kostenlos für Mitglieder!
Anmeldung: ausbildung[at]freefm.de