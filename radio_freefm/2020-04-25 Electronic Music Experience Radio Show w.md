---
id: "182587786515310"
title: Electronic Music Experience Radio Show /w Alex R & Friends
start: 2020-04-25 22:00
end: 2020-04-26 00:00
locationName: Radio free FM
address: Platzgasse 18, 89073 Ulm
link: https://www.facebook.com/events/182587786515310/
image: 87964124_2707573029296031_3413415427315859456_o.jpg
teaser: "Empfang: 102,6 mhz Ukw Ulm u. Umgebung Kabel 97,7 Mhz und 93,45 Mhz on
  webstream --> www.freefm.de   Infos soon...."
isCrawled: true
---
Empfang: 102,6 mhz Ukw Ulm u. Umgebung Kabel 97,7 Mhz und 93,45 Mhz on webstream --> www.freefm.de


Infos soon....