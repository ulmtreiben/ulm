---
id: "884046235454159"
title: "Online-Workshop :: Grundbegriffe des Radiomachens"
start: 2020-09-22 17:00
end: 2020-09-22 19:00
link: https://www.facebook.com/events/884046235454159/
teaser: "Online-Workshop Grundbegriffe des Radiomachens und rechtliche Grundlagen:
  Nichtkommerzieller, öffentlich-rechtlicher, kommerzieller Hörfunk - die Anfä"
isCrawled: true
---
Online-Workshop Grundbegriffe des Radiomachens und rechtliche Grundlagen:
Nichtkommerzieller, öffentlich-rechtlicher, kommerzieller Hörfunk - die Anfänge der freien Radios als Piratenradios, Frequenzen & Marktanalysen, warum die Politiker lieber sogenannte Jugendradios lizenzieren als die Hörfunk produzierende Jugend zu fördern, Werbung und Schleichwerbung und Freunde und Feinde im Ätherdschungel.
Referent: Friedrich Hog
Online-Link:
https://meeting.bz-bm.de/radiobaukastenfreefm
Für Mitglieder kostenlos, Pflicht für Neueinsteiger!
Anmeldung: ausbildung[at]freefm.de