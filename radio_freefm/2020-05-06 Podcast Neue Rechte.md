---
id: "173361773870055"
title: Podcast "Neue Rechte"
start: 2020-05-06 15:00
end: 2020-05-06 15:30
locationName: Radio free FM
address: Platzgasse 18, 89073 Ulm
link: https://www.facebook.com/events/173361773870055/
image: 94618693_1121076814945746_3291450085665144832_o.jpg
teaser: Das Projekt "Vielfalt Leben in Ulm in der einen Welt" möchte Stereotypen
  abbauen und für Vielfalt und gegen Rassismus und Diskriminierung eintreten. D
isCrawled: true
---
Das Projekt "Vielfalt Leben in Ulm in der einen Welt" möchte Stereotypen abbauen und für Vielfalt und gegen Rassismus und Diskriminierung eintreten. Die neue Podcast-Reihe behandelt Themen wie Diversity, Menschenrechte, Demokratie oder Extremismus. Heute zu Gast ist Andreas Hässler von der Fachstelle mobirex - Mobile Beratung gegen Rechts . Es geht um die "Neue Rechte". Wer/was sind die neuen Rechte? Was bezwecken sie und wie agieren sie?
Live zu hören bei Radio free FM am 06.05.20 um 15.00 Uhr oder danach als Podcast bei www.freefm.de