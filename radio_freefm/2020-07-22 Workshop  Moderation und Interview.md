---
id: "303137127243433"
title: "Online-Workshop :: Moderation und Interview"
start: 2020-07-22 19:00
end: 2020-07-22 21:00
locationName: Radio free FM
address: Platzgasse 18, 89073 Ulm
link: https://www.facebook.com/events/303137127243433/
image: 95559918_10158269655611668_4784481357428424704_o.jpg
teaser: "Online-Workshop :: Moderation und Interview  Regeln für die Vorbereitung und
  für die Fragestellung eines Interviews; Technik (Umgang mit Mikro; Studio"
isCrawled: true
---
Online-Workshop :: Moderation und Interview

Regeln für die Vorbereitung und für die Fragestellung eines Interviews; Technik (Umgang mit Mikro; Studio-Technik für Telefon-Interview). Theoretische Vorbereitung auf die moderierte Sendung: Aufteilung, Jingles, Musikeinsatz, Präsentation und Nachrichten. "Bei Radio free FM beobachtete Fehler" mit Hörbeispielen. (Nicht: Moderationsschulung im Studio.)

Referent: Clemens Grote

Online-Link: 
https://meeting.bz-bm.de/radiobaukastenfreefm

Pflicht für Neueinsteiger, kostenlos für Mitglieder

Anmeldung: ausbildung[at]freefm.de