---
id: "2864304350507584"
title: "Online-Workshop :: Grundbegriffe des Radiomachens"
start: 2021-03-30 19:00
end: 2021-03-30 21:00
link: https://www.facebook.com/events/2864304350507584/
image: 135583425_10158927613941668_5987702895683324535_o.jpg
teaser: "Online-Workshop Grundbegriffe des Radiomachens und rechtliche Grundlagen:
  Nichtkommerzieller, öffentlich-rechtlicher, kommerzieller Hörfunk - die Anfä"
isCrawled: true
---
Online-Workshop Grundbegriffe des Radiomachens und rechtliche Grundlagen:
Nichtkommerzieller, öffentlich-rechtlicher, kommerzieller Hörfunk - die Anfänge der freien Radios als Piratenradios, Frequenzen & Marktanalysen, warum die Politiker lieber sogenannte Jugendradios lizenzieren als die Hörfunk produzierende Jugend zu fördern, Werbung und Schleichwerbung und Freunde und Feinde im Ätherdschungel.
Referent: Friedrich Hog
Online-Link:
https://online-seminar.bz-bm.de/bbb/rad-tqf-njz

Für Mitglieder kostenlos, Pflicht für Neueinsteiger!

Anmeldung: ausbildung[at]freefm.de 