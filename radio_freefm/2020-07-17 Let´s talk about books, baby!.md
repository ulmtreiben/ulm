---
id: "608778686612192"
title: Let´s talk about books, baby!
start: 2020-07-16 19:30
end: 2020-07-16 22:00
locationName: Stadtbibliothek Ulm
address: Vestgasse 1, 89073 Ulm
link: https://www.facebook.com/events/608778686612192/
image: 86971024_10158004173566668_5272746472431943680_o.jpg
teaser: "#25JahrefreeFM  Zum free FM-Geburtstag unterhält sich Martin Gaiser,
  Redakteur der Literatursendung „Freunde reden Tacheles“ mit Autorin Fee Katrin
  Ka"
isCrawled: true
---
#25JahrefreeFM

Zum free FM-Geburtstag unterhält sich Martin Gaiser, Redakteur der Literatursendung „Freunde reden Tacheles“ mit Autorin Fee Katrin Kanzler, Musiker Joo Kraus und Fotografin Martina Strilic über deren Lieblingsbücher in gemütlicher Lounge-Atmosphäre. 

Schauspieler Markus Hummel liest Ausschnitte aus den Texten.

Liveübertragung auf der 102,6 MHz und www.freefm.de