---
id: "529458681517036"
title: "Online-Workshop :: Homepageworkshop"
start: 2021-07-19 18:00
end: 2021-07-19 20:00
link: https://www.facebook.com/events/529458681517036/
image: 204520455_10159343117626668_1974223882912106382_n.jpg
isCrawled: true
---
Einführung in die neue free FM Webseite

Workshopinhalte sind unter anderem:
- Anlegen von Usern
- Tagging von Inhalten
- Erstellung von Audios, Artikeln und Playlisten

Referent: Dominic Köstler

Online-Kanal:
https://online-seminar.bz-bm.de/bbb/rad-tqf-njz

Anmeldung unter: 

ausbildung[at]freefm.de 