---
id: "2568269570148828"
title: "Online-Workshop :: Homepageworkshop"
start: 2021-09-20 18:00
end: 2021-09-20 20:00
address: Radio free FM
link: https://www.facebook.com/events/2568269570148828/
image: 241668719_10159487855386668_948201900988118649_n.jpg
isCrawled: true
---
Einführung in die neue free FM Webseite
Workshopinhalte sind unter anderem:

- Anlegen von Usern
- Tagging von Inhalten
- Erstellung von Audios, Artikeln und Playlisten

Referent: Dominic Köstler

Online-Kanal:
https://online-seminar.bz-bm.de/bbb/rad-tqf-njz
Anmeldung unter:

ausbildung[at]freefm.de