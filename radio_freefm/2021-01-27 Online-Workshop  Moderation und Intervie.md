---
id: "765975780673961"
title: "Online-Workshop :: Moderation und Interview"
start: 2021-01-27 17:00
end: 2021-01-27 19:00
link: https://www.facebook.com/events/765975780673961/
image: 135739394_10158927624171668_1695428989431228666_o.jpg
teaser: "Online-Workshop :: Moderation und Interview Regeln für die Vorbereitung und
  für die Fragestellung eines Interviews; Technik (Umgang mit Mikro; Studio-"
isCrawled: true
---
Online-Workshop :: Moderation und Interview
Regeln für die Vorbereitung und für die Fragestellung eines Interviews; Technik (Umgang mit Mikro; Studio-Technik für Telefon-Interview). Theoretische Vorbereitung auf die moderierte Sendung: Aufteilung, Jingles, Musikeinsatz, Präsentation und Nachrichten. "Bei Radio free FM beobachtete Fehler" mit Hörbeispielen. (Nicht: Moderationsschulung im Studio.)
Referent: Clemens Grote
Online-Link: https://meeting.bz-bm.de/radiobaukastenfreefm
Pflicht für Neueinsteiger, kostenlos für Mitglieder
Anmeldung: ausbildung[at]freefm.de