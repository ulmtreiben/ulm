---
id: "1660921590784109"
title: Let's talk about books, baby
start: 2021-08-13 20:00
locationName: Stadtbibliothek Ulm
address: Ulm
link: https://www.facebook.com/events/1660921590784109/
image: 231196718_10159425921861668_8141290663086464167_n.jpg
isCrawled: true
---
Am 13.08. um 20 Uhr wird es eine Live-Veranstaltung mit dem Titel "Let's talk about books, baby!" geben. Vier Personen (siehe unten) werden über vier Bücher sprechen/diskutieren, jede Teilnemerin, jeder Teilnehmer hat dazu ein Buch ausgesucht. Martin Gaiser, Redakteur der Redaktion „Freunde reden Tacheles“ wird die Moderation übernehmen, der Schauspieler Herbert Schäfer wird ausgewählte Passagen aus jedem Buch vorlesen. 

Ort der Veranstaltung ist die Zentralbibliothek Ulm, verantwortliche Veranstalter sind die Zentralbibliothek Ulm und Radio freeFM. Die Veranstaltung wird live auf Radio freeFM übertragen. Der Eintritt ist frei.

Die TeilnehmerInnen und "ihre" Bücher sind:

- Fee Katrin Kanzler - Stefanie Sourlier: Das weisse Meer

- Edith Ehrhardt - Yvonne Hergane: Die Chamäleondamen

- Dagmar Engels - Ian McEwan: Kakerlake

- Imran Ayata - Nicolas Mathieu: Rose Royal

Anmeldungen unter stadtbibliothek@ulm.de oder unter 0731/161-4101

Auf den Steinstufen des Freilichtforums werden dünne Filzkissen bereitgestellt. Wer es bequemer möchte, darf sich gern eigene Kissen mitbringen.