---
id: "357862008833943"
title: Podcast mit Dr. Klaudia Tietze "Demokratieförderung in Betrieben"
start: 2020-11-04 15:00
end: 2020-11-04 15:30
locationName: Radio free FM
address: Platzgasse 18, 89073 Ulm
link: https://www.facebook.com/events/357862008833943/
image: 123037446_1268083866911706_6359596822701561651_n.jpg
teaser: Das Projekt "Vielfalt Leben in Ulm in der einen Welt" möchte Stereotypen
  abbauen und für Vielfalt und gegen Rassismus und Diskriminierung eintreten. D
isCrawled: true
---
Das Projekt "Vielfalt Leben in Ulm in der einen Welt" möchte Stereotypen abbauen und für Vielfalt und gegen Rassismus und Diskriminierung eintreten. Die Podcast-Reihe behandelt Themen wie Diversity, Menschenrechte, Demokratie oder Extremismus. 
Heute zu Gast ist Dr. Klaudia Tietze. Sie ist die Geschäftsführerin des Vereins "Mach' meinen Kumpel nicht an! – für Gleichbehandlung, gegen Fremdenfeindlichkeit und Rassismus e.V. ", gelbe Hand. Mit ihr reden wir über die Demokratieförderung in Betrieben. Was kann man tun bei Rassismus oder Extremismus am Arbeitsplatz? Wie gehe ich mit rechten Sprüchen um? Welche besonderen Angebote für Azubis gibt es?

Live zu hören bei Radio free FM, 102.6 MHz, am 04.11.2020.2020 um 15.00 Uhr oder danach als Podcast bei www.freefm.de 