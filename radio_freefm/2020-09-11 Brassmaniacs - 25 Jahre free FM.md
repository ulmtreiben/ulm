---
id: "655160241771869"
title: Brassmaniacs - 25 Jahre free FM
start: 2020-09-11 20:00
end: 2020-09-11 22:00
locationName: Liederkranz Kulturbiergarten
address: Friedrichsau 9, 89073 Ulm
link: https://www.facebook.com/events/655160241771869/
teaser: "Radio free FM präsentiert:  brassmaniacs   Im Jahr 2007 entschloss sich das
  als Freiheitskämpfer betitelte Funkyhorn zu einem bis dahin einmaligem Pro"
isCrawled: true
---
Radio free FM präsentiert:

brassmaniacs 

Im Jahr 2007 entschloss sich das als Freiheitskämpfer betitelte Funkyhorn zu einem bis dahin einmaligem Projekt: Es wollte mit Musik gegen die politische Ungerechtigkeit im Zauberwald vorgehen…

Dafür scharrte es zahlreiche Musiker um sich - um letztlich zu erkennen, dass Diese absolut nicht seine Sprache sprechen.
Und so ist bis heute nicht übermittelt, welche Missstände es aufdecken wollte.
Allerdings ergab sich daraus eine klassische Funkhippopsoul Kombi - Die bis heute mit dem politischen Einfluss des Funkyhorns
durch die Lande ziehen, um die Musik zu verbreiten und das Funkyhorn zu suchen, da es auf mysteriöse & tragische Weise verschwand.
Die Suche geht bei einem Kultur Kränzchen in die nächste Runde. 

Vorbei schauen ist Pflicht! Wir freuen uns schon unglaublich auf euch! 


-------------------------
Radio free FM gem. GmbH
Platzgasse 18
89077 Ulm

Wir danken für die Unterstützung durch das Ministerium für Wissenschaft, Forschung und Kunst Baden-Württemberg.