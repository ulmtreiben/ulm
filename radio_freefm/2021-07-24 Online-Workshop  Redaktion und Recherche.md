---
id: "261695758703835"
title: "Online-Workshop :: Redaktion und Recherche"
start: 2021-07-24 11:00
end: 2021-07-24 13:00
address: Radio free FM
link: https://www.facebook.com/events/261695758703835/
image: 136127908_10158927733121668_6967822689958487886_n.jpg
isCrawled: true
---
Online-Workshop :: Redaktion und Recherche

Wie recherchiere ich Themen im Radio und texte sie verständlich? Wo finde ich spannende Geschichten? Was macht gute "Radiosprache" aus? Und: Wie bereite ich meine Inhalte auf, bringe sie auf den Punkt und entwickle sie weiter? Wie baue ich ein Themen-Netzwerk auf?

Referent: K.W.

Online-Link:
https://online-seminar.bz-bm.de/bbb/rad-tqf-njz

Pflicht für Neueinsteiger, kostenlos für Mitglieder!

Anmeldung: ausbildung[at]freefm.de