---
id: "522265505530852"
title: Night On Air - Die Kulturnacht bei Radio free FM
start: 2021-09-18 17:00
end: 2021-09-18 23:30
locationName: Radio free FM
address: Platzgasse 18, 89073 Ulm
link: https://www.facebook.com/events/522265505530852/
image: 241434793_10159483189026668_5429712514969983688_n.jpg
isCrawled: true
---
Night On Air:

Passend zur Kulturnacht Ulm / Neu-Ulm lädt Radio free FM zur Night On Air.
An diesem Abend werden sich verschiedene Bands aus der Region bei Radio free FM die Klinke in die Hand geben.
Folgende Bands könnt ihr dabei Live bei uns im Büro erleben und on Air auf der 102,6 mHz:

MISCHA 
Easyman meets Carthaus 
Moltke & Mörike
Loud Packers 

Dazu bekommt ihr von uns eine Führung durch den Radiosender und die Möglichkeit ein Sendestudio hautnah mitzuerleben.