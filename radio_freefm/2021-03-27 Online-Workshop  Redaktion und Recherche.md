---
id: "1044720032694065"
title: "Online-Workshop :: Redaktion und Recherche"
start: 2021-03-27 11:00
end: 2021-03-27 13:00
link: https://www.facebook.com/events/1044720032694065/
image: 135533133_10158927731691668_8238529209575876164_o.jpg
teaser: "Online-Workshop :: Redaktion und Recherche  Wie recherchiere ich Themen im
  Radio und texte sie verständlich? Wo finde ich spannende Geschichten? Was m"
isCrawled: true
---
Online-Workshop :: Redaktion und Recherche

Wie recherchiere ich Themen im Radio und texte sie verständlich? Wo finde ich spannende Geschichten? Was macht gute "Radiosprache" aus? Und: Wie bereite ich meine Inhalte auf, bringe sie auf den Punkt und entwickle sie weiter? Wie baue ich ein Themen-Netzwerk auf?

Referent: K.W.

Online-Link:
https://online-seminar.bz-bm.de/bbb/rad-tqf-njz

Pflicht für Neueinsteiger, kostenlos für Mitglieder!

Anmeldung: ausbildung[at]freefm.de