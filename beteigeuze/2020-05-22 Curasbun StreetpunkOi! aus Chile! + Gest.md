---
id: "2543604212580382"
title: Curasbun Streetpunk/Oi! aus Chile! + Gestiefelt und Verkatert
start: 2020-05-22 20:00
end: 2020-05-23 01:00
locationName: Beteigeuze
address: Pfaffenäcker 1, 89075 Ulm
link: https://www.facebook.com/events/2543604212580382/
image: 87813976_2863769053716977_7938195097496059904_n.jpg
teaser: Curasbun - Streetpunk/Oi aus Chile  Zum ersten mal in Ulm, eigentlich ein muss
  für jeden der Oi oder Punk mag und Bands vom anderen Ende der Welt komm
isCrawled: true
---
Curasbun - Streetpunk/Oi aus Chile

Zum ersten mal in Ulm, eigentlich ein muss für jeden der Oi oder Punk mag und Bands vom anderen Ende der Welt kommen jetzt auch nicht so oft, also hört euch rein und kommt vorbei

https://www.facebook.com/CurasbunOficial/

https://www.youtube.com/watch?v=QpLv-fh7Xgk&t=947s

https://www.youtube.com/watch?v=HoZO_TiMeBA&feature=youtu.be&fbclid=IwAR3lJs8pbr_nL0Z5Hmv9AfVf2NNVK9Lj6xhbbjj1iCK_KQcOuhYbTRZ6TrE

Achja und als Support gibts noch:

Gestiefelt und Verkatert
Bester Worst Oi! Punk aus Nürtingen

https://www.facebook.com/Gestiefelt-und-Verkatert-684404345072395/