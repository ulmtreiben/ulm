---
id: "435024267133463"
title: Wonk Unit vs. Kommando Walter - Beteigeuze Westend Ulm
start: 2021-04-01 20:00
end: 2021-04-02 01:00
locationName: Beteigeuze
address: Pfaffenäcker 1, 89075 Ulm
link: https://www.facebook.com/events/435024267133463/
image: 89503571_2900472843379931_2703119323655831552_o.jpg
teaser: "VVK !!!!  :    http://punk.de/ticketshop.php?ProdID=1310   KOMMANDO WALTER:
  Applaus für the Reverend Walter, Junior Kong Walter,  Johnny Lee Walter, B"
isCrawled: true
---
VVK !!!!  :    http://punk.de/ticketshop.php?ProdID=1310 

KOMMANDO WALTER:
Applaus für the Reverend Walter, Junior Kong Walter,  Johnny Lee Walter, Bunny Walter, Alias Gott Walter, Waldo Walter am Bass, an den Gitarren, am Keyboard, an den Mics, Posaune, an der Schießbude und an sonstigen Krach erzeugenden Mitteln!!
Tanzbeinskapunk aus Ulm, mit eigenen deutschen Liedern, vereint auf mal amüsante und mal ernste Weise Einflüsse von Punk/Rock über 80er NDW mit Reggae/Ska. Entertainment garantiert!

https://youtu.be/H8mJoI3QB94

https://youtu.be/GpNr9QWUf94

https://www.facebook.com/kommandowalter24/



WONK UNIT ist eine der vielseitigsten und abwechslungsreichsten Bands, die London im Moment zu bieten hat. 2005 wurden sie von Alex nach Auflösung der Punkband The Flying Medallions ins Leben gerufen. 
Punk in England war so tot und langweilig zu der Zeit. Doch mit dem zufälligen Aufeinandertreffen von Alex und zwei sehr talentierten Musikern, Drummer Mez Clough und Gitarrist Gavin Kinch, deren Art zu spielen Alex Songs plötzlich überfrisch erklingen ließen, gab es eine neue Punkrockhoffnung. 
Eine in Europa einzigartige Band mit eigenem Sound, eigenem Humor, eigenem Festival und tief verwurzeltem DIY Background. Alex Brindle Johnson ist dabei als Lead Singer, Poet, Skater, Stage Performer und kreativer Motor das Herz der Band, von der nicht wenige behaupten, dass es das derzeit Spannendste sei, was es auf Europas Punkrockbühnen zu sehen gibt. Dazu passend Musik und Songs, die sich nicht im Geringsten um Genregrenzen scheren und die doch zu jeder Zeit Alex Brindle Johnson und damit Wonk Unit sind. Punk braucht seit Jahren neuen Input und Wonk Unit liefern genau das. 
Auf der Bühne wird Mastermind Alex zur Dampfwalze und zelebriert ein Feuerwerk voller Energie, Wut und Ironie. Textlich verarbeitet Alex sein Tagebuch. Dabei geht es zum Beispiel um Großmütter, Freundinnen, die sich in den Finger schneiden und Züge. Eine Band, die live einfach nur Spass macht !!!!

https://youtu.be/k9gWy1LClYI

https://youtu.be/PG5rDMiy60E

www.facebook.com/wonkunitband