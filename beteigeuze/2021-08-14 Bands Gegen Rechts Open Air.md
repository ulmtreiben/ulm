---
id: "543104773372387"
title: "Stürmt die Burg: Bands Gegen Rechts [Konzert]"
start: 2021-08-14 18:00
end: 2021-08-15 00:10
address: Wilhelmsburg, 89075 Ulm
link: https://www.facebook.com/events/543104773372387/
image: 202684753_3924034691028813_6172031993357809545_n.jpg
isCrawled: true
---
Bands gegen Rechts - Das Ulmer Spendenkonzert zu Gunsten Opfer Rechter Gewalt. 

Veranstaltungsort: Bühne im Innenhof

Eintritt Frei, um eine Spende wird gebeten.

Unterstützt von: 
Kultur in Ulm 
https://www.facebook.com/kultur.in.ulm
Beteigeuze
https://www.facebook.com/beteigeuzeulm

Line Up:

KOMMANDO WALTER
„Wird sind dein Punky Reggae Party 6 Mann Kommando aus Ulm mit Pauken und Trompeten.Mal lustig, mal ernst immer gegen menschenverachtendeIdeologien!“

BLEEDING FINGERS
Turn On, Tune In, Drop Out! Hip Shakin’ Alternative Rock zum Auftakt des diesjährigen "Bands gegen Rechts" Konzertabends. In klassischer Besetzung, zwei Gitarren, Bass, Schlagzeug und mehrstimmiger Gesang, laden die vier erfahrenen Musiker zu einem Trip ein, die trüben Zeiten hinter sich zu lassen. Da wird das Vorspiel zum Höhepunkt. Also rechtzeitig kommen!http://BleedingFingers.de

JESUS GEORGE 
Jesus George ist...... wenn sich an einem Abend Lenny Kravitz Selig an Amy Winehouse lehnt– gestützt durch die Kings Of Leon, gezogen von den Red Hot Chili Peppers, gedrückt von The Knack und abgeholt durch die Sportfreunde Stiller.“Vor zehn Jahren als Weihnachtsprojekt geboren, mit Musikern aus dem Raum Ulm, schnürt Jesus George, derenName aus einem Zitat von Michael J. Fox aus „Zurück in die Zukunft" stammt, ein  überraschendes Paket mit Rock und Roll von den 1960ern bis heute. Bei sechs Geschmäckern kommt ein Mix zustande, der unverkrampft und mit Kultcharakter auf der Bühne gespielt wird. Cover  –  „eigene Ideen und Kreativität einbringen“ lautet die Devise von Jesus George, die mit Klassikern wie „Cometogether", „GimmeShelter“, den Ramones , Clash und den Pixies jede Menge Spaß haben...Wie?Gitarre - Gitarre – Bass - Schlagzeug - Gesang - GesangKontakt. www.jesus-george.deinfo@jesus-george.defacebook: jesusgeorge

HOMESICK HOBOS
Die Homesick Hobos aus dem Rottal-Delta zwischen Ulm und Biberach gelten als Geheimtipp.Die Band hat sich ganz dem hausgemachten Blues verschrieben.Das bis zu 4- stündige Programm live zu erleben heißt dynamisch, bluesig relaxt abzudriften,und den Swinging- Jumpblues in den Gelenken zu spüren.Homesick Hobos ist Bluespower pur, die in ihrer kompakten Einheit, ihrer lockeren Art und ihrer großen Spielfreude zu begeistern weiß.Die Geschichte der Band begann in den 90er Jahren.Die Band formierte sich im Sommer 2001 mit dem Ziel, dem Live- Blues wieder Leben einzuhauchen. Seitdem spielt die Band in zahlreichen Kneipen, Privatpartys und Musiknächten.......anhören lohnt sich!!

ACOUSTIC CASE
Zwei GITARREN und ein SCHLAGWERK. Gute Songs aus den 60-ern bis heute. Von SIMON & GARFUNKEL über U2 zu WANDA und NEIL YOUNG und VIELEN MEHR. Kontakt: 01754008052 oder 015112372347 oder fb


#StürmtdieBurg2021 
Vom 30. Juli bis 29. August 2021 darf die Wilhelmsburg jeweils von Mittwoch bis Sonntag gestürmt werden! Wie im vergangenen Sommer steht „Stürmt die Burg" im Zeichen von #kulturerhalten und bietet Kulturschaffenden eine langersehnte Bühne und Räumlichkeiten zur Darbietung von Musik, Literatur, Theater, Tanz, Kunst und Kinderprogramm.
Nicht nur auf der Bühne, sondern auch neben der Bühne ist in diesem Jahr viel geboten: Audiovisuelle Performances, Audiowalks, ein Theaterprojekt der adk, FabienneYoga über den Dächern Ulms und sechs Installationen in den Innenräumen der Bundesfestung bieten ein abwechslungsreiches Programm. Über 80 verschiedene Akteure beteiligen sich am Pop up Space auf der Wilhelmsburg, mit dabei sind das Ulmer Zelt, die popbastion.ulm, die vh ulm, die Literaturwoche Donau, das Internationale Donaufest, der Arbeitskreis Kultur, Bands gegen Rechts und Broken Stage.

#Familienprogramm 
Die Sonntage stehen ganz im Zeichen der Familie: Das Clown-Theater „Der kleine Clown", der Märchenparcours „Die rätselhafte Märchenstunde", das Konzert der Kinderband Blindfische, eine Luftartistik-Show sowie Aktionen, Mitmach- und Bastelangebot des Donaubüros, des Ulmer Zelts und kontiki-Ateliers der vh ulm bieten ein buntes Ferienprogramm. Auch Erwachsene werden sonntags bestens unterhalten: Jeweils am Nachmittag und frühen Abend sorgen Konzerte lokaler Bands von Jazz über Pop bis hin zu Tango für einen tollen Wochenausklang.

#Gastronomie_Shuttlebus 
Auch in diesem Sommer bietet die Burgbar No. XII erfrischende Drinks und leckeres Essen an allen Veranstaltungstagen. Für weitere Informationen zur Gastronomie, besucht die Burgbar-Webseite: www.burgbar12.de
Wie gehabt bringt Euch ein Shuttlebus kostenlos vom Hans-und-Sophie-Scholl-Platz in der Stadtmitte oder von der Haltestelle Frauenstraße (Höhe Gold Ochsen Brauerei) auf die Wilhelmsburg und bis 24 Uhr wieder zurück in die Stadt.

#Corona_Hinweis
Zur Sicherheit aller haltet euch bitte an die aktuell gültigen Hygiene- und Abstandsregelungen – weitere Informationen veröffentlichen wir vor dem Opening von Stürmt die Burg unter Berücksichtigung der dann geltenden Corona-Bestimmungen. Wir minimieren die Infektionsgefahr durch professionelles Management und bitten euch, rücksichtsvoll mit den anderen Gästen umzugehen. Bei Krankheitssymptomen bleibt bitte zuhause. Stay safe & stay tuned!

#WeitereInfos
Das Programm und weitere Infos findet ihr im Juli auf www.die-wilhelmsburg.de

#Unterstützung 
Der Pop up Space wird von der Kulturabteilung der Stadt Ulm koordiniert und von der Brauerei Gold Ochsen unterstützt. Alle Infos unter: www.die-wilhelmsburg.de