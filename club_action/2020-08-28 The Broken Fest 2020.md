---
id: "507385876591963"
title: The Broken Fest 2021
start: 2021-08-27 20:00
end: 2021-08-28 23:59
locationName: Club Action
address: Beim Alten Fritz 3, 89075 Ulm
link: https://www.facebook.com/events/507385876591963/
image: 103592443_834519133739407_5360942457206120315_n.jpg
isCrawled: true
---
@[571340699559301:274:Broken Stage] proudly presents:

THE BROKEN FEST

Line-Up:
tba


NEU: U18 Eintritt frei!!!
Open Air im @[155650217807116:274:Club Action]
Beim Alten Fritz 3, Ulm
(bei schlechtem Wetter findet die Veranstaltung drinnen statt)

INFOS:
Covid-19 (Corona):
Nun ist es offiziell: Es wird dieses Jahr kein The Broken Fest geben. Neuer Termin: 27./28.08.2021


Essen & Getränke:
Damit ihr nicht während eurer Lieblingsband zum Dönerstand in die Stadt rennen müsst, weil euch der Magen in den Kniekehlen hängt, wird es auch dieses Jahr wieder einen Essensstand geben. Im Club Action an der Bar gibt es außerdem die gewohnten alkoholischen und anti-alkoholischen Getränke.

Anfahrt & Parken:
Bus: Der Club Action ist nur etwa 15 Geh-Minuten von Innenstadt und Hauptbahnhof entfernt. Wer trotzdem mit dem Bus fahren will, die Linie 2 und Linie 5 hält direkt davor. Vom Hbf aus Richtung "Wissenschaftsstadt" oder "Science Park II", Haltestelle "Lehrer Tal" aussteigen.
Auto: Von der Kienlesbergstraße aus in die Straße "Beim Alten Fritz" abbiegen und der Straße bergauf folgen. Nach etwa 200 Metern kommt, nach der Kurve, auf der linken Seite ein Tor. Das ist die Einfahrt zum Parkplatz. Vom Parkplatz aus dann einfach dem Feldweg entlang der Festungsmauer zum Konzertgelände folgen.

Sponsoren:
Bei der Auswahl unserer Sponsoren, legen wir viel Wert darauf, Unternehmen auszuwählen, welche wir unterstützenswert finden und bei welchen wir selbst gerne konsumieren. Bei unseren Sponsoren handelt es sich ausnahmslos um die Betriebe von Freunden.
Wir danken allen unseren Sponsoren herzlich für die Zusammenarbeit. Ohne unsere Sponsoren wäre das Festival nicht möglich.
Wir werden unterstützt von:

@[351528125015732:274:Hemperium]

@[112651348836023:274:No Pain No Gain Tätowierungen]

Schreinerei Necknig

@[237336456287546:274:Turbine Powered]

Außerdem wird das Festival präsentiert von:
@[1482868281974924:274:AWAY FROM LIFE]
Fragen & Anregungen:
Wenn ihr Fragen oder Vorschläge zum Festival habt, dann könnt ihr uns auf verschiedene Wege kontaktieren: In die Veranstaltung posten, Nachricht an unsere Facebook-Seite, oder eine E-Mail an info@thebrokenfest.com

LOVE MUSIC, HATE FASCISM