---
id: "547418353161137"
title: Afghanistan. Wie kam es zur jetzigen Situation?
start: 2021-09-25 19:00
locationName: Bürgerhaus Mitte
address: Schaffner Straße, 89073 Ulm
link: https://www.facebook.com/events/547418353161137/
image: 240737214_6682878028404236_4074033812047804429_n.jpg
isCrawled: true
---
Heiner Tettenborn (Anwalt in Augsburg) ist ein profunder Kenner Afghanistans. Mehrmals bereiste er das Land zwischen 2003 und 2010 und verbrachte dort insgesamt etwa ein Jahr.
Während der ganzen letzten Jahre stand er in Kontakt zu seinen dortigen Freunden und auch jetzt telefoniert er regelmäßig mit ihnen. Mit seiner Partnerin zusammen schrieb er das Buch DER UNBEUGSAME über Khazan Gul Tani – Lehrer, ehemaliger Widerstandskämpfer im Guerillakrieg gegen die Sowjetunion und zeitweilig Erziehungsminister der Provinz Khost. Die beiden hatten Khazan Gul 2004 in Afghanistan kennengelernt, woraus sich eine langjährige Freundschaft entwickelt hat.
Heiner Tettenborn hat einen eigenen Blick auf das Land, der weit über die aktuelle Berichterstattung hinausgeht. Seine Sichtweise, die geprägt ist durch die Erfahrungen mit der Kultur und Denkweise der Menschen in Afghanistan sowie durch die Sichtweisen der afghanischen Freunde, die weiterhin im Land leben, soll in seinem Vortrag zum Ausdruck kommen.
Wegen der aktuellen Ereignisse in und um Afghanistan ist es uns wichtig, diese Thematik kurzfristig in den Themenbereich der Ulmer Friedenswochen 2021 aufzunehmen.

Gruppe FRIEDENSBEWEGT ULM | Ulmer Netz für eine andere Welt e. V. | Verein Ulmer Weltladen e. V. | Förderverein des Behandlungszentrums für Folteropfer Ulm e. V.
