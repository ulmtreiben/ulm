---
id: "852380522079083"
title: Der Konflikt Israel/Palästina, Judenfeindlichkeit/Antisemitismus und unsere
  besondere Verantwortung
start: 2021-09-24 19:00
locationName: Bürgerhaus Mitte
address: Schaffner Straße, 89073 Ulm
link: https://www.facebook.com/events/852380522079083/
image: 238016282_6634981839860522_5692396418863323625_n.jpg
isCrawled: true
---
Der Konflikt Israel/Palästina, Judenfeindlichkeit/Antisemitismus und unsere besondere Verantwortung als Deutsche
Wir wollen sprechen über den Konflikt Israel-Palästina, über die aktuelle Situation und Chancen für eine gerechte Lösung. Ebenso wollen wir sprechen über Judenfeindlichkeit/Antisemitismus, über Begriffe und Definitionen und Strategien zur Bekämpfung. Schließlich geht es um die besondere Verantwortung der Deutschen und um die Grenzen des Sagbaren. Bei allem sollen diverse jüdische Stimmen in Deutschland Beachtung finden.

Referent*innen: Nirit Sommerfeld, Andreas Zumach u. a.

Veranstalter:
Verein Ulmer Weltladen e. V.

Ort:
Bürgerhaus Mitte, Schaffnerstraße 17, 89073 Ulm

Eintritt frei, Spenden erbeten

Mehr Informationen über die Ulmer Friedenswochen:
https://www.friedenswochen-ulm.de
