---
id: "4234854086597329"
title: Der allgegenwärtige Antisemit
start: 2021-09-29 19:00
link: https://www.facebook.com/events/4234854086597329/
image: 238210113_6638614526163920_3998258205281264432_n.jpg
isCrawled: true
---
Ein Ungeist geht um in Deutschland – in der Auseinandersetzung mit dem Antisemitismus werden wahllos Begriffe durcheinandergeworfen, Menschen verleumdet, Juden von Nicht-Juden des Antisemitismus bezichtigt. Die Debattenkultur in Deutschland ist vergiftet und die Realität völlig aus dem Blickfeld des Diskurses geraten. Moshe Zuckermann nimmt den aktuellen Diskurs in den Blick und plädiert für eine ehrliche Auseinandersetzung mit der deutsch-israelischen Geschichte. „Zuckermann legt den Finger in einige offene Wunden. Er macht sichtbar, wo in Deutschland und in Israel der Anti-Antisemitismus-Diskurs ins Heuchlerische umschlägt“ (Deutschlandfunk Kultur).
 
Referent: Moshe Zuckermann, Tel Aviv/Israel

Veranstalter: 
- Verein Ulmer Weltladen e. V.
- Ulmer Netz für eine andere Welt e. V.

Online-Veranstaltung, keine Gebühr

Anmeldung unter verein@ulmer-weltladen.de 

Mehr Informationen über die Ulmer Friedenswochen:
https://www.friedenswochen-ulm.de
