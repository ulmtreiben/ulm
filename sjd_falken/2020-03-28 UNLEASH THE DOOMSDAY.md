---
id: "189863192242441"
title: UNLEASH THE DOOMSDAY
start: 2020-03-28 19:00
locationName: SJD - Die Falken Ulm
address: Ziegelländeweg 3 (Oberer Donauturm), Ulm
link: https://www.facebook.com/events/189863192242441/
image: 88056608_2960214770711665_4394140972787695616_n.jpg
teaser: ¡Unleash the DOOMSDAY!  Gönnt euch am 28.03.2020 endlich wieder geilen Punk
  und Hardcore im Falkenkeller!  Gorilla BRAWL EP Release Show  https://yout
isCrawled: true
---
¡Unleash the DOOMSDAY!

Gönnt euch am 28.03.2020 endlich wieder geilen Punk und Hardcore im Falkenkeller!

Gorilla BRAWL EP Release Show 
https://youtu.be/MaJoagYtCxI

Special Guests:
Act The Fool (Hardcore Punk, Langenau)
https://actthefool.bandcamp.com

Pump Gas (Punk ‘N Roll, Weißenhorn)
https://open.spotify.com/artist/6coUBWpUIAqNi2Z4tVxwm6?si=9llwz8abSaGBnvBoRkmkug

Einlass ab 19 Uhr und gegen Spende!

Kommt für geile Mucke rum :)