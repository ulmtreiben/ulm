---
id: "1690316401167507"
title: Demo zum feministischen Kampftag
start: 2021-03-13 15:00
address: Marktplatz, 89073 Ulm
link: https://www.facebook.com/events/1690316401167507/
image: 159376726_3974070849326047_2865758530930901035_o.jpg
teaser: Zum feministischen Kampftag gehen wir zusammen auf die Straße! Auch und
  insbesondere während der weiteranhaltenden Pandemie ist es wichtig sich gegen
isCrawled: true
---
Zum feministischen Kampftag gehen wir zusammen auf die Straße! Auch und insbesondere während der weiteranhaltenden Pandemie ist es wichtig sich gegen die systematische Unterdrückung von FLINTA* Personen weltweit zu stellen. Während des Lockdowns wurde ein starker Anstieg der häuslichen Gewalttaten gegen FLINTA* vernommen und in 2021 gab es bis heute (07.03.21) schon 32 Femizide (gezielte Morde an Frauen). FLINTA* erfahren immernoch Diskrimierung, Benachteiligung u.o. Reduzierung aufgrund ihres Geschlechtes, in allen Lebensbereichen - sei es nun bei der Gehaltszahlung, der Arbeitssuche oder dem Recht zur Selbstbestimmung sowohl als auch im Privat- und Sozialleben. Die Kriminalisierung von Abtreibung wie z.B in Polen entzieht den jeweiligen Frauen die Entscheidung über ihren eigenen Körper, ähnlich wie die Kriminalisierung von Sexarbeiterinnen. 
Der allgegenwärtige Sexismus wird häufig als Alltagssexismus abgetan und durch Musiktexte, Medien und soziale Netzwerke legitimiert und reproduziert. 
Innerhalb dieser patriarchalen und kapitalistischen Zustände wird auch anhand der Gender Data Gap deutlich wie stark Frauen systematisch benachteiligt werden. 

Deswegen zeigen wir uns solidarisch, zeigen dass wir nicht einverstanden sind mit den patriarchalen Umständen und stellen uns entschlossen gegen diese! Stellt euch mit uns zusammen, anlässlich des internationalen Weltfrauentags, auf die Straße und gegen die anhaltende Diskriminierung von FLINTA*. Am Samstag den 13.03.2021 vsl. um 15 Uhr, nach den gegebenen Corona-Maßnahmen und Hygiene Regeln, was für alle Beteiligten eine Maske am Start zu haben und untereinander den Mindestabstand einzuhalten bedeutet. Dem Wetterbericht zufolge ist Regenschutz angebracht. Der voraussichtliche Veranstaltungsort ist der Münsterplatz, falls dieser nicht schon besetzt ist. Mögliche Änderungen werden schnellstmöglich bekannt gegeben.

LG der Falkenskeller <3