---
id: "2220968301543014"
title: "Talk: Aufstand oder Aussterben"
start: 2020-03-04 19:00
end: 2020-03-04 22:00
locationName: Gleis 44
address: Schillerstraße 44, 89077 Ulm
link: https://www.facebook.com/events/2220968301543014/
teaser: "Die Ortsgruppe Ulm der Klima- und Umweltbewegung Extinction Rebellion lädt
  ein zum Vortrag:  +++ Aufstand oder Aussterben? Über die Klimakatastrophe,"
isCrawled: true
---
Die Ortsgruppe Ulm der Klima- und Umweltbewegung Extinction Rebellion lädt ein zum Vortrag:

+++ Aufstand oder Aussterben? Über die Klimakatastrophe, ökologischen Kollaps und zivilen Ungehorsam. +++

Schon lange diskutieren Politiker*innen über die Klimakrise, doch die Zeit rennt uns davon! Die ganze Wahrheit über unsere katastrophale Lage muss erzählt werden. Längst steuern wir auf eine existenzbedrohende Erderwärmung von mehr als drei Grad Celsius zu und das sechste große Artensterben ist bereits in vollem Gange. Unsere derzeitige Lebensweise zerstört unsere eigene Lebensgrundlage: Kurzum, wenn wir jetzt nicht handeln, stehen wir vor dem irreversiblen Kollaps!

Wir können nicht länger auf die Schritte anderer warten, sondern müssen selbst aktiv werden. Es ist Zeit für eine bunte, gewaltfreie und internationale Rebellion. 

Informiert euch, kommt zum Vortrag und schließt euch uns an!

---

Im Anschluss an den Vortrag wird es Zeit geben, Fragen zu stellen und zu diskutieren.

Der Eintritt ist frei.

Falls Du am 
- Mi, 4. März 19 Uhr im Gleis 44
nicht dabei sein kannst, hättest Du am 
- Fr, 6. März 19 Uhr im Club Orange, Volkshochschule Ulm 
nochmals die Möglichkeit teilzunehmen. 