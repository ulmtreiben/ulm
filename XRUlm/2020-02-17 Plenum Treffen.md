---
id: "214229226273767"
title: Plenum Treffen
start: 2020-02-17 19:00
end: 2020-02-17 22:00
locationName: Bürgerhaus Mitte
address: Schaffner Straße, 89073 Ulm
link: https://www.facebook.com/events/214229226273767/
image: 83042733_1013527532363276_1052564886451650560_o.jpg
teaser: Hallo liebe Rebels,   Wir treffen uns jeden 1. & 3. Montag im Monat im
  Bürgerhaus Mitte zum Plenum. Bei diesen Treffen besprechen wir wichtige
  organis
isCrawled: true
---
Hallo liebe Rebels, 

Wir treffen uns jeden 1. & 3. Montag im Monat im Bürgerhaus Mitte zum Plenum. Bei diesen Treffen besprechen wir wichtige organisatorische Themen.

Für alle, die noch nie bei uns mitgemacht haben: Es wird auch in Zukunft regelmäßig Einführungstreffen geben, die speziell Interessierten gewidmet sind. Die nächste Gelegenheit hierfür wird im März sein (Termin kommt noch!). 

Ansonsten seid ihr aber natürlich auch sehr gerne beim Plenum gesehen. Kommt gerne vorbei!

Unser Treffpunkt: Bürgerhaus Mitte, Schaffnerstraße 17 . 
Wann? 19 Uhr, jeden 1. und 3. Montag im Monat

Wir freuen uns über jeden der kommt! :)