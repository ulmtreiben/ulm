---
name: Humboldt-Studienzentrum Uni Ulm
website: http://www.humboldt-studienzentrum.de
email: humboldt@uni-ulm.de
scrape:
  source: facebook
  options:
    page_id: humboldt.studienzentrum.uniulm
---
Mit seinem Lehrangebot will das Humboldt-Studienzentrum für Philosophie und Geisteswissenschaften die fachwissenschaftliche Ausbildung in Form zeitgemäßer akademischer Bildung erweitern.

Fragestellungen in der Philosophie setzen meist bei Alltagsphänomenen oder allgemeinen Voraussetzungen der Wissenschaften an und problematisieren diese durch Fragestellungen wie Was ist Zeit? Was ist Gerechtigkeit? Warum leben wir in einem Staat? Können moralische Überzeugungen begründet werden? Was ist Wahrheit? Können Computer denken? Wie ist Erkenntnis von der Welt möglich? Das heißt, in der Philosophie werden oft Themen oder Überzeugungen kritisch hinterfragt, die ansonsten als selbstverständlich unterstellt werden.

Ziel des Philosophiestudiums ist es dabei weniger, bestimmte Fakten zu lernen, als vielmehr die Fähigkeit zum abwägenden Reflektieren von Thesen oder Überzeugungen und zum Erkennen von oftmals stillschweigend gemachten Voraussetzungen einzuüben und weiter auszubilden.
