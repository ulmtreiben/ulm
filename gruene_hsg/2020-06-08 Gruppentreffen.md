---
id: "1183776655302385"
title: Gruppentreffen
start: 2020-06-08 20:00
end: 2020-06-08 22:00
link: https://www.facebook.com/events/1183776655302385/
image: 101265363_737516967052148_8034915944230813696_n.jpg
teaser: Wir laden euch herzlich ein zum nächsten Treffen der Grünen Hochschulgruppe.
  Wir treffen uns am Montag, den 08. Juni um 20:00 Uhr. Den Link zu unserem
isCrawled: true
---
Wir laden euch herzlich ein zum nächsten Treffen der Grünen Hochschulgruppe. Wir treffen uns am Montag, den 08. Juni um 20:00 Uhr. Den Link zu unserem Online-Treffen erhaltet ihr auf Anfrage.
Wir freuen uns über neue und alte Gesichter! 
