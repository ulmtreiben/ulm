---
id: "988117325026015"
title: Gruppentreffen
start: 2020-11-03 19:00
end: 2020-11-03 20:30
link: https://www.facebook.com/events/988117325026015/
image: 122997974_848089065994937_4116775134915446678_n.jpg
teaser: Wir laden Euch herzlich ein zum nächsten Treffen der Grünen Hochschulgruppe.
  Wir treffen uns am Dienstag, den 3. November um 19:00 Uhr. Den Link zu un
isCrawled: true
---
Wir laden Euch herzlich ein zum nächsten Treffen der Grünen Hochschulgruppe. Wir treffen uns am Dienstag, den 3. November um 19:00 Uhr. Den Link zu unserem Online-Treffen erhaltet ihr auf Anfrage.
Herzlich Einladung auch an alle Erstis. Ihr könnt einfach vorbeikommen, reinschnuppern und Euch ein Eindruck von unserer Arbeit als politische Hochschulgruppe machen.
Wir freuen uns auf Euch!  