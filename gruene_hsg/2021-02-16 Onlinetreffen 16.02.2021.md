---
id: "3385793054859173"
title: Onlinetreffen 16.02.2021
start: 2021-02-16 19:00
link: https://www.facebook.com/events/3385793054859173/
image: 151146098_919032538900589_2601181073679302806_n.jpg
teaser: Wir laden Euch herzlich ein zum nächsten Treffen der Grünen Hochschulgruppe.
  Wir treffen uns am Montag, den 16.02. um 19:00 Uhr. Den Link zu unserem O
isCrawled: true
---
Wir laden Euch herzlich ein zum nächsten Treffen der Grünen Hochschulgruppe. Wir treffen uns am Montag, den 16.02. um 19:00 Uhr. Den Link zu unserem Online-Treffen erhaltet ihr auf Anfrage.