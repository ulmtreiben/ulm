---
id: "324843268844803"
title: Onlinetreffen 01.02.2021
start: 2021-02-01 19:00
link: https://www.facebook.com/events/324843268844803/
image: 142939004_907978666672643_6386074085720257334_n.jpg
teaser: Wir laden Euch herzlich ein zum nächsten Treffen der Grünen Hochschulgruppe.
  Wir treffen uns am Montag, den 01.02. um 19:00 Uhr. Den Link zu unserem O
isCrawled: true
---
Wir laden Euch herzlich ein zum nächsten Treffen der Grünen Hochschulgruppe. Wir treffen uns am Montag, den 01.02. um 19:00 Uhr. Den Link zu unserem Online-Treffen erhaltet ihr auf Anfrage.