---
id: "846872112831421"
title: Onlinetreffen 19.01.2021
start: 2021-01-19 19:00
link: https://www.facebook.com/events/846872112831421/
image: 138057117_898999484237228_5460897557009930124_n.jpg
teaser: Am 19.01. findet unsere nächste Sitzung statt. Wenn du Lust hast teilzunehmen,
  Interesse schreib uns einfach!
isCrawled: true
---
Am 19.01. findet unsere nächste Sitzung statt. Wenn du Lust hast teilzunehmen, Interesse schreib uns einfach!