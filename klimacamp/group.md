---
name: Klimacamp Ulm
website: https://www.instagram.com/klimacampulm/
email: klimacampulm@gmail.com
scrape:
  source: facebook
  options:
    page_id: KlimacampUlm
---
Wir campen seit dem 10. September auf dem Marktplatz beim Rathaus und fordern von der Stadt Ulm einen angemessenen Umgang mit der Klimakrise. Gleichzeitig bieten wir den Bürger:innen der Städte Ulm und Neu-Ulm eine Plattform, um sich zu vernetzen und auszutauschen. Unser Programm soll über die Klimakrise aufklären und Lösungen aufzeigen.
