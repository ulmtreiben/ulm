---
id: "764149744451134"
title: Pressekonferenz GermanZero Klimaentscheid
start: 2021-01-12 17:00
link: https://www.facebook.com/events/764149744451134/
teaser: "Die Wissenschaft ist sich einig: um die globale Erderwärmung auf 1,5 °C zu
  beschränken sind drastische Maßnahmen und ein generelles Umdenken notwendig"
isCrawled: true
---
Die Wissenschaft ist sich einig: um die globale Erderwärmung auf 1,5 °C zu beschränken sind drastische Maßnahmen und ein generelles Umdenken notwendig. Dies muss auch auf lokaler Ebene geschehen!

Im Herbst 2020 haben wir 54 Tage auf dem Marktplatz gecampt und von der Stadt konsequenten Klimaschutz gefordert. Eine unserer Forderungen vom Klimacamp lautet Klimaneutralität in Ulm und Neu-Ulm. 
Jetzt planen wir gemeinsam mit GermanZero ein Bürger:innenbegehren zum Erreichen der Klimaneutralität sowohl in Ulm als auch in Neu-Ulm. 
Die Abstimmungsfrage des Bürgerbegehrens fordert letztendlich, dass ein Planungsbüro bzw. ein Bürger:innenrat beauftragt wird, zu erarbeiten wie die CO2-Emissionen in Ulm und in Neu-Ulm bis 2030 ein Nettonull erreichen können. Was wo durchgeführt wird, werden wir auslosen.

Kurz gesagt, wir wollen, dass konkrete Maßnahmen erarbeitet – und umgesetzt – werden, wie die beiden Städte bis 2030 CO2-neutral werden. 

Interesse? Noch Fragen? 
In einer Onlinepressekonferenz am 12.01.2021 um 17 Uhr werden wir erklären was ein Klimaentscheid ist, wie ein Bürger:innenrat abläuft und die Möglichkeit geben Fragen zu stellen. 

Wir sind danach auf Eure Stimmen angewiesen! Die Klimakrise betrifft uns alle und somit brauchen wir alle, um den Klimaentscheid durchzusetzen.