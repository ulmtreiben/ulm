---
id: "662972521007025"
title: Wir haben Platz! Evakuiert Moria!
start: 2020-09-17 18:30
end: 2020-09-17 20:00
locationName: Münsterplatz Ulm
address: Münsterplatz 21, 89073 Ulm
link: https://www.facebook.com/events/662972521007025/
teaser: Die Zustände auf Lesbos sind weiterhin katastrophal und ein Wiederaufbau
  Morias darf kein Thema sein. Deshalb machen wir weiter Druck und fordern „Eva
isCrawled: true
---
Die Zustände auf Lesbos sind weiterhin katastrophal und ein Wiederaufbau Morias darf kein Thema sein. Deshalb machen wir weiter Druck und fordern „Evakuiert Moria! Wir haben Platz!“. 

WICHTIG: Für diese Aktion benötigen wir ganz viele Stühle. Bringt also gerne Stühle aller Art mit, um unsere Forderung zu untermauern.