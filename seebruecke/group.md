---
name: Seebrücke Ulm
website: http://seebruecke.org
email: ulm@seebruecke.org
scrape:
  source: facebook
  options:
    page_id: Seebruecke-Ulm-321153678719088
---
Die SEEBRÜCKE ist eine internationale Bewegung, getragen von verschiedenen Bündnissen und Akteur*innen der Zivilgesellschaft. Wir sind die Ulmer Gruppe.
