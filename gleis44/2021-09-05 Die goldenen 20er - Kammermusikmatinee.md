---
id: "4200662729988254"
title: Die goldenen 20er - Kammermusikmatinee
start: 2021-09-05 11:00
locationName: Liederkranz Ulm
address: Friedrichsau 9, 89073 Ulm
link: https://www.facebook.com/events/4200662729988254/
image: 241041441_578994276786611_4730720360533606000_n.jpg
isCrawled: true
---
Die goldenen 20er

Wer denkt bei diesem Stichwort nicht sofort an Stars wie die Comedian Harmonists, Marlene Dietrich oder Zarah Leander? Wer verbindet damit nicht Lebensfreude und stilvolle, gehobene Unterhaltungskunst?

Die junge Sängerin Kristine Emde, Finalistin im Bundeswettbewerb Gesang, wird gemeinsam mit einem Klaviertrio um den Ulmer Pianisten Janis Pfeifer Lieder und Chansons aus dieser Zeit zur Aufführung bringen. Gleichzeitig stehen auch Songs aus amerikanischen Musicals dieser Zeit auf dem Programm – schliesslich fällt in die „Roaring Twenties“ auch die Entstehung des Broadway, wie er heute bekannt ist.

Das Klaviertrio über irländische Volkslieder von dem Schweizer Komponisten Frank Martin und die Suite in Tanzform von dem Berliner Operettenkomponist Oscar Straus ergänzen das Programm als Werke der klassischen Musik aus den 20er Jahren. 

Freuen Sie sich auf eine abwechslungsreiche, einzigartige Matinee, die sich auf der Grenze zwischen Klassik & Jazz und zwischen Broadway & deutschem Vorkriegsschlager bewegt.

Violine: Thomas Probst
Violoncello: Clara Berger
Klavier: Janis Pfeifer
Gesang: Kristine Emde

Der Eintritt ist frei, um Spenden wird gebeten.
Bei schlechtem Wetter findet die Veranstaltung in der Teutonia statt. 
