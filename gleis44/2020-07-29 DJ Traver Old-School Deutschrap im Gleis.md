---
id: "637699893822716"
title: DJ Traver Old-School Deutschrap im Gleisgarten
start: 2020-07-29 17:30
end: 2020-07-29 22:00
locationName: Gleis 44
address: Schillerstraße 44, 89077 Ulm
link: https://www.facebook.com/events/637699893822716/
teaser: DJ Traver versorgt euch, mithilfe einer kleinen Zeitreise in die 90er und
  2000er, mit einem DJ-Set der damaligen Deutschrap Szene und anderen Genres,
isCrawled: true
---
DJ Traver versorgt euch, mithilfe einer kleinen Zeitreise in die 90er und 2000er, mit einem DJ-Set der damaligen Deutschrap Szene und anderen Genres, die die damalige Zeit geprägt haben.

 ____________________


 Damit sich alle gut und sicher fühlen haben wir ein Hygiene-Konzept erarbeitet und werden alle Tische etc. sehr regelmäßig Desinfizieren. 
 Hier für euch kurz und bündig alle Änderungen: 
 - ihr dürft in Gruppen von maximal 10 Personen kommen und gemeinsam an einem Tisch Sitzen.
 - wir müssen eure Namen und Kontaktdaten aufnehmen und 4 Wochen speichern.
 - Bitte wartet am Eingang, unser Team weist euch Plätze zu. 
 - Leider können wir keine Stehplätze an der Theke anbieten.


 ____________________


 Gleisgarten Öffnungszeiten: 
 Sonntag - Donnerstag 17 - 23 Uhr 
 Freitag - Samstag 17 - 24 Uhr 
 Wir freuen uns auf euch! 🐇🦊🦆