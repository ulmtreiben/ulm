---
id: "303316397439566"
title: Mal Élevé Soundsystem LIVE - 25 Jahre free FM
start: 2020-09-05 17:00
end: 2020-09-05 22:00
address: Liederkranz Kulturbiergarten
link: https://www.facebook.com/events/303316397439566/
teaser: "Radio free FM und Rebellution präsentieren:  MAL ÉLEVÉ (ehem. Frontman von
  Irie Révoltés)  EINTRITT FREI!!!  Das Warmup und die Aftershow wird von Tom"
isCrawled: true
---
Radio free FM und Rebellution präsentieren:

MAL ÉLEVÉ (ehem. Frontman von Irie Révoltés)

EINTRITT FREI!!!

Das Warmup und die Aftershow wird von Tom von der "Schüttel dein Speck"-Crew bestritten 

----
Im Frühjahr 2020 brachr Mal Élevé sein erstes Solo-Album «Résistance mondiale» raus. Keine Frage: Die neuen Songs müssen auf die Bühne. Denn wie sollte er besser zum «weltweiten Widerstand» aufrufen als mit dem Mikrofon in der Hand und der geballten Faust in der Luft? Fast zwei Jahrzehnte lang war er mit seiner Band Irie Révoltés unterwegs. Weit über Deutschland hinaus brachten die Musiker mit ihrer Mischung aus Reggae, Dancehall, Ska, Rap und Punk tausende von Menschen in Bewegung. Nun setzt Mal Élevé diesen Weg als Solokünstler fort. Im Sommer 2019 meldete er sich mit der „Megafon“-EP zurück, auf der einige neue Stücke zu hören waren.
Ohne Wenn und Aber fordern seine zumeist französischen und deutschen Lyrics zum globalen Protest gegen die unhaltbaren Missstände auf, unter denen allzu viele Menschen leiden. „Ich kämpfe für eine Welt ohne Grenzen, ohne Rassismus und ohne Ausbeutung“, erklärt Mal Élevé, was ihn antreibt...EINTRITT FREI!!!


-------------------------
Radio free FM gem. GmbH
Platzgasse 18
89077 Ulm

Wir danken für die Unterstützung durch das Ministerium für Wissenschaft, Forschung und Kunst Baden-Württemberg.

