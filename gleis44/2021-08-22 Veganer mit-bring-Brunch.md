---
id: "2054868674674443"
title: Veganer "mit-bring-Brunch"
start: 2021-08-22 11:00
end: 2021-08-22 15:00
locationName: Gleis 44
address: Schillerstraße 44, 89077 Ulm
link: https://www.facebook.com/events/2054868674674443/
image: 237009970_2956975907856793_5302638170677830319_n.jpg
isCrawled: true
---
Die Earthlings Ulm und das Gleis 44 laden herzlich zu unserem Veganen Brunch ein!

Zusammen mit euch möchten wir einem veganen „mit-bring-Brunch“ gestalten. Das Ganze findet im Biergarten vom Gleis44 statt. Deswegen ist die Veranstaltung wetterabhängig und muss bei schlechtem Wetter abgesagt werden, wir halten euch auf dem Laufenden. Freut euch auf interessante Gespräche und leckeres veganes Essen.   

Auch nach dem Brunch ist noch Zeit zum gemütlichen zusammensitzen und sich auszutauschen.

Der Brunch beginnt gegen 11:00 Uhr, kommt bitte ein wenig früher damit alles vorbereitet ist, wenn es los gehen soll.   
___________________________________________________

Informationen zum Brunch: (vor Ort gehen wir mit euch den Ablauf nochmal durch)

- Du bereitest einfach ein veganes Gericht für das Buffet vor und bringst es mit passendem Schöpfer mit. Mengenmäßig so viel wie du auch in etwa essen würdest. Ohne mitgebrachte Speise kannst du nicht am Brunch teilnehmen, ansonsten ist der Eintritt kostenlos.

- Wichtig: Vegan heißt frei von ALLEN tierischen Produkten, neben Fleisch und Wurst, Geflügel und Fisch zählen dazu auch Milch, Eier, Käse, Honig, Sahne, Joghurt, Gelatine, Fleischbrühe, Butter, tierfetthaltige Margarine usw. im Zweifel gerne bei uns melden 

- Brot, Leitungswasser und Heißgetränke werden vom Veranstalter bereitgestellt, Kaltgetränke können im Biergarten erworben werden. 

- Bitte beachte, dass auf jeden Fall eigene Teller und Schüssel, Besteck und Becher mitgebracht werden müssen, Danke!

- Im Biergarten vom Gleis44 gilt die 3G Regel nicht, allerdings bitten wir euch darum am Buffet Maske zu tragen und vor dem Schöpfen die Hände zu desinfizieren. Desinfektionsmittel ist vor Ort.  

- Deine vierbeinigen Freunde dürfen gerne mitkommen, bitte nur wenn sie sich mit Ihresgleichen vertragen.

Vielen Dank an das Gleis 44 und sein Team, das wir die Räumlichkeiten gratis zur Verfügung gestellt bekommen.

Wir freuen uns auf euch, eure earthlings Ulm