---
id: "603235237160463"
title: Taste Street Food Sunday Opening
start: 2020-06-07 13:00
end: 2020-06-21 13:00
locationName: Gleis 44
address: Schillerstraße 44, 89077 Ulm
link: https://www.facebook.com/events/603235237160463/
image: 82188999_2969783236386000_888114192165371904_n.jpg
teaser: TASTE STREET FOOD SUNDAY OPENING   ❌WIRD AUF GRUND VON CORONA VERSCHOBEN
  ❌  Termin folgt !  Liebe Foodlover, ab Juni  findet einmal im Monat unser Str
isCrawled: true
---
TASTE
STREET FOOD SUNDAY OPENING 

❌WIRD AUF GRUND VON CORONA VERSCHOBEN ❌

Termin folgt !

Liebe Foodlover,
ab Juni  findet einmal im Monat unser Street Food Sunday statt.

Das kulinarische Highlight und den wohl kleinsten Street Food Markt findet Ihr im Herzen von Ulm (Bahnhofnähe) im Kulturzentrum "Gleis 44" .

Es erwarten Euch maximal 6 Foodtrucks,  erfrischende Getränke, leckere Cocktails, Lounges, verschiedene Specials, eine gemütliche und entspannte Atmosphäre.
Der moderne Biergarten wird abwechselnd von Live Musik und DJ's begleitet.

Natürlich kostet das ganze KEINEN

 "Essen ist ein Bedürfnis - Geniesen eine Kunst".
Die TASTE Crew und die Foodler freuen sich auf Euch


Ps.: für alle Streetfoodler / Foodtrucks / Hobbyköche/ Interessenten - Wir haben noch etwas Kapazität, bei Interesse einfach eine Email mit dem Betreff ,,Street Food Sunday'' an: info@Taste-Foodmarkt.de oder eine Private Nachricht an TASTE!!