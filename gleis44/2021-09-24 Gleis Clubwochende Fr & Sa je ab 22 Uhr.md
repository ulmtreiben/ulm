---
id: "858634311508605"
title: Gleis Clubwochende Fr & Sa je ab 22 Uhr
start: 2021-09-24 22:00
end: 2021-09-26 05:00
locationName: Gleis 44
address: Schillerstraße 44, 89077 Ulm
link: https://www.facebook.com/events/858634311508605/
image: 242116351_1182092962287580_5626380224064036305_n.jpg
isCrawled: true
---
Die 44 Hz Disko bringt Freitag Misch-Mukke und Techno auf die Plattenteller, am Samstag gibt es Melodic Techno, Tech-House und Trash bei der Kollektiv Schillerstraße! ❤️👌🏼💯

FR & SA je 22-05 Uhr

Außerdem gibt es ab sofort wieder regulär Fr & Sa für euch @ahispizza alá beste der Stadt / Nacht! 

See Ya! 

Die Veranstaltungen sind gefördert durch den Beauftragten der Bundesregierung für Kultur und Medien („BKM“) und die Initiative Musik. Die Veranstaltungen laufen im Programm NEUSTART KULTUR.
