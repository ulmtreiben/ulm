---
id: "518311055898765"
title: 'Kultursommer 2021: "Are you there? Rampensäue, Rampensause" in der Reithalle'
start: 2021-07-30 19:00
address: Reithalle Ulm, Schillerstraße 1
link: https://www.facebook.com/events/518311055898765/
image: 223473087_1403310596730117_8995083992248894435_n.jpg
isCrawled: true
---
Für die einen Teil der Bundesfestung, für die anderen ein zweites Zuhause: Die Reithalle ist als Treffpunkt der Ulmer Skaterszene nicht mehr wegzudenken. Statt den Rampen lassen an diesem Wochenende die Ulmer DJs Nico Bulla und Serdar Dogan die Halle krachen. Pop- und Subkultur vom besten, die am Samstag mit der Eröffnung der Literaturwoche Donau 2021 verfeinert wird.

Die ansonsten gut gehüteten Pforten öffnen sich an diesem Abend für alle die Lust haben. Nico legt seinen neusten Deephouse auf und sein Stuttgarter Freund und Rapper Schobi lässt Erinnerungen wach werden, was süddeutscher Hip-Hop noch mal bedeutet. Nico und Schobi nehmen gerade zusammen ein Album auf und vielleicht verraten sie an diesem Abend, wie sich das anhören wird.

Die Sache rund macht Serdar Dogan (Bleepgeeks), ein ebenso bekanntes Ulmer DJ-Gesicht, wie Nico. Von Serdars Tellern gibt es entspannten House zu hören, wie damals im Eden.

Live Visuals von O_POL, ein weiteres Projekt von Tausendsassa Nico Bulla, begleiten den Abend. Zu sehen ist eine Mischung aus digitalem Artwork und Aufnahmen aus alten Skater-Tagen.

Der Verein von der Reithalle kümmert sich  um Getränke.

Der Eintritt ist kostenlos -> first come/ first served

Der Kultursommer ist eine Initiative der Kulturabteilung der Stadt Ulm in Zusammenarbeit mit der Ulmer Kulturszene. Die einzelnen Programmpunkte werden von Gleis 44, Roxy Ulm oder HfG Archiv veranstaltet. Alle Beteiligten bedanken sich bei den Stadtwerke Ulm / Neu-Ulm und bei der Kulturstiftung des Bundes für die freundliche Unterstützung. Die Veranstaltungsreihe wird im Programm Kultursommer 2021 durch die Beauftragte der Bundesregierung für Kultur und Medien (BKM) mit Mitteln aus NEUSTART KULTUR gefördert.

Weitere Informationen findet ihr unter:
https://www.kultursommer-ulm.de/