---
id: "187883795692616"
title: Adrian Hermes & Transall (Krautpop/ Berlin).
start: 2020-10-01 21:00
end: 2020-10-01 23:30
locationName: Gleis 44
address: Schillerstraße 44, 89077 Ulm
link: https://www.facebook.com/events/187883795692616/
image: 83285723_766300200533527_4792557931759403008_o.jpg
teaser: "Adrian Hermes & the TRANSALL band.  Einlass: 20.30 Uhr Beginn: 21 Uhr
  Eintritt Frei. --------------------------------  ADRIAN HERMES & TRANSALL ist
  ei"
isCrawled: true
---
Adrian Hermes & the TRANSALL band. 
Einlass: 20.30 Uhr
Beginn: 21 Uhr
Eintritt Frei.
--------------------------------

ADRIAN HERMES & TRANSALL ist ein Krautpop Trio aus Berlin. 
Beeinflusst von Indierock der 90er, LoFi, Krautrock, Indiepop und früher Neue deutsche Welle, haben sie ihre ganz eigene Version von Popmusik entwickelt, die sie im Oktober auf ihrer Tour präsentieren!

Youtube: https://youtu.be/Ixub6JthCEQ
Facebook: https://www.facebook.com/adrianhermesmusic/
Insta: https://www.instagram.com/herr_hermes/
Spoti: https://open.spotify.com/artist/74tCWu05clOKs4Orv5e5RT
Bandcamp: https://adrianhermes.bandcamp.com
Home: http://www.adrianhermes.de

--------------------------------

Die Veranstaltung wird unterstützt vom Ministerium für Wissenschaft, Forschung und Kunst Baden-Württemberg.