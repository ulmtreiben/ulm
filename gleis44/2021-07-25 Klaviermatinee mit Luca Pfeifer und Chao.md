---
id: "184795943675517"
title: Klaviermatinee mit Luca Pfeifer und Chao Tian
start: 2021-07-25 10:30
locationName: Teutonia Biergarten & Restaurant
address: Friedrichsau 6, 89073 Ulm
link: https://www.facebook.com/events/184795943675517/
image: 223358355_555197352499637_1422244289100502893_n.jpg
isCrawled: true
---
!!! Wegen Regenvorhersage müssen wir in die Teutonia ausweichen und schon um 10:30 Uhr beginnen !!!

Der Ulmer Pianist Luca Pfeifer gewann 2017 den Förderpreis Junge Ulmer Kunst und studiert zusammen mit Chao Tian an der Musikhochschule Trossingen Klavier bei Prof. Raluca Chifane-Wagenhäuser. Für diese Matinee haben die beiden Pianisten solistische Werke von Mozart und Chopin vorbereitet.

Programm:

Chao Tian

F. Chopin
Klaviersonate Nr. 3 in h-moll, Op. 58
3. Largo 4. Presto, ma non tanto
F. Chopin
Nocturne Op. 27/2, in Des-Dur
F. Chopin 
Op. 25 Nr. 9 ,11

Luca Pfeifer

W. A. Mozart Klaviersonate in F-Dur, KV 533/494

F. Chopin 
Op 10 Nr 1,4