---
id: "327855134946166"
title: Fotografie Grundlagen Workshop
start: 2020-11-07 12:00
end: 2020-11-07 17:00
locationName: Gleis 44
address: Schillerstraße 44, 89077 Ulm
link: https://www.facebook.com/events/327855134946166/
image: 120567206_4072043396145675_5322167445967394468_o.jpg
teaser: Gleis 44s >hauseigener< Fotograf Moritz, verantwortlich für alle von uns
  geposteten Fotos, freut sich, sein know-how bei uns zusammen mit seinem Kolle
isCrawled: true
---
Gleis 44s >hauseigener< Fotograf Moritz, verantwortlich für alle von uns geposteten Fotos, freut sich, sein know-how bei uns zusammen mit seinem Kollegen Dominik an euch weiter zu geben. 

In unserem Grundlagen Fotografie Workshop bieten wir euch einen 3 stündigen Theorieteil und danach einen zwei stündigen Praxis Teil inklusive Fotowalk durch Ulm. Dabei werden wir die gelernten Techniken direkt ausprobieren. Der Workshop geht insgesamt ca. 5 Stunden. Wir starten um 12 Uhr in unserem Seminarraum direkt am Gleis 44. Im Theorieteil werden wir Themen durchnehmen wie: 
-	Kamera 
-	Objektive 
-	Funktion der Kameras (System/Spiegel) 
-	Verschiedenen Funktionen 
-	Iso/Blende/Belichtungszeit/Weißabgleich
-	Fokus 
-	Belichtung 
-	Dateitypen 
-	Bildgestaltung (Perspektiven / Goldener Schnitt / Drittelregel) 


Der Preis liegt bei 80 € pro Person. 




Die Teilnehmerzahl ist auf 12 Personen begrenzt. 



Anmeldungen bitte per Mail an hallo@moritz-reulein.de