---
id: "368696931548382"
title: Gleis Clubwochende Fr & Sa je ab 22 Uhr
start: 2021-09-10 22:00
end: 2021-09-12 05:00
locationName: Gleis 44
address: Schillerstraße 44, 89077 Ulm
link: https://www.facebook.com/events/368696931548382/
image: 241634414_1177497306080479_1321699650348617427_n.jpg
isCrawled: true
---
Ab kommenden Freitag starten wir jetzt auch wieder mit unserem Club- und Soziokultur-Programm. Die 44 Hz Disko bringt Misch-Mukke und Techno auf die Plattenteller, am Samstag gibt es Melodic Techno, Tech-House und Trash bei der Kollektiv Schillerstraße - alles wie in alten Tagen! ❤️👌🏼💯

FR & SA je 22-05 Uhr

Außerdem gibt es ab sofort wieder regulär Fr & Sa für euch @ahispizza alá beste der Stadt / Nacht! 

See Ya! 

Die Veranstaltungen sind gefördert durch den Beauftragten der Bundesregierung für Kultur und Medien („BKM“) und die Initiative Musik. Die Veranstaltungen laufen im Programm NEUSTART KULTUR.
