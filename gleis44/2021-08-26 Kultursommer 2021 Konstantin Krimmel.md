---
id: "3194945560733549"
title: "Kultursommer 2021: Konstantin Krimmel"
start: 2021-08-26 19:00
end: 2021-08-26 21:00
address: Marktplatz, 89073 Ulm
link: https://www.facebook.com/events/3194945560733549/
image: 239365333_1167727477057462_4020044603947267764_n.jpg
isCrawled: true
---
Vor der großzügigen und romantisch-altstädtischen Kulisse des Marktplatzes (Rathaus) wird am Donnerstag, den 26. August, eine Ulmer Koryphäe des Operngesangs einen Liederabend gestalten: Der Bariton Konstantin Krimmel (u.a. BBC Young Generation Artist). Begleitet wird dieser besondere Gast des Kultursommers vom Pianisten Ammiel Bushakevitz. 

#Corona-Bedingt ist nur begrenzter Zugang auf das Gelände möglich. #3G! Der Zugang zum Event ist barrierefrei.
Der Eintritt ist kostenlos -> first come/ first served

Der Kultursommer ist eine Initiative der Kulturabteilung der Stadt Ulm in Zusammenarbeit mit der Ulmer Kulturszene. Die einzelnen Programmpunkte werden von Gleis 44, Roxy Ulm oder HfG Archiv veranstaltet. Alle Beteiligten bedanken sich bei den Stadtwerke Ulm / Neu-Ulm und bei der Kulturstiftung des Bundes für die freundliche Unterstützung. Die Veranstaltungsreihe wird im Programm Kultursommer 2021 durch die Beauftragte der Bundesregierung für Kultur und Medien (BKM) mit Mitteln aus NEUSTART KULTUR gefördert.

