---
id: "490655655171745"
title: Veganer "mit-bring-Brunch" mit Vortrag im Gleis 44
start: 2020-05-03 12:00
end: 2020-05-03 15:00
locationName: Gleis 44
address: Schillerstraße 44, 89077 Ulm
link: https://www.facebook.com/events/490655655171745/
image: 87539129_2558124344408620_7986382092231507968_o.jpg
teaser: Die Earthlings Ulm und das Gleis 44 laden herzlich zu unserem Veganen Brunch
  ein! Zusammen mit euch möchten wir einmal im Monat gemeinsam, vegan Brunc
isCrawled: true
---
Die Earthlings Ulm und das Gleis 44 laden herzlich zu unserem Veganen Brunch ein! Zusammen mit euch möchten wir einmal im Monat gemeinsam, vegan Brunchen. Zusätzlich werden wir verschiedene Referenten einladen, die Rund um das Thema Umweltschutz und Vegane Ernährung Vorträge halten werden. Das erste Mal war schon ein voller Erfolg, wir freuen uns beim nächsten Brunch neue und bekannte Gesichter zu sehen.

Referent steht noch aus.   

Der Vortrag wird im Anschluss an den Brunch stattfinden. Danach ist noch Zeit für Diskussionen und Austausch.

Der Brunch beginnt gegen 12:00 Uhr, kommt bitte ein wenig früher damit alles vorbereitet ist, wenn es los gehen soll.   
_________________________________________________

Grober Ablauf vom Brunch, vor Ort gehen wir mit euch den Ablauf nochmal durch:

- Du bereitest einfach ein veganes Gericht für das Buffet vor und bringst es mit passendem Schöpfer mit. Mengenmäßig so viel wie du auch in etwa essen würdest. Ohne mitgebrachte Speise kannst du nicht am Brunch teilnehmen, ansonsten ist der Eintritt kostenlos.

- Wichtig: Vegan heißt frei von ALLEN tierischen Produkten, neben Fleisch und Wurst, Geflügel und Fisch zählen dazu auch Milch, Eier, Käse, Honig, Sahne, Joghurt, Gelatine, Fleischbrühe, Butter, tierfetthaltige Margarine usw. im Zweifel gerne bei uns melden 

- Brot und Getränke werden vom Veranstalter bereitgestellt, u.U. können diese für einen Unkostenbeitrag erworben werden. 

- Bitte beachte, dass auf jeden Fall eigene Teller und Schüssel, Besteck und Becher mitgebracht werden müssen, Danke!

- Deine vierbeinigen Freunde dürfen gerne mitkommen, bitte nur wenn sie sich mit Ihresgleichen vertragen.

Vielen Dank an das Gleis 44 und sein Team das wir die Räumlichkeiten gratis zur Verfügung gesellt bekommen.

Wir freuen uns auf euch, eure earthlings Ulm