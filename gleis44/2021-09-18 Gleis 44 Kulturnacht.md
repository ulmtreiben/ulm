---
id: "556731885582752"
title: Gleis 44 Kulturnacht
start: 2021-09-18 17:00
end: 2021-09-19 05:00
address: Schillerstraße 44, 89077 Ulm
link: https://www.facebook.com/events/556731885582752/
image: 241639772_1178093496020860_2066125728710696341_n.jpg
isCrawled: true
---
Nachdem die Live-Kultur und insbesondere die Nachtkultur für so lange Zeit in den Hintergrund getretenen ist, freuen wir uns ganz besonderes die Ulmer_innen und Gäste der Stadt zur diesjährlichen Kulturnacht einzuladen! 

Zu diesem Anlass möchten wir gemeinsam eine erfolgreiche, dritte Gleisgarten Saison feiern und bewirten euch ein letztes Mal in unserem urbanen Hinterhof-Biergarten bevor wir den Zapfhahn und Pflanzen Winterfest machen. Neben Bier vom Hahn kredenzen wir noch einmal die beste Pizza der Stadt (mit Applaus für Bäcker Ahi) und servieren Crêpes für den süßen Abgang.

Da es die Kultur- und nicht die kulinarische Nacht ist, bekommt ihr bei uns auch Einiges zu sehen. Die Künstler Steve de Coco, Stefan Grzesina, Marc Klavikowski und Charles Castro stellen ihre Werke zu Schau und Zauberer Meister Eckardt wird schon die frühen Abendstunden magisch erscheinen lassen. Im Anschluss laden wir alle dazu ein die Kulturnacht im Club gemeinsam zu feiern. 

Alle Infos zum Gleis 44: www.gleis44.de
