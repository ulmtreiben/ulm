---
id: "367018431145990"
title: Non Vogue DJ-Kollektiv Open Air
start: 2020-09-04 17:00
end: 2020-09-04 22:00
locationName: Liederkranz Kulturbiergarten
address: Friedrichsau 9, 89073 Ulm
link: https://www.facebook.com/events/367018431145990/
teaser: Das Non Vogue DJ-Kollektiv hat bei uns im Gleis schon so manche, lange
  Klub-Nacht abgefeiert. Die Jungs spielen zwischen straigthem Techno über
  UK-Bre
isCrawled: true
---
Das Non Vogue DJ-Kollektiv hat bei uns im Gleis schon so manche, lange Klub-Nacht abgefeiert. Die Jungs spielen zwischen straigthem Techno über UK-Breakbeats bis zu süßem House alles, was die Tanzfläche "Non Vogue" zum Beben bringt. 

Am Freitag spielen die Jungs im Liederkranz Open-Air (das Wetter soll passen <3 ) und bringen Summer-Vibey Sound mit! Soul, Funk, Miami Bass, Nu-Disco und gegen später dann Techno. Das wird definitiv einer unserer letzten, besten und spannendsten elektronischen Abenden in der schönen Au! 

x Kimber
x Prime
x Uncut
x Silence

(Non vogue DJ-Kollektiv)

____________________


Liederkranz Öffnungszeiten: 
Montag - Freitag ab 17 Uhr
Samstag, Sonntag & Feiertags ab 12 Uhr

Wir freuen uns auf euch! 🐇🦊🦆