---
id: "1123363438190347"
title: United We Stream x Gleis 44 x Kultursommer Ulm
start: 2021-08-07 18:00
end: 2021-08-07 22:00
link: https://www.facebook.com/events/1123363438190347/
image: 227217791_2089061327908685_5376164622762431543_n.jpg
isCrawled: true
---
United We Stream x Gleis 44 x Kultursommer Ulm

◥ ARTISTS
Blumenpanzer
Laau Pama
Monkey Safari
RE. YOU

◥ DATE
7th of August 18.00 - 22.00 CET
Stream:
facebook.com/unitedwestream
bit.ly/UWS_YT

◥ FURTHER INFORMATION
With lockdown coming to an end, clubs are trying to get back to normal business. 
Considering that there are still many restrictions and parties can only happen outside, the question of cultural participation in public spaces arises again. 
Even in a city like Berlin, where clubs are acknowledged as culture by the Senate they still have to face many challenges mainly brought up by the municipal administration and local authorities. For the survival of club culture, it’s essential that they can count on the full support of all parties involved!
What can be possible if those key players join forces is shown by several flagship projects all across Germany. We will promote and stream some of the best practice examples to spark some inspiration for how a ‘creative bureaucracy’ concept could look like.

We start with a small, dreamy city located in South Germany:

“Kultursommer” is an initiative of the cultural department of the city of Ulm in cooperation with the local cultural scene. Thanks to the amazingly smooth teamwork of the individual city departments and authorities, the kind support of  Stadtwerke Ulm / Neu-Ulm and the Kulturstiftung des Bundes, the "last minute" project was made possible, including kilometres of cycle path diversions and a carefully thought-out hygiene concept. The event series including this week’s stream is funded by the Federal Government Commissioner for Culture and the Media (BKM) with funds from NEUSTART KULTUR.

The “Blaubeurer Tor” which is one of the key traffic junctions of the city was the first location to be brought to life culturally - with concerts, readings, cabaret, and plenty of space to meet (again). During the next months, the “Kultursommer” will travel to all kinds of different public spaces and venues to create real participation of all citizens within. Ulm’s cultural stakeholders joined forces in curating and shaping the summer together!

One of the main organizers is Gleis 44 which arose from an initiative by the city of Ulm to revive the Dichterviertel (poets' quarter) by temporarily transforming an old railway building into a club. Gleis 44 is intended to enrich Ulm's cultural scene as a socio-cultural centre. On an area of over 2,000 square meters, containing studios, workshops, club culture, a beer garden, and rooms for workshops, lectures, and concerts, there is plenty of meeting space for creative and open-minded people and projects. 

The team of Vir2al Fx might be the best proof for progressive projects that emerge from the synergies of socio-cultural spaces. The Ulm-based production company, which has already realised live streams of DJ sets from Ulm on Lockdown days, also partly consists of DJs and other driving forces in the proximity of the nightclub. As the newest member of our United We Stream family, they not only produced the upcoming Stream, they also initiated many more of our upcoming projects and we can’t wait to present them to you!  

Join the summer of culture at Ulm in a multidisciplinary stream featuring the revival of expressing art, music, and togetherness.

#UnitedWeStream #gleis44 #kultursommerulm #dichterviertel #culturalrevival #togetherness #diversity #stream


◥ WEB
https://www.unitedwestream.org
facebook.com/unitedwestream
https://bit.ly/UWS_Youtube
https://www.facebook.com/gleis44/
https://www.ulm.de/aktuelle-meldungen/ka/kultursommer2021