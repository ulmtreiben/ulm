---
id: "121585270097686"
title: 'Kultursommer 2021: "I never promised you a rose garden" im Rosengarten'
start: 2021-08-06 18:00
end: 2021-08-07 00:00
address: Rosengarten Ulm Donau
link: https://www.facebook.com/events/121585270097686/
image: 228725318_1406106869783823_5427904558208592380_n.jpg
isCrawled: true
---
Der Rosengarten verwandelt sich unter dem Motto „I never promised you a Rose garden“ für ein Wochenende in einen Ort für zeitgenössische, bildende Kunst.        

Am Freitag, 6. August, wird um 19 Uhr zur Vernissage der dreitägigen open-Air Ausstellung geladen. Bis Sonntag, 8. August um 18 Uhr, werden zwölf nationale, internationale und lokale Künstler*innen mit ihren Arbeiten den Rosengarten neu definieren. 

Im Fokus stehen ortsspezifische Installationen und skulpturale Kunst.
Petra Schmitt von der Griesbadgalerie ist für die künstlerische Leitung verantwortlich. Auch die Rahmengestaltung des Areals folgt dem kuratorischen Ansatz.

So wird die Kunstausstellung im Rosengarten mit einer stimmungsvollen Lichtinszenierung und Musik untermalt. 

Die gastronomische Bewirtung übernimmt die Stiege. 

Die Veranstaltung ist ein Gemeinschaftsprojekt von der Griesbadgalerie, der Stiege und dem Kulturzentrum Gleis 44.

Beteiligte Künstler*innen sind: 
Biancoshock/ Christian Greifendorf/ Edgar Braig/ Emil Kräs/ 
Guido Weggenmann/ Helmut Smits/ Matthias Garff/ Patrick Nicolas/ Peter Pan Soundwerkstatt/ Richard Geczi/ Sany und Sil Krol


#Corona-Bedingt ist nur begrenzter Zugang auf das Gelände möglich. Bitte entschuldigt eventuelle Wartezeiten. Am Eingang müssen wir eure Kontaktdaten aufnehmen. Der Zugang aus Richtung Donau ist nicht möglich.

Der Eintritt ist kostenlos -> first come/ first served

Der Kultursommer ist eine Initiative der Kulturabteilung der Stadt Ulm in Zusammenarbeit mit der Ulmer Kulturszene. Die einzelnen Programmpunkte werden von Gleis 44, Roxy Ulm oder HfG Archiv veranstaltet. Alle Beteiligten bedanken sich bei den Stadtwerke Ulm / Neu-Ulm und bei der Kulturstiftung des Bundes für die freundliche Unterstützung. Die Veranstaltungsreihe wird im Programm Kultursommer 2021 durch die Beauftragte der Bundesregierung für Kultur und Medien (BKM) mit Mitteln aus NEUSTART KULTUR gefördert.
