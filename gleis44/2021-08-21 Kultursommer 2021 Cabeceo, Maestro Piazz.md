---
id: "189581716492052"
title: "Kultursommer 2021: Cabeceo, Maestro Piazzolla! - auf der HfG-Terasse"
start: 2021-08-21 20:00
end: 2021-08-21 22:30
locationName: HfG-Archiv Ulm
address: Am Hochsträß 8, 89081 Ulm
link: https://www.facebook.com/events/189581716492052/
image: 236638393_1161241907706019_2151845877160867294_n.jpg
isCrawled: true
---
Cabeceo, Maestro Piazzolla!
Auf der HfG-Terasse. 

Gemeinsam mit dem ungarischen Akkordeonisten Krisztián Palágyi wird das Ponte Festival Ulm den 100. Geburtstag von Astor Piazzolla feiern. 

Das Jubiläumskonzert findet auf der Terrasse der HfG Ulm statt, ein besonderer Ort der Neues und Altes zeitgleich mit seiner besonderen Geschichte würdigt. Neben Piazzolla wird Palágyi auch Argentinischen Tango von Wegbegleitern des großen Komponisten spielen. Im Anschluss sehen Sie ausgefallene Visuals des Musikers, die einzigartige Einblicke in die Welt des Tangos schaffen. 


#Corona-Bedingt ist nur begrenzter Zugang auf das Gelände möglich. 
Der Eintritt ist kostenlos -> first come/ first served

Der Kultursommer ist eine Initiative der Kulturabteilung der Stadt Ulm in Zusammenarbeit mit der Ulmer Kulturszene. Die einzelnen Programmpunkte werden von Gleis 44, Roxy Ulm oder HfG Archiv veranstaltet. Alle Beteiligten bedanken sich bei den Stadtwerke Ulm / Neu-Ulm und bei der Kulturstiftung des Bundes für die freundliche Unterstützung. Die Veranstaltungsreihe wird im Programm Kultursommer 2021 durch die Beauftragte der Bundesregierung für Kultur und Medien (BKM) mit Mitteln aus NEUSTART KULTUR gefördert.

