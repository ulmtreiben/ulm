---
id: "170076595111272"
title: Kammermusikmatinee mit Mitgliedern des Philharmonischen Orchesters der Stadt
  Ulm
start: 2021-07-18 11:00
locationName: Liederkranz Ulm
address: Friedrichsau 9, 89073 Ulm
link: https://www.facebook.com/events/170076595111272/
image: 215641128_550020636350642_8392189539138313710_n.jpg
isCrawled: true
---
Programm:

W.A. Mozart: Flötenquartett D-Dur KV 285 für Flöte, Violine, Viola und Violincello

Ludwig van Beethoven: Serenade für Flöte, Violine und Viola op.25

Es spielen Mitglieder des Philharmonischen Orchesters der Stadt Ulm: 
Ferhat Kunt, Flöte
Chiao Yin Chang, Violine
Maria Braun, Viola
Seoyeon Hong, Violoncello

Der Eintritt ist frei, um Spenden wird gebeten.
Bei Regen findet die Veranstaltung in der Teutonia statt. 