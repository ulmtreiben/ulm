---
id: "597710137429773"
title: Käsespätzle & Afro-House
start: 2020-08-06 17:00
end: 2020-08-06 22:00
locationName: Gleis 44
address: Schillerstraße 44, 89077 Ulm
link: https://www.facebook.com/events/597710137429773/
teaser: "Donnerstag gibt es selbstgemachte Käsespätzle und Afro-House von unserem
  Resident Blumenpanzer!  ____________________  Gleisgarten Öffnungszeiten:  So"
isCrawled: true
---
Donnerstag gibt es selbstgemachte Käsespätzle und Afro-House von unserem Resident Blumenpanzer!

____________________

Gleisgarten Öffnungszeiten: 
Sonntag - Donnerstag 17 - 23 Uhr 
Freitag - Samstag 17 - 24 Uhr 

Wir freuen uns auf euch! 🐇🦊🦆

