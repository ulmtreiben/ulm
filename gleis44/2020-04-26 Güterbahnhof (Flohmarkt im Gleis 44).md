---
id: "625062641369910"
title: "Verschoben: Güterbahnhof"
start: 2021-03-27 15:00
end: 2021-03-27 20:00
locationName: Gleis 44
address: Schillerstraße 44, 89077 Ulm
link: https://www.facebook.com/events/625062641369910/
image: 82476801_756666781496869_194721258716266496_o.jpg
teaser: TRÖDELITYOURWAY  Kleider, Kunst, Krempel & Kulinarik im Gleis und ein
  wunderschöner Samstag...  ❤️✖️💫Mit Frühlingsgefühlen, Sonne und vielen tollen
  M
isCrawled: true
---
TRÖDELITYOURWAY

Kleider, Kunst, Krempel & Kulinarik im Gleis und ein wunderschöner Samstag...  ❤️✖️💫Mit Frühlingsgefühlen, Sonne und vielen tollen Menschen mit tollem Kram! Drinnen und draußen ist mega viel los, seid gespannt, wir haben viele neue Ideen in die Tat umgesetzt! 

Auch wenn ihr nur bummel wollt, gibt es einiges zu tun und zu entdecken.Für Hungrige ist gesorgt, es gibt Burger, veganes arabisches Streetfood und süßes. Oder ihr chillt einfach am Frühlingslagerfeuer und erfreut euch des Lebens! 

Wer ist neben vielen Privaten Trödlern am Start? 
- Ahi´s Pizza
- Kaffee Fred
- TBA Burger 
- TBA Vegan
- No Pain No Gain (Tattoo)
- Colins Messerschmuck (Messer)
- Ulmer Stadthonig (Süß)
- Signore Barbarossa (Vinyl-Platten) 
- El Mano (60s/70s Trödel)
- Gen Himmel (Label / Kleidung)
- So beschenkt (Handgemachtes)
- Jacke wie Hose (Upcycling)
- Max Raval Arts (Schmuck)
- Mias kreative Welt (Taschen)
- Indisein (Prints / Art)
- DerBee (Prints / Art)
- Dominik Kast (Prints / Art)
- Moritz Reulein (Prints / Art)
- BG Surfboards (Surfbretter)
- Surf Repair Ulm (Streetwear)

- Nico Bulla (Untitled / EINS TIEFER / Eden)
- YAX (ehemals Sucasa)
- Ekko, Stefan Hepp, Blumenpanzer (Kollektiv Schillerstraße)

Afterparty:
www.facebook.com/events/673136123226475/

Leider sind wir für diesen Termin voll! Im Juni gibt es dann den nächsten Termin. Schreibt uns: gueterbahnhof@gleis44.de 
