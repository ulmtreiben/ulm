---
id: "282379796195020"
title: Der eingebildete Kranke - commedia dell'arte im Liederkranz
start: 2020-08-19 20:00
end: 2020-08-19 21:30
locationName: Liederkranz Kulturbiergarten
address: Friedrichsau 9, 89073 Ulm
link: https://www.facebook.com/events/282379796195020/
teaser: Im Liederkranz fängt das Treiben schon an, bevor der Biergarten seine Pforten
  öffnet. In den letzten Wochen haben die Schauspieler*innen der Ulmer Aka
isCrawled: true
---
Im Liederkranz fängt das Treiben schon an, bevor der Biergarten seine Pforten öffnet. In den letzten Wochen haben die Schauspieler*innen der Ulmer Akademie für darstellende Kunst ihre eigens für den Biergarten entwickelte Inszenierung dort geprobt. 

Jetzt wird das Stück „Der eingebildete Kranke“ regelmäßig bei uns aufgeführt. Der berühmte Klassiker von Molière behandelt das Thema Hypochondrie. Obwohl das Stück im 15. Jahrhundert geschrieben wurde, ist der Stoff hochaktuell, da die Definition von krank und gesund nach wie vor große gesellschaftliche Fragen aufwirft. Fun-Fact: Molière spielte selbst die Hypochonder-Hauptrolle des Stücks und starb durch einen Blutsturz, den er bei der vierten Aufführung des Stücks erlitt.