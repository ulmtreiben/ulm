---
id: "753676758796015"
title: Liebevoller Schwörmontag im Gleisgarten
start: 2020-07-20 17:00
end: 2020-07-20 23:00
locationName: Gleis 44
address: Schillerstraße 44, 89077 Ulm
link: https://www.facebook.com/events/753676758796015/
teaser: Schwörmontag im Gleis ist inzwischen ja schon fast Tradition, immerhin haben
  das Gleis 2018 damals das erste Mal geöffnet. Dieses Jahr wird es natürli
isCrawled: true
---
Schwörmontag im Gleis ist inzwischen ja schon fast Tradition, immerhin haben das Gleis 2018 damals das erste Mal geöffnet. Dieses Jahr wird es natürlich etwas anders, aber wir freuen uns auf alle, die den schönsten Montag im Jahr mit uns entspannt feiern wollen. 

Es gibt Falafel, frisches Bier vom Fass und DJ-Sets von unseren Techno & House Residents! 

Lineup:
- Nico Bulla
- EkkoOfficial
- Tim Bøxx
- Blumenpanzer  
____________________

Damit sich alle gut und sicher fühlen haben wir ein Hygiene-Konzept erarbeitet und werden alle Tische etc. sehr regelmäßig Desinfizieren. 
Hier für euch kurz und bündig alle Änderungen: 
- Ihr dürft in Gruppen von maximal 20 Personen kommen. 
- wir müssen eure Namen und Kontaktdaten aufnehmen und 4 Wochen speichern.
- Bitte wartet am Eingang, unser Team weist euch Plätze zu. 
- Leider können wir keine Stehplätze an der Theke anbieten. 

____________________

Gleisgarten Öffnungszeiten: 
 Montag - Donnerstag 17 - 23 Uhr 
 Freitag - Samstag 17 - 24 Uhr 
 Sonntag 13 - 23 Uhr

Wir freuen uns auf euch! 🐇🦊🦆

