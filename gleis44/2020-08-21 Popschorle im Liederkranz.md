---
id: "3320786758145782"
title: Popschorle im Liederkranz
start: 2020-08-21 20:00
end: 2020-08-21 22:00
locationName: Liederkranz Kulturbiergarten
address: Friedrichsau 9, 89073 Ulm
link: https://www.facebook.com/events/3320786758145782/
teaser: "Für die nächste #popschorle quartieren wir uns im Liederkranz
  Kulturbiergarten ein.  Der Eintritt ist frei —  alle Spenden gehen direkt an
  die Künstle"
isCrawled: true
---
Für die nächste #popschorle quartieren wir uns im Liederkranz Kulturbiergarten ein. 
Der Eintritt ist frei —  alle Spenden gehen direkt an die KünstlerInnen. 
Beginn: 20:00 Uhr 

Am 21.08. sind mit dabei: 

GEISTHA | Electronica | Mannheim 
https://www.youtube.com/watch?v=UZkpt0n6Rgg

Schattenhaft füllt GEISTHA Räume mit Licht und Klang - schafft eine Symbiose aus Dunkelheit und Schönheit. Exzess, Lust, Sex -  die Suche nach Intensität in allen Facetten bestimmt bestimmt die Ästhetik von GEISTHA.


Everdeen | Indie Rock | Stuttgart  
https://www.youtube.com/watch?v=gSZ6SvZ1dkQ

Nachdenklich und hoffnungsvoll, dann aber wieder energisch und auch sanft. EVERDEEN macht Indie Pop Rock von der Art, wie man ihn in Großbritannien oder Skandinavien erwartet, obwohl das multinationale Trio ihre aktuelle Homebase in Süddeutschland hat. EVERDEEN kombiniert Elemente
von Shoegaze, Post Rock und Synthie Pop mit einer überzeugenden weiblichen Stimme.


Norðir | Indietronica | Ulm
https://www.youtube.com/watch?v=kFyD2CmFdrU

NORÐIR, das sind zwei weltoffene Vollblutmusiker mit einer Faszination für das Zeitreisen und die Weltgeschichte.
Ausgestattet mit E-Drums, Looper, E-Gitarren, Synthesizer, visuellen Effekten und 3 Sprachen, katapultiert die Band das Publikum auf eine Reise durch die Zeit.


Die #popschorle ist ein Kooperationsprojekt des Club Schilli und der popbastion ulm