---
id: "311168220012418"
title: Liederkranz Schwörsonntag - Bands, Streetfood & Sonne
start: 2020-07-19 13:00
end: 2020-07-19 22:30
locationName: Liederkranz Kulturbiergarten
address: Friedrichsau 9, 89073 Ulm
link: https://www.facebook.com/events/311168220012418/
image: 107827421_886249881871891_15874756298142905_o.jpg
teaser: "Schwörsonntag steht für Live-Musik und leckere Kulinarik! :)   Folgende Bands
  sind für euch dabei:   13:30 Valentino testa  14:15 single malt  15:30 C"
isCrawled: true
---
Schwörsonntag steht für Live-Musik und leckere Kulinarik! :) 

Folgende Bands sind für euch dabei: 

13:30 Valentino testa 
14:15 single malt 
15:30 Cactus Tree
16:45 Rainbow head
18:00 Roadstring Army


Die Veranstaltung wird ermöglich durch: 
SWU Stadtwerke Ulm/Neu-Ulm GmbH
Indauna e.V 
Gleis 44

_______


Der Abend findet im Rahmen von TASTE Street Food to go Picknick in der Au wir Schwören. statt, für Kulinarische Untermalung ist also gesorgt! : ))) 

____________________
Damit sich alle gut und sicher fühlen haben wir ein Hygiene-Konzept erarbeitet und werden alle Tische etc. sehr regelmäßig Desinfizieren.
Hier für euch kurz und bündig alle Änderungen:

- ihr dürft in Gruppen von maximal 20 Personen kommen und gemeinsam an einem Tisch Sitzen.
- wir müssen eure Namen und Kontaktdaten aufnehmen und 4 Wochen speichern.
- Bitte wartet am Eingang, unser Team weist euch Plätze zu.
- Leider können wir keine Stehplätze an der Theke anbieten.

____________________

Liederkranz Öffnungszeiten:
Montag - Freitag ab 17 Uhr
Samstag, Sonntag & Feiertags ab 12 Uhr
Wir freuen uns auf euch! 🐇🦊🦆
