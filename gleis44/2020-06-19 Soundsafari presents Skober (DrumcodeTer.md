---
id: "207842666967674"
title: Soundsafari presents Skober (Drumcode/Terminal M)
start: 2020-06-19 22:00
end: 2020-06-20 05:00
locationName: Gleis 44
address: Schillerstraße 44, 89077 Ulm
link: https://www.facebook.com/events/207842666967674/
image: 87691467_2943089202421596_7312921633851703296_n.jpg
teaser: Soundsafari presents Skober 🖤 (Drumcode /Terminal M)  Skober is a Ukrainian
  techno DJ and producer with his unique deep- techno style. Having good m
isCrawled: true
---
Soundsafari presents Skober 🖤 (Drumcode /Terminal M)

Skober is a Ukrainian techno DJ and producer with his unique deep-
techno style. Having good music background and experience playing
in rock band in his early youth it is no surprise that after being 
infected by House vibes this young man jumped on the right wagon. 
Skober has done lots of tracks and remixes for labels like Drumcode,
Tronic, Terminal M, Elevate, Phobiq, Alleanza, Analytictrail, Respekt, 
Trapez, Fone Audio, Remain, Rhythm Converted. There were 
numerous entries in techno and minimal tops on different music 
shops all over the world done by Skober's music and he got support 
from some serious players in this industry. 
He is supported by such famous artists as Adam Beyer, Alan 
Fitzpatrick, Joseph Capriati, Slam, Dubfire, Sasha Carassi, 
Markantonio, Luigi Madonna, Harvey McKay, Mark Reeve, Spektre, 
Tom Hades, Axel Karakasis, Steve Parker Loco & Jam, Mr. Bizz, 
RedHead and many others. But he is not running over the quantity 
of tracks as his main point is quality and ideas. So watch out and 
support!

Djs:
Skober 
D.VID
Hantex
Stefan Hepp
Sülow
Zerotonin