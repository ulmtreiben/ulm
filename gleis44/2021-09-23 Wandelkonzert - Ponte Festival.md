---
id: "598919214804133"
title: Wandelkonzert - Ponte Festival
start: 2021-09-23 20:00
locationName: Gleis 44
address: Schillerstraße 44, 89077 Ulm
link: https://www.facebook.com/events/598919214804133/
image: 242110491_589624379056934_1944334653735079331_n.jpg
isCrawled: true
---

Das Wandelkonzert im Gleis 44 ist ein musikalischer Rundgang durch verschiedenste Stimmungen.

Freut euch auf ein klassisches Konzert auf drei Tanzflächen des Gleis 44 am 23.09.2021 um 20:00 Uhr. 

Karten findet ihr unter:
https://www.ulmtickets.de/events/59-ponte-kammermusikfestival-ulm

_______________________________________________________________________
Programm 

Franz Schubert
Quartettsatz c-Moll, D 703
_______________________________________________________________________
Auswahl aus:

Jörg Widmann 
Duos
&
J. S. Bach 
Zweistimmige Inventionen 

_______________________________________________________________________

Pierre Jodlowski 
"IT"
_______________________________________________________________________
Weitere Informationen findest du unter: 
www.ponte-ulm.com

Die Veranstaltung ist gefördert durch den Beauftragten der Bundesregierung für Kultur und Medien („BKM“) und die Initiative Musik. Der Abend läuft im Programm NEUSTART KULTUR.
