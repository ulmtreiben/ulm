---
id: "2672876779626100"
title: "Alternative Autokino: Big Lebowski"
start: 2020-07-23 20:15
end: 2020-07-24 00:00
address: Bodelschwinghweg 1, 89160 Dornstadt
link: https://www.facebook.com/events/2672876779626100/
teaser: "#25JahrefreeFM Radio free FM und der Obstwiesenfestival e.V.
  präsentieren:   Alternative Autokino: Big Lebowski Jeff Lebowski (Jeff
  Bridges), der sich"
isCrawled: true
---
#25JahrefreeFM
Radio free FM und der Obstwiesenfestival e.V. präsentieren: 

Alternative Autokino: Big Lebowski
Jeff Lebowski (Jeff Bridges), der sich schlicht „Der Dude“ nennt, ist der wohl trägste Mensch von Los Angeles. Ein schlaffer Alt-Hippie, der sich ausschließlich von „White Russian-Cocktails ernährt, am liebsten Walgesänge hört und auch den Joint nur selten aus der Hand legt. Die verbleibende Zeit widmet er gemeinsam mit seinem Freunden Walter (John Goodman) und Donny (Steve Buscemi) dem Bowling. Doch mit dem „easy living“ ist es schlagartig vorbei, als der „Dude“ mit einem gleichnamigen Millionär verwechselt wird. Erst pinkeln zwei Geldeintreiber auf seinen Lieblingsteppich, dann wird er von ihnen verprügelt, um Schulden seiner angeblichen Frau Bunny bei ihm einzukassieren. Schließlich wird eben diese Bunny entführt und der „echte“ Lebowski (David Huddleston) heuert seinen Namensvetter als Lösegeldkurier an. Als der „Dude“ die Geldübergabe vermasselt, geht der Ärger erst richtig los…

Spielregeln:
Um einen reibungslosen Ablauf zu garantieren benötigen wir eure Unterstützung und bitten um die Einhaltung unserer Spielregeln.
Schon jetzt vielen Dank für eure Unterstützung.

1. Pro Fahrzeug sind maximal 2 Personen zugelassen

2. Die Teilnahme ist nur in einem geschlossenen Fahrzeug möglich. Die Fahrzeuggröße ist auf die Größe eines VW-Busses, Vito etc. begrenzt

3. Die Altersfreigabe der Filme ist zu beachten und wird bei der Einfahrt kontrolliert

4. Die Tonübertragung erfolgt über Radio freeFM. Bitte schaltet kurz vor dem Film die Frequenz von freeFM 102,6 ein, dort informieren wir euch über die Frequenz für die Tonübertragung

5. Bitte schaltet während des Filmes die Beleuchtung eurer Fahrzeuge aus

6. Das Verlassen des Geländes während der Vorstellung ist nicht gestattet

7. Das Verlassen des Fahrzeuges ist nur für Toilettengänge sowie den Erwerb von Getränken und Snacks gestattet. Wir empfehlen euch bereits mit dem Ticket Getränke und Snacks zu erwerben. Die aktuell gültigen Hygieneregeln sind dabei einzuhalten. Bitte haltet mind. 1,5 m Abstand und tragt beim Verlassen des Fahrzeuges unbedingt eine Mund- und Nasenmaske

8. An- und Abfahrt
Die Zu- und Abfahrt erfolgt über den Hubertusweg. Bei Ankunft werdet ihr und euer Fahrzeug auf den Parkplatz geleitet und dort durch unsere Ordner auf die Parkfläche eingewiesen. Die Einfahrt der Fahrzeuge ist für ein Zeitfenster von 20:15 Uhr - 21:15 Uhr geplant. Die Abfahrt des Platzes erfolgt reihenweise nach Anweisung durch das Ordnerpersonal.

9. Für eventuelle Pannen (in erster Linie leere Autobatterien) ist ein KFZ-Techniker vor Ort

10. Was passiert, wenn es regnet?
Das Gute ist, wir können den Film auch bei Regen spielen. Tritt während der Veranstaltung Starkregen oder Sturm auf, werden wir die Vorstellung abbrechen oder bei entsprechender Vorhersage rechtzeitig absagen. Wenn der Film bereits begonnen hat, erfolgt keine Erstattung. Sollte der Film vorsorglich abgesagt werden müssen, erhaltet ihr den Eintrittspreis zurück, da es keinen Ersatztermin geben kann. Cool wäre es natürlich, wenn ihr das Tickets dann an freeFM spenden würdet. Alle Infos dazu findet ihr unter freefm.de/25Jahre oder über die sozialen Kanäle. 