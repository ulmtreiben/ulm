---
id: "199154812201028"
title: Gleis Club-Eröffnungswochende - Fr & Sa je ab 22 Uhr
start: 2021-09-03 22:00
end: 2021-09-05 05:00
locationName: Gleis 44
address: Schillerstraße 44, 89077 Ulm
link: https://www.facebook.com/events/199154812201028/
image: 241134395_1173427599820783_5085367231986425609_n.jpg
isCrawled: true
---
Also liebe Leute – los geht’s! ❤ Ab dem kommenden Wochenende gibt es wieder so richtig Gleis-Club-Amore. 

FR & SA je 22-05 Uhr

Unsere Residents vom Kollektiv Schillerstraße freuen sich schon wieder mega, für euch im Club aufzulegen. Außerdem haben wir den Produzenten Egotot (Schimmer Records) am Samstag eingeladen, um den 2nd-Floor so richtig einzuweihen. Aber auch Indie und House-Fans werden auf nun 3 Floors auf ihre Kosten kommen. <3

Mit dabei: Egotot, Hans Pech, Ekko, Bastian Stimmig, Sam Travolta, TBXX, Blumenpanzer, Lenn Reich, Van Kost & Disco Dietrich.

Außerdem gibt es ab sofort wieder regulär Fr & Sa für euch @ahispizza alá beste der Stadt / Nacht! 

See Ya! 

Die Veranstaltungen sind gefördert durch den Beauftragten der Bundesregierung für Kultur und Medien („BKM“) und die Initiative Musik. Die Veranstaltungen laufen im Programm NEUSTART KULTUR.
