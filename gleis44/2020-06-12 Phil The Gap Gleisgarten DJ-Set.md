---
id: "706165713512841"
title: Phil The Gap Gleisgarten DJ-Set
start: 2020-06-12 18:00
end: 2020-06-12 22:00
locationName: Gleis 44
address: Schillerstraße 44, 89077 Ulm
link: https://www.facebook.com/events/706165713512841/
image: 83325460_858053958024817_210399237966397440_o.jpg
teaser: "Phil The Gap vom Eden-Rocks-Indie-Team präsentiert Alternative, Indie, Indie
  Pop und artenverwantes für einen schniecken Freitagabend! : )  __________"
isCrawled: true
---
Phil The Gap vom Eden-Rocks-Indie-Team präsentiert Alternative, Indie, Indie Pop und artenverwantes für einen schniecken Freitagabend! : ) 
____________________

Damit sich alle gut und sicher fühlen haben wir ein Hygiene-Konzept erarbeitet und werden alle Tische etc. sehr regelmäßig Desinfizieren. 
Hier für euch kurz und bündig alle Änderungen: 
- ihr dürft nur in Gruppen die aus 2 Haushalten bestehen kommen! 
- wir müssen eure Namen und Kontaktdaten aufnehmen und 4 Wochen speichern.
- Bitte wartet am Eingang, unser Team weist euch Plätze zu. 
- Leider können wir keine Stehplätze an der Theke anbieten. 

____________________

Gleisgarten Öffnungszeiten: 
Montag - Donnerstag 17 - 23 Uhr 
Freitag - Samstag 17 - 24 Uhr 
Sonntag 13 - 23 Uhr

Wir freuen uns auf euch! 🐇🦊🦆

