---
id: "1511369335692353"
title: SEV - STUDI-Mittwoch im Gleis!
start: 2020-06-10 23:00
end: 2020-06-11 03:00
locationName: Gleis 44
address: Schillerstraße 44, 89077 Ulm
link: https://www.facebook.com/events/1511369335692353/
image: 70234505_665650747265140_6126294072014405632_o.jpg
teaser: StudiMittwoch im Gleis!   - Freier Eintritt - Mexi/Pfeffi 2 €  - Mach die
  Mittwoch Nacht zur fabelhaften Träumerei!   Schön und entspannt ein wenig Da
isCrawled: true
---
StudiMittwoch im Gleis! 

- Freier Eintritt
- Mexi/Pfeffi 2 € 
- Mach die Mittwoch Nacht zur fabelhaften Träumerei! 

Schön und entspannt ein wenig Dancen, Tischtennis zocken oder einfach einen schicken Mexi an der Bar trinken? Der Mittwoch im Gleis ist für alle Menschen die vom Dienstag gelangweilt sind, Prüfung geschrieben haben oder gut gelaunt sind. See youuu!
