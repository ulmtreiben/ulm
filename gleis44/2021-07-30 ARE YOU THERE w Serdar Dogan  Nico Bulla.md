---
id: "843214999634839"
title: ARE YOU THERE??? w// Serdar Dogan // Nico Bulla // Schobi Live
start: 2021-07-30 19:00
end: 2021-07-31 00:00
locationName: Reithalle Ulm (offizielle Seite)
address: Schillerstraße 1, 89077 Ulm
link: https://www.facebook.com/events/843214999634839/
image: 221724344_2331892790276272_6740649768445908677_n.jpg
isCrawled: true
---
ARE YOU THERE??? w// Serdar Dogan, Nico Bulla, Schobi Live

Lange sah man ihn nur zwischen Plattentellern von Ulmer Clubs und der Reithalle wechseln: Nico Bulla gehört ins Stadtbild, wie ein Ulmer Münster der Subkultur. Mittlerweile hat der DJ und Skater im Kollektiv Eins Tiefer die deutsche Hauptstadt erobert, ist seit letztem Jahr aber wieder zurück in seine Heimat gekehrt. Das bedeutet für den Nico Bulla natürlich auch eine Rückkehr zu seinen Wurzeln, in die Reithalle. Diesmal aber nicht auf dem Brett, sondern mit seinem Format „Are you there?“

Die ansonsten gut gehüteten Pforten öffnen sich an diesem Abend für alle die Lust haben. Nico legt seinen neusten Deephouse auf und sein Stuttgarter Freund und Rapper Schobi lässt Erinnerungen wach werden, was süddeutscher Hip-Hop noch mal bedeutet. Nico und Schobi nehmen gerade zusammen ein Album auf und vielleicht verraten sie an diesem Abend, wie sich das anhören wird.

Die Sache rund macht Serdar Dogan (Bleepgeeks), ein ebenso bekanntes Ulmer DJ-Gesicht, wie Nico. Von Serdars Tellern gibt es entspannten House zu hören, wie damals im Eden.

Live Visuals von O_POL, ein weiteres Projekt von Tausendsassa Nico Bulla, begleiten den Abend. Zu sehen ist eine Mischung aus digitalem Artwork und Aufnahmen aus alten Skater-Tagen.

Für die Besänftigung des trockenen Gaumens sorgt der Verein von der Reithalle.


#Einlass: kostenlos 

Der Kultursommer Ulm ist ein Projekt der der Kulturabteilung der Stadt Ulm in Kooperation mit den Kulturzentren Roxy Ulm und Gleis 44 sowie dem HfG Archiv Ulm. Wir bedanken uns bei den Stadtwerke Ulm / Neu-Ulm und bei der Kulturstiftung des Bundes für die freundliche Unterstützung. Die Veranstaltungsreihe wird im Programm Kultursommer 2021 durch die Beauftrage der Bundesregierung für Kultur und Medien (BKM) mit Mitteln aus NEUSTART KULTUR gefördert.

Mehr Infos unter kultursommer.ulm.de