---
id: "1044081523062081"
title: Gleis Klubnacht mit Fabi Haas und Pino Peña
start: 2021-09-17 22:00
end: 2021-09-18 05:00
locationName: Gleis 44
address: Schillerstraße 44, 89077 Ulm
link: https://www.facebook.com/events/1044081523062081/
image: 241971447_1182061585624051_4762154184886531309_n.jpg
isCrawled: true
---
FR  22-05 Uhr
Klubnacht
Techno, Breaks & Indie

Fabi Haas
(nicht jeder kann es hören, Stuttgart)
Pino Peña
(Jones&Roth, Berlin)
Ekko
Kid Kun
Van Kost
Ben Disco

Die Auftritte sind gefördert durch den Beauftragten der Bundesregierung für Kultur und Medien („BKM“) und die Initiative Musik. Die Veranstaltungen laufen im Programm NEUSTART KULTUR.
