---
id: "822161201927325"
title: Sean Noonan´s Picnic (jazz from hell)
start: 2020-11-01 20:00
address: Gleis 44, Schillerstraße 44, Ulm
link: https://www.facebook.com/events/822161201927325/
image: 121082738_4892859700732073_7852734532377360611_o.jpg
teaser: Die Band ist gewissermaßen das Punk-Jazz-Ding des New Yorker Schlagzeugers,
  den wir vor 17 Jahren schon zweimal mit „The Hub“ bei uns zu Gast hatten.
isCrawled: true
---
Die Band ist gewissermaßen das Punk-Jazz-Ding des New Yorker Schlagzeugers, den wir vor 17 Jahren schon zweimal mit „The Hub“ bei uns zu Gast hatten. Gespielt wird eine unglaublich anarchische, verzinkte, punkig-jazzige und rockig-funkige Mischung, die die Kompositionen des Drummers prägt, der zuletzt durch die Zusammenarbeit mit Marc Ribot und Jamaaladeen Tacuma von sich reden gemacht hat. Und auf der Bühne wird der charismatische Noonan zum Trommeltier!
Kongenialer Partner hier wie in anderen Projekten ist ihm dabei der aberwitzige Norbert Bürger, den man in Ulm vielleicht noch vom Orchester Bürger Kreitmeyer in Erinnerung hat. New Yorker Wahnsinn trifft auf bayrische Gemütlichkeit? Nein, nicht wirklich, denn Noonans Mitmusiker tragen dessen wildes Konzept inbrünstig mit. Ein Erlebnis! 
Norbert Bürger: Gitarre
Christian Schantz: Bass
Sean Noonan: Schlagzeug
Eintritt: 14 €, ermäßigt 10 € (mit Ausweis: Mitglieder/Schüler/Studenten/Bufdis/FSJ)

Wegen doch recht begrenzter Plätze empfehlen wir den Vorverkauf:
https://www.ulmtickets.de/produkte/5509-tickets-sean-noonan-s-picnic-jazz-from-hell-gleis-44-ulm-am-01-11-2020
