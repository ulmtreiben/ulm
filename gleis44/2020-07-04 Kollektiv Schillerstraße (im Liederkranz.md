---
id: "188566645925610"
title: Kollektiv Schillerstraße (im Liederkranz)
start: 2020-07-04 17:00
end: 2020-07-04 22:00
locationName: Liederkranz Kulturbiergarten
address: Friedrichsau 9, 89073 Ulm
link: https://www.facebook.com/events/188566645925610/
image: 105455597_873293619834184_4718847595846941877_o.jpg
isCrawled: true
---
