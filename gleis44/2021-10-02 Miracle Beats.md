---
id: "380241333588981"
title: Miracle Beats
start: 2021-11-06 22:00
end: 2021-11-07 05:00
address: Schillerstraße 44, 89077 Ulm
link: https://www.facebook.com/events/380241333588981/
image: 242240739_1184578935372316_6927248871728379473_n.jpg
isCrawled: true
---
Techno, House, Minimal, 90s

Endlich startet die Miracle Beats wieder bei uns im Gleis! Jeden 2ten Monat kommt uns der ungarische DJ Aydan besuchen und präsentiert einen einzigartigen, elektronischen Mix, der unsere musikalische Bandbreite stylisch erweitert!

Das kommt nicht von ungefähr: Aydan war lange Zeit im bekanntesten Elektro & Afterhour-Club Budapests Resident. Aus dem Coronita wurde der Minimal-Techno-Sound nach ganz Europa getragen! Bis heute begeistern die Partyreihe und Aydan großes Publikum vom Balkan bis nach Berlin. 
Mit dabei ist Dennis Berey – ein junges Talent, dass von Aydan musikalisch ausgebildet wurde und dennoch einen sehr eigenen, melodischen Sound entwickelt hat. Die Silvester-Afterhour 2019 im Gleis bleibt uns unvergessen!

Auf dem Werk-Floor ist endlich mal wieder Moonligth am Start und versorgt euch mit ACID-Techno. Begleitet wird die Resident-DJ von unserem lieben Ekko.

Außerdem spiet Disco Dietrich auf Floor Nummer 3 für euch ein Special: Wir gehen zurück in die 90er und kramen House- und Pop-Scheiben in stilvoller Gleis-Manier aus den Plattenkoffern.  
