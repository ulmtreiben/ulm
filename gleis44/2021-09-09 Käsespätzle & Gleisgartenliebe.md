---
id: "545630230057603"
title: Käsespätzle & Gleisgartenliebe
start: 2021-09-09 17:00
end: 2021-09-09 22:00
locationName: Gleis 44
address: Schillerstraße 44, 89077 Ulm
link: https://www.facebook.com/events/545630230057603/
image: 241650334_1177963756033834_8702556917591420040_n.jpg
isCrawled: true
---
Donnerstag gibt es selbstgemachte Käsespätzle! 

Wir freuen uns auf euch! 🐇🦊🦆

