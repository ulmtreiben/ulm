---
id: "192828039421513"
title: Kollektiv Schillerstraße Gleis-Bar Wochenende 13.&14.08
start: 2021-08-13 22:00
end: 2021-08-15 05:00
address: Schillerstraße 44, 89077 Ulm
link: https://www.facebook.com/events/192828039421513/
image: 235334076_1160563957773814_8677515937942838233_n.jpg
isCrawled: true
---
Das Kollektiv Schillerstraße spielt schöne Musik in der Gleis-Bar
#Techno&House
#je 22-05 Uhr
#3G! 

Freitag: Ekko, Van Kost
Samstag: Tim Boxx, Lenn Reich, Sam Travolta

Ab nächster Woche könnt ihr in der Gleis-Bar dann auch endlich tanzen! :)  Hierfür benötigt ihr einen Impf- oder Genesenennachweis oder einen PCR-Test. Wir arbeiten gerade gemeinsam mit dem Theatro und anderen Ulmer Clubs an einer gemeinsamen Lösung. 

Der Club wird wieder offiziell ab Anfang September eröffnen! <3 

Das Projekt "Gleis-Bar" ist gefördert durch den Beauftragten der Bundesregierung für Kultur und Medien („BKM“) und die Initiative Musik. Die Veranstaltungen laufen im Programm NEUSTART KULTUR.