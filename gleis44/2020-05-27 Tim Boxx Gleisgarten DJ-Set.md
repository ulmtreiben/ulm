---
id: "2568257216608467"
title: Tim Boxx Gleisgarten DJ-Set
start: 2020-05-27 17:00
end: 2020-05-27 23:00
locationName: Gleis 44
address: Schillerstraße 44, 89077 Ulm
link: https://www.facebook.com/events/2568257216608467/
image: 98463375_850918145405065_8203991755654168576_o.jpg
teaser: "Schöne Musik, Techno & House, für euch im Gleisgarten! :
  )   ____________________  Damit sich alle gut und sicher fühlen haben wir ein
  Hygiene-Konzept"
isCrawled: true
---
Schöne Musik, Techno & House, für euch im Gleisgarten! : ) 

____________________

Damit sich alle gut und sicher fühlen haben wir ein Hygiene-Konzept erarbeitet und werden alle Tische etc. sehr regelmäßig Desinfizieren. 
Hier für euch kurz und bündig alle Änderungen: 
- ihr dürft nur in Gruppen die aus 2 Haushalten bestehen kommen! 
- wir müssen eure Namen und Kontaktdaten aufnehmen und 4 Wochen speichern.
- Bitte wartet am Eingang, unser Team weist euch Plätze zu. 
- Leider können wir keine Stehplätze an der Theke anbieten. 

____________________

Gleisgarten Öffnungszeiten: 
Montag - Donnerstag 17 - 23 Uhr 
Freitag - Samstag 17 - 24 Uhr 
Sonntag 13 - 23 Uhr

Wir freuen uns auf euch! 🐇🦊🦆

