---
id: "622174045368733"
title: Non Vogue at Gleisgarten (DJ-Set)
start: 2020-07-10 18:00
end: 2020-07-10 22:00
locationName: Gleis 44
address: Schillerstraße 44, 89077 Ulm
link: https://www.facebook.com/events/622174045368733/
image: 107330682_883360935494119_3683039808103654255_o.jpg
teaser: Die Jungs vom Non-Vogue DJ-Kollektiv schauen bei uns am Freitag vorbei!
  Techno, House & Bass.   ____________________   Damit sich alle gut und sicher
isCrawled: true
---
Die Jungs vom Non-Vogue DJ-Kollektiv schauen bei uns am Freitag vorbei! Techno, House & Bass. 

____________________

 Damit sich alle gut und sicher fühlen haben wir ein Hygiene-Konzept erarbeitet und werden alle Tische etc. sehr regelmäßig Desinfizieren. 
Hier für euch kurz und bündig alle Änderungen: 
- ihr dürft in Gruppen von maximal 20 Personen kommen und gemeinsam an einem Tisch Sitzen.
- wir müssen eure Namen und Kontaktdaten aufnehmen und 4 Wochen speichern.
- Bitte wartet am Eingang, unser Team weist euch Plätze zu. 
- Leider können wir keine Stehplätze an der Theke anbieten. 

____________________

Gleisgarten Öffnungszeiten: 
Montag - Donnerstag 17 - 23 Uhr 
Freitag - Samstag 17 - 24 Uhr 
Sonntag 13 - 23 Uhr

Wir freuen uns auf euch! 🐇🦊🦆

