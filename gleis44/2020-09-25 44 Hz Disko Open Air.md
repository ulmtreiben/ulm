---
id: "1805625389575176"
title: 44 Hz Sitz-Disko (Gleis-Bar)
start: 2020-09-25 21:30
end: 2020-09-26 04:30
locationName: Gleis 44
address: Schillerstraße 44, 89077 Ulm
link: https://www.facebook.com/events/1805625389575176/
teaser: T R A S H !   Endlich mal wieder 44 Hz - wir bringen eine unserer schönsten
  Partyreihen in die Gleis-Bar-Sitz-Disko. Funk, 80s-Trash, 90er oder Old-Sc
isCrawled: true
---
T R A S H ! 

Endlich mal wieder 44 Hz - wir bringen eine unserer schönsten Partyreihen in die Gleis-Bar-Sitz-Disko. Funk, 80s-Trash, 90er oder Old-School-Hip-Hop - bis Freitag! 

Für euch an den Reglern sind SAM TRAVOLTA und BEN DISCO. 

Im Werk legt EKKO für euch Techno auf! 