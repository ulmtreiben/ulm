---
id: "1647738738719061"
title: SUBstance - Drum and Bass im Gleisgarten
start: 2020-08-12 17:30
end: 2020-08-12 22:00
locationName: Gleis 44
address: Schillerstraße 44, 89077 Ulm
link: https://www.facebook.com/events/1647738738719061/
teaser: TBA.   ____________________    Damit sich alle gut und sicher fühlen haben wir
  ein Hygiene-Konzept erarbeitet und werden alle Tische etc. sehr regelmä
isCrawled: true
---
TBA.

 ____________________


 Damit sich alle gut und sicher fühlen haben wir ein Hygiene-Konzept erarbeitet und werden alle Tische etc. sehr regelmäßig Desinfizieren. 
 Hier für euch kurz und bündig alle Änderungen: 
 - ihr dürft in Gruppen von maximal 10 Personen kommen und gemeinsam an einem Tisch Sitzen.
 - wir müssen eure Namen und Kontaktdaten aufnehmen und 4 Wochen speichern.
 - Bitte wartet am Eingang, unser Team weist euch Plätze zu. 
 - Leider können wir keine Stehplätze an der Theke anbieten.


 ____________________


 Gleisgarten Öffnungszeiten: 
 Sonntag - Donnerstag 17 - 23 Uhr 
 Freitag - Samstag 17 - 24 Uhr 
 Wir freuen uns auf euch! 🐇🦊🦆