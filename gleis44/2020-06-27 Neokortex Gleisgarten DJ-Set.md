---
id: "3083816908365327"
title: Neokortex Gleisgarten DJ-Set
start: 2020-06-27 17:00
end: 2020-06-27 22:00
locationName: Gleis 44
address: Schillerstraße 44, 89077 Ulm
link: https://www.facebook.com/events/3083816908365327/
image: 105037696_871984449965101_2313512785530651787_o.jpg
teaser: Neokortex spielt eines seiner letzten Sets in unserer schönen Stadt - Techno,
  House & Bass.   ____________________   Damit sich alle gut und sicher fü
isCrawled: true
---
Neokortex spielt eines seiner letzten Sets in unserer schönen Stadt - Techno, House & Bass. 

____________________

 Damit sich alle gut und sicher fühlen haben wir ein Hygiene-Konzept erarbeitet und werden alle Tische etc. sehr regelmäßig Desinfizieren. 
Hier für euch kurz und bündig alle Änderungen: 
- ihr dürft in Gruppen von maximal 10 Personen kommen und gemeinsam an einem Tisch Sitzen.
- wir müssen eure Namen und Kontaktdaten aufnehmen und 4 Wochen speichern.
- Bitte wartet am Eingang, unser Team weist euch Plätze zu. 
- Leider können wir keine Stehplätze an der Theke anbieten. 

____________________

Gleisgarten Öffnungszeiten: 
Montag - Donnerstag 17 - 23 Uhr 
Freitag - Samstag 17 - 24 Uhr 
Sonntag 13 - 23 Uhr

Wir freuen uns auf euch! 🐇🦊🦆

