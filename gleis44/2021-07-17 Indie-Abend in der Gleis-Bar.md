---
id: "205954668107918"
title: Indie-Abend in der Gleis-Bar
start: 2021-07-17 22:00
end: 2021-07-18 04:30
address: Schillerstraße 44, 89077 Ulm
link: https://www.facebook.com/events/205954668107918/
isCrawled: true
---
Indie, Britpop, Alternativ und ganz viel #amoooooooooore

Serviert werden Klassiker von The Cure oder The Smiths, Dauerbrenner von The Kooks, Kraftklub oder The Killers, aber auch viel Genreverwandtes rund um Bilderbuch, Daft Punk oder Casper. Hauptsache gute Laune!

Wir freuen uns - Bussi Baby 😉

DJ´s: Phil the Gap & San Francesco

Das Projekt "Gleis-Bar" ist gefördert durch den Beauftragten der Bundesregierung für Kultur und Medien („BKM“) und die Initiative Musik. Die Veranstaltungen laufen im Programm NEUSTART KULTUR. Thx dafür und soo! 