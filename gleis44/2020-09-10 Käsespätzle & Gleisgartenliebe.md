---
id: "944160766066005"
title: Käsespätzle & Gleisgartenliebe
start: 2020-09-10 17:00
end: 2020-09-10 22:00
locationName: Gleis 44
address: Schillerstraße 44, 89077 Ulm
link: https://www.facebook.com/events/944160766066005/
teaser: "Donnerstag gibt es selbstgemachte
  Käsespätzle!   ____________________  Gleisgarten Öffnungszeiten:  Sonntag -
  Donnerstag 17 - 23 Uhr  Freitag - Samsta"
isCrawled: true
---
Donnerstag gibt es selbstgemachte Käsespätzle! 

____________________

Gleisgarten Öffnungszeiten: 
Sonntag - Donnerstag 17 - 23 Uhr 
Freitag - Samstag 17 - 24 Uhr 

Wir freuen uns auf euch! 🐇🦊🦆

