---
id: "529174218027434"
title: Gleis Alive Stream Sonntags 15 Uhr
start: 2020-04-26 15:00
end: 2020-04-26 16:30
locationName: Gleis 44
address: Schillerstraße 44, 89077 Ulm
link: https://www.facebook.com/events/529174218027434/
image: 90917453_811719912658222_2705340268424462336_o.jpg
teaser: "Am Sonntag um 15 Uhr starten wir unseren ersten Gleis-Livestream mit
  schniecker Musik aus dem Club. ACHTUNG: Am Osterwochenende starten wir schon
  am S"
isCrawled: true
---
Am Sonntag um 15 Uhr starten wir unseren ersten Gleis-Livestream mit schniecker Musik aus dem Club. ACHTUNG: Am Osterwochenende starten wir schon am Samstagabend von 20 bis 23 Uhr! 

Den Anfang macht der wundervolle Paul im Werk-Floor mit einem furiosen Tech-House Set. Und in den kommenden Wochen präsentieren wir euch verschiedene Styles und Sounds in jeweils 90 Minuten zusammen mit dem Kollektiv Schillerstraße. Habt ihr Lust? 

- >> Gleis Alive! <3 <3 <3 

Supportet werden wir übrigens vom Team von VIRAL 2 FX, die uns kostenlos mit Kameras, Licht und ganz viel Expertise versorgen. Wir freuen uns über jeden, der das Team unterstützen möchte!!! Link zu Paypal: www.paypal.me/vir2alfx

______________________________________________

Bock auf noch mehr Musik? Checkt unseren Soundcloud: www.soundcloud.com/kollektivschillerstrasse