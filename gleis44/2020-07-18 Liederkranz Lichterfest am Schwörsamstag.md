---
id: "324191182070837"
title: Liederkranz Lichterfest am Schwörsamstag
start: 2020-07-18 16:00
end: 2020-07-18 23:30
locationName: Liederkranz Kulturbiergarten
address: Friedrichsau 9, 89073 Ulm
link: https://www.facebook.com/events/324191182070837/
image: 107597161_885648098598736_2314069204584783231_o.jpg
teaser: Lichterfest im Liederkranz. Wir feiern unsere eigene kleine Feuer-Fete mit der
  Band Donald Funk, einem großen haufen Wunderkerzen und viel Magie!   Di
isCrawled: true
---
Lichterfest im Liederkranz. Wir feiern unsere eigene kleine Feuer-Fete mit der Band Donald Funk, einem großen haufen Wunderkerzen und viel Magie! 

Die Veranstaltung wird ermöglich durch: 
Kunstwerk e.V. Ulm als Mitveranstalter bei Donald Funk
sowie:
SWU
Indauna e.V 
Gleis 44
Ministerium für Wissenschaft, Forschung und Kunst Baden-Württemberg

_______Donald Funk:

Tower of Power mit Aretha Franklin vorne dran? Stevie Wonder als Stargast bei Phat Phunction? So wie die Vorstellung dieser Kombis schwindlig macht, ist das doch das Konzept von Donald Funk. Leidenschaftliche Vocals, ebensolche Grooves, messerscharfe Bläsersätze, jede Menge Spaß und Spielfreude, die schwuppdiwupp auf das Publikum überspringen. Wenn das mal kein Ersatz für eine Lichterserenade ist!

Didi Knoblauch - voc, Anni Schießl - voc, Jonas Meyer - kb, Matthias Müller - git, Matthias Kost - bs, Florian Krauss - dr, Britta Meixner - sax, voc, Maren Eisele - sax, Daniel Köhl - sax, Christian Link - trp, Christof Löhle - trp

_______


Der Abend findet im Rahmen von TASTE Street Food to go Picknick in der Au wir Schwören. statt, für Kulinarische Untermalung ist also gesorgt! : ))) 

____________________
Damit sich alle gut und sicher fühlen haben wir ein Hygiene-Konzept erarbeitet und werden alle Tische etc. sehr regelmäßig Desinfizieren.
Hier für euch kurz und bündig alle Änderungen:

- ihr dürft in Gruppen von maximal 20 Personen kommen und gemeinsam an einem Tisch Sitzen.
- wir müssen eure Namen und Kontaktdaten aufnehmen und 4 Wochen speichern.
- Bitte wartet am Eingang, unser Team weist euch Plätze zu.
- Leider können wir keine Stehplätze an der Theke anbieten.

____________________

Liederkranz Öffnungszeiten:
Montag - Freitag ab 17 Uhr
Samstag, Sonntag & Feiertags ab 12 Uhr
Wir freuen uns auf euch! 🐇🦊🦆
