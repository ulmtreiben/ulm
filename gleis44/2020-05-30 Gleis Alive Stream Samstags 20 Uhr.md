---
id: "558361421775380"
title: Gleis Alive Stream Samstags 20 Uhr
start: 2020-05-30 20:00
end: 2020-05-30 23:00
locationName: Gleis 44
address: Schillerstraße 44, 89077 Ulm
link: https://www.facebook.com/events/558361421775380/
image: 93127717_822976898199190_3536106536835219456_o.jpg
teaser: Schöne Musik und bunte Farben! Jeden Samstag streamen wir über YouTube aus dem
  Gleis in eure WG-Küchen, Wohnzimmer und sogar auf den Balkon!   - >> Gl
isCrawled: true
---
Schöne Musik und bunte Farben! Jeden Samstag streamen wir über YouTube aus dem Gleis in eure WG-Küchen, Wohnzimmer und sogar auf den Balkon! 

- >> Gleis Alive! <3 <3 <3 

Supportet werden wir  vom Team von VIRAL 2 FX, die uns kostenlos mit Kameras, Licht und ganz viel Expertise versorgen. Wir freuen uns über jeden, der das Team unterstützen möchte!!! Link zu Paypal: www.paypal.me/vir2alfx

______________________________________________

Bock auf noch mehr Musik? Checkt unseren Soundcloud: www.soundcloud.com/kollektivschillerstrasse