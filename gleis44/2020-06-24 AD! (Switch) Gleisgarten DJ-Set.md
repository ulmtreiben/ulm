---
id: "698883367324066"
title: AD! (Switch) Gleisgarten DJ-Set
start: 2020-06-24 18:00
end: 2020-06-24 22:00
locationName: Gleis 44
address: Schillerstraße 44, 89077 Ulm
link: https://www.facebook.com/events/698883367324066/
image: 104478418_869453173551562_2238591005151835734_o.jpg
teaser: HOUSE  ____________________  Damit sich alle gut und sicher fühlen haben wir
  ein Hygiene-Konzept erarbeitet und werden alle Tische etc. sehr regelmäßi
isCrawled: true
---
HOUSE

____________________

Damit sich alle gut und sicher fühlen haben wir ein Hygiene-Konzept erarbeitet und werden alle Tische etc. sehr regelmäßig Desinfizieren. 
Hier für euch kurz und bündig alle Änderungen: 
- ihr dürft in Gruppen von maximal 10 Personen kommen und gemeinsam an einem Tisch Sitzen.
- wir müssen eure Namen und Kontaktdaten aufnehmen und 4 Wochen speichern.
- Bitte wartet am Eingang, unser Team weist euch Plätze zu. 
- Leider können wir keine Stehplätze an der Theke anbieten. 

____________________

Gleisgarten Öffnungszeiten: 
Montag - Donnerstag 17 - 23 Uhr 
Freitag - Samstag 17 - 24 Uhr 
Sonntag 13 - 23 Uhr

Wir freuen uns auf euch! 🐇🦊🦆

