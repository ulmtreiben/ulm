---
id: "286895872659543"
title: Frieden braucht Bewegung (Plakatausstellung)
start: 2020-09-12 09:00
end: 2020-09-12 17:00
locationName: Einsteinhaus Ulm
address: Kornhausplatz 5, D-89073  Germany, 89073 Ulm
link: https://www.facebook.com/events/286895872659543/
teaser: Eine Geschichte der Friedensbewegung in Plakaten und Bildern. „Frieden braucht
  Bewegung“ hieß es damals, und es kam tatsächlich zu einer Bewegung. Ein
isCrawled: true
---
Eine Geschichte der Friedensbewegung in Plakaten und Bildern.
„Frieden braucht Bewegung“ hieß es damals, und es kam tatsächlich zu einer
Bewegung. Einige haben als Aktivisten mitgemacht und sehr viele beteiligten sich.
Heute ist das Friedensengagement wichtiger denn je. Die Ausstellung sensibilisiert,
liefert Denkanstöße und setzt ein Zeichen für den Frieden auf Erden.
EinsteinHaus, Kornhausplatz 5, 89073 Ulm
Eintritt frei
Veranstalter: Freundschaft, Kultur und Jugend e. V. Ulm (DIDF-Ulm)