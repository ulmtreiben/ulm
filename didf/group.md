---
name: DIDF
website: https://www.facebook.com/pg/DIDFUlm/
email: dkg-ulm@hotmail.de
scrape:
  source: facebook
  options:
    page_id: DIDFUlm
---
Der Verein wurde im Jahre 1980 durch Migranten aus der Türkei gegründet. Hauptaufgabe des Vereines ist es, durch ihre Aktivitäten, eine Brücke zwischen inländischen und migrantischen Mitbürgern aufzubauen.
