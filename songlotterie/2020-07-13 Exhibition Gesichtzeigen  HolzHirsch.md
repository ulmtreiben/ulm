---
id: "929023984176383"
title: "Exhibition: Gesichtzeigen // HolzHirsch"
start: 2020-07-13 17:00
end: 2020-07-13 23:00
locationName: Liederkranz Kulturbiergarten
address: Friedrichsau 9, 89073 Ulm
link: https://www.facebook.com/events/929023984176383/
image: 101807740_123474936037448_2055286303771066368_o.jpg
teaser: Die Kunstetagen Neu Ulm präsentieren  Gesichtgeben Vernissage im Liederkranz
  Ulm 10.06.2020 			  Lisa Rudolf, Ulrike Honlet, Manuel Stahl, Mark Klawik
isCrawled: true
---
Die Kunstetagen Neu Ulm präsentieren

Gesichtgeben Vernissage im Liederkranz Ulm
10.06.2020
			

Lisa Rudolf, Ulrike Honlet, Manuel Stahl, Mark Klawikowski und Steve de Coco   

Das zu Beginn des Jahres durch Mark Klawikowski und Steve de Coco initiierte Projekt möchte dem Besucher  die Bedeutung des Werkstoff Holz nahe legen und wählt als Medium ausrangierte Industriehölzer, die eindrucksvoll in Form und Farbe gestaltet wurden. Die so entstandenen Großformat Gesichter, zeigen sie Diversität und Einzigartigkeit des menschlichen Daseins und den damit verbundenen handwerklichen Upcycling Prozess.

Durch die ausgebrochene Pandemie, erhielt das Thema eine noch größere Bedeutung, da es durch den Umstand der Ausgangsbeschränkungen und Maskenpflicht, lange Zeit nur schwer war, Gesichter und deren Mimik zu erkennen.
Erschwerend hinzu kam, dass Kunst und Kultur auch für einige Monate nicht erlebt werden konnten – so entstand die Zusammenarbeit mit dem Team des Gleis44 und der damit verbundenen Location Liederkranz. Aus der Vielzahl der Umstände kann letzten Endes unter freiem Himmel, in einer wundervollen örtlichen Gegebenheit Kunst unkonventionell erlebt werden.

„In einer Zeit, die von einem Virus die Menschen zur Einkehr zwingt, liegt es uns Nahe,
den Mensch und die Natur wieder zusammen zu fügen.
Ein weiteres Wunder des Werkstoff Holz zu entdecken.
Sich das Wunder des Lebens bewusst machen.
Bunte Dankbarkeit.
Zu erleben.“

 ____________________
 
 
Damit sich alle gut und sicher fühlen haben wir ein Hygiene-Konzept erarbeitet und werden alle Tische etc. sehr regelmäßig Desinfizieren. 
Hier für euch kurz und bündig alle Änderungen: 
- ihr dürft in Gruppen von maximal 10 Personen kommen und gemeinsam an einem Tisch Sitzen.
- wir müssen eure Namen und Kontaktdaten aufnehmen und 4 Wochen speichern.
- Bitte wartet am Eingang, unser Team weist euch Plätze zu. 
- Leider können wir keine Stehplätze an der Theke anbieten. 

 
 ____________________
 
 
 Liederkranz Öffnungszeiten: 
 Montag - Freitag ab 12 Uhr
 Samstag, Sonntag & Feiertags ab 12 Uhr
 
 Wir freuen uns auf euch! 🐇🦊🦆