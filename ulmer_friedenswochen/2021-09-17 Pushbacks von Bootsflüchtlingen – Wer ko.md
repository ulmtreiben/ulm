---
id: "344959027358683"
title: Pushbacks von Bootsflüchtlingen – Wer kontrolliert Frontex?
start: 2021-09-17 19:30
link: https://www.facebook.com/events/344959027358683/
image: 237526652_6634799673212072_7203304599652719108_n.jpg
isCrawled: true
---
Seit März 2020 haben griechische Einsatzkräfte weit über 100 Boote mit Geflüchteten „abgefangen“. Hierin sind europäische Einsatzkräfte von Frontex verwickelt, die Flüchtlingsboote stoppen und sie der griechischen Küstenwache übergeben. Asylsuchende werden zurück in türkische Gewässer geschleppt und oft auf aufblasbaren Rettungsinseln ausgesetzt. Und das alles trotz des Rechts auf individuelle Prüfung und des Verbots, Geflüchtete in Gefahr zu bringen.  

Veranstalter:
- Forum Asyl und Menschenrechte
- Flüchtlingsrat Ulm/Alb-Donau-Kreis e. V.
- menschlichkeit-ulm e. V.
-  Evangelischer Diakonieverband Ulm/Alb-Donau
- Förderverein für das Behandlungszentrum für Folteropfer Ulm e. V.

Anmeldung zur Online-Veranstaltung: 
dietmar.oppermann@kirche-diakonie-ulm.de

Mehr Informationen über die Ulmer Friedenswochen:
https://www.friedenswochen-ulm.de
