---
id: "2994424954212988"
title: Julian Assange - Der Friedenspreisträger
start: 2021-09-08 20:00
locationName: Einsteinhaus Ulm
address: Kornhausplatz 5, D-89073  Germany, 89073 Ulm
link: https://www.facebook.com/events/2994424954212988/
image: 235386326_6592232404135466_407516597894870166_n.jpg
isCrawled: true
---
Julian Assange hat für seine Arbeit eine Vielzahl an Auszeichnungen entgegengenommen. Neben journalistischen Auszeichnungen wurden ihm auch einige Friedenspreise verliehen, wie beispielsweise der Yoko Ono Courage Award oder die Sidney Peace Medal, die vor ihm nur Nelson Mandela und der Dalai Lama erhalten hatten. Amnesty International verlieh Wikileaks den Media Award (New Media) für „Kenya: The Cry of Blood – Extra Judicial Killings and Disappearances“, ein Bericht über Hinrichtungen ohne Gerichtsverfahren und Verschwindenlassen in Kenia. Es folgte der Global Exchange Human Rights Award, von People's Choice. Der brasilianische Presseverband Associação Brasileira de Imprensa (ABI) in Rio de Janeiro würdigte Assange mit der Medaille für Menschenrechte. Zuletzt wurde er auch in Deutschland mit dem Stuttgarter Friedenspreis 2020 geehrt. Er ist 2021 insgesamt zum 9. Male für den Friedensnobelpreis nominiert. In diesem Jahr von Abgeordneten aus Australien, Norwegen, Frankreich und der Slowakei.
An diesem Abend wollen wir, begleitet von Musik, zeigen, in welcher Form sich Julian Assange für Frieden eingesetzt hat, mit welcher Begründung er ausgezeichnet wurde und warum das so wichtig für unsere Zeit ist.
 
Veranstalter: Free Assange Gruppe, Ulm

Eintritt frei

Mehr Informationen über die Ulmer Friedenswochen:
https://www.friedenswochen-ulm.de


