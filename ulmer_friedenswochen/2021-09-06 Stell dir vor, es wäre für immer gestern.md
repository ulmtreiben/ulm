---
id: "511384356734466"
title: Stell dir vor, es wäre für immer gestern
start: 2021-09-06 16:00
end: 2021-09-06 20:00
address: Münsterplatz, 89073 Ulm
link: https://www.facebook.com/events/511384356734466/
image: 235691056_6592220737469966_6320440069614797630_n.jpg
isCrawled: true
---
Wenn Kunst auf Politik klatscht, dann ist es Mein ICH gegen Rassismus.
Es ist ein Collag der Zeit, durch welche Sie die Gründerin Esra Oneli mit Texten à la Avantgarde entführt, kraftvoll untermalt mit tänzerischen Elementen von Ruth Weigel. Reisen Sie live in ein Klassenzimmer mit Lena Saulich und erleben Sie unter Leitung des Dreiergespanns den ersten experimentellen Live-Workshop mit Schüler*innen.
Musikalischer Support von Serotonia, We Say So und Moltke und Mörike. 

Veranstalter: Mein ICH gegen Rassismus e. V.

Eintritt frei 

Mehr Informationen über die Ulmer Friedenswochen:
https://www.friedenswochen-ulm.de

