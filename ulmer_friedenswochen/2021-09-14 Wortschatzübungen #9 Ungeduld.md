---
id: "1432302853778140"
title: 'Wortschatzübungen #9: "Ungeduld"'
start: 2021-09-14 19:30
locationName: Stadtbibliothek Ulm
address: Ulm
link: https://www.facebook.com/events/1432302853778140/
image: 189239158_5940336589317707_3141676176584233760_n.jpg
isCrawled: true
---
Warten und Stillstand werden oft begleitet von Ungeduld. Wann wird es besser, anders oder wieder so wie früher? In der Ungeduld kann Unzufriedenheit und Unsicherheit stecken, aber auch Energie und Mut zur Veränderung.
An diesem Abend erfahren wir, wie Personen des öffentlichen Lebens aus Ulm und um Ulm herum dieses Thema verstehen und wo sie es in der Literatur wiederfinden.

Die Veranstaltung findet im Rahmen der Ulmer Friedenswochen statt.

Eintritt frei