---
id: "420171426098801"
title: Welten aktiv gestalten und verändern (Lesung – Gespräch – Musik)
start: 2021-09-15 19:30
locationName: Bürgerhaus Mitte
address: Schaffner Straße, 89073 Ulm
link: https://www.facebook.com/events/420171426098801/
image: 239309198_6634761983215841_3168494690124971017_n.jpg
isCrawled: true
---
„Ich kämpfe um meine Freiheit. Um meine Zugehörigkeit. Um meinen Platz in dieser Welt. Um das Recht, anzukommen und zu heilen.“ - 
Als Mädchen ﬂüchtete Muna AnNisa Aikins aus einem Kriegsgebiet. In Deutschland fand sie Schutz – und durfte dennoch nicht dazugehören. Heute will sie mit Literatur die Debatte über das Flüchten verändern. 

Referentin: Muna AnNisa Aikins

Veranstalter:
- Forum Asyl und Menschenrechte
- Ulmer Netz für eine andere Welt e. V.
- Verein Ulmer Weltladen e. V.,
- Flüchtlingsrat Ulm/Alb-Donau-Kreis e. V.
- menschlichkeit-ulm e. V.
-  Evangelischer Diakonieverband Ulm/Alb-Donau
- Evangelisches Bildungswerk Alb-Donau
- Förderverein für das Behandlungszentrum für Folteropfer Ulm e. V.
- Stiftung Menschenrechtsbildung
- Eine Welt-Regionalpromotorin

Ort fürs Navi: Bürgerhaus Mitte, Schaffnerstraße 17, 89073 Ulm  

 Eintritt frei, Spenden erbeten

Mehr Informationen über die Ulmer Friedenswochen:
https://www.friedenswochen-ulm.de
