---
id: "744160716399625"
title: "Friedensbotschaften: Freeden on Air"
start: 2020-09-01 10:00
end: 2020-09-04 00:00
address: Ulm
link: https://www.facebook.com/events/744160716399625/
teaser: In diesem Zeitraum dreht sich auf der 102,6 MHz alles um das Thema „Frieden“.
  Wie viel Frieden steckt in Ulm? Was bedeutet eigentlich Frieden? Und wie
isCrawled: true
---
In diesem Zeitraum dreht sich auf der 102,6 MHz alles um das Thema „Frieden“. Wie
viel Frieden steckt in Ulm? Was bedeutet eigentlich Frieden? Und wie erhält man
Frieden? Dies und vieles mehr erfahren Sie in Beiträgen, Recherchen und Interviews
auf UKW: 102,6 MHz und anschließend im Stream unter freefm.de
Veranstalter: Radio free FM Ulm