---
id: "272694167573336"
title: FGM – weibliche Genitalverstümmelung und was sie mit uns zu tun hat
start: 2021-09-16 19:00
locationName: Einsteinhaus Ulm
address: Kornhausplatz 5, D-89073  Germany, 89073 Ulm
link: https://www.facebook.com/events/272694167573336/
image: 237144285_6634779499880756_1291429107768605759_n.jpg
isCrawled: true
---
Weltweit sind 200 Millionen Mädchen und Frauen betroffen, mit gravierenden Folgen für ihre körperliche und seelische Gesundheit, 70.000 Frauen in Deutschland und 15.000 Mädchen sind bedroht. Wie reagiert man, wenn man im Ehrenamt oder als Fachfrau damit konfrontiert wird? Wo kann man sich engagieren, um gegen diese Menschenrechtsverletzung anzukämpfen? Die Expertin für fgm-c, Fadumo Korn, geboren in Somalia, berichtet und zeigt kultursensible Wege auf.

Veranstalter:
- Verein Ulmer Weltladen e. V.
- WeMuFra-Gleichstellungsstelle LRA Neu-Ulm
- Frauenforum Ulm
- vh Ulm 

Ort:
EinsteinHaus, Kornhausplatz 5, 89073 Ulm  

Eintritt frei, Spenden erbeten

 Mehr Informationen über die Ulmer Friedenswochen:
https://www.friedenswochen-ulm.de
