---
id: "816287049251998"
title: Stolperstein - Friedenssteine
start: 2021-09-24 14:00
end: 2021-09-24 17:00
address: Bahnhofstraße 1, 89073 Ulm
link: https://www.facebook.com/events/816287049251998/
image: 237023637_6634967546528618_5134113638732934284_n.jpg
isCrawled: true
---
„Frieden ist ohne Erinnerung an die Vergangenheit nicht möglich“ In Ulm erinnern mittlerweile 109 Stolpersteine vor den jeweiligen Wohnorten an von Nationalsozialisten ermordete und verfolgte Bürgerinnen und Bürger der Stadt.
Bei der diesjährigen Verlegung während der Friedenswochen kommen 16 neue Gedenksteine hinzu. Dauer: 2,5 bis 3 Stunden. Nähere Informationen: stolpersteine-fuer-ulm.de und in der Tageszeitung

Veranstalter:
Stolpersteininitiative Ulm 

Start: Bahnhofstraße 1 , Ulm 

Eintritt frei

Mehr Informationen über die Ulmer Friedenswochen:
https://www.friedenswochen-ulm.de
