---
id: "381345146162835"
title: Verhandlungsspiel Klimakonferenz
start: 2020-09-17 18:00
address: Ulm
link: https://www.facebook.com/events/381345146162835/
teaser: Bäume pflanzen oder Abgas reduzieren? Im fiktiven „Verhandlungsspiel
  Klimakonferenz“ verhandeln Industrieländer und Entwicklungsländer um ihren
  Beitra
isCrawled: true
---
Bäume pflanzen oder Abgas reduzieren? Im fiktiven „Verhandlungsspiel
Klimakonferenz“ verhandeln Industrieländer und Entwicklungsländer um ihren
Beitrag zur Rettung des Weltklimas. Eine vom MIT entwickelt Computersimulation
rechnet für jedes Verhandlungsergebnis aus, um wieviel die globale
Durchschnittstemperatur ansteigen wird...
Begrenzte Teilnehmerzahl, daher bitte im Agenda-Büro anmelden:
agendabuero@ulm.de, Telefon 0731-161 1015.
Der Ort wird bei Anmeldung bekanntgeben
Eintritt frei
Veranstalter: Fridays for Future Ulm