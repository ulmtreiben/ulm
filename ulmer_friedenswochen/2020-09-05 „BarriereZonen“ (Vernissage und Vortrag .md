---
id: "203477651070349"
title: „Barriere:Zonen“ (Vernissage und Vortrag von Till Mayer)
start: 2020-09-05 17:00
address: ROXY.ulm
link: https://www.facebook.com/events/203477651070349/
teaser: Konflikte und Kriege werfen einen langen Schatten. Sie hinterlassen körperlich
  und seelisch Versehrte. Sie rauben Leben, Zukunft und Hoffnung. Manche
isCrawled: true
---
 Konflikte und Kriege werfen einen langen Schatten. Sie hinterlassen körperlich und
seelisch Versehrte. Sie rauben Leben, Zukunft und Hoffnung. Manche der Kriege
sind seit Jahrzehnten vorbei, andere fordern noch immer neue Opfer. Die
Portraitierten kämpfen als Menschen mit Behinderung um ein würdiges Leben. Jeden
Tag aufs Neue. Die Portraits geben Mut, andere stimmen traurig. „Sie alle fordern
Respekt ein“, sagt Journalist und Fotograf Till Mayer.
ROXY Ulm
Eintritt: frei
Verannstaltrer: ROXY gemeinnützige GmbH
In Kooperation mit Till Mayer und Handicap International