---
id: "522152339046153"
title: Flucht und Seenotrettung (interaktiver Mitmachstand)
start: 2021-09-03 15:00
end: 2021-09-03 18:00
address: Hans-und-Sophie-Scholl-Platz, 89073 Ulm
link: https://www.facebook.com/events/522152339046153/
image: 235294913_6592026717489368_2780564125639106330_n.jpg
isCrawled: true
---
Mit Geschichten zum Mitnehmen/Anhören/Durchlesen/ Nachempfinden von betroffenen Menschen wollen wir Sie mitnehmen auf eine Reise nach Afrika.
Sie sehen Kurzfilme und eine Präsentation. Es wird Ihnen die Arbeit und Notwendigkeit der Seenotrettung dargestellt. Für die Hilfe im Mittelmeer haben Sie die Möglichkeit, aktiv zu werden. Wir stellen gemeinsam Dinge her und sind kreativ. Hierbei sollen Gespräche stattfinden, die über die internationalen Grenzen hinausgehen.

Veranstalter: Sea-Eye Gruppe Ulm
Eintritt frei, Spenden für die Seenotrettung erbeten

Mehr Informationen über die Ulmer Friedenswochen:
https://www.friedenswochen-ulm.de
