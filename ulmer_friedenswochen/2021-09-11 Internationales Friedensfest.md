---
id: "2992783334377383"
title: Internationales Friedensfest
start: 2021-09-11 12:00
end: 2021-09-11 22:00
locationName: Fort Unterer Kuhberg
address: 10  Unterer Kuh berg, 89077 Ulm
link: https://www.facebook.com/events/2992783334377383/
image: 241433667_6744341822257856_6152469729382860324_n.jpg
isCrawled: true
---
Das Friedensfest ist ein Tag der Begegnung von Familien vieler Nationalitäten, von Mitgliedern der Friedensbewegung von Freunden engagierter Musik.
Wir setzen ein Zeichen für ein Leben in Frieden, für eine Welt ohne Kriege und für eine Welt in sozialer Gerechtigkeit. Nur im gegenseitigen Kennenlernen können Vorurteile abgebaut werden. In der Begegnung, im gemeinsamen Tun, entstehen Akzeptanz und Toleranz, Gemeinsamkeiten werden sichtbar, Freundschaften möglich. Lasst uns zusammen feiern!

Ab 12 Uhr:
•	Mittagessen, internationale Küche
•	Infostände der teilnehmenden Organisationen

Kinderprogramm ab 14 Uhr:
•	Flohmarkt für Kinder, Mitmachaktionen, Kreativzelt
•	Clown Mizzi und Kindertheater

Bühnenprogramm:
•	ab 14:30 Uhr: Feschtagsmusik Fetzige Folk-Musik und engagierte Texte, lässig, groovig, kurzweilig.
•	ab 16 Uhr: Walter Spira 
Im Zeichen großer Fabulier- und Parodierlust, frech, spitzbübisch und unverkrampft, mal ironisch zubeißend, mal witzig-melancholisch, mal einfühlsam und nachdenklich.
•	ab 17.30 Uhr Unter Wilden 
Die bunte Welt des Pop, die nostalgische Welt des Chanson oder die des gutgelaunten Reggae – deutsche Texte, Unterhaltung mit Tiefgang und guter Laune.
	ab 17.30 Uhr Black Frog Friday
pure Rock ´n´ Roll without Bullshit 

