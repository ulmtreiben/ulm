---
id: "877465836501928"
title: Klimawandel als Konfliktursache?
start: 2021-09-01 18:00
address: DGB-Haus Ulm, Weinhof 23, 89073 Ulm
link: https://www.facebook.com/events/877465836501928/
image: 235044411_6592002444158462_5148754627142724183_n.jpg
isCrawled: true
---
Umwelteinflüsse wie Verschmutzung, Bodenverschlechterung und extreme Wetterlagen haben zu einer Zunahme des Hungers in der Welt geführt – etwa 800 Millionen Menschen vom Sahel bis Südostasien sind betroffen. Demzufolge nehmen Streitigkeiten über Ressourcen zu und führen zu Konflikten oder verschärfen diese. Wir wollen über die Situation informieren und nach Lösungen suchen. 

Referentin: Raquel Munayer (adelphi) 
Anmeldung unter ulm@dgb.de
Veranstaltung: DGB Ulm/Alb-Donau
Eintritt frei

Mehr Informationen über die Ulmer Friedenswochen:
https://www.friedenswochen-ulm.de

