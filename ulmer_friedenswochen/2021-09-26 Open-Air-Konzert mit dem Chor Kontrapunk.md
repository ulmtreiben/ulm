---
id: "538507804149984"
title: Open-Air-Konzert mit dem Chor Kontrapunkt
start: 2021-09-26 17:00
address: Grüner Hof 2, 89073 Ulm
link: https://www.facebook.com/events/538507804149984/
image: 239468179_6635048603187179_3464295214181613765_n.jpg
isCrawled: true
---
Der Chor Kontrapunkt kommt unter neuer musikalischer Leitung von Agnes Schmauder mit einem neuen Programm zurück auf die Bühne. Wir beleuchten in diesem Programm das gesellschaftliche Mit- und Gegeneinander bei uns und anderswo auf der Welt, im Privaten, im Betrieb, in der Gemeinde, in der Gesellschaft.

Veranstalter:
Chor Kontrapunkt e. V.

Ort: 
Reichenauer Hof, Grüner Hof 2, vor dem Minnesängersaal  

Eintritt frei  

Mehr Informationen über die Ulmer Friedenswochen:
https://www.friedenswochen-ulm.de
