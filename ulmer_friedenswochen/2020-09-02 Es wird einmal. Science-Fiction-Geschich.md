---
id: "345653486436502"
title: Es wird einmal. Science-Fiction-Geschichten gegen den Krieg
start: 2020-09-02 20:00
address: 14  Fort Albeck, 89075 Ulm
link: https://www.facebook.com/events/345653486436502/
teaser: "ACHTUNG: Durch einen Fehler war die Veransfaltung auf 18 Uhr angekündigt.
  RICHTIG ist aber: 20 Uhr  Es gibt nicht nur Krieg der Sterne und Military-SF"
isCrawled: true
---
ACHTUNG: Durch einen Fehler war die Veransfaltung auf 18 Uhr angekündigt. RICHTIG ist aber: 20 Uhr

Es gibt nicht nur Krieg der Sterne und Military-SF, sondern auch Science- und Social-Fiction-Autoren, die in ihren Texten deutlich Stellung beziehen gegen Krieg, Militarismus und Gewaltphantasien. Wulf Neuschwander, Reinhard Köhler und Johannes Honnef lesen solche Texte vor, Johannes Honnef und Reinhard Köhler steuern außerdem mit ungewöhnlichen Instrumenten ein paar kleine Klanglandschaften bei.
Fort Albeck 14 , 89075 Ulm
Eintritt frei
Veranstalter: Übermorgenwelt Ulm e. V.
