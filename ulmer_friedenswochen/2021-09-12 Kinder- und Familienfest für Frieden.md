---
id: "169494805291528"
title: Kinder- und Familienfest für Frieden
start: 2021-09-12 13:00
locationName: Naturfreundehaus Spatzennest
address: 89134 Weidach
link: https://www.facebook.com/events/169494805291528/
image: 238057899_6634633079895398_6861881843977956766_n.jpg
isCrawled: true
---
Nach der Friedenswanderung der NaturFreunde vom 12. Mai bis zum 4. Juli 2021 durch Deutschland laden die NaturFreunde Ulm zum Kinder- und Familienfest ein. Auch dieses Jahr wird es eine Kinderaktion zum Thema Frieden geben. Mitmachaktionen, Friedenslieder, Tombola... Ein großes Fest des Friedens feiern wir im wunderschönen Naturfreundehaus Spatzennest.

Veranstalter: NaturFreunde Ulm e. V.

Mehr Informationen über die Ulmer Friedenswochen:
https://www.friedenswochen-ulm.de

