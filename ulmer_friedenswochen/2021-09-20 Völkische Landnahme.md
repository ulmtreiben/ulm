---
id: "208526661237541"
title: Völkische Landnahme
start: 2021-09-20 19:30
address: Bürgerhaus Mitte
link: https://www.facebook.com/events/208526661237541/
image: 237290546_6634894189869287_4893852678189563685_n.jpg
isCrawled: true
---
Seit Jahren siedeln sich extreme Rechte bewusst in ländlichen Regionen der Bundesrepublik an, um dort mit ansässigen völkischen Großfamilien „nationale Graswurzelarbeit“ zu betreiben. Sie betreiben ökologische Landwirtschaft, pflegen altes Handwerk; es herrschen rückwärtsgewandte Rollenbilder und autoritäre Erziehungsmuster. Andrea Röpke gibt Einblick in diese Bewegungen, die einer demokratischen Gesellschaft gefährlich werden.

Veranstalter:
VVN-BdA Ulm

Zoom Meeting
https://uni-ulm.zoom.us/j/67080932008?pwd=dVgwb0p2dVd0NTVoQ2V5OGhOdnNWUT09
Meeting ID: 670 8093 2008
Passcode: 49055157

Eintritt frei  


Mehr Informationen über die Ulmer Friedenswochen:
https://www.friedenswochen-ulm.de

