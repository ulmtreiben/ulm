---
id: "216980187149765"
title: Atomwaffen sind zwar weltweit verboten, aber nicht bei uns
start: 2021-09-22 18:00
link: https://www.facebook.com/events/216980187149765/
image: 236591658_6634916469867059_3248015607835813507_n.jpg
isCrawled: true
---
Wie bewegen wir die Bundesregierung zum Umdenken?

Obwohl Atomwaffen weltweit verboten sind, lagert die BRD in Büchel Atombomben und verschwendet Milliarden, um im Rahmen einer nuklearen Teilhabe atomare Lenkwaffen loszuschicken. Was können wir tun, um unsere Regierung zum Umdenken zu bewegen? 

Referenten: Arailym Kubayeva  (Internationalen Kampagne zur Abschaffung von Atomwaffen) und Patrick Wödl  (Friedenswerkstatt Mutlangen)

Veranstalter: 
- Ulmer Ärzteinitiative/IPPNW
- Friedenswerkstatt Mutlangen
- Verein für Friedensarbeit Ulm e. V.

Online-Veranstaltung
Keine Gebühr, Anmeldung unter info@ippnw-ulm.de

Mehr Informationen über die Ulmer Friedenswochen:
https://www.friedenswochen-ulm.de
