---
id: "765405130894857"
title: Das „Lieferkettengesetz“
start: 2020-09-24 19:30
end: 2020-09-24 22:30
locationName: Gleis 44
address: Schillerstraße 44, 89077 Ulm
link: https://www.facebook.com/events/765405130894857/
teaser: Gesprächsrunde über Wirtschaft und Menschenrechte Zur Zeit wird in Deutschland
  intensiv über ein „Lieferkettengesetz“ diskutiert. Hintergrund dieser D
isCrawled: true
---
Gesprächsrunde über Wirtschaft und Menschenrechte
Zur Zeit wird in Deutschland intensiv über ein „Lieferkettengesetz“ diskutiert. Hintergrund dieser Diskussion ist, dass auch deutsche Unternehmen weltweit an Menschenrechtsverletzungen und Umweltzerstörungen beteiligt sind, ohne dass sie dafür rechtliche Konsequenzen befürchten müssen. Jetzt geht es darum, die menschenrechtliche Sorgfaltspflicht von Unternehmen gesetzlich festzuschreiben. Wie stehen die Bundestagsabgeordneten aus der Region dazu?
   
Teilnehmer/innen: Bundestagsabgeordnete der Region Ulm/Neu-Ulm
Eintritt frei
Veranstalter: Ulmer Netz für eine andere Welt e.V., Verein UImer Weltladen e.V., Eine Welt-Regionalpromotorin
