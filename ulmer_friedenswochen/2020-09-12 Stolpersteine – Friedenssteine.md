---
id: "325171215183063"
title: Stolpersteine – Friedenssteine
start: 2020-09-12 14:30
address: Weinhof (am Brunnen) Ulm
link: https://www.facebook.com/events/325171215183063/
teaser: „Frieden ist ohne Erinnerung an die Vergangenheit nicht möglich.“ Seit Jahren
  werden auch in Ulm Stolpersteine verlegt – mittlerweile sind es 108 Stei
isCrawled: true
---
„Frieden ist ohne Erinnerung an die Vergangenheit nicht möglich.“
Seit Jahren werden auch in Ulm Stolpersteine verlegt – mittlerweile sind es
108 Steine an ehemaligen Wohnorten ermordeter und verfolgter Ulmer
Bürgerinnen und Bürger. Wir machen einen Gang durch Ulm zu einigen dieser
Gedenksteine für Opfer des Nationalsozialismus.
Dauer ca. 1 ½ Stunden
Treffpunkt: Weinhof (am Brunnen), 89073 Ulm
Eintritt frei.
Veranstalter: Stolpersteininitiative Ulm
