---
id: "203966728365521"
title: Lieder für die Friedenstaube
start: 2021-09-04 18:00
locationName: Einsteinhaus Ulm
address: Kornhausplatz 5, D-89073  Germany, 89073 Ulm
link: https://www.facebook.com/events/203966728365521/
image: 235128099_6596370983721608_1237121997029555566_n.jpg
isCrawled: true
---
Helga Kölle-Köhler und Ayhan Coskun singen Lieder für glückliche Tage und über bedrückende Zeiten des Krieges. Christa Mayerhofer fragt sich als Autorin und Erzählerin des Märchens vom Elefanten, vom Affen, vom Hasen oder der Taube, was denn für das gute Zusammenleben entscheidend ist. Lieder zu kunstvoller Gitarrenbegleitung wechseln sich ab mit der spannenden Geschichte, so wird das Programm zu guter Unterhaltung für die ganze Familie.

Veranstalter: DIDF (unterstützt durch Demokratieverein Wiblingen e. V. und vh Ulm)
Eintritt frei

Mehr Informationen über die Ulmer Friedenswochen:
https://www.friedenswochen-ulm.de
