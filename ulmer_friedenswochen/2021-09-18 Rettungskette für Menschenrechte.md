---
id: "139531508331353"
title: Rettungskette für Menschenrechte
start: 2021-09-18 12:00
address: Treffpunkt ab 11:15 Uhr auf dem Hans-und-Sophie-Scholl-Platz
link: https://www.facebook.com/events/139531508331353/
image: 237173251_6634813236544049_788556715749766375_n.jpg
isCrawled: true
---
Helft mit! – bei der Rettungskette für Menschenrechte, die von der Nordsee bis zum Mittelmeer reichen soll. Zusammen mit vielen Menschen aus Deutschland, Österreich und Italien setzen wir ein Zeichen für Menschlichkeit, Menschenrechte und gegen das Sterben im Mittelmeer. Mit der Corona-Pandemie ist das Leid der Geflüchteten an den Außengrenzen Europas nicht weniger geworden.

Auf der anschliessenden Kundgebung ab 12:45 Uhr werden auf dem Hans-und-Sophie-Scholl-Platz reden:

Fatima Bayat, Verein menschlichkeit-ulm
Simone Schliemann, Eine Welt          Regionalpromotorin
Hilde Mattheis, MdB

Weitere Informationen unter: www.rettungskette-ulm-neu-ulm.de

Veranstalter:
Bündnis Rettungskette Ulm / Neu-Ulm

Sammelpunkte für Ulm und Neu-Ulm bei www.rettungskette-ulm-neu-ulm.de

Mehr Informationen über die Ulmer Friedenswochen:
https://www.friedenswochen-ulm.de
