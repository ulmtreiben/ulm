---
id: "366822538181267"
title: "Nachdenken über Kriegerdenkmäler in Ulm: VERSCHOBEN!!!!"
start: 2021-09-28 18:00
locationName: Einsteinhaus Ulm
address: Kornhausplatz 5, D-89073  Germany, 89073 Ulm
link: https://www.facebook.com/events/366822538181267/
image: 237340040_6638589549499751_8169586688613094964_n.jpg
isCrawled: true
---
Die Veranstaltung muss aus organisatorischen Gründen verschoben werden. Das Nachdenken wird NICHT verschoben!

An vielen Orten findet man sie, die Kriegertafeln und -denkmäler – auch in Ulm. Sie symbolisieren stark den „Zeitgeist“ ihrer Entstehung. Da wird das „ehrende Gedenken an die gefallenen Kameraden“ beschworen oder auch die tradierte Formel vom „Opfertod für das Vaterland“ benutzt. Und heute? Ist es nicht überfällig, sie zu Friedensmahnmalen einer lebendigen Erinnerungskultur werden zu lassen? Wir wollen dies in einer Gesprächsrunde – in praktischer Absicht – für Ulm diskutieren.   

Veranstalter:
Gruppe Friedensbewegt Ulm

Ort: 
EinsteinHaus, Kornhausplatz 5, 89073 Ulm  

Eintritt frei 

Mehr Informationen über die Ulmer Friedenswochen:
https://www.friedenswochen-ulm.de
