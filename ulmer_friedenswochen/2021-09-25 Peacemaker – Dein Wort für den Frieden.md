---
id: "292984805962781"
title: Peacemaker – Dein Wort für den Frieden
start: 2021-09-25 10:00
end: 2021-09-25 12:00
address: Cafe Jam, Münsterplatz 21, 89073 Ulm
link: https://www.facebook.com/events/292984805962781/
image: 239310808_6635004523191587_896408532447515390_n.jpg
isCrawled: true
---
Die Jugendlichen des Evangelischen Jugendwerks (EJW), des CVJM und des Bundes der Deutschen Katholischen Jugend (BDKJ) wollen als christliche Kirchen ein Zeichen setzen für den Frieden. Was heißt für dich „Frieden“ und wie können wir uns hier vor Ort für ein friedliches Miteinander einsetzen? Diesen Fragen dürfen sich alle Passanten, die am Kaffeestand vor dem Café JAM zur Marktzeit vorbeikommen, stellen und in einer gemütlichen Atmosphäre ins Gespräch kommen.

Veranstalter:
EJW / CVJM / Café JAM 

Ort:
Café Jam, Münsterplatz 21, 89073 Ulm  

Eintritt frei

Mehr Informationen über die Ulmer Friedenswochen:
https://www.friedenswochen-ulm.de
