---
id: "412364616333752"
title: „Barriere:Zonen“ (Fotoausstellung)
start: 2020-09-16 14:00
end: 2020-09-16 19:00
address: ROXY.ulm
link: https://www.facebook.com/events/412364616333752/
teaser: Konflikte und Kriege werfen einen langen Schatten. Sie hinterlassen körperlich
  und seelisch Versehrte. Sie rauben Leben, Zukunft und Hoffnung. Manche
isCrawled: true
---
Konflikte und Kriege werfen einen langen Schatten. Sie hinterlassen körperlich und
seelisch Versehrte. Sie rauben Leben, Zukunft und Hoffnung. Manche der Kriege
sind seit Jahrzehnten vorbei, andere fordern noch immer neue Opfer. Die
Portraitierten kämpfen als Menschen mit Behinderung um ein würdiges Leben. Jeden
Tag aufs Neue. Die Portraits geben Mut, andere stimmen traurig. „Sie alle fordern
Respekt ein“, sagt Journalist und Fotograf Till Mayer.
ROXY Ulm
Eintritt: frei
Veranstalter: ROXY gemeinnützige GmbH