---
id: "639497540252384"
title: Friede, Freude, Eierkuchen (Kunstausstellung)
start: 2020-09-11 17:00
end: 2020-09-11 20:00
address: Kunstpool, Galerie am Ehinger Tor, Ulm
link: https://www.facebook.com/events/639497540252384/
teaser: Bildende Künstler haben sich von jeher mit Krieg und Frieden beschäftigt, das
  ist ja gewissermaßen ein Menschheitsthema, sie stehen da traditionell in
isCrawled: true
---
Bildende Künstler haben sich von jeher mit Krieg und Frieden beschäftigt, das ist ja gewissermaßen ein Menschheitsthema, sie stehen da traditionell in einer großen Verantwortung. Wir leben nicht in einer friedlichen Welt, auch wenn das der Blick auf unseren Nahbereich zu zeigen scheint. Und das, was  Frieden für die Menschen bedeutet, ist so überwältigend wichtig, dass wir nicht ruhen dürfen und das alte
Thema immer wieder aufrollen müssen. Die KUNSTPOOL-Galerie hat Künstler dazu aufgefordert, sich mit
Doppelmoral im Kontext von Krieg und Frieden zu beschäftigen, auch mit Kleingeistereien, die häufig Ursache von Unfrieden und systemrelevant für die Entstehung von Strukturen des Unfriedens und der Ungerechtigkeit sind.
Frieden bedeutet nicht nur die Abwesenheit von Krieg, heißt es in der Grundsatzerklärung der Friedenswochen, sondern auch die Schaffung von gerechten sozialen Zuständen, auf der Welt, aber auch im Innern einer Gesellschaft.
Aber natürlich wollten wir mit dem Aufruf zur Ausstellungsbeteiligung, im Unterschied zu Politik und Werbung, nicht zur Produktion von Propaganda oder Gebrauchsgrafik anregen.
Künstlerische Bearbeitungen eröffnen vielfältige Assoziationsräume, die nicht kontrollierbar sind und wild wuchern dürfen. In diesem Feld entwickeln sie ihre Polyvalenz und Sinn(en)haftigkeit. E
ine Jury wählte unter den eingereichten Arbeiten 20 Künstler*innen aus ganz Deutschland aus, die während der Ulmer Friedenswochen 2020 in einer Ausstellung zu sehen sind:

Myrah Adams (Neu-Ulm) 
Winfried Becker (Kempten)
Maren Dietrich (Georgensgmünd)
Josef Feistle (Weißenhorn)
Dorothea Grathwohl (Ulm)
Dietmar Herzog (Neu-Ulm)
Gisela Hoßfeld-Weber (Senden)
Petra Hübel (Otterbach)
Ruth Knecht (Blaubeuren)
Reinhard Köhler (Ulm)
Hans Liebl (Neu-Ulm)
Martin Lorenz (Landau)
Monika Meinold (Warstein)
Hans-Günther Obermeier (Köln)
Dietmar Paetzold (Köln)
Caritt Reichl (Ulm)
Kerstin Römhild (Lohr)
Harald Schmidt (Aachen)
Renate Vetter (Reutlingen)
Amei Gerlinde Wöllmer (Ulm)

KUNSTPOOL. Galerie am Ehinger Tor (Haltestellengelände)
Eintritt frei
Veranstalter: KUNSTPOOL. Galerie am Ehinger Tor, Verein für Friedensarbeit e. V.