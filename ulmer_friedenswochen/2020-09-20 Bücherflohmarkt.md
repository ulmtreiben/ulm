---
id: "2739643413026839"
title: Bücherflohmarkt
start: 2020-09-20 09:00
end: 2020-09-20 18:00
locationName: Rathaus Pfaffenhofen
address: Kirchplatz 6, 89284 Pfaffenhofen an der Roth
link: https://www.facebook.com/events/2739643413026839/
teaser: Im Rahmen des Pfaffenhofener Herbstmarktes veranstaltet die Tibet Initiative
  wieder ihren großen Bücherflohmarkt (auch mit DVDs und CDs). Der Erlös de
isCrawled: true
---
Im Rahmen des Pfaffenhofener Herbstmarktes veranstaltet die Tibet Initiative wieder ihren großen Bücherflohmarkt (auch mit DVDs und CDs).
Der Erlös des Flohmarktes finanziert u. a. verschiedene Projekte der Tibet Initiative.

Eintritt frei.
Veranstalter: Tibet Initiative Deutschland e. V., Regionalgruppe Ulm/Neu-Ulm
