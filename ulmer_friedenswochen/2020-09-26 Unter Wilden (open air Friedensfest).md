---
id: "699024347328068"
title: Unter Wilden (open air Friedensfest)
start: 2020-09-26 20:00
end: 2020-09-26 23:00
locationName: Fort Unterer Kuhberg
address: 10  Unterer Kuh berg, 89077 Ulm
link: https://www.facebook.com/events/699024347328068/
image: 104068432_670806200315951_10446381635120589_o.jpg
teaser: UNTER WILDEN ist eine neue Ulmer Band um Sänger Simon Kombrink, der seine
  Texte in deutscher Sprache schreibt. Schlagzeug, Bass, Keyboard und natürlic
isCrawled: true
---
UNTER WILDEN ist eine neue Ulmer Band um Sänger Simon Kombrink, der seine Texte in deutscher Sprache schreibt. Schlagzeug, Bass, Keyboard und natürlich Stimmbänder. Die Band sitzt mit ihre, Sound zwischen allen Stilen und Stühlen. Musik und Texte mit Feinheiten, anspruchsvolle Unterhaltung mit Tiefgang, aber ohne intellektuelle Überfrachtung.

Außerdem noch mit dabei:
Feschtagsmusik
Tilufa