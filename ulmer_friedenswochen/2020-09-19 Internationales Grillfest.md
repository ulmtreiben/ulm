---
id: "288239222407129"
title: Internationales Grillfest
start: 2020-09-19 12:00
end: 2020-09-19 19:00
address: Fort Unterer Kuhberg 16, 89075 Ulm
link: https://www.facebook.com/events/288239222407129/
teaser: "Leider muss die Veranstaltung aus organisatorischen Gründen
  ausfallen.  Veranstalter: menschlichkeit-ulm e. V. und Freundschaft, Kultur
  und Jugend e."
isCrawled: true
---
Leider muss die Veranstaltung aus organisatorischen Gründen ausfallen.

Veranstalter: menschlichkeit-ulm e. V. und Freundschaft, Kultur und Jugend e. V. Ulm (DIDF-Ulm)
