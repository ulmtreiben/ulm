---
id: "888510825205336"
title: Friedenssong-Marathon
start: 2021-09-29 10:00
end: 2021-09-29 17:00
link: https://www.facebook.com/events/888510825205336/
image: 237167924_6638599579498748_8450901755669022030_n.jpg
isCrawled: true
---
Friedenssongs gibt es in allen Sprachen. Wir wollen mit unseren internationalen Redaktionen zum Friedenssong-Marathon einen Blick auf internationale Künstler*innen werfen. Doch auch Sie sollen das Programm aktiv mitgestalten. Schicken Sie Songvorschläge und Friedensbotschaft per Whatsapp ins Studio unter
0731/9 38 62 99 oder per Message an unser Facebook und Instagram (Radio free FM). Bestimmen Sie mit unseren Redakteur*innen zusammen, was an diesem Tag gespielt wird!

Radio free FM
UKW 102,6 MHz, Stream freefm.de

Mehr Informationen über die Ulmer Friedenswochen:
https://www.friedenswochen-ulm.de
