---
id: "323694889540903"
title: Bücherflohmarkt (muss leider coronabedingt ausfallen)
start: 2021-09-19 09:00
end: 2021-09-19 17:00
locationName: Rathaus Pfaffenhofen
address: Kirchplatz 6, 89284 Pfaffenhofen an der Roth
link: https://www.facebook.com/events/323694889540903/
image: 236982629_6634852056540167_8072335859725639867_n.jpg
isCrawled: true
---
Leider muss der Flohmarkt coronabedingt ausfallen.

Im Rahmen des Pfaffenhofener Herbstmarktes veranstaltet die Tibet Initiative wieder ihren großen Bücherflohmarkt (auch mit DVDs und CDs).
Der Erlös des Flohmarkts finanziert u. a. verschiedene Projekte der Tibet Initiative.

Veranstalter:
Tibet Initiative Deutschland e. V., Regionalgruppe Ulm/NeuUlm

Ort: 
Rathaus Pfaffenhofen, Kirchplatz 6, 89284 Pfaffenhofen a. d. Roth 

Eintritt frei

Mehr Informationen über die Ulmer Friedenswochen:
https://www.friedenswochen-ulm.de
