---
id: "172311278227019"
title: Friedensgebet
start: 2021-09-19 20:00
address: Auf dem Schal Neu-Ulm
link: https://www.facebook.com/events/172311278227019/
image: 236581789_6634879619870744_5075994783874713088_n.jpg
isCrawled: true
---
Weil wir Menschen den Frieden nicht schaffen – daher wollen wir Gott um seine Mithilfe bitten. Die Botschaft von Jesus ist klar und deutlich: Liebt nicht nur eure Nächsten, sondern auch eure Feinde. Ein hoher Anspruch. Damit uns das gelingt, wollen wir um Hilfe bitten. Doch nicht nur das Gebet, auch Friedenslieder sind zu hören und sollen zum Mitsingen einladen. 

Veranstalter:
Katholische Kirche Neu-Ulm, Diakon Wolfgang Dirscherl

Ort:
Schwal-Denkmal auf der Donauinsel 89231 Neu-Ulm – bei schlechtem Wetter in der Kirche St. Johann Baptist, Johannesplatz 1, 89231 Neu-Ulm  

   Eintritt frei

Mehr Informationen über die Ulmer Friedenswochen:
https://www.friedenswochen-ulm.de
