---
id: "2661318257495363"
title: "Agenda Rüstung: Deutschlands Strategie zur Stärkung der Waffenindustrie"
start: 2021-09-27 19:30
locationName: Einsteinhaus Ulm
address: Kornhausplatz 5, D-89073  Germany, 89073 Ulm
link: https://www.facebook.com/events/2661318257495363/
image: 237443987_6638521982839841_2760785180528411135_n.jpg
isCrawled: true
---
Seit Jahren erfolgt eine gezielte Stärkung und Subventionierung von Rüstungsunternehmen, indem systematisch immer mehr Gelder auch über Schattenhaushalte in den Bereich gepumpt, Exporthindernisse abgebaut und Rüstungsprojekte subventioniert werden. Das alles ist Teil einer Strategie, die sich hinter dem Begriff „Agenda Rüstung" verbirgt und die auch in Ulm ihren Niederschlag findet. 

Referent: Jürgen Wagner, Informationsstelle Militarisierung e. V.

Veranstalter:
- Verein für Friedensarbeit e. V.
- Ulmer Ärzteinitiative / IPPNW
- Verein Ulmer Weltladen e. V.
- vh Ulm

Ort:
EinsteinHaus, Kornhausplatz 5, 89073 Ulm 

Eintritt frei 

Mehr Informationen über die Ulmer Friedenswochen:
https://www.friedenswochen-ulm.de
