---
id: "885405078740914"
title: Stadtrundfahrt zum Nachdenken - Militär und Rüstung in Ulm
start: 2021-09-10 18:00
end: 2021-09-10 21:00
address: Treffpunkt Agnes-Schultheiß-Platz, Ulm
link: https://www.facebook.com/events/885405078740914/
image: 235668335_6592424654116241_8238806847365640768_n.jpg
isCrawled: true
---
Im Bewusstsein der Ulmer ist es nicht unbedingt, welche herausragende Rolle Militär und Rüstung hier spielen. Manchmal bekommt man häppchenweise Informationen. Heute machen wir uns im Reisebus auf den Weg, schauen uns die meisten dieser Stellen im Stadtbild an, manche auffällig, manche fast nicht erkennbar. Natürlich gibt es auch sachkundige Informationen.

Begrenzte Teilnehmerzahl, daher bitte anmelden: kontakt@ruestungsatlas-ulm.de

Treffpunkt 18 Uhr am Westbad, Moltkestraße 30, 89077 Ulm    |   Teilnahme kostenlos, Spenden willkommen

Veranstalter: Gruppe Friedensbewegt

Mehr Informationen über die Ulmer Friedenswochen:
https://www.friedenswochen-ulm.de
