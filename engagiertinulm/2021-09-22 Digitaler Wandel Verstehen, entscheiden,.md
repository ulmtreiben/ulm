---
id: "232769352125092"
title: "Digitaler Wandel: Verstehen, entscheiden, umsetzen"
start: 2021-09-22 15:30
end: 2021-09-22 18:30
address: Radgasse 8, 89073 Ulm
link: https://www.facebook.com/events/232769352125092/
image: 241977016_4685568134801353_1798340748967327039_n.jpg
isCrawled: true
---
Digitalisierung eröffnet zivilgesellschaftlichen Organisationen und Engagierten viele Chancen für wirkungsvolles Handeln – stellt sie aber auch gleichzeitig vor neuen Herausforderungen. In diesem dreistündigen Vor-Ort-Workshop widmen wir uns der Frage, wie der digitale Wandel in zivilgesellschaftlichen Organisationen gelingen kann. Dabei liegt der Schlüssel zum Erfolg nicht nur in der Einführung von neuen Technologien. Es kommt vor allem auf eine gut durchdachte, strategische Organisationsentwicklung an. Mithilfe von praxisnahen Orientierungsfragen geben wir Euch einen Leitfaden an die Hand, mit dem Ihr Eure Organisation fit für die Digitalisierung machen könnt.

Konkret beschäftigen wir uns in dem Workshop „Digitaler Wandel“ mit folgenden Fragestellungen:

• Wie verändert die Digitalisierung die Zivilgesellschaft?
• Welche Chancen und Herausforderungen hat die Digitalisierung für unsere Organisation ?
• Wie entwickeln wir eine digitale Strategie und welchen Einfluss hat diese auf die Organisationsstrukturen und -prozesse?
• In welchen Bereichen macht ein digitaler Veränderungsprozess Sinn und in welchen nicht?
• Wie können sich alle Engagierten an dem Digitalisierungsprozess beteiligen?
• Wo gibt es weitere Unterstützungsangebote bei der Umsetzung?

Da dies eine Präsenzveranstaltung sein wird, bitten wir um Anmeldung per Telefon unter 0731 70 88 555 oder per Mail bis Freitag, 17. September 2021.