---
id: "434582684263742"
title: "Workshop: Ehrenamt in der digitalen Welt"
start: 2021-03-12 16:00
end: 2021-03-12 17:30
link: https://www.facebook.com/events/434582684263742/
image: 154568021_4081815515176621_4717412535973403072_o.jpg
teaser: "Referent/in:  Dr. Markus Marquard, Theresa Kocher, ZAWiW Uni Ulm Die
  Digitalisierung beeinflusst alle unsere Lebensbereiche auf unterschiedliche
  Art u"
isCrawled: true
---
Referent/in: 
Dr. Markus Marquard, Theresa Kocher, ZAWiW Uni Ulm
Die Digitalisierung beeinflusst alle unsere Lebensbereiche auf unterschiedliche Art und Weise. Corona hat das im letzten Jahr noch verstärkt. Auch im Bereich der Freiwilligenarbeit ergeben sich dadurch Veränderungen beispielsweise für die Verwaltung, aber auch für die Kommunikation mit und zwischen Ehrenamtlichen. Die neuen Medien und das Internet sind als Arbeits- und Organisationsinstrumente kaum noch wegzudenken. Doch nicht nur bestehendes Ehrenamt ist durch Digitalisierung im Wandel. Vielmehr ergeben sich dadurch auch ganz neue Zielgruppen und Engagementformen. So unterstützen in Ulm beispielsweise bald ehrenamtliche Digitalmentor*innen in den Quartieren ihre Mitmenschen bei technischen Fragen und Problemen. Nicht zuletzt kann das freiwillige Engagement aber auch komplett online stattfinden. So können sich Menschen orts- und zeitunabhängig für eine gute Sache einbringen.
Was bedeutet die Digitalisierung also für das freiwillige Engagement? Welche Chancen und mögliche Schwierigkeiten ergeben sich daraus? Was ist wichtig für Engagierte, sowie für Vereine, Organisationen und andere Träger? Und schließlich: Welche Angebote gibt es hier in Ulm? Nach einem Vortrag freuen wir uns auf den Austausch mit Ihnen.

Anmeldung über heusohn@engagiert-in-ulm.de oder telefonisch unter 0731 70 88 514.
Den Zugangslink schicken wir Ihnen vor der Veranstaltung. 

Fotoquelle: unsplash.com 