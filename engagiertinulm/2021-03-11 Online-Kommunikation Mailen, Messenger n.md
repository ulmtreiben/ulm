---
id: "241961333930289"
title: "Online-Kommunikation: Mailen, Messenger nutzen und Videotelefonie starten"
start: 2021-03-11 16:00
end: 2021-03-11 18:00
link: https://www.facebook.com/events/241961333930289/
image: 154000611_4086702664687906_2660538226156453511_o.jpg
teaser: In Kooperation mit der Digitalen Nachbarschaft (Deutschland sicher im Netz e.
  V.) In dem zweistündigen digitalen Seminar „Online- Kommunikation“ besch
isCrawled: true
---
In Kooperation mit der Digitalen Nachbarschaft (Deutschland sicher im Netz e. V.)
In dem zweistündigen digitalen Seminar „Online- Kommunikation“ beschäftigen wir uns mit den Themen sichere E-Mails, Schutz vor Phishing und Co. sowie mit Instant Messaging.
Möchten Sie wissen, welche digitalen Möglichkeiten Sie haben, sicher mit Ihren Mitgliedern oder Kolleg*innen zu kommunizieren? Dann sind Sie in diesem Online- Seminar genau richtig!
Wir geben Ihnen einen Überblick über digitale Kommunikationsanwendungen und wie Sie diese sicher nutzen können. Mithilfe von konkreten Beispielen und Übungen können Sie Ihr neues Wissen direkt in die Praxis umsetzen.
Konkret gibt Ihnen das Online-Seminar „Online- Kommunikation“ Antworten auf
folgende Fragestellungen:
• Worauf muss ich beim Versand von E- Mails achten?
• Was ist Spam, Phishing und Malware und wie schütze ich mich davor?
• Was ist Instant Messaging?
• Wie kann ich meine Privatsphäre und Daten beim Messaging schützen?
• Wieso steht WhatsApp als Messenger- Dienst in der Kritik?
• Was zeichnet einen sicheren und datensparsamen Messenger-Dienst aus?
• Welche (sicheren) Alternativen zu WhatsApp gibt es?
Anmeldung erforderlich: mreisi@engagiert-in-ulm.de