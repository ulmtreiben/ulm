---
name: engagiert in ulm e.V.
website: http://www.engagiert-in-ulm.de/
email: info@engagiert-in-ulm.de
scrape:
  source: facebook
  options:
    page_id: engagiert-in-ulm-e-V-194760997215445
---
Gemeinsam mit vier weiteren Organisationen des Bürgerschaftlichen Engagements macht “engagiert in ulm” die Adresse Radgasse 8 zu einer Anlaufstelle für viele Anliegen der Bürger*innen in Ulm. Beratung in Selbsthilfefragen, vergünstigte Tickets, Beratung für Radfahrer*innen oder die beliebte FREIWILLIGENCARD sind einige davon.
