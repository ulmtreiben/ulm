---
id: "618563365750103"
title: Ulmer Engagements on-Leine und online
start: 2020-09-11 10:00
end: 2020-09-20 18:00
address: Ulm in Germany
link: https://www.facebook.com/events/618563365750103/
teaser: In der Woche des bürgerschaftlichen Engagements vom 11. bis 20. September 2020
  wollen wir online und offline auf die verschiedenen Engagementfelder in
isCrawled: true
---
In der Woche des bürgerschaftlichen Engagements vom 11. bis 20. September 2020 wollen wir online und offline auf die verschiedenen Engagementfelder in Ulm aufmerksam machen. Dazu werden Informationen der teilnehmenden Einrichtungen auf farbige Blätter gedruckt und in der Stadt aufgehängt. Digital stellen wir verschiedene ehrenamtliche Tätigkeiten an Thementagen in Form von kurzen Videos vor.  