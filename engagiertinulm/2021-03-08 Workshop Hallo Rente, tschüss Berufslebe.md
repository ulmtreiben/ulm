---
id: "758122928462642"
title: "Workshop: Hallo Rente, tschüss Berufsleben – Vorbereitung auf eine neue
  Lebensphase"
start: 2021-03-08 17:00
end: 2021-03-08 18:30
link: https://www.facebook.com/events/758122928462642/
image: 153253967_4081794038512102_255198098167207589_o.jpg
teaser: "Referentin:  Eva Hrabal, ZAWiW Uni Ulm  Den Übergang in die Rente empfinden
  viele Menschen als großen Einschnitt – bei den einen überwiegen positive E"
isCrawled: true
---
Referentin:  Eva Hrabal, ZAWiW Uni Ulm

Den Übergang in die Rente empfinden viele Menschen als großen Einschnitt – bei den einen überwiegen positive Erwartungen, bei anderen eher negative. Sind Sie voller Vorfreude oder sehen Sie dem neuen Lebensabschnitt eher mit Bedacht entgegen?
In diesem Workshop möchten wir gemeinsam beleuchten, welchen Chance und Potenziale der neue Lebensabschnitt mit sich bringen kann und wie man mit etwaigen Herausforderungen umgehen kann. Dabei soll auch herausgearbeitet werden, welche Rolle das Engagement in der nachberuflichen Phase spielen kann, welchen Mehrwert es bietet und was es zu beachten gilt. 

Anmeldung unter heusohn@engagiert-in-ulm.de oder telefonisch unter 0731 70 88 514. Den Zugangslink schicken wir Ihnen vor der Veranstaltung. 

Fotoquelle: https://pixabay.com/de/