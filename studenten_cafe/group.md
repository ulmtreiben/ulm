---
name: Studentencafé
website: https://www.studentencafe.de
email: chef@studentencafe.de
scrape:
  source: facebook
  options:
    page_id: studentencafe
---
Das Studentencafe wird nun schon seit über 35 Jahren von Studenten und Nicht-Studenten ehrenamtlich betrieben und ist in der Ulmer Kneipenszene eine günstige und clevere Alternative zu den kommerziellen Lokalitäten.
Unsere Studentenkneipe bietet ein ganz besonderes Flair, da sie sich in einem renovierten Teil der Ulmer Bundesfestungsanlage unterhalb der Hochschule Ulm befindet. Das alte Gemäuer bietet eine chillig-urige Atmosphäre.
