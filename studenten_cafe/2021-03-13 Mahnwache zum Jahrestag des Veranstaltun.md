---
id: "485069275849412"
title: Mahnwache zum Jahrestag des Veranstaltungslockdown
start: 2021-03-13 15:00
end: 2021-03-13 16:30
address: Münsterplatz, 89073 Ulm
link: https://www.facebook.com/events/485069275849412/
teaser: "Liebe kulturgesichter, liebe Freunde der Aktion,  wir haben eine Idee: wir
  treffen uns am 13.03.2021 um 15h zur Mahnwache auf dem Münsterplatz, um an"
isCrawled: true
---
Liebe kulturgesichter, liebe Freunde der Aktion,

wir haben eine Idee:
wir treffen uns am 13.03.2021 um 15h zur Mahnwache auf dem Münsterplatz, um an den Jahrestag des Veranstaltungs-Lockdowns zu erinnern.
Für diese Mahnwache gilt: Abstand halten & Maske auf! Wir wollen alle schnellstmöglich zurück auf Veranstaltungen! Also haltet euch an die Regeln!