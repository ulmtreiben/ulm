---
id: "361885678977630"
title: Electroswing-Techno-Psytech-DrumN'Bass • Wir sind wieder da!
start: 2021-09-18 16:00
end: 2021-09-18 22:00
locationName: Studentencafé
address: Stuttgarter Strasse 15, 89075 Ulm
link: https://www.facebook.com/events/361885678977630/
image: 241200537_4323609777720956_5175318035083570077_n.jpg
isCrawled: true
---
WIR SIND WIEDER DA!!!!!! 

Und das muss gefeiert werden! 

Wir haben alles was ihr braucht: 
• Electroswing
• Techno 
• Psytech 
• Drum n' Bass

Dazu kühle Getränke in eurer Lieblingslocation ♥
Wir haben euch vermisst, kommt vorbei!

Der Eintritt ist frei - zwischen 21:00 und  22:00 Uhr gibt´s ein Getränkespecial. Lasst euch überraschen! 

• 16:00 bis 22:00 Uhr 
Lounge im Garten
-> Die Maskenpflicht (medizinische Maske oder FFP2) entfällt nur am Tisch/Sitzplatz
-> Registrierung über die Luca-App oder Kontaktdatenzettel
-> Es sind 1,5 m Abstand zu halten


Zur leichteren bzw. schnelleren Kontaktdatenaufnahme könnt ihr die LucaApp verwenden.
Da eine Nichteinhaltung für uns sehr schnell sehr teuer werden kann, sind wir im Zweifelsfall dazu gezwungen, Gäste der Veranstaltung zu verweisen oder sogar die Party zu beenden.
Es gelten die Regelungen der CoronaVO des Landes BW.