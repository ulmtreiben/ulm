---
name: Ulm isst gut e.V.
website: http://www.ulm-isst-gut.de
email: info@ulm-isst-gut.de
scrape:
  source: facebook
  options:
    page_id: Ulm-isst-gut-eV-1781184585486397
---
Im Verein ulm isst gut e.V. haben sich unterschiedliche
regionale Gruppen, Organisationen und Betriebe zusammengeschlossen, um den Verbraucher-, Tier- und Umweltschutz im Zusammenhang mit unserer Ernährung zu fördern.
