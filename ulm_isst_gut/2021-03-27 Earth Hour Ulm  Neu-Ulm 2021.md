---
id: "184212593088578"
title: Earth Hour Ulm / Neu-Ulm 2021
start: 2021-03-27 20:30
end: 2021-03-27 21:30
address: Ulm/Neu-Ulm
link: https://www.facebook.com/events/184212593088578/
image: 153200622_3538436939598803_2571742509792948961_n.jpg
teaser: In Ulm und Neu-Ulm gehen wieder die Lichter aus! Setzt ein Zeichen mit uns -
  für Klimaschutz und Energiewende.   Mitmachen kann jedeR, auch und gerade
isCrawled: true
---
In Ulm und Neu-Ulm gehen wieder die Lichter aus! Setzt ein Zeichen mit uns - für Klimaschutz und Energiewende. 

Mitmachen kann jedeR, auch und gerade von zuhause aus: Schaltet am 27. März für eine Stunde die Lichter, Computer, Fernseher und andere verzichtbare Geräte aus. 

Wir planen eine Online-Mitmach-Veranstaltung, Infos demnächst hier!