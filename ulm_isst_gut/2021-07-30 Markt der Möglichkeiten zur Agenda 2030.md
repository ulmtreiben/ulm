---
id: "1207486859693004"
title: Markt der Möglichkeiten zur Agenda 2030
start: 2021-07-30 14:00
end: 2021-07-30 18:00
locationName: Münsterplatz Ulm
address: Münsterplatz 21, 89073 Ulm
link: https://www.facebook.com/events/1207486859693004/
image: 221764034_3972888442820315_3029082085388596104_n.jpg
isCrawled: true
---
Ulmer Organisationen präsentieren ihre Aktivitäten rund um die 17 Ziele für nachhaltige Entwicklung und zeigen, wo und wie Du selbst aktiv werden kannst!

Mit dabei: ADFC, AG Städtisches Gärtnern, BUND, divest ulm, GWÖ, Haus der Nachhaltigkeit, Ingenieure ohne Grenzen, Klimacamp Ulm, Landratsamt Alb-Donau, lokale agenda ulm 21, Naturfreunde Ulm, Parents and People for Future, Psychologists for Future, Radio Free FM, Regionale Energieagentur, Sanierungsmobil Zukunft Altbau, Stadt Ulm, Technische Hochschule Ulm, VCD.