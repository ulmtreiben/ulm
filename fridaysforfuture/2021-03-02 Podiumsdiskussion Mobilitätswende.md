---
id: "630364471084834"
title: Podiumsdiskussion Mobilitätswende
start: 2021-03-02 18:00
link: https://www.facebook.com/events/630364471084834/
image: 151345364_2848409228704694_5132955628197626123_o.jpg
teaser: Bis 2035 muss der Autoverkehr halbiert und der ÖPNV verdoppelt werden, um das
  1,5 Grad-Ziel des Pariser Klimaabkommens einhalten zu können. Aber nicht
isCrawled: true
---
Bis 2035 muss der Autoverkehr halbiert und der ÖPNV verdoppelt werden, um das 1,5 Grad-Ziel des Pariser Klimaabkommens einhalten zu können. Aber nicht nur für das Klima ist eine Mobilitätswende unumgänglich, sondern auch für die Gesundheit und eine bessere Lebensqualität.

Am 02.03.2021 von 18 bis 20 Uhr fragen wir die Landtagskandidaten aus Ulm nach Ihren Plänen für eine Mobilitätswende in Baden-Württemberg. Die Diskussion wird digital stattfinden und kann mitverfolgt werden. Zudem gibt es die Möglichkeit selbst Fragen über den Chat mit in die Diskussion einzubringen.

Den Link zur Anmeldung ist zeitnah auf www.unw-ulm.de zu finden.