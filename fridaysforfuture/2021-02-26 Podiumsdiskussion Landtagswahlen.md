---
id: "415743149502830"
title: Podiumsdiskussion Landtagswahlen
start: 2021-02-26 17:00
link: https://www.facebook.com/events/415743149502830/
image: 149034686_2845378832341067_959809441144561396_o.jpg
teaser: "#wirmüssenreden  Was wollen Ulmer Regionalpolitiker:innen für die
  Klimagerechtigkeit tun? Wie sehen die Wahlprogramme aus?  Am 26.2. um 17 Uhr
  veranst"
isCrawled: true
---
#wirmüssenreden

Was wollen Ulmer Regionalpolitiker:innen für die Klimagerechtigkeit tun? Wie sehen die Wahlprogramme aus?

Am 26.2. um 17 Uhr veranstalten wir von FFF Ulm/Neu-Ulm auf dem Münsterplatz eine Podiumsdiskussion mit den Wahlkandidaten für Ulm der größten Parteien und stellen ihnen dort diese und noch mehr Fragen. Denn das ist eine #klimawahl! #KlimaWähltGerechtigkeit

Es gibt auch die Möglichkeit online die Podiumsdiskussion live zu verfolgen unter folgendem Link: 

https://www.youtube.com/watch?v=Jm9RktlAMOg

Wir freuen uns auf euch!

#2021 #landtagswahl #bawü #ltw #wählen #wählengehen #diskussion #discussion #demo #demonstration #aktivismus #politik #präsenzzeigen #future #takeresponsibility⁠ #actnow ⁠#fridaysforfuture #fff #fffulmneuulm #neuulm #ulm #stadtulm #ulmcity #stadtneuulm #ulmtagundnacht #neuulmcity #forourfuture #climatecrisis #climatejustice #klimagerechtigkeit #klimakrise #climate #klima Weniger anzeigen