---
id: "379457853144227"
title: Klimaaktionstag
start: 2020-12-11 16:30
end: 2020-12-11 19:30
address: Ulm in Germany
link: https://www.facebook.com/events/379457853144227/
image: 128565220_2793204604225157_7227283349296526444_n.jpg
teaser: "Unter dem Motto #FightFor1Point5 und zum 5-jährigen Pariser Klimaabkommen
  wird es am 11.12 auf dem Münsterplatz eine Kundgebung mit Reden, Musik, Vide"
isCrawled: true
---
Unter dem Motto #FightFor1Point5 und zum 5-jährigen Pariser Klimaabkommen wird es am 11.12 auf dem Münsterplatz eine Kundgebung mit Reden, Musik, Videos und Kerzenschriftzügen geben. 

Wer kann, gerne eigene Lichter mitbringen! 

Wir freuen uns auf euch :) 