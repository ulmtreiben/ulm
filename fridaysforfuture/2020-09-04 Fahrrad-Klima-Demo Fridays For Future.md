---
id: "321385375979499"
title: Fahrrad-Klima-Demo Fridays For Future
start: 2020-09-04 15:00
end: 2020-09-04 17:00
address: Münsterplatz, 89073 Ulm
link: https://www.facebook.com/events/321385375979499/
teaser: // Fahrrad-Klima-Demo diesen Freitag //  Wir sind hier, wir sind laut, weil
  ihr uns die Zukunft klaut!!!⁠ ⁠ Seid dabei, wenn wir am 04. September der
isCrawled: true
---
// Fahrrad-Klima-Demo diesen Freitag //

Wir sind hier, wir sind laut, weil ihr uns die Zukunft klaut!!!⁠
⁠
Seid dabei, wenn wir am 04. September der verfehlten Klima-Politik wieder sagen, was wir von ihr halten!⁠
Klimakatastrophe und einfach nur zukucken? Nicht mit uns!⁠
⁠
Treffpunkt Münsterplatz.
Fahrrad und Maske mitbringen und ab geht's! Wir freuen uns auf euch! ⁠

Im Anschluss gibt es die Möglichkeit, euer Fahrrad vom #ADFC als Diebstahlschutz codieren zu lassen!

#CyclingForFuture
#FridaysForFutureUlm
#ParentsForFutureUlm
#Fahrraddemo
⁠