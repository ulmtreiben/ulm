---
id: "451707472699133"
title: Globaler Klimastreik
start: 2021-03-19 08:00
address: Ulm Münsterplatz
link: https://www.facebook.com/events/451707472699133/
image: 158617257_265715655028559_7013744451627809680_o.jpg
teaser: Vor fünf Jahren wurde das Pariser Klima-Abkommen unterzeichnet. Vor drei Jahre
  gelangte der alarmierende Bericht des IPCC an die Öffentlichkeit. Es is
isCrawled: true
---
Vor fünf Jahren wurde das Pariser Klima-Abkommen unterzeichnet. Vor drei Jahre gelangte der alarmierende Bericht des IPCC an die Öffentlichkeit. Es ist zwei Jahre her, dass Millionen Jugendliche auf der ganzen Welt auf die Straße gingen, um für ihre Zukunft zu streiken. Wir wünschten, wir könnten sagen, dass sich die Dinge seither verbessert haben, dass die Welt gerettet und alles in Ordnung sei – aber das wäre eine Lüge. Die Klimakrise verschlimmert sich und nimmt verheerende Ausmaße an.

Wenn wir jetzt nicht handeln, werden unsere Zukunft und Gegenwart katastrophal. Deshalb streiken wir – für sofortiges und konsequentes Handeln in Zeiten der Krise. Für Klimagerechtigkeit. Weltweit und coronakonform am 19. März. Auch in Ulm!

Am 19. März streiken wir und die Welt. ⁠
Wir haben eine umfangreiche 24H Aktion geplant von der ihr bald mehr Infos bekommt! Natürlich läuft auch dieser Protest wieder sicher und COVID-19 konform ab.⁠
⁠
Gerade jetzt ist #Klimagerechtigkeit wichtiger denn je, denn unser Klima wartet nicht bis die Pandemie vorbei ist. Wir bleiben laut und kämpfen um so mehr für Klimagerechtigkeit!⁠
⁠
Falls ihr Interesse habt mitzuwirken in unserer Orga, dann meldet euch gerne! Wir haben immer Platz für neue engagierte Seelen. ⁠
⁠
#bawü #aktivismus #fff #future #actnow #ulm #ulmcity #neuulm #Klimagerechtigkeit #forourfuture #190321 #climate #climatecrisis #fffulmneuulm #politics #climatejustice
#AlleFür1Komma5