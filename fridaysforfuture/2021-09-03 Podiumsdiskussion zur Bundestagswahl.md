---
id: "1186662635189844"
title: Podiumsdiskussion zur Bundestagswahl
start: 2021-09-03 16:00
locationName: Münsterplatz Ulm
address: Münsterplatz 21, 89073 Ulm
link: https://www.facebook.com/events/1186662635189844/
image: 241124272_2986168208262128_2973046973403522625_n.jpg
isCrawled: true
---
Wir reden mit den Politiker:innen von CDU/CSU, FDP, Grüne, Linke, SPD, die in Ulm und Neu-Ulm zur Bundestagswahl aufgestellt wurden.

Auch als Livestream auf unserem YouTube Kanal