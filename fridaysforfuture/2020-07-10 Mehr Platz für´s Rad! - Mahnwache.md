---
id: "2633850640225088"
title: Mehr Platz für´s Rad! - Mahnwache
start: 2020-07-10 17:30
end: 2020-07-10 18:30
locationName: Münsterplatz Ulm
address: Münsterplatz 21, 89073 Ulm
link: https://www.facebook.com/events/2633850640225088/
image: 107020807_158724042394388_6489024836126727138_n.jpg
teaser: Seit dem „Lockdown“ boomt die Nachfrage nach Fahrrädern in Deutschland - trotz
  Rezession und wirtschaftlicher Ungewissheit. Die Menschen haben das Fah
isCrawled: true
---
Seit dem „Lockdown“ boomt die Nachfrage nach Fahrrädern in Deutschland - trotz Rezession und wirtschaftlicher Ungewissheit. Die Menschen haben das Fahrrad wiederentdeckt. 

Beste Voraussetzungen für die Verkehrswende!
Denn die ist auch angesichts des immer weiter voranschreitenden Klimawandels mehr als notwendig. Damit die Verkehrswende gelingt, brauchen wir eine Infrastruktur, die ein sicheres und bequemes Radfahren in der Stadt ermöglicht. Eine Infrastruktur, die dazu einlädt, das Fahrrad zu nutzen. Davon ist Ulm noch weit entfernt. 

Wir fordern daher: 
#MEHRPLATZFÜRSRAD

- Einrichten temporärer Radfahrstreifen (Pop-up-Bike-Lanes)
- Erhöhung des Radverkehrsetats
- Aufstockung der Zahl der Radverkehrsbeauftragten
- Rascher Ausbau eines lückenlosen sicheren Radverkehrsnetzes
- Öffnung der Busspuren für Radfahrende
- Konsequente Verfolgung von Falschparkern auf Radwegen, Radfahrstreifen und Schutzstreifen und in Fahrradstraßen
- Flächendeckende und sichere Fahrradabstellmöglichkeiten 

Die Stadt Ulm muss endlich die sich selbst gesteckten Ziele umsetzen!

Unterstützt unsere Forderungen bei der Mahnwache am Freitag, den 10. Juli von 17.30 bis 18.30 Uhr auf dem Münsterplatz.

Bitte Mund-Nasen-Schutz verwenden und mindestens 1,50 m Abstand untereinander halten, 5 m zu PassantInnen. Bringt nach Möglichkeit Eure Fahrräder mit, als Signal für nachhaltige Mobilität und um das Abstandhalten zu erleichtern. Personen, die sich krank fühlen oder krank sind, bleiben bitte zuhause. Damit beim Auftreten einer Corona-Infektion die Teilnehmenden benachrichtigt werden können, notieren wir auf freiwilliger Basis Kontaktdaten der Teilnehmenden. Bitte wendet Euch dafür an einen der Ordner vor Ort.

Organisation: ADFC Ulm/Alb-Donau und Greenpeace Ulm/Neu-Ulm
Unterstützer: BUND Donau-Iller, Ulm, Bündnis für eine agrogentechnikfreie Region (um) Ulm, divest ulm, Extinction Rebellion Ulm, Fridays For Future Ulm, Gemeinwohl-Ökonomie Ulm, Genfrei (um) Ulm, Lokale Agenda Ulm 21, Parents for Future Ulm/Neu-Ulm/Alb-Donau, Solidarische Landwirtschaft Ulm/Neu-Ulm, Ulmer Netz für eine andere Welt e.V., Umweltgewerkschaft Gruppe Ulm/Neu-Ulm.