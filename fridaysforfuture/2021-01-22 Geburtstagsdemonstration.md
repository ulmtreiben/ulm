---
id: "1883006901847944"
title: Geburtstagsdemonstration
start: 2021-01-22 15:00
end: 2021-01-22 17:30
locationName: Stadt Ulm
address: Marktplatz 1, 89073 Ulm
link: https://www.facebook.com/events/1883006901847944/
image: 135563046_2818759398336344_3023340058359152074_o.jpg
teaser: FFF Ulm / Neu-Ulm wird 2 Jahre alt! Seit dem 18.01.2019 gibt es die
  FridaysforFuture Ortsgruppe Ulm (mittlerweile Ulm/Neu-Ulm). Seit zwei Jahren
  veran
isCrawled: true
---
FFF Ulm / Neu-Ulm wird 2 Jahre alt!
Seit dem 18.01.2019 gibt es die FridaysforFuture Ortsgruppe Ulm (mittlerweile Ulm/Neu-Ulm). Seit zwei Jahren veranstalten wir regelmäßig Demonstrationen und andere Aktionen - und nun gibt es unsere erste Menschenkette! Am Freitag nach unserem Geburtstag, den 22.01.21 formen wir ab 15 Uhr gemeinsam eine über 800m lange Menschenkette zwischen Ulmer Rathaus und Petrusplatz! Es wird ganz viele verschiedene Aktionen währenddessen geben, zum Beispiel Reden, Dancing for Climate und vieles mehr. 

Wir freuen uns auf Euch!