---
id: "217466796291255"
title: Fridays for Future im Kino
start: 2020-03-20 15:00
end: 2020-03-20 18:00
locationName: Obscura Ulm. (Kino)
address: Schillerstr. 1, 89077 Ulm
link: https://www.facebook.com/events/217466796291255/
image: 89514238_607098539847777_7128964791539859456_o.jpg
teaser: "Film: AN INCONVENIENT SEQUEL: TRUTH TO POWER. Al Gore, ehemaliger
  US-Vizepräsident und Präsidentschaftskandidat, setzt seinen unermüdlichen
  Kampf gege"
isCrawled: true
---
Film: AN INCONVENIENT SEQUEL: TRUTH TO POWER.
Al Gore, ehemaliger US-Vizepräsident und Präsidentschaftskandidat, setzt seinen unermüdlichen Kampf gegen die globale Klimaerwärmung fort. Er bereist die Welt, um die aktuellen, durch den Klimawandel hervorgerufenen Veränderungen zu dokumentieren, um Klimaexperten zu schulen und die internationale Klimapolitik zu beeinflussen. Kameras folgen ihm hinter die Kulissen - in privaten und öffentlichen sowie humorvollen und ergreifenden Momenten. Anschaulich zeigt die packende Dokumentation, wie er seine Vision verfolgt und die Gefahren des Klimawandels mit Einfallsreichtum und Leidenschaft zu überwinden versucht.

Mit anschließender Podiumsdiskussion gemeinsam mit Fridays For Future Ulm und eingeladenen Gästen.