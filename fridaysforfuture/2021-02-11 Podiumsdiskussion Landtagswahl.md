---
id: "132285792089825"
title: Podiumsdiskussion Landtagswahl
start: 2021-02-11 17:00
link: https://www.facebook.com/events/132285792089825/
image: 149085061_2845257302353220_5224965141679152021_n.jpg
teaser: "#wirmüssenreden Was wollen Ulmer Regionalpolitiker:innen für die
  Klimagerechtigkeit tun? Wie sehen die Wahlprogramme aus?  Am 26.2. um 17 Uhr
  veransta"
isCrawled: true
---
#wirmüssenreden
Was wollen Ulmer Regionalpolitiker:innen für die Klimagerechtigkeit tun? Wie sehen die Wahlprogramme aus?

Am 26.2. um 17 Uhr veranstalten wir von FFF Ulm/Neu-Ulm auf dem Münsterplatz eine Podiumsdiskussion mit den Wahlkandidaten für Ulm der größten Parteien und stellen ihnen dort diese und noch mehr Fragen. Denn das ist eine #klimawahl! #KlimaWähltGerechtigkeit

Es wird ebenfalls die Möglichkeit geben die Podiumsdiskussion online live zu verfolgen auf unserer Youtube Seite: https://www.youtube.com/watch?v=Jm9RktlAMOg

Wir freuen uns auf euch!
#2021 #landtagswahl #bawü #ltw #wählen #wählengehen #diskussion #discussion #demo #demonstration #aktivismus #politik #präsenzzeigen #future #takeresponsibility⁠ #actnow ⁠#fridaysforfuture #fff #fffulmneuulm #neuulm #ulm #stadtulm #ulmcity #stadtneuulm #ulmtagundnacht #neuulmcity #forourfuture #climatecrisis #climatejustice #klimagerechtigkeit #klimakrise #climate #klima