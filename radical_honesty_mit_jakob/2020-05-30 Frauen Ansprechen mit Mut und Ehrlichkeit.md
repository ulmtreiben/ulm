---
id: "1"
title: Frauen Ansprechen mt Mut und Ehrlichkeit
start: 2020-05-30 10:00
end: 2020-05-30 18:00
address: Ulm
link: https://www.facebook.com/events/1212620592403069/
image: frauen_ansprechen_mit_mut_und_ehrlichkeit.jpg
teaser: "Wie oft hast du eine tolle Frau gesehen und sie aus Angst nicht angesprochen? Und wie oft hast du dir danach in den ARSCH gebissen?! Wenn dir das schon 10, 100 oder 1000 mal so ging, bist du hier genau richtig."
---

Wie oft hast du eine tolle Frau gesehen und sie aus Angst nicht angesprochen?

Und wie oft hast du dir danach in den ARSCH gebissen?!

Wenn dir das schon 10, 100 oder 1000 mal so ging, bist du hier genau richtig.

Hier geht es nicht um Manipulation, Techniken oder Tricks. Das hier ist kein “Pickup”-Workshop, bei dem es darum geht, um jeden Preis ‘ne Handynummer zu kriegen oder dich in irgendein Bett reinzumogeln.

“80% of success is about showing up.”

Die Frau(en) die du dir in deinem Leben wünscht, sind noch nicht da, weil sie nicht mal wissen, dass du existierst! Solange du unsichtbar, vorsichtig und zurückhaltend durch die Welt wandelst, musst du auf den Zufall vertrauen.

Vielleicht triffst du dann hoffentlich, wenn alles gut läuft, irgendwann deine Traumfrau ...

**FUCK THAT!**

Du bist gefragt dich ehrlich zu zeigen. Als Mann!

Deine Angst spüren und es trotzdem tun. Das ist wahre Stärke.

Stell dir die unglaubliche Freiheit vor, wenn du jede Frau, egal wo, ansprechen kannst. Einfach nur, weil du es willst. Und ihr endlich die Möglichkeit gibst einen feinen Kerl, wie dich, kennenzulernen!

Hinter deiner Angst, liegen die größten Schätze.

Lass uns gemeinsam auf die Straßen von Ulm gehen und die Angst mit Freude & Spiel konfrontieren. In einem TEAM aus kraftvollen, sensiblen, mutigen MÄNNERN. Ohne Macho-Scheiss, sondern brüderlich, unterstützend und liebevoll.

Weißt du jetzt schon, dass du Lust auf einen Tag des Abenteuers hast und dich weiter entwickeln willst? Dann klick einfach auf diesen Link, um dein Ticket zu buchen:

https://www.eventbrite.de/e/frauen-ansprechen-mit-mut-und-ehrlichkeit-30-mai-in-ulm-tickets-106348956592

**Ablauf:**

Zu Beginn kommen wir als Team zusammen, lernen uns kennen und stärken die Verbindung zwischen uns Männern. Dann geht es los mit einfachen Übungen zum warm werden und zur Überwindung der ersten Hemmungen.

Weiter geht es dann mit der eigentlichen Herausforderung: Frauen anzusprechen, mit Mut und Ehrlichkeit!

Hier geht es an's Eingemachte! Als Team werden wir uns maximal unterstützen. Kombiniert mit meinen Fähigkeiten, aus jahrelanger Arbeit als Coach, hast du dadurch vollen Rückenhalt.

Für die Integrierung der Erfahrung kommen wir dann immer zusammen, tauschen uns aus und reflektieren über das Erlebte.

Mach dich bereit für einen Tag voller Angstlust, Kameradschaft und Spiel!

**Wo?**
Ulm

**Wann?**
Samstag, 30. Mai
10:00 bis 18:00Uhr

**Investition in deine Power:**
94€+ Eventbrite-Gebühren

Wähle die Herausforderung, geh durch die Angst und spiel dabei!

Klick einfach auf den Link, um dir deinen Platz zu sichern. Wichtig:

https://www.eventbrite.de/e/frauen-ansprechen-mit-mut-und-ehrlichkeit-30-mai-in-ulm-tickets-106348956592

Die Plätze sind limitiert, um eine fokussiertes Männerteam garantieren zu können.

Schreib mir, wenn du Fragen hast an jakob.eichhorn@mail.de

Ich freue mich auf dich!

Jakob

---

So schreibt Jakob über sich selbst in der 3. Person:

"Jakob ist zertifizierter Radical Honesty Trainer, Pornosucht-Coach und hat mit Hunderten von Menschen in mehr als 20 mehrtägigen Workshops, Retreats und Veranstaltungen gearbeitet. Er lebt in Deutschland, liebt sein Leben und schreibt derzeit ein Buch über Sex."

|||-- Dieser Workshop ist NICHT für dich, wenn… --|||

  * du weiter mit Angst und Traurigkeit auf dein Liebesleben gucken willst.
  * du weiterhin ständig von dir selbst und den Frauen enttäuscht werden willst.
  * du dich beim Frauen Ansprechen weiterhin fertig machen willst.
  * du weiterhin ewig und sinnlos in der Innenstandt rumlaufen willst ohne Frauen anzusprechen.
  * du dir dein Zusammensein mit den Frauen weiterhin sabotieren willst mit sinnlosen Lügen, emotionslosem Vögeln oder unbewusster Oberflächlichkeit.
  * du den unmännlichen Bullshit der konventionellen Pick-up Theorie weiterhin glauben willst. 

|||-- Dieser Workshop ist für dich, wenn du… --|||

  * Frauen mit Liebe, Freude und Spaß ansprechen willst.
  * du erfahren möchtest, dass Mut und Bewusstheit erlernbar sind und dich aus deinem ewigen Dilemma von Zweifeln, Grübeln und Angst befreien können.
  * du nachhaltig in der Lage sein möchtest Frauen auch nach dem Workshop alleine ansprechen zu können.
  * du lernen willst, deine Ehrlichkeit beim Frauen ansprechen radikal und gewinnbringend für dich und die Frauen einzusetzen.
  * deine Fähigkeiten beim Frauen Ansprechen auf ein wertebasiertes, solides Fundament stellen willst.
  * du endlich auf deine Bequemlichkeit scheißen willst und stattdessen herausfordernde, krasse, ungewöhnliche Übungen praktizieren möchtest, bei denen du dir deinen Arsch ablachst.
  * du auf ein gelebtes Leben voller großartiger Erinnerungen zurückblicken willst. 

Ist es Zeit für dich rumzupimmeln oder Zeit für Abenteuer?!

Hol dir jetzt dein Ticket:
https://www.eventbrite.de/e/frauen-ansprechen-mit-mut-und-ehrlichkeit-30-mai-in-ulm-tickets-106348956592
